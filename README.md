# Documentation - Yono 0.1

**Reasonable [Symfony](http://symfony.com/doc/) and Linux know how is assumed!**

# Installation

### 1. Software

Following software has to be installed before starting deployment
(depending on the installion options some packets could be installed allready):

#### openSuse Leap 42.2

* **Apache**  
`apt-get install apache2`

* **PHP**  
`apt-get install php7 php7-intl php7-mysql php7-openssl php7-phar php7-curl`

* **MySQL/MariaDB**  
`apt-get install mysql-server`

* **Git**  
`apt-get install git`

* **CURL**  
`apt-get install curl`

* **Composer**  
`curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer`

* **Node.js and npm**  
`apt-get install npm`

* **Bower**  
`npm install bower -g`

* **Uglify**  
`npm install uglify-js -g`
`npm install uglifycss -g`

* **LibreOffice**  
`apt-get install libreoffice`

* **Unoconv**  
`apt-get install unoconv`

#### Ubuntu 16.04.2 LTS

* **Apache**  
`apt-get install apache2 libapache2-mod-php7.0`

* **PHP**  
`apt-get install php php-intl php-curl php-mysql`

* **MySQL/MariaDB**  
`apt-get install mysql-server`
    
* **Git**  
`apt-get install git`

* **CURL**  
`apt-get install curl`

* **Composer**  
`curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer`

* **Node.js and npm**  
`apt-get install npm`

* **Bower**  
`npm install bower -g`

* **Uglify**  
`npm install uglify-js -g`
`npm install uglifycss -g`

* **LibreOffice**  
`apt-get install libreoffice`

* **Unoconv**  
`apt-get install unoconv`

### 2. Configuration 

* **Apache**  
Configure a vhost with:
```
    <VirtualHost *:80>
        ServerAdmin webmaster@crea.de
        ServerName <subhost.example.com>

        DocumentRoot /srv/www/vhosts/yono/web

        ErrorLog /var/log/apache2/subhost.example.com-error_log
        CustomLog /var/log/apache2/subhost.example.com-access_log combined

        HostnameLookups Off
        UseCanonicalName Off
        ServerSignature Off

        <Directory "/srv/www/vhosts/yono/web">
            Options FollowSymLinks
            AllowOverride All
            Require all granted
            <IfModule mod_rewrite.c>
                Options -MultiViews
                RewriteEngine On
                RewriteCond %{REQUEST_FILENAME} !-f
                RewriteRule ^(.*)$ app.php [QSA,L]
            </IfModule>
        </Directory>

        <Directory /var/www/vhost/yono/web/bundles>
            <IfModule mod_rewrite.c>
                RewriteEngine Off
            </IfModule>
        </Directory>
    </VirtualHost>
```
    `systemctl enable apache2.service`  
    `service apache2 start`  
    `a2enmod php7`
    
* **PHP**  
Enable extentions in /etc/php7/apache2/php.ini:  
`intl`
`mb_string` only for phpmyadmin

* **MySQL/MariaDB**  
Start/Enable the database and harden it:  
`systemctl enable mysql.service`  
`service mysql start`  
`mysql_secure_installation`  

* **Composer**  
Test if composer works and has everything it needs:  
`composer --version`  
`composer diagnose`

* **Unoconv**  FIXME Does not work  
`unoconv libreoffice libreoffice-base`

#### 3. Configure Database

Use a frontend editor of your choice for MySQL like PhpMyAdmin, Workbench
or mysql command line tool.

* **PhpMyAdmin**  
Downloadlink for newest version: www.phpmyadmin.net  
`wget https://files.phpmyadmin.net/phpMyAdmin/4.7.2/phpMyAdmin-4.7.2-all-languages.zip`  
`apt-get install php7-mbstring`

* **Configure VHost**
```
    <VirtualHost *:80>
        ServerAdmin webmaster@crea.de
        ServerName phpmyadmin.localhost
    
        DocumentRoot /srv/www/vhosts/phpmyadmin
    
        ErrorLog /var/log/apache2/phpmyadmin-error_log
        CustomLog /var/log/apache2/phpmyadmin-access_log combined
    
        HostnameLookups Off
        UseCanonicalName Off
        ServerSignature Off
    
        <Directory "/srv/www/vhosts/phpmyadmin">
            Options FollowSymLinks
            AllowOverride None
            Require all granted
        </Directory>
    </VirtualHost>
```
* **Create database** with utf8_general_ci (e.g. yono)
* **Add user** with password (e.g. 'crea'@'localhost')
* **Grant all preveleges** of the database to the user

#### 4. Deploy Yono

Following information should be known before starting installation:

    database_host: host machine mysql/mariadb runs on
    database_port: port mysql/mariadb runs on
    database_name: database name of this instance
    database_user: database user to access the database instance
    database_password: database password to access the database instance

Use default value for all other properties! You can change them later in the file named 
parameter.yml!


* Navigate to your installation directory  
`cd /srv/www/vhosts`
    
* Clone the git repository  
If ksshaskpass bothers you, press ESC to close the dialog and go on with the command line  
`git clone https://gitlab.com/crea/yono.git`
    
* Use composer to install all php dependencies and configure yono  
`composer install`

* Prepare database  
`php bin/console doctrine:schema:update --force`
    
* Prepare assets  
`php bin/console assetic:dump --env=prod`
    
* Prepare menu  
1 for site menu -> 2 to create all menu items
If this doesn't work, delete them first with 1 -> 1
`bin/console yono:datafixtures`  

* Configure file owner and group permissions  
`chown -R wwwrun:www /srv/www/vhosts/yono

Create `/etc/init.d/unoconvd` with the following content:    
    
    #!/bin/sh
    ### BEGIN INIT INFO
    # Provides: unoconvd
    # Required-Start: $network
    # Required-Stop: $network
    # Default-Start: 2 3 5
    # Default-Stop:
    # Description: unoconvd - Converting documents to PDF by unoconv
    ### END INIT INFO
    case "$1" in
        start)
            /usr/bin/unoconv --listener &
            ;;
        stop)
            killall soffice.bin
            ;;
        restart)
            killall soffice.bin
            sleep 1
            /usr/bin/unoconv --listener &
            ;;
    esac

And start `unoconv` with `/etc/init.d/unoconvd start`

FIXME Enable on reboot

You can now login with `superadmin:superadmin`.

# 3rd-Party Bundles

[DoctrineFixturesBundle](http://symfony.com/doc/current/bundles/DoctrineFixturesBundle/index.html)

[StofDoctrineExtensionBundle](https://symfony.com/doc/current/bundles/StofDoctrineExtensionsBundle/index.html)

[FOSRestBundle](http://symfony.com/doc/current/bundles/FOSRestBundle/)

[FOSOAuthServerBundle](https://github.com/FriendsOfSymfony/FOSOAuthServerBundle/blob/master/Resources/doc/index.md)

[FOSUserBundle](https://github.com/FriendsOfSymfony/FOSUserBundle)

[FOSJsRoutingBundle](https://github.com/FriendsOfSymfony/FOSJsRoutingBundle)

[KnpMenuBundle](http://symfony.com/doc/current/bundles/KnpMenuBundle/index.html)

[KnpPaginatorBundle](https://github.com/KnpLabs/KnpPaginatorBundle)

[NelmioApiDocBundle](https://github.com/nelmio/NelmioApiDocBundle/blob/master/Resources/doc/index.rst)

[JMSSerializerBundle](https://github.com/schmittjoh/JMSSerializerBundle)

[LiipThemeBundle](https://github.com/liip/LiipThemeBundle)

[RavenbergUikitBundle](https://github.com/ravenberg/uikit-bundle)

[IvoryCKEditorBundle](https://github.com/egeloen/IvoryCKEditorBundle)

[Sentry](https://sentry.io/for/symfony/)

[Cron Bundle](https://packagist.org/packages/creadev/cron-bundle)


# Yono Specifics

- [1. Adding a Module](#1-adding-a-module)
- [2. Entities](#2-entities)
  - [2.1 Making Entities Blameable](#21-making-entities-blameable)
  - [2.2 Altering the User Entity](#22-altering-the-user-entity)
- [3. Login Listener](#3-login-listener)
- [4. Logout Listener](#4-logout-listener)
- [5. Browser Push Notifications](#5-browser-push-notifications)
- [6. Search Items](#6-search-items)
- [7. Flash Messages](#7-flash-messages)
- [8. Macros](#8-macros)
  - [8.1. Page Header](#81-page-header)
  - [8.2. Alerts](#82-alerts)
  - [8.3. Readable File Size](#83-readable-file-size)
- [9. Table Settings](#9-table-settings)
  - [9.1 In your Controller](#91-in-your-controller)
  - [9.2 In your Template](#92-in-your-template)
- [10. Themes](#10-themes)
- [11. Permissions](#11-permissions)
- [12. Api](#12-api)
- [13. Relations (experimental!)](#13-relations-experimental)
- [14. Document Conversion](#14-document-conversion)
- [15. Cron Jobs](#15-cron-jobs)

## 1. Adding a Module

A module is not much more than a symfony bundle. But it has to be in a certain namespace (`Modules`) and it must implement an interface (`Core\YonoBundleInterface`).

The bundle class must look like this:

    namespace Modules\MyBundle;
    
    use Core\YonoBundleInterface;
    use Symfony\Component\HttpKernel\Bundle\Bundle;
    
    class ModulesMyBundle extends Bundle implements YonoBundleInterface
    {
        /**
         * @return string
         */
        public function getDisplayName()
        {
            return 'My Awesome Bundle';
        }
    
        /**
         * @return string
         */
        public function getDescription()
        {
            return 'My Awesome Bundle can do a lot of magic!';
        }
        
        /**
         * @return string
         */
        public function getIcon()
        {
            return 'star';
        }
    }

Now add it to the Kernel (`app/AppKernel.php`) in the `Modules` section:

    public function registerBundles()
    {
        $bundles = [
            // ...
            
            // Modules
            // ...
            new Modules\MyBundle\ModulesMyBundle(),
            
            // ...
        ];
        
        // ...
    }

Add your configuration file to to list in `app/config/modules.yml`:

    imports:
        # ...
        - { resource: "@ModulesMyBundle/Resources/config/config.yml" }

This file can hold all the configuration for your bundle like adding services, extensions, twig globals and so on. It might look like this:

    services:
        my_bundle.helper:
            class: Modules\MyBundle\Service\MyBundleHelper
    
    twig:
        globals:
            my_bundle_helper: '@my_bundle.helper'

Of course you are free to separate your configuration in multiple files and then import them in your config.yml for example.

You can also generate your bundle sceleton with the following command:

    php bin/console generate:bundle --namespace=Modules/MyBundle

**Note: Please be aware that this command automatically alters the `config.yml` and `routing.yml`. Remove these changes, as they are implemented in other ways.**
    
## 2. Entities

You can add entities to your bundle as you like. Just make sure the table name is prefixed with `mod_mybundle_`.

    /**
     * MyEntity
     *
     * @ORM\Table(name="mod_mybundle_myentity")
     * ...
     */
    class MyEntity
    {
        // ...
    }

### 2.1 Making Entities Blameable

To make your entities "blameable" just use `Core\AppBundle\Entity\BlameableTrait`.

    use Core\AppBundle\Entity\BlameableTrait;
    
    /**
     * MyEntity
     * ...
     */
    class MyEntity
    {
        use BlameableTrait;
    
        // ...

This adds the properties `createdAt`, `createdBy`, `updatedAt`, `updatedBy` and the related getters and setters to your entity.

Thanks to some annotation magic this is all you need to do. Setting and updating these values happens automatically.

### 2.2 Altering the User Entity

It is possible to alter the core's user entity using traits. **BUT(!) please first consider, if you really need this. In most use cases it is totally fine if only your objects know about their user(s).**
Then of course you can't do some magic like `$this->getUser()->getMyEntities()` but you can still just use repositories for a `$myRepo->findByUser($user)` call for example.

Nevertheless, if you really want to add fields to the user entity you can use traits. Create a trait inside of your bundle:

    <?php
    namespace Modules\MyBundle\Entity;
    
    use Core\AppBundle\Entity\User;
    use Doctrine\Common\Collections\ArrayCollection;
    
    trait UserTrait
    {
        // Define your additional user fields.
        // You can use annotation to define the mappings.
        
        // Example:
        
        /**
         * @var MyEntity[]
         *
         * @ORM\OneToMany(targetEntity="Modules\MyBundle\Entity\MyEntity", mappedBy="user")
         */
        private $myEntities;
        
        /**
         * Get myEntities
         *
         * @return MyEntity[]
         */
        public function getMyEntities()
        {
            if (!$this->myEntities) $this->myEntities = new ArrayCollection();
    
            return $this->myEntities;
        }
        
        // Add more methods here...
    }

Add your trait to the main `UserTrait` inside the `Core\AppBundle`.

    <?php
    namespace Core\AppBundle\Entity;
    
    trait UserTraits
    {
        // ...
        use \Modules\MyBundle\Entity\UserTrait;
    }
    
Remember that you have to update the database if you added new fields!

    php bin/console doctrine:schema:update --force
    # or for short
    php bin/console d:s:u --force

## 3. Login Listener

To hook into the login to perform some actions with the user object you can add a `LoginListener`.

Just create a class implementing `LoginListenerInterface` inside of your bundle and register it as a service with the `app.login_listener` tag.

    <?php    
    namespace Modules\MyBundle\Event\Listener;
    
    
    use Core\AppBundle\Event\Listener\LoginListenerInterface;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\HttpFoundation\Response;
    use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
    
    class LoginListener implements LoginListenerInterface
    {
        public function login(Request $request, TokenInterface $token)
        {
            $user = $token->getUser();
            // ...
        }
    }
    
In your bundles config.yml (`Modules/MyBundle/Resources/config/config.yml`) add the service definition.

    services:
        my_bundle.login_listener:
            class: Modules\MyBundle\Event\Listener\LoginListener
            tags:
                - { name: app.login_listener, priority: 99 }

## 4. Logout Listener

It's the same for the logout. You can also add a `LogoutListener` implementing `LogoutListenerInterface` ...

    <?php    
    namespace Modules\MyBundle\Event\Listener;
    
    
    use Core\AppBundle\Event\Listener\LogoutListenerInterface;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\HttpFoundation\Response;
    use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
    
    class LogoutListener implements LogoutListenerInterface
    {
        public function logout(Request $request, Response $response, TokenInterface $token)
        {
            $user = $token->getUser();
            // ...
        }
    }
    
... and add the corresponding service.

    services:
        my_bundle.logout_listener:
            class: Modules\MyBundle\Event\Listener\LogoutListener
            tags:
                - { name: app.logout_listener, priority: 99 }

With the `priority` attribute you can define when the listener is executed. The highest priority gets executed first. Default is 0.

## 5. Browser Push Notifications

You can simply trigger browser push notifications with javascript:

    Push.create("Hello world!", {
        body: "How's it hangin'?",
        icon: 'https://nickersoft.github.io/push.js/icon.png',
        timeout: 4000,
        onClick: function () {
            window.focus();
            this.close();
        }
    });

This is nothing more than an implementation of [push.js](https://nickersoft.github.io/push.js/).

## 6. Search Items

The global search displays autocompleted suggestions in the search input in the top right corner of the UI and detailed results on the search page.
To add the possibility to search for your entites you need to add search services for them. Add a directory called `Search` to your bundle
and create a class file for each entity you want to be searchable.

These classes must extend `Core\AppBundle\Search\SearchItem` and you need to implement at least the two methods `getResults` and `getAutocompleteResults`.
You will also want inject some dependencies like the entity manager, twig and the router. So your classes might look like this:

    <?php
    namespace Modules\MyBundle\Search;
    
    use Core\AppBundle\Search\SearchItem;
    use Doctrine\ORM\EntityManager;
    use Symfony\Bridge\Twig\TwigEngine;
    use Symfony\Bundle\FrameworkBundle\Routing\Router;
    
    class MyEntity extends SearchItem
    {
        /**
         * @var EntityManager
         */
        private $em;
    
        /**
         * @var TwigEngine
         */
        private $twig;
    
        /**
         * @var Router
         */
        private $router;
    
        public function __construct(EntityManager $em, TwigEngine $twig, Router $router)
        {
            $this->em = $em;
            $this->twig = $twig;
            $this->router = $router;
        }
    
        /**
         * @param $query
         * @return array
         */
        public function getResults($query)
        {
            $myEntities = $this->em->getRepository('ModulesMyBundle:MyEntity')->createQueryBuilder('entity')
                ->where('entity.name LIKE :query')
                ->setParameter('query', '%' . $query . '%')
                ->getQuery()->getResult()
            ;
    
            $results = [];
            if ($myEntities) {
                foreach ($myEntities as $myEntity) {
                    $results[] = $this->twig->render('@ModulesMy/Search/entity.html.twig', ['entity' => $myEntity]);
                }
            }
    
            return $results;
        }
    
        /**
         * @param $query
         * @return array
         */
        public function getAutocompleteResults($query)
        {
            $myEntities = $this->em->getRepository('ModulesMyBundle:MyEntity')->createQueryBuilder('entity')
                ->where('entity.name LIKE :query')
                ->setParameter('query', '%' . $query . '%')
                ->getQuery()->getResult()
            ;
    
            $results = [];
            if ($myEntities) {
                foreach ($myEntities as $myEntity) {
                    $results[] = [
                        'icon' => $this->getIcon(), // the icon is optional
                        'title' => $myEntity->getName(),
                        'url' => $this->router->generate('modules_my_default_entity', ['entity' => $myEntity->getId()])
                    ];
                }
            }
    
            return $results;
        }
    }

As you can see, you are completely free to display anything as a search result because you are just rendering some template like you do all the time e.g. in your controller responses.

For the autocomplete results it looks slightly different. Here you must provide an array of arrays with certain keys, which are `icon`, `title` and `url`. This is to ensure a certain structure for the autocompleted results as there is not that much room to display everything you might want.

Concerning how and what kind of results you want to return you are completely free.
 
Now just add a service for each of your search item classes and tag them with the `app.search_item` tag.

    services:
        # ...
        
        mod.my.search.myentity:
            class: Modules\MyBundle\Search\MyEntity
            arguments: ['@doctrine.orm.entity_manager', '@templating', '@router']
            tags:
                - {name: app.search_item}

Your entitie's search item should already work now but you probably want to adjust the displayed label and the icon in the search filter settings. You can do so by adding the two methods `getName` and `getIcon`.

    <?php
    namespace Modules\MyBundle\Search;
    
    use Core\AppBundle\Search\SearchItem;
    // ...
    
    class MyEntity extends SearchItem
    {
        // ...
    
        /**
         * @return string
         */
        public function getName()
        {
            return 'My Entity';
        }
    
        /**
         * @return string
         */
        public function getIcon()
        {
            return 'star';
        }
    }

By default there is no icon and the label is simply the name of your class.

## 7. Flash Messages

Flash Messages are those little alert boxes appearing after you added or updated some entity or for whatever messages one might want.

You can add them in your controllers like always by using symfony's `addFlash` method.

    // ...
    
    if ($form->isSubmitted() && $form->isValid()) {
        $em = $this->getDoctrine()->getManager();
        $em->persist($myEntity);
        $em->flush();

        // ...

        // You can use 'info', 'success', 'warning' and 'error' as the first parameter.
        $this->addFlash('success', '<i class="uk-icon-check"></i> Entity successfully created!');

        return $this->redirectToRoute(...);
    }
    
    // ...

**Note: The html for the icon is specific for the uikit or uikit has at least to be implemented in the used theme. This is bad design and might change in the future.**

Flash Messages are grouped by the first parameter of the `addFlash` method. The groups 'info', 'success', 'warning' and 'error' are automatically displayed if you use the `pageHeader` twig macro.

    # Modules/MyBundle/Resources/themes/uikit/MyEntity/new.html.twig
    
    {# ... #}
    
    {{ macros.pageHeader('My Headline', 'star') }}
    
    {# ... #}
    
Read more about the page header and other macros in the [Macros](#) section.

## 8. Macros

The Core's App Bundle provides a set of macros which you can use in your templates.

    # Modules/MyBundle/Resources/themes/uikit/MyEntity/new.html.twig
    
    {% extends '@CoreApp/layout.html.twig' %}
    
    {% import '@CoreApp/macros.html.twig' as macros %}
    
    {{ macros.pageHeader('My Headline', 'star') }}
    
    {# ... #}

### 8.1 Page Header

The Page Header macro displays a headline with an icon, buttons, a breadcrumb and a settings dropdown for adding favorites etc. and managing tables.

    {% set breadcrumb %}
        <i class="uk-icon-star"></i> My Bundle <i class="uk-icon-angle-right"></i> My Subsection
    {% endset %}
    {% set buttons %}
        <div class="uk-button-group">
            <a href="{{ path('...') }}" class="uk-button uk-button-small uk-button-success"
               data-uk-tooltip title="Add new Entity">
                <i class="uk-icon-plus"></i>
            </a>
            <a href="{{ path('...') }}" class="uk-button uk-button-small uk-button-primary">
               ...
            </a>
        </div>
    {% endset %}
    
    {{ macros.pageHeader('My Headline', 'star', buttons, breadcrumb, 1) }}

The last parameter is a boolean or integer determining if there are table settings available. See [Table Settings](#) for more info on that.

### 8.2 Alerts

The `alerts` macro is used inside the `pageHeader` macro and it displays all current flash messages belonging to the groups 'info', 'success', 'warning' or 'error'.

### 8.3 Readable File Size

Given an integer of bytes it displays a human readable file size.

    {{ document.name }} ({{ macros.readableFileSize(document.size) }})
    # my-document.pdf (2,3 MB)

## 9. Table Settings

Tables are paginated, can be ordered and columns can be displayed or not. These settings apply for each user individually.

To provide this functionality follow these steps:

### 9.1 In your Controller

Use the KnpPaginator to create a paginated result set and configure your column defaults.

    /**
     * @Route("/")
     * ...
     */
    public function indexAction(Request $request)
    {
        // ...

        // collect your entities however you like,
        // but use 'sort' and 'direction' request parameters to order your entities.
        $myEntities = $this->getDoctrine()->getManager()->getRepository('ModulesMyBundle:MyEntity')->findBy(
            $criteria,
            [$request->query->get('sort', 'name') => $request->query->get('direction', 'ASC')]
        );
        
        // configure column defaults
        $columns = $this->get('app.helper')->setTableColumns('mod_my_myentity_table_columns', [
            'name' => ['active' => 1, 'label' => 'Name'],
            'categories' => ['active' => 1, 'label' => 'Categories'],
            // ...
        ]);
        
        // prepare pagination
        $perPage = $this->get('app.helper')->setTablePerPage('mod_my_myentity_table_rows');
        $paginator = $this->get('knp_paginator');
        $page = $request->query->getInt('page', 1);
        $pagination = $paginator->paginate($myEntities, $page, $perPage);

        return $this->render('ModulesMyBundle:MyEntity:index.html.twig', [
            'pagination' => $pagination,
            'columns' => $columns,
            'perPage' => $perPage
        ]);
    }

### 9.2 In your Template

    # Modules/MyBundle/Resources/themes/uikit/MyEntity/index.html.twig
    
    {% extends '@CoreApp/layout.html.twig' %}
    
    {% import '@CoreApp/macros.html.twig' as macros %}
    
    {% block content %}
        
        {# ... #}
        
        {# Render the page header with your buttons, breadcrumb and the flag for table settings set to 1. #}
        {{ macros.pageHeader('My Entities', 'star', buttons, breadcrumb, 1) }}
        
        {# Render the pagination above the table. #}
        {{ knp_pagination_render(pagination) }}
        
        {# Render your table. #}
        <div class="uk-overflow-container">
            <table class="uk-table">
                <thead>
                <tr>
                    {# Render column header only if active for the current user. #}
                    {% if columns['name'].active %}
                        <th class="uk-text-nowrap">
                            <i class="uk-icon-map-marker"></i>
                            {# Render sortable column header. #}
                            {{ knp_pagination_sortable(pagination, columns['name'].label, 'name') }}
                        </th>
                    {% endif %}
                    
                    {% if columns['categories'].active %}
                        <th class="uk-text-nowrap">
                            <i class="uk-icon-map-marker"></i>
                            {# Render sortable column header. #}
                            {{ knp_pagination_sortable(pagination, columns['categories'].label, 'categories') }}
                        </th>
                    {% endif %}
                    
                    {# ... #}
                    
                    <th></th>
                </tr>
                </thead>
                <tbody>
                {% for myEntity in pagination %}
                    <tr>
                        {% if columns['name'].active %}
                            <td class="uk-text-nowrap">
                                {{ myEntity.name }}
                            </td>
                        {% endif %}
                        {% if columns['categories'].active %}
                            <td class="uk-text-nowrap">
                                {% for category in myEntity.categories %}
                                    <a href="{{ path('...') }}">
                                        {{ category.name }}
                                    </a><br>
                                {% endfor %}
                            </td>
                        {% endif %}
                        <td class="uk-text-right">
                            <div class="uk-button-group">
                                {# your row buttons (show, edit, delete, etc.) #}
                            </div>
                        </td>
                    </tr>
                {% endfor %}
                </tbody>
            </table>
        </div>
    
        {# Add second pagination at the botton for convenience. #}
        {{ knp_pagination_render(pagination) }}

So, that's it! Your table columns can now be toggled on and of and your table is paginated and can be sorted.

**Note: Of course you have to take care of the ordering yourself in the way you collect your entities from the database. This might need a more complex method in your repository.**

## 10. Themes

Theme support is provided by the `LiipThemeBundle` which implements a new template file locating hierarchy.

This hierarchy is configured in `app/config/config.yml`: 

    # ...
    
    # LiipTheme
    liip_theme:
        themes: ['metronic', 'uikit'] # There are two possible themes at the time.
        active_theme: %theme% # The active theme is configured in app/config/parameters.yml
        # ...
        path_patterns:
            app_resource: # global templates and bundle overrides
              - %%app_path%%/themes/%%current_theme%%_%%current_device%%/%%template%% # First look for theme and device specific templates..
              - %%app_path%%/themes/%%current_theme%%/%%template%% # ...then look for theme specific templates..
              - %%app_path%%/views/%%template%% # ...then look for templates used by all themes.
            bundle_resource: # bundles templates
              - %%bundle_path%%/Resources/themes/%%current_theme%%_%%current_device%%/%%template%%
              - %%bundle_path%%/Resources/themes/%%current_theme%%/%%template%%
              - %%bundle_path%%/Resources/views/%%template%%

This basically leads to the following logic. If you put templates in `MyBundle/Resources/views` these templates can be used by all themes.
If you put templates in `MyBundle/Resources/themes/uikit` these templates will be used definitly and only by the uikit theme (configured with `active_theme: %theme%`).

Referencing templates in your controllers works the same as always.

## 11. Permissions

Permissions are simply roles of users and groups, where users can have individual roles or inherit them from their groups BUT (!) not both at the same time!

The available roles/permissions are defined by entities annotated with the `@Acl` annotation.

    <?php
    
    namespace Modules\MyBundle\MyEntity;
    
    use Core\AppBundle\Annotation\Acl;
    use Doctrine\ORM\Mapping as ORM;
    // ...
    
    /**
     * MyEntity
     *
     * @Acl("My Entity")
     * @ORM\Table(name="mod_my_myentity")
     * @ORM\Entity(repositoryClass="Modules\MyBundle\Repository\MyEntityRepository")
     */
    class MyEntity
    {
        // ...

You must provide a label as the only parameter, for displaying the entity permissions in the permissions table in the user and group form.

Each entity has four permissions by default: `VIEW`, `CREATE`, `EDIT` and `DELETE`

These permissions are represented by the following roles:

    ROLE_PERM_VIEW_MODULES_MYBUNDLE_ENTITY_MYENTITY
    ROLE_PERM_CREATE_MODULES_MYBUNDLE_ENTITY_MYENTITY
    ROLE_PERM_EDIT_MODULES_MYBUNDLE_ENTITY_MYENTITY
    ROLE_PERM_DELETE_MODULES_MYBUNDLE_ENTITY_MYENTITY

The schema behind this is: `ROLE_PERM_{PERMISSION}_{ENTITY_NAMESPACE}_{ENTITY_CLASSNAME}`

Checking permissions can be done either with symfony's `isGranted` method on a specific role or with the `can` shortcut method from the `AppHelper` service.

In controller:

    if ($this->isGranted('ROLE_PERM_EDIT_MODULES_MYBUNDLE_ENTITY_MYENTITY')) { ...
    // or
    if ($this->get('app_helper')->can($user, 'EDIT', MyEntity::class)) { ...

The `can` method takes a user or group object as the first argument. So checking for specific users or groups is possible. Symfony's `isGranted` method can't do that.

## 12. Api

Extending the Yono api with your own endpoints is easy. In your bundles `Controller` directory create a subdirectory called `Api`. There you can add your api controllers.

Then you have to add the routing configuration to `app/config/routing.yml`:

    # Modules
    
    # ...
    
    modules_mybundle_api:
        resource: "@ModulesMyBundleBundle/Controller/Api/"
        type:     rest
        prefix:   /api/

Your api controllers must extend `FOSRestController`.

    <?php
    
    namespace Modules\MyBundleBundle\Controller\Api;
    
    use Modules\MyBundle\Entity\MyEntity;
    use FOS\RestBundle\Controller\Annotations\View;
    use FOS\RestBundle\Controller\FOSRestController;
    use Nelmio\ApiDocBundle\Annotation\ApiDoc;
    use Symfony\Component\HttpFoundation\Response;
    
    class MyEntityController extends FOSRestController
    {
        // ...
    }

The yono api is based on the FOSRestBundle. [See its documentation](http://symfony.com/doc/current/bundles/FOSRestBundle/) for further information.

## 13. Relations (experimental!)

If you want to add relationships between entities of different types, without hardcoding the relationship in your entity classes, then you can use the cores relation entity as an intermediary.

Let your entity's repository extend the relation repository:

    <?php
    
    namespace Modules\MyBundle\Repository;
    
    use Core\AppBundle\Repository\RelationRepository;
    use Modules\MyBundle\Entity\MyEntity;
    
    class MyEntityRepository extends RelationRepository
    {
        // ...
    }

Now you can use its method to retrieve related entities.

    public function getMyEntitiesFor($otherEntity)
    {
        return $this->findRelation($otherEntity, 'owns', MyEntity::class, null, new \DateTime('2016-08-16'), new \DateTime('2016-08-20'));
    }

Todo: adding relations

# 14. Document Conversion

In Yono you can work with several Document types such as docx, doc, odt and so on. You are also able to use them as templates
and fill them out programatically. 

    $tbs = $this->get('app.opentbs');
    $tbs->loadTemplate("template.odt");
    $tbs->MergeField('client',['name'=>$this->getUser()->getUsername()]);
    $tbs->Show(OPENTBS_FILE, 'output.odt');

Placeholders in templates must look like `[namespace.variable]`. For example: `[client.name]`.

After you have generated the document, simply use our service to convert the document to other formats:
 
    $converter = $this->get('app.documentconverter');
    $converter->convertTo('input.odt', 'output.pdf', 'pdf');

We use unoconv for document conversion. See [their homepage](http://dag.wiee.rs/home-made/unoconv/) for availabe document formats.

# 15. Cron Jobs

We use the creadev/cron-bundle to handle Cronjobs. There are three easy steps to create cronjobs in yono.

1) Create a Command and add the @CronJob Annotation to its' PHPDoc.
2) Run php bin/console cron:scan to regognize yono the new Cronjob
3) And finally run php bin/console cron:run to execute all cronjobs.

### Example:

    <?php

    namespace Core\AppBundle\Command;
    
    use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
    use Symfony\Component\Console\Input\InputInterface;
    use Symfony\Component\Console\Output\OutputInterface;
    use ColourStream\Bundle\CronBundle\Annotation\CronJob;
    
    /**
     * Class TestCommand
     * @package Core\AppBundle\Command
     * @CronJob("PT5M")
     */
    class TestCommand extends ContainerAwareCommand
    {
        public function configure()
        {
            $this->setName('yono:test')->setDescription("");
        }
    
        public function execute(InputInterface $input, OutputInterface $output)
        {
            $output->writeln("RUN!");
    
            /*
             * Return: 0 - Success
             * Return: 1 - Failed
             */
            return 0;
        }
    }

This Command will run every 5 minutes.

(PT5M = period time 5 minutes,
P5M = period 5 months)

The CronJob Annotation takes a period of time as its first argument. Check out the php docs for more information about 
time intervals: [DateInterval::__construct](http://au.php.net/manual/en/dateinterval.construct.php)

This bundle is designed around the idear that your tasks will be run with a minimum interval - the tasks will be 
run no more frequently than you schedule them, but they can only run when you trigger then (by running app/console 
cron:run, or the forthcoming web endpoint, for use with webcron services).

To facilitate this, you can create a cron job on your system like this:

*/ * * * * /path/to/yono/install/bin/console cron:run

This will schedule your tasks to run at most every minute.

# Contributing

Just create a fork and send pull requests. You can also let gitlab do the mirroring with the main repo by following this guide: [How to Keep your Fork Up-To-Date with its Origin](https://about.gitlab.com/2016/12/01/how-to-keep-your-fork-up-to-date-with-its-origin)
