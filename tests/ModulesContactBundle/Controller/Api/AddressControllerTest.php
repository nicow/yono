<?php

namespace Tests\Modules\ContactBundle\Controller\Api;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints as Assert;

class AddressControllerTest extends WebTestCase
{
    /**
     * Test create, read, update, delete Address.
     *
     * TODO: Implement OAuth
     * TODO: Test linking (LINK)
     * TODO: Test validation
     * TODO: Test bad requests
     */
    public function testAddressCRUD()
    {
        $client = self::createClient();
        $accessToken = $client->getContainer()->get('app.oauth')->getAccessToken();
        $accessTokenHeader = 'Bearer ' . $accessToken;

        // generate unique random name for this test run
        $testAddressName = 'Test-Adresse ' . time();

        // create address
        $client->request(
            'POST',
            '/api/addresses',
            [],
            [],
            [
                'CONTENT_TYPE' => 'application/json',
                'HTTP_AUTHORIZATION' => $accessTokenHeader
            ],
            '{"address":{"name":"' . $testAddressName . '"}}'
        );

        // check status code
        $this->assertEquals(Response::HTTP_CREATED, $client->getResponse()->getStatusCode());

        // check content type
        $this->assertTrue($client->getResponse()->headers->contains('Content-Type', 'application/json'));

        // check location
        $this->assertTrue($client->getResponse()->headers->has('Location'));
        $testAddressLocation = $client->getResponse()->headers->get('Location');

        // get address
        $client->request(
            'GET',
            $testAddressLocation,
            [],
            [],
            [
                'HTTP_AUTHORIZATION' => $accessTokenHeader
            ]
        );

        // check status code
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());

        // check for created test address name
        $this->assertContains($testAddressName, $client->getResponse()->getContent());

        // get all addresses
        $client->request(
            'GET',
            '/api/addresses',
            [],
            [],
            [
                'HTTP_AUTHORIZATION' => $accessTokenHeader
            ]
        );

        // check status code
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());

        // check for created test address name
        $this->assertContains($testAddressName, $client->getResponse()->getContent());

        // update address
        $client->request(
            'PUT',
            $testAddressLocation,
            [],
            [],
            [
                'CONTENT_TYPE' => 'application/json',
                'HTTP_AUTHORIZATION' => $accessTokenHeader
            ],
            '{"address":{"name":"' . $testAddressName . ' updated"}}'
        );

        // check status code
        $this->assertEquals(Response::HTTP_NO_CONTENT, $client->getResponse()->getStatusCode());

        // get updated address
        $client->request(
            'GET',
            $testAddressLocation,
            [],
            [],
            [
                'HTTP_AUTHORIZATION' => $accessTokenHeader
            ]
        );

        // check for updated test address name
        $this->assertContains($testAddressName . ' updated', $client->getResponse()->getContent());

        // fail validation (by sending empty address name)
        $client->request(
            'PUT',
            $testAddressLocation,
            [],
            [],
            [
                'CONTENT_TYPE' => 'application/json',
                'HTTP_AUTHORIZATION' => $accessTokenHeader
            ],
            '{"address":{"name":""}}'
        );

        // check status code
        $this->assertEquals(Response::HTTP_BAD_REQUEST, $client->getResponse()->getStatusCode());

        // check for validation failed messages
        $this->assertContains('"name":{"errors"', $client->getResponse()->getContent());

        // delete test address
        $client->request(
            'DELETE',
            $testAddressLocation,
            [],
            [],
            [
                'HTTP_AUTHORIZATION' => $accessTokenHeader
            ]
        );

        // check status code
        $this->assertEquals(Response::HTTP_NO_CONTENT, $client->getResponse()->getStatusCode());
    }
}
