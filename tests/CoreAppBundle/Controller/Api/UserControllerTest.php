<?php

namespace Tests\Core\AppBundle\Controller\Api;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints as Assert;

class UserControllerTest extends WebTestCase
{
    /**
     * Test create, read, update, delete User.
     *
     * TODO: Implement OAuth
     * TODO: Test linking (LINK)
     * TODO: Test validation
     * TODO: Test bad requests
     */
    public function testUserCRUD()
    {
        $client = self::createClient();
        $accessToken = $client->getContainer()->get('app.oauth')->getAccessToken();
        $accessTokenHeader = 'Bearer ' . $accessToken;

        // generate unique random name for this test run
        $testUserName = 'Test-User-' . time();
        $testUserEmail = $testUserName . '@example.org';

        // test user data
        $testUserData = json_encode(['user' => [
            'username' => $testUserName,
            'email' => $testUserEmail,
            'plain_password' => ['first' => '1234', 'second' => '1234']
        ]]);

        // create user
        $client->request(
            'POST',
            '/api/users',
            [],
            [],
            [
                'CONTENT_TYPE' => 'application/json',
                'HTTP_AUTHORIZATION' => $accessTokenHeader
            ],
            $testUserData
        );

        // check status code
        $this->assertEquals(Response::HTTP_CREATED, $client->getResponse()->getStatusCode());

        // check content type
        $this->assertTrue($client->getResponse()->headers->contains('Content-Type', 'application/json'));

        // check location
        $this->assertTrue($client->getResponse()->headers->has('Location'));
        $testUserLocation = $client->getResponse()->headers->get('Location');

        // get user
        $client->request(
            'GET',
            $testUserLocation,
            [],
            [],
            [
                'HTTP_AUTHORIZATION' => $accessTokenHeader
            ]
        );

        // check status code
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());

        // check for created test user name
        $this->assertContains($testUserName, $client->getResponse()->getContent());

        // get all users
        $client->request(
            'GET',
            '/api/users',
            [],
            [],
            [
                'HTTP_AUTHORIZATION' => $accessTokenHeader
            ]
        );

        // check status code
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());

        // check for created test user name
        $this->assertContains($testUserName, $client->getResponse()->getContent());

        // update user
        $client->request(
            'PUT',
            $testUserLocation,
            [],
            [],
            [
                'CONTENT_TYPE' => 'application/json',
                'HTTP_AUTHORIZATION' => $accessTokenHeader
            ],
            '{"user":{"username":"' . $testUserName . ' updated", "email": "updated-' . $testUserEmail . '"}}'
        );

        // check status code
        $this->assertEquals(Response::HTTP_NO_CONTENT, $client->getResponse()->getStatusCode());

        // get updated user
        $client->request(
            'GET',
            $testUserLocation,
            [],
            [],
            [
                'HTTP_AUTHORIZATION' => $accessTokenHeader
            ]
        );

        // check for updated test user name
        $this->assertContains($testUserName . ' updated', $client->getResponse()->getContent());

        // fail validation (by sending invalid email)
        $client->request(
            'PUT',
            $testUserLocation,
            [],
            [],
            [
                'CONTENT_TYPE' => 'application/json',
                'HTTP_AUTHORIZATION' => $accessTokenHeader
            ],
            '{"user":{"username":"' . $testUserName . '", "email": "asd"}}'
        );

        // check status code
        $this->assertEquals(Response::HTTP_BAD_REQUEST, $client->getResponse()->getStatusCode());

        // check for validation failed messages
        $this->assertContains('"email":{"errors"', $client->getResponse()->getContent());

        // delete test user
        $client->request(
            'DELETE',
            $testUserLocation,
            [],
            [],
            [
                'HTTP_AUTHORIZATION' => $accessTokenHeader
            ]
        );

        // check status code
        $this->assertEquals(Response::HTTP_NO_CONTENT, $client->getResponse()->getStatusCode());
    }
}
