<?php

namespace Tests\Core\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    public function testHome()
    {
        $client = $this->getClient();

        $crawler = $client->request('GET', '/');

        $homeUrl = $client->getContainer()->get('app.helper')->getUserHomeUrl();

        $this->assertEquals(302, $client->getResponse()->getStatusCode());
        $this->assertContains('Redirecting to ' . $homeUrl, $crawler->filter('title')->text());
    }

    public function testDashboard()
    {
        $client = $this->getClient();

        $crawler = $client->request('GET', '/dashboard');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('Welcome!', $crawler->filter('title')->text());
    }

    private function getClient()
    {
        return static::createClient([], [
            'PHP_AUTH_USER' => 'admin',
            'PHP_AUTH_PW'   => 'admin',
        ]);
    }
}
