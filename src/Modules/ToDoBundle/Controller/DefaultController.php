<?php

namespace Modules\ToDoBundle\Controller;

use Core\AppBundle\Entity\User;
use Modules\ToDoBundle\Entity\ToDoArchiv;
use Modules\ToDoBundle\Entity\ToDoNote;
use Modules\ToDoBundle\Form\ToDoType;
use Modules\ToDoBundle\Entity\ToDo;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class DefaultController extends Controller
{
    /**
     * Lists all my todos entities.
     * @Route("/")
     * @Method({"GET", "POST"})
     * @Security("is_granted('VIEW', 'Modules\\ToDoBundle\\Entity\\ToDo')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {

        if ($request->query->all()) {
            $this->get('app.settings')->set('mod_todo_index_overview', $request->query->all());
        }

        $filter = $this->get('app.settings')->get('mod_todo_index_overview');

        $todos = $this->getDoctrine()->getManager()->getRepository('ModulesToDoBundle:ToDo')->findByUser($request->query->get('sort', 'id'), $request->query->get('direction', 'DESC'), $this->get('security.token_storage')->getToken()->getUser(), $filter);

        $user = $this->get('security.token_storage')->getToken()->getUser();
        $archiv = $this->get('todo.helper')->getArchiv($user);
        $counter = 0;
        if (isset($filter['status'])) {
            if ($filter['status'] != 4) {
                foreach ($todos as $todo) {
                    if ($this->get('todo.helper')->isArchiv($todo, $archiv)) {
                        unset($todos[$counter]);
                    }
                    $counter++;
                }
            } else {
                foreach ($todos as $todo) {
                    if (!$this->get('todo.helper')->isArchiv($todo, $archiv)) {
                        unset($todos[$counter]);
                    }
                    $counter++;
                }
            }
        } else {
            foreach ($todos as $todo) {
                if ($this->get('todo.helper')->isArchiv($todo, $archiv)) {
                    unset($todos[$counter]);
                }
                $counter++;
            }
        }


        $columns = $this->get('app.helper')->setTableColumns('mod_todo_default_table_columns', ['title' => ['active' => 1,
            'label' => 'Titel'],
            'priority' => ['active' => 1, 'label' => 'Priorität'],
            'status' => ['active' => 1, 'label' => 'Status'],
            'contact' => ['active' => 1, 'label' => 'Kontakt'],
            'createdBy' => ['active' => 0, 'label' => 'erstellt von'],
            'createdAt' => ['active' => 0, 'label' => 'erstellt am'],
            'updatedAt' => ['active' => 0, 'label' => 'letzte Änderung'],]);

        $perPage = $this->get('app.helper')->setTablePerPage('mod_todo_default_table_rows');
        $paginator = $this->get('knp_paginator');
        $page = $request->query->getInt('page', 1);
        $pagination = $paginator->paginate($todos, $page, $perPage);

        $deleteForms = [];
        foreach ($todos as $todo) {
            $deleteForms[$todo->getId()] = $this->createDeleteForm($todo)->createView();
        }

        $members = $this->getDoctrine()->getManager()->getRepository('ModulesToDoBundle:ToDo')->findAllMembers();

        return $this->render('ModulesToDoBundle:Default:index.html.twig', ['columns' => $columns,
            'pagination' => $pagination,
            'perPage' => $perPage,
            'filterSettings' => $filter,
            'members' => $members,
            'deleteForms' => $deleteForms,]);
    }

    /**
     * Creates a form to delete a product entity.
     * @param ToDo $todo
     * @return Form The form
     */
    private function createDeleteForm(ToDo $todo)
    {
        return $this->createFormBuilder()->setAction($this->generateUrl('modules_todo_default_delete', ['todo' => $todo->getId()]))->setMethod('DELETE')->getForm();
    }

    /**
     * Lists all todos entities.
     * @Route("/all/{member}")
     * @Method({"GET", "POST"})
     * @Security("is_granted('DELETE', 'Modules\\ToDoBundle\\Entity\\ToDo')")
     * @param Request $request
     * @param $member
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function allAction(Request $request, $member = null)
    {

        if ($request->query->all()) {
            $this->get('app.settings')->set('mod_todo_all_overview', $request->query->all());
        }

        $filter = $this->get('app.settings')->get('mod_todo_all_overview');

        $todos = $this->getDoctrine()->getManager()->getRepository('ModulesToDoBundle:ToDo')->findAllSortBy($request->query->get('sort', 'id'), $request->query->get('direction', 'DESC'), $filter);

        $user = $this->get('security.token_storage')->getToken()->getUser();
        $archiv = $this->get('todo.helper')->getArchiv($user);
        $counter = 0;
        if (isset($filter['status'])) {
            if ($filter['status'] != 4) {
                foreach ($todos as $todo) {
                    if ($this->get('todo.helper')->isArchiv($todo, $archiv)) {
                        unset($todos[$counter]);
                    }
                    $counter++;
                }
            } else {
                foreach ($todos as $todo) {
                    if (!$this->get('todo.helper')->isArchiv($todo, $archiv)) {
                        unset($todos[$counter]);
                    }
                    $counter++;
                }
            }
        } else {
            foreach ($todos as $todo) {
                if ($this->get('todo.helper')->isArchiv($todo, $archiv)) {
                    unset($todos[$counter]);
                }
                $counter++;
            }
        }


        $columns = $this->get('app.helper')->setTableColumns('mod_todo_all_table_columns', ['title' => ['active' => 1,
            'label' => 'Titel'],
            'members' => ['active' => 1, 'label' => 'Mitarbeiter'],
            'priority' => ['active' => 1, 'label' => 'Priorität'],
            'status' => ['active' => 1, 'label' => 'Status'],
            'contact' => ['active' => 1, 'label' => 'Kontakt'],
            'createdBy' => ['active' => 1, 'label' => 'erstellt von'],
            'createdAt' => ['active' => 0, 'label' => 'erstellt am'],
            'updatedAt' => ['active' => 0, 'label' => 'letzte Änderung'],]);

        $perPage = $this->get('app.helper')->setTablePerPage('mod_todo_all_table_rows');
        $paginator = $this->get('knp_paginator');
        $page = $request->query->getInt('page', 1);
        $pagination = $paginator->paginate($todos, $page, $perPage);

        $deleteForms = [];
        foreach ($todos as $todo) {
            $deleteForms[$todo->getId()] = $this->createDeleteForm($todo)->createView();
        }

        $members = $this->getDoctrine()->getManager()->getRepository('ModulesToDoBundle:ToDo')->findAllMembers();

        return $this->render('ModulesToDoBundle:Default:all.html.twig', ['columns' => $columns,
            'pagination' => $pagination,
            'perPage' => $perPage,
            'members' => $members,
            'filterSettings' => $filter,
            'deleteForms' => $deleteForms,]);
    }

    /**
     * Lists all delegated todos entities.
     * @Route("/delegate")
     * @Method({"GET", "POST"})
     * @Security("is_granted('VIEW', 'Modules\\ToDoBundle\\Entity\\ToDo')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function delegateAction(Request $request)
    {

        if ($request->query->all()) {
            $this->get('app.settings')->set('mod_todo_delegated_overview', $request->query->all());
        }

        $filter = $this->get('app.settings')->get('mod_todo_delegated_overview');

        $todos = $this->getDoctrine()->getManager()->getRepository('ModulesToDoBundle:ToDo')->findByUserCreated($request->query->get('sort', 'id'), $request->query->get('direction', 'DESC'), $this->get('security.token_storage')->getToken()->getUser(), $filter);

        $user = $this->get('security.token_storage')->getToken()->getUser();
        $archiv = $this->get('todo.helper')->getArchiv($user);
        $counter = 0;
        if (isset($filter['status'])) {
            if ($filter['status'] != 4) {
                foreach ($todos as $todo) {
                    if ($this->get('todo.helper')->isArchiv($todo, $archiv)) {
                        unset($todos[$counter]);
                    }
                    $counter++;
                }
            } else {
                foreach ($todos as $todo) {
                    if (!$this->get('todo.helper')->isArchiv($todo, $archiv)) {
                        unset($todos[$counter]);
                    }
                    $counter++;
                }
            }
        } else {
            foreach ($todos as $todo) {
                if ($this->get('todo.helper')->isArchiv($todo, $archiv)) {
                    unset($todos[$counter]);
                }
                $counter++;
            }
        }

        $columns = $this->get('app.helper')->setTableColumns('mod_todo_delegated_table_columns', ['title' => ['active' => 1,
            'label' => 'Titel'],
            'members' => ['active' => 1, 'label' => 'Mitarbeiter'],
            'priority' => ['active' => 1, 'label' => 'Priorität'],
            'status' => ['active' => 1, 'label' => 'Status'],
            'contact' => ['active' => 1, 'label' => 'Kontakt'],
            'createdBy' => ['active' => 1, 'label' => 'erstellt von'],
            'createdAt' => ['active' => 0, 'label' => 'erstellt am'],
            'updatedAt' => ['active' => 0, 'label' => 'letzte Änderung'],]);

        $perPage = $this->get('app.helper')->setTablePerPage('mod_todo_delegated_table_rows');
        $paginator = $this->get('knp_paginator');
        $page = $request->query->getInt('page', 1);
        $pagination = $paginator->paginate($todos, $page, $perPage);

        $deleteForms = [];
        foreach ($todos as $todo) {
            $deleteForms[$todo->getId()] = $this->createDeleteForm($todo)->createView();
        }

        $members = $this->getDoctrine()->getManager()->getRepository('ModulesToDoBundle:ToDo')->findAllMembers();

        return $this->render('ModulesToDoBundle:Default:delegate.html.twig', ['columns' => $columns,
            'pagination' => $pagination,
            'filterSettings' => $filter,
            'perPage' => $perPage,
            'members' => $members,
            'deleteForms' => $deleteForms,]);
    }

    /**
     * Creates a new ToDo entity.
     * @Route("/new", defaults={"order": null})
     * @Method({"GET", "POST"})
     * @Security("is_granted('CREATE', 'Modules\\ToDoBundle\\Entity\\ToDo')")
     * @param Request $request
     * @param $todos
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $todo = new ToDo();

        $contacts = null;

        /**
         * @var User $user
         */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if ($user->getContact() && $user->getGroups() && $user->hasRole('ROLE_PERM_EDIT_MODULES_TODOBUNDLE_ENTITY_TODO') && (!$user->getMember() || !$user->hasRole('ROLE_PERM_DELETE_MODULES_TODOBUNDLE_ENTITY_TODO'))) {

            $users = [];
            foreach ($user->getGroups() as $group) {

                $groupUser = false;
                foreach ($group->getRoles() as $role) {
                    if ('ROLE_PERM_VIEW_MODULES_TODOBUNDLE_ENTITY_TODO' == $role) {
                        $groupUser = true;
                    }
                }
                if ($groupUser) $users[] = $em->getRepository('CoreAppBundle:User')->findByGroup($group);
            }
            if ($users) {
                $contacts = $em->getRepository('ModulesContactBundle:Contact')->findByUsers($users);
                $todo->setContact($user->getContact());
            } elseif ($user->getContact()) {
                $contacts[] = $user->getContact();
                $todo->setContact($user->getContact());
            }
        } elseif ($user->getContact() && !$user->getMember()) {
            $contacts[] = $user->getContact();
            $todo->setContact($user->getContact());
        } else {
            $contacts = $em->getRepository('ModulesContactBundle:Contact')->findAll();
        }

        $form = $this->createForm(ToDoType::class, $todo, ['contacts' => $contacts]);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {

            $em->persist($todo);
            $em->flush();

            $mailer = $this->get('mailer');

            $mail = (new \Swift_Message('Neues ToDo - ' . $todo->getTitle()));
            $prefix = $this->getParameter('schema') . '://' . $this->getParameter('host');
            $mail->setFrom($this->getParameter('mailer_from_address'), $this->getParameter('mailer_from_name'));
            $mail->setBody($this->renderView('ModulesToDoBundle:Default:mail.html.twig', array('todo' => $todo,
                'prefix' => $prefix)), 'text/html');

            $mailTo = false;

            if ($todo->getMembers()) {
                foreach ($todo->getMembers() as $member) {
                    if ($member->getEmail()) {
                        $mail->addTo($member->getEmail());
                        $mailTo = true;
                    }
                }
            }
            if ($todo->getContact() && !$todo->getCreatedBy()->getContact()) {

                if ($todo->getContact()->getEmail()) {
                    $mail->addTo($todo->getContact()->getEmail());
                    $mailTo = true;
                }

            }
            if ($mailTo == false) {
                $mail->addTo($this->getParameter('modules_ordermanagement_accounting_mail'));
            }
            $mail->addBcc('tim.strakerjahn@crea.de');
            $mailer->send($mail);

            $this->addFlash('success', '<i class="uk-icon-check"></i> Das To Do wurde erfolgreich angelegt!');

            return $this->redirectToRoute('modules_todo_default_delegate');
        }

        return $this->render('ModulesToDoBundle:Default:new.html.twig', ['todo' => $todo,
            'form' => $form->createView(),]);
    }

    /**
     * @Route("/{todo}/details/{delegate}")
     * @Security("is_granted('VIEW', 'Modules\\ToDoBundle\\Entity\\ToDo')")
     * @param ToDo $todo
     * @param $delegate
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function detailsAction(Request $request, ToDo $todo, $delegate = null)
    {

        $notes = $this->getDoctrine()->getManager()->getRepository('ModulesToDoBundle:ToDoNote')->findBy(['todo' => $todo]);

        $columns = $this->get('app.helper')->setTableColumns('mod_todo_todoNote_table_columns', ['createdBy' => ['active' => 1,
            'label' => 'Ersteller'],
            'text' => ['active' => 1, 'label' => 'text'],
            'createdAt' => ['active' => 1, 'label' => 'Datum'],
            'status' => ['active' => 1, 'label' => 'Status'],]);

        $perPage = $this->get('app.helper')->setTablePerPage('mod_todo_todoNote_table_rows');
        $paginator = $this->get('knp_paginator');
        $page = $request->query->getInt('page', 1);
        $pagination = $paginator->paginate($notes, $page, $perPage);

        $em = $this->getDoctrine()->getManager();
        $todo->setLastseen(new \DateTime(date('Y-m-d H:i:s')));
        $em->flush();

        return $this->render('ModulesToDoBundle:Default:details.html.twig', ['todo' => $todo,
            'delegate' => $delegate,
            'columns' => $columns,
            'pagination' => $pagination,
            'perPage' => $perPage]);
    }

    /**
     * Edits a todo entity.
     * @Route("/{todo}/edit/{delegate}")
     * @Method({"GET", "POST"})
     * @Security("is_granted('EDIT', 'Modules\\ToDoBundle\\Entity\\ToDo')")
     * @param Request $request
     * @param ToDo $todo
     * @return RedirectResponse|Response
     */
    public function editAction(Request $request, ToDo $todo, $delegate = 0)
    {
        $form = $this->createForm(ToDoType::class, $todo);
        $form->handleRequest($request);

        $em = $this->getDoctrine()->getManager();
        $todo->setLastseen(new \DateTime(date('Y-m-d H:i:s')));
        $em->flush();

        if ($form->isValid() && $form->isSubmitted()) {
            $em = $this->getDoctrine()->getManager();

            $em->flush();

            $mailer = $this->get('mailer');

            $mail = (new \Swift_Message('ToDo - ' . $todo->getTitle() . ' wurde aktualisiert'));
            $prefix = $this->getParameter('schema') . '://' . $this->getParameter('host');
            $mail->setFrom($this->getParameter('mailer_from_address'), $this->getParameter('mailer_from_name'));
            $mail->setBody($this->renderView('ModulesToDoBundle:Default:mailEdit.html.twig', array('todo' => $todo,
                'prefix' => $prefix)), 'text/html');

            $mailTo = false;

            if ($todo->getMembers()) {
                foreach ($todo->getMembers() as $member) {
                    if ($member->getEmail()) {
                        $mail->addTo($member->getEmail());
                        $mailTo = true;
                    }
                }
            }
            if ($todo->getContact() && !$todo->getUpdatedBy()->getContact() && $todo->getCreatedBy()->getContact()) {

                if ($todo->getContact()->getEmail()) {
                    $mail->addTo($todo->getContact()->getEmail());
                    $mailTo = true;
                }

            }
            if ($mailTo == false) {
                $mail->addTo($this->getParameter('modules_ordermanagement_accounting_mail'));
            }
            $mail->addBcc('tim.strakerjahn@crea.de');
            $mailer->send($mail);

            $this->addFlash('success', '<i class="uk-icon-check"></i> Das To Do wurde erfolgreich aktualisiert!');

            return new RedirectResponse($this->generateUrl('modules_todo_default_details', ['todo' => $todo->getId(),
                'delegate' => $delegate]));
        }

        return $this->render('ModulesToDoBundle:Default:edit.html.twig', ['todo' => $todo,
            'form' => $form->createView()]);
    }

    /**
     * Edits a todo status.
     * @Route("/{todo}/status/{status}/{index}")
     * @Method({"GET", "POST"})
     * @Security("is_granted('VIEW', 'Modules\\ToDoBundle\\Entity\\ToDo')")
     * @param $status
     * @param ToDo $todo
     * @return RedirectResponse|Response
     */
    public function changeStatusAction($status, ToDo $todo, $index = false)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->get('security.token_storage')->getToken()->getUser();
        $checkArchiv = $em->getRepository('ModulesToDoBundle:ToDoArchiv')->findBy(['user' => $user, 'todo' => $todo]);
        if ($status == 4) {
            if (!$checkArchiv) {
                $archiv = new ToDoArchiv();
                $archiv->setTodo($todo);
                $archiv->setUser($user);
                $em->persist($archiv);
                $em->flush();
            }
        } else {
            if ($checkArchiv) {
                $em->remove($checkArchiv);
            }
            $todo->setStatus($status);
            $todo->setLastseen(new \DateTime(date('Y-m-d H:i:s')));
        }


        $em->flush();

        $this->addFlash('success', '<i class="uk-icon-check"></i> Der Status des To Dos wurde erfolgreich geändert!');
        if ($index) {
            if ($index == 2) {
                return new RedirectResponse($this->generateUrl('modules_todo_default_delegate'));
            } elseif ($index == 3) {
                return new RedirectResponse($this->generateUrl('modules_todo_default_all'));
            } else {
                return new RedirectResponse($this->generateUrl('modules_todo_default_index'));
            }
        } else {
            return new RedirectResponse($this->generateUrl('modules_todo_default_details', ['todo' => $todo->getId()]));
        }

    }

    /**
     * Edits a todo jobticket.
     * @Route("/{todo}/ticket")
     * @Method({"GET", "POST"})
     * @Security("is_granted('EDIT', 'Modules\\ToDoBundle\\Entity\\ToDo')")
     * @param ToDo $todo
     * @return RedirectResponse|Response
     */
    public function todoJobAction(Request $request, ToDo $todo)
    {
        $em = $this->getDoctrine()->getManager();

        if ($request->request->get('jobticket') == '0') {
            return new RedirectResponse($this->generateUrl('modules_ordermanagement_jobticket_new', ['order' => 0,
                'todo' => $todo->getId()]));
        } else {
            $ticket = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:JobTicket')->find($request->request->get('jobticket'));

            if ($ticket) $todo->setJobticket($ticket);
        }


        $em->flush();

        $this->addFlash('success', '<i class="uk-icon-check"></i> Dem To Dos wurde erfolgreich ein Jobticket zugewiesen!');

        return new RedirectResponse($this->generateUrl('modules_todo_default_details', ['todo' => $todo->getId()]));

    }

    /**
     * Deletes a todo entity.
     * @Route("/{todo}/delete")
     * @Method({"GET", "DELETE"})
     * @Security("is_granted('DELETE', 'Modules\\ToDoBundle\\Entity\\ToDo')")
     * @param Request $request
     * @param ToDo $todo
     * @return RedirectResponse
     */
    public function deleteAction(Request $request, ToDo $todo)
    {
        $form = $this->createDeleteForm($todo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $query = $em->createQuery("DELETE FROM ModulesToDoBundle:ToDoNote as t WHERE t.todo = '" . $todo->getId() . "'");
            $query->execute();

            $em->remove($todo);
            $em->flush();

            $this->addFlash('success', '<i class="uk-icon-check"></i> Das To Do wurde erfolgreich gelöscht!');
        }

        if ($request->request->get('routing') == 1) {
            return $this->redirectToRoute('modules_todo_default_delegate');
        } elseif ($request->request->get('routing') == 2) {
            return $this->redirectToRoute('modules_todo_default_all');
        } else {
            return $this->redirectToRoute('modules_todo_default_index');
        }

    }

    /**
     * Creates a new ToDoNote entity
     * @Route("/{todo}/note")
     * @Method({"GET", "POST"})
     * @Security("is_granted('VIEW', 'Modules\\ToDoBundle\\Entity\\ToDo')")
     * @param Request $request
     * @param ToDo $todo
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function noteAction(Request $request, ToDo $todo)
    {
        if ($request->getMethod() == 'POST') {
            $em = $this->getDoctrine()->getManager();

            $note = new ToDoNote();
            $note->setToDo($todo);
            $note->setText($request->request->get('text'));
            $em->persist($note);
            $em->flush();

            $mailer = $this->get('mailer');

            $mail = (new \Swift_Message('Neue Notiz zum Todo - ' . $todo->getTitle()));
            $prefix = $this->getParameter('schema') . '://' . $this->getParameter('host');
            $mail->setFrom($this->getParameter('mailer_from_address'), $this->getParameter('mailer_from_name'));
            $mail->setBody($this->renderView('ModulesToDoBundle:Default:mail_note.html.twig', array('todo' => $todo,
                'note' => $note,
                'prefix' => $prefix)), 'text/html');

            if ($note->getCreatedBy()->getContact()) {

                $mail->addTo($this->getParameter('modules_ordermanagement_accounting_mail'));

            }
            $mail->addTo($this->getParameter('modules_todo_info_mail'));

            if ($todo->getContact() && !$note->getCreatedBy()->getContact() && $todo->getCreatedBy()->getContact()) {

                if ($todo->getContact()->getEmail()) {
                    $mail->addTo($todo->getContact()->getEmail());
                }

            }

            $mail->addBcc('tim.strakerjahn@crea.de');
            $mailer->send($mail);

        }

        return $this->redirectToRoute('modules_todo_default_details', ['todo' => $todo->getId()]);
    }

    /**
     * Delete a new ToDoNote entity
     * @Route("/{todoNote}/note/delete")
     * @Method({"GET", "POST"})
     * @Security("is_granted('VIEW', 'Modules\\ToDoBundle\\Entity\\ToDo')")
     * @param ToDoNote $todoNote
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function noteDeleteAction(ToDoNote $todoNote)
    {
        $todo = $todoNote->getToDo();
        $em = $this->getDoctrine()->getManager();
        $em->remove($todoNote);
        $em->flush();

        $this->addFlash('success', '<i class="uk-icon-check"></i> Die Notiz wurde erfolgreich gelöscht!');

        return $this->redirectToRoute('modules_todo_default_details', ['todo' => $todo->getId()]);
    }

}
