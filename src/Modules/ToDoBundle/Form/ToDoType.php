<?php

namespace Modules\ToDoBundle\Form;

use Doctrine\ORM\EntityManager;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class ToDoType extends AbstractType
{

    /** @var \Doctrine\ORM\EntityManager */
    private $em;


    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }


    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('title', null, ['label' => 'Titel',
            'required' => true])->add('text', CKEditorType::class, ['label' => 'Beschreibung']);

        $builder->add('contact', null, ['label' => 'Kontakt',
            'placeholder' => 'Kontakt wählen',
            'choices' => $options['contacts'],
            'required' => false])->add('text', CKEditorType::class, ['label' => 'Beschreibung']);


        $contact = false;
        if ($options['data']->getContactToDo()) {
            $contact = true;
        }

        $builder->add('contactToDo', CheckboxType::class, ['label' => 'ToDo für Kontakt','data' => $contact,
            'required' => false]);

        $builder->add('members', null, ['label' => 'Mitarbeiter',
            'choices' => $this->getMembers(),
            'required' => false,
            'constraints' => [new Assert\Count(['min' => 0,
                'minMessage' => 'Es muss mindestens ein Mitarbeiter ausgewählt werden.'])]])->add('priority', ChoiceType::class, ['label' => 'Priorität',
            'choices' => ['niedrig' => 3, 'mittel' => 2, 'hoch' => 1],
            'required' => true,]);

    }

    protected function getMembers()
    {
        $projectRepo = $this->em->getRepository('ModulesTeamBundle:Member');

        $projects = $projectRepo->findByNotHidden();

        return $projects;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => 'Modules\ToDoBundle\Entity\ToDo',
            'csrf_protection' => false,
            'contacts' => null]);
    }

}
