<?php

namespace Modules\ToDoBundle;

use Core\YonoBundleInterface;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class ModulesToDoBundle extends Bundle implements YonoBundleInterface
{

    /**
     * @return string
     */
    public function getDisplayName()
    {
        return 'ToDo';
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return '';
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'check';
    }
}
