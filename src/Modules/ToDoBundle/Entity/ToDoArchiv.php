<?php

namespace Modules\ToDoBundle\Entity;

use Core\AppBundle\Annotation\Acl;
use Core\AppBundle\Entity\BlameableTrait;
use Core\AppBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use InvalidArgumentException;
use Modules\ContactBundle\Entity\Contact;
use Modules\OrderManagementBundle\Entity\JobTicket;
use Modules\TeamBundle\Entity\Member;

/**
 * ToDoArchiv
 * @ORM\Table(name="mod_todo_todo_archiv")
 * @ORM\Entity(repositoryClass="Modules\ToDoBundle\Repository\ToDoArchivRepository")
 */
class ToDoArchiv
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\User")
     */
    public $user;

    /**
     * @var Todo
     * @ORM\ManyToOne(targetEntity="Modules\ToDoBundle\Entity\ToDo")
     */
    public $todo;
    


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param User $user
     * @return ToDo
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param ToDo $todo
     * @return ToDo
     */
    public function setTodo($todo)
    {
        $this->todo = $todo;

        return $this;
    }

    /**
     * @return ToDo
     */
    public function getTodo()
    {
        return $this->todo;
    }

}

