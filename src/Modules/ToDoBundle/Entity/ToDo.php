<?php

namespace Modules\ToDoBundle\Entity;

use Core\AppBundle\Annotation\Acl;
use Core\AppBundle\Entity\BlameableTrait;
use Doctrine\ORM\Mapping as ORM;
use InvalidArgumentException;
use Modules\ContactBundle\Entity\Contact;
use Modules\OrderManagementBundle\Entity\JobTicket;
use Modules\TeamBundle\Entity\Member;

/**
 * ToDos
 * @Acl("ToDos")
 * @ORM\Table(name="mod_todo_todo")
 * @ORM\Entity(repositoryClass="Modules\ToDoBundle\Repository\ToDoRepository")
 */
class ToDo
{

    const STATUS_OPEN       = 1;
    const STATUS_INPROGRESS = 2;
    const STATUS_DONE       = 3;
    const STATUS_ARCHIV     = 4;

    const PRIO_HIGH   = 1;
    const PRIO_MIDDLE = 2;
    const PRIO_LOW    = 3;

    use BlameableTrait;
    /**
     * @var JobTicket
     * @ORM\ManyToOne(targetEntity="Modules\OrderManagementBundle\Entity\JobTicket")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    public $jobticket;
    /**
     * @var Contact
     * @ORM\ManyToOne(targetEntity="Modules\ContactBundle\Entity\Contact")
     */
    public $contact;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=150, nullable=true)
     */
    private $title;
    /**
     * @var string
     *
     * @ORM\Column(name="text", type="text", nullable=true)
     */
    private $text;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deadline", type="date", nullable=true)
     */
    private $deadline;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="lastseen", type="datetime", nullable=true)
     */
    private $lastseen;
    /**
     * @var int
     *
     * @ORM\Column(name="repetition", type="integer", nullable=true)
     */
    private $repetition;
    /**
     * @var int
     *
     * @ORM\Column(name="priority", type="integer", nullable=true)
     */
    private $priority;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="repetitionFinished", type="date", nullable=true)
     */
    private $repetitionFinished;
    /**
     * @var Member[]
     * @ORM\ManyToMany(targetEntity="Modules\TeamBundle\Entity\Member")
     * @ORM\JoinTable(name="mod_todo_todo_members")
     */
    private $members;

    /**
     * @var int
     *
     * @ORM\Column(name="contactToDo", type="integer", nullable=true)
     */
    private $contactToDo;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return ToDo
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return ToDo
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get deadline
     *
     * @return \DateTime
     */
    public function getDeadline()
    {
        return $this->deadline;
    }

    /**
     * Set deadline
     *
     * @param \DateTime $deadline
     *
     * @return ToDo
     */
    public function setDeadline($deadline)
    {
        $this->deadline = $deadline;

        return $this;
    }

    /**
     * Get repetition
     *
     * @return int
     */
    public function getRepetition()
    {
        return $this->repetition;
    }

    /**
     * Set repetition
     *
     * @param integer $repetition
     *
     * @return ToDo
     */
    public function setRepetition($repetition)
    {
        $this->repetition = $repetition;

        return $this;
    }

    /**
     * Get priority
     *
     * @return int
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * Set priority
     *
     * @param integer $priority
     *
     * @return ToDo
     */
    public function setPriority($priority)
    {
        if (in_array($priority, [self::PRIO_HIGH, self::PRIO_LOW, self::PRIO_MIDDLE])) {
            $this->priority = $priority;

            return $this;
        }

        throw new InvalidArgumentException('Invalid todo prio.');
    }

    /**
     * Get repetitionFinished
     *
     * @return \DateTime
     */
    public function getRepetitionFinished()
    {
        return $this->repetitionFinished;
    }

    /**
     * Set repetitionFinished
     *
     * @param \DateTime $repetitionFinished
     *
     * @return ToDo
     */
    public function setRepetitionFinished($repetitionFinished)
    {
        $this->repetitionFinished = $repetitionFinished;

        return $this;
    }

    /**
     * Get status
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set staus
     *
     * @param integer $status
     *
     * @return ToDo
     */
    public function setStatus($status)
    {
        if (in_array($status, [self::STATUS_DONE, self::STATUS_ARCHIV, self::STATUS_INPROGRESS, self::STATUS_OPEN])) {
            $this->status = $status;

            return $this;
        }

        throw new InvalidArgumentException('Invalid todo status.');
    }

    /**
     * @return \Modules\TeamBundle\Entity\Member[]
     */
    public function getMembers()
    {
        return $this->members;
    }

    /**
     * @param \Modules\TeamBundle\Entity\Member[] $members
     * @return ToDo
     */
    public function setMembers($members)
    {
        $this->members = $members;

        return $this;
    }

    /**
     * @return JobTicket
     */
    public function getJobticket()
    {
        return $this->jobticket;
    }

    /**
     * @param JobTicket $jobticket
     * @return ToDo
     */
    public function setJobticket($jobticket)
    {
        $this->jobticket = $jobticket;

        return $this;
    }

    /**
     * @return Contact
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * @param Contact $contact
     * @return ToDo
     */
    public function setContact($contact)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getLastseen()
    {
        return $this->lastseen;
    }

    /**
     * @param \DateTime $lastseen
     * @return ToDo
     */
    public function setLastseen($lastseen)
    {
        $this->lastseen = $lastseen;

        return $this;
    }

    public function __toString()
    {
        return $this->title;
    }

    /**
     * @return int
     */
    public function getContactToDo()
    {
        return $this->contactToDo;
    }

    /**
     * @param int $contactToDo
     * @return ToDo
     */
    public function setContactToDo($contactToDo)
    {
        $this->contactToDo = $contactToDo;

        return $this;
    }
}

