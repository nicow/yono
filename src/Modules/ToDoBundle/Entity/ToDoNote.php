<?php

namespace Modules\ToDoBundle\Entity;

use Core\AppBundle\Entity\BlameableTrait;
use Doctrine\ORM\Mapping as ORM;
use Modules\ToDoBundle\Entity\ToDo;

/**
 * ToDONote
 * @ORM\Table(name="mod_todo_todo_note")
 * @ORM\Entity(repositoryClass="Modules\ToDoBundle\Repository\ToDoNoteRepository")
 */
class ToDoNote
{

    use BlameableTrait;

    /**
     * @var ToDo
     * @ORM\ManyToOne(targetEntity="Modules\ToDoBundle\Entity\ToDo")
     */
    public $todo;

    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */

    private $id;
    /**
     * @var string
     * @ORM\Column(name="text", type="text")
     */

    private $text;

    /**
     * Get id
     * @return int
     */

    public function getId()
    {
        return $this->id;
    }

    /**
     * Get text
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set text
     * @param string $text
     * @return ToDoNote
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * @return ToDo
     */
    public function getToDo()
    {
        return $this->todo;
    }

    /**
     * @param ToDo $todo
     * @return ToDoNote
     */
    public function setToDo($todo)
    {
        $this->todo = $todo;

        return $this;
    }
}

