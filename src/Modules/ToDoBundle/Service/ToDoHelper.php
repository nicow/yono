<?php
/**
 * Created by PhpStorm.
 * User: mkt
 * Date: 31.12.16
 * Time: 04:09
 */

namespace Modules\ToDoBundle\Service;


use Core\AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Modules\ToDoBundle\Entity\ToDo;
use Modules\ToDoBundle\Entity\ToDoArchiv;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\RequestStack;

class ToDoHelper
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var RequestStack
     */
    private $requestStack;
    /**
     * @var Container
     */
    private $container;


    public function __construct(EntityManager $em, RequestStack $requestStack, Container $container)
    {
        $this->em = $em;
        $this->requestStack = $requestStack;
        $this->request = $this->requestStack->getCurrentRequest();
        $this->container = $container;
    }

    public function getArchiv(User $user)
    {
        $allArchiv = $this->em->getRepository('ModulesToDoBundle:ToDoArchiv')->findBy(['user' => $user]);

        return $allArchiv;
    }


    public function getPriority(ToDo $todo)
    {
        if ($todo->getPriority() == ToDo::PRIO_HIGH) {
            return 'Hoch';
        } elseif ($todo->getPriority() == ToDo::PRIO_MIDDLE) {
            return 'Mittel';
        } elseif ($todo->getPriority() == ToDo::PRIO_LOW) {
            return 'Niedrig';
        } else {
            return 'Niedrig';
        }
    }

    public function isArchiv(ToDo $todo, $archiv)
    {
        /**
         * @var ToDoArchiv $item
         */
        foreach ($archiv as $item) {
            if ($item->getTodo()->getId() == $todo->getId()) {
                return true;
            }
        }

        return false;
    }

    public function getStatus(ToDo $todo, $archiv)
    {
        /**
         * @var ToDoArchiv $item
         */
        foreach ($archiv as $item) {
            if ($item->getTodo()->getId() == $todo->getId()) {
                return 'archiviert';
            }
        }
        
        if ($todo->getStatus() == ToDo::STATUS_DONE) {
            return 'erledigt';
        } elseif ($todo->getStatus() == ToDo::STATUS_INPROGRESS) {
            return 'in Arbeit';
        } elseif ($todo->getStatus() == ToDo::STATUS_OPEN) {
            return 'offen';
        } elseif ($todo->getStatus() == ToDo::STATUS_ARCHIV) {
            return 'archiviert';
        } else {
            return 'offen';
        }
    }
}