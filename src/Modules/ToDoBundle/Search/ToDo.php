<?php

namespace Modules\ToDoBundle\Search;

use Core\AppBundle\Entity\User;
use Core\AppBundle\Search\SearchItem;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Twig\TwigEngine;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ToDo extends SearchItem
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var TwigEngine
     */
    private $twig;

    /**
     * @var Router
     */
    private $router;
    /**
     * @var User
     */
    private $user;
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(EntityManager $em, TwigEngine $twig, Router $router, ContainerInterface $container)
    {
        $this->em = $em;
        $this->twig = $twig;
        $this->router = $router;
        $this->container = $container;
    }

    /**
     * @param $query
     * @return array
     */
    public function getResults($query)
    {

        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $myEntities =
            $this->em->getRepository('ModulesToDoBundle:ToDo')->findByUserCreated('title', 'asc', $user, []);
        if ($user->getMember()) {
            $myEntities = array_merge($myEntities,
                $this->em->getRepository('ModulesToDoBundle:ToDo')->findByMember(
                    $user->getMember(),
                    'title', 'asc'
                )
            );
        }
        $results = [];
        if ($myEntities) {
            foreach ($myEntities as $myEntity) {
                $results[] = $this->twig->render('ModulesToDoBundle:Search:todo.html.twig', ['todo' => $myEntity]);
            }
        }

        return $results;
    }

    /**
     * @param $query
     * @return array
     */
    public function getAutocompleteResults($query)
    {
        $user = $this->container->get('security.token_storage')->getToken()->getUser();

        $myEntities =
            $this->em->getRepository('ModulesToDoBundle:ToDo')->findByUserCreated('title', 'asc', $user, []);
        if ($user->getMember()) {
            $myEntities = array_merge($myEntities,
                $this->em->getRepository('ModulesToDoBundle:ToDo')->findByMember(
                    $user->getMember(),
                    'title', 'asc'
                )
            );
        }

        $results = [];
        if ($myEntities) {
            /**
             * @var \Modules\ToDoBundle\Entity\Todo $myEntity
             */
            foreach ($myEntities as $myEntity) {

                $results[] = [
                    'icon' => $this->getIcon(), // the icon is optional
                    'title' => $myEntity->getTitle(),
                    'url' => $this->router->generate('modules_todo_default_details', ['delegate' => null, 'todo' => $myEntity->getId()])
                ];
            }
        }

        return $results;
    }

    public function getIcon()
    {
        return 'check';
    }

    public function getName()
    {
        return "ToDo's";
    }

}
