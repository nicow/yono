<?php

namespace Modules\ProjectBundle\Controller;

use Modules\ProjectBundle\Entity\Project;
use Modules\ProjectBundle\Form\ProjectType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ProjectController extends Controller
{
    /**
     * Lists all Project entities.
     * @Route("/")
     * @Method({"GET","POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $projects = $em->getRepository('ModulesProjectBundle:Project')->findAll();
        $columns = $this->get('app.helper')->setTableColumns('mod_project_table_columns', ['title' => ['active' => 1, 'label' => 'Title'], 'address' => ['active' => 1, 'label' => 'Adresse'], 'time' => ['active' => 1, 'label' => 'Zeitraum'], 'persons' => ['active' => 1, 'label' => 'Mitarbeiter'], 'contact' => ['active' => 1, 'label' => 'Kontakt'],'status' => ['active' => 1, 'label' => 'Status']]);
        $perPage = $this->get('app.helper')->setTablePerPage('mod_contact_table_rows');
        $paginator = $this->get('knp_paginator');
        $page = $request->query->getInt('page', 1);
        $pagination = $paginator->paginate($projects, $page, $perPage);

        return $this->render('ModulesProjectBundle:Project:index.html.twig', ['pagination' => $pagination, 'columns' => $columns, 'perPage' => $perPage]);
    }

    /**
     * Creates a new Project entity.
     * @Route("/new")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $project = new Project();
        $form = $this->createForm(ProjectType::class, $project);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($project);
            $em->flush();

            $this->addFlash('success', '<i class="uk-icon-check"></i> Das Projekt wurde erfolgreich angelegt!');

            return $this->redirectToRoute('modules_project_project_show', ['id' => $project->getId()]);
        }

        return $this->render('ModulesProjectBundle:Project:new.html.twig', ['project' => $project, 'form' => $form->createView(),]);
    }

    /**
     * Finds and displays a Project entity.
     * @Route("/{id}")
     * @Method("GET")
     * @param Project $project
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Project $project)
    {
        $deleteForm = $this->createDeleteForm($project);

        $em = $this->getDoctrine()->getManager();
        $offerRepo = $em->getRepository("ModulesOrderManagementBundle:Offer");
        $orderRepo = $em->getRepository("ModulesOrderManagementBundle:Orders");
        $jobTicketRepo = $em->getRepository("ModulesOrderManagementBundle:JobTicket");

        $jobTickets=$jobTicketRepo->findByProject($project);

        return $this->render('ModulesProjectBundle:Project:show.html.twig', ['jobTickets'=> $jobTickets,'project' => $project, 'offers' => $offerRepo->findBy(["project" => $project]), 'orders' => $orderRepo->findBy(["project" => $project]), 'delete_form' => $deleteForm->createView(),]);
    }

    /**
     * Edits a Project status.
     * @Route("/{project}/status/{status}")
     * @Method({"GET", "POST"})
     * @Security("is_granted('EDIT', 'Modules\\OrderManagementBundle\\Entity\\Project')")
     * @param        $status
     * @param Project $project
     * @return RedirectResponse|Response
     */
    public function changeStatusAction($status, Project $project)
    {
        $em = $this->getDoctrine()->getManager();

        $project->setStatus($status);

        $em->flush();

        return new RedirectResponse($this->generateUrl('modules_project_project_show', ['id' => $project->getId()]));

    }

    /**
     * Creates a form to delete a Project entity.
     * @param Project $project The Project entity
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Project $project)
    {
        return $this->createFormBuilder()->setAction($this->generateUrl('modules_project_project_delete', array('id' => $project->getId())))->setMethod('DELETE')->getForm();
    }

    /**
     * Displays a form to edit an existing Project entity.
     * @Route("/{id}/edit")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param Project $project
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @ParamConverter("id", class="Modules\ProjectBundle\Entity\Project")
     */
    public function editAction(Request $request, Project $project)
    {
        $deleteForm = $this->createDeleteForm($project);
        $editForm = $this->createForm('Modules\ProjectBundle\Form\ProjectType', $project);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($project);
            $em->flush();

            $this->addFlash('success', '<i class="uk-icon-check"></i> Das Projekt wurde erfolgreich aktualisiert!');

            return $this->redirectToRoute('modules_project_project_show', ['id' => $project->getId()]);
        }

        return $this->render('ModulesProjectBundle:Project:edit.html.twig', ['project' => $project, 'edit_form' => $editForm->createView(), 'delete_form' => $deleteForm->createView(),]);
    }


    /**
     * @param Request $request
     * @param Project $project
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/{project}/deleteModal",options={"expose"=true})
     */
    public function renderDeleteModal(Request $request, Project $project)
    {
        return $this->render("ModulesProjectBundle:Project:delete-modal.html.twig", ["project" => $project]);
    }


    /**
     * Deletes a Project entity.
     * @Route("/{id}/delete")
     * @param Request $request
     * @param Project $project
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, Project $project)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($project);
        $em->flush();

        return $this->redirectToRoute('modules_project_project_index');
    }
}
