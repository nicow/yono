<?php
/**
 * Created by PhpStorm.
 * User: mkt
 * Date: 31.12.16
 * Time: 04:09
 */

namespace Modules\ProjectBundle\Service;


use Doctrine\ORM\EntityManager;
use Modules\ProjectBundle\Entity\Category;
use Modules\ProjectBundle\Entity\Project;
use Symfony\Component\HttpFoundation\RequestStack;

class ProjectHelper
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var RequestStack
     */
    private $requestStack;

    public function __construct(EntityManager $em, RequestStack $requestStack)
    {
        $this->em = $em;
        $this->requestStack = $requestStack;
        $this->request = $this->requestStack->getCurrentRequest();
    }

    /**
     * @return Category|null
     */
    public function getCurrentCategory()
    {
        if ($this->request->attributes->has('_route') && strpos($this->request->attributes->get('_route'), 'modules_project') !== false && $this->request->attributes->has('category') && $this->request->attributes->get('category') instanceof Category) {
            return $this->request->attributes->get('category');
        }

        return null;
    }

    public function getProjectCountForCategory(Category $category)
    {
        $projects = $this->em->getRepository('ModulesProjectBundle:Project')->findByCategory($category);

        return count($projects);
    }

    public function getProjectStatus(Project $project)
    {
        if ($project->getStatus() == Project::STATUS_PREPARED) {
            return 'vorbereitet';
        } elseif ($project->getStatus() == Project::STATUS_ACTIVE) {
            return 'offen';
        } elseif ($project->getStatus() == Project::STATUS_COMPLETED) {
            return 'abgeschlossen';
        } else {
            return '-';
        }
    }
}