<?php
/**
 * Created by PhpStorm.
 * User: mkt
 * Date: 04.01.17
 * Time: 20:54
 */

namespace Modules\ProjectBundle\Search;

use Core\AppBundle\Search\SearchItem;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Twig\TwigEngine;
use Symfony\Bundle\FrameworkBundle\Routing\Router;

class Project extends SearchItem
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var TwigEngine
     */
    private $twig;

    /**
     * @var Router
     */
    private $router;

    public function __construct(EntityManager $em, TwigEngine $twig, Router $router)
    {
        $this->em = $em;
        $this->twig = $twig;
        $this->router = $router;
    }

    /**
     * @param $query
     * @return array
     */
    public function getResults($query)
    {
        $projects = $this->em->getRepository('ModulesProjectBundle:Project')->createQueryBuilder('p')->where('p.name LIKE :query')->setParameter('query', '%' . $query . '%')->getQuery()->getResult();

        $results = [];
        if ($projects) {
            /** @var \Modules\ProjectBundle\Entity\Project $project */
            foreach ($projects as $project) {
                $results[] = $this->twig->render('@ModulesProject/Search/project.html.twig', ['project' => $project]);
            }
        }

        return $results;
    }

    /**
     * @param $query
     * @return array
     */
    public function getAutocompleteResults($query)
    {
        $projects = $this->em->getRepository('ModulesProjectBundle:Project')->createQueryBuilder('p')->where('p.name LIKE :query')->setParameter('query', '%' . $query . '%')->getQuery()->getResult();

        $results = [];
        if ($projects) {
            /** @var \Modules\ProjectBundle\Entity\Project $project */
            foreach ($projects as $project) {
                $results[] = ['icon' => 'university', 'title' => $project->getName(), 'url' => $this->router->generate('modules_project_project_show', ['id' => $project->getId()]), 'text' => ''];
            }
        }

        return $results;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'Projekte';
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'university';
    }

    public function getRequiredPermissions()
    {
        return ['VIEW' => 'Modules\\ProjectBundle\\Entity\\Project'];
    }
}