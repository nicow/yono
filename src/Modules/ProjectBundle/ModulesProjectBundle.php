<?php

namespace Modules\ProjectBundle;

use Core\YonoBundleInterface;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class ModulesProjectBundle extends Bundle implements YonoBundleInterface
{
    /**
     * @return string
     */
    public function getDisplayName()
    {
        return 'Projekte';
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return '';
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'university';
    }
}
