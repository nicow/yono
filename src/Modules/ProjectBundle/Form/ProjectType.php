<?php

namespace Modules\ProjectBundle\Form;

use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProjectType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', null, ['label' => 'Titel'])->add('description', CKEditorType::class, ['label' => 'Beschreibung'])->add('categories', null, ['label' => 'Kategorien'])->add('start', DateType::class, ['required' => true, 'widget' => 'single_text', 'format' => 'd.M.y',])->add('end', DateType::class, ['required' => false, 'widget' => 'single_text', 'format' => 'd.M.y',])->add('contact', null, ['label' => 'Kontakt'])->add('users', null, ['label' => 'Benutzer'])->add('address', null, ['label' => 'Adresse', 'required' => false]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array('data_class' => 'Modules\ProjectBundle\Entity\Project'));
    }
}
