<?php

namespace Modules\OrderManagementBundle\Traits;

use Doctrine\ORM\Mapping as ORM;
use Modules\OrderManagementBundle\Entity\Division;

/**
 * Created by PhpStorm.
 * User: Nico
 * Date: 11.05.2017
 * Time: 10:34
 */
trait DivisionTrait
{
    /**
     * @var Division
     * @ORM\ManyToOne(targetEntity="Modules\OrderManagementBundle\Entity\Division")
     * @ORM\JoinColumn(name="division_id", referencedColumnName="id")
     */
    private $division;

    /**
     * @return Division
     */
    public function getDivision()
    {
        return $this->division;
    }

    /**
     * @param Division $division
     * @return $this
     */
    public function setDivision(Division $division)
    {
        $this->division = $division;

        return $this;
    }
}