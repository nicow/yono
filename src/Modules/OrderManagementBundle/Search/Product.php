<?php

namespace Modules\OrderManagementBundle\Search;

use Core\AppBundle\Search\SearchItem;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Twig\TwigEngine;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Product extends SearchItem
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var TwigEngine
     */
    private $twig;

    /**
     * @var Router
     */
    private $router;

    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(EntityManager $em, TwigEngine $twig, Router $router, ContainerInterface $container)
    {
        $this->em = $em;
        $this->twig = $twig;
        $this->router = $router;
        $this->container = $container;
    }

    /**
     * @param $query
     * @return array
     */
    public function getResults($query)
    {
        $myEntities = $this->em->getRepository('ModulesOrderManagementBundle:Product')->createQueryBuilder('entity')->where('entity.title LIKE :query')->setParameter('query', '%' . $query . '%')->getQuery()->getResult();

        $user = $this->container->get('security.token_storage')->getToken()->getUser();

        if ($user->getContact() && !$user->getMember()) {
            $myEntities = null;
        }

        $results = [];
        if ($myEntities) {
            foreach ($myEntities as $myEntity) {
                $results[] = $this->twig->render('ModulesOrderManagementBundle:Search:product.html.twig', ['product' => $myEntity]);
            }
        }

        return $results;
    }

    /**
     * @param $query
     * @return array
     */
    public function getAutocompleteResults($query)
    {
        $myEntities = $this->em->getRepository('ModulesOrderManagementBundle:Product')->createQueryBuilder('entity')->where('entity.title LIKE :query')->setParameter('query', '%' . $query . '%')->getQuery()->getResult();

        $user=$this->container->get('security.token_storage')->getToken()->getUser();
        
        if ($user->getContact() && !$user->getMember()) {
            $myEntities = null;
        }
        
        $results = [];
        if ($myEntities) {
            /**
             * @var \Modules\OrderManagementBundle\Entity\Product $myEntity
             */
            foreach ($myEntities as $myEntity) {

                $price = number_format($myEntity->getPrice(), 2, ',', '.');
                $type = $myEntity->getPriceTypeName();

                if ($type) { //$type may be null!
                    $price = $price . '€ / ' . $type;
                }

                $results[] = ['icon' => $this->getIcon(), // the icon is optional
                    'title' => $myEntity->getTitle() . ' ( ' . $price . ' )',
                    'url' => $this->router->generate('modules_ordermanagement_product_details', ['product' => $myEntity->getId()])];
            }
        }

        return $results;
    }

    public function getIcon()
    {
        return 'cubes';
    }

    public function getName()
    {
        return "Produkte";
    }

}
