<?php

namespace Modules\OrderManagementBundle\Search;

use Core\AppBundle\Entity\UserGroup;
use Core\AppBundle\Search\SearchItem;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Twig\TwigEngine;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Offer extends SearchItem
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var TwigEngine
     */
    private $twig;

    /**
     * @var Router
     */
    private $router;

    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(EntityManager $em, TwigEngine $twig, Router $router,ContainerInterface $container)
    {
        $this->em = $em;
        $this->twig = $twig;
        $this->router = $router;
        $this->container = $container;
    }

    /**
     * @param $query
     * @return array
     */
    public function getResults($query)
    {
        $myEntities = $this->em->getRepository('ModulesOrderManagementBundle:Offer')
            ->createQueryBuilder('entity')
            ->leftJoin('entity.contact','c')
            ->leftJoin('entity.contactAddress','a')
            ->leftJoin('entity.contactPerson','p')
            ->leftJoin('entity.project','pr')
            ->where('c.name LIKE :query')
            ->orWhere('a.name LIKE :query')
            ->orWhere('a.street LIKE :query')
            ->orWhere('a.city LIKE :query')
            ->orWhere('p.firstname LIKE :query')
            ->orWhere('p.lastname LIKE :query')
            ->orWhere('pr.name LIKE :query')
            ->orWhere('entity.title LIKE :query')
            ->orWhere('entity.description LIKE :query')
            ->orWhere('entity.number LIKE :query')
            ->orWhere('entity.number LIKE :query');


        $user=$this->container->get('security.token_storage')->getToken()->getUser();

        if($user->getContact() && (!$user->getMember() || !$user->hasRole('ROLE_PERM_EDIT_MODULES_ORDERMANAGEMENTBUNDLE_ENTITY_OFFER'))) {
            /**
             * @var UserGroup $group
             */
            $users = [];
            foreach ($user->getGroups() as $group) {

                $groupUser = false;
                foreach ($group->getRoles() as $role) {
                    if ('ROLE_PERM_VIEW_MODULES_ORDERMANAGEMENTBUNDLE_ENTITY_OFFER' == $role) {
                        $groupUser = true;
                    }
                }
                if ($groupUser) $users[] = $this->em->getRepository('CoreAppBundle:User')->findByGroup($group);
            }

            if ($users) {
                $where = '';
                foreach ($users as $userlsit) {
                    foreach ($userlsit as $userFromList) {
                        if ($userFromList->getContact()) $where .= ' OR entity.contact = ' . $userFromList->getContact()->getId();
                    }
                }
                if($user->getContact())
                {
                    $myEntities->andWhere('entity.contact = :userContact ' . $where);
                    $myEntities->setParameter('userContact', $user->getContact()->getId());
                }


            } else {
                $myEntities->andWhere('entity.contact = :userContact');
                $myEntities->setParameter('userContact', $user->getContact()->getId());
            }
        }

        $query = $myEntities->setParameter('query', '%' . $query . '%')->getQuery();
        $myEntities = $query->getResult();

        $results = [];
        if ($myEntities) {
            foreach ($myEntities as $myEntity) {
                $results[] = $this->twig->render('ModulesOrderManagementBundle:Search:offer.html.twig', ['offer' => $myEntity]);
            }
        }

        return $results;
    }

    /**
     * @param $query
     * @return array
     */
    public function getAutocompleteResults($query)
    {
        $myEntities = $this->em->getRepository('ModulesOrderManagementBundle:Offer')
            ->createQueryBuilder('entity')
            ->leftJoin('entity.contact','c')
            ->leftJoin('entity.contactAddress','a')
            ->leftJoin('entity.contactPerson','p')
            ->leftJoin('entity.project','pr')
            ->where('c.name LIKE :query')
            ->orWhere('a.name LIKE :query')
            ->orWhere('a.street LIKE :query')
            ->orWhere('a.city LIKE :query')
            ->orWhere('p.firstname LIKE :query')
            ->orWhere('p.lastname LIKE :query')
            ->orWhere('pr.name LIKE :query')
            ->orWhere('entity.title LIKE :query')
            ->orWhere('entity.description LIKE :query')
            ->orWhere('entity.number LIKE :query')
            ->orWhere('entity.number LIKE :query');

        $user=$this->container->get('security.token_storage')->getToken()->getUser();

        if($user->getContact() && (!$user->getMember() || !$user->hasRole('ROLE_PERM_EDIT_MODULES_ORDERMANAGEMENTBUNDLE_ENTITY_OFFER'))) {
            /**
             * @var UserGroup $group
             */
            $users = [];
            foreach ($user->getGroups() as $group) {

                $groupUser = false;
                foreach ($group->getRoles() as $role) {
                    if ('ROLE_PERM_CREATE_MODULES_ORDERMANAGEMENTBUNDLE_ENTITY_OFFER' == $role) {
                        $groupUser = true;
                    }
                }
                if ($groupUser) $users[] = $this->em->getRepository('CoreAppBundle:User')->findByGroup($group);
            }

            if ($users) {
                $where = '';
                foreach ($users as $userlsit) {
                    foreach ($userlsit as $userFromList) {
                        if ($userFromList->getContact()) $where .= ' OR entity.contact = ' . $userFromList->getContact()->getId();
                    }
                }
                if($user->getContact())
                {
                    $myEntities->andWhere('entity.contact = :userContact ' . $where);
                    $myEntities->setParameter('userContact', $user->getContact()->getId());
                }


            } else {
                $myEntities->andWhere('entity.contact = :userContact');
                $myEntities->setParameter('userContact', $user->getContact()->getId());
            }
        }
        $query = $myEntities->setParameter('query', '%' . $query . '%')->getQuery();
        $myEntities = $query->getResult();

        $results = [];
        if ($myEntities) {
            /**
             * @var \Modules\OrderManagementBundle\Entity\Offer $myEntity
             */
            foreach ($myEntities as $myEntity) {

                $results[] = ['icon' => $this->getIcon(), // the icon is optional
                    'title' => $myEntity->getTitle() . ' - ' . $myEntity->getNumber(), 'url' => $this->router->generate('modules_ordermanagement_offer_details', ['offer' => $myEntity->getId()])];
            }
        }

        return $results;
    }

    public function getIcon()
    {
        return 'bank';
    }

    public function getName()
    {
        return "Angebote";
    }

}
