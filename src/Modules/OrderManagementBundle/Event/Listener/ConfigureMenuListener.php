<?php
/**
 * Created by PhpStorm.
 * User: mkt
 * Date: 26.08.16
 * Time: 17:22
 */

namespace Modules\OrderManagementBundle\Event\Listener;


use Core\AppBundle\Entity\User;
use Core\AppBundle\Event\ConfigureMenuEvent;
use Doctrine\ORM\EntityManager;
use Knp\Menu\FactoryInterface;
use Modules\OrderManagementBundle\Service\OrderManagementHelper;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class ConfigureMenuListener
{
    /**
     * @var User
     */
    protected $user;
    /**
     * @var EntityManager
     */
    private $em;
    /**
     * @var Router
     */
    private $router;
    /**
     * @var OrderManagementHelper
     */
    private $omHelper;
    /**
     * @var RequestStack
     */
    private $requestStack;

    public function __construct(EntityManager $em, Router $router, OrderManagementHelper $omHelper, RequestStack $requestStack, TokenStorage $tokenStorage)
    {
        $this->em = $em;
        $this->user = $tokenStorage->getToken()->getUser();
        $this->router = $router;
        $this->omHelper = $omHelper;
        $this->requestStack = $requestStack;
        $this->request = $requestStack->getCurrentRequest();
    }

    public function onMenuConfigure(ConfigureMenuEvent $event)
    {

    }

    public function addChildren(FactoryInterface $factory, Category $category, $menu)
    {

    }
}