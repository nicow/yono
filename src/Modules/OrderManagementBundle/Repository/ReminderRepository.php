<?php

namespace Modules\OrderManagementBundle\Repository;

use Core\AppBundle\Entity\User;
use Core\AppBundle\Entity\UserGroup;
use Modules\OrderManagementBundle\Entity\Reminder;

/**
 * ReminderRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ReminderRepository extends \Doctrine\ORM\EntityRepository
{

    /**
     * @param string $sort
     * @param string $direction
     * @return Reminder[]
     */
    public function findAllOrdered($sort = 'createdAt', $direction = 'ASC', $filter = null,User $user)
    {
        $direction = strtoupper($direction);

        $qb = $this->createQueryBuilder('r');

        $qb->where('r.createdAt IS NOT NULL ');

        if (isset($filter['start'])) {
            if ($filter['start'] != '') {
                $qb->andWhere('r.createdAt >= :starts');
                $qb->setParameter('starts', date('Y-m-d', strtotime($filter['start'])));
            }

        }
        if (isset($filter['stop'])) {
            if ($filter['stop'] != '') {
                $qb->andWhere('r.createdAt <= :stop');
                $qb->setParameter('stop', date('Y-m-d', strtotime($filter['stop'])) . ' 23:59:59');
            }
        }
        if (isset($filter['contact'])) {
            if ($filter['contact'] != '') {
                $qb->leftJoin('r.invoice','i');
                $qb->andWhere('i.contact = :contact');
                $qb->setParameter('contact', $filter['contact']);
            }
        }
        if (isset($filter['division'])) {
            if ($filter['division'] != '') {
                $qb->andWhere('r.division = :division');
                $qb->setParameter('division', $filter['division']);
            }
        }

        if (isset($filter['deadline'])) {
            if ($filter['deadline'] != '') {
                $qb->andWhere('r.deadline = :deadline');
                $qb->setParameter('deadline',  date('Y-m-d', strtotime($filter['deadline'])));
            }
        }

        if (isset($filter['status'])) {
            if ($filter['status'] != '') {
                $qb->andWhere('r.status = :status');
                $qb->setParameter('status', $filter['status']);
            }
        }

        if ($user->getContact() && $user->getGroups() && $user->hasRole('ROLE_PERM_CREATE_MODULES_ORDERMANAGEMENTBUNDLE_ENTITY_REMINDER') && (!$user->getMember() || !$user->hasRole('ROLE_PERM_EDIT_MODULES_ORDERMANAGEMENTBUNDLE_ENTITY_REMINDER'))) {

            if (!$user->hasRole('ROLE_PERM_DELETE_MODULES_ORDERMANAGEMENTBUNDLE_ENTITY_REMINDER')) {
                $qb->leftJoin('r.invoice', 'i');
                /**
                 * @var UserGroup $group
                 */
                $users = [];
                foreach ($user->getGroups() as $group) {

                    $groupUser = false;
                    foreach ($group->getRoles() as $role) {
                        if ('ROLE_PERM_CREATE_MODULES_ORDERMANAGEMENTBUNDLE_ENTITY_REMINDER' == $role) {
                            $groupUser = true;
                        }
                    }
                    if ($groupUser) $users[] = $this->getEntityManager()->getRepository('CoreAppBundle:User')->findByGroup($group);
                }

                if ($users) {
                    $where = '';
                    foreach ($users as $userlsit) {
                        foreach ($userlsit as $user) {
                            if ($user->getContact()) $where .= ' OR i.contact = ' . $user->getContact()->getId();
                        }
                    }
                    if ($user->getContact()) {
                        $qb->andWhere('i.contact = :userContact ' . $where);
                        $qb->setParameter('userContact', $user->getContact()->getId());
                    }


                } else {
                    $qb->andWhere('i.contact = :userContact');
                    $qb->setParameter('userContact', $user->getContact()->getId());
                }
            }
        } elseif ($user->getContact() && (!$user->getMember() || !$user->hasRole('ROLE_PERM_EDIT_MODULES_ORDERMANAGEMENTBUNDLE_ENTITY_REMINDER'))) {
            $qb->leftJoin('r.invoice','i');
            $qb->andWhere('i.contact = :userContact');
            $qb->setParameter('userContact', $user->getContact()->getId());
        }

        $qb->addOrderBy('r.' . $sort, $direction);

        return $qb->getQuery()->getResult();
    }

}
