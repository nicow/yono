<?php

namespace Modules\OrderManagementBundle\Repository;

use Modules\OrderManagementBundle\Entity\JobTicket;
use Modules\ProjectBundle\Entity\Project;
use Modules\TeamBundle\Entity\Member;

/**
 * JobTicketRepository
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class JobTicketRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * @param string $sort
     * @param string $direction
     * @return JobTicket[]
     */
    public function findAllOrdered($sort = 'createdAt', $direction = 'ASC', $filter)
    {
        $direction = strtoupper($direction);

        $qb = $this->createQueryBuilder('p');

        $qb->where('p.createdAt IS NOT NULL ');

        if (isset($filter['member'])) {
            if ($filter['member'] != '') {
                $qb->leftJoin('p.members', 'm')->where('m.id = :member');
                $qb->setParameter('member', $filter['member']);
            }
        }
        if (isset($filter['division'])) {
            if ($filter['division'] != '') {
                $qb->andWhere('p.division = :division');
                $qb->setParameter('division', $filter['division']);
            }
        }

        if (isset($filter['order'])) {
            if ($filter['order'] != '') {
                $qb->andWhere('p.order = :order');
                $qb->setParameter('order', $filter['order']);
            }
        }

        if (isset($filter['status'])) {
            if ($filter['status'] != '') {
                $qb->andWhere('p.status = :status');
                $qb->setParameter('status', $filter['status']);
            } else {
                $qb->andWhere('p.status != 5 OR p.status IS NULL');
            }
        } else {
            $qb->andWhere('p.status != 5 OR p.status IS NULL');
        }

        $qb->addOrderBy('p.' . $sort, $direction);


        return $qb->getQuery()->getResult();
    }

    /**
     * @param Project $project
     * @return array
     */
    public function findByProject(Project $project)
    {
        $qb = $this->createQueryBuilder('j')->leftJoin('j.order', 'o')->leftJoin('o.project', 'p')->where('p.id = :project')->setParameter('project', $project->getId());

        $orders = $qb->getQuery()->getResult();

        $qb = $this->createQueryBuilder('j')->where('j.project = :project')->setParameter('project', $project->getId());

        $project = $qb->getQuery()->getResult();

        return array_merge($orders, $project);
    }


    /**
     * @return array
     */
    public function findOpen()
    {
        $qb = $this->createQueryBuilder('j')->where('j.status <= :status')->setParameter('status', 3)->orderBy('j.title');

        return $qb->getQuery()->getResult();
    }

    /**
     * @param Member $member
     * @return array
     */
    public function findByMember(Member $member)
    {
        $qb = $this->createQueryBuilder('j')->leftJoin('j.members', 'm')->where('m.id = :member')->andWhere('j.status != 4')->setParameter('member', $member->getId());

        return $qb->getQuery()->getResult();
    }

    /**
     * @param Member $member
     * @return array
     */
    public function findByMemberAll(Member $member)
    {
        $qb = $this->createQueryBuilder('j')->leftJoin('j.members', 'm')->where('m.id = :member')->setParameter('member', $member->getId());

        return $qb->getQuery()->getResult();
    }

    /**
     * @param $event
     * @param $jobticket
     * @return array
     */
    public function findByEvent($event, $jobticket)
    {
        $qb = $this->createQueryBuilder('j')->leftJoin('j.event', 'e')->where('e.id = :event')->setParameter('event', $event->getId())->andWhere('j.id = :jobticket ')->setParameter('jobticket', $jobticket->getId());

        return $qb->getQuery()->getResult();
    }

    /**
     * @param Member $member
     * @return array
     */
    public function findByMemberJobs(Member $member)
    {
        $qb = $this->createQueryBuilder('j')->leftJoin('j.members', 'm')->where('m.id = :member')->setParameter('member', $member->getId())->leftJoin('j.event', 'e')->andWhere('j.fixDate <= :fix OR j.fixDate IS NULL')->andWhere('j.visible = 1 AND m.id = :member')->setParameter('fix', date('Y-m-d'))->andWhere('j.status != 4');

        $all = $qb->getQuery()->getArrayResult();


        $qb = $this->createQueryBuilder('j')->leftJoin('j.event', 'e')->andWhere('e.start >= :start')->setParameter('start', date('Y-m-d') . ' 00:10:00')->andWhere('e.end <= :ende')->setParameter('ende', date('Y-m-d') . ' 23:59:59')->andWhere('j.status != 4')->leftJoin('e.members', 'm')->andWhere('m.id = :member')->setParameter('member', $member->getId());

        $allCalender = $qb->getQuery()->getArrayResult();



        return array_map("unserialize", array_unique(array_map("serialize", array_merge($all, $allCalender))));
    }
}
