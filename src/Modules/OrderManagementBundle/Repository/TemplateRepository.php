<?php

namespace Modules\OrderManagementBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Modules\OrderManagementBundle\Entity\Division;
use Modules\OrderManagementBundle\Entity\Template;
use Symfony\Component\HttpFoundation\File\File;

/**
 * TemplateRepository

 */
class TemplateRepository extends EntityRepository
{
    /**
     * @param Division $division
     * @param $class
     * @return string|bool
     */
    public function getTemplateName(Division $division, $class)
    {
        $template = $this->findOneBy([
            'division' => $division,
            'class' => $class
        ]);

        if ($template) {
            /**
             * @var Template $template
             */
            return $template->getDocument()->getName();
        }

        return false;
    }

    /**
     * @param Division $division
     * @param $class
     * @return string|bool
     */
    public function getTemplatePath(Division $division, $class)
    {
        $template = $this->findOneBy([
            'division' => $division,
            'class' => $class
        ]);

        if ($template) {
            /**
             * @var Template $template
             */
            return $template->getDocument()->getPath();
        }

        return false;
    }

    /**
     * @param Division $division
     * @param $class
     * @return bool|File
     */
    public function getTemplate(Division $division, $class)
    {
        $template = $this->findOneBy([
            'division' => $division,
            'class' => $class
        ]);

        if ($template) {
            $file = new File($template->getDocument()->getPath() . $template->getDocument()->getName());
            return $file;
        }

        return false;
    }
}
