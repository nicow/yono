<?php

namespace Modules\OrderManagementBundle\Controller;

use Core\AppBundle\Entity\User;
use Modules\OrderManagementBundle\Entity\Calculation;
use Modules\OrderManagementBundle\Entity\CalculationItem;
use Modules\OrderManagementBundle\Entity\Document;
use Modules\OrderManagementBundle\Entity\Invoice;
use Modules\OrderManagementBundle\Entity\InvoicePositions;
use Modules\OrderManagementBundle\Entity\Orders;
use Modules\OrderManagementBundle\Entity\Product;
use Modules\OrderManagementBundle\Entity\Service;
use Modules\OrderManagementBundle\Form\CreditEditType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;


/**
 * credit controller.
 * @Route("/credit")
 */
class CreditController extends Controller
{
    /**
     * Lists all credit entities.
     * @Route("/")
     * @Method({"GET", "POST"})
     * @Security("is_granted('VIEW', 'Modules\\OrderManagementBundle\\Entity\\Invoice')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();

        if ($request->query->all()) {
            $this->get('app.settings')->set('mod_order_management_credit_overview', $request->query->all());
        }

        $filter = $this->get('app.settings')->get('mod_order_management_credit_overview');


        $credits = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:Invoice')->findAllOrdered($request->query->get('sort', 'title'), $request->query->get('direction', 'ASC'), $filter, true, $user);

        $columns = $this->get('app.helper')->setTableColumns('mod_order_management_credit_table_columns', ['number' => ['active' => 1,
            'label' => 'Nummer'],
            'title' => ['active' => 1, 'label' => 'Title'],
            'contact' => ['active' => 1, 'label' => 'Kunde'],
            'project' => ['active' => 1, 'label' => 'Projekt'],
            'status' => ['active' => 1, 'label' => 'Status'],
            'totalPrice' => ['active' => 1, 'label' => 'Gesamtsumme'],
            'createdAt' => ['active' => 1, 'label' => 'erstellt am'],
            'updatedAt' => ['active' => 0, 'label' => 'letzte Änderung'],]);
        $perPage = $this->get('app.helper')->setTablePerPage('mod_order_management_credit_table_rows');
        $paginator = $this->get('knp_paginator');
        $page = $request->query->getInt('page', 1);
        $pagination = $paginator->paginate($credits, $page, $perPage);

        $deleteForms = [];
        foreach ($credits as $credit) {
            $deleteForms[$credit->getId()] = $this->createDeleteForm($credit)->createView();
        }
        $divisions = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:Division')->findAll();

        return $this->render('ModulesOrderManagementBundle:Credit:index.html.twig', ['columns' => $columns,
            'divisions' => $divisions,
            'pagination' => $pagination,
            'perPage' => $perPage,
            'credits' => $credits,
            'filterSettings' => $filter,
            'deleteForms' => $deleteForms,]);
    }

    /**
     * Creates a form to delete a credit entity.
     * @param Invoice $credit The member entity
     * @return Form The form
     */
    private function createDeleteForm(Invoice $credit)
    {
        return $this->createFormBuilder()->setAction($this->generateUrl('modules_ordermanagement_credit_delete', ['credit' => $credit->getId()]))->setMethod('DELETE')->getForm();
    }

    /**
     * @Route("/{credit}/details")
     * @Security("is_granted('VIEW', 'Modules\\OrderManagementBundle\\Entity\\Invoice')")
     * @param Orders $credit
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function detailsAction(Request $request, Invoice $credit)
    {
        $omHelper = $this->get('mod.order_management.helper');

        $calaculationItems = $omHelper->getInvoiceItems($credit);

        return $this->render('ModulesOrderManagementBundle:Credit:details.html.twig', ['invoice' => $credit,
            'calaculationItems' => $calaculationItems,]);
    }

    /**
     * Edits a Credit status.
     * @Route("/{credit}/status/{status}/{index}")
     * @Method({"GET", "POST"})
     * @Security("is_granted('EDIT', 'Modules\\OrderManagementBundle\\Entity\\Invoice')")
     * @param      $status
     * @param      $index
     * @param Invoice $credit
     * @return RedirectResponse|Response
     */
    public function changeStatusAction($status, Invoice $credit, $index = false)
    {
        $em = $this->getDoctrine()->getManager();

        $credit->setStatus($status);

        $em->flush();

        $this->addFlash('success', '<i class="uk-icon-check"></i> Der Status der Gutschrift wurde erfolgreich geändert!');
        if ($index) {
            return new RedirectResponse($this->generateUrl('modules_ordermanagement_credit_index'));
        } else {
            return new RedirectResponse($this->generateUrl('modules_ordermanagement_credit_details', ['credit' => $credit->getId()]));
        }


    }

    /**
     * Creates a new credit entity.
     * @Route("/new")
     * @Method({"GET", "POST"})
     * @Security("is_granted('CREATE', 'Modules\\OrderManagementBundle\\Entity\\Invoice')")
     * @param Request $request
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $credit = new Invoice();
        $calculation = new Calculation();
        $creditPositions = null;

        $flow = $this->get('mod.order_management.form.flow.credit');


        $dataRequest = $request->request->get('credit');
        if (isset($dataRequest['contactAddress'])) {
            $request->getSession()->set('credit_skip', $request->request->all());
        }


        $flow->bind($credit);

        $form = $flow->createForm();
        if ($flow->isValid($form)) {
            $creditPositions = $request->request->all();

            if ($flow->getCurrentStep() == 1) {
                $request->getSession()->remove('credit_calculationitems');
                $request->getSession()->remove('credit_discount');
                $request->getSession()->remove('credit_skip');
                $request->getSession()->remove('credit_internal');
                $request->getSession()->remove('credit_project_skip');
                $request->getSession()->remove('credit_terms_of_payment');
            }

            if ($flow->getCurrentStep() == 6) {
                $request->getSession()->set('credit_internal', $request->request->all());
                $request->getSession()->set('credit_project_skip', $request->request->all());
            }


            if ($flow->getCurrentStep() == 8) {
                //Save Services & Products in Session
                $request->getSession()->set('credit_calculationitems', $request->request->all());

            }

            $flow->saveCurrentStepData($form);


            if ($flow->nextStep()) {
                // form for the next step
                $form = $flow->createForm();

            } else {

                // flow finished
                $em = $this->getDoctrine()->getManager();

                //create contact if not exists
                if (!$credit->contact->getId()) {
                    $em->persist($credit->contact);
                    $em->flush();
                }
                //create address if not exists
                $addressID = $credit->getContactAddress() ? $credit->getContactAddress()->getId() : null;
                $addressName = $credit->getContactAddress() ? $credit->getContactAddress()->getName() : null;
                if (!$addressID && $addressName) {
                    $credit->contactAddress->setContact($credit->contact);
                    $em->persist($credit->contactAddress);
                    $em->flush();
                }
                //create person if not exists
                $personID = $credit->getContactPerson() ? $credit->getContactPerson()->getId() : null;
                $personName = $credit->getContactPerson() ? $credit->getContactPerson()->getFirstname() : null;
                if (!$personID && $personName) {
                    $credit->contactPerson->setContact($credit->contact);
                    $em->persist($credit->contactPerson);
                    $em->flush();
                }

                $calculationPrice = $request->request->all();

                //create calculation entity and save items
                if ($request->getSession()->has('credit_calculationitems')) {

                    //CREATE CALCULATION ENITY
                    $calculation = new Calculation();
                    $calculation->setStatus(1);

                    if (isset($calculationPrice['credit']['discount'])) if (is_numeric($calculationPrice['credit']['discount'])) $calculation->setDiscount($calculationPrice['credit']['discount']);

                    if ($request->getSession()->get('credit_discount') == null) {
                        $calculation->setContactDiscount(1);
                    }

                    if (isset($calculationPrice['credit']['discountType'])) $calculation->setDiscountType(1);

                    if ($calculationPrice['creditFinalPrice']) $calculation->setFinalPrice(number_format($calculationPrice['creditFinalPrice'], 2, ".", ""));

                    $isNettoAccount = $request->getSession()->get('credit_internal');
                    if (isset($isNettoAccount['credit']['netto'])) $calculation->setNetto(1);


                    $em->persist($calculation);
                    $em->flush();

                    $data = $request->getSession()->get('credit_calculationitems');
                    $omHelper = $this->get('mod.order_management.helper');
                    //CREATE CALCULATION ITEMS
                    if (isset($data['credit']['services'])) {
                        foreach ($data['credit']['services'] as $services) {

                            if ($services['textFree'] != '' && $services['priceFree'] != '') {
                                $services['priceFree'] = str_replace('.', '', $services['priceFree']);
                                $services['priceFree'] = str_replace(',', '.', $services['priceFree']);
                                $calculationItem = new CalculationItem();
                                $calculationItem->setCalculation($calculation);
                                $calculationItem->setStatus(1);
                                $calculationItem->setFreeText($services['textFree']);
                                $calculationItem->setFreeTax($services['taxFree']);
                                $calculationItem->setFreePriceType($services['priceFreeType']);
                                $calculationItem->setFreeText($services['textFree']);
                                $singlePrice = $services['priceFree'] / $services['amountFree'];
                                $calculationItem->setSinglePrice(number_format($singlePrice, 2, ".", ""));
                                $calculationItem->setAmount($services['amountFree']);
                                $em->persist($calculationItem);

                                if ($services['saveFree'] == '1') {
                                    $serviceNew = new Service();
                                    $serviceNew->setTitle($services['textFree']);
                                    $serviceNew->setPrice(number_format($singlePrice, 2, ".", ""));
                                    $serviceNew->setPriceType($omHelper->getServicePriceTypeByName($services['priceFreeType']));
                                    $serviceNew->setTax($services['taxFree']);

                                    $em->persist($serviceNew);
                                }

                                if ($services['saveFree'] == '2') {
                                    $productNew = new Product();
                                    $productNew->setTitle($services['textFree']);
                                    $productNew->setPrice(number_format($singlePrice, 2, ".", ""));
                                    $productNew->setPriceType($omHelper->getServicePriceTypeByName($services['priceFreeType']));
                                    $productNew->setTax($services['taxFree']);

                                    $em->persist($productNew);
                                }

                            }
                        }
                    }
                }

                //SAVE INVOCIE ENTITY AND SET STATUS
                $credit->setStatus(0);
                $credit->setCredit(1);
                $em->persist($credit);
                $em->flush();

                //SET NUMBER FOR EXPORT
                $omHelper = $this->get('mod.order_management.helper');
                $division = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:Division')->find($credit->getDivision());
                $division->setInvoiceCount($division->getInvoiceCount() + 1);
                $em->flush();

                $getNumber = $omHelper->buildNumber($credit, 'credit', $division->getInvoiceCount());

                $credit->setNumber($getNumber);
                $em->flush();

                $creditPosition = new InvoicePositions();
                $creditPosition->setInvoice($credit);
                $creditPosition->setCalculation($calculation);
                $em->persist($creditPosition);
                $em->flush();

                $flow->reset(); // remove step data from the session
                $request->getSession()->remove('credit_calculationitems');
                $request->getSession()->remove('credit_discount');
                $request->getSession()->remove('credit_skip');
                $request->getSession()->remove('credit_project_skip');
                $request->getSession()->remove('credit_internal');
                $request->getSession()->remove('credit_terms_of_payment');

                $this->addFlash('success', '<i class="uk-icon-check"></i> Die Gutschrift wurde erfolgreich angelegt!');

                return $this->redirectToRoute('modules_ordermanagement_credit_details', ['credit' => $credit->getId()]);
            }
        }

        return $this->render('ModulesOrderManagementBundle:Credit:new.html.twig', ['formData' => $credit,
            'creditPositions' => $creditPositions,
            'flow' => $flow,
            'form' => $form->createView(),]);
    }


    /**
     * Creates a new credit entity.
     * @Route("/{credit}/edit")
     * @Method({"GET", "POST"})
     * @Security("is_granted('CREATE', 'Modules\\OrderManagementBundle\\Entity\\Invoice')")
     * @param Request $request
     * @param Invoice $credit
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, Invoice $credit)
    {
        $em = $this->getDoctrine()->getManager();

        $editForm = $this->createForm(CreditEditType::class, $credit);

        $editForm->handleRequest($request);

        $omHelper = $this->get('mod.order_management.helper');

        $calaculationItems = $omHelper->getInvoiceItems($credit);

        if ($editForm->isSubmitted() && $editForm->isValid()) {

            $items = $request->request->get('credit_position_item');
            $deleteItems = $request->request->get('delete');
            $amount = $request->request->get('credit_position_amount');

            if ($items) {
                for ($x = 0; $x < count($items); $x++) {
                    $calaculationItem = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:CalculationItem')->find($items[$x]);

                    $calaculationItem->setAmount($amount[$x]);
                }
            }
            if ($deleteItems) {
                for ($x = 0; $x < count($deleteItems); $x++) {
                    $calaculationItem = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:CalculationItem')->find($deleteItems[$x]);

                    $em->remove($calaculationItem);
                }


            }
            $price = str_replace('.', '', $request->request->get('invoice_total_price'));
            $price = str_replace(',', '.', $price);

            //GET FIRST CALC ID - ONLY KM
            $calaculationFromPosiiton = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:InvoicePositions')->findOneBy(['invoice' => $credit]);

            $calculation = $calaculationFromPosiiton->getCalculation();

            $calculation->setFinalPrice($price);

            $em->flush();

            $this->addFlash('success', '<i class="uk-icon-check"></i> Die Gutschrift wurde erfolgreich aktualisiert!');


            return $this->redirectToRoute('modules_ordermanagement_credit_details', ['credit' => $credit->getId()]);
        }

        return $this->render('ModulesOrderManagementBundle:Credit:edit.html.twig', ['invoice' => $credit,
            'calaculationItems' => $calaculationItems,
            'edit_form' => $editForm->createView(),]);
    }

    /**
     * Creates a new credit entity from an offer entity
     * @Route("/import")
     * @Method({"GET", "POST"})
     * @Security("is_granted('CREATE', 'Modules\\OrderManagementBundle\\Entity\\Invoice')")
     * @param Request $request
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function importAction(Request $request)
    {

        if ($request->getMethod() == 'POST') {
            $em = $this->getDoctrine()->getManager();
            $order = $em->getRepository('ModulesOrderManagementBundle:Orders')->findOneBy(['id' => $request->request->get('order')]);

            if ($order) {
                $credit = new Invoice();
                $credit->setContactAddress($order->getContactAddress());
                $credit->setContact($order->getContact());
                $credit->setContactPerson($order->getContactPerson());
                $credit->setDescription($order->getDescription());
                $credit->setProject($order->getProject());
                $credit->setTitle($order->getTitle());
                $credit->setStatus(0);
                $credit->setDivision($order->getDivision());
                $em->persist($credit);

                $order->setStatus(2);
                $em->flush();

                //SET NUMBER FOR EXPORT
                $omHelper = $this->get('mod.order_management.helper');
                $division = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:Division')->find($credit->getDivision());
                $division->setInvoiceCount($division->getInvoiceCount() + 1);
                $em->flush();

                $getNumber = $omHelper->buildNumber($credit, 'invoice', $division->getInvoiceCount());

                $credit->setNumber($getNumber);

                $em->flush();

                $creditPosition = new InvoicePositions();
                $creditPosition->setInvoice($credit);
                $creditPosition->setOrders($order);
                $creditPosition->setCalculation($order->getCalculation());
                $em->persist($creditPosition);
                $em->flush();


                $this->addFlash('success', '<i class="uk-icon-check"></i>Der Auftrag wurde erfolgreich in eine Rechnung überführt.');

                return $this->redirectToRoute('modules_ordermanagement_credit_details', ['credit' => $credit->getId()]);
            }

        }

        return $this->redirectToRoute('modules_ordermanagement_credit_index');
    }

    /**
     * Edit a credit entity.
     * @Route("/{credit}/editajax")
     * @Method({"GET", "POST"})
     * @Security("is_granted('CREATE', 'Modules\\OrderManagementBundle\\Entity\\Invoice')")
     * @param Request $request
     * @param Invoice $credit
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAjaxAction(Request $request, Invoice $credit)
    {

        $editForm = $this->createForm(CreditEditType::class, $credit);

        $editForm->handleRequest($request);


        return $this->render('ModulesOrderManagementBundle:Invoice:edit.html.twig', ['credit' => $credit,
            'edit_form' => $editForm->createView(),]);
    }

    /**
     * Deletes a credit entity.
     * @Route("/{credit}/delete")
     * @Method({"GET", "DELETE"})
     * @Security("is_granted('DELETE', 'Modules\\OrderManagementBundle\\Entity\\Credit') or is_granted('DELETE', credit)")
     * @param Request $request
     * @param Invoice $credit
     * @return RedirectResponse
     */
    public function deleteAction(Request $request, Invoice $credit)
    {
        $form = $this->createDeleteForm($credit);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($credit);
            $em->flush();

            $this->addFlash('success', '<i class="uk-icon-check"></i> Die Rechnung wurde erfolgreich gelöscht!');
        }


        return $this->redirectToRoute('modules_ordermanagement_credit_index');
    }


    /**
     * Export Credit in Choosen File
     * @Route("/export/{direct}")
     * @Security("is_granted('VIEW', 'Modules\\OrderManagementBundle\\Entity\\Invoice')")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function exportAction(Request $request, $direct = false)
    {

        $place = null;

        if ($request->request->get('place')) {
            $place = $request->request->get('place');
        }
        $fileformat = 'pdf';

        if (!$direct) {
            /**
             * @var Invoice $invoice
             */
            $invoice = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:Invoice')->findOneBy(['id' => $request->request->get('invoice')]);
            $invoice->setPrintDate(new \DateTime($request->request->get('printDate')));
            $invoice->setStatus(1);
            $invoice->setPlaceholder($place);

            $this->getDoctrine()->getManager()->flush();
        } else {
            $invoice = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:Invoice')->findOneBy(['id' => $direct]);
            $place = $invoice->getPlaceholder();
        }

        $omHelper = $this->get('mod.order_management.helper');

        $inovicePositions = $omHelper->getInvoicePositions($invoice);

        $calaculationItems = $omHelper->getInvoiceItems($invoice);


        $nettoInvoice = false;

        /**
         * @var InvoicePositions $inovicePosition
         */
        foreach ($inovicePositions as $inovicePosition) {

            if ($inovicePosition->getCalculation()->getNetto()) {
                $nettoInvoice = true;
            }
        }

        $document = new Document($invoice->getDivision()->getCreditTemplate()->getPath() . $invoice->getDivision()->getCreditTemplate()->getName());


        $document->NoErr = true;

        @$document->MergeField("customer", ['id' => $invoice->getContact()->getId(),
            'city' => utf8_decode($invoice->getContactAddress() ? $invoice->getContactAddress()->getCity() : ""),
            'zip' => $invoice->getContactAddress() ? $invoice->getContactAddress()->getZip() : "",
            'company' => utf8_decode($invoice->getContact() ? $invoice->getContact()->getName() : ""),
            'street' => utf8_decode($invoice->getContactAddress() ? $invoice->getContactAddress()->getStreet() : ""),
            'firstname' => utf8_decode($invoice->getContactPerson() ? $invoice->getContactPerson()->getFirstname() : ""),
            'lastname' => utf8_decode($invoice->getContactPerson() ? $invoice->getContactPerson()->getLastname() : ""),
            'salutation' => utf8_decode($invoice->getContactPerson() ? $invoice->getContactPerson()->getSalutation() : ""),
            'position' => utf8_decode($invoice->getContactPerson() ? $invoice->getContactPerson()->getPosition() : "")]);


        $count = 1;
        $priceTotal = 0;
        $price = 0;
        $amount = 0;
        $type = null;
        $singleprice = 0;
        $title = null;
        $flatrate = null;

        if ($request->request->get('place')) {
            $place = $request->request->get('place');
        }

        $list = [];

        /**
         * @var CalculationItem $item
         */
        foreach ($calaculationItems as $item) {
            if ($item->getProduct()) {

                $singleprice = $item->getSinglePrice();

                $amount = $item->getAmount();
                $title = $item->getProduct()->getTitle();
                $description = $item->getProduct()->getDescription();
                $price = $singleprice * $amount;
                $type = $item->getProduct()->getPriceTypeName();
            }
            if ($item->getService()) {

                $singleprice = $item->getSinglePrice();

                $amount = $item->getAmount();
                $title = $item->getService()->getTitle();
                $description = $item->getService()->getDescription();
                $type = $item->getService()->getPriceTypeName();
                $price = $singleprice * $amount;
            }
            if ($item->getFreeText() and $item->getSinglePrice()) {
                $amount = $item->getAmount();
                $singleprice = $item->getSinglePrice();
                $title = $item->getFreeText();
                $type = $item->getFreePriceType();
                $price = $singleprice * $amount;
                $description = '';
            }

            if ($item->getSpecialPrice()) {
                $singleprice = $item->getSpecialPrice() / $amount;
                $price = $item->getSpecialPrice();
            }

            $priceTotal = $priceTotal + $price;

            if ($count == 25) {
                $list[] = ['title' => '',
                    'description' => '',
                    'amount' => '',
                    'pos' => '',
                    'type' => '',
                    'singleprice' => '',
                    'sum' => '',];
            }

            $list[] = ['title' => utf8_decode($title),
                'description' => utf8_decode(strip_tags($description)),
                'amount' => number_format($amount, 2, ',', '.') . ' ' . utf8_decode($type),
                'pos' => $count,
                'type' => utf8_decode($type),
                'singleprice' => number_format($singleprice, 2, ',', '.'),
                'sum' => number_format($price, 2, ',', '.')];

            $count++;
        }
        //Fill the table with products and services
        $document->MergeBlock("ps", $list);
        $netto = $priceTotal * 0.81;
        $discount = 0;

        $invoicePosition = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:InvoicePositions')->findOneBy(['invoice' => $invoice]);


        if ($omHelper->invoiceDiscountCheck($invoice)) {
            $discount = ($priceTotal * $invoice->getContact()->getDiscount()) / 100;
        }

        /**
         * @var InvoicePositions $inovicePosition
         */
        foreach ($inovicePositions as $inovicePosition) {

            if ($inovicePosition->getCalculation()->getDiscount()) {
                if ($inovicePosition->getCalculation()->getDiscountType()) {
                    $discount = $discount + ($priceTotal * $inovicePosition->getCalculation()->getDiscount()) / 100;
                } else {
                    $discount = $discount + $inovicePosition->getCalculation()->getDiscount();
                }

            }
        }

        $steuer = ($priceTotal - $discount) * 0.19;
        if ($nettoInvoice) {
            $steuer = 0;
        }


        $user = $invoice->getCreatedBy();
        $member = $user->getMember() ?: $user;

        if ($member instanceof User) {
            $name = $member->getUsername();
        } else {
            $name = $member->getFullName();
        }

        $document->MergeField("team", ['fullname' => utf8_decode($name)]);

        if (!$omHelper->isNetto($invoice)) {
            $total = $omHelper->getInvoicePrice($invoice);
        } else {
            $total = $omHelper->getInvoicePrice($invoice);
        }

        if ($invoice->getPrintDate()) {
            $printDate = $invoice->getPrintDate()->format('d.m.Y');
        } else {
            $printDate = $invoice->getCreatedAt()->format('d.m.Y');
        }

        $document->MergeField("offer", ['printdate' => "\t" . $printDate,
            'id' => $invoice->getNumber(),
            'address' => ['name' => utf8_decode($invoice->getContactAddress() ? $invoice->getContactAddress()->getName() : ""),
                'city' => utf8_decode($invoice->getContactAddress() ? $invoice->getContactAddress()->getCity() : ""),],
            'subtotal' => $priceTotal,
            'mwst' => $steuer,
            'discount' => $discount,
            'total' => $total,
            'place' => utf8_decode($place),
            'psch' => $flatrate]);


        $helper = $this->get('app.filehandle');


        $outputFile = $helper->getCompiledPath($invoice->getId(), 'Modules\OrderManagementBundle\Entity\Invoice');
        $document->Show(OPENTBS_FILE, $outputFile);

        $docConverter = $this->get('app.documentconverter');
        $convertedPath = $helper->getConvertedPath($invoice->getId(),$invoice->getNumber(), 'Modules\OrderManagementBundle\Entity\Invoice', $fileformat);
        $docConverter->convertTo($outputFile, $convertedPath, $fileformat);

        $response = new BinaryFileResponse($convertedPath);
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);

        return $response;

    }
}
