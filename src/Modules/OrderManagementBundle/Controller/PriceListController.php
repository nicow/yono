<?php

namespace Modules\OrderManagementBundle\Controller;

use Modules\OrderManagementBundle\Entity\PriceList;
use Modules\OrderManagementBundle\Form\PriceListEditType;
use Modules\OrderManagementBundle\Form\PriceListType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


/**
 * Service controller.
 * @Route("/pricelist")
 */
class PriceListController extends Controller
{
    /**
     * Lists all Pricelist entities.
     * @Route("/")
     * @Method({"GET", "POST"})
     * @Security("is_granted('VIEW', 'Modules\\OrderManagementBundle\\Entity\\PriceList')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $pricelists = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:PriceList')->findAllOrdered($request->query->get('sort', 'title'), $request->query->get('direction', 'ASC'));

        $columns = $this->get('app.helper')->setTableColumns('mod_order_management_pricelist_table_columns', ['contact' => ['active' => 1, 'label' => 'Kontakt'], 'description' => ['active' => 0, 'label' => 'Beschreibung'], 'createdBy' => ['active' => 1, 'label' => 'erstellt von'], 'createdAt' => ['active' => 1, 'label' => 'erstellt am'], 'updatedAt' => ['active' => 0, 'label' => 'letzte Änderung'],]);

        $perPage = $this->get('app.helper')->setTablePerPage('mod_order_management_pricelist_table_rows');
        $paginator = $this->get('knp_paginator');
        $page = $request->query->getInt('page', 1);
        $pagination = $paginator->paginate($pricelists, $page, $perPage);

        $deleteForms = [];
        foreach ($pricelists as $pricelist) {
            $deleteForms[$pricelist->getId()] = $this->createDeleteForm($pricelist)->createView();
        }

        return $this->render('ModulesOrderManagementBundle:PriceList:index.html.twig', ['columns' => $columns, 'pagination' => $pagination, 'perPage' => $perPage, 'deleteForms' => $deleteForms,]);
    }

    /**
     * Creates a form to delete a product entity.
     * @param PriceList $priceList
     * @return Form The form
     */
    private function createDeleteForm(PriceList $priceList)
    {
        return $this->createFormBuilder()->setAction($this->generateUrl('modules_ordermanagement_pricelist_delete', ['priceList' => $priceList->getId()]))->setMethod('DELETE')->getForm();
    }

    /**
     * Creates a new Pricelist entity.
     * @Route("/new")
     * @Route("/category/{category}/new")
     * @Method({"GET", "POST"})
     * @Security("is_granted('CREATE', 'Modules\\OrderManagementBundle\\Entity\\Product')")
     * @param Request $request
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $pricelist = new PriceList();

        $form = $this->createForm(PriceListType::class, $pricelist);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($pricelist);
            $pricelist->getContact()->setPricelist($pricelist);
            $em->flush();

            $this->addFlash('success', '<i class="uk-icon-check"></i> Die Preisliste wurde erfolgreich angelegt!');

            return $this->redirectToRoute('modules_ordermanagement_pricelist_details', ['pricelist' => $pricelist->getId()]);

        }

        return $this->render('ModulesOrderManagementBundle:PriceList:new.html.twig', ['pricelist' => $pricelist, 'form' => $form->createView(),]);
    }

    /**
     * @Route("/{pricelist}/details")
     * @Security("is_granted('VIEW', 'Modules\\OrderManagementBundle\\Entity\\PriceList')")
     * @param PriceList $pricelist
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function detailsAction(Request $request, PriceList $pricelist)
    {
        return $this->render('ModulesOrderManagementBundle:PriceList:details.html.twig', ['pricelist' => $pricelist]);
    }

    /**
     * Edits a pricelist entity.
     * @Route("/{pricelist}/edit")
     * @Method({"GET", "POST"})
     * @Security("is_granted('EDIT', 'Modules\\OrderManagementBundle\\Entity\\PriceList')")
     * @param Request $request
     * @param PriceList $pricelist
     * @return RedirectResponse|Response
     */
    public function editAction(Request $request, PriceList $pricelist)
    {
        $form = $this->createForm(PriceListEditType::class, $pricelist);
        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted()) {
            $em = $this->getDoctrine()->getManager();

            $contactRepo = $em->getRepository('ModulesContactBundle:Contact');

            $contact = $contactRepo->findOneBy(['pricelist' => $pricelist]);

            if ($contact) {
                if ($contact !== $pricelist->getContact()) {
                    $contact->setPricelist(null);

                    $pricelist->getContact()->setPricelist($pricelist);
                }
            }

            $em->flush();

            return new RedirectResponse($this->generateUrl('modules_ordermanagement_pricelist_details', ['pricelist' => $pricelist->getId()]));
        }

        return $this->render('ModulesOrderManagementBundle:PriceList:edit.html.twig', ['pricelist' => $pricelist, 'form' => $form->createView()]);
    }

    /**
     * Deletes a PriceList entity.
     * @Route("/{priceList}/delete")
     * @Method({"GET", "DELETE"})
     * @Security("is_granted('DELETE', 'Modules\\OrderManagementBundle\\Entity\\PriceList')")
     * @param Request $request
     * @param PriceList $priceList
     * @return RedirectResponse
     */
    public function deleteAction(Request $request, PriceList $priceList)
    {
        $form = $this->createDeleteForm($priceList);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $priceList->getContact()->setPricelist(null);
            $em->remove($priceList);
            $em->flush();

            $this->addFlash('success', '<i class="uk-icon-check"></i> Die Preisliste wurde erfolgreich gelöscht!');
        }


        return $this->redirectToRoute('modules_ordermanagement_pricelist_index');
    }
}
