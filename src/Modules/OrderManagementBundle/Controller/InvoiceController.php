<?php

namespace Modules\OrderManagementBundle\Controller;

use Core\AppBundle\Entity\User;
use Modules\OrderManagementBundle\Entity\ExportPlaceholder;
use Modules\OrderManagementBundle\Entity\Invoice;
use Modules\OrderManagementBundle\Entity\Calculation;
use Modules\OrderManagementBundle\Entity\CalculationItem;
use Modules\OrderManagementBundle\Entity\Document;
use Modules\OrderManagementBundle\Entity\InvoicePositions;
use Modules\OrderManagementBundle\Entity\Orders;
use Modules\OrderManagementBundle\Entity\Product;
use Modules\OrderManagementBundle\Entity\Service;
use Modules\OrderManagementBundle\Form\ContactEditAjaxType;
use Modules\OrderManagementBundle\Form\InvoiceEditAjaxType;
use Modules\OrderManagementBundle\Form\InvoiceEditType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;


/**
 * invoice controller.
 * @Route("/invoice")
 */
class InvoiceController extends Controller
{
    /**
     * Lists all invoice entities.
     * @Route("/")
     * @Method({"GET", "POST"})
     * @Security("is_granted('VIEW', 'Modules\\OrderManagementBundle\\Entity\\Invoice')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();

        if ($request->query->all()) {
            $this->get('app.settings')->set('mod_order_management_invoice_overview', $request->query->all());
        }

        $filter = $this->get('app.settings')->get('mod_order_management_invoice_overview');

        $sort = 'number';
        if (isset($filter['sort'])) {
            $sort = $filter['sort'];
        }

        $direction = 'ASC';
        if (isset($filter['direction'])) {
            $direction = $filter['direction'];
        }

        $invoices = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:Invoice')->findAllOrdered($request->query->get('sort', $sort), $request->query->get('direction', $direction), $filter, false, $user);


        $columns = $this->get('app.helper')->setTableColumns('mod_order_management_invoice_table_columns', ['number' => ['active' => 1,
            'label' => 'Nummer'],
            'title' => ['active' => 1, 'label' => 'Title'],
            'contact' => ['active' => 1, 'label' => 'Kunde'],
            'project' => ['active' => 1, 'label' => 'Projekt'],
            'status' => ['active' => 1, 'label' => 'Status'],
            'totalPrice' => ['active' => 1, 'label' => 'Gesamtsumme'],
            'printDate' => ['active' => 1, 'label' => 'Rechnungsdatum'],
            'createdAt' => ['active' => 0, 'label' => 'erstellt am'],
            'updatedAt' => ['active' => 0, 'label' => 'letzte Änderung'],]);
        $perPage = $this->get('app.helper')->setTablePerPage('mod_order_management_invoice_table_rows');
        $paginator = $this->get('knp_paginator');
        $page = $request->query->getInt('page', 1);
        $pagination = $paginator->paginate($invoices, $page, $perPage);

        $deleteForms = [];
        foreach ($invoices as $invoice) {
            $deleteForms[$invoice->getId()] = $this->createDeleteForm($invoice)->createView();
        }
        $divisions = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:Division')->findAll();


        return $this->render('ModulesOrderManagementBundle:Invoice:index.html.twig', ['columns' => $columns,
            'divisions' => $divisions,
            'pagination' => $pagination,
            'perPage' => $perPage,
            'filterSettings' => $filter,
            'invoices' => $invoices,
            'deleteForms' => $deleteForms,]);
    }

    /**
     * Creates a form to delete a invoice entity.
     * @param Invoice $invoice The member entity
     * @return Form The form
     */
    private function createDeleteForm(Invoice $invoice)
    {
        return $this->createFormBuilder()->setAction($this->generateUrl('modules_ordermanagement_invoice_delete', ['invoice' => $invoice->getId()]))->setMethod('DELETE')->getForm();
    }

    /**
     * Lists all invoice entities which are monthlyfee.
     * @Route("/monthlyfee")
     * @Method({"GET", "POST"})
     * @Security("is_granted('VIEW', 'Modules\\OrderManagementBundle\\Entity\\Invoice')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function monthlyFeeAction(Request $request)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();

        if ($request->query->all()) {
            $this->get('app.settings')->set('mod_order_management_invoice_monthlyfee', $request->query->all());
        }

        $filter = $this->get('app.settings')->get('mod_order_management_invoice_monthlyfee');

        $sort = 'number';
        if (isset($filter['sort'])) {
            $sort = $filter['sort'];
        }

        $direction = 'ASC';
        if (isset($filter['direction'])) {
            $direction = $filter['direction'];
        }

        $invoices = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:Invoice')->findAllOrderedMonthlyFee($request->query->get('sort', $sort), $request->query->get('direction', $direction), $filter, false, $user);


        $columns = $this->get('app.helper')->setTableColumns('mod_order_management_invoice_monthlyfee_table_columns', ['number' => ['active' => 1,
            'label' => 'Nummer'],
            'title' => ['active' => 1, 'label' => 'Title'],
            'contact' => ['active' => 1, 'label' => 'Kunde'],
            'project' => ['active' => 1, 'label' => 'Projekt'],
            'status' => ['active' => 1, 'label' => 'Status'],
            'totalPrice' => ['active' => 1, 'label' => 'Gesamtsumme'],
            'monthlyFee' => ['active' => 1, 'label' => 'Monat'],
            'printDate' => ['active' => 1, 'label' => 'Rechnungsdatum'],
            'createdAt' => ['active' => 0, 'label' => 'erstellt am'],
            'updatedAt' => ['active' => 0, 'label' => 'letzte Änderung'],]);
        $perPage = $this->get('app.helper')->setTablePerPage('mod_order_management_invoice_monthlyfee_table_rows');
        $paginator = $this->get('knp_paginator');
        $page = $request->query->getInt('page', 1);
        $pagination = $paginator->paginate($invoices, $page, $perPage);

        $deleteForms = [];
        foreach ($invoices as $invoice) {
            $deleteForms[$invoice->getId()] = $this->createDeleteForm($invoice)->createView();
        }
        $divisions = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:Division')->findAll();


        return $this->render('ModulesOrderManagementBundle:Invoice:monthlyFee.html.twig', ['columns' => $columns,
            'divisions' => $divisions,
            'pagination' => $pagination,
            'perPage' => $perPage,
            'filterSettings' => $filter,
            'invoices' => $invoices,
            'deleteForms' => $deleteForms,]);
    }

    /**
     * @Route("/{invoice}/details")
     * @Security("is_granted('VIEW', 'Modules\\OrderManagementBundle\\Entity\\Invoice')")
     * @param Orders $invoice
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function detailsAction(Request $request, Invoice $invoice)
    {
        $omHelper = $this->get('mod.order_management.helper');

        $calaculationItems = $omHelper->getInvoiceItems($invoice);

        return $this->render('ModulesOrderManagementBundle:Invoice:details.html.twig', ['invoice' => $invoice,
            'calaculationItems' => $calaculationItems,]);
    }

    /**
     * Edits a Invoice status.
     * @Route("/{invoice}/status/{status}/{index}")
     * @Method({"GET", "POST"})
     * @Security("is_granted('EDIT', 'Modules\\OrderManagementBundle\\Entity\\Invoice')")
     * @param      $status
     * @param Invoice $invoice
     * @param      $index
     * @return RedirectResponse|Response
     */
    public function changeStatusAction($status, Invoice $invoice, $index = false)
    {
        $em = $this->getDoctrine()->getManager();

        $invoice->setStatus($status);

        $em->flush();

        $this->addFlash('success', '<i class="uk-icon-check"></i> Der Status des Auftrags wurde erfolgreich geändert!');
        if ($index) {
            return new RedirectResponse($this->generateUrl('modules_ordermanagement_invoice_index'));
        } else {
            return new RedirectResponse($this->generateUrl('modules_ordermanagement_invoice_details', ['invoice' => $invoice->getId()]));
        }
    }

    /**
     * Creates a new invoice entity.
     * @Route("/new")
     * @Method({"GET", "POST"})
     * @Security("is_granted('CREATE', 'Modules\\OrderManagementBundle\\Entity\\Invoice')")
     * @param Request $request
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $invoice = new Invoice();
        $calculation = new Calculation();
        $invoicePositions = null;

        $flow = $this->get('mod.order_management.form.flow.invoice');


        $dataRequest = $request->request->get('invoice');
        if (isset($dataRequest['contactAddress'])) {
            $request->getSession()->set('invoice_skip', $request->request->all());
        }

        $flow->bind($invoice);
        $form = $flow->createForm();
        if ($flow->isValid($form)) {
            $invoicePositions = $request->request->all();

            if ($flow->getCurrentStep() == 1) {
                $request->getSession()->remove('invoice_calculationitems');
                $request->getSession()->remove('invoice_discount');
                $request->getSession()->remove('invoice_skip');
                $request->getSession()->remove('invoice_project_skip');
                $request->getSession()->remove('invoice_internal');
                $request->getSession()->remove('invoice_terms_of_payment');
            }

            if ($flow->getCurrentStep() == 6) {
                $request->getSession()->set('invoice_internal', $request->request->all());

                $request->getSession()->set('invoice_project_skip', $request->request->all());
            }


            if ($flow->getCurrentStep() == 8) {
                //Save Services & Products in Session
                $request->getSession()->set('invoice_calculationitems', $request->request->all());

                $dataRequest = $request->request->get('invoice');

                if (isset($dataRequest['noContactDiscount'])) {

                    $request->getSession()->set('invoice_discount', $dataRequest['noContactDiscount']);
                }
            }

            $flow->saveCurrentStepData($form);


            if ($flow->nextStep()) {
                // form for the next step
                $form = $flow->createForm();

            } else {
                // flow finished
                $em = $this->getDoctrine()->getManager();

                //create contact if not exists
                if (!$invoice->contact->getId()) {
                    $em->persist($invoice->contact);
                    $em->flush();
                }
                //create address if not exists
                $addressID = $invoice->getContactAddress() ? $invoice->getContactAddress()->getId() : null;
                $addressName = $invoice->getContactAddress() ? $invoice->getContactAddress()->getName() : null;
                if (!$addressID && $addressName) {
                    $invoice->contactAddress->setContact($invoice->contact);
                    $em->persist($invoice->contactAddress);
                    $em->flush();
                }
                //create person if not exists
                $personID = $invoice->getContactPerson() ? $invoice->getContactPerson()->getId() : null;
                $personName = $invoice->getContactPerson() ? $invoice->getContactPerson()->getFirstname() : null;
                if (!$personID && $personName) {
                    $invoice->contactPerson->setContact($invoice->contact);
                    $em->persist($invoice->contactPerson);
                    $em->flush();
                }

                $calculationPrice = $request->request->all();

                //create calculation entity and save items
                if ($request->getSession()->has('invoice_calculationitems')) {

                    //CREATE CALCULATION ENITY
                    $calculation = new Calculation();
                    $calculation->setStatus(1);

                    if (isset($calculationPrice['invoice']['discount'])) if (is_numeric($calculationPrice['invoice']['discount'])) $calculation->setDiscount($calculationPrice['invoice']['discount']);

                    if ($request->getSession()->get('invoice_discount') == null) {
                        $calculation->setContactDiscount(1);
                    }

                    if (isset($calculationPrice['invoice']['discountType'])) $calculation->setDiscountType(1);

                    if ($calculationPrice['invoiceFinalPrice']) {
                        if (strpos($calculationPrice['invoiceFinalPrice'], ',') !== false) {
                            $calculationPrice['invoiceFinalPrice'] = str_replace('.', '', $calculationPrice['invoiceFinalPrice']);
                            $calculationPrice['invoiceFinalPrice'] = str_replace(',', '.', $calculationPrice['invoiceFinalPrice']);
                        }

                        $calculation->setFinalPrice(number_format($calculationPrice['invoiceFinalPrice'], 2, ".", ""));
                    }

                    if (isset($request->getSession()->get('invoice_internal')['invoice']['netto'])) $calculation->setNetto(1);

                    if (isset($request->getSession()->get('invoice_internal')['invoice']['monthlyFee'])) {
                        if ($request->getSession()->get('invoice_internal')['invoice']['monthlyFee'] >= 1) $calculation->setMonthlyFee($request->getSession()->get('invoice_internal')['invoice']['monthlyFee']);
                    }

                    if (isset($request->getSession()->get('invoice_internal')['invoice']['termsOfPayment'])) {
                        $termsOfPaymentRepo = $em->getRepository('ModulesOrderManagementBundle:TermsOfPayment');

                        $termsOfPayment = $termsOfPaymentRepo->find($request->getSession()->get('invoice_internal')['invoice']['termsOfPayment']);
                        $calculation->setTermsOfPayment($termsOfPayment);
                    }


                    $em->persist($calculation);
                    $em->flush();

                    $data = $request->getSession()->get('invoice_calculationitems');

                    $omHelper = $this->get('mod.order_management.helper');

                    //CREATE CALCULATION ITEMS
                    if (isset($data['invoice']['services'])) {
                        foreach ($data['invoice']['services'] as $services) {

                            $service = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:Service')->find($services['service']);
                            if ($service) {
                                $calculationItem = new CalculationItem();
                                $calculationItem->setCalculation($calculation);
                                $calculationItem->setStatus(1);
                                $calculationItem->setService($service);
                                if ($services['specialPrice']) {
                                    $services['specialPrice'] = str_replace('.', '', $services['specialPrice']);
                                    $services['specialPrice'] = str_replace(',', '.', $services['specialPrice']);
                                    $calculationItem->setSpecialPrice(number_format($services['specialPrice'], 2, ".", ""));
                                }
                                $calculationItem->setAmount($services['amount']);
                                $calculationItem->setExtraText($services['extraText']);
                                $calculationItem->setSinglePrice($service->getPrice());
                                $em->persist($calculationItem);
                            } elseif ($services['textFree'] != '' && $services['priceFree'] != '') {
                                $services['priceFree'] = str_replace('.', '', $services['priceFree']);
                                $services['priceFree'] = str_replace(',', '.', $services['priceFree']);
                                $calculationItem = new CalculationItem();
                                $calculationItem->setCalculation($calculation);
                                $calculationItem->setStatus(1);
                                $calculationItem->setFreeText($services['textFree']);
                                $calculationItem->setFreeTax($services['taxFree']);
                                $calculationItem->setFreePriceType($services['priceFreeType']);
                                $calculationItem->setFreeText($services['textFree']);
                                $calculationItem->setExtraText($services['extraText']);
                                $singlePrice = $services['priceSingleFree'];
                                $calculationItem->setSinglePrice(number_format($singlePrice, 2, ".", ""));
                                $calculationItem->setAmount($services['amountFree']);
                                $em->persist($calculationItem);

                                if ($services['saveFree'] == '1') {
                                    $serviceNew = new Service();
                                    $serviceNew->setTitle($services['textFree']);
                                    $serviceNew->setPrice(number_format($singlePrice, 2, ".", ""));
                                    $serviceNew->setPriceType($omHelper->getServicePriceTypeByName($services['priceFreeType']));
                                    $serviceNew->setTax($services['taxFree']);

                                    $em->persist($serviceNew);
                                }

                                if ($services['saveFree'] == '2') {
                                    $productNew = new Product();
                                    $productNew->setTitle($services['textFree']);
                                    $productNew->setPrice(number_format($singlePrice, 2, ".", ""));
                                    $productNew->setPriceType($omHelper->getServicePriceTypeByName($services['priceFreeType']));
                                    $productNew->setTax($services['taxFree']);

                                    $em->persist($productNew);
                                }

                            } else {
                                $product = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:Product')->find($services['product']);
                                if ($product) {
                                    $calculationItem = new CalculationItem();
                                    $calculationItem->setCalculation($calculation);
                                    $calculationItem->setStatus(1);
                                    $calculationItem->setProduct($product);
                                    $calculationItem->setExtraText($services['extraText']);
                                    if ($services['specialPriceProduct']) {
                                        $services['specialPriceProduct'] = str_replace('.', '', $services['specialPriceProduct']);
                                        $services['specialPriceProduct'] = str_replace(',', '.', $services['specialPriceProduct']);
                                        $calculationItem->setSpecialPrice(number_format($services['specialPriceProduct'], 2, ".", ""));
                                    }
                                    $calculationItem->setAmount($services['amountProduct']);
                                    $calculationItem->setSinglePrice($product->getPrice());
                                    $em->persist($calculationItem);
                                }
                            }
                        }
                    }
                }

                //SAVE OFFER ENTITY AND SET STATUS
                $invoice->setStatus(0);
                $em->persist($invoice);
                $em->flush();

                //SET NUMBER FOR EXPORT

                $division = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:Division')->find($invoice->getDivision());
                $division->setInvoiceCount($division->getInvoiceCount() + 1);
                $em->flush();

                $getNumber = $omHelper->buildNumber($invoice, 'invoice', $division->getInvoiceCount());

                $invoice->setNumber($getNumber);
                $em->flush();

                $invoicePosition = new InvoicePositions();
                $invoicePosition->setInvoice($invoice);
                $invoicePosition->setCalculation($calculation);
                $em->persist($invoicePosition);
                $em->flush();

                $flow->reset(); // remove step data from the session
                $request->getSession()->remove('invoice_calculationitems');
                $request->getSession()->remove('invoice_discount');
                $request->getSession()->remove('invoice_project_skip');
                $request->getSession()->remove('invoice_skip');
                $request->getSession()->remove('invoice_internal');
                $request->getSession()->remove('invoice_terms_of_payment');

                $this->addFlash('success', '<i class="uk-icon-check"></i> Die Rechnung wurde erfolgreich angelegt!');

                return $this->redirectToRoute('modules_ordermanagement_invoice_details', ['invoice' => $invoice->getId()]);
            }
        }

        return $this->render('ModulesOrderManagementBundle:Invoice:new.html.twig', ['formData' => $invoice,
            'invoicePositions' => $invoicePositions,
            'flow' => $flow,
            'form' => $form->createView(),]);
    }


    /**
     * Creates a new invoice entity.
     * @Route("/{invoice}/edit")
     * @Method({"GET", "POST"})
     * @Security("is_granted('CREATE', 'Modules\\OrderManagementBundle\\Entity\\Invoice')")
     * @param Request $request
     * @param Invoice $invoice
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, Invoice $invoice)
    {
        $em = $this->getDoctrine()->getManager();

        $calaculationFromPosiiton = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:InvoicePositions')->findOneBy(['invoice' => $invoice]);

        $calculation = $calaculationFromPosiiton->getCalculation();
        $netto = false;
        if ($calculation->getNetto()) {
            $netto = true;
        }
        $montlyFee = false;
        if ($calculation->getMonthlyFee()) {
            $montlyFee = $calculation->getMonthlyFee();
        }
        $editForm = $this->createForm(InvoiceEditType::class, $invoice, ['netto' => $netto,
            'monthlyFee' => $montlyFee]);

        $editForm->handleRequest($request);

        $omHelper = $this->get('mod.order_management.helper');

        $calaculationItems = $omHelper->getInvoiceItems($invoice);

        if ($editForm->isSubmitted() && $editForm->isValid()) {

            $invoicePositions = $em->getRepository('ModulesOrderManagementBundle:InvoicePositions')->findOneBy(['invoice' => $invoice]);
            $positions = $request->request->get('invoice_edit');

            $contactData = $request->request->get('contact_edit_ajax');

            if (isset($contactData['contactPerson'])) {
                $contactPersonEntity = $em->getRepository('ModulesContactBundle:Person')->find($contactData['contactPerson']);
                $invoice->setContactPerson($contactPersonEntity);
            }

            if (isset($contactData['contactAddress'])) {
                $contactAddressEntity = $em->getRepository('ModulesContactBundle:Address')->find($contactData['contactAddress']);
                $invoice->setContactAddress($contactAddressEntity);
            }


            if (isset($positions['services'])) {
                foreach ($positions['services'] as $services) {
                    $service = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:Service')->find($services['service']);
                    if ($service) {
                        $calculationItem = new CalculationItem();
                        $calculationItem->setCalculation($invoicePositions->getCalculation());
                        $calculationItem->setStatus(1);
                        $calculationItem->setExtraText($services['extraText']);
                        $calculationItem->setService($service);
                        if ($services['specialPrice']) {
                            $services['specialPrice'] = str_replace('.', '', $services['specialPrice']);
                            $services['specialPrice'] = str_replace(',', '.', $services['specialPrice']);
                            $calculationItem->setSpecialPrice($services['specialPrice']);
                        }
                        $calculationItem->setAmount($services['amount']);
                        $calculationItem->setSinglePrice($service->getPrice());
                        $em->persist($calculationItem);
                    } elseif ($services['textFree'] != '' && $services['priceFree'] != '') {
                        $services['priceFree'] = str_replace('.', '', $services['priceFree']);
                        $services['priceFree'] = str_replace(',', '.', $services['priceFree']);
                        $calculationItem = new CalculationItem();
                        $calculationItem->setCalculation($invoicePositions->getCalculation());
                        $calculationItem->setStatus(1);
                        $calculationItem->setFreeText($services['textFree']);
                        $calculationItem->setFreeTax($services['taxFree']);
                        $calculationItem->setFreePriceType($services['priceFreeType']);
                        $calculationItem->setFreeText($services['textFree']);
                        $calculationItem->setExtraText($services['extraText']);
                        $singlePrice = $services['priceSingleFree'];
                        $calculationItem->setSinglePrice(number_format($singlePrice, 2, ".", ""));
                        $calculationItem->setAmount($services['amountFree']);
                        $em->persist($calculationItem);

                        if ($services['saveFree'] == '1') {
                            $serviceNew = new Service();
                            $serviceNew->setTitle($services['textFree']);
                            $serviceNew->setPrice(number_format($singlePrice, 2, ".", ""));
                            $serviceNew->setPriceType($omHelper->getServicePriceTypeByName($services['priceFreeType']));
                            $serviceNew->setTax($services['taxFree']);

                            $em->persist($serviceNew);
                        }

                        if ($services['saveFree'] == '2') {
                            $productNew = new Product();
                            $productNew->setTitle($services['textFree']);
                            $productNew->setPrice(number_format($singlePrice, 2, ".", ""));
                            $productNew->setPriceType($omHelper->getServicePriceTypeByName($services['priceFreeType']));
                            $productNew->setTax($services['taxFree']);

                            $em->persist($productNew);
                        }

                    } else {
                        $product = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:Product')->find($services['product']);
                        if ($product) {
                            $calculationItem = new CalculationItem();
                            $calculationItem->setCalculation($invoicePositions->getCalculation());
                            $calculationItem->setStatus(1);
                            $calculationItem->setProduct($product);
                            $calculationItem->setExtraText($services['extraText']);
                            if ($services['specialPriceProduct']) {
                                $services['specialPriceProduct'] = str_replace('.', '', $services['specialPriceProduct']);
                                $services['specialPriceProduct'] = str_replace(',', '.', $services['specialPriceProduct']);
                                $calculationItem->setSpecialPrice($services['specialPrice']);
                            }
                            $calculationItem->setAmount($services['amountProduct']);
                            $calculationItem->setSinglePrice($product->getPrice());
                            $em->persist($calculationItem);
                        }
                    }
                }
            }
            $items = $request->request->get('invoice_position_item');
            $deleteItems = $request->request->get('delete');
            $amount = $request->request->get('invoice_position_amount');
            $specialprice = $request->request->get('invoice_position_specialprice');
            $freeText = $request->request->get('invoice_position_freeText');
            $extraText = $request->request->get('invoice_position_extraText');

            if ($items) {
                for ($x = 0; $x < count($items); $x++) {
                    $calaculationItem = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:CalculationItem')->find($items[$x]);

                    $calaculationItem->setAmount($amount[$x]);

                    if ($specialprice[$x]) {
                        $specialprice[$x] = str_replace('.', '', $specialprice[$x]);
                        $specialprice[$x] = str_replace(',', '.', $specialprice[$x]);
                        $calaculationItem->setSpecialPrice(number_format($specialprice[$x], 2, ".", ""));
                        $calaculationItem->setSpecialPrice($specialprice[$x]);
                    }
                    if ($calaculationItem->getFreeTax()) {
                        $calaculationItem->setFreeText($freeText[$calaculationItem->getId()]);
                    }
                    $calaculationItem->setExtraText($extraText[$x]);

                }
            }
            if ($deleteItems) {
                for ($x = 0; $x < count($deleteItems); $x++) {
                    $calaculationItem = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:CalculationItem')->find($deleteItems[$x]);

                    $em->remove($calaculationItem);
                }


            }
            $price = str_replace('.', '', $request->request->get('invoice_total_price'));
            $price = str_replace(',', '.', $price);

            //GET FIRST CALC ID - ONLY KM

            if (isset($request->request->get('invoice_edit')['netto'])) {
                $calculation->setNetto(1);
            } else {
                $calculation->setNetto(0);
            }

            if (isset($request->request->get('invoice_edit')['monthlyFee'])) {
                if ($request->request->get('invoice_edit')['monthlyFee'] >= 1) $calculation->setMonthlyFee($request->request->get('invoice_edit')['monthlyFee']);
            }

            $termsOfPaymentRepo = $em->getRepository('ModulesOrderManagementBundle:TermsOfPayment');

            $termsOfPayment = $termsOfPaymentRepo->find($request->request->get('invoice_edit')['termsOfPayment']);
            $calculation->setTermsOfPayment($termsOfPayment);


            $calculation->setFinalPrice($price);

            $em->flush();

            $this->addFlash('success', '<i class="uk-icon-check"></i> Die Rechnung wurde erfolgreich aktualisiert!');


            return $this->redirectToRoute('modules_ordermanagement_invoice_details', ['invoice' => $invoice->getId()]);
        }

        return $this->render('ModulesOrderManagementBundle:Invoice:edit.html.twig', ['invoice' => $invoice,
            'calaculationItems' => $calaculationItems,
            'edit_form' => $editForm->createView(),]);
    }

    /**
     * Creates a new invoice entity from an order entity
     * @Route("/import")
     * @Method({"GET", "POST"})
     * @Security("is_granted('CREATE', 'Modules\\OrderManagementBundle\\Entity\\Invoice')")
     * @param Request $request
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function importAction(Request $request)
    {

        if ($request->getMethod() == 'POST') {
            $em = $this->getDoctrine()->getManager();
            $order = $em->getRepository('ModulesOrderManagementBundle:Orders')->findOneBy(['id' => $request->request->get('order')]);

            if ($order) {
                $invoice = new Invoice();
                $invoice->setContactAddress($order->getContactAddress());
                $invoice->setContact($order->getContact());
                $invoice->setContactPerson($order->getContactPerson());
                $invoice->setDescription($order->getDescription());
                $invoice->setProject($order->getProject());
                $invoice->setTitle($order->getTitle());
                $invoice->setStatus(0);
                $invoice->setDivision($order->getDivision());
                $em->persist($invoice);

                $order->setStatus(2);
                $em->flush();

                //SET NUMBER FOR EXPORT
                $omHelper = $this->get('mod.order_management.helper');
                $division = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:Division')->find($invoice->getDivision());
                $division->setInvoiceCount($division->getInvoiceCount() + 1);
                $em->flush();

                $getNumber = $omHelper->buildNumber($invoice, 'invoice', $division->getInvoiceCount());

                $invoice->setNumber($getNumber);
                // $order->setStatus(4);

                $em->flush();

                $invoicePosition = new InvoicePositions();
                $invoicePosition->setInvoice($invoice);
                $invoicePosition->setOrders($order);
                $invoicePosition->setCalculation($order->getCalculation());

                if ($request->request->get('month')) {
                    $order->getCalculation()->setMonthlyFee($request->request->get('month'));
                }

                $em->persist($invoicePosition);
                $em->flush();


                $this->addFlash('success', '<i class="uk-icon-check"></i>Der Auftrag wurde erfolgreich in eine Rechnung überführt.');

                return $this->redirectToRoute('modules_ordermanagement_invoice_details', ['invoice' => $invoice->getId()]);
            }

        }

        return $this->redirectToRoute('modules_ordermanagement_invoice_index');
    }

    /**
     * Edit a invoice entity.
     * @Route("/{invoice}/editajax")
     * @Method({"GET", "POST"})
     * @Security("is_granted('CREATE', 'Modules\\OrderManagementBundle\\Entity\\Invoice')")
     * @param Request $request
     * @param Invoice $invoice
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAjaxAction(Request $request, Invoice $invoice)
    {
        $em = $this->getDoctrine()->getManager();

        $contact = $request->request->get('invoice_edit');
        $contactEntity = $em->getRepository('ModulesContactBundle:Contact')->find($contact['contact']);
        $invoice->setContact($contactEntity);

        $editForm = $this->createForm(ContactEditAjaxType::class, $invoice);


        $editForm->handleRequest($request);


        return $this->render('ModulesOrderManagementBundle:Form:editajax.html.twig', ['edit_form' => $editForm->createView()]);
    }

    /**
     * Deletes a invoice entity.
     * @Route("/{invoice}/delete")
     * @Method({"GET", "DELETE"})
     * @Security("is_granted('DELETE', 'Modules\\OrderManagementBundle\\Entity\\Invoice') or is_granted('DELETE', invoice)")
     * @param Request $request
     * @param Invoice $invoice
     * @return RedirectResponse
     */
    public function deleteAction(Request $request, Invoice $invoice)
    {
        $form = $this->createDeleteForm($invoice);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($invoice);
            $em->flush();

            $this->addFlash('success', '<i class="uk-icon-check"></i> Die Rechnung wurde erfolgreich gelöscht!');
        }


        return $this->redirectToRoute('modules_ordermanagement_invoice_index');
    }


    /**
     * Creates a new invoice entity from an invoice entity
     * @Route("/new/month")
     * @Method({"GET", "POST"})
     * @Security("is_granted('CREATE', 'Modules\\OrderManagementBundle\\Entity\\Invoice')")
     * @param Request $request
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newMonthAction(Request $request)
    {

        if ($request->getMethod() == 'POST') {
            $em = $this->getDoctrine()->getManager();
            $invoice = $em->getRepository('ModulesOrderManagementBundle:Invoice')->findOneBy(['id' => $request->request->get('invoice')]);

            $calaculationFromPosiiton = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:InvoicePositions')->findOneBy(['invoice' => $invoice]);

            $calculationOld = $calaculationFromPosiiton->getCalculation();
            if ($invoice) {

                $calculation = new Calculation();
                $calculation->setMonthlyFee($request->request->get('month'));
                $calculation->setDiscountType($calculationOld->getDiscountType());
                $calculation->setDiscount($calculationOld->getDiscount());
                $calculation->setContactDiscount($calculationOld->getContactDiscount());
                $calculation->setFinalPrice($calculationOld->getFinalPrice());
                $calculation->setNetto($calculationOld->getNetto());
                $calculation->setSpecialPrice($calculationOld->getSpecialPrice());
                $calculation->setTermsOfPayment($calculationOld->getTermsOfPayment());

                $em->persist($calculation);
                $em->flush();

                $omHelper = $this->get('mod.order_management.helper');

                $calaculationItems = $omHelper->getInvoiceItems($invoice);

                if ($calaculationItems) {
                    /**
                     * @var CalculationItem $item
                     */
                    foreach ($calaculationItems as $item) {
                        $calculationItem = new CalculationItem();
                        $calculationItem->setSpecialPrice($item->getSpecialPrice());
                        $calculationItem->setStatus($item->getStatus());
                        $calculationItem->setAmount($item->getAmount());
                        $calculationItem->setCalculation($calculation);
                        $calculationItem->setDiscount($item->getDiscount());
                        $calculationItem->setProduct($item->getProduct());
                        $calculationItem->setService($item->getService());
                        $calculationItem->setExtraText($item->getExtraText());
                        $calculationItem->setFreeText($item->getFreeText());
                        $calculationItem->setFreeTax($item->getFreeTax());
                        $calculationItem->setFreePriceType($item->getFreePriceType());
                        $calculationItem->setSinglePrice($item->getSinglePrice());

                        $em->persist($calculationItem);
                        $em->flush();
                    }
                }


                $invoice_new = new Invoice();
                $invoice_new->setContactAddress($invoice->getContactAddress());
                $invoice_new->setContact($invoice->getContact());
                $invoice_new->setContactPerson($invoice->getContactPerson());
                $invoice_new->setDescription($invoice->getDescription());
                $invoice_new->setProject($invoice->getProject());
                $invoice_new->setTitle($invoice->getTitle());
                $invoice_new->setDivision($invoice->getDivision());
                $invoice_new->setStatus(0);


                $invoice_new->setDivision($invoice->getDivision());
                $em->persist($invoice_new);
                $em->flush();

                $division = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:Division')->find($invoice->getDivision());
                $division->setInvoiceCount($division->getInvoiceCount() + 1);
                $em->flush();
                $getNumber = $omHelper->buildNumber($invoice_new, 'invoice', $invoice->getDivision()->getInvoiceCount());

                $invoice_new->setNumber($getNumber);

                $invoicePosition = new InvoicePositions();
                $invoicePosition->setInvoice($invoice_new);
                $invoicePosition->setOrders(NULL);
                $invoicePosition->setCalculation($calculation);
                $em->persist($invoicePosition);
                $em->flush();

                $this->addFlash('success', '<i class="uk-icon-check"></i>Die Rechnung wurde erfolgreich angelegt.');

                return $this->redirectToRoute('modules_ordermanagement_invoice_details', ['invoice' => $invoice_new->getId()]);
            }

        }

        return $this->redirectToRoute('modules_ordermanagement_invoice_index');
    }

    /**
     * Export Invoice in Choosen File
     * @Route("/export/{direct}")
     * @Security("is_granted('VIEW', 'Modules\\OrderManagementBundle\\Entity\\Invoice')")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function exportAction(Request $request, $direct = false)
    {

        $helper = $this->get('app.filehandle');


        $place = null;

        if ($request->request->get('place')) {
            $place = $request->request->get('place');
        }
        $fileformat = 'pdf';

        if (!$direct) {
            /**
             * @var Invoice $invoice
             */
            $invoice = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:Invoice')->findOneBy(['id' => $request->request->get('invoice')]);
            $invoice->setPrintDate(new \DateTime($request->request->get('printDate')));
            $invoice->setStatus(1);
            $invoice->setPlaceholder($place);
            $this->getDoctrine()->getManager()->flush();

            if ($request->request->get('placeholderSave')) {
                $placeholder = new ExportPlaceholder();
                $placeholder->setText($request->request->get('place'));
                $this->getDoctrine()->getManager()->persist($placeholder);
                $this->getDoctrine()->getManager()->flush();
            }

            $fileformat = $request->request->get('fileformat');

        } else {
            $invoice = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:Invoice')->findOneBy(['id' => $direct]);
            $place = $invoice->getPlaceholder();
        }


        $omHelper = $this->get('mod.order_management.helper');

        $inovicePositions = $omHelper->getInvoicePositions($invoice);

        $calaculationItems = $omHelper->getInvoiceItems($invoice);


        $nettoInvoice = false;

        /**
         * @var InvoicePositions $inovicePosition
         */
        foreach ($inovicePositions as $inovicePosition) {

            if ($inovicePosition->getCalculation()->getNetto()) {
                $nettoInvoice = true;
            }
        }

        if ($nettoInvoice) {
            $document = new Document($invoice->getDivision()->getInvoiceNettoTemplate()->getPath() . $invoice->getDivision()->getInvoiceNettoTemplate()->getName());
        } else {
            $document = new Document($invoice->getDivision()->getInvoiceTemplate()->getPath() . $invoice->getDivision()->getInvoiceTemplate()->getName());
        }


        $document->NoErr = true;

        @$document->MergeField("customer", ['id' => $invoice->getContact()->getId(),
            'city' => utf8_decode($invoice->getContactAddress() ? $invoice->getContactAddress()->getCity() : ""),
            'zip' => $invoice->getContactAddress() ? $invoice->getContactAddress()->getZip() : "",
            'company' => utf8_decode($invoice->getContact() ? $invoice->getContact()->getName() : ""),
            'street' => utf8_decode($invoice->getContactAddress() ? $invoice->getContactAddress()->getStreet() : ""),
            'firstname' => utf8_decode($invoice->getContactPerson() ? $invoice->getContactPerson()->getFirstname() : ""),
            'lastname' => utf8_decode($invoice->getContactPerson() ? $invoice->getContactPerson()->getLastname() : ""),
            'salutation' => utf8_decode($invoice->getContactPerson() ? $invoice->getContactPerson()->getSalutation() : ""),
            'position' => utf8_decode($invoice->getContactPerson() ? $invoice->getContactPerson()->getPosition() : "")]);


        $count = 1;
        $priceTotal = 0;
        $price = 0;
        $amount = 0;
        $type = null;
        $singleprice = 0;
        $title = null;

        $flatrate = null;


        $list = [];

        $contact = $invoice->getContact();

        /**
         * @var CalculationItem $item
         */
        foreach ($calaculationItems as $item) {
            if ($item->getProduct()) {

                $singleprice = $item->getSinglePrice();

                $amount = $item->getAmount();
                $title = $item->getProduct()->getTitle();
                $description = $item->getProduct()->getDescription();
                $price = $singleprice * $amount;
                $type = $item->getProduct()->getPriceTypeName();
            }
            if ($item->getService()) {

                $singleprice = $item->getSinglePrice();

                $amount = $item->getAmount();
                $title = $item->getService()->getTitle();
                $description = $item->getService()->getDescription();
                $type = $item->getService()->getPriceTypeName();
                $price = $singleprice * $amount;
            }
            if ($item->getFreeText()) {
                $amount = $item->getAmount();
                $singleprice = $item->getSinglePrice();
                $title = $item->getFreeText();
                $type = $item->getFreePriceType();
                $price = $singleprice * $amount;
                $description = '';
            }

            if ($item->getSpecialPrice()) {
                $singleprice = $item->getSpecialPrice() / $amount;
                $price = $item->getSpecialPrice();
            }

            $priceTotal = $priceTotal + $price;

            if ($item->getExtraText()) {
                $title = $title . ' ( ' . $item->getExtraText() . ' )';
            }

            if ($count == 25) {
                $list[] = ['title' => '',
                    'description' => '',
                    'amount' => '',
                    'pos' => '',
                    'type' => '',
                    'singleprice' => '',
                    'sum' => '',];
            }

            $list[] = ['title' => utf8_decode($title),
                'description' => utf8_decode(strip_tags($description)),
                'amount' => number_format($amount, 2, ',', '.') . ' ' . utf8_decode($type),
                'pos' => $count,
                'type' => utf8_decode($type),
                'singleprice' => number_format($singleprice, 2, ',', '.'),
                'sum' => number_format($price, 2, ',', '.')];

            $count++;
        }
        //Fill the table with products and services
        $document->MergeBlock("ps", $list);
        $netto = $priceTotal * 0.81;
        $discount = 0;

        $invoicePosition = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:InvoicePositions')->findOneBy(['invoice' => $invoice]);


        if ($omHelper->invoiceDiscountCheck($invoice)) {
            $discount = ($priceTotal * $invoice->getContact()->getDiscount()) / 100;
        }

        /**
         * @var InvoicePositions $inovicePosition
         */
        foreach ($inovicePositions as $inovicePosition) {

            if ($inovicePosition->getCalculation()->getDiscount()) {
                if ($inovicePosition->getCalculation()->getDiscountType()) {
                    $discount = $discount + ($priceTotal * $inovicePosition->getCalculation()->getDiscount()) / 100;
                } else {
                    $discount = $discount + $inovicePosition->getCalculation()->getDiscount();
                }

            }
        }

        $steuer = ($priceTotal - $discount) * 0.19;
        if ($nettoInvoice) {
            $steuer = 0;
        }


        $user = $invoice->getCreatedBy();
        $member = $user->getMember() ?: $user;

        if ($member instanceof User) {
            $name = $member->getUsername();
        } else {
            $name = $member->getFullName();
        }

        $document->MergeField("team", ['fullname' => utf8_decode($name)]);

        if (!$omHelper->isNetto($invoice)) {
            $total = $omHelper->getInvoicePrice($invoice);
        } else {
            $total = $priceTotal - $discount;
        }

        if ($invoice->getPrintDate()) {
            $printDate = $invoice->getPrintDate()->format('d.m.Y');
        } else {
            $printDate = $invoice->getCreatedAt()->format('d.m.Y');
        }

        $placeHolderBank = 'Wir bitten Sie freundlich um Anweisung des sich ergebenden Gesamtbetrages in Höhe von ' . number_format($total, 2, ',', '.') . ' Euro innerhalb von 7 Tagen, ohne Abzug, auf das unten aufgeführte Konto.';

        if ($omHelper->isDebit($invoice) == 'Lastschrift') {
            $placeHolderBank = 'Der Gesamtbetrag in Höhe von  ' . number_format($total, 2, ',', '.') . ' Euro wird in den nächsten Tagen von Ihrem Konto eingezogen.';
        }
        
       

        $document->MergeField("offer", ['printdate' => "\t" . $printDate,
            'id' => $invoice->getNumber(),
            'address' => ['name' => utf8_decode($invoice->getContactAddress() ? $invoice->getContactAddress()->getName() : ""),
                'city' => utf8_decode($invoice->getContactAddress() ? $invoice->getContactAddress()->getCity() : ""),],
            'subtotal' => $priceTotal,
            'mwst' => $steuer,
            'discount' => $discount,
            'total' => $total,
            'place' => utf8_decode($place),
            'bank' =>  utf8_decode($placeHolderBank),
            'psch' => $flatrate]);

        $outputFile = $helper->getCompiledPath($invoice->getId(), 'Modules\OrderManagementBundle\Entity\Invoice');


        $document->Show(OPENTBS_FILE, $outputFile);

        $docConverter = $this->get('app.documentconverter');

        $convertedPath = $helper->getConvertedPath($invoice->getId(), $invoice->getNumber(), 'Modules\OrderManagementBundle\Entity\Invoice', $fileformat);
        $docConverter->convertTo($outputFile, $convertedPath, $fileformat);


        $response = new BinaryFileResponse($convertedPath);
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);

        return $response;

    }
}