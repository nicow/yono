<?php

namespace Modules\OrderManagementBundle\Controller;

use Modules\OrderManagementBundle\Entity\PriceList;
use Modules\OrderManagementBundle\Entity\PriceListService;
use Modules\OrderManagementBundle\Form\PriceListServiceType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


/**
 * Service controller.
 * @Route("/pricelist/service")
 */
class PriceListServiceController extends Controller
{

    /**
     * Creates a new PriceListService entity.
     * @Route("/new/{priceList}")
     * @Method({"GET", "POST"})
     * @Security("is_granted('CREATE', 'Modules\\OrderManagementBundle\\Entity\\PriceList')")
     * @param Request $request
     * @param PriceList $priceList
     * @return RedirectResponse|Response
     */
    public function newAction(Request $request, PriceList $priceList)
    {
        $service = new PriceListService();
        $service->setPricelist($priceList);

        $form = $this->createForm(PriceListServiceType::class, $service);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $price=$request->request->get('pricelistservice');
            $price['price']=str_replace('.','',$price['price']);
            $price['price']=str_replace(',','.',$price['price']);
            $priceSet=number_format($price['price'], 2, ".", "");
            $service->setPrice($priceSet);

            $em = $this->getDoctrine()->getManager();
            $em->persist($service);
            $em->flush();

            $this->addFlash('success', '<i class="uk-icon-check"></i> Die Leistung wurde erfolgreich der Preisliste hinzugefügt!');

            return $this->redirectToRoute('modules_ordermanagement_pricelist_details', ['pricelist' => $priceList->getId()]);

        }

        return $this->render('ModulesOrderManagementBundle:PriceListService:new.html.twig', ['pricelistservice' => $service, 'pricelist' => $priceList, 'form' => $form->createView(),]);
    }

    /**
     * Edits a pricelist entity.
     * @Route("/{priceListService}/edit")
     * @Method({"GET", "POST"})
     * @Security("is_granted('EDIT', 'Modules\\OrderManagementBundle\\Entity\\PriceList')")
     * @param Request $request
     * @param PriceListService $priceListService
     * @return RedirectResponse|Response
     */
    public function editAction(Request $request, PriceListService $priceListService)
    {
        $form = $this->createForm(PriceListServiceType::class, $priceListService);
        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted()) {

            $this->getDoctrine()->getManager()->flush();

            return new RedirectResponse($this->generateUrl('modules_ordermanagement_pricelist_details', ['pricelist' => $priceListService->getPricelist()->getId()]));
        }

        return $this->render('ModulesOrderManagementBundle:PriceListService:edit.html.twig', ['pricelistservice' => $priceListService, 'form' => $form->createView()]);
    }

    /**
     * Deletes a PriceList entity.
     * @Route("/{priceListService}/delete")
     * @Method({"GET", "DELETE"})
     * @Security("is_granted('DELETE', 'Modules\\OrderManagementBundle\\Entity\\PriceList')")
     * @param Request $request
     * @param PriceListService $priceListService
     * @return RedirectResponse
     */
    public function deleteAction(Request $request, PriceListService $priceListService)
    {
        $form = $this->createDeleteForm($priceListService);
        $id = $priceListService->getPricelist()->getId();
        $form->handleRequest($request);

        $em = $this->getDoctrine()->getManager();

        $em->remove($priceListService);
        $em->flush();

        $this->addFlash('success', '<i class="uk-icon-check"></i> Die Leistung wurde erfolgreich aus der Preisliste gelöscht!');


        return $this->redirectToRoute('modules_ordermanagement_pricelist_details', ['pricelist' => $id]);
    }

    /**
     * Creates a form to delete a PriceListService entity.
     * @param PriceListService $priceListService
     * @return Form The form
     */
    private function createDeleteForm(PriceListService $priceListService)
    {
        return $this->createFormBuilder()->setAction($this->generateUrl('modules_ordermanagement_pricelistservice_delete', ['priceListService' => $priceListService->getId()]))->setMethod('DELETE')->getForm();
    }
}
