<?php

namespace Modules\OrderManagementBundle\Controller;

use Modules\OrderManagementBundle\Entity\PriceList;
use Modules\OrderManagementBundle\Entity\PriceListService;
use Modules\OrderManagementBundle\Entity\Service;
use Modules\OrderManagementBundle\Entity\ServiceCategory;
use Modules\OrderManagementBundle\Form\ServiceType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


/**
 * Service controller.
 * @Route("/service")
 */
class ServiceController extends Controller
{
    /**
     * Lists all service entities.
     * @Route("/")
     * @Method({"GET", "POST"})
     * @Security("is_granted('VIEW', 'Modules\\OrderManagementBundle\\Entity\\Service')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $services = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:Service')->findAllOrdered($request->query->get('sort', 'title'), $request->query->get('direction', 'ASC'));

        $columns = $this->get('app.helper')->setTableColumns('mod_order_management_service_table_columns', ['title' => ['active' => 1, 'label' => 'Name'], 'price' => ['active' => 1, 'label' => 'Preis'], 'categories' => ['active' => 0, 'label' => 'Kategorien'], 'createdBy' => ['active' => 0, 'label' => 'erstellt von'], 'createdAt' => ['active' => 0, 'label' => 'erstellt am'], 'updatedAt' => ['active' => 0, 'label' => 'letzte Änderung'],]);
        $perPage = $this->get('app.helper')->setTablePerPage('mod_order_management_service_table_rows');
        $paginator = $this->get('knp_paginator');
        $page = $request->query->getInt('page', 1);
        $pagination = $paginator->paginate($services, $page, $perPage);

        $deleteForms = [];
        foreach ($services as $service) {
            $deleteForms[$service->getId()] = $this->createDeleteForm($service)->createView();
        }

        return $this->render('ModulesOrderManagementBundle:Service:index.html.twig', ['columns' => $columns, 'pagination' => $pagination, 'perPage' => $perPage, 'deleteForms' => $deleteForms,]);
    }

    /**
     * Creates a form to delete a service entity.
     * @param Service $service The member entity
     * @return Form The form
     */
    private function createDeleteForm(Service $service)
    {
        return $this->createFormBuilder()->setAction($this->generateUrl('modules_ordermanagement_service_delete', ['service' => $service->getId()]))->setMethod('DELETE')->getForm();
    }

    /**
     * @Route("/{service}/details")
     * @Security("is_granted('VIEW', 'Modules\\OrderManagementBundle\\Entity\\Service')")
     * @param Service $service
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function detailsAction(Request $request, Service $service)
    {

        return $this->render('ModulesOrderManagementBundle:Service:details.html.twig', ['service' => $service,]);
    }

    /**
     * @Route("/data")
     * @Security("is_granted('VIEW', 'Modules\\OrderManagementBundle\\Entity\\Service')")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function dataAction(Request $request)
    {
        $data = [];

        $servicePriceList = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:PriceList');
        /**
         * @var PriceList $list
         */
        $list = $servicePriceList->findByService($request->request->get('contact'), $request->request->get('service'));

        /**
         * @var PriceListService $item
         */
        if ($list) {
            foreach ($list->getServices() as $item) {
                if ($item->getService()->getId() == $request->request->get('service')) {
                    $data['price'] = $item->getPrice();
                }
            }
        }
        $serviceEnity = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:Service')->find($request->request->get('service'));

        if (!isset($data['price'])) {
            $data['price'] = $serviceEnity->getPrice();
        }

        $data['brutto'] = $serviceEnity->getTax();

        return new JsonResponse(json_encode($data));
    }

    /**
     * Creates a new service entity.
     * @Route("/new")
     * @Route("/category/{category}/new")
     * @Method({"GET", "POST"})
     * @Security("is_granted('CREATE', 'Modules\\OrderManagementBundle\\Entity\\Service')")
     * @param ServiceCategory $category
     * @param Request $request
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request, ServiceCategory $category = null)
    {
        $service = new Service();
        if ($category) {
            $service->addCategory($category);
        }
        $form = $this->createForm(ServiceType::class, $service);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $price=$request->request->get('service');
            $price['price']=str_replace('.','',$price['price']);
            $price['price']=str_replace(',','.',$price['price']);
            $priceSet=number_format($price['price'], 2, ".", "");
            $service->setPrice($priceSet);
            $em = $this->getDoctrine()->getManager();
            $em->persist($service);
            $em->flush();

            $this->addFlash('success', '<i class="uk-icon-check"></i> Die Leistung wurde erfolgreich angelegt!');


            return $this->redirectToRoute('modules_ordermanagement_service_details', ['service' => $service->getId()]);

        }

        return $this->render('ModulesOrderManagementBundle:Service:new.html.twig', ['category' => $category, 'service' => $service, 'form' => $form->createView(),]);
    }

    /**
     * Edits a service entity.
     * @Route("/edit/{service}")
     * @Method({"GET", "POST"})
     * @Security("is_granted('EDIT', 'Modules\\OrderManagementBundle\\Entity\\Service')")
     * @param Request $request
     * @param Service $service
     * @return RedirectResponse|Response
     */
    public function editAction(Request $request, Service $service)
    {
        $form = $this->createForm(ServiceType::class, $service);
        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted()) {

            $price=$request->request->get('service');
            $price['price']=str_replace('.','',$price['price']);
            $price['price']=str_replace(',','.',$price['price']);
            $priceSet=number_format($price['price'], 2, ".", "");
            $service->setPrice($priceSet);

            $this->getDoctrine()->getManager()->flush();

            return new RedirectResponse($this->generateUrl('modules_ordermanagement_service_details', ['service' => $service->getId()]));
        }

        return $this->render('ModulesOrderManagementBundle:Service:edit.html.twig', ['service' => $service, 'form' => $form->createView()]);
    }

    /**
     * Deletes a Service entity.
     * @Route("/{service}/delete")
     * @Method({"GET", "DELETE"})
     * @Security("is_granted('DELETE', 'Modules\\OrderManagementBundle\\Entity\\Service') or is_granted('DELETE', service)")
     * @param Request $request
     * @param Service $service
     * @return RedirectResponse
     */
    public function deleteAction(Request $request, Service $service)
    {
        $form = $this->createDeleteForm($service);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($service);
            $em->flush();

            $this->addFlash('success', '<i class="uk-icon-check"></i> Die Leistung wurde erfolgreich gelöscht!');
        }


        return $this->redirectToRoute('modules_ordermanagement_service_index');
    }
}
