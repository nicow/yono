<?php

namespace Modules\OrderManagementBundle\Controller;

use Modules\OrderManagementBundle\Entity\Document;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class DocumentController
 * @package Modules\OrderManagementBundle\Controller
 * @Route("/document")
 */
class DocumentController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {

        $document = new Document(Document::getDocument('test.odt'));
        $document->MergeField("client", ['name' => "herman"]);
        $outputFile = Document::getRenderedPath($document->getTemplate());
        $document->Show(OPENTBS_FILE, $outputFile);

        $docConverter = $this->get('app.documentconverter');
        $convertedPath = Document::getConvertPath(basename($outputFile));
        $docConverter->convertTo($outputFile, $convertedPath, 'pdf');

        dump("Dein konvertiertes Dokument: " . basename($convertedPath));


        return $this->render('ModulesOrderManagementBundle:Document:index.html.twig');
    }


}
