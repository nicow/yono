<?php
/**
 * Created by PhpStorm.
 * User: Nico
 * Date: 01.06.2017
 * Time: 09:46
 */

namespace Modules\OrderManagementBundle\Controller;

use Modules\OrderManagementBundle\Form\ExportType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ExportController
 * @package Modules\OrderManagementBundle\Controller
 * @Route("/export")
 */
class ExportController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/")
     */
    public function indexAction()
    {

        $form = $this->createForm(ExportType::class, ['action' => $this->generateUrl('modules_ordermanagement_export_export')]);

        return $this->render('ModulesOrderManagementBundle:Export:index.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @Route("/run")
     */
    public function exportAction(Request $request)
    {
        $export = $request->request->get('export');
        $em = $this->getDoctrine()->getManager();

        if (isset($export['bills'])) {
            $bills = true;
        } else {
            $bills = false;
        }

        if (isset($export['orders'])) {
            $orders = true;
        } else {
            $orders = false;
        }

        if (isset($export['offers'])) {
            $offers = true;
        } else {
            $offers = false;
        }

        if (!$orders && !$bills && !$offers) {
            return $this->redirectToRoute('modules_ordermanagement_export_index');
        }

        if ($export['customer']) {
            $customer = $em->getRepository('ModulesContactBundle:Contact')->find($export['customer']);
        } else {
            $customer = null;
        }

        $division = $em->getRepository('ModulesOrderManagementBundle:Division')->find($export['division']);

        $start = new \DateTime($export['start']);

        if ($export['end']) {
            $end = new \DateTime($export['end']);
        } else {
            $end = new \DateTime();
        }

        $csv = '';

        $billRepo = $em->getRepository('ModulesOrderManagementBundle:Invoice');
        $offerRepo = $em->getRepository('ModulesOrderManagementBundle:Offer');
        $orderRepo = $em->getRepository('ModulesOrderManagementBundle:Orders');
        $omHelper = $this->get('mod.order_management.helper');

        if ($bills) {
            if ($offers || $orders) $csv .= $this->getCSVLine(['Rechnungen', '', '', '']);
            $csv .= $this->getCSVLine(['Umsatz', 'Gegenkonto', 'Rechnungsnummer', 'Datum', 'Erlöskonto', 'Text']);
            $invoices = $billRepo->exportFilter($customer, $division, $start, $end);

            if (count($invoices) > 0) {
                /**
                 * @var \Modules\OrderManagementBundle\Entity\Invoice $invoice
                 */
                foreach ($invoices as $invoice) {
                    $brutto = $omHelper->getInvoicePrice($invoice);
                    if ($invoice->getPrintDate()) {
                        $date = $invoice->getPrintDate()->format('dmy');
                    } else {
                        $date = $invoice->getCreatedAt()->format('dmy');
                    }

                    $csv .= $this->getCSVLine([number_format($brutto, 2, ',', ''),
                        $invoice->getContact()->getId(),
                        $invoice->getNumber(),
                        $date,
                        $this->getParameter('banknumber'),
                        $invoice->getContact()->getName()]);
                }
            }
        }
        if ($offers) {
            if ($bills || $orders) $csv .= $this->getCSVLine(['Angebote', '', '', '']);
            $csv .= $this->getCSVLine(['Kundennummer', 'Brutto', 'Netto', 'Angebotsnummer']);
            $offers = $offerRepo->exportFilter($customer, $division, $start, $end);
            if (count($offers) > 0) {
                /**
                 * @var \Modules\OrderManagementBundle\Entity\Offer $offer
                 */
                foreach ($offers as $offer) {
                    $brutto = $offer->getCalculation()->getFinalPrice();
                    if ($offer->getPrintDate()) {
                        $date = $offer->getPrintDate()->format('dmy');
                    } else {
                        $date = $offer->getCreatedAt()->format('dmy');
                    }

                    $csv .= $this->getCSVLine([number_format($brutto, 2, ',', ''),
                        $offer->getContact()->getId(),
                        $offer->getNumber(),
                        $date,
                        $this->getParameter('banknumber'),
                        $offer->getContact()->getName()]);
                }
            }
        }

        if ($orders) {
            if ($bills || $offers) $csv .= $this->getCSVLine([utf8_decode('Aufträge'), '', '', '']);
            $csv .= $this->getCSVLine(['Kundennummer', 'Brutto', 'Netto', 'Auftragsnummer']);
            $orders = $orderRepo->exportFilter($customer, $division, $start, $end);
            if (count($orders) > 0) {
                /**
                 * @var \Modules\OrderManagementBundle\Entity\Offer $order
                 */
                foreach ($orders as $order) {

                    $brutto = $order->getCalculation()->getFinalPrice();

                    if ($order->getPrintDate()) {
                        $date = $order->getPrintDate()->format('dmy');
                    } else {
                        $date = $order->getCreatedAt()->format('dmy');
                    }

                    $csv .= $this->getCSVLine([number_format($brutto, 2, ',', ''),
                        $order->getContact()->getId(),
                        $order->getNumber(),
                        $date,
                        $this->getParameter('banknumber'),
                        $order->getContact()->getName()]);
                }
            }
        }


        $response = new Response();
        $response->setStatusCode(200);
        $response->setContent($csv);
        $response->headers->set('Content-Type', 'text/csv; charset=utf-8');
        $response->headers->set('Content-Disposition', 'attachment; filename="export.csv"');

        return $response;

    }

    private function getCSVLine(array $data, $glue = ';')
    {
        return implode($glue, $data) . PHP_EOL;
    }
}

