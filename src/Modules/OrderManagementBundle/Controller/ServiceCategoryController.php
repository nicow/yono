<?php

namespace Modules\OrderManagementBundle\Controller;

use Modules\OrderManagementBundle\Entity\ServiceCategory;
use Modules\OrderManagementBundle\Form\ServiceCategoryType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Category controller.
 * @Route("/service/category")
 */
class ServiceCategoryController extends Controller
{
    /**
     * Lists all ServiceCategory entities.
     * @Route("/")
     * @Security("is_granted('VIEW', 'Modules\\OrderManagementBundle\\Entity\\ServiceCategory')")
     * @Method("GET")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $categories = $em->getRepository('ModulesOrderManagementBundle:ServiceCategory')->findAll();

        // get delete forms
        $deleteForms = [];
        foreach ($categories as $category) {
            $deleteForms[$category->getId()] = $this->createDeleteForm($category)->createView();
        }

        return $this->render('ModulesOrderManagementBundle:ServiceCategory:index.html.twig', ['categories' => $categories, 'deleteForms' => $deleteForms]);
    }

    /**
     * Creates a form to delete a category entity.
     * @param ServiceCategory $category The category entity
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(ServiceCategory $category)
    {
        return $this->createFormBuilder()->setAction($this->generateUrl('modules_ordermanagement_servicecategory_delete', ['id' => $category->getId()]))->setMethod('DELETE')->getForm();
    }

    /**
     * Creates a new ServiceCategory entity.
     * @Route("/new")
     * @Security("is_granted('VIEW', 'Modules\\OrderManagementBundle\\Entity\\ServiceCategory')")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $category = new ServiceCategory();
        $category->setShowInMenu(true);
        $form = $this->createForm(ServiceCategoryType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();

            $this->addFlash('success', '<i class="uk-icon-check"></i> Die Kategorie wurde erfolgreich angelegt!');

            return $this->redirectToRoute('modules_ordermanagement_servicecategory_index');
        }

        return $this->render('ModulesOrderManagementBundle:ServiceCategory:new.html.twig', ['category' => $category, 'form' => $form->createView(),]);
    }

    /**
     * Displays a form to edit an existing category entity.
     * @Route("/{id}/edit")
     * @Security("is_granted('EDIT', category)")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param ServiceCategory $category
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @ParamConverter("id", class="Modules\OrderManagementBundle\Entity\ServiceCategory")
     */
    public function editAction(Request $request, ServiceCategory $category)
    {
        $editForm = $this->createForm(ServiceCategoryType::class, $category);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('modules_ordermanagement_servicecategory_index');
        }

        return $this->render('ModulesOrderManagementBundle:ServiceCategory:edit.html.twig', ['category' => $category, 'form' => $editForm->createView(),]);
    }

    /**
     * Deletes a category entity.
     * @Route("/{id}")
     * @Security("is_granted('DELETE', category)")
     * @Method("DELETE")
     * @param Request $request
     * @param ServiceCategory $category
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, ServiceCategory $category)
    {
        $form = $this->createDeleteForm($category);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $items = $em->getRepository('ModulesOrderManagementBundle:Service')->findByCategory($category);

            if ($items) {
                $this->addFlash('error', '<i class="uk-icon-exclamation"></i> Sie können nur leere Kategorien löschen! Bitte entfernen Sie zunächst alle Produkte aus der Kategorie.');

                return $this->redirectToRoute("modules_ordermanagement_servicecategory_index");
            } else {
                $em->remove($category);
                $em->flush();
            }
        }

        return $this->redirectToRoute('modules_ordermanagement_servicecategory_index');
    }
}
