<?php

namespace Modules\OrderManagementBundle\Controller;

use Modules\OrderManagementBundle\Entity\ProductCategory;
use Modules\OrderManagementBundle\Form\ProductCategoryType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Category controller.
 * @Route("/product/category")
 */
class ProductCategoryController extends Controller
{
    /**
     * Lists all ProductCategory entities.
     * @Route("/")
     * @Security("is_granted('VIEW', 'Modules\\OrderManagementBundle\\Entity\\ProductCategory')")
     * @Method("GET")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $categories = $em->getRepository('ModulesOrderManagementBundle:ProductCategory')->findAll();

        // get delete forms
        $deleteForms = [];
        foreach ($categories as $category) {
            $deleteForms[$category->getId()] = $this->createDeleteForm($category)->createView();
        }

        return $this->render('ModulesOrderManagementBundle:ProductCategory:index.html.twig', ['categories' => $categories, 'deleteForms' => $deleteForms]);
    }

    /**
     * Creates a form to delete a category entity.
     * @param ProductCategory $category The category entity
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(ProductCategory $category)
    {
        return $this->createFormBuilder()->setAction($this->generateUrl('modules_ordermanagement_productcategory_delete', ['id' => $category->getId()]))->setMethod('DELETE')->getForm();
    }

    /**
     * Creates a new ProductCategory entity.
     * @Route("/new")
     * @Security("is_granted('VIEW', 'Modules\\OrderManagementBundle\\Entity\\ProductCategory')")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $category = new ProductCategory();
        $category->setShowInMenu(true);
        $form = $this->createForm(ProductCategoryType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();

            $this->addFlash('success', '<i class="uk-icon-check"></i> Die Kategorie wurde erfolgreich angelegt!');

            return $this->redirectToRoute('modules_ordermanagement_productcategory_index');
        }

        return $this->render('ModulesOrderManagementBundle:ProductCategory:new.html.twig', ['category' => $category, 'form' => $form->createView(),]);
    }

    /**
     * Displays a form to edit an existing category entity.
     * @Route("/{id}/edit")
     * @Security("is_granted('EDIT', category)")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param ProductCategory $category
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @ParamConverter("id", class="Modules\OrderManagementBundle\Entity\ProductCategory")
     */
    public function editAction(Request $request, ProductCategory $category)
    {
        $editForm = $this->createForm(ProductCategoryType::class, $category);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('modules_ordermanagement_productcategory_index');
        }

        return $this->render('ModulesOrderManagementBundle:ProductCategory:edit.html.twig', ['category' => $category, 'form' => $editForm->createView(),]);
    }

    /**
     * Deletes a category entity.
     * @Route("/{id}")
     * @Security("is_granted('DELETE', category)")
     * @Method("DELETE")
     * @param Request $request
     * @param ProductCategory $category
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, ProductCategory $category)
    {
        $form = $this->createDeleteForm($category);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $items = $em->getRepository('ModulesOrderManagementBundle:Product')->findByCategory($category);

            if ($items) {
                $this->addFlash('error', '<i class="uk-icon-exclamation"></i> Sie können nur leere Kategorien löschen! Bitte entfernen Sie zunächst alle Produkte aus der Kategorie.');

                return $this->redirectToRoute("modules_ordermanagement_productcategory_index");
            } else {
                $em->remove($category);
                $em->flush();
            }
        }

        return $this->redirectToRoute('modules_ordermanagement_productcategory_index');
    }
}
