<?php
/**
 * Created by PhpStorm.
 * User: tim
 * Date: 09.05.17
 * Time: 12:04
 */

namespace Modules\OrderManagementBundle\Controller;

use Modules\OrderManagementBundle\Entity\JobTicket;
use Modules\OrderManagementBundle\Entity\JobTicketNote;
use Modules\OrderManagementBundle\Form\JobTicketType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;


/**
 * JobTicket controller.
 * @Route("/ticket")
 */
class JobTicketController extends Controller
{
    /**
     * Lists all tickets entities.
     * @Route("/")
     * @Method({"GET", "POST"})
     * @Security("is_granted('VIEW', 'Modules\\OrderManagementBundle\\Entity\\JobTicket')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {

        if ($request->query->all()) {
            $this->get('app.settings')->set('mod_order_management_jobticket_overview', $request->query->all());
        }

        $filter = $this->get('app.settings')->get('mod_order_management_jobticket_overview');

        $jobtickets = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:JobTicket')->findAllOrdered($request->query->get('sort', 'title'), $request->query->get('direction', 'ASC'), $filter);

        $columns = $this->get('app.helper')->setTableColumns('mod_order_management_jobTicket_table_columns', ['title' => ['active' => 1,
            'label' => 'Titel'],
            'order' => ['active' => 1, 'label' => 'Auftrag'],
            'members' => ['active' => 1, 'label' => 'Mitarbeiter'],
            'status' => ['active' => 1, 'label' => 'Status'],
            'createdBy' => ['active' => 0, 'label' => 'erstellt von'],
            'createdAt' => ['active' => 0, 'label' => 'erstellt am'],
            'updatedAt' => ['active' => 0, 'label' => 'letzte Änderung'],]);

        $perPage = $this->get('app.helper')->setTablePerPage('mod_order_management_jobTicket_table_rows');
        $paginator = $this->get('knp_paginator');
        $page = $request->query->getInt('page', 1);
        $pagination = $paginator->paginate($jobtickets, $page, $perPage);

        $deleteForms = [];
        foreach ($jobtickets as $jobticket) {
            $deleteForms[$jobticket->getId()] = $this->createDeleteForm($jobticket)->createView();
        }

        $divisions = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:Division')->findAll();

        return $this->render('ModulesOrderManagementBundle:JobTicket:index.html.twig', ['columns' => $columns,
            'divisions' => $divisions,
            'pagination' => $pagination,
            'perPage' => $perPage,
            'filterSettings' => $filter,
            'deleteForms' => $deleteForms,]);
    }

    /**
     * Creates a form to delete a product entity.
     * @param JobTicket $jobTicket
     * @return Form The form
     */
    private function createDeleteForm(JobTicket $jobTicket)
    {
        return $this->createFormBuilder()->setAction($this->generateUrl('modules_ordermanagement_jobticket_delete', ['jobTicket' => $jobTicket->getId()]))->setMethod('DELETE')->getForm();
    }

    /**
     * Creates a new JobTicket entity.
     * @Route("/new/{order}/{todo}/{event}", defaults={"order": null,"todo": null,"event": null})
     * @Method({"GET", "POST"})
     * @Security("is_granted('CREATE', 'Modules\\OrderManagementBundle\\Entity\\JobTicket')")
     * @param Request $request
     * @param         $order
     * @param         $todo
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request, $order, $todo = null, $event = null)
    {
        $em = $this->getDoctrine()->getManager();

        $jobTicket = new JobTicket();

        if ($order) {
            $order = $em->getRepository('ModulesOrderManagementBundle:Orders')->find($order);
            $jobTicket->setOrder($order);
        }

        if ($todo) {
            $todo = $em->getRepository('ModulesToDoBundle:ToDo')->find($todo);
            $jobTicket->setTitle($todo->getTitle());
        }

        if ($event) {
            $event = $em->getRepository('ModulesCalendarBundle:Event')->find($event);
            $jobTicket->setTitle($event->getName());
            $jobTicket->setMembers($event->getMembers());

        }

        $form = $this->createForm(JobTicketType::class, $jobTicket, ['order' => $order]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em->persist($jobTicket);
            $em->flush();


            $this->addFlash('success', '<i class="uk-icon-check"></i> Das Jobticket wurde erfolgreich angelegt!');

            if ($request->request->get('event')) {
                $event = $em->getRepository('ModulesCalendarBundle:Event')->find($request->request->get('event'));

                if ($event->getSeries()) {
                    $events = $em->getRepository('ModulesCalendarBundle:Event')->findBy(['series' => $event->getSeries()]);

                    if ($events) {
                        foreach ($events as $event) {
                            $event->setJobticket($jobTicket);
                            $jobTicket->addEvent($event);
                        }
                    }
                } else {
                    $event->setJobticket($jobTicket);
                    $jobTicket->addEvent($event);
                }

                $em->flush();

                return $this->redirectToRoute('modules_calendar_default_index');
            } elseif ($request->request->get('todo')) {
                $todo = $em->getRepository('ModulesToDoBundle:ToDo')->find($request->request->get('todo'));
                $todo->setJobticket($jobTicket);
                $em->flush();

                return $this->redirectToRoute('modules_todo_default_details', ['todo' => $todo->getId()]);
            }

            return $this->redirectToRoute('modules_ordermanagement_jobticket_details', ['jobTicket' => $jobTicket->getId()]);

        }

        return $this->render('ModulesOrderManagementBundle:JobTicket:new.html.twig', ['jobTicket' => $jobTicket,
            'event' => $event,
            'todo' => $todo,
            'form' => $form->createView()]);
    }

    /**
     * Creates a new JobTicket entity. AJAX
     * @Route("/newajax")
     * @Method({"GET", "POST"})
     * @Security("is_granted('CREATE', 'Modules\\OrderManagementBundle\\Entity\\JobTicket')")
     * @param Request $request
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAjaxAction(Request $request)
    {
        $jobTicket = new JobTicket();

        $form = $this->createForm(JobTicketType::class, $jobTicket);
        $form->handleRequest($request);

        return $this->render('ModulesOrderManagementBundle:JobTicket:new.html.twig', ['jobTicket' => $jobTicket,
            'form' => $form->createView(),]);
    }

    /**
     * @Route("/{jobTicket}/details")
     * @Security("is_granted('VIEW', 'Modules\\OrderManagementBundle\\Entity\\JobTicket')")
     * @param JobTicket $jobTicket
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function detailsAction(Request $request, JobTicket $jobTicket)
    {

        $notes = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:JobTicketNote')->findBy(['jobticket' => $jobTicket]);

        $columns = $this->get('app.helper')->setTableColumns('mod_order_management_jobTicketNote_table_columns', ['createdBy' => ['active' => 1,
            'label' => 'Ersteller'],
            'text' => ['active' => 1, 'label' => 'text'],
            'createdAt' => ['active' => 1, 'label' => 'Datum'],
            'status' => ['active' => 1, 'label' => 'Status'],]);

        $perPage = $this->get('app.helper')->setTablePerPage('mod_order_management_jobTicketNote_table_rows');
        $paginator = $this->get('knp_paginator');
        $page = $request->query->getInt('page', 1);
        $pagination = $paginator->paginate($notes, $page, $perPage);


        return $this->render('ModulesOrderManagementBundle:JobTicket:details.html.twig', ['jobTicket' => $jobTicket,
            'columns' => $columns,
            'pagination' => $pagination,
            'perPage' => $perPage,]);
    }

    /**
     * Creates a new JobTicketNote entity
     * @Route("/{jobTicket}/note")
     * @Method({"GET", "POST"})
     * @Security("is_granted('CREATE', 'Modules\\OrderManagementBundle\\Entity\\JobTicket')")
     * @param Request $request
     * @param JobTicket $jobTicket
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function noteAction(Request $request, JobTicket $jobTicket)
    {
        if ($request->getMethod() == 'POST') {
            $em = $this->getDoctrine()->getManager();

            $note = new JobTicketNote();
            $note->setJobticket($jobTicket);
            $note->setText($request->request->get('text'));
            $em->persist($note);
            $em->flush();
        }

        return $this->redirectToRoute('modules_ordermanagement_jobticket_details', ['jobTicket' => $jobTicket->getId()]);
    }

    /**
     * Delete a new JobTicketNote entity
     * @Route("/{jobTicketNote}/delete")
     * @Method({"GET", "POST"})
     * @Security("is_granted('CREATE', 'Modules\\OrderManagementBundle\\Entity\\JobTicket')")
     * @param JobTicketNote $jobTicketNote
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function noteDeleteAction(JobTicketNote $jobTicketNote)
    {
        $jobTicket = $jobTicketNote->getJobticket();
        $em = $this->getDoctrine()->getManager();
        $em->remove($jobTicketNote);
        $em->flush();

        $this->addFlash('success', '<i class="uk-icon-check"></i> Die Notiz wurde erfolgreich gelöscht!');

        return $this->redirectToRoute('modules_ordermanagement_jobticket_details', ['jobTicket' => $jobTicket->getId()]);
    }

    /**
     * Edits a jobTicket entity.
     * @Route("/{jobTicket}/edit")
     * @Method({"GET", "POST"})
     * @Security("is_granted('EDIT', 'Modules\\OrderManagementBundle\\Entity\\JobTicket')")
     * @param Request $request
     * @param JobTicket $jobTicket
     * @return RedirectResponse|Response
     */
    public function editAction(Request $request, JobTicket $jobTicket)
    {
        $form = $this->createForm(JobTicketType::class, $jobTicket);
        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted()) {
            $em = $this->getDoctrine()->getManager();

            $em->flush();

            $this->addFlash('success', '<i class="uk-icon-check"></i> Das Jobticket wurde erfolgreich aktualisiert!');

            return new RedirectResponse($this->generateUrl('modules_ordermanagement_jobticket_details', ['jobTicket' => $jobTicket->getId()]));
        }

        return $this->render('ModulesOrderManagementBundle:JobTicket:edit.html.twig', ['jobTicket' => $jobTicket,
            'form' => $form->createView()]);
    }

    /**
     * Edits a jobTicket status.
     * @Route("/{jobTicket}/status/{status}/{index}")
     * @Method({"GET", "POST"})
     * @Security("is_granted('EDIT', 'Modules\\OrderManagementBundle\\Entity\\JobTicket')")
     * @param           $status
     * @param JobTicket $jobTicket
     * @return RedirectResponse|Response
     */
    public function changeStatusAction($status, JobTicket $jobTicket, $index = false)
    {
        $em = $this->getDoctrine()->getManager();

        $jobTicket->setStatus($status);

        $em->flush();

        $this->addFlash('success', '<i class="uk-icon-check"></i> Der Status des Jobtickets wurde erfolgreich geändert!');
        if ($index) {
            return new RedirectResponse($this->generateUrl('modules_ordermanagement_jobticket_index'));
        } else {
            return new RedirectResponse($this->generateUrl('modules_ordermanagement_jobticket_details', ['jobTicket' => $jobTicket->getId()]));
        }

    }

    /**
     * Deletes a Jobticket entity.
     * @Route("/{jobTicket}/delete")
     * @Method({"GET", "DELETE"})
     * @Security("is_granted('DELETE', 'Modules\\OrderManagementBundle\\Entity\\JobTicket')")
     * @param Request $request
     * @param JobTicket $jobTicket
     * @return RedirectResponse
     */
    public function deleteAction(Request $request, JobTicket $jobTicket)
    {
        $form = $this->createDeleteForm($jobTicket);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($jobTicket);
            $em->flush();

            $this->addFlash('success', '<i class="uk-icon-check"></i> Das Jobticket wurde erfolgreich gelöscht!');
        }

        return $this->redirectToRoute('modules_ordermanagement_jobticket_index');
    }

}