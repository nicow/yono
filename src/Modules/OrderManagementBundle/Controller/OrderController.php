<?php

namespace Modules\OrderManagementBundle\Controller;

use Core\AppBundle\Entity\User;
use Modules\OrderManagementBundle\Entity\Calculation;
use Modules\OrderManagementBundle\Entity\CalculationItem;
use Modules\OrderManagementBundle\Entity\Document;
use Modules\OrderManagementBundle\Entity\Orders;
use Modules\OrderManagementBundle\Entity\PriceList;
use Modules\OrderManagementBundle\Entity\Product;
use Modules\OrderManagementBundle\Entity\Service;
use Modules\OrderManagementBundle\Form\ContactEditAjaxType;
use Modules\OrderManagementBundle\Form\OrderEditType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;


/**
 * Order controller.
 * @Route("/order")
 */
class OrderController extends Controller
{
    /**
     * Lists all order entities.
     * @Route("/")
     * @Method({"GET", "POST"})
     * @Security("is_granted('VIEW', 'Modules\\OrderManagementBundle\\Entity\\Orders')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();

        if ($request->query->all()) {
            $this->get('app.settings')->set('mod_order_management_order_overview', $request->query->all());
        }

        $filter = $this->get('app.settings')->get('mod_order_management_order_overview');

        $orders = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:Orders')->findAllOrdered($request->query->get('sort', 'number'), $request->query->get('direction', 'ASC'), $filter, $user);

        $columns = $this->get('app.helper')->setTableColumns('mod_order_management_orders_table_columns', ['number' => ['active' => 1,
            'label' => 'Nummer'],
            'title' => ['active' => 1, 'label' => 'Title'],
            'contact' => ['active' => 1, 'label' => 'Kunde'],
            'project' => ['active' => 1, 'label' => 'Projekt'],
            'status' => ['active' => 1, 'label' => 'Status'],
            'totalPrice' => ['active' => 1, 'label' => 'Gesamtsumme'],
            'printDate' => ['active' => 1, 'label' => 'Datum'],
            'createdAt' => ['active' => 1, 'label' => 'erstellt am'],
            'updatedAt' => ['active' => 0, 'label' => 'letzte Änderung'],]);
        $perPage = $this->get('app.helper')->setTablePerPage('mod_order_management_orders_table_rows');
        $paginator = $this->get('knp_paginator');
        $page = $request->query->getInt('page', 1);
        $pagination = $paginator->paginate($orders, $page, $perPage);

        $deleteForms = [];
        foreach ($orders as $order) {
            $deleteForms[$order->getId()] = $this->createDeleteForm($order)->createView();
        }
        $divisions = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:Division')->findAll();

        return $this->render('ModulesOrderManagementBundle:Order:index.html.twig', ['columns' => $columns,
            'divisions' => $divisions,
            'pagination' => $pagination,
            'perPage' => $perPage,
            'filterSettings' => $filter,
            'orders' => $orders,
            'deleteForms' => $deleteForms,]);
    }

    /**
     * Lists all order entities.
     * @Route("/monthlyFee")
     * @Method({"GET", "POST"})
     * @Security("is_granted('VIEW', 'Modules\\OrderManagementBundle\\Entity\\Orders')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function monthlyFeeAction(Request $request)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();

        if ($request->query->all()) {
            $this->get('app.settings')->set('mod_order_management_order_monthlyfee_overview', $request->query->all());
        }

        $filter = $this->get('app.settings')->get('mod_order_management_order-monthlyfee_overview');

        $orders = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:Orders')->findAllOrderedMonthlyFee($request->query->get('sort', 'number'), $request->query->get('direction', 'ASC'), $filter, $user);

        $columns = $this->get('app.helper')->setTableColumns('mod_order_management_orders_monthlyfee_table_columns', ['number' => ['active' => 1,
            'label' => 'Nummer'],
            'title' => ['active' => 1, 'label' => 'Title'],
            'contact' => ['active' => 1, 'label' => 'Kunde'],
            'project' => ['active' => 1, 'label' => 'Projekt'],
            'status' => ['active' => 1, 'label' => 'Status'],
            'totalPrice' => ['active' => 1, 'label' => 'Gesamtsumme'],
            'month' => ['active' => 1, 'label' => 'Letzter Monat'],
            'printDate' => ['active' => 1, 'label' => 'Datum'],
            'createdAt' => ['active' => 1, 'label' => 'erstellt am'],
            'updatedAt' => ['active' => 0, 'label' => 'letzte Änderung'],]);
        $perPage = $this->get('app.helper')->setTablePerPage('mod_order_management_orders_monthlyfee_table_rows');
        $paginator = $this->get('knp_paginator');
        $page = $request->query->getInt('page', 1);
        $pagination = $paginator->paginate($orders, $page, $perPage);

        $deleteForms = [];
        foreach ($orders as $order) {
            $deleteForms[$order->getId()] = $this->createDeleteForm($order)->createView();
        }
        $divisions = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:Division')->findAll();

        return $this->render('ModulesOrderManagementBundle:Order:monthlyFee.html.twig', ['columns' => $columns,
            'divisions' => $divisions,
            'pagination' => $pagination,
            'perPage' => $perPage,
            'filterSettings' => $filter,
            'orders' => $orders,
            'deleteForms' => $deleteForms,]);
    }

    /**
     * Creates a form to delete a order entity.
     * @param Orders $order The member entity
     * @return Form The form
     */
    private function createDeleteForm(Orders $order)
    {
        return $this->createFormBuilder()->setAction($this->generateUrl('modules_ordermanagement_order_delete', ['order' => $order->getId()]))->setMethod('DELETE')->getForm();
    }

    /**
     * @Route("/{order}/details")
     * @Security("is_granted('VIEW', 'Modules\\OrderManagementBundle\\Entity\\Orders')")
     * @param Orders $order
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function detailsAction(Request $request, Orders $order)
    {
        $calaculationItems = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:CalculationItem')->findByOrders($order);

        $columns = $this->get('app.helper')->setTableColumns('mod_order_management_calculation_item_table_columns', ['position' => ['active' => 1,
            'label' => 'Artikel'],
            'amount' => ['active' => 1, 'label' => 'Anzahl'],
            'price' => ['active' => 1, 'label' => 'Einzelpreis'],
            'tax' => ['active' => 1, 'label' => 'MwSt.'],
            'total' => ['active' => 1, 'label' => 'Gesamtpreis'],
            'specialPrice' => ['active' => 1, 'label' => 'Sonderpreis']]);

        $perPage = $this->get('app.helper')->setTablePerPage('mod_order_management_calculation_item_table_rows');
        $paginator = $this->get('knp_paginator');
        $page = $request->query->getInt('page', 1);
        $pagination = $paginator->paginate($calaculationItems, $page, $perPage);


        //GET JOBTICKETS FROM ORDER
        $jobtickets = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:JobTicket')->findBy(['order' => $order]);

        $columnsJobTicket = $this->get('app.helper')->setTableColumns('mod_order_management_jobTicket_table_columns', ['title' => ['active' => 1,
            'label' => 'Titel'],
            'members' => ['active' => 1, 'label' => 'Mitarbeiter'],
            'status' => ['active' => 1, 'label' => 'Status'],
            'createdAt' => ['active' => 1, 'label' => 'erstellt am'],
            'createdBy' => ['active' => 0, 'label' => 'erstellt am'],
            'updatedAt' => ['active' => 1, 'label' => 'letzte Änderung'],]);

        $perPageJobTickets = $this->get('app.helper')->setTablePerPage('mod_order_management_jobTicket_table_rows');
        $paginatorJobTicket = $this->get('knp_paginator');
        $pageJobTicket = $request->query->getInt('page', 1);
        $paginationJobTicket = $paginatorJobTicket->paginate($jobtickets, $pageJobTicket, $perPageJobTickets);


        return $this->render('ModulesOrderManagementBundle:Order:details.html.twig', ['order' => $order,
            'columns' => $columns,
            'pagination' => $pagination,
            'perPage' => $perPage,
            'calaculationItems' => $calaculationItems,
            'columnsJobTicket' => $columnsJobTicket,
            'paginationJobTicket' => $paginationJobTicket,
            'perPageJobTicket' => $perPageJobTickets]);
    }

    /**
     * Creates a new order entity.
     * @Route("/new")
     * @Method({"GET", "POST"})
     * @Security("is_granted('CREATE', 'Modules\\OrderManagementBundle\\Entity\\Orders')")
     * @param Request $request
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $order = new Orders();
        $calculation = new Calculation();
        $orderPositions = null;

        $flow = $this->get('mod.order_management.form.flow.order');


        $dataRequest = $request->request->get('order');
        if (isset($dataRequest['contactAddress'])) {
            $request->getSession()->set('order_skip', $request->request->all());
        }

        $flow->bind($order);

        $form = $flow->createForm();
        if ($flow->isValid($form)) {
            $orderPositions = $request->request->all();

            if ($flow->getCurrentStep() == 1) {
                $request->getSession()->remove('order_calculationitems');
                $request->getSession()->remove('order_discount');
                $request->getSession()->remove('order_skip');
                $request->getSession()->remove('order_terms_of_payment');
                $request->getSession()->remove('order_project_skip');
            }

            if ($flow->getCurrentStep() == 6) {
                $request->getSession()->set('order_terms_of_payment', $request->request->all());
                $request->getSession()->set('order_project_skip', $request->request->all());
            }

            if ($flow->getCurrentStep() == 8) {
                //Save Services & Products in Session
                $request->getSession()->set('order_calculationitems', $request->request->all());


                $dataRequest = $request->request->get('order');

                if (isset($dataRequest['noContactDiscount'])) {

                    $request->getSession()->set('order_discount', $dataRequest['noContactDiscount']);
                }
            }

            $flow->saveCurrentStepData($form);


            if ($flow->nextStep()) {
                // form for the next step
                $form = $flow->createForm();

            } else {
                // flow finished
                $em = $this->getDoctrine()->getManager();

                //create contact if not exists
                if (!$order->contact->getId()) {
                    $em->persist($order->contact);
                    $em->flush();
                }
                //create address if not exists
                $addressID = $order->getContactAddress() ? $order->getContactAddress()->getId() : null;
                $addressName = $order->getContactAddress() ? $order->getContactAddress()->getName() : null;
                if (!$addressID && $addressName) {
                    $order->contactAddress->setContact($order->contact);
                    $em->persist($order->contactAddress);
                    $em->flush();
                }
                //create person if not exists
                $personID = $order->getContactPerson() ? $order->getContactPerson()->getId() : null;
                $personName = $order->getContactPerson() ? $order->getContactPerson()->getFirstname() : null;
                if (!$personID && $personName) {
                    $order->contactPerson->setContact($order->contact);
                    $em->persist($order->contactPerson);
                    $em->flush();
                }

                $calculationPrice = $request->request->all();

                //create calculation entity and save items
                if ($request->getSession()->has('order_calculationitems')) {

                    $calculation = new Calculation();
                    $calculation->setStatus(1);

                    if (isset($calculationPrice['order']['discount'])) if (is_numeric($calculationPrice['order']['discount'])) $calculation->setDiscount($calculationPrice['order']['discount']);

                    if ($request->getSession()->get('order_discount') == null) {
                        $calculation->setContactDiscount(1);
                    }

                    if (isset($calculationPrice['order']['discountType'])) $calculation->setDiscountType(1);

                    if ($calculationPrice['orderFinalPrice']) {
                        if (strpos($calculationPrice['orderFinalPrice'], ',') !== false) {
                            $calculationPrice['orderFinalPrice'] = str_replace('.', '', $calculationPrice['orderFinalPrice']);
                            $calculationPrice['orderFinalPrice'] = str_replace(',', '.', $calculationPrice['orderFinalPrice']);
                        }

                        $calculation->setFinalPrice(number_format($calculationPrice['orderFinalPrice'], 2, ".", ""));
                    }

                    if (isset($request->getSession()->get('order_terms_of_payment')['order']['netto'])) $calculation->setNetto(1);

                    if (isset($request->getSession()->get('order_terms_of_payment')['order']['termsOfPayment'])) {
                        $termsOfPaymentRepo = $em->getRepository('ModulesOrderManagementBundle:TermsOfPayment');

                        $termsOfPayment = $termsOfPaymentRepo->find($request->getSession()->get('order_terms_of_payment')['order']['termsOfPayment']);
                        $calculation->setTermsOfPayment($termsOfPayment);
                    }
                    if (isset($request->getSession()->get('order_terms_of_payment')['order']['monthlyFee'])) {
                        if ($request->getSession()->get('order_terms_of_payment')['order']['monthlyFee'] >= 1) $calculation->setMonthlyFee($request->getSession()->get('order_terms_of_payment')['order']['monthlyFee']);
                    }


                    $em->persist($calculation);
                    $em->flush();

                    $data = $request->getSession()->get('order_calculationitems');
                    $omHelper = $this->get('mod.order_management.helper');

                    if (isset($data['order']['services'])) {
                        foreach ($data['order']['services'] as $services) {
                            $service = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:Service')->find($services['service']);
                            if ($service) {
                                $calculationItem = new CalculationItem();
                                $calculationItem->setCalculation($calculation);
                                $calculationItem->setStatus(1);
                                $calculationItem->setService($service);
                                if ($services['specialPrice']) {
                                    $services['specialPrice'] = str_replace('.', '', $services['specialPrice']);
                                    $services['specialPrice'] = str_replace(',', '.', $services['specialPrice']);
                                    $calculationItem->setSpecialPrice(number_format($services['specialPrice'], 2, ".", ""));
                                }
                                $calculationItem->setAmount($services['amount']);
                                $calculationItem->setExtraText($services['extraText']);
                                $calculationItem->setSinglePrice($service->getPrice());
                                $em->persist($calculationItem);
                            } elseif ($services['textFree'] != '' && $services['priceFree'] != '') {
                                $services['priceFree'] = str_replace('.', '', $services['priceFree']);
                                $services['priceFree'] = str_replace(',', '.', $services['priceFree']);
                                $calculationItem = new CalculationItem();
                                $calculationItem->setCalculation($calculation);
                                $calculationItem->setStatus(1);
                                $calculationItem->setFreeText($services['textFree']);
                                $calculationItem->setFreeTax($services['taxFree']);
                                $calculationItem->setFreePriceType($services['priceFreeType']);
                                $calculationItem->setFreeText($services['textFree']);
                                $calculationItem->setExtraText($services['extraText']);
                                $singlePrice = $services['priceSingleFree'];
                                $calculationItem->setSinglePrice(number_format($singlePrice, 2, ".", ""));
                                $calculationItem->setAmount($services['amountFree']);
                                $em->persist($calculationItem);

                                if ($services['saveFree'] == '1') {
                                    $serviceNew = new Service();
                                    $serviceNew->setTitle($services['textFree']);
                                    $serviceNew->setPrice(number_format($singlePrice, 2, ".", ""));
                                    $serviceNew->setPriceType($omHelper->getServicePriceTypeByName($services['priceFreeType']));
                                    $serviceNew->setTax($services['taxFree']);

                                    $em->persist($serviceNew);
                                }

                                if ($services['saveFree'] == '2') {
                                    $productNew = new Product();
                                    $productNew->setTitle($services['textFree']);
                                    $productNew->setPrice(number_format($singlePrice, 2, ".", ""));
                                    $productNew->setPriceType($omHelper->getServicePriceTypeByName($services['priceFreeType']));
                                    $productNew->setTax($services['taxFree']);

                                    $em->persist($productNew);
                                }

                            } else {
                                $product = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:Product')->find($services['product']);
                                if ($product) {
                                    $calculationItem = new CalculationItem();
                                    $calculationItem->setCalculation($calculation);
                                    $calculationItem->setStatus(1);
                                    $calculationItem->setExtraText($services['extraText']);
                                    $calculationItem->setProduct($product);
                                    if ($services['specialPriceProduct']) {
                                        $services['specialPriceProduct'] = str_replace('.', '', $services['specialPriceProduct']);
                                        $services['specialPriceProduct'] = str_replace(',', '.', $services['specialPriceProduct']);
                                        $calculationItem->setSpecialPrice(number_format($services['specialPriceProduct'], 2, ".", ""));
                                    }
                                    $calculationItem->setAmount($services['amountProduct']);
                                    $calculationItem->setSinglePrice($product->getPrice());
                                    $em->persist($calculationItem);
                                }
                            }
                        }
                    }
                }
                $order->setCalculation($calculation);

                $order->setStatus(0);
                $em->persist($order);
                $em->flush();


                $division = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:Division')->find($order->getDivision());
                $division->setOrderCount($division->getOrderCount() + 1);
                $em->flush();

                $getNumber = $omHelper->buildNumber($order, 'order', $division->getOrderCount());

                $order->setNumber($getNumber);
                $em->flush();

                $flow->reset(); // remove step data from the session
                $request->getSession()->remove('order_calculationitems');
                $request->getSession()->remove('order_discount');
                $request->getSession()->remove('order_skip');
                $request->getSession()->remove('order_terms_of_payment');
                $request->getSession()->remove('order_project_skip');

                $this->addFlash('success', '<i class="uk-icon-check"></i> Der Auftrag wurde erfolgreich angelegt!');

                return $this->redirectToRoute('modules_ordermanagement_order_details', ['order' => $order->getId()]);
            }
        }

        return $this->render('ModulesOrderManagementBundle:Order:new.html.twig', ['formData' => $order,
            'orderPositions' => $orderPositions,
            'flow' => $flow,
            'form' => $form->createView(),]);
    }


    /**
     * Creates a new order entity.
     * @Route("/{order}/edit")
     * @Method({"GET", "POST"})
     * @Security("is_granted('CREATE', 'Modules\\OrderManagementBundle\\Entity\\Orders')")
     * @param Request $request
     * @param Orders $order
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, Orders $order)
    {
        $em = $this->getDoctrine()->getManager();

        $netto = false;
        if ($order->getCalculation()->getNetto()) {
            $netto = true;
        }

        $monthfee = false;
        if ($order->getMonthFee()) {
            $monthfee = true;
        }

        $editForm = $this->createForm(OrderEditType::class, $order, ['netto' => $netto, 'monthfee' => $monthfee]);

        $editForm->handleRequest($request);

        $calaculationItems = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:CalculationItem')->findByOrders($order);
        $omHelper = $this->get('mod.order_management.helper');

        if ($editForm->isSubmitted() && $editForm->isValid()) {

            $positions = $request->request->get('order_edit');

            $contactData = $request->request->get('contact_edit_ajax');

            if (isset($contactData['contactPerson'])) {
                $contactPersonEntity = $em->getRepository('ModulesContactBundle:Person')->find($contactData['contactPerson']);
                $order->setContactPerson($contactPersonEntity);
            }

            if (isset($contactData['contactAddress'])) {
                $contactAddressEntity = $em->getRepository('ModulesContactBundle:Address')->find($contactData['contactAddress']);
                $order->setContactAddress($contactAddressEntity);
            }

            if (isset($positions['services'])) {
                foreach ($positions['services'] as $services) {
                    $service = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:Service')->find($services['service']);
                    if ($service) {
                        $calculationItem = new CalculationItem();
                        $calculationItem->setCalculation($order->getCalculation());
                        $calculationItem->setStatus(1);
                        $calculationItem->setExtraText($services['extraText']);
                        $calculationItem->setService($service);
                        if ($services['specialPrice']) {
                            $services['specialPrice'] = str_replace('.', '', $services['specialPrice']);
                            $services['specialPrice'] = str_replace(',', '.', $services['specialPrice']);
                            $calculationItem->setSpecialPrice($services['specialPrice']);
                        }
                        $calculationItem->setAmount($services['amount']);
                        $calculationItem->setSinglePrice($service->getPrice());
                        $em->persist($calculationItem);
                    } elseif ($services['textFree'] != '' && $services['priceFree'] != '') {
                        $services['priceFree'] = str_replace('.', '', $services['priceFree']);
                        $services['priceFree'] = str_replace(',', '.', $services['priceFree']);
                        $calculationItem = new CalculationItem();
                        $calculationItem->setCalculation($order->getCalculation());
                        $calculationItem->setStatus(1);
                        $calculationItem->setFreeText($services['textFree']);
                        $calculationItem->setFreeTax($services['taxFree']);
                        $calculationItem->setFreePriceType($services['priceFreeType']);
                        $calculationItem->setFreeText($services['textFree']);
                        $calculationItem->setExtraText($services['extraText']);
                        $singlePrice = $services['priceSingleFree'];
                        $calculationItem->setSinglePrice(number_format($singlePrice, 2, ".", ""));
                        $calculationItem->setAmount($services['amountFree']);
                        $em->persist($calculationItem);

                        if ($services['saveFree'] == '1') {
                            $serviceNew = new Service();
                            $serviceNew->setTitle($services['textFree']);
                            $serviceNew->setPrice(number_format($singlePrice, 2, ".", ""));
                            $serviceNew->setPriceType($omHelper->getServicePriceTypeByName($services['priceFreeType']));
                            $serviceNew->setTax($services['taxFree']);

                            $em->persist($serviceNew);
                        }

                        if ($services['saveFree'] == '2') {
                            $productNew = new Product();
                            $productNew->setTitle($services['textFree']);
                            $productNew->setPrice(number_format($singlePrice, 2, ".", ""));
                            $productNew->setPriceType($omHelper->getServicePriceTypeByName($services['priceFreeType']));
                            $productNew->setTax($services['taxFree']);

                            $em->persist($productNew);
                        }

                    } else {
                        $product = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:Product')->find($services['product']);
                        if ($product) {
                            $calculationItem = new CalculationItem();
                            $calculationItem->setCalculation($order->getCalculation());
                            $calculationItem->setStatus(1);
                            $calculationItem->setProduct($product);
                            $calculationItem->setExtraText($services['extraText']);
                            if ($services['specialPriceProduct']) {
                                $services['specialPriceProduct'] = str_replace('.', '', $services['specialPriceProduct']);
                                $services['specialPriceProduct'] = str_replace(',', '.', $services['specialPriceProduct']);
                                $calculationItem->setSpecialPrice($services['specialPrice']);
                            }
                            $calculationItem->setAmount($services['amountProduct']);
                            $calculationItem->setSinglePrice($product->getPrice());
                            $em->persist($calculationItem);
                        }
                    }
                }
            }
            $items = $request->request->get('order_position_item');
            $deleteItems = $request->request->get('delete');
            $amount = $request->request->get('order_position_amount');
            $specialprice = $request->request->get('order_position_specialprice');
            $extraText = $request->request->get('offer_position_extraText');

            if ($items) {
                for ($x = 0; $x < count($items); $x++) {
                    $calaculationItem = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:CalculationItem')->find($items[$x]);

                    $calaculationItem->setAmount($amount[$x]);

                    if ($specialprice[$x]) {
                        $specialprice[$x] = str_replace('.', '', $specialprice[$x]);
                        $specialprice[$x] = str_replace(',', '.', $specialprice[$x]);
                        $calaculationItem->setSpecialPrice(number_format($specialprice[$x], 2, ".", ""));
                        $calaculationItem->setSpecialPrice($specialprice[$x]);
                    }
                    $calaculationItem->setExtraText($extraText[$x]);
                }
            }
            if ($deleteItems) {
                for ($x = 0; $x < count($deleteItems); $x++) {
                    $calaculationItem = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:CalculationItem')->find($deleteItems[$x]);

                    $em->remove($calaculationItem);
                }


            }
            $price = str_replace('.', '', $request->request->get('order_total_price'));
            $price = str_replace(',', '.', $price);

            $calculation = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:Calculation')->find($order->getCalculation());

            if (isset($request->request->get('order_edit')['netto'])) {
                $calculation->setNetto(1);
            } else {
                $calculation->setNetto(0);
            }

            if (isset($request->getSession()->get('order_edit')['monthFee'])) {
                if ($request->getSession()->get('order_edit')['monthFee'] >= 1) $order->setMonthFee($request->getSession()->get('order_edit')['monthFee']);
            }


            $calculation->setFinalPrice($price);

            $em->flush();

            $this->addFlash('success', '<i class="uk-icon-check"></i> Der Auftrag wurde erfolgreich aktualisiert!');


            return $this->redirectToRoute('modules_ordermanagement_order_details', ['order' => $order->getId()]);
        }

        return $this->render('ModulesOrderManagementBundle:Order:edit.html.twig', ['order' => $order,
            'calaculationItems' => $calaculationItems,
            'edit_form' => $editForm->createView(),]);
    }

    /**
     * Creates a new order entity from an offer entity
     * @Route("/import")
     * @Method({"GET", "POST"})
     * @Security("is_granted('CREATE', 'Modules\\OrderManagementBundle\\Entity\\Orders')")
     * @param Request $request
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function importAction(Request $request)
    {

        if ($request->getMethod() == 'POST') {
            $em = $this->getDoctrine()->getManager();
            $offer = $em->getRepository('ModulesOrderManagementBundle:Offer')->findOneBy(['id' => $request->request->get('offer')]);

            if ($offer) {
                $order = new Orders();
                $order->setContactAddress($offer->getContactAddress());
                $order->setContact($offer->getContact());
                $order->setContactPerson($offer->getContactPerson());
                $order->setCalculation($offer->getCalculation());
                $order->setDescription($offer->getDescription());
                $order->setOffer($offer);
                $order->setProject($offer->getProject());
                $order->setTitle($offer->getTitle());
                $order->setMonthFee($offer->getMonthFee());
                $order->setStatus(0);
                $order->setDivision($offer->getDivision());
                $em->persist($order);
                $em->flush();

                $omHelper = $this->get('mod.order_management.helper');
                $division = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:Division')->find($order->getDivision());
                $division->setOrderCount($division->getOrderCount() + 1);
                $em->flush();

                $getNumber = $omHelper->buildNumber($order, 'order', $division->getOrderCount());

                $order->setNumber($getNumber);

                $offer->setStatus(2);
                $em->flush();


                $this->addFlash('success', '<i class="uk-icon-check"></i>Das Angebot wurde erfolgreich in ein Auftrag überführt.');

                return $this->redirectToRoute('modules_ordermanagement_order_details', ['order' => $order->getId()]);
            }

        }

        return $this->redirectToRoute('modules_ordermanagement_order_index');
    }

    /**
     * Edits a Order status.
     * @Route("/{order}/status/{status}/{index}")
     * @Method({"GET", "POST"})
     * @Security("is_granted('EDIT', 'Modules\\OrderManagementBundle\\Entity\\Orders')")
     * @param        $status
     * @param Orders $order
     * @param        $index
     * @return RedirectResponse|Response
     */
    public function changeStatusAction($status, Orders $order, $index = false)
    {
        $em = $this->getDoctrine()->getManager();

        $order->setStatus($status);

        $em->flush();

        $this->addFlash('success', '<i class="uk-icon-check"></i> Der Status des Auftrags wurde erfolgreich geändert!');
        if ($index) {
            return new RedirectResponse($this->generateUrl('modules_ordermanagement_order_index'));
        } else {
            return new RedirectResponse($this->generateUrl('modules_ordermanagement_order_details', ['order' => $order->getId()]));
        }


    }

    /**
     * Edit a order entity.
     * @Route("/{order}/editajax")
     * @Method({"GET", "POST"})
     * @Security("is_granted('CREATE', 'Modules\\OrderManagementBundle\\Entity\\Offer')")
     * @param Request $request
     * @param Orders $order
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAjaxAction(Request $request, Orders $order)
    {

        $em = $this->getDoctrine()->getManager();

        $contact = $request->request->get('order_edit');
        $contactEntity = $em->getRepository('ModulesContactBundle:Contact')->find($contact['contact']);
        $order->setContact($contactEntity);

        $editForm = $this->createForm(ContactEditAjaxType::class, $order,['data_class'=>'Modules\OrderManagementBundle\Entity\Orders']);


        $editForm->handleRequest($request);


        return $this->render('ModulesOrderManagementBundle:Form:editajax.html.twig', ['edit_form' => $editForm->createView()]);
    }

    /**
     * Deletes a order entity.
     * @Route("/{order}/delete")
     * @Method({"GET", "DELETE"})
     * @Security("is_granted('DELETE', 'Modules\\OrderManagementBundle\\Entity\\Orders') or is_granted('DELETE', order)")
     * @param Request $request
     * @param Orders $order
     * @return RedirectResponse
     */
    public function deleteAction(Request $request, Orders $order)
    {
        $form = $this->createDeleteForm($order);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($order);
            $em->flush();

            $this->addFlash('success', '<i class="uk-icon-check"></i> Der Auftrag wurde erfolgreich gelöscht!');
        }


        return $this->redirectToRoute('modules_ordermanagement_order_index');
    }

    /**
     * Export Ordder in Choosen File
     * @Route("/export/{direct}")
     * @Security("is_granted('VIEW', 'Modules\\OrderManagementBundle\\Entity\\Orders')")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function exportAction(Request $request, $direct = false)
    {
        $place = null;

        if ($request->request->get('place')) {
            $place = $request->request->get('place');
        }

        $fileformat = 'pdf';

        if (!$direct) {

            /**
             * @var Orders $order
             */
            $order = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:Orders')->findOneBy(['id' => $request->request->get('order')]);
            $order->setStatus(1);
            $order->setPrintDate(new \DateTime($request->request->get('printDate')));
            $order->setPlaceholder($place);

            $this->getDoctrine()->getManager()->flush();

            $fileformat = $request->request->get('fileformat');

        } else {
            $order = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:Orders')->findOneBy(['id' => $direct]);
            $place = $order->getPlaceholder();
        }

        $calaculationItems = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:CalculationItem')->findByOrders($order);

        $document = new Document($order->getDivision()->getOrderTemplate()->getPath() . $order->getDivision()->getOrderTemplate()->getName());
        $document->NoErr = true;

        @$document->MergeField("customer", ['id' => $order->getContact()->getId(),
            'city' => utf8_decode($order->getContactAddress() ? $order->getContactAddress()->getCity() : ""),
            'zip' => $order->getContactAddress() ? $order->getContactAddress()->getZip() : "",
            'company' => utf8_decode($order->getContact() ? $order->getContact()->getName() : ""),
            'street' => utf8_decode($order->getContactAddress() ? $order->getContactAddress()->getStreet() : ""),
            'firstname' => utf8_decode($order->getContactPerson() ? $order->getContactPerson()->getFirstname() : ""),
            'lastname' => utf8_decode($order->getContactPerson() ? $order->getContactPerson()->getLastname() : ""),
            'salutation' => utf8_decode($order->getContactPerson() ? $order->getContactPerson()->getSalutation() : ""),
            'position' => utf8_decode($order->getContactPerson() ? $order->getContactPerson()->getPosition() : "")]);


        $count = 1;
        $priceTotal = 0;
        $price = 0;
        $amount = 0;
        $type = null;
        $singleprice = 0;
        $title = null;

        $list = [];

        foreach ($calaculationItems as $item) {
            if ($item->getProduct()) {

                $singleprice = $item->getSinglePrice();

                $amount = $item->getAmount();
                $title = $item->getProduct()->getTitle();
                $description = $item->getProduct()->getDescription();
                $price = $singleprice * $amount;
                $type = $item->getProduct()->getPriceTypeName();
            }
            if ($item->getService()) {

                $singleprice = $item->getSinglePrice();

                $amount = $item->getAmount();
                $title = $item->getService()->getTitle();
                $description = $item->getService()->getDescription();
                $type = $item->getService()->getPriceTypeName();
                $price = $singleprice * $amount;
            }

            if ($item->getFreeText()) {
                $singleprice = $item->getSinglePrice();
                $amount = $item->getAmount();
                $title = $item->getFreeText();
                $type = $item->getFreePriceType();
                $price = $singleprice * $amount;
                $description = '';
            }

            if ($item->getSpecialPrice()) {
                $singleprice = $item->getSpecialPrice() / $amount;
                $price = $item->getSpecialPrice();
            }
            $priceTotal = $priceTotal + $price;

            if ($item->getExtraText()) {
                $title = $title . ' ( ' . $item->getExtraText() . ' )';
            }

            if ($count == 25) {
                $list[] = ['title' => '',
                    'description' => '',
                    'amount' => '',
                    'pos' => '',
                    'type' => '',
                    'singleprice' => '',
                    'sum' => '',];
            }

            $list[] = ['title' => utf8_decode($title),
                'description' => utf8_decode(strip_tags($description)),
                'amount' => number_format($amount, 2, ',', '.') . ' ' . utf8_decode($type),
                'pos' => $count,
                'type' => utf8_decode($type),
                'singleprice' => number_format($singleprice, 2, ',', '.'),
                'sum' => number_format($price, 2, ',', '.')];

            $count++;
        }
        //Fill the table with products and services
        $document->MergeBlock("ps", $list);
        $netto = $priceTotal * 0.81;
        $discount = 0;
        if ($order->getCalculation()->getContactDiscount()) {
            $discount = ($priceTotal * $order->getContact()->getDiscount()) / 100;
        }
        if ($order->getCalculation()->getDiscount()) {
            if ($order->getCalculation()->getDiscountType()) {
                $discount = $discount + ($priceTotal * $order->getCalculation()->getDiscount()) / 100;
            } else {
                $discount = $discount + $order->getCalculation()->getDiscount();
            }

        }

        $steuer = ($priceTotal - $discount) * 0.19;

        if ($order->getCalculation()->getNetto()) {
            $steuer = 0;
        }


        $user = $order->getCreatedBy();
        $member = $user->getMember() ?: $user;

        if ($member instanceof User) {
            $name = $member->getUsername();
        } else {
            $name = $member->getFullName();
        }

        if ($order->getPrintDate()) {
            $printDate = $order->getPrintDate()->format('d.m.Y');
        } else {
            $printDate = $order->getCreatedAt()->format('d.m.Y');
        }


        $document->MergeField("team", ['fullname' => utf8_decode($name)]);

        $document->MergeField("offer", ['printdate' => "\t" . $printDate,
            'id' => $order->getNumber(),
            'address' => ['name' => utf8_decode($order->getContactAddress() ? $order->getContactAddress()->getName() : ""),
                'city' => utf8_decode($order->getContactAddress() ? $order->getContactAddress()->getCity() : ""),],
            'subtotal' => $priceTotal,
            'mwst' => $steuer,
            'discount' => $discount,
            'place' => utf8_decode($place),
            'total' => $order->getCalculation()->getFinalPrice()]);
        $helper = $this->get('app.filehandle');
        $outputFile = $helper->getCompiledPath($order->getId(), 'Modules\OrderManagementBundle\Entity\Orders');
        $document->Show(OPENTBS_FILE, $outputFile);

        $docConverter = $this->get('app.documentconverter');
        $convertedPath = $helper->getConvertedPath($order->getId(), $order->getNumber(), 'Modules\OrderManagementBundle\Entity\Orders', $fileformat);
        $docConverter->convertTo($outputFile, $convertedPath, $fileformat);

        $response = new BinaryFileResponse($convertedPath);
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);

        return $response;

    }
}
