<?php

namespace Modules\OrderManagementBundle\Controller;

use Core\AppBundle\Entity\User;
use Modules\OrderManagementBundle\Entity\Document;
use Modules\OrderManagementBundle\Entity\Invoice;
use Modules\OrderManagementBundle\Entity\Reminder;
use Modules\OrderManagementBundle\Form\ReminderType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;


/**
 * Reminder controller.
 * @Route("/reminder")
 */
class ReminderController extends Controller
{
    /**
     * Lists all reminder entities.
     * @Route("/")
     * @Method({"GET", "POST"})
     * @Security("is_granted('VIEW', 'Modules\\OrderManagementBundle\\Entity\\Reminder')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        
        if ($request->query->all()) {
            $this->get('app.settings')->set('mod_order_management_reminder_overview', $request->query->all());
        }

        $filter = $this->get('app.settings')->get('mod_order_management_reminder_overview');


        $reminders = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:Reminder')->findAllOrdered($request->query->get('sort', 'title'), $request->query->get('direction', 'ASC'), $filter,$user);

        $columns = $this->get('app.helper')->setTableColumns('mod_order_management_reminder_table_columns', ['version' => ['active' => 1,
            'label' => 'Mahnstufe'],
            'number' => ['active' => 1, 'label' => 'Nummer'],
            'contact' => ['active' => 1, 'label' => 'Kunde'],
            'sum' => ['active' => 1, 'label' => 'Gesamtbetrag'],
            'invoice' => ['active' => 1, 'label' => 'Rechnung'],
            'status' => ['active' => 1, 'label' => 'Status'],
            'deadline' => ['active' => 1, 'label' => 'Frist'],
            'reminderDate' => ['active' => 1, 'label' => 'Datum'],]);
        $perPage = $this->get('app.helper')->setTablePerPage('mod_order_management_reminder_table_rows');
        $paginator = $this->get('knp_paginator');
        $page = $request->query->getInt('page', 1);
        $pagination = $paginator->paginate($reminders, $page, $perPage);

        $deleteForms = [];
        foreach ($reminders as $reminder) {
            $deleteForms[$reminder->getId()] = $this->createDeleteForm($reminder)->createView();
        }

        $divisions = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:Division')->findAll();

        return $this->render('ModulesOrderManagementBundle:Reminder:index.html.twig', ['columns' => $columns,
            'divisions' => $divisions,
            'pagination' => $pagination,
            'filterSettings' => $filter,
            'perPage' => $perPage,
            'deleteForms' => $deleteForms,]);
    }

    /**
     * Creates a form to delete a offer entity.
     * @param Reminder $reminder The reminder entity
     * @return Form The form
     */
    private function createDeleteForm(Reminder $reminder)
    {
        return $this->createFormBuilder()->setAction($this->generateUrl('modules_ordermanagement_reminder_delete', ['reminder' => $reminder->getId()]))->setMethod('DELETE')->getForm();
    }

    /**
     * @Route("/{reminder}/details")
     * @Security("is_granted('VIEW', 'Modules\\OrderManagementBundle\\Entity\\Reminder')")
     * @param Reminder $reminder
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function detailsAction(Request $request, Reminder $reminder)
    {
        return $this->render('ModulesOrderManagementBundle:Reminder:details.html.twig', ['reminder' => $reminder]);
    }

    /**
     * Creates a new reminder entity.
     * @Route("/new/{invoice}", defaults={"invoice": null})
     * @Method({"GET", "POST"})
     * @Security("is_granted('CREATE', 'Modules\\OrderManagementBundle\\Entity\\Reminder')")
     * @param Request $request
     * @param $invoice
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request, $invoice)
    {

        $em = $this->getDoctrine()->getManager();

        $reminder = new Reminder();

        if ($invoice) {
            $invoice = $em->getRepository('ModulesOrderManagementBundle:Invoice')->find($invoice);
            $reminder->setInvoice($invoice);

            $number = $invoice->getReminders() + 1;
            $reminder->setVersion($number);
        } else {
            $number = 1;
            $reminder->setVersion($number);
        }

        $form = $this->createForm(ReminderType::class, $reminder, ['invoice' => $invoice, 'version' => $number]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $reminder->setDivision($reminder->getInvoice()->getDivision());
            $em->persist($reminder);
            $em->flush();

            $omHelper = $this->get('mod.order_management.helper');
            $division = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:Division')->find($reminder->getDivision());
            $division->setReminderCount($division->getReminderCount() + 1);
            $em->flush();

            $getNumber = $omHelper->buildNumber($reminder, 'reminder', $division->getReminderCount());

            $reminder->setNumber($getNumber);
            $invoice = $reminder->getInvoice();
            $invoice->setReminders($invoice->getReminders() + 1);

            if ($reminder->getInvoice()->getReminders() > 1) {
                $reminder->getInvoice()->setStatus(5);
            } else {
                $reminder->getInvoice()->setStatus(4);
            }

            $em->flush();

            $this->addFlash('success', '<i class="uk-icon-check"></i> Die Mahnung wurde erfolgreich angelegt!');

            return $this->redirectToRoute('modules_ordermanagement_reminder_details', ['reminder' => $reminder->getId()]);

        }

        return $this->render('ModulesOrderManagementBundle:Reminder:new.html.twig', ['reminder' => $reminder,
            'form' => $form->createView(),]);
    }


    /**
     * Edit a reminder entity.
     * @Route("/{reminder}/edit")
     * @Method({"GET", "POST"})
     * @Security("is_granted('CREATE', 'Modules\\OrderManagementBundle\\Entity\\Reminder')")
     * @param Request $request
     * @param Reminder $reminder
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, Reminder $reminder)
    {
        $form = $this->createForm(ReminderType::class, $reminder);
        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted()) {
            $em = $this->getDoctrine()->getManager();

            $em->flush();

            $this->addFlash('success', '<i class="uk-icon-check"></i> Die Mahnung wurde erfolgreich aktualisiert!');


            return new RedirectResponse($this->generateUrl('modules_ordermanagement_reminder_details', ['reminder' => $reminder->getId()]));
        }

        return $this->render('ModulesOrderManagementBundle:Reminder:edit.html.twig', ['reminder' => $reminder,
            'form' => $form->createView()]);
    }


    /**
     * Deletes a reminder entity.
     * @Route("/{reminder}/delete")
     * @Method({"GET", "DELETE"})
     * @Security("is_granted('DELETE', 'Modules\\OrderManagementBundle\\Entity\\Reminder') or is_granted('DELETE', reminder)")
     * @param Request $request
     * @param Reminder $reminder
     * @return RedirectResponse
     */
    public function deleteAction(Request $request, Reminder $reminder)
    {
        $form = $this->createDeleteForm($reminder);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($reminder);
            $em->flush();

            $this->addFlash('success', '<i class="uk-icon-check"></i> Die Mahnung wurde erfolgreich gelöscht!');
        }


        return $this->redirectToRoute('modules_ordermanagement_reminder_index');
    }

    /**
     * Edits a Reminder status.
     * @Route("/{reminder}/status/{status}/{index}")
     * @Method({"GET", "POST"})
     * @Security("is_granted('EDIT', 'Modules\\OrderManagementBundle\\Entity\\Reminder')")
     * @param $status
     * @param $index
     * @param Reminder $reminder
     * @return RedirectResponse|Response
     */
    public function changeStatusAction($status, Reminder $reminder, $index = false)
    {
        $em = $this->getDoctrine()->getManager();

        $reminder->setStatus($status);
        $invoice = $reminder->getInvoice();

        if ($status == Reminder::STATUS_DONE) {
            $invoice->setStatus(Invoice::STATUS_BILLED);
        }

        $em->flush();

        $this->addFlash('success', '<i class="uk-icon-check"></i> Der Status der Mahnung wurde erfolgreich geändert!');
        if ($index) {
            return new RedirectResponse($this->generateUrl('modules_ordermanagement_reminder_index'));
        } else {
            return new RedirectResponse($this->generateUrl('modules_ordermanagement_reminder_details', ['reminder' => $reminder->getId()]));
        }


    }

    /**
     * Export Reminder in Choosen File
     * @Route("/export")
     * @Security("is_granted('VIEW', 'Modules\\OrderManagementBundle\\Entity\\Reminder')")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function exportAction(Request $request)
    {

        $place = null;

        if ($request->request->get('place')) {
            $place = $request->request->get('place');
        }

        /**
         * @var Reminder $reminder
         */
        $reminder = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:Reminder')->findOneBy(['id' => $request->request->get('reminder')]);
        $reminder->setPrintDate(new \DateTime($request->request->get('printDate')));
        $reminder->setStatus(1);
        $reminder->setPlaceholder($place);

        $this->getDoctrine()->getManager()->flush();


        $omHelper = $this->get('mod.order_management.helper');


        $document = new Document(
            $reminder->getDivision()->getReminderTemplate()->getPath()
            .
            $reminder->getDivision()->getReminderTemplate()->getName()
        );


        $document->NoErr = true;

        @$document->MergeField("customer", ['id' => $reminder->getInvoice()->getContact()->getId(),
            'city' => utf8_decode($reminder->getInvoice()->getContactAddress() ? $reminder->getInvoice()->getContactAddress()->getCity() : ""),
            'zip' => $reminder->getInvoice()->getContactAddress() ? $reminder->getInvoice()->getContactAddress()->getZip() : "",
            'company' => utf8_decode($reminder->getInvoice()->getContact() ? $reminder->getInvoice()->getContact()->getName() : ""),
            'street' => utf8_decode($reminder->getInvoice()->getContactAddress() ? $reminder->getInvoice()->getContactAddress()->getStreet() : ""),
            'firstname' => utf8_decode($reminder->getInvoice()->getContactPerson() ? $reminder->getInvoice()->getContactPerson()->getFirstname() : ""),
            'lastname' => utf8_decode($reminder->getInvoice()->getContactPerson() ? $reminder->getInvoice()->getContactPerson()->getLastname() : ""),
            'salutation' => utf8_decode($reminder->getInvoice()->getContactPerson() ? $reminder->getInvoice()->getContactPerson()->getSalutation() : ""),
            'position' => utf8_decode($reminder->getInvoice()->getContactPerson() ? $reminder->getInvoice()->getContactPerson()->getPosition() : "")]);


        if ($request->request->get('place')) {
            $place = $request->request->get('place');
        }

        $user = $reminder->getCreatedBy();
        $member = $user->getMember() ?: $user;

        if ($member instanceof User) {
            $name = $member->getUsername();
        } else {
            $name = $member->getFullName();
        }

        $document->MergeField("team", ['fullname' => utf8_decode($name)]);

        $document->MergeField("offer", ['printd' => "\t" . $reminder->getPrintDate()->format('d.m.Y'),
            'id' => $reminder->getNumber(),
            'address' => ['name' => utf8_decode($reminder->getInvoice()->getContactAddress() ? $reminder->getInvoice()->getContactAddress()->getName() : ""),
                'city' => utf8_decode($reminder->getInvoice()->getContactAddress() ? $reminder->getInvoice()->getContactAddress()->getCity() : ""),],
            'netto' =>  number_format($omHelper->getInvoicePrice($reminder->getInvoice()), 2, ',', '.'),
            'printdate' =>$reminder->getInvoice()->getPrintDate() ? $reminder->getInvoice()->getPrintDate()->format('d.m.Y') : date('d.m.Y'),
            'deadline' => $reminder->getDeadline()->format('d.m.Y'),
            'place' => utf8_decode($place),
            'version' => $reminder->getVersion(),
            'invoice' => $reminder->getInvoice()->getNumber()]);



        $helper = $this->get('app.filehandle');
        $outputFile = $helper->getCompiledPath($reminder->getId(),'Modules\OrderManagementBundle\Entity\Reminder');
        $document->Show(OPENTBS_FILE, $outputFile);

        $docConverter = $this->get('app.documentconverter');
        $convertedPath = $helper->getConvertedPath($reminder->getId(),$reminder->getNumber(), 'Modules\OrderManagementBundle\Entity\Reminder', $request->request->get('fileformat'));
        $docConverter->convertTo($outputFile, $convertedPath, $request->request->get('fileformat'));

        $response = new BinaryFileResponse($convertedPath);
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);

        return $response;

    }
}
