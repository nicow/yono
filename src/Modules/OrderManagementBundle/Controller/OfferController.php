<?php

namespace Modules\OrderManagementBundle\Controller;

use Core\AppBundle\Entity\User;
use Modules\OrderManagementBundle\Entity\Calculation;
use Modules\OrderManagementBundle\Entity\CalculationItem;
use Modules\OrderManagementBundle\Entity\Document;
use Modules\OrderManagementBundle\Entity\Offer;
use Modules\OrderManagementBundle\Entity\PriceList;
use Modules\OrderManagementBundle\Entity\Product;
use Modules\OrderManagementBundle\Entity\Service;
use Modules\OrderManagementBundle\Form\ContactEditAjaxType;
use Modules\OrderManagementBundle\Form\OfferEditType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;


/**
 * Offer controller.
 * @Route("/offer")
 */
class OfferController extends Controller
{
    /**
     * Lists all offer entities.
     * @Route("/")
     * @Method({"GET", "POST"})
     * @Security("is_granted('VIEW', 'Modules\\OrderManagementBundle\\Entity\\Offer')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();

        if ($request->query->all()) {
            $this->get('app.settings')->set('mod_order_management_offer_overview', $request->query->all());
        }

        $filter = $this->get('app.settings')->get('mod_order_management_offer_overview');

        $offers = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:Offer')->findAllOrdered($request->query->get('sort', 'number'), $request->query->get('direction', 'ASC'), $filter, $user);

        $columns = $this->get('app.helper')->setTableColumns('mod_order_management_offer_table_columns', ['number' => ['active' => 1,
            'label' => 'Nummer'],
            'title' => ['active' => 1, 'label' => 'Title'],
            'contact' => ['active' => 1, 'label' => 'Kunde'],
            'project' => ['active' => 1, 'label' => 'Projekt'],
            'status' => ['active' => 1, 'label' => 'Status'],
            'totalPrice' => ['active' => 1, 'label' => 'Gesamtsumme'],
            'printDate' => ['active' => 1, 'label' => 'zugestellt'],
            'reminder' => ['active' => 1, 'label' => 'Erinnerung'],
            'createdAt' => ['active' => 1, 'label' => 'erstellt am'],
            'updatedAt' => ['active' => 0, 'label' => 'letzte Änderung'],]);
        $perPage = $this->get('app.helper')->setTablePerPage('mod_order_management_offer_table_rows');
        $paginator = $this->get('knp_paginator');
        $page = $request->query->getInt('page', 1);
        $pagination = $paginator->paginate($offers, $page, $perPage);

        $deleteForms = [];
        foreach ($offers as $offer) {
            $deleteForms[$offer->getId()] = $this->createDeleteForm($offer)->createView();
        }
        $divisions = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:Division')->findAll();

        return $this->render('ModulesOrderManagementBundle:Offer:index.html.twig', ['columns' => $columns,
            'divisions' => $divisions,
            'pagination' => $pagination,
            'perPage' => $perPage,
            'filterSettings' => $filter,
            'offers' => $offers,
            'deleteForms' => $deleteForms,]);
    }

    /**
     * Creates a form to delete a offer entity.
     * @param Offer $offer The offer entity
     * @return Form The form
     */
    private function createDeleteForm(Offer $offer)
    {
        return $this->createFormBuilder()->setAction($this->generateUrl('modules_ordermanagement_offer_delete', ['offer' => $offer->getId()]))->setMethod('DELETE')->getForm();
    }

    /**
     * @Route("/{offer}/details")
     * @Security("is_granted('VIEW', 'Modules\\OrderManagementBundle\\Entity\\Offer')")
     * @param Offer $offer
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function detailsAction(Request $request, Offer $offer)
    {
        $calaculationItems = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:CalculationItem')->findByOffer($offer);

        $columns = $this->get('app.helper')->setTableColumns('mod_order_management_calculation_item_table_columns', ['position' => ['active' => 1,
            'label' => 'Artikel'],
            'amount' => ['active' => 1, 'label' => 'Anzahl'],
            'price' => ['active' => 1, 'label' => 'Einzelpreis'],
            'tax' => ['active' => 1, 'label' => 'MwSt.'],
            'total' => ['active' => 1, 'label' => 'Gesamtpreis'],
            'specialPrice' => ['active' => 1, 'label' => 'Sonderpreis']]);

        $perPage = $this->get('app.helper')->setTablePerPage('mod_order_management_calculation_item_table_rows');
        $paginator = $this->get('knp_paginator');
        $page = $request->query->getInt('page', 1);
        $pagination = $paginator->paginate($calaculationItems, $page, $perPage);

        return $this->render('ModulesOrderManagementBundle:Offer:details.html.twig', ['offer' => $offer,
            'columns' => $columns,
            'calaculationItems' => $calaculationItems,
            'pagination' => $pagination,
            'perPage' => $perPage,]);
    }

    /**
     * Creates a new offer entity.
     * @Route("/new")
     * @Method({"GET", "POST"})
     * @Security("is_granted('CREATE', 'Modules\\OrderManagementBundle\\Entity\\Offer')")
     * @param Request $request
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $offer = new Offer();
        $calculation = new Calculation();
        $offerPositions = null;

        $flow = $this->get('mod.order_management.form.flow.offer');


        $dataRequest = $request->request->get('offer');
        $em = $this->getDoctrine()->getManager();

        if (isset($dataRequest['contactAddress'])) {
            $request->getSession()->set('offer_skip', $request->request->all());
        }

        if (isset($dataRequest['division'])) {
            $division = $em->getRepository('ModulesOrderManagementBundle:Division')->findOneBy(['id' => $dataRequest['division']]);
            $offer->setDivision($division);
        }

        $flow->bind($offer);

        $form = $flow->createForm();
        if ($flow->isValid($form)) {
            $offerPositions = $request->request->all();

            if ($flow->getCurrentStep() == 1) {
                $request->getSession()->remove('offer_calculationitems');
                $request->getSession()->remove('offer_discount');
                $request->getSession()->remove('offer_skip');
                $request->getSession()->remove('offer_internal');
                $request->getSession()->remove('offer_project_skip');
            }

            if ($flow->getCurrentStep() == 6) {
                $request->getSession()->set('offer_internal', $request->request->all());
                $request->getSession()->set('offer_project_skip', $request->request->all());
            }

            if ($flow->getCurrentStep() == 8) {
                //Save Services & Products in Session
                $request->getSession()->set('offer_calculationitems', $request->request->all());

                $dataRequest = $request->request->get('offer');

                if (isset($dataRequest['noContactDiscount'])) {
                    $request->getSession()->set('offer_discount', $dataRequest['noContactDiscount']);
                }
            }

            $flow->saveCurrentStepData($form);


            if ($flow->nextStep()) {
                // form for the next step
                $form = $flow->createForm();

            } else {
                // flow finished

                //create contact if not exists
                if (!$offer->contact->getId()) {
                    $em->persist($offer->contact);
                    $em->flush();
                }
                //create address if not exists
                $addressID = $offer->getContactAddress() ? $offer->getContactAddress()->getId() : null;
                $addressName = $offer->getContactAddress() ? $offer->getContactAddress()->getName() : null;
                if (!$addressID && $addressName) {
                    $offer->contactAddress->setContact($offer->contact);
                    $em->persist($offer->contactAddress);
                    $em->flush();
                }
                //create person if not exists
                $personID = $offer->getContactPerson() ? $offer->getContactPerson()->getId() : null;
                $personName = $offer->getContactPerson() ? $offer->getContactPerson()->getFirstname() : null;
                if (!$personID && $personName) {
                    $offer->contactPerson->setContact($offer->contact);
                    $em->persist($offer->contactPerson);
                    $em->flush();
                }

                $calculationPrice = $request->request->all();

                //create calculation entity and save items
                if ($request->getSession()->has('offer_calculationitems')) {

                    $calculation = new Calculation();
                    $calculation->setStatus(1);


                    if (isset($calculationPrice['offer']['discount'])) if (is_numeric($calculationPrice['offer']['discount'])) $calculation->setDiscount($calculationPrice['offer']['discount']);

                    if (!$request->getSession()->has('offer_discount')) {
                        $calculation->setContactDiscount(1);
                    }

                    if (isset($calculationPrice['offer']['discountType'])) $calculation->setDiscountType(1);

                    if ($calculationPrice['offerFinalPrice']) {
                        if (strpos($calculationPrice['offerFinalPrice'], ',') !== false) {
                            $calculationPrice['offerFinalPrice'] = str_replace('.', '', $calculationPrice['offerFinalPrice']);
                            $calculationPrice['offerFinalPrice'] = str_replace(',', '.', $calculationPrice['offerFinalPrice']);
                        }


                        $calculation->setFinalPrice(number_format($calculationPrice['offerFinalPrice'], 2, ".", ""));
                    }

                    if (isset($request->request->get('offer_internal')['offer']['monthlyFee'])) {
                        if ($request->request->get('offer_internal')['offer']['monthlyFee'] >= 1) $calculation->setMonthlyFee($request->request->get('offer_internal')['offer']['monthlyFee']);
                    }

                    if (isset($request->getSession()->get('offer_internal')['offer']['netto'])) $calculation->setNetto(1);

                    $em->persist($calculation);
                    $em->flush();

                    $data = $request->getSession()->get('offer_calculationitems');
                    $omHelper = $this->get('mod.order_management.helper');

                    if (isset($data['offer']['services'])) {
                        foreach ($data['offer']['services'] as $services) {
                            $service = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:Service')->find($services['service']);
                            if ($service) {
                                $calculationItem = new CalculationItem();
                                $calculationItem->setCalculation($calculation);
                                $calculationItem->setStatus(1);
                                $calculationItem->setService($service);
                                if ($services['specialPrice']) {
                                    $services['specialPrice'] = str_replace('.', '', $services['specialPrice']);
                                    $services['specialPrice'] = str_replace(',', '.', $services['specialPrice']);
                                    $calculationItem->setSpecialPrice(number_format($services['specialPrice'], 2, ".", ""));
                                }
                                $calculationItem->setExtraText($services['extraText']);
                                $calculationItem->setAmount($services['amount']);
                                $calculationItem->setSinglePrice($service->getPrice());
                                $em->persist($calculationItem);
                            } elseif ($services['textFree'] != '' && $services['priceFree'] != '') {
                                $services['priceFree'] = str_replace('.', '', $services['priceFree']);
                                $services['priceFree'] = str_replace(',', '.', $services['priceFree']);
                                $calculationItem = new CalculationItem();
                                $calculationItem->setCalculation($calculation);
                                $calculationItem->setStatus(1);
                                $calculationItem->setFreeText($services['textFree']);
                                $calculationItem->setFreeTax($services['taxFree']);
                                $calculationItem->setFreePriceType($services['priceFreeType']);
                                $calculationItem->setFreeText($services['textFree']);

                                if(isset($services['extraText']))
                                {
                                    $calculationItem->setExtraText($services['extraText']);
                                }

                                $singlePrice = $services['priceSingleFree'];
                                $calculationItem->setSinglePrice(number_format($singlePrice, 2, ".", ""));
                                $calculationItem->setAmount($services['amountFree']);
                                $em->persist($calculationItem);

                                if ($services['saveFree'] == '1') {
                                    $serviceNew = new Service();
                                    $serviceNew->setTitle($services['textFree']);
                                    $serviceNew->setPrice(number_format($singlePrice, 2, ".", ""));
                                    $serviceNew->setPriceType($omHelper->getServicePriceTypeByName($services['priceFreeType']));
                                    $serviceNew->setTax($services['taxFree']);

                                    $em->persist($serviceNew);
                                }

                                if ($services['saveFree'] == '2') {
                                    $productNew = new Product();
                                    $productNew->setTitle($services['textFree']);
                                    $productNew->setPrice(number_format($singlePrice, 2, ".", ""));
                                    $productNew->setPriceType($omHelper->getServicePriceTypeByName($services['priceFreeType']));
                                    $productNew->setTax($services['taxFree']);

                                    $em->persist($productNew);
                                }

                            } else {
                                $product = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:Product')->find($services['product']);
                                if ($product) {
                                    $calculationItem = new CalculationItem();
                                    $calculationItem->setCalculation($calculation);
                                    $calculationItem->setStatus(1);
                                    $calculationItem->setExtraText($services['extraText']);
                                    $calculationItem->setProduct($product);
                                    if ($services['specialPriceProduct']) {
                                        $services['specialPriceProduct'] = str_replace('.', '', $services['specialPriceProduct']);
                                        $services['specialPriceProduct'] = str_replace(',', '.', $services['specialPriceProduct']);
                                        $calculationItem->setSpecialPrice(number_format($services['specialPriceProduct'], 2, ".", ""));
                                    }
                                    $calculationItem->setAmount($services['amountProduct']);
                                    $calculationItem->setSinglePrice($product->getPrice());
                                    $em->persist($calculationItem);
                                }
                            }
                        }
                    }
                }
                $offer->setCalculation($calculation);
                $offer->setStatus(0);
                $em->persist($offer);
                $em->flush();


                $division = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:Division')->find($offer->getDivision());
                $division->setOfferCount($division->getOfferCount() + 1);
                $em->flush();

                $getNumber = $omHelper->buildNumber($offer, 'offer', $division->getOfferCount());

                $offer->setNumber($getNumber);
                $em->flush();

                $flow->reset(); // remove step data from the session
                $request->getSession()->remove('offer_calculationitems');
                $request->getSession()->remove('offer_discount');
                $request->getSession()->remove('offer_skip');
                $request->getSession()->remove('offer_internal');
                $request->getSession()->remove('offer_project_skip');

                $this->addFlash('success', '<i class="uk-icon-check"></i> Das Angebot wurde erfolgreich angelegt!');

                return $this->redirectToRoute('modules_ordermanagement_offer_details', ['offer' => $offer->getId()]);
            }
        }

        return $this->render('ModulesOrderManagementBundle:Offer:new.html.twig', ['formData' => $offer,
            'offerPositions' => $offerPositions,
            'flow' => $flow,
            'form' => $form->createView(),]);
    }


    /**
     * Creates a new offer entity.
     * @Route("/{offer}/edit")
     * @Method({"GET", "POST"})
     * @Security("is_granted('CREATE', 'Modules\\OrderManagementBundle\\Entity\\Offer')")
     * @param Request $request
     * @param Offer $offer
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, Offer $offer)
    {
        $em = $this->getDoctrine()->getManager();

        $netto = false;
        if ($offer->getCalculation()->getNetto()) {
            $netto = true;
        }

        $monthfee = false;
        if ($offer->getMonthFee()) {
            $monthfee = true;
        }

        $editForm = $this->createForm(OfferEditType::class, $offer, ['netto' => $netto, 'monthfee' => $monthfee]);

        $editForm->handleRequest($request);

        $calaculationItems = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:CalculationItem')->findByOffer($offer);
        $omHelper = $this->get('mod.order_management.helper');


        if ($editForm->isSubmitted() && $editForm->isValid()) {

            $positions = $request->request->get('offer_edit');

            $contactData = $request->request->get('contact_edit_ajax');

            if (isset($contactData['contactPerson'])) {
                $contactPersonEntity = $em->getRepository('ModulesContactBundle:Person')->find($contactData['contactPerson']);
                $offer->setContactPerson($contactPersonEntity);
            }

            if (isset($contactData['contactAddress'])) {
                $contactAddressEntity = $em->getRepository('ModulesContactBundle:Address')->find($contactData['contactAddress']);
                $offer->setContactAddress($contactAddressEntity);
            }

            if (isset($positions['services'])) {
                foreach ($positions['services'] as $services) {
                    $service = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:Service')->find($services['service']);
                    if ($service) {
                        $calculationItem = new CalculationItem();
                        $calculationItem->setCalculation($offer->getCalculation());
                        $calculationItem->setStatus(1);
                        $calculationItem->setExtraText($services['extraText']);
                        $calculationItem->setService($service);
                        if ($services['specialPrice']) {
                            $services['specialPrice'] = str_replace('.', '', $services['specialPrice']);
                            $services['specialPrice'] = str_replace(',', '.', $services['specialPrice']);
                            $calculationItem->setSpecialPrice($services['specialPrice']);
                        }
                        $calculationItem->setAmount($services['amount']);
                        $calculationItem->setSinglePrice($service->getPrice());
                        $em->persist($calculationItem);
                    } elseif ($services['textFree'] != '' && $services['priceFree'] != '') {
                        $services['priceFree'] = str_replace('.', '', $services['priceFree']);
                        $services['priceFree'] = str_replace(',', '.', $services['priceFree']);
                        $calculationItem = new CalculationItem();
                        $calculationItem->setCalculation($offer->getCalculation());
                        $calculationItem->setStatus(1);
                        $calculationItem->setFreeText($services['textFree']);
                        $calculationItem->setFreeTax($services['taxFree']);
                        $calculationItem->setFreePriceType($services['priceFreeType']);
                        $calculationItem->setFreeText($services['textFree']);
                        $calculationItem->setExtraText($services['extraText']);
                        $singlePrice = $services['priceSingleFree'];
                        $calculationItem->setSinglePrice(number_format($singlePrice, 2, ".", ""));
                        $calculationItem->setAmount($services['amountFree']);
                        $em->persist($calculationItem);

                        if ($services['saveFree'] == '1') {
                            $serviceNew = new Service();
                            $serviceNew->setTitle($services['textFree']);
                            $serviceNew->setPrice(number_format($singlePrice, 2, ".", ""));
                            $serviceNew->setPriceType($omHelper->getServicePriceTypeByName($services['priceFreeType']));
                            $serviceNew->setTax($services['taxFree']);

                            $em->persist($serviceNew);
                        }

                        if ($services['saveFree'] == '2') {
                            $productNew = new Product();
                            $productNew->setTitle($services['textFree']);
                            $productNew->setPrice(number_format($singlePrice, 2, ".", ""));
                            $productNew->setPriceType($omHelper->getServicePriceTypeByName($services['priceFreeType']));
                            $productNew->setTax($services['taxFree']);

                            $em->persist($productNew);
                        }

                    } else {
                        $product = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:Product')->find($services['product']);
                        if ($product) {
                            $calculationItem = new CalculationItem();
                            $calculationItem->setCalculation($offer->getCalculation());
                            $calculationItem->setStatus(1);
                            $calculationItem->setProduct($product);
                            $calculationItem->setExtraText($services['extraText']);
                            if ($services['specialPriceProduct']) {
                                $services['specialPriceProduct'] = str_replace('.', '', $services['specialPriceProduct']);
                                $services['specialPriceProduct'] = str_replace(',', '.', $services['specialPriceProduct']);
                                $calculationItem->setSpecialPrice($services['specialPrice']);
                            }
                            $calculationItem->setAmount($services['amountProduct']);
                            $calculationItem->setSinglePrice($product->getPrice());
                            $em->persist($calculationItem);
                        }
                    }
                }
            }
            $items = $request->request->get('offer_position_item');
            $deleteItems = $request->request->get('delete');
            $amount = $request->request->get('offer_position_amount');
            $specialprice = $request->request->get('offer_position_specialprice');
            $extraText = $request->request->get('offer_position_extraText');

            if ($items) {
                for ($x = 0; $x < count($items); $x++) {
                    $calaculationItem = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:CalculationItem')->find($items[$x]);

                    $calaculationItem->setAmount($amount[$x]);


                    if ($specialprice[$x]) {
                        $specialprice[$x] = str_replace('.', '', $specialprice[$x]);
                        $specialprice[$x] = str_replace(',', '.', $specialprice[$x]);
                        $calaculationItem->setSpecialPrice(number_format($specialprice[$x], 2, ".", ""));
                        $calaculationItem->setSpecialPrice($specialprice[$x]);
                    }

                    $calaculationItem->setExtraText($extraText[$x]);
                }
            }
            if ($deleteItems) {
                for ($x = 0; $x < count($deleteItems); $x++) {
                    $calaculationItem = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:CalculationItem')->find($deleteItems[$x]);

                    $em->remove($calaculationItem);
                }


            }
            $price = str_replace('.', '', $request->request->get('offer_total_price'));
            $price = str_replace(',', '.', $price);

            $calculation = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:Calculation')->find($offer->getCalculation());

            if (isset($request->request->get('offer_edit')['netto'])) {
                $calculation->setNetto(1);
            } else {
                $calculation->setNetto(0);
            }
            $calculation->setFinalPrice($price);

            if (isset($request->getSession()->get('offer_edit')['monthFee'])) {
                if ($request->getSession()->get('offer_edit')['monthFee'] >= 1) $offer->setMonthFee($request->getSession()->get('offer_edit')['monthFee']);
            }

            $em->flush();

            $this->addFlash('success', '<i class="uk-icon-check"></i> Das Angebot wurde erfolgreich aktualisiert!');


            return $this->redirectToRoute('modules_ordermanagement_offer_details', ['offer' => $offer->getId()]);
        }

        return $this->render('ModulesOrderManagementBundle:Offer:edit.html.twig', ['offer' => $offer,
            'calaculationItems' => $calaculationItems,
            'edit_form' => $editForm->createView(),]);
    }


    /**
     * Edit a offer entity.
     * @Route("/{offer}/editajax")
     * @Method({"GET", "POST"})
     * @Security("is_granted('CREATE', 'Modules\\OrderManagementBundle\\Entity\\Offer')")
     * @param Request $request
     * @param Offer $offer
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAjaxAction(Request $request, Offer $offer)
    {

        $em = $this->getDoctrine()->getManager();

        $contact = $request->request->get('offer_edit');
        $contactEntity = $em->getRepository('ModulesContactBundle:Contact')->find($contact['contact']);
        $offer->setContact($contactEntity);

        $editForm = $this->createForm(ContactEditAjaxType::class, $offer,['data_class'=>'Modules\OrderManagementBundle\Entity\Offer']);


        $editForm->handleRequest($request);


        return $this->render('ModulesOrderManagementBundle:Form:editajax.html.twig', [
            'edit_form' => $editForm->createView()]);
    }

    /**
     * Deletes a offer entity.
     * @Route("/{offer}/delete")
     * @Method({"GET", "DELETE"})
     * @Security("is_granted('DELETE', 'Modules\\OrderManagementBundle\\Entity\\Offer') or is_granted('DELETE', offer)")
     * @param Request $request
     * @param Offer $offer
     * @return RedirectResponse
     */
    public function deleteAction(Request $request, Offer $offer)
    {
        $form = $this->createDeleteForm($offer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($offer);
            $em->flush();

            $this->addFlash('success', '<i class="uk-icon-check"></i> Das Angebot wurde erfolgreich gelöscht!');
        }


        return $this->redirectToRoute('modules_ordermanagement_offer_index');
    }

    /**
     * Edits a Offer status.
     * @Route("/{offer}/status/{status}/{index}")
     * @Method({"GET", "POST"})
     * @Security("is_granted('EDIT', 'Modules\\OrderManagementBundle\\Entity\\Offer')")
     * @param       $status
     * @param Offer $offer
     * @param $index
     * @return RedirectResponse|Response
     */
    public function changeStatusAction($status, Offer $offer, $index = false)
    {
        $em = $this->getDoctrine()->getManager();

        $offer->setStatus($status);

        $em->flush();

        $this->addFlash('success', '<i class="uk-icon-check"></i> Der Status des Angebots wurde erfolgreich geändert!');
        if ($index) {
            return new RedirectResponse($this->generateUrl('modules_ordermanagement_offer_index'));
        } else {
            return new RedirectResponse($this->generateUrl('modules_ordermanagement_offer_details', ['offer' => $offer->getId()]));
        }


    }

    /**
     * Export Offer in Choosen File
     * @Route("/export/{direct}")
     * @Security("is_granted('VIEW', 'Modules\\OrderManagementBundle\\Entity\\Offer')")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function exportAction(Request $request, $direct = false)
    {

        $place = null;

        if ($request->request->get('place')) {
            $place = $request->request->get('place');
        }

        $fileformat = 'pdf';

        if (!$direct) {
            /**
             * @var Offer $offer
             */
            $offer = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:Offer')->findOneBy(['id' => $request->request->get('offer')]);
            $offer->setStatus(2);
            $offer->setPrintDate(new \DateTime($request->request->get('printDate')));
            $offer->setPlaceholder($place);

            $this->getDoctrine()->getManager()->flush();

            $fileformat = $request->request->get('fileformat');

        } else {
            $offer = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:Offer')->findOneBy(['id' => $direct]);
            $place = $offer->getPlaceholder();
        }

        $calaculationItems = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:CalculationItem')->findByOffer($offer);

        $document = new Document($offer->getDivision()->getOfferTemplate()->getPath() . $offer->getDivision()->getOfferTemplate()->getName());
        $document->NoErr = true;


        @$document->MergeField("customer", ['id' => $offer->getContact()->getId(),
            'city' => utf8_decode($offer->getContactAddress() ? $offer->getContactAddress()->getCity() : ""),
            'zip' => $offer->getContactAddress() ? $offer->getContactAddress()->getZip() : "",
            'company' => utf8_decode($offer->getContact() ? $offer->getContact()->getName() : ""),
            'street' => utf8_decode($offer->getContactAddress() ? $offer->getContactAddress()->getStreet() : ""),
            'firstname' => utf8_decode($offer->getContactPerson() ? $offer->getContactPerson()->getFirstname() : ""),
            'lastname' => utf8_decode($offer->getContactPerson() ? $offer->getContactPerson()->getLastname() : ""),
            'salutation' => utf8_decode($offer->getContactPerson() ? $offer->getContactPerson()->getSalutation() : ""),
            'position' => utf8_decode($offer->getContactPerson() ? $offer->getContactPerson()->getPosition() : "")]);


        $count = 1;
        $priceTotal = 0;
        $price = 0;
        $amount = 0;
        $type = null;
        $singleprice = 0;
        $title = null;

        $list = [];


        foreach ($calaculationItems as $item) {
            if ($item->getProduct()) {

                $singleprice = $item->getSinglePrice();

                $amount = $item->getAmount();
                $title = $item->getProduct()->getTitle();
                $description = $item->getProduct()->getDescription();
                $price = $singleprice * $amount;
                $type = $item->getProduct()->getPriceTypeName();
            }
            if ($item->getService()) {

                $singleprice = $item->getSinglePrice();

                $amount = $item->getAmount();
                $title = $item->getService()->getTitle();
                $description = $item->getService()->getDescription();
                $type = $item->getService()->getPriceTypeName();
                $price = $singleprice * $amount;
            }

            if ($item->getFreeText()) {
                $amount = $item->getAmount();
                $singleprice = $item->getSinglePrice();
                $title = $item->getFreeText();
                $type = $item->getFreePriceType();
                $price = $singleprice * $amount;
                $description = '';
            }

            if ($item->getExtraText()) {
                $title = $title . ' ( ' . $item->getExtraText() . ' )';
            }

            if ($item->getSpecialPrice()) {
                $singleprice = $item->getSpecialPrice() / $amount;
                $price = $item->getSpecialPrice();
            }
            $priceTotal = $priceTotal + $price;

            if ($count == 25) {
                $list[] = ['title' => '',
                    'description' => '',
                    'amount' => '',
                    'pos' => '',
                    'type' => '',
                    'singleprice' => '',
                    'sum' => '',];
            }

            $list[] = ['title' => utf8_decode($title),
                'description' => utf8_decode(strip_tags($description)),
                'amount' => number_format($amount, 2, ',', '.') . ' ' . utf8_decode($type),
                'pos' => $count,
                'type' => utf8_decode($type),
                'singleprice' => number_format($singleprice, 2, ',', '.'),
                'sum' => number_format($price, 2, ',', '.')];

            $count++;
        }
        //Fill the table with products and services
        $document->MergeBlock("ps", $list);
        $netto = $priceTotal * 0.81;
        $discount = 0;
        if ($offer->getCalculation()->getContactDiscount()) {
            $discount = ($priceTotal * $offer->getContact()->getDiscount()) / 100;
        }
        if ($offer->getCalculation()->getDiscount()) {
            if ($offer->getCalculation()->getDiscountType()) {
                $discount = $discount + ($priceTotal * $offer->getCalculation()->getDiscount()) / 100;
            } else {
                $discount = $discount + $offer->getCalculation()->getDiscount();
            }

        }

        $steuer = ($priceTotal - $discount) * 0.19;

        if ($offer->getCalculation()->getNetto()) {
            $steuer = 0;
        }


        $user = $offer->getCreatedBy();
        $member = $user->getMember() ?: $user;

        if ($member instanceof User) {
            $name = $member->getUsername();
        } else {
            $name = $member->getFullName();
        }

        $document->MergeField("team", ['fullname' => utf8_decode($name)]);


        if ($offer->getPrintDate()) {
            $printDate = $offer->getPrintDate()->format('d.m.Y');
        } else {
            $printDate = $offer->getCreatedAt()->format('d.m.Y');
        }

        $document->MergeField("offer", ['printdate' => "\t" . $printDate,
            'id' => $offer->getNumber(),
            'address' => ['name' => utf8_decode($offer->getContactAddress() ? $offer->getContactAddress()->getName() : ""),
                'city' => utf8_decode($offer->getContactAddress() ? $offer->getContactAddress()->getCity() : ""),],
            'subtotal' => $priceTotal,
            'mwst' => $steuer,
            'place' => utf8_decode($place),
            'discount' => $discount,
            'total' => $offer->getCalculation()->getFinalPrice()]);

        $helper = $this->get('app.filehandle');

        $outputFile = $helper->getCompiledPath($offer->getId(), 'Modules\OrderManagementBundle\Entity\Offer');
        $document->Show(OPENTBS_FILE, $outputFile);

        $docConverter = $this->get('app.documentconverter');
        $convertedPath = $helper->getConvertedPath($offer->getId(), $offer->getNumber(), 'Modules\OrderManagementBundle\Entity\Offer', $fileformat);
        $docConverter->convertTo($outputFile, $convertedPath, $fileformat);

        $response = new BinaryFileResponse($convertedPath);
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);

        return $response;

    }
}
