<?php

namespace Modules\OrderManagementBundle\Controller;


use Core\AppBundle\Entity\Documents;
use FOS\RestBundle\Controller\Annotations as Rest;
use Modules\OrderManagementBundle\Entity\WorkingPaper;
use Modules\OrderManagementBundle\Form\WorkingPaperCustomType;
use Modules\OrderManagementBundle\Form\WorkingPaperType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class WorkingPaperController
 * @package Modules\OrderManagementBundle\Controller
 * @Route("/workingpaper")
 */
class WorkingPaperController extends Controller
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Rest\Route("/")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $workingPapers = $em->getRepository('ModulesOrderManagementBundle:WorkingPaper');

        $columns = [

            'contact' => ['active' => true,
                'label' => 'Kontakt'],
            'member' => ['active' => true,
                'label' => 'Mitarbeiter'],
            'signature' => ['active' => false,
                'label' => 'Unterschrift']];
        $columns = $this->get('app.helper')->setTableColumns('mod_order_management_workingpaper_table_columns', $columns);

        $perPage = $this->get('app.helper')->setTablePerPage('mod_order_management_workingpaper_table_rows');
        $paginator = $this->get('knp_paginator');
        $page = $request->query->getInt('page', 1);
        $pagination = $paginator->paginate($workingPapers->findAll(), $page, $perPage);

        return $this->render('ModulesOrderManagementBundle:WorkingPaper:index.html.twig', ['columns' => $columns,
            'pagination' => $pagination,
            'perPage' => $perPage]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Rest\Route("/new")
     */
    public function newAction(Request $request)
    {

        $workingPaper = new WorkingPaper();
        $workingPaper->setCustom(false);

        $form = $this->createForm(WorkingPaperType::class, $workingPaper);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($workingPaper);
            $em->flush();

            return $this->redirectToRoute('modules_ordermanagement_workingpaper_index');
        }

        return $this->render('ModulesOrderManagementBundle:WorkingPaper:new.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Rest\Route("/custom/new")
     */
    public function newCustomAction(Request $request)
    {

        $workingPaper = new WorkingPaper();
        $workingPaper->setCustom(true);

        $form = $this->createForm(WorkingPaperCustomType::class, $workingPaper);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $workingPaper->setCustomResult($request->request->get('imagedata'));
            $em->persist($workingPaper);
            $em->flush();
//
            return $this->redirectToRoute('modules_ordermanagement_workingpaper_index');
        }

        return $this->render('ModulesOrderManagementBundle:WorkingPaper:custom_new.html.twig', ['form' => $form->createView()]);
    }


    /**
     * @param Request $request
     * @param WorkingPaper $paper
     * @return JsonResponse
     * @Route("/saveSignature/{id}", options={"expose"=true})
     */
    public function saveSignatureAction(Request $request, WorkingPaper $paper)
    {
        $entity = 'Modules\OrderManagementBundle\Entity\WorkingPaper';
        $ns = "Modules\\OrderManagementBundle\\Entity\\";

        $signature = new Documents();
        $signature->setName('signatur.png');
        $signature->setEntity($entity);
        $signature->setEntry($paper->getId());
        $signature->setMime('image/png');

        //CREATE PATH FROM ENTITY PATH
        $path = str_replace('Modules\\', '', $entity);
        $path = str_replace('\\Entity\\', '/', $path);

        $path .= '/' . $paper->getId() . '/';

        $path = '../data/' . $path;
        $path = str_replace('\\', '/', $path);


        $fs = new Filesystem();

        if (!$fs->exists($path)) {
            $fs->mkdir($path);
        }

        $fs->dumpFile($path . '/signatur.png', base64_decode($request->request->get('signatur')));

        $signature->setPath($path);

        $em = $this->getDoctrine()->getManager();
        $em->persist($signature);
        $em->flush();

        $paper->setSignature($signature);

        $em->flush();

        return new JsonResponse(['status' => 'ok']);
    }

    /**
     * @param Request $request
     * @param WorkingPaper $paper
     * @Route("/signature/{id}", options={"expose"=true})
     * @return Response
     */
    public function showSignature(Request $request, WorkingPaper $paper)
    {

        $response = new Response();
        $response->headers->set('Content-Type', 'image/png');
        if ($paper->getSignature() === null) {
            $response->setContent(base64_decode('iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mNk+P+/HgAFhAJ/wlseKgAAAABJRU5ErkJggg=='));

            return $response;
        }

        $path = str_replace('\\', '/', '../data/' . $paper->getSignature()->getPath());
        $path = str_replace('//', '/', $path);
        $path = str_replace('/', DIRECTORY_SEPARATOR, $path);

        $response->setContent(file_get_contents($path . 'signatur.png'));

        return $response;

    }

    /**
     * @param Request $request
     * @param WorkingPaper $paper
     * @Route("/export/{id}", options={"expose"=true})
     * @return Response
     */
    public function showCustom(Request $request, WorkingPaper $paper)
    {

        $response = new Response();
        $response->headers->set('Content-Type', 'image/png');
        if ($paper->getCustomResult() === null) {
            $response->setContent(base64_decode('iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mNk+P+/HgAFhAJ/wlseKgAAAABJRU5ErkJggg=='));

            return $response;
        }

        $response->setContent(base64_decode(str_replace('data:image/png;base64,', '', $paper->getCustomResult())));

        return $response;

    }

    /**
     * @param Request $request
     * @param WorkingPaper $paper
     * @Route("/{id}")
     * @return Response
     */
    public function detailAction(Request $request, WorkingPaper $paper)
    {
        return $this->render('ModulesOrderManagementBundle:WorkingPaper:detail.html.twig', ['working_paper' => $paper]);

    }

    /**
     * @param Request $request
     * @param WorkingPaper $paper
     * @Route("/{id}/delete", options={"expose"=true})
     * @return RedirectResponse
     */
    public function deleteAction(Request $request, WorkingPaper $paper)
    {

        $em = $this->getDoctrine()->getManager();
        $em->remove($paper);
        $em->flush();

        return $this->redirectToRoute('modules_ordermanagement_workingpaper_index');

    }


}