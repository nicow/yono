<?php

namespace Modules\OrderManagementBundle\Controller;

use Modules\OrderManagementBundle\Entity\PriceList;
use Modules\OrderManagementBundle\Entity\PriceListProduct;
use Modules\OrderManagementBundle\Form\PriceListProductType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


/**
 * Service controller.
 * @Route("/pricelist/product")
 */
class PriceListProductController extends Controller
{

    /**
     * Creates a new PriceListProduct entity.
     * @Route("/new/{priceList}")
     * @Method({"GET", "POST"})
     * @Security("is_granted('CREATE', 'Modules\\OrderManagementBundle\\Entity\\PriceList')")
     * @param Request $request
     * @param PriceList $priceList
     * @return RedirectResponse|Response
     */
    public function newAction(Request $request, PriceList $priceList)
    {
        $product = new PriceListProduct();
        $product->setPricelist($priceList);

        $form = $this->createForm(PriceListProductType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($product);
            $em->flush();

            $this->addFlash('success', '<i class="uk-icon-check"></i> Das Produkt wurde erfolgreich der Preisliste hinzugefügt!');

            return $this->redirectToRoute('modules_ordermanagement_pricelist_details', ['pricelist' => $priceList->getId()]);

        }

        return $this->render('ModulesOrderManagementBundle:PriceListProduct:new.html.twig', ['pricelistproduct' => $product, 'pricelist' => $priceList, 'form' => $form->createView(),]);
    }

    /**
     * Edits a pricelist entity.
     * @Route("/{priceListProduct}/edit")
     * @Method({"GET", "POST"})
     * @Security("is_granted('EDIT', 'Modules\\OrderManagementBundle\\Entity\\PriceList')")
     * @param Request $request
     * @param PriceListProduct $priceListProduct
     * @return RedirectResponse|Response
     */
    public function editAction(Request $request, PriceListProduct $priceListProduct)
    {
        $form = $this->createForm(PriceListProductType::class, $priceListProduct);
        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted()) {

            $price=$request->request->get('pricelistproduct');
            $price['price']=str_replace('.','',$price['price']);
            $price['price']=str_replace(',','.',$price['price']);
            $priceSet=number_format($price['price'], 2, ".", "");
            $priceListProduct->setPrice($priceSet);

            $this->getDoctrine()->getManager()->flush();

            return new RedirectResponse($this->generateUrl('modules_ordermanagement_pricelist_details', ['pricelist' => $priceListProduct->getPricelist()->getId()]));
        }

        return $this->render('ModulesOrderManagementBundle:PriceListProduct:edit.html.twig', ['pricelistproduct' => $priceListProduct, 'form' => $form->createView()]);
    }

    /**
     * Deletes a PriceList entity.
     * @Route("/{priceListProduct}/delete")
     * @Method({"GET", "DELETE"})
     * @Security("is_granted('DELETE', 'Modules\\OrderManagementBundle\\Entity\\PriceList')")
     * @param Request $request
     * @param PriceListProduct $priceListProduct
     * @return RedirectResponse
     */
    public function deleteAction(Request $request, PriceListProduct $priceListProduct)
    {

        $id = $priceListProduct->getPricelist()->getId();

        $em = $this->getDoctrine()->getManager();
        $em->remove($priceListProduct);
        $em->flush();

        $this->addFlash('success', '<i class="uk-icon-check"></i> Das Produkt wurde erfolgreich aus der Preisliste gelöscht!');


        return $this->redirectToRoute('modules_ordermanagement_pricelist_details', ['pricelist' => $id]);
    }
}
