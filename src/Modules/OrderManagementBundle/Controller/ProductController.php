<?php

namespace Modules\OrderManagementBundle\Controller;

use Modules\OrderManagementBundle\Entity\PriceList;
use Modules\OrderManagementBundle\Entity\PriceListProduct;
use Modules\OrderManagementBundle\Entity\Product;
use Modules\OrderManagementBundle\Entity\ProductCategory;
use Modules\OrderManagementBundle\Form\ProductType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


/**
 * Service controller.
 * @Route("/product")
 */
class ProductController extends Controller
{
    /**
     * Lists all product entities.
     * @Route("/")
     * @Method({"GET", "POST"})
     * @Security("is_granted('VIEW', 'Modules\\OrderManagementBundle\\Entity\\Product')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $services = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:Product')->findAllOrdered($request->query->get('sort', 'title'), $request->query->get('direction', 'ASC'));

        $columns = $this->get('app.helper')->setTableColumns('mod_order_management_product_table_columns', ['title' => ['active' => 1, 'label' => 'Name'], 'price' => ['active' => 1, 'label' => 'Preis'], 'categories' => ['active' => 0, 'label' => 'Kategorien'], 'createdBy' => ['active' => 0, 'label' => 'erstellt von'], 'createdAt' => ['active' => 0, 'label' => 'erstellt am'], 'updatedAt' => ['active' => 0, 'label' => 'letzte Änderung'],]);
        $perPage = $this->get('app.helper')->setTablePerPage('mod_order_management_product_table_rows');
        $paginator = $this->get('knp_paginator');
        $page = $request->query->getInt('page', 1);
        $pagination = $paginator->paginate($services, $page, $perPage);

        $deleteForms = [];
        foreach ($services as $service) {
            $deleteForms[$service->getId()] = $this->createDeleteForm($service)->createView();
        }

        return $this->render('ModulesOrderManagementBundle:Product:index.html.twig', ['columns' => $columns, 'pagination' => $pagination, 'perPage' => $perPage, 'deleteForms' => $deleteForms,]);
    }

    /**
     * Creates a form to delete a product entity.
     * @param Product $product
     * @return Form The form
     */
    private function createDeleteForm(Product $product)
    {
        return $this->createFormBuilder()->setAction($this->generateUrl('modules_ordermanagement_product_delete', ['product' => $product->getId()]))->setMethod('DELETE')->getForm();
    }

    /**
     * @Route("/{product}/details")
     * @Security("is_granted('VIEW', 'Modules\\OrderManagementBundle\\Entity\\Product')")
     * @param Product $product
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function detailsAction(Request $request, Product $product)
    {
        return $this->render('ModulesOrderManagementBundle:Product:details.html.twig', ['product' => $product,]);
    }

    /**
     * @Route("/data")
     * @Security("is_granted('VIEW', 'Modules\\OrderManagementBundle\\Entity\\Product')")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function dataAction(Request $request)
    {

        $data = [];
        $productsPriceList = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:PriceList');

        /**
         * @var PriceList $list
         */
        $list = $productsPriceList->findByProduct($request->request->get('contact'), $request->request->get('product'));

        /**
         * @var PriceListProduct $item
         */
        if ($list) {
            foreach ($list->getProducts() as $item) {
                if ($item->getProduct()->getId() == $request->request->get('product')) {
                    $data['price'] = $item->getPrice();
                }
            }
        }

        $productEnity = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:Product')->find($request->request->get('product'));


        if (!isset($data['price'])) {
            $data['price'] = $productEnity->getPrice();
        }
        $data['brutto'] = $productEnity->getTax();

        return new JsonResponse(json_encode($data));
    }

    /**
     * Creates a new Product entity.
     * @Route("/new")
     * @Route("/category/{category}/new")
     * @Method({"GET", "POST"})
     * @Security("is_granted('CREATE', 'Modules\\OrderManagementBundle\\Entity\\Product')")
     * @param ProductCategory $category
     * @param Request $request
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request, ProductCategory $category = null)
    {
        $product = new Product();
        if ($category) {
            $product->addCategory($category);
        }
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $price=$request->request->get('product');
            $price['price']=str_replace('.','',$price['price']);
            $price['price']=str_replace(',','.',$price['price']);
            $priceSet=number_format($price['price'], 2, ".", "");
            $product->setPrice($priceSet);
            
            $em = $this->getDoctrine()->getManager();
            $em->persist($product);
            $em->flush();

            $this->addFlash('success', '<i class="uk-icon-check"></i> Das Produkt wurde erfolgreich angelegt!');

            return $this->redirectToRoute('modules_ordermanagement_product_details', ['product' => $product->getId()]);

        }


        return $this->render('ModulesOrderManagementBundle:Product:new.html.twig', ['category' => $category, 'product' => $product, 'form' => $form->createView(),]);
    }

    /**
     * Edits a product entity.
     * @Route("/edit/{product}")
     * @Method({"GET", "POST"})
     * @Security("is_granted('EDIT', 'Modules\\OrderManagementBundle\\Entity\\Product')")
     * @param Request $request
     * @param Product $product
     * @return RedirectResponse|Response
     */
    public function editAction(Request $request, Product $product)
    {
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted()) {
            $price=$request->request->get('product');
            $price['price']=str_replace('.','',$price['price']);
            $price['price']=str_replace(',','.',$price['price']);
            $priceSet=number_format($price['price'], 2, ".", "");
            $product->setPrice($priceSet);
            $this->getDoctrine()->getManager()->flush();

            return new RedirectResponse($this->generateUrl('modules_ordermanagement_product_details', ['product' => $product->getId()]));
        }

        return $this->render('ModulesOrderManagementBundle:Product:edit.html.twig', ['product' => $product, 'form' => $form->createView()]);
    }

    /**
     * Deletes a Product entity.
     * @Route("/{product}/delete")
     * @Method({"GET", "DELETE"})
     * @Security("is_granted('DELETE', 'Modules\\OrderManagementBundle\\Entity\\Product') or is_granted('DELETE', product)")
     * @param Request $request
     * @param Product $product
     * @return RedirectResponse
     */
    public function deleteAction(Request $request, Product $product)
    {
        $form = $this->createDeleteForm($product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($product);
            $em->flush();

            $this->addFlash('success', '<i class="uk-icon-check"></i> Das Produkt wurde erfolgreich gelöscht!');
        }


        return $this->redirectToRoute('modules_ordermanagement_product_index');
    }
}
