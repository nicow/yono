<?php

namespace Modules\OrderManagementBundle\Form;

use Doctrine\ORM\EntityRepository;
use Modules\OrderManagementBundle\Entity\ProductCategory;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductCategoryType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $category = $builder->getData();

        $builder->add('name', TextType::class, ['label' => 'Name',])->add('parent', EntityType::class, ['label' => 'Elternkategorie', 'choice_label' => 'name', 'query_builder' => function (EntityRepository $er) use ($category) {
            $qb = $er->createQueryBuilder('c');

            // if we are editing an existing category, exclude it
            if ($category->getId()) {
                $qb->where('c != :category')->setParameter('category', $category);
            }

            return $qb;
        }, 'class' => ProductCategory::class, 'required' => false])->add('showInMenu', null, ['label' => 'Im Menü anzeigen?']);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => 'Modules\OrderManagementBundle\Entity\ProductCategory', 'csrf_protection' => false]);
    }
}
