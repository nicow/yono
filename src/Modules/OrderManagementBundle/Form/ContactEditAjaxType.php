<?php

namespace Modules\OrderManagementBundle\Form;

use Doctrine\ORM\EntityManager;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Modules\ContactBundle\Entity\Contact;
use Modules\OrderManagementBundle\Entity\PriceList;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactEditAjaxType extends AbstractType
{


    public function __construct()
    {

    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        $builder->add('contact', null, ['label' => 'Kunde', 'required' => true, 'placeholder' => 'Kunden auswählen']);

        $formModifier = function (FormInterface $form, Contact $contact = null) {
            $persons = null === $contact ? array() : $contact->getPersons();
            $address = null === $contact ? array() : $contact->getAddresses();

            $form->add('contactPerson', EntityType::class, array('class' => 'Modules\ContactBundle\Entity\Person', 'placeholder' => 'Ansprechpartner auswählen', 'label' => 'Ansprechpartner', 'required' => false, 'choices' => $persons,'attr'=>['class'=>'edit_contact_person']));

            $form->add('contactAddress', EntityType::class, array('class' => 'Modules\ContactBundle\Entity\Address', 'placeholder' => 'Adresse auswählen', 'label' => 'Adresse', 'choices' => $address, 'required' => false,'attr'=>['class'=>'edit_contact_adress']));
        };

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($formModifier) {
            $data = $event->getData();
            $formModifier($event->getForm(), $data->getContact());
        });

        $builder->get('contact')->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) use ($formModifier) {
            $data = $event->getForm()->getData();
            $formModifier($event->getForm()->getParent(), $data);
        });

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => 'Modules\OrderManagementBundle\Entity\Invoice', 'csrf_protection' => false]);
    }

}
