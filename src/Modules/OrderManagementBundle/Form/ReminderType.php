<?php

namespace Modules\OrderManagementBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class ReminderType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('version', ChoiceType::class, ['label' => 'Mahnstufe',
            'required' => true,
            'choices' => ['1. Mahnung' => 1,
                '2. Mahnung' => 2],
            'attr' => ['value' => $options['version']]]);
        $builder->add('invoice', null, ['label' => 'Rechnung',
            'required' => true,
            'placeholder' => 'Rechnung auswählen']);
        $builder->add('fee', IntegerType::class, ['label' => 'Mahngebühr',
            'required' => false,
            'attr' => ['step' => '0.01']]);
        $builder->add('deadline', DateType::class, ['label' => 'Frist',
            'widget' => 'single_text',
            'format' => 'd.M.y',
            'required' => true]);
        $builder->add('reminderDate', DateType::class, ['label' => 'Datum der Mahnung',
            'widget' => 'single_text',
            'format' => 'd.M.y',
            'required' => true]);

    }


    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => 'Modules\OrderManagementBundle\Entity\Reminder',
            'csrf_protection' => false,
            'invoice' => false,
            'version' => false]);
    }

}