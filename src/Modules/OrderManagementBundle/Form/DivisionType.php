<?php

namespace Modules\OrderManagementBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DivisionType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, ["label" => "Name"])
            ->add('tag', null, ["label" => "Tag"])
            ->add('offerTemplate', FileType::class, [
                'label' => "Angebots Vorlage",
                'data' => null,
                'attr' => [
                    'accept' => 'application/vnd.oasis.opendocument.text'
                ]
            ])
            ->add('orderTemplate', FileType::class, [
                'label' => "Auftrags Vorlage",
                'data' => null,
                'attr' => [
                    'accept' => 'application/vnd.oasis.opendocument.text'
                ]
            ])
            ->add('invoiceTemplate', FileType::class, [
                'label' => "Rechnungs Vorlage",
                'data' => null,
                'attr' => [
                    'accept' => 'application/vnd.oasis.opendocument.text'
                ]
            ])
            ->add('invoiceNettoTemplate', FileType::class, [
                'label' => "Netto Rechnungs Vorlage",
                'data' => null,
                'attr' => [
                    'accept' => 'application/vnd.oasis.opendocument.text'
                ]
            ])
            ->add('reminderTemplate', FileType::class, [
                'label' => "Mahnungs Vorlage",
                'data' => null,
                'attr' => [
                    'accept' => 'application/vnd.oasis.opendocument.text'
                ]
            ])
            ->add('creditTemplate', FileType::class, [
                'label' => "Gutschrift Template",
                'data' => null,
                'attr' => [
                    'accept' => 'application/vnd.oasis.opendocument.text'
                ]
            ])
            ->add('workingPaperTemplate', FileType::class, [
                'label' => "Arbeitszettel Vorlage",
                'required' => false,
                'data' => null,
                'attr' => [
                    'accept' => 'image/png'
                ]
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array('data_class' => 'Modules\OrderManagementBundle\Entity\Division'));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'modules_ordermanagementbundle_division';
    }


}
