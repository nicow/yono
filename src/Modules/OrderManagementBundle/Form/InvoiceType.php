<?php

namespace Modules\OrderManagementBundle\Form;

use Doctrine\ORM\EntityManager;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Modules\ContactBundle\Form\AddressType;
use Modules\ContactBundle\Form\ContactType;
use Modules\ContactBundle\Form\PersonType;
use Modules\OrderManagementBundle\Entity\PriceList;
use Modules\ProjectBundle\Form\ProjectType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class InvoiceType extends AbstractType
{


    /** @var \Doctrine\ORM\EntityManager */
    private $em;


    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        switch ($options['flow_step']) {
            case 1:

                $builder->add('contact', null, ['label' => 'Kunde wählen oder neuen anlegen',
                    'placeholder' => 'neuen Kunden anlegen']);

                break;
            case 2:
                $builder->add('contact', ContactType::class, ['label' => false, 'required' => true]);
                break;
            case 3:
                $builder->add('contactPerson', null, ['label' => 'Ansprechpartner',
                    'required' => false,
                    'placeholder' => 'neuen Ansprechpartner anlegen',
                    'choices' => $options['data']->contact->persons]);
                $builder->add('contactAddress', null, ['label' => 'Adresse',
                    'required' => false,
                    'placeholder' => 'neue Adresse anlegen',
                    'choices' => $options['data']->contact->addresses]);
                $builder->add('cPSkip', CheckboxType::class, ['label' => 'Keinen Ansprechpartner',
                    'mapped' => false,
                    'required' => false]);
                $builder->add('cASkip', CheckboxType::class, ['label' => 'Keine Adresse',
                    'mapped' => false,
                    'required' => false]);
                break;
            case 4:
                $builder->add('contactPerson', PersonType::class, ['label' => false, 'required' => true]);
                break;
            case 5:
                $builder->add('contactAddress', AddressType::class, ['label' => false, 'required' => true]);
                break;
            case 6:
                $builder->add('title', null, ['label' => 'Titel'])->add("division", null, ["label" => "Geschäftsbereich",
                    "required" => true,
                    'placeholder' => 'Geschäftsbereich wählen'])->add('description', CKEditorType::class, ['label' => 'Beschreibung',
                        'required' => false])->add('termsOfPayment', ChoiceType::class, ['label' => 'Zahlungsbedingungen',
                        'mapped' => false,
                        'required' => false,
                        'placeholder' => false,
                        'choices' => $this->buildChoicesPayment()]);
                $builder->add('project', null, ['label' => 'Projekt',
                    'placeholder' => 'kein Projekt',
                    'choices' => $this->buildChoicesProject($options['data']->contact)]);
                $builder->add('pSkip', CheckboxType::class, ['label' => 'Kein Projekt',
                    'mapped' => false,
                    'required' => false]);
                $builder->add('netto', CheckboxType::class, ['label' => 'Netto Rechnung',
                    'mapped' => false,
                    'required' => false]);
                $builder->add('monthlyFee', ChoiceType::class, ['label' => 'Monatspauschale','placeholder' => 'Keine Monatspauschale', 'mapped' => false, 'required' => false,
                    'choices' => ['Januar' => 1, 'Februar' => 2, 'März' => 3, 'April' => 4, 'Mai' => 5, 'Juni' => 6, 'Juli' => 7, 'August' => 8, 'September' => 9, 'Oktober' => 10, 'November' => 11, 'Dezember' => 12]]);
                break;
            case 7:
                $builder->add('project', ProjectType::class, ['label' => false, 'required' => false]);
                break;
            case 8:
                $builder->add('services', CollectionType::class, ['label' => false,
                    'mapped' => false,
                    'entry_type' => ServiceSelectType::class,
                    'allow_add' => true,
                    'allow_delete' => true,
                    'by_reference' => false,
                    'entry_options' => ['label' => false,
                        'products' => $this->buildChoicesProducts($options['data']->contact),
                        'services' => $this->buildChoicesServices($options['data']->contact),]]);
                $builder->add('noContactDiscount', CheckboxType::class, ['attr' => ['class' => 'noContactDiscount'],
                    'label' => 'Keinen Kundenrabatt',
                    'mapped' => false,
                    'required' => false]);
                break;
            case 9:
                $builder->add('discount', IntegerType::class, ['label' => 'Rabatt',
                    'required' => false,
                    'mapped' => false,
                    'attr' => ['min' => 0,
                        'data-uk-tooltip',
                        'title' => 'Rabatt wird von Gesamtsumme abgezogen (optional)',
                        'class' => 'invoice-total-discount']]);
                $builder->add('discountType', CheckboxType::class, ['label' => 'prozentualer Rabatt',
                    'mapped' => false,
                    'required' => false]);
                break;
        }

    }

    protected function buildChoicesPayment()
    {
        $termsOfPaymentRepo = $this->em->getRepository('ModulesOrderManagementBundle:TermsOfPayment');

        $termsOfPayment = $termsOfPaymentRepo->findAll();

        $choice = array();
        foreach ($termsOfPayment as $item) {
            $choice[$item->getName()] = $item->getId();
        }

        return $choice;
    }

    protected function buildChoicesProject($contact)
    {
        $projectRepo = $this->em->getRepository('ModulesProjectBundle:Project');

        $projects = $projectRepo->findBy(['contact' => $contact]);

        return $projects;
    }

    protected function buildChoicesProducts($contact)
    {
        $products = $this->em->getRepository('ModulesOrderManagementBundle:Product');
        $allproducts = $products->findAll();

        $productsPriceList = $this->em->getRepository('ModulesOrderManagementBundle:PriceList');
        /**
         * @var PriceList $list
         */
        $list = $productsPriceList->findOneBy(['id' => $contact->getPricelist()]);

        $productList = [];

        if ($list) {
            foreach ($list->getProducts() as $pricelist) {
                $productList[$pricelist->getProduct()->getTitle() . ' / ' . $pricelist->getPrice() . ' € je ' . $pricelist->getProduct()->getPriceTypeName() . ' - ' . $pricelist->getProduct()->getTax() . '% MwSt.'] = $pricelist->getProduct()->getId();
            }
        }

        foreach ($allproducts as $product) {
            if (!in_array($product->getId(), $productList)) {
                $productList[$product->getTitle() . ' / ' . $product->getPrice() . ' € je ' . $product->getPriceTypeName() . ' - ' . $product->getTax() . '% MwSt.'] = $product->getId();
            }
        }

        return $productList;
    }

    protected function buildChoicesServices($contact)
    {
        $services = $this->em->getRepository('ModulesOrderManagementBundle:Service');
        $allservices = $services->findAll();

        $servicePriceList = $this->em->getRepository('ModulesOrderManagementBundle:PriceList');
        /**
         * @var PriceList $list
         */
        $list = $servicePriceList->findOneBy(['id' => $contact->getPricelist()]);

        $serviceList = [];

        if ($list) {
            foreach ($list->getServices() as $pricelist) {
                $serviceList[$pricelist->getService()->getTitle() . ' / ' . $pricelist->getPrice() . ' € je ' . $pricelist->getService()->getPriceTypeName() . ' - ' . $pricelist->getService()->getTax() . '% MwSt.'] = $pricelist->getService()->getId();
            }
        }
        foreach ($allservices as $service) {
            if (!in_array($service->getId(), $serviceList)) {
                $serviceList[$service->getTitle() . ' / ' . $service->getPrice() . ' € je ' . $service->getPriceTypeName() . ' - ' . $service->getTax() . '% MwSt.'] = $service->getId();
            }
        }

        return $serviceList;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => 'Modules\OrderManagementBundle\Entity\Invoice',
            'csrf_protection' => false,]);
    }


}
