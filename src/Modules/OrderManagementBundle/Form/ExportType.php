<?php

namespace Modules\OrderManagementBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ExportType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('bills', CheckboxType::class, ['label' => "Rechnungen", 'required' => false, 'attr' => ['required' => false]]);
        $builder->add('orders', CheckboxType::class, ['label' => "Aufträge", 'required' => false]);
        $builder->add('offers', CheckboxType::class, ['label' => "Angebote", 'required' => false]);

        $builder->add('start', DateType::class, ['label' => "Von", 'widget' => 'single_text', 'format' => 'd.M.y', 'data' => null]);
        $builder->add('end', DateType::class, ['label' => "Bis", 'required' => false, 'widget' => 'single_text', 'format' => 'd.M.y', 'data' => null]);

        $builder->add('customer', EntityType::class, ['label' => "Kunde", 'required' => false, 'class' => 'Modules\ContactBundle\Entity\Contact', 'placeholder' => 'Alle Kunden']);

        $builder->add('division', EntityType::class, ['label' => "Geschäftsbereich", 'required'=>false ,'class' => 'Modules\OrderManagementBundle\Entity\Division','placeholder'=>'Alle', 'choice_label' => 'name']);

        $builder->add('submit', SubmitType::class, ['label' => 'Als CSV exportieren']);
    }


    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => null, 'csrf_protection' => false]);
    }

}
