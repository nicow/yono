<?php

namespace Modules\OrderManagementBundle\Form;

use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class WorkingPaperPositionType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', ChoiceType::class, [
                'choices' => [
                    'Freitext' => 'text',
                    'Jobticket' => 'jobticket',
                    'Leistung' => 'service'
                ],
                'placeholder' => 'Bitte wählen Sie eine Position aus',
                'mapped' => false,
                'attr' => [
                    'class' => 'position_select'
                ],
                'label' => ''
            ])
            ->add('jobticket', EntityType::class, ['label'=>'Jobticket', 'required'=> false, 'class'=>'Modules\OrderManagementBundle\Entity\JobTicket','choice_label'=>'title'])
            ->add('service', EntityType::class, ['label'=>'Leistung', 'required'=> false, 'class'=>'Modules\OrderManagementBundle\Entity\Service'])
            ->add('text', CKEditorType::class, ['label'=>'Freitext','required'=> false])
        ;

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => 'Modules\OrderManagementBundle\Entity\WorkingPaperPosition', 'csrf_protection' => false]);
    }
}