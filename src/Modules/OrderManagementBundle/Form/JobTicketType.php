<?php

namespace Modules\OrderManagementBundle\Form;

use Doctrine\ORM\EntityManager;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Modules\OrderManagementBundle\Entity\Orders;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class JobTicketType extends AbstractType
{

    /** @var \Doctrine\ORM\EntityManager */
    private $em;


    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }


    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $dataVisile = false;
        if ($options['data']->getVisible() == 1) {
            $dataVisile = true;
        }

        $builder->add('title', null, ['label' => 'Titel',
            'required' => true])->add('description', CKEditorType::class, ['label' => 'Job Info']);

        $builder->add('order', null, ['label' => 'Auftrag', 'required' => false, 'placeholder' => 'Auftrag auswählen', 'choices' => $this->getOrder()]);

        $formModifier = function (FormInterface $form, Orders $order = null) {
            $persons = null === $order ? array() : $order->getContact()->getPersons();

            $form->add('contactPerson', EntityType::class, array('class' => 'Modules\ContactBundle\Entity\Person',
                'placeholder' => 'Ansprechpartner auswählen',
                'label' => 'Ansprechpartner',
                'required' => false,
                'choices' => $persons));
        };

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($formModifier) {
            $data = $event->getData();
            $formModifier($event->getForm(), $data->getOrder());
        });

        $builder->get('order')->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) use ($formModifier) {
            $data = $event->getForm()->getData();
            $formModifier($event->getForm()->getParent(), $data);
        });

        $builder->add('visible', CheckboxType::class, ['label' => 'Anzeigen bis Jobticket erledigt ist',
            'required' => false,
            'data' => $dataVisile]);

        $builder->add('project', null, ['label' => 'Projekt',
            'required' => false,
            'placeholder' => 'Projekt auswählen',
            'choices' => $this->getProject()]);

        $builder->add('fixDate', DateType::class, ['label' => 'Fixes Datum',
            'widget' => 'single_text',
            'format' => 'd.M.y',
            'data' => null,
            'required' => false])->add('members', null, ['label' => 'Mitarbeiter',
                'required' => true,
                'choices' => $this->getMembers(),
                'constraints' => [new Assert\Count(['min' => 1,
                    'minMessage' => 'Es muss mindestens ein Mitarbeiter ausgewählt werden.'])]])->add('status', ChoiceType::class, ['label' => 'Status',
                'choices' => ['geplant' => 0, 'offen' => 1, 'in Arbeit' => 2, 'dringend' => 3, 'erledigt' => 4],
                'required' => true,]);

    }

    protected function getMembers()
    {
        $memberRepo = $this->em->getRepository('ModulesTeamBundle:Member');

        $member = $memberRepo->findByNotHidden();

        return $member;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => 'Modules\OrderManagementBundle\Entity\JobTicket',
            'csrf_protection' => false,
            'order' => false]);
    }

    protected function getProject()
    {
        $projectRepo = $this->em->getRepository('ModulesProjectBundle:Project');

        $projects = $projectRepo->findAllOpen();

        return $projects;
    }

    protected function getOrder()
    {
        $orderRepo = $this->em->getRepository('ModulesOrderManagementBundle:Orders');

        $order = $orderRepo->findAllOpen();

        return $order;
    }

}
