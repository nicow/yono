<?php

namespace Modules\OrderManagementBundle\Form;

use function PHPSTORM_META\type;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class WorkingPaperType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', null, ['label' => 'Titel'])
            ->add('contact', EntityType::class, ['label' => 'Kontakt', 'class'=> 'Modules\ContactBundle\Entity\Contact'])
            ->add('order', EntityType::class,['label'=> 'Auftrag', 'class'=> 'Modules\OrderManagementBundle\Entity\Orders'])
            ->add('member', EntityType::class,['label'=> 'Mitarbeiter', 'class'=> 'Modules\TeamBundle\Entity\Member'])
            ->add('positions', CollectionType::class, ['label' => false, 'entry_type' => WorkingPaperPositionType::class, 'allow_add' => true, 'allow_delete' => true, 'by_reference' => false, 'entry_options' => ['label' => false]])
            ->add("submit", SubmitType::class,['label'=>'Anlegen', 'attr' => ['class' => 'uk-button-success']])
        ;

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => 'Modules\OrderManagementBundle\Entity\WorkingPaper', 'csrf_protection' => false]);
    }
}