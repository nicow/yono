<?php

namespace Modules\OrderManagementBundle\Form;

use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Modules\OrderManagementBundle\Entity\Product;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $tax = 19;
        $price=0;
        if ($options['data']->getTax()) {
            $tax = $options['data']->getTax();
        }
        if ($options['data']->getPrice()) {
            $price =  number_format($options['data']->getPrice(), 2, ',', '.');
        }
        $builder->add('title', null, ['label' => 'Titel'])//            ->add('categories', EntityType::class, [
        //                'class' => 'Modules\OrderManagementBundle\Entity\ProductCategory',
        //                'label' => 'Kategorie',
        //                'choice_label' => 'name',
        //                'required' => false
        //            ])
        ->add('categories', null, ['label' => 'Kategorien'])->add('priceType', ChoiceType::class, ['label' => 'Preiseinheit',
            'choices' => ['Stück' => Product::TYPE_PIECE,
                'Quadratmeter' => Product::TYPE_SQUAREMETERS,
                'Pauschale' => Product::TYPE_FLATRATE,
                'Kubikmeter' => Product::TYPE_CUBICMETER,
                'Laufender Meter' => Product::TYPE_RUNNINGMETERS]])->add("tax", TextType::class, ['label' => "MwSt.",
            'data' => $tax])->add('description', CKEditorType::class, ['label' => 'Beschreibung'])->add('price', TextType::class, ['label' => 'Nettopreis', 'data' => $price,
            'attr' => ['step' => '0.01',
                'size' => 15,]])->add('deliveryTime', TextType::class, ['label' => 'Lieferzeit',
            'attr' => ['size' => 15]])->add('shippingCosts', TextType::class, ['label' => 'Lieferkosten',
            'attr' => ['size' => 15]]);

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => 'Modules\OrderManagementBundle\Entity\Product',
            'csrf_protection' => false]);
    }
}
