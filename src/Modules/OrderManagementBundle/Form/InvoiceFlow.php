<?php
/**
 * Created by PhpStorm.
 * User: tim
 * Date: 12.04.17
 * Time: 11:25
 */

namespace Modules\OrderManagementBundle\Form;

use Craue\FormFlowBundle\Event\GetStepsEvent;
use Craue\FormFlowBundle\Event\PostBindFlowEvent;
use Craue\FormFlowBundle\Event\PostBindRequestEvent;
use Craue\FormFlowBundle\Event\PostBindSavedDataEvent;
use Craue\FormFlowBundle\Event\PostValidateEvent;
use Craue\FormFlowBundle\Event\PreBindEvent;
use Craue\FormFlowBundle\Form\FormFlow;
use Craue\FormFlowBundle\Form\FormFlowEvents;
use Craue\FormFlowBundle\Form\FormFlowInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormTypeInterface;
use Symfony\Component\HttpFoundation\Session\Session;

class InvoiceFlow extends FormFlow implements EventSubscriberInterface
{
    protected $revalidatePreviousSteps = false;

    /**
     * @var FormTypeInterface
     */
    protected $formType;

    protected $session;


    public function __construct(Session $session)
    {
        $this->session = $session;
    }

    public static function getSubscribedEvents()
    {
        return array(FormFlowEvents::PRE_BIND => 'onPreBind',
            FormFlowEvents::GET_STEPS => 'onGetSteps',
            FormFlowEvents::POST_BIND_SAVED_DATA => 'onPostBindSavedData',
            FormFlowEvents::POST_BIND_FLOW => 'onPostBindFlow',
            FormFlowEvents::POST_BIND_REQUEST => 'onPostBindRequest',
            FormFlowEvents::POST_VALIDATE => 'onPostValidate',);
    }

    public function setFormType(FormTypeInterface $formType)
    {

        $this->formType = $formType;

    }

    public function setEventDispatcher(EventDispatcherInterface $dispatcher)
    {
        parent::setEventDispatcher($dispatcher);
        $dispatcher->addSubscriber($this);
    }

    public function getFormOptions($step, array $options = array())
    {
        $options = parent::getFormOptions($step, $options);

        return $options;
    }

    public function onPreBind(PreBindEvent $event)
    {

    }

    public function onGetSteps(GetStepsEvent $event)
    {
        // ...
    }

    public function onPostBindSavedData(PostBindSavedDataEvent $event)
    {
    }

    public function onPostBindFlow(PostBindFlowEvent $event)
    {

    }

    public function onPostBindRequest(PostBindRequestEvent $event)
    {
        // ...
    }

    public function onPostValidate(PostValidateEvent $event)
    {
        // ...
    }

    protected function loadStepsConfig()
    {
        return array(['label' => 'Kunde auswählen', 'form_type' => InvoiceType::class,],
            ['label' => 'Kunde anlegen',
                'form_type' => InvoiceType::class,
                'skip' => function ($estimatedCurrentStepNumber, FormFlowInterface $flow) {
                    return $estimatedCurrentStepNumber > 1 && $flow->getFormData()->getContact();
                },],
            ['label' => 'Adresse & Ansprechpartner', 'form_type' => InvoiceType::class,],
            ['label' => 'Ansprechpartner anlegen',
                'form_type' => InvoiceType::class,
                'skip' => function ($estimatedCurrentStepNumber, FormFlowInterface $flow) {
                    $data = $this->session->get('invoice_skip');

                    return $estimatedCurrentStepNumber > 3 && ($flow->getFormData()->getContactPerson() || isset($data['invoice']['cPSkip']));

                },],
            ['label' => 'Adresse anlegen',
                'form_type' => InvoiceType::class,
                'skip' => function ($estimatedCurrentStepNumber, FormFlowInterface $flow) {
                    $data = $this->session->get('invoice_skip');

                    return $estimatedCurrentStepNumber > 3 && ($flow->getFormData()->getContactAddress() || isset($data['invoice']['cASkip']));
                },],
            [

                'label' => 'Interne Daten',
                'form_type' => InvoiceType::class,],
            ['label' => 'Projekt anlegen',
                'form_type' => InvoiceType::class,
                'skip' => function ($estimatedCurrentStepNumber, FormFlowInterface $flow) {
                    $data = $this->session->get('invoice_project_skip');

                    return $estimatedCurrentStepNumber > 6 && ($flow->getFormData()->getProject() || isset($data['invoice']['pSkip']));
                },],
            ['label' => 'Rechnungspositionen', 'form_type' => InvoiceType::class,],
            ['label' => 'Zusammenfassung', 'form_type' => InvoiceType::class,]);
    }
}