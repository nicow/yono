<?php

namespace Modules\OrderManagementBundle\Form;

use Doctrine\ORM\EntityManager;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Modules\ContactBundle\Entity\Contact;
use Modules\OrderManagementBundle\Entity\PriceList;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CreditEditType extends AbstractType
{

    /** @var \Doctrine\ORM\EntityManager */
    private $em;


    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('contact', null, ['label' => 'Kunde', 'required' => true, 'placeholder' => 'Kunden auswählen']);

        $formModifier = function (FormInterface $form, Contact $contact = null) {
            $persons = null === $contact ? array() : $contact->getPersons();
            $address = null === $contact ? array() : $contact->getAddresses();

            $form->add('contactPerson', EntityType::class, array('class' => 'Modules\ContactBundle\Entity\Person',
                'placeholder' => 'Ansprechpartner auswählen',
                'label' => 'Ansprechpartner',
                'required' => false,
                'choices' => $persons));

            $form->add('contactAddress', EntityType::class, array('class' => 'Modules\ContactBundle\Entity\Address',
                'placeholder' => 'Adresse auswählen',
                'label' => 'Adresse',
                'choices' => $address,
                'required' => false));
        };

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($formModifier) {
            $data = $event->getData();
            $formModifier($event->getForm(), $data->getContact());
        });

        $builder->get('contact')->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) use ($formModifier) {
            $data = $event->getForm()->getData();
            $formModifier($event->getForm()->getParent(), $data);
        });

        $builder->add('title', null, ['label' => 'Titel',
            'required' => true])->add("division", null, ["label" => "Geschäftsbereich",
            "required" => true])->add('description', CKEditorType::class, ['label' => 'Beschreibung',
            'required' => false]);

        $builder->add('project', null, ['label' => 'Projekt',
            'placeholder' => 'kein Projekt',
            'choices' => $this->buildChoicesProject($options['data']->contact)]);

    }

    protected function buildChoicesProject($contact)
    {
        $projectRepo = $this->em->getRepository('ModulesProjectBundle:Project');

        $projects = $projectRepo->findBy(['contact' => $contact]);

        return $projects;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => 'Modules\OrderManagementBundle\Entity\Invoice',
            'csrf_protection' => false]);
    }

    protected function buildChoicesProducts($contact)
    {
        $products = $this->em->getRepository('ModulesOrderManagementBundle:Product');
        $allproducts = $products->findAll();

        $productsPriceList = $this->em->getRepository('ModulesOrderManagementBundle:PriceList');
        /**
         * @var PriceList $list
         */
        $list = $productsPriceList->findOneBy(['id' => $contact->getPricelist()]);

        $productList = [];

        if ($list) {
            foreach ($list->getProducts() as $pricelist) {
                $productList[$pricelist->getProduct()->getTitle() . ' / ' . $pricelist->getPrice() . ' € je ' . $pricelist->getProduct()->getPriceTypeName() . ' - ' . $pricelist->getProduct()->getTax() . '% MwSt.'] = $pricelist->getProduct()->getId();
            }
        }

        foreach ($allproducts as $product) {
            if (!in_array($product->getId(), $productList)) {
                $productList[$product->getTitle() . ' / ' . $product->getPrice() . ' € je ' . $product->getPriceTypeName() . ' - ' . $product->getTax() . '% MwSt.'] = $product->getId();
            }
        }

        return $productList;
    }

    protected function buildChoicesServices($contact)
    {
        $services = $this->em->getRepository('ModulesOrderManagementBundle:Service');
        $allservices = $services->findAll();

        $servicePriceList = $this->em->getRepository('ModulesOrderManagementBundle:PriceList');
        /**
         * @var PriceList $list
         */
        $list = $servicePriceList->findOneBy(['id' => $contact->getPricelist()]);

        $serviceList = [];

        if ($list) {
            foreach ($list->getServices() as $pricelist) {
                $serviceList[$pricelist->getService()->getTitle() . ' / ' . $pricelist->getPrice() . ' € je ' . $pricelist->getService()->getPriceTypeName() . ' - ' . $pricelist->getService()->getTax() . '% MwSt.'] = $pricelist->getService()->getId();
            }
        }
        foreach ($allservices as $service) {
            if (!in_array($service->getId(), $serviceList)) {
                $serviceList[$service->getTitle() . ' / ' . $service->getPrice() . ' € je ' . $service->getPriceTypeName() . ' - ' . $service->getTax() . '% MwSt.'] = $service->getId();
            }
        }

        return $serviceList;
    }

}
