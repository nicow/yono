<?php

namespace Modules\OrderManagementBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FreeTextSelectType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('textFree', TextType::class, ['label' => 'Bezeichnung',
                'mapped' => false,
                'attr' => ['class' => 'om_free_text_amount']])->add('taxFree', TextType::class, ['label' => 'MwSt',
                'mapped' => false,
                'attr' => ['step' => '1.00',
                    'class' => 'om_free_tax',
                    'data' => '19',
                    'readonly' => true]])->add('amountFree', IntegerType::class, ['label' => 'Anzahl',
                'mapped' => false,
                'attr' => ['step' => '1.00', 'class' => 'om_free_amount'],
                'data' => '1'])->add('priceSingleFree', IntegerType::class, ['label' => 'Einzelpreis',
                'mapped' => false,
                'attr' => ['step' => '0.01',
                    'class' => 'om_free_price_single',
                    'readonly' => false]])->add('priceFreeType', ChoiceType::class, ['label' => 'Preiseinheit',
                'placeholder' => false,
                'required' => false,
                'mapped' => false,
                'attr' => ['class' => 'om_free_price_type'],
                'choices' => ['psch.' => 'psch.',
                    'Stk.' => 'Stk.',
                    'Std.' => 'Std.',
                    'Lfm' => 'Lfm',
                    'm²' => 'm²',
                    'm³' => 'm³',
                    'kg' => 'kg',
                    't' => 't',
                    'ltr' => 'ltr',
                    'm' => 'm']])->add('priceFree', TextType::class, ['label' => 'Nettopreis',
                'mapped' => false,
                'attr' => ['step' => '0.01', 'class' => 'om_free_price om_position_price', 'readonly' => true]]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => 'Modules\OrderManagementBundle\Entity\Service',
            'csrf_protection' => false,
            'products' => [],
            'services' => []]);
    }


}
