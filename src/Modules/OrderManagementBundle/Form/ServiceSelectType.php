<?php

namespace Modules\OrderManagementBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ServiceSelectType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('positionType', ChoiceType::class, array('label' => 'Art der Position',
            'mapped' => false,
            'choices' => array('Art wählen' => null,
                'Freitext' => 'om_free_select_container',
                'Leistung' => 'om_service_select_container',
                'Produkt' => 'om_product_select_container',),
            'choices_as_values' => true, // this will keep the same functionality as before
            'choice_value' => function ($value) {
                return $value;
            },
            'attr' => ['class' => 'om_select_position_type']))->add('service', ChoiceType::class, ['label' => 'Leistung',
            'mapped' => false,
            'placeholder' => 'Leistung wählen',
            'choices' => $options['services'],
            'attr' => ['class' => 'om_select_service']])->add('product', ChoiceType::class, ['label' => 'Produkt',
            'mapped' => false,
            'data_class' => 'Modules\OrderManagementBundle\Form\ProductType',
            'placeholder' => 'Produkt wählen',
            'choices' => $options['products'],
            'attr' => ['class' => 'om_select_product']])->add('amount', IntegerType::class, ['label' => 'Anzahl',
            'mapped' => false,
            'attr' => ['step' => '0.01', 'class' => 'om_service_amount'],
            'data' => '1'])->add('price', TextType::class, ['label' => 'Nettopreis',
            'mapped' => false,
            'attr' => [
                'class' => 'om_service_price om_position_price',
                'readonly' => true]])->add('specialPrice', TextType::class, ['label' => 'Sonderpreis',
            'mapped' => false,
            'required' => false,
            'attr' => [
                'class' => 'om_product_special_price om_position_special_price',
                'data-uk-tooltip',
                'title' => 'Preis nur für dieses Angebot']])->add('amountProduct', IntegerType::class, ['label' => 'Anzahl',
            'mapped' => false,
            'attr' => ['step' => '0.01', 'class' => 'om_product_amount'],
            'data' => '1'])->add('priceProduct', TextType::class, ['label' => 'Nettopreis',
            'mapped' => false,
            'attr' => [
                'class' => 'om_product_price om_position_price',
                'readonly' => true]])->add('specialPriceProduct', TextType::class, ['label' => 'Sonderpreis',
            'mapped' => false,
            'required' => false,
            'attr' => [
                'class' => 'om_product_special_price om_position_special_price',
                'data-uk-tooltip',
                'title' => 'Preis nur für dieses Angebot']])->add('textFree', TextType::class, ['label' => 'Bezeichnung',
            'mapped' => false,
            'attr' => ['class' => 'om_free_text_amount']])->add('taxFree', TextType::class, ['label' => 'MwSt',
            'mapped' => false,
            'attr' => ['step' => '0.01',
                'class' => 'om_free_tax',
                'data' => '19',
                'readonly' => true]])->add('amountFree', IntegerType::class, ['label' => 'Anzahl',
            'mapped' => false,
            'attr' => ['step' => '0.01', 'class' => 'om_free_amount'],
            'data' => '1'])->add('priceSingleFree', IntegerType::class, ['label' => 'Einzelpreis',
            'mapped' => false,
            'attr' => ['step' => '0.01',
                'class' => 'om_free_price_single',
                'readonly' => false]])->add('priceFreeType', ChoiceType::class, ['label' => 'Preiseinheit',
            'placeholder' => 'Bitte auswählen',
            'required' => true,
            'mapped' => false,
            'attr' => ['class' => 'om_free_price_type'],
            'choices' => ['psch.' => 'psch.',
                'Stk.' => 'Stk.',
                'Std.' => 'Std.',
                'Lfm' => 'Lfm',
                'm²' => 'm²',
                'm³' => 'm³',
                'kg' => 'kg',
                't' => 't',
                'ltr' => 'ltr',
                'm' => 'm']])->add('priceFree', TextType::class, ['label' => 'Nettopreis',
            'mapped' => false,
            'attr' => [
                'class' => 'om_free_price om_position_price',
                'readonly' => true]])->add('saveFree', ChoiceType::class, array('label' => 'Eingabe speichern?',
            'required' => false,
            'mapped' => false,
            'data' => 0,
            'attr' => ['class' => 'om_free_save_radio'],
            'choices' => array('nein' => 0,
                'als Leistung' => 1,
                'als Produkt' => 2),
            'multiple' => false,))->add('extraText', TextType::class, ['label' => 'Zusatzinformation',
                'mapped' => false,'required'=>false,
                'attr' => ['class' => 'om_extra_text']]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => 'Modules\OrderManagementBundle\Entity\Service',
            'csrf_protection' => false,
            'products' => [],
            'services' => []]);
    }


}
