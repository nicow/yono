<?php

namespace Modules\OrderManagementBundle\Form;

use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Modules\OrderManagementBundle\Entity\Service;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ServiceType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $tax = 19;
        $price=0;
        if ($options['data']->getTax()) {
            $tax = $options['data']->getTax();
        }
        if ($options['data']->getPrice()) {
            $price =  number_format($options['data']->getPrice(), 2, ',', '.');
        }

        $builder->add('title', null, ['label' => 'Titel'])->add('categories', null, ['label' => 'Kategorien'])->add("tax", TextType::class, ['label' => "MwSt.",
            'data' => $tax])->add('priceType', ChoiceType::class, ['label' => 'Preiseinheit',
            'choices' => ['Stunden' => Service::TYPE_HOUR,
                'Pauschale' => Service::TYPE_FLATRATE,
                'Quadratmeter' => Service::TYPE_SQUAREMETERS,
                'Kubikmeter' => Service::TYPE_CUBICMETER,
                'Laufender Meter' => Service::TYPE_RUNNINGMETERS]])->add('description', CKEditorType::class, ['label' => 'Beschreibung'])
            ->add('price', TextType::class, ['label' => 'Nettopreis', 'data' => $price,
            'attr' => ['step' => '0.01']]);

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => 'Modules\OrderManagementBundle\Entity\Service',
            'csrf_protection' => false]);
    }
}
