<?php

namespace Modules\OrderManagementBundle\Form;

use Modules\OrderManagementBundle\Entity\Product;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PriceListProductType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('product', EntityType::class, ['class' => 'Modules\OrderManagementBundle\Entity\Product',
            'label' => 'Leistung',
            'choice_label' => function (Product $product) {
                return $product->getTitle() . " / " . number_format($product->getPrice(), 2, ',', '.') . '€';
            }])->add('price', MoneyType::class, ['label' => 'Preis',
            'attr' => ['step' => '0.01', 'size' => 15]])->add('info', TextType::class, ['label' => 'Interne Notiz',
            'required' => false]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => 'Modules\OrderManagementBundle\Entity\PriceListProduct',
            'csrf_protection' => false]);
    }
}
