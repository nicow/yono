<?php

namespace Modules\OrderManagementBundle\Form;

use Modules\OrderManagementBundle\Entity\Service;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PriceListServiceType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('service', EntityType::class, ['class' => 'Modules\OrderManagementBundle\Entity\Service',
            'label' => 'Leistung',
            'choice_label' => function (Service $service) {
                return $service->getTitle() . " / " . number_format($service->getPrice(), 2, ',', '.') . '€';
            }])->add('price', MoneyType::class, ['label' => 'Preis', 
            'attr' => ['step' => '0.01', 'size' => 15]])->add('info', TextType::class, ['label' => 'Interne Notiz',
            'required' => false]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => 'Modules\OrderManagementBundle\Entity\PriceListService',
            'csrf_protection' => false]);
    }
}
