<?php

namespace Modules\OrderManagementBundle\Command;

use ColourStream\Bundle\CronBundle\Annotation\CronJob;
use Modules\OrderManagementBundle\Entity\Offer;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Created by PhpStorm
 * User: Nico
 * Date: 06.06.2017
 * Time: 08:23
 */

/**
 * Class OfferReminderCommand
 * @package Modules\OrderManagementBundle\Command
 * @CronJob(interval="PT24H", firstrun="09:00:00")
 */
class OfferReminderCommand extends ContainerAwareCommand
{
    public function configure()
    {
        $this->setName('yono:modules:ordermanagement:offerreminder')->setDescription('Send offer reminder emails');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $em = $this->getContainer()->get('doctrine')->getManager();

        $offerRepo = $em->getRepository('ModulesOrderManagementBundle:Offer');

        $offers = $offerRepo->getReminders();

        if (empty($offers)) {
            return 1;
        }

        $prefix = $this->getContainer()->getParameter('schema') . '://' . $this->getContainer()->getParameter('host');

        $mailer = $this->getContainer()->get('mailer');


        $tpl = $this->getContainer()->get('twig')->render('@ModulesOrderManagement/Mails/offerReminder.html.twig',
            [
                'table' => $this->buildOfferTable($offers),
                'prefix' => $prefix
            ]
        );

        $mail = (new \Swift_Message('Angebots Reminder'))
            ->setFrom($this->getContainer()->getParameter('mailer_from_address'), $this->getContainer()->getParameter('mailer_from_name'))
            ->addTo($this->getContainer()->getParameter('modules_ordermanagement_accounting_mail'))
            ->setBody($tpl, 'text/html');

        $mailer->send($mail);

        return 0;
    }

    /**
     * @param Offer[] $offers
     * @return mixed
     */
    private function buildOfferTable(array $offers)
    {
        $table = '<table style="width: 100%; border: 1px solid; border-collapse: collapse; " >
                      <thead>
                          <tr>
                              <th>Kunde</th>
                              <th>Angebots Nr.</th>
                              <th>Angebots Datum</th>
                              <th>Reminder</th>
                              <th>Betrag</th>
                          </tr>
                      </thead>
                      <tbody style="border-bottom: 1px solid; border-top: 1px solid;">[body]</tbody>
                      <tfoot>
                           <tr>
                              <th>Kunde</th>
                              <th>Angebots Nr.</th>
                              <th>Angebots Datum</th>
                              <th>Reminder</th>
                              <th>Betrag</th>
                           </tr>
                      </tfoot>
                  </table>';

        $body = '';

        foreach ($offers as $offer) {
            $body .= $this->buildOfferTableRow($offer);
        }

        return str_replace("[body]", $body, $table);

    }


    private function buildOfferTableRow(Offer $offer)
    {

        $template = '<tr style="border-bottom: 1px solid; border-top: 1px solid; ">
                         <td style="border: 1px solid;width: 150px;color: #0a0a0a;font-family: Lucida Grande,Geneva,Verdana,sans-serif;font-size: 14px;font-weight: 400;line-height: 1.3;">[customer]</td>
                         <td style="border: 1px solid;color: #0a0a0a;font-family: Lucida Grande,Geneva,Verdana,sans-serif;font-size: 14px;font-weight: 400;line-height: 1.3;">[number]</td>
                         <td style="border: 1px solid;color: #0a0a0a;font-family: Lucida Grande,Geneva,Verdana,sans-serif;font-size: 14px;font-weight: 400;line-height: 1.3;">[printDate]</td>                        
                         <td style="border: 1px solid;color: #0a0a0a;font-family: Lucida Grande,Geneva,Verdana,sans-serif;font-size: 14px;font-weight: 400;line-height: 1.3;">[deadline]</td>
                         <td style="text-align: right; width: 150px; border: 1px solid;color: #0a0a0a;font-family: Lucida Grande,Geneva,Verdana,sans-serif;font-size: 14px;font-weight: 400;line-height: 1.3;">[total]</td>
                     </tr>';

        $template = str_replace(
            [
                "[customer]", "[number]", "[printDate]", "[deadline]", "[total]"
            ],
            [
                $offer->getContact()->getName(),
                $offer->getNumber(),
                $offer->getPrintDate() == null ? '/' : $offer->getPrintDate()->format('d.m.Y'),
                $offer->getReminder()->format('d.m.Y'),
                number_format($offer->getCalculation()->getFinalPrice(), 2, ',', '.') . ' €',

            ],
            $template);

        return str_repeat($template, 25);
    }
}