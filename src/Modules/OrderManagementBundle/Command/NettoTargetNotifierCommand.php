<?php

namespace Modules\OrderManagementBundle\Command;

use ColourStream\Bundle\CronBundle\Annotation\CronJob;
use Modules\OrderManagementBundle\Entity\Invoice;
use Modules\OrderManagementBundle\Entity\InvoicePositions;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Created by PhpStorm.
 * User: Nico
 * Date: 06.06.2017
 * Time: 08:23
 */

/**
 * Class NettoTargetNotifierCommand
 * @package Modules\OrderManagementBundle\Command
 * @CronJob(interval="PT24H", firstrun="09:00:00")
 */
class NettoTargetNotifierCommand extends ContainerAwareCommand
{
    public function configure()
    {
        $this->setName('yono:modules:ordermanagement:nettotargetnotifier')->setDescription('Send an email if an invoice is overdue');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $em = $this->getContainer()->get('doctrine')->getManager();


        $topRepo = $em->getRepository('ModulesOrderManagementBundle:TermsOfPayment');
        $invoiceRepo = $em->getRepository('ModulesOrderManagementBundle:Invoice');

        $invoices = $invoiceRepo->findUnpaied($this->getContainer()->get('mod.order_management.helper'));

        if (empty($invoices)) {
            return 1;
        }

        $prefix = $this->getContainer()->getParameter('schema') . '://' . $this->getContainer()->getParameter('host');

        $mailer = $this->getContainer()->get('mailer');


        $tpl = $this->getContainer()->get('twig')->render('@ModulesOrderManagement/Mails/invoiceNotify.html.twig', ['table' => $this->buildInvoiceTable($invoices),
            'prefix' => $prefix]);

        $mail = (new \Swift_Message('Überfällige Rechnungen'))->setFrom($this->getContainer()->getParameter('mailer_from_address'), $this->getContainer()->getParameter('mailer_from_name'))->addTo($this->getContainer()->getParameter('modules_ordermanagement_accounting_mail'))->setBody($tpl, 'text/html');

        $mailer->send($mail);


        file_put_contents('web/mail.html', $tpl);

        return 0;
    }

    /**
     * @param array $invoices
     */
    private function buildInvoiceTable(array $invoices)
    {
        $table = '<table style="width: 100%; border: 1px solid; border-collapse: collapse; " >
                      <thead>
                          <tr>
                              <th>Kunde</th>
                              <th>Re-Nr.</th>
                              <th>Re Datum</th>
                              <th>Fällig</th>
                              <th>Betrag</th>
                          </tr>
                      </thead>
                      <tbody style="border-bottom: 1px solid; border-top: 1px solid;">[body]</tbody>
                      <tfoot>
                           <tr>
                               <th>Kunde</th>
                               <th>Re-Nr.</th>
                               <th>Re Datum</th>
                               <th>Fällig</th>
                               <th>Betrag</th>
                           </tr>
                      </tfoot>
                  </table>';

        $body = '';

        foreach ($invoices as $invoice) {
            $body .= $this->buildInvoiceTableRow($invoice);
        }

        return str_replace("[body]", $body, $table);

    }


    private function buildInvoiceTableRow(Invoice $invoice)
    {

        $template = '<tr style="border-bottom: 1px solid; border-top: 1px solid; ">
                         <td style="border: 1px solid;color: #0a0a0a;font-family: Lucida Grande,Geneva,Verdana,sans-serif;font-size: 16px;font-weight: 400;line-height: 1.3;">[customer]</td>
                         <td style="border: 1px solid;color: #0a0a0a;font-family: Lucida Grande,Geneva,Verdana,sans-serif;font-size: 16px;font-weight: 400;line-height: 1.3;">[number]</td>
                         <td style="border: 1px solid;color: #0a0a0a;font-family: Lucida Grande,Geneva,Verdana,sans-serif;font-size: 16px;font-weight: 400;line-height: 1.3;">[printDate]</td>                        
                         <td style="border: 1px solid;color: #0a0a0a;font-family: Lucida Grande,Geneva,Verdana,sans-serif;font-size: 16px;font-weight: 400;line-height: 1.3;">[deadline]</td>
                         <td style="text-align: right; border: 1px solid;color: #0a0a0a;font-family: Lucida Grande,Geneva,Verdana,sans-serif;font-size: 16px;font-weight: 400;line-height: 1.3;">[total]</td>
                     </tr>';

        $omHelper = $this->getContainer()->get('mod.order_management.helper');

        $targetNetto = 14;
        /**
         * @var InvoicePositions $position
         */
        foreach ($omHelper->getInvoicePositions($invoice) as $position) {
            $targetNetto = $position->getCalculation()->getTermsOfPayment()->getTargetNetto();
        }
        $dealine = '';
        if ($invoice->getPrintDate()) $dealine = $invoice->getPrintDate()->add((new \DateInterval('P' . $targetNetto . 'D')))->format('d.m.Y');

        $template = str_replace(["[customer]",
            "[number]",
            "[printDate]",
            "[deadline]",
            "[total]"], [$invoice->getContact()->getName(),
            $invoice->getNumber(),
            $invoice->getPrintDate()->format('d.m.Y'),
            $dealine,
            number_format($invoice->getCalculation()->getFinalPrice(), 2, ',', '.') . ' €',

        ], $template);

        return $template;
    }
}