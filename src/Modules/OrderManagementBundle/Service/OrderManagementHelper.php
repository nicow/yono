<?php
/**
 * Created by PhpStorm.
 * User: mkt
 * Date: 31.12.16
 * Time: 04:09
 */

namespace Modules\OrderManagementBundle\Service;


use Core\AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Modules\OrderManagementBundle\Entity\Calculation;
use Modules\OrderManagementBundle\Entity\Division;
use Modules\OrderManagementBundle\Entity\Invoice;
use Modules\OrderManagementBundle\Entity\InvoicePositions;
use Modules\OrderManagementBundle\Entity\JobTicket;
use Modules\OrderManagementBundle\Entity\Offer;
use Modules\OrderManagementBundle\Entity\Orders;
use Modules\OrderManagementBundle\Entity\PriceList;
use Modules\OrderManagementBundle\Entity\PriceListProduct;
use Modules\OrderManagementBundle\Entity\PriceListService;
use Modules\OrderManagementBundle\Entity\Product;
use Modules\OrderManagementBundle\Entity\ProductCategory;
use Modules\OrderManagementBundle\Entity\Reminder;
use Modules\OrderManagementBundle\Entity\Service;
use Modules\OrderManagementBundle\Entity\ServiceCategory;
use Symfony\Component\Debug\Exception\ClassNotFoundException;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\RequestStack;

class OrderManagementHelper
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var RequestStack
     */
    private $requestStack;
    /**
     * @var Container
     */
    private $container;


    public function __construct(EntityManager $em, RequestStack $requestStack, Container $container)
    {
        $this->em = $em;
        $this->requestStack = $requestStack;
        $this->request = $this->requestStack->getCurrentRequest();
        $this->container = $container;
    }

    public function getItemCountForCategory($category)
    {

        if ($category instanceof ProductCategory) {
            $products = $this->em->getRepository('ModulesOrderManagementBundle:Product')->findByCategory($category);
        }
        if ($category instanceof ServiceCategory) {
            $products = $this->em->getRepository('ModulesOrderManagementBundle:Service')->findByCategory($category);
        }


        return count($products);
    }

    public function getOrderByOffer(Offer $offer)
    {
        $order = $this->em->getRepository('ModulesOrderManagementBundle:Orders')->findOneBy(['offer' => $offer]);

        return $order;
    }

    public function getAllOrdersWithJobtickets()
    {
        $order = $this->em->getRepository('ModulesOrderManagementBundle:Orders')->findWithJobTicket();

        return $order;
    }

    public function getUserJobTickets(User $user)
    {
       
        if ($user->getMember()) $jobtickets = $this->em->getRepository('ModulesOrderManagementBundle:JobTicket')->findByMemberAll($user->getMember()); else
            $jobtickets = false;
        
        $jobs = [];
        $closed = 0;
        if ($jobtickets) {
            foreach ($jobtickets as $jobticket) {
                if ($jobticket->getStatus() == JobTicket::STATUS_DONE) {
                    $closed++;
                }
            }
            $jobs['total'] = count($jobtickets);
            $jobs['closed'] = $closed;
            $jobs['open'] = count($jobtickets) - $closed;
            $jobs['percent'] = round(((count($jobtickets) - $closed) / count($jobtickets)) * 100);
        }


        return $jobs;
    }

    public function getInvoiceByOrder(Orders $order)
    {
        $invoice = $this->em->getRepository('ModulesOrderManagementBundle:InvoicePositions')->findOneBy(['orders' => $order]);

        return $invoice;
    }

    public function getMonth($month)
    {
        switch ($month) {
            case 1:
                return 'Januar';
                break;
            case 2:
                return 'Februar';
                break;
            case 3:
                return 'März';
                break;
            case 4:
                return 'April';
                break;
            case 5:
                return 'Mai';
                break;
            case 6:
                return 'Juni';
                break;
            case 7:
                return 'Juli';
                break;
            case 8:
                return 'August';
                break;
            case 9:
                return 'September';
                break;
            case 10:
                return 'Oktober';
                break;
            case 11:
                return 'November';
                break;
            case 12:
                return 'Dezember';
                break;
        }

    }

    public function getOfferStatus(Offer $offer)
    {
        if ($offer->getStatus() == Offer::STATUS_ACCEPT) {
            return 'Auftragsklarheit';
        } elseif ($offer->getStatus() == Offer::STATUS_REQUEST) {
            return 'liegt beim Kunden';
        } elseif ($offer->getStatus() == Offer::STATUS_ARCHIV) {
            return 'archiviert';
        } elseif ($offer->getStatus() == Offer::STATUS_OPEN) {
            return 'offen';
        } elseif ($offer->getStatus() == Offer::STATUS_PREPARED) {
            return 'vorbereitet';
        } elseif ($offer->getStatus() == Offer::STATUS_CANCELD) {
            return 'abgesagt';
        } else {
            return '-';
        }
    }

    public function getOrdersStatus(Orders $order)
    {
        if ($order->getStatus() == Orders::STATUS_BILLED) {
            return 'abgerechnet';
        } elseif ($order->getStatus() == Orders::STATUS_RESET) {
            return 'zurückgestellt';
        } elseif ($order->getStatus() == Orders::STATUS_ARCHIV) {
            return 'archiviert';
        } elseif ($order->getStatus() == Orders::STATUS_PREPARED) {
            return 'vorbereitet';
        } elseif ($order->getStatus() == Orders::STATUS_CANCELED) {
            return 'storniert';
        } elseif ($order->getStatus() == Orders::STATUS_COMPLETED) {
            return 'abgeschlossen';
        } elseif ($order->getStatus() == Orders::STATUS_ACTIVE) {
            return 'aktiv';
        } else {
            return '-';
        }
    }

    public function getInvoiceStatus(Invoice $invoice)
    {
        if ($invoice->getStatus() == Invoice::STATUS_RESET) {
            return 'zurückgestellt';
        } elseif ($invoice->getStatus() == Invoice::STATUS_ARCHIV) {
            return 'archiviert';
        } elseif ($invoice->getStatus() == Invoice::STATUS_PREPARED) {
            return 'vorbereitet';
        } elseif ($invoice->getStatus() == Invoice::STATUS_CANCELED) {
            return 'storniert';
        } elseif ($invoice->getStatus() == Invoice::STATUS_COMPLETED) {
            return 'nicht bezahlt';
        } elseif ($invoice->getStatus() == Invoice::STATUS_BILLED) {
            return 'bezahlt';
        } elseif ($invoice->getStatus() == Invoice::STATUS_LAWYER) {
            return 'Anwalt';
        } elseif ($invoice->getStatus() == Invoice::STATUS_FIRSTWARNING) {
            return '1. Mahnung';
        } elseif ($invoice->getStatus() == Invoice::STATUS_SECONDWARNING) {
            return '2. Mahnung';
        } else {
            return 'offen';
        }
    }

    public function checkOrderJobTicketStatus(Orders $order)
    {
        $jobTickets = $this->em->getRepository('ModulesOrderManagementBundle:JobTicket')->findBy(['order' => $order]);

        $allDone = true;
        if ($jobTickets) {
            foreach ($jobTickets as $jobTicket) {
                if ($jobTicket->getStatus() != JobTicket::STATUS_DONE) {
                    $allDone = false;
                }
            }
        }

        return $allDone;
    }

    public function getExportPlaceholder()
    {
        $placeholder = $this->em->getRepository('ModulesOrderManagementBundle:ExportPlaceholder')->findAll();

        return $placeholder;
    }

    public function getJobTicketStatus(JobTicket $jobTicket)
    {
        if ($jobTicket->getStatus() == JobTicket::STATUS_DONE) {
            return 'erledigt';
        } elseif ($jobTicket->getStatus() == JobTicket::STATUS_OPEN) {
            return 'offen';
        } elseif ($jobTicket->getStatus() == JobTicket::STATUS_PLANNED) {
            return 'geplant';
        } elseif ($jobTicket->getStatus() == JobTicket::STATUS_INPROGRESS) {
            return 'in Arbeit';
        } elseif ($jobTicket->getStatus() == JobTicket::STATUS_IMPORTANT) {
            return 'dringend';
        }
        elseif ($jobTicket->getStatus() == JobTicket::STATUS_ARCHIV) {
            return 'archiviert';
        } else {
            return '-';
        }
    }

    public function getReminderStatus(Reminder $reminder)
    {
        if ($reminder->getStatus() == Reminder::STATUS_DONE) {
            return 'erledigt';
        } elseif ($reminder->getStatus() == Reminder::STATUS_OPEN) {
            return 'offen';
        } elseif ($reminder->getStatus() == Reminder::STATUS_PLANNED) {
            return 'geplant';
        } elseif ($reminder->getStatus() == Reminder::STATUS_NORESPONSE) {
            return 'nicht erfüllt';
        } else {
            return '-';
        }
    }

    public function getServicePriceTypeByName($type)
    {
        if ($type == 'psch.') {
            return Service::TYPE_FLATRATE;
        } elseif ($type == 'Stk.') {
            return Service::TYPE_AMOUNT;
        } elseif ($type == 'Std.') {
            return Service::TYPE_HOUR;
        } elseif ($type == 'Lfm') {
            return Service::TYPE_RUNNINGMETERS;
        } elseif ($type == 'm²') {
            return Service::TYPE_SQUAREMETERS;
        } elseif ($type == 'm³') {
            return Service::TYPE_CUBICMETER;
        } elseif ($type == 'kg') {
            return Service::TYPE_KILO;
        } elseif ($type == 't') {
            return Service::TYPE_TON;
        } elseif ($type == 'ltr') {
            return Service::TYPE_LITER;
        } elseif ($type == 'm') {
            return Service::TYPE_METER;
        } else {
            return Service::TYPE_AMOUNT;
        }
    }

    public function getServicePriceType($service)
    {
        $serviceEnity = $this->em->getRepository('ModulesOrderManagementBundle:Service')->find($service);

        if ($serviceEnity->getPriceType() == Service::TYPE_FLATRATE) {
            return 'psch.';
        }
        if ($serviceEnity->getPriceType() == Service::TYPE_AMOUNT) {
            return 'Stk.';
        } elseif ($serviceEnity->getPriceType() == Service::TYPE_HOUR) {
            return 'Std.';
        } elseif ($serviceEnity->getPriceType() == Service::TYPE_RUNNINGMETERS) {
            return 'Lfm';
        } elseif ($serviceEnity->getPriceType() == Service::TYPE_SQUAREMETERS) {
            return 'm²';
        } elseif ($serviceEnity->getPriceType() == Service::TYPE_CUBICMETER) {
            return 'm³';
        } elseif ($serviceEnity->getPriceType() == Service::TYPE_KILO) {
            return 'kg';
        } elseif ($serviceEnity->getPriceType() == Service::TYPE_TON) {
            return 't';
        } elseif ($serviceEnity->getPriceType() == Service::TYPE_LITER) {
            return 'ltr';
        } elseif ($serviceEnity->getPriceType() == Service::TYPE_METER) {
            return 'm';
        } else {
            return '-';
        }
    }

    public function getProductPriceTypeByName($type)
    {
        if ($type == 'Stk.') {
            return Product::TYPE_PIECE;
        } elseif ($type == 'psch.') {
            return Product::TYPE_FLATRATE;
        } elseif ($type == 'Lfm') {
            return Product::TYPE_RUNNINGMETERS;
        } elseif ($type == 'm²') {
            return Product::TYPE_SQUAREMETERS;
        } elseif ($type == 'm³') {
            return Product::TYPE_CUBICMETER;
        } elseif ($type == 'kg') {
            return Product::TYPE_KILO;
        } elseif ($type == 't') {
            return Product::TYPE_TON;
        } elseif ($type == 'ltr') {
            return Product::TYPE_LITER;
        } elseif ($type == 'm') {
            return Product::TYPE_METER;
        } else {
            return Product::TYPE_PIECE;
        }
    }

    public function getProductPriceType($product)
    {
        $productEnity = $this->em->getRepository('ModulesOrderManagementBundle:Product')->find($product);

        if ($productEnity->getPriceType() == Product::TYPE_PIECE) {
            return 'Stk.';
        } elseif ($productEnity->getPriceType() == Product::TYPE_FLATRATE) {
            return 'psch.';
        } elseif ($productEnity->getPriceType() == Product::TYPE_RUNNINGMETERS) {
            return 'Lfm';
        } elseif ($productEnity->getPriceType() == Product::TYPE_SQUAREMETERS) {
            return 'm²';
        } elseif ($productEnity->getPriceType() == Product::TYPE_CUBICMETER) {
            return 'm³';
        } elseif ($productEnity->getPriceType() == Product::TYPE_KILO) {
            return 'kg';
        } elseif ($productEnity->getPriceType() == Product::TYPE_TON) {
            return 't';
        } elseif ($productEnity->getPriceType() == Product::TYPE_LITER) {
            return 'ltr';
        } elseif ($productEnity->getPriceType() == Product::TYPE_METER) {
            return 'm';
        } else {
            return '-';
        }
    }

    public function getService($service)
    {
        $serviceEnity = $this->em->getRepository('ModulesOrderManagementBundle:Service')->find($service);

        return $serviceEnity;
    }

    public function getProduct($product)
    {
        $productEnity = $this->em->getRepository('ModulesOrderManagementBundle:Product')->find($product);

        return $productEnity;
    }

    public function getServiceTitle($service)
    {
        $serviceEnity = $this->em->getRepository('ModulesOrderManagementBundle:Service')->find($service);

        return $serviceEnity->getTitle();
    }

    public function getServiceTax($service)
    {
        $serviceEnity = $this->em->getRepository('ModulesOrderManagementBundle:Service')->find($service);

        return $serviceEnity->getTax();
    }

    public function getProductTitle($product)
    {
        $productEnity = $this->em->getRepository('ModulesOrderManagementBundle:Product')->find($product);

        return $productEnity->getTitle();
    }

    public function getProductTax($product)
    {
        $productEnity = $this->em->getRepository('ModulesOrderManagementBundle:Product')->find($product);

        return $productEnity->getTax();
    }

    public function invoiceDiscountCheck($invoice)
    {
        $InvoicePositions = $this->em->getRepository('ModulesOrderManagementBundle:InvoicePositions')->findBy(['invoice' => $invoice]);

        $discount = false;
        if ($InvoicePositions) {
            foreach ($InvoicePositions as $invoicePosition) {
                if ($invoicePosition->getCalculation()->getContactDiscount()) {
                    $discount = true;
                }
            }
        }

        return $discount;

    }

    public function getInvoiceItems($invoice)
    {
        $InvoicePositions = $this->em->getRepository('ModulesOrderManagementBundle:InvoicePositions')->findBy(['invoice' => $invoice]);
        $CalaculationItemRepo = $this->em->getRepository('ModulesOrderManagementBundle:CalculationItem');

        $items = [];
        /**
         * @var InvoicePositions invoicePosition
         */
        if ($InvoicePositions) {
            foreach ($InvoicePositions as $invoicePosition) {

                $calculationItems = $CalaculationItemRepo->findBy(['calculation' => $invoicePosition->getCalculation()]);
                if ($calculationItems) {
                    foreach ($calculationItems as $calculationItem) {
                        $items[] = $calculationItem;
                    }
                }

            }

            return $items;
        }

        return null;
    }

    public function getInvoicePrice($invoice)
    {
        $InvoicePositions = $this->em->getRepository('ModulesOrderManagementBundle:InvoicePositions')->findBy(['invoice' => $invoice]);

        $price = 0;
        /**
         * @var InvoicePositions invoicePosition
         */
        if ($InvoicePositions) {
            foreach ($InvoicePositions as $invoicePosition) {
                $price = $price + $invoicePosition->getCalculation()->getFinalPrice();
            }

            return $price;
        }

        return false;
    }

    public function isNetto(Invoice $invoice)
    {
        $positions = $this->getInvoicePositions($invoice);
        if ($positions) {
            $item = $positions[0];
            /**
             * @var Calculation $calc ;
             */
            $calc = $item->getCalculation();

            return $calc->getNetto() ? true : false;
        }

        return false;
    }

    public function isDebit(Invoice $invoice)
    {
        $positions = $this->getInvoicePositions($invoice);
        if ($positions) {
            $item = $positions[0];
            /**
             * @var Calculation $calc ;
             */
            $calc = $item->getCalculation();

            return $calc->getTermsOfPayment()->getHandling();
        }

        return false;
    }

    public function isMonthlyFee(Invoice $invoice)
    {
        $positions = $this->getInvoicePositions($invoice);
        if ($positions) {
            $item = $positions[0];
            /**
             * @var Calculation $calc ;
             */
            $calc = $item->getCalculation();

            return $calc->getMonthlyFee() ? true : false;
        }

        return false;
    }

    public function getMonthlyFee(Invoice $invoice)
    {
        $positions = $this->getInvoicePositions($invoice);
        if ($positions) {
            $item = $positions[0];
            /**
             * @var Calculation $calc ;
             */
            $calc = $item->getCalculation();

            return $calc->getMonthlyFee();
        }

        return false;
    }

    public function getMonthlyFeeByOrder(Orders $order)
    {
        $positions =$this->em->getRepository('ModulesOrderManagementBundle:InvoicePositions')->findBy(['calculation' => $order->getCalculation()]);
        if ($positions) {
            $item = $positions[count($positions)-1];
         
            return $item;
        }

        return false;
    }

    public function getInvoicePositions($invoice)
    {
        $InvoicePositions = $this->em->getRepository('ModulesOrderManagementBundle:InvoicePositions')->findBy(['invoice' => $invoice]);

        return $InvoicePositions;
    }

    public function getInvoiceReminder($invoice)
    {
        $reminders = $this->em->getRepository('ModulesOrderManagementBundle:Reminder')->findBy(['invoice' => $invoice]);

        return $reminders;
    }


    public function getPriceListProductPrice($pricelist, $product)
    {
        $productsPriceList = $this->em->getRepository('ModulesOrderManagementBundle:PriceList');

        /**
         * @var PriceList $list
         */
        $list = $productsPriceList->findByProduct($pricelist, $product);
        /**
         * @var PriceListProduct $item
         */
        if ($list) {
            foreach ($list->getProducts() as $item) {
                if ($item->getProduct()->getId() == $product) {
                    return $item->getPrice();
                }
            }
        }

        return false;
    }

    public function getPriceListServicePrice($pricelist, $service)
    {
        $servicePriceList = $this->em->getRepository('ModulesOrderManagementBundle:PriceList');

        /**
         * @var PriceList $list
         */
        $list = $servicePriceList->findByService($pricelist, $service);

        /**
         * @var PriceListService $item
         */
        if ($list) {
            foreach ($list->getServices() as $item) {
                if ($item->getService()->getId() == $service) {
                    $data['price'] = $item->getPrice();
                }
            }
        }

        return false;
    }

    /**
     * @param          $className
     * @param Division $division
     * @return array
     * @throws ClassNotFoundException
     */
    public function getNumberOfEntityBelongToDivision($className, Division $division)
    {
        if (!class_exists($className)) {
            throw new ClassNotFoundException(sprintf("The class %s was not found!", $className), new \ErrorException());
        }

        $traits = class_uses($className, true);

        if (!isset($traits['Modules\OrderManagementBundle\Traits\DivisionTrait'])) {
            throw new ClassNotFoundException(sprintf("The trait %s seems not to be used in %s!", "Modules\OrderManagementBundle\Traits\DivisionTrait", $className), new \ErrorException());
        }

        $repo = $this->em->getRepository($className);

        return count($repo->findBy(["division" => $division]));
    }

    /**
     * @param $entity
     * @param $type
     * @param $number
     * @return mixed
     */
    public function buildNumber($entity, $type, $number)
    {
        if ($type == 'reminder') {
            $contactPerson = $entity->getInvoice()->getContactPerson();
            $contact = $entity->getInvoice()->getContact();
        } else {
            $contactPerson = $entity->getContactPerson();
            $contact = $entity->getContact();
        }
        $framework = $this->container->getParameter('modules_ordermanagement_' . $type . '_framework');
        $framework = str_replace(["[devision]",
            "[customer]",
            "[year]",
            "[month]",
            "[id]"], [$entity->getDivision()->getTag(),
            $contactPerson ? strtoupper(substr($contactPerson->getFirstname(), 0, 1)) . strtoupper(substr($contactPerson->getLastname(), 0, 1)) : $contact->getName(),
            date('y'),
            date('m'),
            $number], $framework);

        return $framework;
    }


}