<?php

namespace Modules\OrderManagementBundle\Entity;

use Core\AppBundle\Entity\BlameableTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * PriceListProduct
 * @ORM\Table(name="mod_order_management_price_list_product")
 * @ORM\Entity(repositoryClass="Modules\OrderManagementBundle\Repository\PriceListProductRepository")
 */
class PriceListProduct
{
    use BlameableTrait;

    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Product
     * @ORM\ManyToOne(targetEntity="Modules\OrderManagementBundle\Entity\Product")
     */
    private $product;

    /**
     * @var float
     * @ORM\Column(name="price", type="float", nullable=true)
     */
    private $price;

    /**
     * @var string
     * @ORM\Column(name="info", type="string", length=255, nullable=true)
     */
    private $info;

    /**
     * @var PriceList
     * @ORM\ManyToOne(targetEntity="Modules\OrderManagementBundle\Entity\PriceList", inversedBy="products")
     * @ORM\JoinColumn(name="pricelist_id", referencedColumnName="id")
     */
    private $pricelist;


    /**
     * Get id
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get info
     * @return string
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * Set info
     * @param string $info
     * @return PriceListProduct
     */
    public function setInfo($info)
    {
        $this->info = $info;

        return $this;
    }

    /**
     * @return PriceList
     */
    public function getPricelist()
    {
        return $this->pricelist;
    }

    /**
     * @param PriceList $pricelist
     * @return PriceListProduct
     */
    public function setPricelist($pricelist)
    {
        $this->pricelist = $pricelist;

        return $this;
    }

    /**
     * @return float
     */
    public function getBrutto()
    {
        /*
         * Make it possible to use Taxes like 19 for 19 percent or 0.19
         */
        $tax = $this->getProduct()->getTax();
        if ($tax > 0) {
            $tax = $tax / 100;
        }

        $multiplikator = 1 + $tax;

        return $this->getPrice() * $multiplikator;

    }

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param Product $product
     * @return PriceListProduct
     */
    public function setProduct($product)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get price
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set price
     * @param float $price
     * @return PriceListProduct
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }
}

