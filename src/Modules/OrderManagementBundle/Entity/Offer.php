<?php

namespace Modules\OrderManagementBundle\Entity;

use Core\AppBundle\Annotation\Acl;
use Core\AppBundle\Entity\BlameableTrait;
use Doctrine\ORM\Mapping as ORM;
use InvalidArgumentException;
use Modules\ContactBundle\Entity\Address;
use Modules\ContactBundle\Entity\Contact;
use Modules\ContactBundle\Entity\Person;
use Modules\OrderManagementBundle\Traits\DivisionTrait;
use Modules\ProjectBundle\Entity\Project;

/**
 * Offer
 * @Acl("Angebote")
 * @ORM\Table(name="mod_order_management_offer")
 * @ORM\Entity(repositoryClass="Modules\OrderManagementBundle\Repository\OfferRepository")
 */
class Offer
{

    const STATUS_OPEN = 0;
    const STATUS_PREPARED = 1;
    const STATUS_REQUEST = 2;
    const STATUS_CANCELD = 3;
    const STATUS_ACCEPT = 4;
    const STATUS_ARCHIV = 5;

    use BlameableTrait;
    use DivisionTrait;


    /**
     * @var Contact
     * @ORM\ManyToOne(targetEntity="Modules\ContactBundle\Entity\Contact",cascade={"persist"})
     */
    public $contact;

    /**
     * @var Address
     * @ORM\ManyToOne(targetEntity="Modules\ContactBundle\Entity\Address")
     */
    public $contactAddress;

    /**
     * @var Person
     * @ORM\ManyToOne(targetEntity="Modules\ContactBundle\Entity\Person", cascade={"persist"})
     */
    public $contactPerson;

    /**
     * @var Project
     * @ORM\ManyToOne(targetEntity="Modules\ProjectBundle\Entity\Project", cascade={"persist"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    public $project;

    /**
     * @var Calculation
     * @ORM\ManyToOne(targetEntity="Modules\OrderManagementBundle\Entity\Calculation")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    public $calculation;

    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="title", type="string", length=120, nullable=true)
     */
    private $title;

    /**
     * @var string
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var \DateTime
     * @ORM\Column(name="reminder", type="datetime", nullable=true)
     */
    private $reminder;

    /**
     * @var \DateTime
     * @ORM\Column(name="printDate", type="date", nullable=true)
     */
    private $printDate;

    /**
     * @var string
     * @ORM\Column(name="number", type="string", length=40, nullable=true)
     */
    private $number;

    /**
     * @var string
     * @ORM\Column(name="placeholder", type="text", nullable=true)
     */
    private $placeholder;

    /**
     * @var int
     * @ORM\Column(name="monthFee", type="integer", nullable=true)
     */
    private $monthFee;

    /**
     * @var int
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;


    /**
     * Get id
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get description
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set description
     * @param string $description
     * @return Offer
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get reminder
     * @return \DateTime
     */
    public function getReminder()
    {
        return $this->reminder;
    }

    /**
     * Set reminder
     * @param \DateTime $reminder
     * @return Offer
     */
    public function setReminder($reminder)
    {
        $this->reminder = $reminder;

        return $this;
    }

    /**
     * Get printDate
     * @return \DateTime
     */
    public function getPrintDate()
    {
        return $this->printDate;
    }

    /**
     * Set printDate
     * @param \DateTime $printDate
     * @return Offer
     */
    public function setPrintDate($printDate)
    {
        $this->printDate = $printDate;

        return $this;
    }

    /**
     * Get status
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set status
     * @param integer $status
     * @return Offer
     */
    public function setStatus($status)
    {

        if (in_array($status, [self::STATUS_ACCEPT, self::STATUS_OPEN, self::STATUS_ARCHIV, self::STATUS_CANCELD, self::STATUS_REQUEST, self::STATUS_PREPARED,])) {
            $this->status = $status;

            return $this;
        }

        throw new InvalidArgumentException('Invalid product status.');
    }

    /**
     * @return Contact
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * @param Contact $contact
     * @return Offer
     */
    public function setContact($contact)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * @return Address
     */
    public function getContactAddress()
    {
        return $this->contactAddress;
    }

    /**
     * @param Address $contactAddress
     * @return Offer
     */
    public function setContactAddress($contactAddress)
    {
        $this->contactAddress = $contactAddress;

        return $this;
    }

    /**
     * @return Person
     */
    public function getContactPerson()
    {
        return $this->contactPerson;
    }

    /**
     * @param Person $contactPerson
     * @return Offer
     */
    public function setContactPerson($contactPerson)
    {
        $this->contactPerson = $contactPerson;

        return $this;
    }

    /**
     * @return Project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * @param Project $project
     * @return Offer
     */
    public function setProject($project)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * @return Calculation
     */
    public function getCalculation()
    {
        return $this->calculation;
    }

    /**
     * @param Calculation $calculation
     * @return Offer
     */
    public function setCalculation($calculation)
    {
        $this->calculation = $calculation;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Offer
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param string $number
     * @return Offer
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * @param string $placeholder
     * @return Offer
     */
    public function setPlaceholder($placeholder)
    {
        $this->placeholder = $placeholder;

        return $this;
    }

    /**
     * @return string
     */
    public function getPlaceholder()
    {
        return $this->placeholder;
    }

    /**
     * @param int $monthFee
     * @return Offer
     */
    public function setMonthFee($monthFee)
    {
        $this->monthFee = $monthFee;

        return $this;
    }

    /**
     * @return int
     */
    public function getMonthFee()
    {
        return $this->monthFee;
    }
}

