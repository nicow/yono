<?php

namespace Modules\OrderManagementBundle\Entity;

use Core\AppBundle\Entity\BlameableTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * Calculation
 * @ORM\Table(name="mod_order_management_calculation")
 * @ORM\Entity(repositoryClass="Modules\OrderManagementBundle\Repository\CalculationRepository")
 */
class Calculation
{

    use BlameableTrait;

    /**
     * @var TermsOfPayment
     * @ORM\ManyToOne(targetEntity="Modules\OrderManagementBundle\Entity\TermsOfPayment")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    public $termsOfPayment;
    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var float
     * @ORM\Column(name="specialPrice", type="float", nullable=true)
     */
    private $specialPrice;
    /**
     * @var int
     * @ORM\Column(name="discount", type="integer", nullable=true)
     */
    private $discount;
    /**
     * @var int
     * @ORM\Column(name="discountType", type="integer", nullable=true)
     */
    private $discountType;
    /**
     * @var int
     * @ORM\Column(name="contactDiscount", type="integer", nullable=true)
     */
    private $contactDiscount;
    /**
     * @var int
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;
    /**
     * @var int
     * @ORM\Column(name="netto", type="integer", nullable=true)
     */
    private $netto;
    /**
     * @var int
     * @ORM\Column(name="monthlyFee", type="integer", nullable=true)
     */
    private $monthlyFee;
    /**
     * @var string
     * @ORM\Column(name="finalPrice", type="string",  length=30, nullable=true)
     */
    private $finalPrice;

    /**
     * Get id
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get specialPrice
     * @return float
     */
    public function getSpecialPrice()
    {
        return $this->specialPrice;
    }

    /**
     * Set specialPrice
     * @param float $specialPrice
     * @return Calculation
     */
    public function setSpecialPrice($specialPrice)
    {
        $this->specialPrice = $specialPrice;

        return $this;
    }

    /**
     * Get discount
     * @return int
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Set discount
     * @param integer $discount
     * @return Calculation
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Get status
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set status
     * @param integer $status
     * @return Calculation
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return int
     */
    public function getContactDiscount()
    {
        return $this->contactDiscount;
    }

    /**
     * @param int $contactDiscount
     * @return Calculation
     */
    public function setContactDiscount($contactDiscount)
    {
        $this->contactDiscount = $contactDiscount;

        return $this;
    }

    /**
     * @return int
     */
    public function getDiscountType()
    {
        return $this->discountType;
    }

    /**
     * @param int $discountType
     * @return Calculation
     */
    public function setDiscountType($discountType)
    {
        $this->discountType = $discountType;

        return $this;
    }

    /**
     * @return string
     */
    public function getFinalPrice()
    {
        return $this->finalPrice;
    }

    /**
     * @param string $finalPrice
     * @return Calculation
     */
    public function setFinalPrice($finalPrice)
    {
        $this->finalPrice = $finalPrice;

        return $this;
    }

    /**
     * @return int
     */
    public function getNetto()
    {
        return $this->netto;
    }

    /**
     * @param int $netto
     * @return Calculation
     */
    public function setNetto($netto)
    {
        $this->netto = $netto;

        return $this;
    }

    /**
     * @return int
     */
    public function getMonthlyFee()
    {
        return $this->monthlyFee;
    }

    /**
     * @param int $monthlyFee
     * @return Calculation
     */
    public function setMonthlyFee($monthlyFee)
    {
        $this->monthlyFee = $monthlyFee;

        return $this;
    }

    /**
     * @return TermsOfPayment
     */
    public function getTermsOfPayment()
    {
        return $this->termsOfPayment;
    }

    /**
     * @param TermsOfPayment $termsOfPayment
     * @return Calculation
     */
    public function setTermsOfPayment($termsOfPayment)
    {
        $this->termsOfPayment = $termsOfPayment;

        return $this;
    }
}

