<?php

namespace Modules\OrderManagementBundle\Entity;

use Core\AppBundle\Annotation\Acl;
use Core\AppBundle\Entity\BlameableTrait;
use Doctrine\ORM\Mapping as ORM;
use InvalidArgumentException;
use Modules\OrderManagementBundle\Traits\DivisionTrait;

/**
 * Reminder
 * @Acl("Mahnungen")
 * @ORM\Table(name="mod_order_management_reminder")
 * @ORM\Entity(repositoryClass="Modules\OrderManagementBundle\Repository\ReminderRepository")
 */
class Reminder
{

    use BlameableTrait;
    use DivisionTrait;

    const STATUS_PLANNED = 0;
    const STATUS_OPEN = 1;
    const STATUS_NORESPONSE = 2;
    const STATUS_DONE = 3;

    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=100, nullable=true)
     */
    private $title;

    /**
     * @var Invoice
     * @ORM\ManyToOne(targetEntity="Modules\OrderManagementBundle\Entity\Invoice")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    public $invoice;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="reminderDate", type="date", nullable=true)
     */
    private $reminderDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="printDate", type="date", nullable=true)
     */
    private $printDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deadline", type="date", nullable=true)
     */
    private $deadline;

    /**
     * @var string
     *
     * @ORM\Column(name="number", type="string", length=40, nullable=true)
     */
    private $number;

    /**
     * @var int
     *
     * @ORM\Column(name="version", type="integer", length=40, nullable=true)
     */
    private $version;

    /**
     * @var float
     *
     * @ORM\Column(name="fee", type="float", nullable=true)
     */
    private $fee;

    /**
     * @var string
     * @ORM\Column(name="placeholder", type="text", nullable=true)
     */
    private $placeholder;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Reminder
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set reminderDate
     *
     * @param \DateTime $reminderDate
     *
     * @return Reminder
     */
    public function setReminderDate($reminderDate)
    {
        $this->reminderDate = $reminderDate;

        return $this;
    }

    /**
     * Get reminderDate
     *
     * @return \DateTime
     */
    public function getReminderDate()
    {
        return $this->reminderDate;
    }

    /**
     * Set deadline
     *
     * @param \DateTime $deadline
     *
     * @return Reminder
     */
    public function setDeadline($deadline)
    {
        $this->deadline = $deadline;

        return $this;
    }

    /**
     * Get deadline
     *
     * @return \DateTime
     */
    public function getDeadline()
    {
        return $this->deadline;
    }

    /**
     * Set number
     *
     * @param string $number
     *
     * @return Reminder
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Reminder
     */
    public function setStatus($status)
    {
        if (in_array($status, [self::STATUS_DONE, self::STATUS_PLANNED,self::STATUS_OPEN,self::STATUS_NORESPONSE])) {
            $this->status = $status;

            return $this;
        }

        throw new InvalidArgumentException('Invalid product status.');
    }

    /**
     * Get status
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param Invoice $invoice
     * @return Reminder
     */
    public function setInvoice($invoice)
    {
        $this->invoice = $invoice;
        return $this;
    }

    /**
     * @return Invoice
     */
    public function getInvoice()
    {
        return $this->invoice;
    }

    /**
     * @param int $version
     * @return Reminder
     */
    public function setVersion($version)
    {
        $this->version = $version;

        return $this;
    }

    /**
     * @return int
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param float $fee
     * @return Reminder
     */
    public function setFee($fee)
    {
        $this->fee = $fee;

        return $this;
    }

    /**
     * @return float
     */
    public function getFee()
    {
        return $this->fee;
    }

    /**
     * @param \DateTime $printDate
     * @return Reminder
     */
    public function setPrintDate($printDate)
    {
        $this->printDate = $printDate;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getPrintDate()
    {
        return $this->printDate;
    }

    /**
     * @param string $placeholder
     * @return Reminder
     */
    public function setPlaceholder($placeholder)
    {
        $this->placeholder = $placeholder;

        return $this;
    }

    /**
     * @return string
     */
    public function getPlaceholder()
    {
        return $this->placeholder;
    }
}

