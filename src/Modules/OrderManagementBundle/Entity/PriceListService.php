<?php

namespace Modules\OrderManagementBundle\Entity;

use Core\AppBundle\Entity\BlameableTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * PriceListService
 * @ORM\Table(name="mod_order_management_price_list_service")
 * @ORM\Entity(repositoryClass="Modules\OrderManagementBundle\Repository\PriceListServiceRepository")
 */
class PriceListService
{
    use BlameableTrait;

    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Service
     * @ORM\ManyToOne(targetEntity="Modules\OrderManagementBundle\Entity\Service")
     */
    private $service;

    /**
     * @var float
     * @ORM\Column(name="price", type="float", nullable=true)
     */
    private $price;

    /**
     * @var string
     * @ORM\Column(name="info", type="string", length=255, nullable=true)
     */
    private $info;

    /**
     * @var int
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;

    /**
     * @var PriceList
     * @ORM\ManyToOne(targetEntity="Modules\OrderManagementBundle\Entity\PriceList", inversedBy="services")
     * @ORM\JoinColumn(name="pricelist_id", referencedColumnName="id")
     */
    private $pricelist;

    /**
     * Get id
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get info
     * @return string
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * Set info
     * @param string $info
     * @return PriceListService
     */
    public function setInfo($info)
    {
        $this->info = $info;

        return $this;
    }

    /**
     * Get status
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set status
     * @param integer $status
     * @return PriceListService
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return PriceList
     */
    public function getPricelist()
    {
        return $this->pricelist;
    }

    /**
     * @param PriceList $pricelist
     * @return PriceListService
     */
    public function setPricelist($pricelist)
    {
        $this->pricelist = $pricelist;

        return $this;
    }

    /**
     * @return float
     */
    public function getBrutto()
    {
        /*
         * Make it possible to use Taxes like 19 for 19 percent or 0.19
         */
        $tax = $this->getService()->getTax();
        if ($tax > 0) {
            $tax = $tax / 100;
        }

        $multiplikator = 1 + $tax;

        return $this->getPrice() * $multiplikator;

    }

    /**
     * @return Service
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * @param Service $service
     * @return PriceListService
     */
    public function setService($service)
    {
        $this->service = $service;

        return $this;
    }

    /**
     * Get price
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set price
     * @param float $price
     * @return PriceListService
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }
}

