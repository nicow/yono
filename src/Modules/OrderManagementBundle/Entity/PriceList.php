<?php

namespace Modules\OrderManagementBundle\Entity;

use Core\AppBundle\Annotation\Acl;
use Core\AppBundle\Entity\BlameableTrait;
use Doctrine\ORM\Mapping as ORM;
use Modules\ContactBundle\Entity\Contact;

/**
 * PriceList
 * @Acl("Preisliste")
 * @ORM\Table(name="mod_order_management_price_list")
 * @ORM\Entity(repositoryClass="Modules\OrderManagementBundle\Repository\PriceListRepository")
 */
class PriceList
{


    const TYPE_PRODUCT = 0;
    const TYPE_SERVICE = 1;

    use BlameableTrait;
    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="title", type="string", length=100, nullable=true)
     */
    private $title;

    /**
     * @var string
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var int
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;

    /**
     * @ORM\OneToMany(targetEntity="Modules\OrderManagementBundle\Entity\PriceListProduct", mappedBy="pricelist")
     */
    private $products;

    /**
     * @ORM\OneToMany(targetEntity="Modules\OrderManagementBundle\Entity\PriceListService", mappedBy="pricelist")
     */
    private $services;

    /**
     * @ORM\OneToOne(targetEntity="Modules\ContactBundle\Entity\Contact", mappedBy="pricelist")
     */
    private $contact;


    /**
     * Get id
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get title
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set title
     * @param string $title
     * @return PriceList
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get description
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set description
     * @param string $description
     * @return PriceList
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get status
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set status
     * @param integer $status
     * @return PriceList
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return PriceListProduct[]
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param PriceListProduct[] $products
     * @return PriceList
     */
    public function setProducts($products)
    {
        $this->products = $products;

        return $this;
    }

    /**
     * @return Contact
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * @param Contact $contact
     * @return PriceList
     */
    public function setContact($contact)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getServices()
    {
        return $this->services;
    }

    /**
     * @param mixed $services
     * @return PriceList
     */
    public function setServices($services)
    {
        $this->services = $services;

        return $this;
    }
}

