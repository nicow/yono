<?php

namespace Modules\OrderManagementBundle\Entity;

use Core\AppBundle\Entity\BlameableTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * JobTicketNote
 * @ORM\Table(name="mod_order_management_job_ticket_note")
 * @ORM\Entity(repositoryClass="Modules\OrderManagementBundle\Repository\JobTicketNoteRepository")
 */
class JobTicketNote
{

    use BlameableTrait;

    /**
     * @var JobTicket
     * @ORM\ManyToOne(targetEntity="Modules\OrderManagementBundle\Entity\JobTicket")
     */
    public $jobticket;
    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var string
     * @ORM\Column(name="text", type="text")
     */
    private $text;

    /**
     * Get id
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get text
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set text
     * @param string $text
     * @return JobTicketNote
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * @return JobTicket
     */
    public function getJobticket()
    {
        return $this->jobticket;
    }

    /**
     * @param JobTicket $jobticket
     * @return JobTicketNote
     */
    public function setJobticket($jobticket)
    {
        $this->jobticket = $jobticket;

        return $this;
    }
}

