<?php

namespace Modules\OrderManagementBundle\Entity;

use Core\AppBundle\Annotation\Acl;
use Core\AppBundle\Entity\BlameableTrait;
use Doctrine\ORM\Mapping as ORM;
use InvalidArgumentException;
use Modules\ContactBundle\Entity\Address;
use Modules\ContactBundle\Entity\Contact;
use Modules\ContactBundle\Entity\Person;
use Modules\OrderManagementBundle\Traits\DivisionTrait;
use Modules\ProjectBundle\Entity\Project;

/**
 * Invoice
 * @Acl("Rechnungen")
 * @ORM\Table(name="mod_order_management_invoice")
 * @ORM\Entity(repositoryClass="Modules\OrderManagementBundle\Repository\InvoiceRepository")
 */
class Invoice
{

    const STATUS_PREPARED = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_COMPLETED = 2;
    const STATUS_BILLED = 3;
    const STATUS_FIRSTWARNING = 4;
    const STATUS_SECONDWARNING = 5;
    const STATUS_LAWYER = 6;
    const STATUS_RESET = 7;
    const STATUS_CANCELED = 8;
    const STATUS_ARCHIV = 9;

    use BlameableTrait;
    use DivisionTrait;
    /**
     * @var Contact
     * @ORM\ManyToOne(targetEntity="Modules\ContactBundle\Entity\Contact",cascade={"persist"})
     */
    public $contact;
    /**
     * @var Address
     * @ORM\ManyToOne(targetEntity="Modules\ContactBundle\Entity\Address")
     */
    public $contactAddress;
    /**
     * @var Person
     * @ORM\ManyToOne(targetEntity="Modules\ContactBundle\Entity\Person", cascade={"persist"})
     */
    public $contactPerson;
    /**
     * @var Project
     * @ORM\ManyToOne(targetEntity="Modules\ProjectBundle\Entity\Project",cascade={"persist"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    public $project;

    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="title", type="string", length=100, nullable=true)
     */
    private $title;
    /**
     * @var string
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var \DateTime
     * @ORM\Column(name="printDate", type="date", nullable=true)
     */
    private $printDate;

    /**
     * @var string
     * @ORM\Column(name="number", type="string", length=40, nullable=true)
     */
    private $number;

    /**
     * @var int
     *
     * @ORM\Column(name="reminders", type="integer", nullable=true)
     */
    private $reminders;

    /**
     * @var int
     *
     * @ORM\Column(name="credit", type="integer", nullable=true)
     */
    private $credit;

    /**
     * @var string
     * @ORM\Column(name="placeholder", type="text", nullable=true)
     */
    private $placeholder;

    /**
     * @var int
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;


    /**
     * Get id
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get title
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set title
     * @param string $title
     * @return Invoice
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get description
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set description
     * @param string $description
     * @return Invoice
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get printDate
     * @return \DateTime
     */
    public function getPrintDate()
    {
        return $this->printDate;
    }

    /**
     * Set printDate
     * @param \DateTime $printDate
     * @return Invoice
     */
    public function setPrintDate($printDate)
    {
        $this->printDate = $printDate;

        return $this;
    }

    /**
     * Get status
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set status
     * @param integer $status
     * @return Invoice
     */
    public function setStatus($status)
    {

        if (in_array($status, [self::STATUS_PREPARED, self::STATUS_ARCHIV, self::STATUS_ACTIVE, self::STATUS_BILLED, self::STATUS_COMPLETED, self::STATUS_RESET, self::STATUS_CANCELED, self::STATUS_FIRSTWARNING, self::STATUS_SECONDWARNING, self::STATUS_LAWYER,])) {
            $this->status = $status;

            return $this;
        }

        throw new InvalidArgumentException('Invalid product status.');
    }

    /**
     * @return Contact
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * @param Contact $contact
     * @return Invoice
     */
    public function setContact($contact)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * @return Address
     */
    public function getContactAddress()
    {
        return $this->contactAddress;
    }

    /**
     * @param Address $contactAddress
     * @return Invoice
     */
    public function setContactAddress($contactAddress)
    {
        $this->contactAddress = $contactAddress;

        return $this;
    }

    /**
     * @return Person
     */
    public function getContactPerson()
    {
        return $this->contactPerson;
    }

    /**
     * @param Person $contactPerson
     * @return Invoice
     */
    public function setContactPerson($contactPerson)
    {
        $this->contactPerson = $contactPerson;

        return $this;
    }

    /**
     * @return Project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * @param Project $project
     * @return Invoice
     */
    public function setProject($project)
    {
        $this->project = $project;

        return $this;
    }

    public function __toString()
    {
        return $this->number.' - '.$this->title;
    }
    

    /**
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param string $number
     * @return Invoice
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * @param int $reminders
     * @return Invoice
     */
    public function setReminders($reminders)
    {
        $this->reminders = $reminders;
        return $this;
    }

    /**
     * @return int
     */
    public function getReminders()
    {
        return $this->reminders;
    }

    /**
     * @param int $credit
     * @return Invoice
     */
    public function setCredit($credit)
    {
        $this->credit = $credit;
        return $this;
    }

    /**
     * @return int
     */
    public function getCredit()
    {
        return $this->credit;
    }

    /**
     * @param string $placeholder
     * @return Invoice
     */
    public function setPlaceholder($placeholder)
    {
        $this->placeholder = $placeholder;

        return $this;
    }

    /**
     * @return string
     */
    public function getPlaceholder()
    {
        return $this->placeholder;
    }
}

