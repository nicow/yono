<?php

namespace Modules\OrderManagementBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InvoicePositions
 *
 * @ORM\Table(name="mod_order_management_invoice_positions")
 * @ORM\Entity(repositoryClass="Modules\OrderManagementBundle\Repository\InvoicePositionsRepository")
 */
class InvoicePositions
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Orders
     * @ORM\ManyToOne(targetEntity="Modules\OrderManagementBundle\Entity\Orders")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    public $orders;

    /**
     * @var Invoice
     * @ORM\ManyToOne(targetEntity="Modules\OrderManagementBundle\Entity\Invoice")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    public $invoice;

    /**
     * @var Calculation
     * @ORM\ManyToOne(targetEntity="Modules\OrderManagementBundle\Entity\Calculation")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    public $calculation;

    /**
     * @var int
     *
     * @ORM\Column(name="position", type="integer", nullable=true)
     */
    private $position;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set position
     *
     * @param integer $position
     *
     * @return InvoicePositions
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param Orders $orders
     * @return InvoicePositions
     */
    public function setOrders($orders)
    {
        $this->orders = $orders;
        return $this;
    }

    /**
     * @return Orders
     */
    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * @param Invoice $invoice
     * @return InvoicePositions
     */
    public function setInvoice($invoice)
    {
        $this->invoice = $invoice;
        return $this;
    }

    /**
     * @return Invoice
     */
    public function getInvoice()
    {
        return $this->invoice;
    }

    /**
     * @param Calculation $calculation
     * @return InvoicePositions
     */
    public function setCalculation($calculation)
    {
        $this->calculation = $calculation;
        return $this;
    }

    /**
     * @return Calculation
     */
    public function getCalculation()
    {
        return $this->calculation;
    }
}

