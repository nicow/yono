<?php

namespace Modules\OrderManagementBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class WorkingPaperPosition
 * @package Modules\OrderManagementBundle\Entity
 * @ORM\Table(name="mod_order_management_workingpaper_position")
 * @ORM\Entity()
 */
class WorkingPaperPosition
{
    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var JobTicket $jobticket
     * @ORM\ManyToOne(targetEntity="Modules\OrderManagementBundle\Entity\JobTicket")
     * @ORM\JoinColumn(name="jobticket_id", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     */
    private $jobticket;

    /**
     * @var Service $service
     * @ORM\ManyToOne(targetEntity="Modules\OrderManagementBundle\Entity\Service")
     * @ORM\JoinColumn(name="service_id", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     */
    private $service;

    /**
     * @var string $text
     * @ORM\Column(name="text", nullable=true, type="text")
     */
    private $text;

    /**
     * @var int
     *
     * @ORM\Column(name="position", type="integer", nullable=true)
     */
    private $position;

    /**
     * @var WorkingPaper $workingPaper
     * @ORM\ManyToOne(targetEntity="Modules\OrderManagementBundle\Entity\WorkingPaper", inversedBy="positions")
     * @ORM\JoinColumn(name="working_paper_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $workingPaper;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return JobTicket|null
     */
    public function getJobticket()
    {
        return $this->jobticket;
    }

    /**
     * @param JobTicket $jobticket
     */
    public function setJobticket(JobTicket $jobticket)
    {
        $this->jobticket = $jobticket;
    }

    /**
     * @return Service|null
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * @param Service $service
     */
    public function setService($service)
    {
        $this->service = $service;
    }

    /**
     * @return string|null
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param int $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * @return WorkingPaper
     */
    public function getWorkingPaper()
    {
        return $this->workingPaper;
    }

    /**
     * @param WorkingPaper $workingPaper
     */
    public function setWorkingPaper($workingPaper)
    {
        $this->workingPaper = $workingPaper;
    }
}