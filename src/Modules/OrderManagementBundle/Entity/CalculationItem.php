<?php

namespace Modules\OrderManagementBundle\Entity;

use Core\AppBundle\Entity\BlameableTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * CalculationItem
 * @ORM\Table(name="mod_order_management_calculation_item")
 * @ORM\Entity(repositoryClass="Modules\OrderManagementBundle\Repository\CalculationItemRepository")
 */
class CalculationItem
{

    use BlameableTrait;

    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Calculation
     * @ORM\ManyToOne(targetEntity="Modules\OrderManagementBundle\Entity\Calculation")
     */
    private $calculation;

    /**
     * @var Product
     * @ORM\ManyToOne(targetEntity="Modules\OrderManagementBundle\Entity\Product")
     */
    private $product;

    /**
     * @var Service
     * @ORM\ManyToOne(targetEntity="Modules\OrderManagementBundle\Entity\Service")
     */
    private $service;

    /**
     * @var string
     * @ORM\Column(name="freeText", type="text", nullable=true)
     */
    private $freeText;

    /**
     * @var string
     * @ORM\Column(name="freeTax", type="text", nullable=true)
     */
    private $freeTax;

    /**
     * @var string
     * @ORM\Column(name="freePriceType", type="string",length=100, nullable=true)
     */
    private $freePriceType;

    /**
     * @var float
     * @ORM\Column(name="specialPrice", type="float", nullable=true)
     */
    private $specialPrice;

    /**
     * @var float
     * @ORM\Column(name="singlePrice", type="float", nullable=true)
     */
    private $singlePrice;

    /**
     * @var float
     * @ORM\Column(name="amount", type="float", nullable=true)
     */
    private $amount;

    /**
     * @var int
     * @ORM\Column(name="discount", type="integer", nullable=true)
     */
    private $discount;

    /**
     * @var string
     * @ORM\Column(name="extraText", type="text", nullable=true)
     */
    private $extraText;

    /**
     * @var int
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;


    /**
     * Get id
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get specialPrice
     * @return float
     */
    public function getSpecialPrice()
    {
        return $this->specialPrice;
    }

    /**
     * Set specialPrice
     * @param float $specialPrice
     * @return CalculationItem
     */
    public function setSpecialPrice($specialPrice)
    {
        $this->specialPrice = $specialPrice;

        return $this;
    }

    /**
     * Get discount
     * @return int
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Set discount
     * @param integer $discount
     * @return CalculationItem
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Get status
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set status
     * @param integer $status
     * @return CalculationItem
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return Calculation
     */
    public function getCalculation()
    {
        return $this->calculation;
    }

    /**
     * @param Calculation $calculation
     * @return CalculationItem
     */
    public function setCalculation($calculation)
    {
        $this->calculation = $calculation;

        return $this;
    }

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param Product $product
     * @return CalculationItem
     */
    public function setProduct($product)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * @return Service
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * @param Service $service
     * @return CalculationItem
     */
    public function setService($service)
    {
        $this->service = $service;

        return $this;
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     * @return CalculationItem
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * @return string
     */
    public function getFreeText()
    {
        return $this->freeText;
    }

    /**
     * @param string $freeText
     * @return CalculationItem
     */
    public function setFreeText($freeText)
    {
        $this->freeText = $freeText;

        return $this;
    }

    /**
     * @return string
     */
    public function getFreePriceType()
    {
        return $this->freePriceType;
    }

    /**
     * @param string $freePriceType
     * @return CalculationItem
     */
    public function setFreePriceType($freePriceType)
    {
        $this->freePriceType = $freePriceType;

        return $this;
    }

    /**
     * @return string
     */
    public function getFreeTax()
    {
        return $this->freeTax;
    }

    /**
     * @param string $freeTax
     * @return CalculationItem
     */
    public function setFreeTax($freeTax)
    {
        $this->freeTax = $freeTax;

        return $this;
    }

    /**
     * @return float
     */
    public function getSinglePrice()
    {
        return $this->singlePrice;
    }

    /**
     * @param float $singlePrice
     * @return CalculationItem
     */
    public function setSinglePrice($singlePrice)
    {
        $this->singlePrice = $singlePrice;

        return $this;
    }

    /**
     * @return string
     */
    public function getExtraText()
    {
        return $this->extraText;
    }

    /**
     * @param string $extraText
     * @return CalculationItem
     */
    public function setExtraText($extraText)
    {
        $this->extraText = $extraText;

        return $this;
    }
}

