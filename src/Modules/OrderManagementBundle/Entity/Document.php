<?php

namespace Modules\OrderManagementBundle\Entity;

use Core\AppBundle\Entity\BlameableTrait;
use Doctrine\ORM\Mapping as ORM;
use MBence\OpenTBSBundle\Services\OpenTBS;
use Modules\OrderManagementBundle\Traits\DivisionTrait;


/**
 * Document
 * @ORM\Table(name="mod_order_management_document")
 * @ORM\Entity(repositoryClass="Modules\OrderManagementBundle\Repository\DocumentRepository")
 */
class Document extends OpenTBS
{
    use BlameableTrait;
    use DivisionTrait;


    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="vars", type="json_array", nullable=true)
     */
    private $vars;

    /**
     * @ORM\Column(name="template", type="string")
     */
    private $template;

    /**
     * @ORM\Column(name="compiled", type="string", nullable=true)
     */
    private $compiled;

    public function __construct($template = null)
    {
        parent::__construct();
        if ($template) {
            $this->loadTemplate($template);
            $this->loadVars($this->Source);
            $this->setTemplate(basename($template));
        }
    }

    private function loadVars($source)
    {

        preg_match_all('/\[.*?\]/', $source, $vars);

        $this->setVars($vars[0]);

    }

    public static function getDocument($name)
    {
        return realpath(dirname(__FILE__) . '/../Resources/documents') . DIRECTORY_SEPARATOR . $name;
    }

    public static function getRenderedDocument($name)
    {
        return dirname(dirname(__FILE__) . '/../Resources/documents/compiled/') . DIRECTORY_SEPARATOR . $name;
    }

    public static function getRenderedPath($name)
    {
        $prefix = self::generateRandomString();

        return realpath(dirname(__FILE__) . '/../Resources/documents/compiled/') . DIRECTORY_SEPARATOR . $prefix . '_' . $name;
    }

    public static function generateRandomString($length = 10)
    {
        return substr(str_shuffle(str_repeat($x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length / strlen($x)))), 1, $length);
    }

    public static function getConvertPath($name, $newExtention = 'pdf')
    {
        $prefix = self::generateRandomString();

        $ext = pathinfo($name, PATHINFO_EXTENSION);
        $name = str_replace($ext, $newExtention, $name);

        return realpath(dirname(__FILE__) . '/../Resources/documents/converted/') . DIRECTORY_SEPARATOR . $prefix . '_' . $name;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Document
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getVars()
    {
        return $this->vars;
    }

    /**
     * @param mixed $vars
     * @return Document
     */
    public function setVars($vars)
    {
        $this->vars = $vars;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * @param mixed $template
     * @return $this
     */
    public function setTemplate($template)
    {
        $this->template = $template;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCompiled()
    {
        return $this->compiled;
    }

    /**
     * @param mixed $compiled
     * @return Document
     */
    public function setCompiled($compiled)
    {
        $this->compiled = $compiled;

        return $this;
    }

}