<?php

namespace Modules\OrderManagementBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TermsOfPayment
 * @ORM\Table(name="mod_order_management_terms_of_payment")
 * @ORM\Entity(repositoryClass="Modules\OrderManagementBundle\Repository\TermsOfPaymentRepository")
 */
class TermsOfPayment
{
    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="handling", type="string", length=40)
     */
    private $handling;

    /**
     * @var int
     * @ORM\Column(name="discount", type="integer", nullable=true)
     */
    private $discount;

    /**
     * @var int
     * @ORM\Column(name="targetDiscount", type="integer", nullable=true)
     */
    private $targetDiscount;

    /**
     * @var int
     * @ORM\Column(name="targetNetto", type="integer", nullable=true)
     */
    private $targetNetto;


    /**
     * Get id
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get handling
     * @return string
     */
    public function getHandling()
    {
        return $this->handling;
    }

    /**
     * Set handling
     * @param string $handling
     * @return TermsOfPayment
     */
    public function setHandling($handling)
    {
        $this->handling = $handling;

        return $this;
    }

    /**
     * Get targetDiscount
     * @return int
     */
    public function getTargetDiscount()
    {
        return $this->targetDiscount;
    }

    /**
     * Set targetDiscount
     * @param integer $targetDiscount
     * @return TermsOfPayment
     */
    public function setTargetDiscount($targetDiscount)
    {
        $this->targetDiscount = $targetDiscount;

        return $this;
    }

    /**
     * Get targetNetto
     * @return int
     */
    public function getTargetNetto()
    {
        return $this->targetNetto;
    }

    /**
     * Set targetNetto
     * @param integer $targetNetto
     * @return TermsOfPayment
     */
    public function setTargetNetto($targetNetto)
    {
        $this->targetNetto = $targetNetto;

        return $this;
    }

    /**
     * @return int
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * @param int $discount
     * @return TermsOfPayment
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;

        return $this;
    }

    public function getName()
    {
        return $this->handling . ' - Skonto: ' . $this->discount . ' % - Skonto Ziel: ' . $this->targetDiscount . ' Tage - Fälligkeit: ' . $this->targetNetto . ' Tage';
    }
}

