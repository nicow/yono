<?php

namespace Modules\OrderManagementBundle\Entity;

/**
 * Created by PhpStorm.
 * User: Nico
 * Date: 07.06.2017
 * Time: 13:52
 */
use Core\AppBundle\Entity\BlameableTrait;
use Core\AppBundle\Entity\Documents;
use Doctrine\ORM\Mapping as ORM;
use Modules\OrderManagementBundle\Traits\DivisionTrait;

/**
 * Service
 * @ORM\Table(name="mod_order_management_template")
 * @ORM\Entity(repositoryClass="Modules\OrderManagementBundle\Repository\TemplateRepository")
 */
class Template
{
    use BlameableTrait;
    use DivisionTrait;

    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Documents $document
     * @ORM\Column(name="document")
     * @ORM\JoinColumn(name="document_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $document;
    /**
     * @var string $class
     * @ORM\Column(name="class", type="string")
     */
    private $class;

    /**
     * @return string
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * @param string $class
     * @return $this
     */
    public function setClass($class)
    {
        $this->class = basename(strtolower($class));
        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return Documents
     */
    public function getDocument()
    {
        return $this->document;
    }

    /**
     * @param Documents $document
     * @return $this
     */
    public function setDocument($document)
    {
        $this->document = $document;
        return $this;
    }
}