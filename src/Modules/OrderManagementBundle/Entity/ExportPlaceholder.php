<?php

namespace Modules\OrderManagementBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ExportPlaceholder
 *
 * @ORM\Table(name="export_placeholder")
 * @ORM\Entity(repositoryClass="Modules\OrderManagementBundle\Repository\ExportPlaceholderRepository")
 */
class ExportPlaceholder
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="text", nullable=true)
     */
    private $text;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return ExportPlaceholder
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }
}

