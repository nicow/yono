<?php

namespace Modules\OrderManagementBundle\Entity;

use Core\AppBundle\Annotation\Acl;
use Core\AppBundle\Entity\BlameableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use InvalidArgumentException;
use Modules\CalendarBundle\Entity\Event;
use Modules\ContactBundle\Entity\Person;
use Modules\ProjectBundle\Entity\Project;
use Modules\TeamBundle\Entity\Member;

/**
 * JobTicket
 * @Acl("Jobticket")
 * @ORM\Table(name="mod_order_management_job_ticket")
 * @ORM\Entity(repositoryClass="Modules\OrderManagementBundle\Repository\JobTicketRepository")
 */
class JobTicket
{
    const STATUS_PLANNED    = 0;
    const STATUS_OPEN       = 1;
    const STATUS_INPROGRESS = 2;
    const STATUS_IMPORTANT  = 3;
    const STATUS_DONE       = 4;
    const STATUS_ARCHIV     = 5;

    use BlameableTrait;
    /**
     * @var Person
     * @ORM\ManyToOne(targetEntity="Modules\ContactBundle\Entity\Person")
     */
    public $contactPerson;
    /**
     * @var Orders
     * @ORM\ManyToOne(targetEntity="Modules\OrderManagementBundle\Entity\Orders")
     */
    public $order;

    /**
     * @var Project
     * @ORM\ManyToOne(targetEntity="Modules\ProjectBundle\Entity\Project")
     */
    public $project;

    /**
     * @var Event[]
     * @ORM\ManyToMany(targetEntity="Modules\CalendarBundle\Entity\Event")
     * @ORM\JoinTable(name="mod_order_management_job_ticket_events")
     */
    private $event;

    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var string
     * @ORM\Column(name="title", type="string", length=100, nullable=true)
     */
    private $title;
    /**
     * @var Member[]
     * @ORM\ManyToMany(targetEntity="Modules\TeamBundle\Entity\Member")
     * @ORM\JoinTable(name="mod_order_management_job_ticket_members")
     */
    private $members;
    /**
     * @var string
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var \DateTime
     * @ORM\Column(name="fixDate", type="date", nullable=true)
     */
    private $fixDate;

    /**
     * @var int
     * @ORM\Column(name="visible", type="integer", nullable=true)
     */
    private $visible;

    /**
     * @var int
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;


    public function __construct()
    {
        $this->event = new ArrayCollection();
    }

    /**
     * Get id
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get title
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set title
     * @param string $title
     * @return JobTicket
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get description
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set description
     * @param string $description
     * @return JobTicket
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get fixDate
     * @return \DateTime
     */
    public function getFixDate()
    {
        return $this->fixDate;
    }

    /**
     * Set fixDate
     * @param \DateTime $fixDate
     * @return JobTicket
     */
    public function setFixDate($fixDate)
    {
        $this->fixDate = $fixDate;

        return $this;
    }

    /**
     * Get status
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set status
     * @param integer $status
     * @return JobTicket
     */
    public function setStatus($status)
    {
        if (in_array($status, [self::STATUS_DONE,
            self::STATUS_IMPORTANT,
            self::STATUS_INPROGRESS,
            self::STATUS_ARCHIV,
            self::STATUS_OPEN,
            self::STATUS_PLANNED,])) {
            $this->status = $status;

            return $this;
        }

        throw new InvalidArgumentException('Invalid product status.');
    }

    /**
     * @return Person
     */
    public function getContactPerson()
    {
        return $this->contactPerson;
    }

    /**
     * @param Person $contactPerson
     * @return JobTicket
     */
    public function setContactPerson($contactPerson)
    {
        $this->contactPerson = $contactPerson;

        return $this;
    }

    /**
     * @return Orders
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param Orders $order
     * @return JobTicket
     */
    public function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * @return \Modules\TeamBundle\Entity\Member[]
     */
    public function getMembers()
    {
        return $this->members;
    }

    /**
     * @param \Modules\TeamBundle\Entity\Member[] $members
     * @return JobTicket
     */
    public function setMembers($members)
    {
        $this->members = $members;

        return $this;
    }

    public function __toString()
    {
        return $this->title;
    }

    /**
     * @return int
     */
    public function getVisible()
    {
        return $this->visible;
    }

    /**
     * @param int $visible
     * @return JobTicket
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function removeElement($element)
    {
        $key = array_search($element, $this->elements, true);

        if ($key === false) {
            return false;
        }

        unset($this->elements[$key]);

        return true;
    }

    /**
     * Add event
     * @param Event $event
     * @return Event
     */
    public function addEvent(Event $event)
    {
        $this->event[] = $event;

        return $this;
    }

    /**
     * Remove event
     * @param Event $event
     */
    public function removeMember(Event $event)
    {
        $this->event->removeElement($event);
    }

    /**
     * @return Event
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * @param Event $event
     * @return JobTicket
     */
    public function setEvent($event)
    {
        $this->event = $event;

        return $this;
    }

    /**
     * @return Project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * @param Project $project
     * @return JobTicket
     */
    public function setProject($project)
    {
        $this->project = $project;

        return $this;
    }
}

