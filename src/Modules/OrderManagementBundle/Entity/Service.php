<?php

namespace Modules\OrderManagementBundle\Entity;

use Core\AppBundle\Annotation\Acl;
use Core\AppBundle\Entity\BlameableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use InvalidArgumentException;

/**
 * Service
 * @Acl("Leistung")
 * @ORM\Table(name="mod_order_management_service")
 * @ORM\Entity(repositoryClass="Modules\OrderManagementBundle\Repository\ServiceRepository")
 */
class Service
{

    const TYPE_HOUR          = 'hour';
    const TYPE_FLATRATE      = 'flatrate';
    const TYPE_SQUAREMETERS  = 'square meters';
    const TYPE_CUBICMETER    = 'cubic meter';
    const TYPE_RUNNINGMETERS = 'running meters';
    const TYPE_AMOUNT        = 'amount';
    const TYPE_KILO          = 'kg';
    const TYPE_LITER         = 'liter';
    const TYPE_TON           = 'ton';
    const TYPE_METER         = 'meter';

    use BlameableTrait;

    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var ServiceCategory[]
     * @ORM\ManyToMany(targetEntity="Modules\OrderManagementBundle\Entity\ServiceCategory", inversedBy="products")
     * @ORM\JoinTable(name="mod_order_management_service_categories")
     */
    private $categories;

    /**
     * @var string
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var string
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var float
     * @ORM\Column(name="price", type="float", nullable=true)
     */
    private $price;

    /**
     * @var string
     * @ORM\Column(name="priceType", type="string", length=25)
     */
    private $priceType;

    /**
     * @var int
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;

    /**
     * @var int
     * @ORM\Column(name="tax", type="integer")
     */
    private $tax;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->categories = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->title;
    }

    /**
     * Get id
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get title
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set title
     * @param string $title
     * @return Service
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get description
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set description
     * @param string $description
     * @return Service
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get priceType
     * @return string
     */
    public function getPriceType()
    {
        return $this->priceType;
    }

    /**
     * Set priceType
     * @param string $type
     * @return Service
     * @throws InvalidArgumentException
     */
    public function setPriceType($priceType)
    {
        if (in_array($priceType, [self::TYPE_HOUR,
            self::TYPE_FLATRATE,
            self::TYPE_CUBICMETER,
            self::TYPE_SQUAREMETERS,
            self::TYPE_RUNNINGMETERS,
            self::TYPE_AMOUNT,
            self::TYPE_LITER,
            self::TYPE_METER,
            self::TYPE_TON,
            self::TYPE_KILO,])) {
            $this->priceType = $priceType;

            return $this;
        }

        throw new InvalidArgumentException('Invalid price type.');

    }

    /**
     * Get priceType
     * @return string
     */
    public function getPriceTypeName()
    {
        $priceName = null;
        switch ($this->priceType) {
            case self::TYPE_AMOUNT:
                $priceName = 'Stk.';
                break;
            case self::TYPE_SQUAREMETERS:
                $priceName = 'm²';
                break;
            case self::TYPE_FLATRATE:
                $priceName = 'psch.';
                break;
            case self::TYPE_RUNNINGMETERS:
                $priceName = 'Lfm.';
                break;
            case self::TYPE_CUBICMETER:
                $priceName = 'm³';
                break;
            case self::TYPE_KILO:
                $priceName = 'kg.';
                break;
            case self::TYPE_TON:
                $priceName = 't';
                break;
            case self::TYPE_LITER:
                $priceName = 'ltr';
                break;
            case self::TYPE_METER:
                $priceName = 'm';
                break;
            default:
                $priceName = 'psch.';
                break;

        }

        return $priceName;


    }

    /**
     * Get status
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set status
     * @param integer $status
     * @return Service
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Add category
     * @param ServiceCategory $category
     * @return Service
     */
    public function addCategory(ServiceCategory $category)
    {
        $this->categories[] = $category;

        return $this;
    }

    /**
     * @return ServiceCategory[]
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @param ServiceCategory[] $categories
     * @return Service
     */
    public function setCategories($categories)
    {
        $this->categories = $categories;

        return $this;
    }

    /**
     * @return float
     */
    public function getBrutto()
    {
        /*
         * Make it possible to use Taxes like 19 for 19 percent or 0.19
         */
        $tax = $this->getTax();
        if ($tax > 0) {
            $tax = $tax / 100;
        }

        $multiplikator = 1 + $tax;

        return $this->getPrice() * $multiplikator;

    }

    /**
     * @return int
     */
    public function getTax()
    {
        return $this->tax;
    }

    /**
     * @param int $tax
     * @return Service
     */
    public function setTax($tax)
    {
        $this->tax = $tax;

        return $this;
    }

    /**
     * Get price
     * @return float
     */
    public function getPrice($format = false)
    {
        if ($format) {
            return number_format($this->price, 2, ',', '.');
        }

        return $this->price;
    }

    /**
     * Set price
     * @param float $price
     * @return Service
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }
}

