<?php

namespace Modules\OrderManagementBundle\Entity;

use Core\AppBundle\Annotation\Acl;
use Core\AppBundle\Entity\BlameableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * ServiceCategory
 * @Acl("Kategorien Leistungen")
 * @ORM\Table(name="mod_order_management_service_category")
 * @ORM\Entity(repositoryClass="Modules\OrderManagementBundle\Repository\ServiceCategoryRepository")
 */
class ServiceCategory
{

    use BlameableTrait;

    /**
     * @var ServiceCategory[]
     * @ORM\OneToMany(targetEntity="ServiceCategory", mappedBy="parent")
     */
    protected $children;
    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=100, nullable=true)
     */
    private $name;
    /**
     * @var string
     * @ORM\Column(name="icon", type="string", length=20, nullable=true)
     */
    private $icon;
    /**
     * @var boolean
     * @ORM\Column(name="show_in_menu", type="boolean", nullable=true)
     */
    private $showInMenu;
    /**
     * @var ServiceCategory
     * @ORM\ManyToOne(targetEntity="ServiceCategory", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $parent;

    /**
     * @var Service[]
     * @ORM\ManyToMany(targetEntity="Modules\OrderManagementBundle\Entity\Service", mappedBy="categories")
     */
    private $products;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->products = new ArrayCollection();
        $this->children = new ArrayCollection();
    }

    /**
     * Get id
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     * @param string $name
     * @return ServiceCategory
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get icon
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * Set icon
     * @param string $icon
     * @return ServiceCategory
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * Get showInMenu
     * @return boolean
     */
    public function getShowInMenu()
    {
        return $this->showInMenu;
    }

    /**
     * Set showInMenu
     * @param boolean $showInMenu
     * @return ServiceCategory
     */
    public function setShowInMenu($showInMenu)
    {
        $this->showInMenu = $showInMenu;

        return $this;
    }

    /**
     * @return ServiceCategory
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param ServiceCategory $parent
     * @return ServiceCategory
     */
    public function setParent($parent)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return Service[]
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param Service[] $products
     * @return ServiceCategory
     */
    public function setProducts($products)
    {
        $this->products = $products;

        return $this;
    }

    /**
     * @return ServiceCategory[]
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param ServiceCategory[] $children
     * @return ServiceCategory
     */
    public function setChildren($children)
    {
        $this->children = $children;

        return $this;
    }

    public function __toString()
    {
        return $this->name;
    }
}

