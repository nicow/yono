<?php

namespace Modules\OrderManagementBundle\Entity;

use Core\AppBundle\Annotation\Acl;
use Core\AppBundle\Entity\BlameableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use InvalidArgumentException;

/**
 * Product
 * @Acl("Produkt")
 * @ORM\Table(name="mod_order_management_product")
 * @ORM\Entity(repositoryClass="Modules\OrderManagementBundle\Repository\ProductRepository")
 */
class Product
{

    const TYPE_PIECE         = 0;
    const TYPE_SQUAREMETERS  = 1;
    const TYPE_FLATRATE      = 2;
    const TYPE_CUBICMETER    = 3;
    const TYPE_RUNNINGMETERS = 4;
    const TYPE_HOUR          = 5;
    const TYPE_KILO          = 6;
    const TYPE_LITER         = 7;
    const TYPE_TON           = 8;
    const TYPE_METER         = 9;

    use BlameableTrait;

    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var ProductCategory[]
     * @ORM\ManyToMany(targetEntity="Modules\OrderManagementBundle\Entity\ProductCategory", inversedBy="products")
     * @ORM\JoinTable(name="mod_order_management_product_categories")
     */
    private $categories;

    /**
     * @var string
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var string
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var float
     * @ORM\Column(name="price", type="float", nullable=true)
     */
    private $price;

    /**
     * @var string
     * @ORM\Column(name="priceType", type="string", length=25)
     */
    private $priceType;

    /**
     * @var float
     * @ORM\Column(name="shippingCosts", type="float", nullable=true)
     */
    private $shippingCosts;

    /**
     * @var int
     * @ORM\Column(name="deliveryTime", type="integer", nullable=true)
     */
    private $deliveryTime;

    /**
     * @var int
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;

    /**
     * @var int
     * @ORM\Column(name="tax", type="integer")
     */
    private $tax;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->categories = new ArrayCollection();
    }


    /**
     * Get id
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get title
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set title
     * @param string $title
     * @return Product
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get description
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set description
     * @param string $description
     * @return Product
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get priceType
     * @return string
     */
    public function getPriceType()
    {
        return $this->priceType;
    }

    /**
     * Set priceType
     * @param string $type
     * @return Product
     * @throws InvalidArgumentException
     */
    public function setPriceType($priceType)
    {
        if (in_array($priceType, [self::TYPE_PIECE,
            self::TYPE_SQUAREMETERS,
            self::TYPE_FLATRATE,
            self::TYPE_CUBICMETER,
            self::TYPE_RUNNINGMETERS,
            self::TYPE_METER,
            self::TYPE_TON,
            self::TYPE_HOUR,
            self::TYPE_KILO,
            self::TYPE_LITER,])) {
            $this->priceType = $priceType;

            return $this;
        }

        throw new InvalidArgumentException('Invalid product type.');

    }

    /**
     * Get priceType
     * @return string
     */
    public function getPriceTypeName()
    {
        $priceName = null;
        switch ($this->priceType) {
            case self::TYPE_PIECE:
                $priceName = 'Stk.';
                break;
            case self::TYPE_SQUAREMETERS:
                $priceName = 'm²';
                break;
            case self::TYPE_FLATRATE:
                $priceName = 'psch.';
                break;
            case self::TYPE_RUNNINGMETERS:
                $priceName = 'Lfm.';
                break;
            case self::TYPE_CUBICMETER:
                $priceName = 'm³';
                break;
            case self::TYPE_KILO:
                $priceName = 'kg.';
                break;
            case self::TYPE_TON:
                $priceName = 't';
                break;
            case self::TYPE_LITER:
                $priceName = 'ltr';
                break;
            case self::TYPE_METER:
                $priceName = 'm';
                break;
            default:
                $priceName = 'psch.';
                break;

        }

        return $priceName;
    }

    /**
     * Get shippingCosts
     * @return float
     */
    public function getShippingCosts()
    {
        return $this->shippingCosts;
    }

    /**
     * Set shippingCosts
     * @param float $shippingCosts
     * @return Product
     */
    public function setShippingCosts($shippingCosts)
    {
        $this->shippingCosts = $shippingCosts;

        return $this;
    }

    /**
     * Get deliveryTime
     * @return int
     */
    public function getDeliveryTime()
    {
        return $this->deliveryTime;
    }

    /**
     * Set deliveryTime
     * @param integer $deliveryTime
     * @return Product
     */
    public function setDeliveryTime($deliveryTime)
    {
        $this->deliveryTime = $deliveryTime;

        return $this;
    }

    /**
     * Get status
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set status
     * @param integer $status
     * @return Product
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Add category
     * @param ProductCategory $category
     * @return Product
     */
    public function addCategory(ProductCategory $category)
    {
        $this->categories[] = $category;

        return $this;
    }

    /**
     * @return ProductCategory[]
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @param ProductCategory[] $categories
     * @return Product
     */
    public function setCategories($categories)
    {
        $this->categories = $categories;

        return $this;
    }

    public function getBrutto()
    {
        /*
         * Make it possible to use Taxes like 19 for 19 percent or 0.19
         */
        $tax = $this->getTax();
        if ($tax > 0) {
            $tax = $tax / 100;
        }

        $multiplikator = 1 + $tax;

        return $this->getPrice() * $multiplikator;

    }

    /**
     * @return int
     */
    public function getTax()
    {
        return $this->tax;
    }

    /**
     * @param int $tax
     * @return Product
     */
    public function setTax($tax)
    {
        $this->tax = $tax;

        return $this;
    }

    /**
     * Get price
     * @return float
     */
    public function getPrice($format = false)
    {
        if ($format) {
            return number_format($this->price, 2, ',', '.');
        }

        return $this->price;
    }

    /**
     * Set price
     * @param float $price
     * @return Product
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }
}

