<?php

namespace Modules\OrderManagementBundle\Entity;

use Core\AppBundle\Entity\BlameableTrait;
use Core\AppBundle\Entity\Documents;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Modules\ContactBundle\Entity\Contact;
use Modules\OrderManagementBundle\Traits\DivisionTrait;
use Modules\TeamBundle\Entity\Member;
use Sluggable\Fixture\Position;

/**
 * Class WorkingPaper
 * @ORM\Table(name="mod_order_management_workingpaper")
 * @ORM\Entity()
 */
class WorkingPaper
{

    use BlameableTrait;
    use DivisionTrait;

    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Orders $order
     * @ORM\ManyToOne(targetEntity="Modules\OrderManagementBundle\Entity\Orders")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $order;

    /**
     * @var Member $member
     * @ORM\ManyToOne(targetEntity="Modules\TeamBundle\Entity\Member")
     * @ORM\JoinColumn(name="member_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $member;

    /**
     * @var Contact $contact
     * @ORM\ManyToOne(targetEntity="Modules\ContactBundle\Entity\Contact")
     * @ORM\JoinColumn(name="contact_id", referencedColumnName="id", onDelete="SET NULL")
     *
     */
    private $contact;

    /**
     * @var Documents $signature
     * @ORM\OneToOne(targetEntity="Core\AppBundle\Entity\Documents")
     * @ORM\JoinColumn(name="signature_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $signature;

    /**
     * @var string $title
     * @ORM\Column(name="title", type="string")
     */
    private $title;

    /**
     * @var boolean $custom
     * @ORM\Column(name="custom", type="boolean")
     */
    private $custom;

    /**
     * @var string $customResult
     * @ORM\Column(name="custom_result", type="text", length=500000, nullable=true)
     */
    private $customResult;

    /**
     * @var WorkingPaperPosition[] $positions
     * @ORM\OneToMany(targetEntity="Modules\OrderManagementBundle\Entity\WorkingPaperPosition", mappedBy="workingPaper", cascade={"persist"})
     */
    private $positions;

    public function __construct()
    {
        $this->positions = new ArrayCollection();
    }

    /**
     * @return Documents
     */
    public function getSignature()
    {
        return $this->signature;

    }

    /**
     * @param Documents $signature
     */
    public function setSignature($signature)
    {
        $this->signature = $signature;
    }

    /**
     * @return Member
     */
    public function getMember()
    {
        return $this->member;
    }

    /**
     * @param Member $member
     */
    public function setMember($member)
    {
        $this->member = $member;
    }

    /**
     * @return \Modules\OrderManagementBundle\Entity\Orders
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param \Modules\OrderManagementBundle\Entity\Orders $order
     */
    public function setOrder($order)
    {
        $this->order = $order;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Contact
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * @param Contact $contact
     */
    public function setContact($contact)
    {
        $this->contact = $contact;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Add position
     * @param WorkingPaperPosition $position
     * @return WorkingPaper
     */
    public function addPosition(WorkingPaperPosition $position)
    {
        $position->setWorkingPaper($this);
        $this->positions[] = $position;

        return $this;
    }

    /**
     * Remove position
     * @param WorkingPaperPosition $position
     */
    public function removePosition(WorkingPaperPosition $position)
    {
        $this->positions->removeElement($position);
    }

    /**
     * Get position
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPositions()
    {
        return $this->positions;
    }

    /**
     * @return bool
     */
    public function isCustom()
    {
        return $this->custom;
    }

    /**
     * @param bool $custom
     */
    public function setCustom($custom)
    {
        $this->custom = $custom;
    }

    /**
     * @return string
     */
    public function getCustomResult()
    {
        return $this->customResult;
    }

    /**
     * @param string $customResult
     */
    public function setCustomResult($customResult)
    {
        $this->customResult = $customResult;
    }
}