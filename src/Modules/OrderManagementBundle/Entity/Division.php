<?php
/**
 * Created by PhpStorm.
 * User: Nico
 * Date: 11.05.2017
 * Time: 10:17
 */


namespace Modules\OrderManagementBundle\Entity;

use Core\AppBundle\Entity\Documents;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Division
 * @ORM\Table(name="mod_order_management_division")
 * @ORM\Entity(repositoryClass="Modules\OrderManagementBundle\Repository\DivisionRepository")
 */
class Division
{

    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=100)
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(name="info", type="text", length=2048, nullable=true)
     */
    private $info;

    /**
     * @var int
     * @ORM\Column(name="offerCount", type="integer" ,nullable=true)
     */
    private $offerCount;

    /**
     * @var int
     * @ORM\Column(name="orderCount", type="integer" ,nullable=true)
     */
    private $orderCount;

    /**
     * @var int
     * @ORM\Column(name="invoiceCount", type="integer" ,nullable=true)
     */
    private $invoiceCount;

    /**
     * @var int
     * @ORM\Column(name="reminderCount", type="integer" ,nullable=true)
     */
    private $reminderCount;

    /**
     * @var Documents|UploadedFile
     * @ORM\OneToOne(targetEntity="Core\AppBundle\Entity\Documents")
     * @ORM\JoinColumn(name="offer_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $offerTemplate;
    /**
     * @var Documents|UploadedFile
     * @ORM\OneToOne(targetEntity="Core\AppBundle\Entity\Documents")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $orderTemplate;
    /**
     * @var Documents|UploadedFile
     * @ORM\OneToOne(targetEntity="Core\AppBundle\Entity\Documents")
     * @ORM\JoinColumn(name="invoice_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $invoiceTemplate;
    /**
     * @var Documents|UploadedFile
     * @ORM\OneToOne(targetEntity="Core\AppBundle\Entity\Documents")
     * @ORM\JoinColumn(name="invoice_netto_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $invoiceNettoTemplate;
    /**
     * @var Documents|UploadedFile
     * @ORM\OneToOne(targetEntity="Core\AppBundle\Entity\Documents")
     * @ORM\JoinColumn(name="reminder_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $reminderTemplate;
    /**
     * @var Documents|UploadedFile
     * @ORM\OneToOne(targetEntity="Core\AppBundle\Entity\Documents")
     * @ORM\JoinColumn(name="credit_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $creditTemplate;

    /**
     * @var Documents|UploadedFile
     * @ORM\OneToOne(targetEntity="Core\AppBundle\Entity\Documents")
     * @ORM\JoinColumn(name="working_paper_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $workingPaperTemplate;




    /**
     * @var string
     * @ORM\Column(name="tag", type="string", length=5)
     */
    private $tag;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Division
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * @param string $info
     * @return Division
     */
    public function setInfo($info)
    {
        $this->info = $info;

        return $this;
    }

    /**
     * @return string
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * @param string $tag
     * @return Division
     */
    public function setTag($tag)
    {
        $this->tag = $tag;

        return $this;
    }

    public function __toString()
    {
        return $this->getName();
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Division
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return int
     */
    public function getInvoiceCount()
    {
        return $this->invoiceCount;
    }

    /**
     * @param int $invoiceCount
     * @return Division
     */
    public function setInvoiceCount($invoiceCount)
    {
        $this->invoiceCount = $invoiceCount;

        return $this;
    }

    /**
     * @return int
     */
    public function getOrderCount()
    {
        return $this->orderCount;
    }

    /**
     * @param int $orderCount
     * @return Division
     */
    public function setOrderCount($orderCount)
    {
        $this->orderCount = $orderCount;

        return $this;
    }

    /**
     * @return int
     */
    public function getOfferCount()
    {
        return $this->offerCount;
    }

    /**
     * @param int $offerCount
     * @return Division
     */
    public function setOfferCount($offerCount)
    {
        $this->offerCount = $offerCount;

        return $this;
    }

    /**
     * @param int $reminderCount
     * @return Division
     */
    public function setReminderCount($reminderCount)
    {
        $this->reminderCount = $reminderCount;
        return $this;
    }

    /**
     * @return int
     */
    public function getReminderCount()
    {
        return $this->reminderCount;
    }

    /**
     * @return Documents|UploadedFile
     */
    public function getCreditTemplate()
    {
        return $this->creditTemplate;
    }

    /**
     * @param Documents $creditTemplate
     */
    public function setCreditTemplate($creditTemplate)
    {
        $this->creditTemplate = $creditTemplate;
    }

    /**
     * @return Documents|UploadedFile
     */
    public function getReminderTemplate()
    {
        return $this->reminderTemplate;
    }

    /**
     * @param Documents $reminderTemplate
     */
    public function setReminderTemplate($reminderTemplate)
    {
        $this->reminderTemplate = $reminderTemplate;
    }

    /**
     * @return Documents|UploadedFile
     */
    public function getInvoiceNettoTemplate()
    {
        return $this->invoiceNettoTemplate;
    }

    /**
     * @param Documents $invoiceNettoTemplate
     */
    public function setInvoiceNettoTemplate($invoiceNettoTemplate)
    {
        $this->invoiceNettoTemplate = $invoiceNettoTemplate;
    }

    /**
     * @return Documents|UploadedFile
     */
    public function getInvoiceTemplate()
    {
        return $this->invoiceTemplate;
    }

    /**
     * @param Documents $invoiceTemplate
     */
    public function setInvoiceTemplate($invoiceTemplate)
    {
        $this->invoiceTemplate = $invoiceTemplate;
    }

    /**
     * @return Documents|UploadedFile
     */
    public function getOrderTemplate()
    {
        return $this->orderTemplate;
    }

    /**
     * @param Documents $orderTemplate
     */
    public function setOrderTemplate($orderTemplate)
    {
        $this->orderTemplate = $orderTemplate;
    }

    /**
     * @return Documents|UploadedFile
     */
    public function getOfferTemplate()
    {
        return $this->offerTemplate;
    }

    /**
     * @param Documents $offerTemplate
     */
    public function setOfferTemplate($offerTemplate)
    {
        $this->offerTemplate = $offerTemplate;
    }

    /**
     * @return Documents|UploadedFile
     */
    public function getWorkingPaperTemplate()
    {
        return $this->workingPaperTemplate;
    }

    /**
     * @param Documents|UploadedFile $workingPaperTemplate
     */
    public function setWorkingPaperTemplate($workingPaperTemplate)
    {
        $this->workingPaperTemplate = $workingPaperTemplate;
    }
}