<?php

namespace Modules\TimetrackBundle\Entity;

use Core\AppBundle\Annotation\Acl;
use Core\AppBundle\Entity\BlameableTrait;
use Core\AppBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use InvalidArgumentException;
use Modules\OrderManagementBundle\Entity\JobTicket;
use Modules\ProjectBundle\Entity\Project;

/**
 * Timetrack
 * @Acl("Zeiterfassung")
 * @ORM\Table(name="mod_timetrack_timetrack")
 * @ORM\Entity(repositoryClass="Modules\TimetrackBundle\Repository\TimetrackRepository")
 */
class Timetrack
{
    use BlameableTrait;

    const STATUS_BREAK = 0;
    const STATUS_DONE  = 1;

    const TYPE_JOB          = 1;
    const TYPE_PROJECT      = 2;
    const TYPE_FREEJOB      = 3;
    const TYPE_DRIVE        = 4;
    const TYPE_CONSTRUCTION = 5;
    const TYPE_DONE         = 6;

    /**
     * @var Project
     * @ORM\ManyToOne(targetEntity="Modules\ProjectBundle\Entity\Project")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    public $project;
    /**
     * @var JobTicket
     * @ORM\ManyToOne(targetEntity="Modules\OrderManagementBundle\Entity\JobTicket")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    public $jobticket;
    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var \DateTime
     * @ORM\Column(name="start", type="datetime")
     */
    private $start;
    /**
     * @var \DateTime
     * @ORM\Column(name="stop", type="datetime", nullable=true)
     */
    private $stop;
    /**
     * @var string
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;
    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $user;

    /**
     * @var integer
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;

    /**
     * @var integer
     * @ORM\Column(name="typ", type="integer", nullable=true)
     */
    private $typ;

    /**
     * @var integer
     * @ORM\Column(name="break", type="integer", nullable=true)
     */
    private $break;


    /**
     * Get id
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get start
     * @return \DateTime
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Set start
     * @param \DateTime $start
     * @return Timetrack
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * Get stop
     * @return \DateTime
     */
    public function getStop()
    {
        return $this->stop;
    }

    /**
     * Set stop
     * @param \DateTime $stop
     * @return Timetrack
     */
    public function setStop($stop)
    {
        $this->stop = $stop;

        return $this;
    }

    /**
     * Get comment
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set comment
     * @param string $comment
     * @return Timetrack
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get user
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set user
     * @param User $user
     * @return Timetrack
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * @param Project $project
     * @return Timetrack
     */
    public function setProject($project)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getJobticket()
    {
        return $this->jobticket;
    }

    /**
     * @param mixed $jobticket
     * @return Timetrack
     */
    public function setJobticket($jobticket)
    {
        $this->jobticket = $jobticket;

        return $this;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return Timetrack
     */
    public function setStatus($status)
    {
        if (in_array($status, [self::STATUS_DONE, self::STATUS_BREAK])) {
            $this->status = $status;

            return $this;
        }

        throw new InvalidArgumentException('Invalid timestrack status.');
    }

    /**
     * @return int
     */
    public function getBreak()
    {
        return $this->break;
    }

    /**
     * @param int $break
     * @return Timetrack
     */
    public function setBreak($break)
    {
        $this->break = $break;

        return $this;
    }

    /**
     * @return int
     */
    public function getTyp()
    {
        return $this->typ;
    }

    /**
     * @param int $typ
     * @return Timetrack
     */
    public function setTyp($typ)
    {
        if (in_array($typ, [self::TYPE_FREEJOB,
            self::TYPE_JOB,
            self::TYPE_DRIVE,
            self::TYPE_PROJECT,
            self::TYPE_CONSTRUCTION,
            self::TYPE_DONE])) {
            $this->typ = $typ;

            return $this;
        }

        throw new InvalidArgumentException('Invalid timestrack status.');
    }
}
