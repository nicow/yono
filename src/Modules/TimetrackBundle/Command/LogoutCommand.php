<?php

namespace Modules\TimetrackBundle\Command;

use ColourStream\Bundle\CronBundle\Annotation\CronJob;
use Modules\TimetrackBundle\Entity\Timetrack;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


/**
 * Created by PhpStorm.
 * User: tim
 * Date: 05.07.2017
 * Time: 08:23
 */

/**
 * Class LogoutCommand
 * @package Modules\TimetrackBundle\Command
 * @CronJob(interval="PT30M")
 */
class LogoutCommand extends ContainerAwareCommand
{
    public function configure()
    {
        $this->setName('yono:modules:timetrack:logout')->setDescription('stops the last ticket from users which where login in longer then 12 hours');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $em = $this->getContainer()->get('doctrine')->getManager();

        $timetrack = $em->getRepository('ModulesTimetrackBundle:Timetrack');

        $jobs = $timetrack->findAllOpen();


        if (empty($jobs)) {
            return 1;
        } else {

            /**
             * @var Timetrack $job
             */
            foreach ($jobs as $job) {
                $start_date = new \DateTime($job->getStart()->format('Y-m-d H:i:s'));
                $since_start = $start_date->diff(new \DateTime(date('Y-m-d H:i:s')));

                $minutes = $since_start->days * 24 * 60;
                $minutes += $since_start->h * 60;
                $minutes += $since_start->i;

                if (round($minutes / 60) >= 12) {
                    $job->setStop(new \DateTime(date('Y-m-d H:i:s')));
                    $job->setStatus(Timetrack::STATUS_DONE);
                    $em->flush();
                }
            }
        }


        return 0;
    }
}