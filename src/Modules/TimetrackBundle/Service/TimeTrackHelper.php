<?php
/**
 * Created by PhpStorm.
 * User: mkt
 * Date: 31.12.16
 * Time: 04:09
 */

namespace Modules\TimetrackBundle\Service;


use Core\AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;

use Modules\OrderManagementBundle\Entity\JobTicket;
use Modules\TimetrackBundle\Entity\Timetrack;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\RequestStack;

class TimeTrackHelper
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var RequestStack
     */
    private $requestStack;
    /**
     * @var Container
     */
    private $container;


    public function __construct(EntityManager $em, RequestStack $requestStack, Container $container)
    {
        $this->em = $em;
        $this->requestStack = $requestStack;
        $this->request = $this->requestStack->getCurrentRequest();
        $this->container = $container;
    }

    /**
     * @param User $user
     */
    public function startTracking($user)
    {

        if ($this->container->get('app.helper')->can($user, 'VIEW', Timetrack::class) && $this->container->getParameter('modules_timetrack_modus') != 1) {

            /**
             * @var Timetrack $lastJob
             */
            $lastJob = $this->em->getRepository('ModulesTimetrackBundle:Timetrack')->findLast($user);

            if ($lastJob) {
                $lastJob->setStatus(Timetrack::STATUS_DONE);
                $lastJob->setStop(new \DateTime(date('Y-m-d H:i:s')));
                $this->em->flush();
            }

            $timetrack = new Timetrack();
            $timetrack->setUser($user);
            $timetrack->setStart(new \DateTime(date('Y-m-d H:i:s')));
            $timetrack->setStatus(Timetrack::STATUS_BREAK);
            $timetrack->setBreak(1);
            $this->em->persist($timetrack);
            $this->em->flush();
        }

    }

    /**
     * @param User $user
     * @return string
     */
    public function getCurrentJobTitle($user)
    {
        /**
         * @var Timetrack $lastJob
         */
        $lastJob = $this->em->getRepository('ModulesTimetrackBundle:Timetrack')->findLast($user);

        if ($lastJob) {
            if ($lastJob->getStatus() == Timetrack::STATUS_BREAK AND $lastJob->getBreak()) {
                return 'Pause';
            } elseif ($lastJob->getTyp() == Timetrack::TYPE_JOB) {
                return $lastJob->getJobticket()->getTitle();
            } elseif ($lastJob->getTyp() == Timetrack::TYPE_PROJECT) {
                return $lastJob->getProject()->getName();
            } elseif ($lastJob->getTyp() == Timetrack::TYPE_FREEJOB) {
                if ($lastJob->getComment()) return $lastJob->getComment(); else
                    return 'Büro';
            } elseif ($lastJob->getTyp() == Timetrack::TYPE_CONSTRUCTION) {
                if ($lastJob->getComment()) return $lastJob->getComment(); else
                    return 'Baustelle';
            } elseif ($lastJob->getTyp() == Timetrack::TYPE_DRIVE) {
                if ($lastJob->getComment()) return $lastJob->getComment(); else
                    return 'Fahrt';
            } else {
                return '-';
            }
        } else {
            return '-';
        }
    }

    public function getType(Timetrack $lastJob)
    {
        if ($lastJob->getBreak()) {
            return 'Pause';
        } elseif ($lastJob->getTyp() == Timetrack::TYPE_JOB) {
            return 'Jobticket';
        } elseif ($lastJob->getTyp() == Timetrack::TYPE_PROJECT) {
            return 'Projekt';
        } elseif ($lastJob->getTyp() == Timetrack::TYPE_FREEJOB) {
            return 'Büro';
        } elseif ($lastJob->getTyp() == Timetrack::TYPE_DRIVE) {
            return 'Fahrt';
        } elseif ($lastJob->getTyp() == Timetrack::TYPE_CONSTRUCTION) {
            return 'Baustelle';
        } else {
            return '-';
        }
    }


    /**
     * @param User $user
     * @return array
     */
    public function getUserToday(User $user)
    {
        /**
         * @var Timetrack $lastJob
         */
        $todayJobs = $this->em->getRepository('ModulesTimetrackBundle:Timetrack')->findToday($user);


        if ($todayJobs) {
            $today = [];
            $hours = 0;
            $minutes = 0;
            $countTicket = 0;
            foreach ($todayJobs as $todayJob) {
                $start_date = new \DateTime($todayJob->getStart()->format('Y-m-d H:i:s'));
                if ($todayJob->getStop()) $since_start = $start_date->diff((new \DateTime($todayJob->getStop()->format('Y-m-d H:i:s')))); else
                    $since_start = $start_date->diff(new \DateTime(date('Y-m-d H:i:s')));
                $hours = $hours + $since_start->h;
                $minutes = $minutes + $since_start->i;
                if ($todayJob->getJobticket()) {
                    $countTicket++;
                }
            }
            if ($countTicket) $today['percent'] = round((($countTicket) / count($todayJobs)) * 100); else
                $today['percent'] = 0;

            $today['hours'] = $hours + (round($minutes / 60));

            return $today;
        } else {
            $today = [];
            $today['percent'] = 0;
            $today['hours'] = 0;

            return $today;
        }
    }

    /**
     * @param User $user
     * @return array
     */
    public function getLastJobs($user)
    {
        /**
         * @var Timetrack $lastJob
         */
        $lastJob = $this->em->getRepository('ModulesTimetrackBundle:Timetrack')->findLastJobs($user);

        $lastJobs = [];

        $count = 0;
        if ($lastJob) {
            foreach ($lastJob as $item) {
                if ($item->getBreak() != 1) {
                    $lastJobs[$count]['comment'] = '';
                    if ($item->getTyp() == Timetrack::TYPE_JOB) {
                        $lastJobs[$count]['title'] = $item->getJobticket()->getTitle();
                        $lastJobs[$count]['comment'] = $item->getComment();
                    } elseif ($item->getTyp() == Timetrack::TYPE_PROJECT) {
                        $lastJobs[$count]['title'] = $item->getProject()->getName();
                        $lastJobs[$count]['comment'] = $item->getComment();
                    } elseif ($item->getTyp() == Timetrack::TYPE_FREEJOB) {
                        $lastJobs[$count]['title'] = 'Büro';
                        $lastJobs[$count]['comment'] = $item->getComment();
                    } elseif ($item->getTyp() == Timetrack::TYPE_DRIVE) {
                        $lastJobs[$count]['title'] = 'Fahrt';
                        $lastJobs[$count]['comment'] = $item->getComment();
                    } elseif ($item->getTyp() == Timetrack::TYPE_CONSTRUCTION) {
                        $lastJobs[$count]['title'] = 'Baustelle';
                        $lastJobs[$count]['comment'] = $item->getComment();
                    }
                    $lastJobs[$count]['id'] = $item->getId();
                    $count++;
                }
            }
        }


        return $lastJobs;
    }

    /**
     * @param JobTicket $jobticket
     * @return array|bool|\Modules\TimetrackBundle\Entity\Timetrack[]
     */
    public function getBookingsByJobTicket($jobticket)
    {
        /**
         * @var Timetrack $lastJob
         */
        $jobs = $this->em->getRepository('ModulesTimetrackBundle:Timetrack')->findBy(['jobticket' => $jobticket->getId()]);

        if ($jobs) {
            return $jobs;
        } else {
            return false;
        }
    }

    /**
     * @param JobTicket $jobticket
     * @return array|bool|\Modules\TimetrackBundle\Entity\Timetrack[]
     */
    public function getHoursForJobTicket($jobticket)
    {
        /**
         * @var Timetrack $lastJob
         */
        $jobs = $this->em->getRepository('ModulesTimetrackBundle:Timetrack')->findBy(['jobticket' => $jobticket->getId()]);

        if ($jobs) {
            $hours = 0;
            $minutes = 0;
            foreach ($jobs as $job) {
                $start_date = new \DateTime($job->getStart()->format('Y-m-d H:i:s'));
                if ($job->getStop()) $since_start = $start_date->diff((new \DateTime($job->getStop()->format('Y-m-d H:i:s')))); else
                    $since_start = $start_date->diff(new \DateTime(date('Y-m-d H:i:s')));
                $hours = $hours + $since_start->h;
                $minutes = $minutes + $since_start->i;
            }

            $plusHours = round($minutes / 60, 0, PHP_ROUND_HALF_DOWN);
            $hours = $hours + $plusHours;
            $totalminutes = $minutes - ($plusHours * 60);

            if ($totalminutes < 10) $totalminutes = '0' . $totalminutes;

            return $hours . ':' . $totalminutes;
        } else {
            return 0;
        }
    }


    /**
     * @param User $user
     * @return bool|Timetrack
     */
    public function getCurrentJob($user)
    {
        /**
         * @var Timetrack $lastJob
         */
        $lastJob = $this->em->getRepository('ModulesTimetrackBundle:Timetrack')->findLast($user);

        if ($lastJob) {
            return $lastJob;
        } else {
            return false;
        }
    }

    /**
     * @param User $user
     * @return bool|Timetrack
     */
    public function getLastRealJob($user)
    {
        /**
         * @var Timetrack $lastJob
         */
        $lastJob = $this->em->getRepository('ModulesTimetrackBundle:Timetrack')->findLastReal($user);

        if ($lastJob) {
            return $lastJob;
        } else {
            return false;
        }
    }

    /**
     * @param Timetrack $job
     * @return string
     */
    public function getCurrentDuration($job)
    {
        $start_date = new \DateTime($job->getStart()->format('Y-m-d H:i:s'));
        $since_start = $start_date->diff(new \DateTime(date('Y-m-d H:i:s')));
        $minuten = $since_start->i;
        $seconds = $since_start->s;
        if ($since_start->h > 0) {
            $minuten = $since_start->i + ($since_start->h * 60);
        }
        if (strlen($minuten) == 1) {
            $minuten = '0' . $minuten;
        }
        if (strlen($since_start->s) == 1) {
            $seconds = '0' . $seconds;
        }

        return $minuten . ':' . $seconds;


    }
}