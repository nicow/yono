<?php

namespace Modules\TimetrackBundle\Controller;

use Modules\TimetrackBundle\Entity\Timetrack;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TimetrackController extends Controller
{
    /**
     * Lists all Timetrack entities.
     * @Route("/", name="timetrack_index")
     * @Security("is_granted('VIEW', 'Modules\\TimetrackBundle\\Entity\\Timetrack')")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        if ($request->query->all()) {
            $this->get('app.settings')->set('mod_timetrack_index_overview', $request->query->all());
        }

        $filter = $this->get('app.settings')->get('mod_timetrack_index_overview');

        $timetracks = $em->getRepository('ModulesTimetrackBundle:Timetrack')->findAllByFilter($filter);

        $deleteForms = [];
        foreach ($timetracks as $timetrack) {
            $deleteForms[$timetrack->getId()] = $this->createDeleteForm($timetrack)->createView();
        }


        return $this->render('ModulesTimetrackBundle:Timetrack:index.html.twig', ['timetracks' => $timetracks,
            'filterSettings' => $filter,
            'deleteForms' => $deleteForms]);
    }

    /**
     * Creates a form to delete a Timetrack entity.
     * @param Timetrack $timetrack The Timetrack entity
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Timetrack $timetrack)
    {
        return $this->createFormBuilder()->setAction($this->generateUrl('timetrack_delete', ['id' => $timetrack->getId()]))->setMethod('DELETE')->getForm();
    }

    /**
     * Lists all Timetrack entities.
     * @Route("/user", name="timetrack_user")
     * @Security("is_granted('VIEW', 'Modules\\TimetrackBundle\\Entity\\Timetrack')")
     * @Method("GET")
     */
    public function userAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        if ($request->query->all()) {
            $this->get('app.settings')->set('mod_timetrack_user_overview', $request->query->all());
        }

        $filter = $this->get('app.settings')->get('mod_timetrack_user_overview');

        $user = $this->get('security.token_storage')->getToken()->getUser();
        $timetracks = $em->getRepository('ModulesTimetrackBundle:Timetrack')->findToday($user, false, $filter);

        return $this->render('ModulesTimetrackBundle:Timetrack:user.html.twig', ['timetracks' => $timetracks,
            'filterSettings' => $filter]);
    }

    /**
     * Lists all poosible jobs.
     * @Route("/jobs", name="timetrack_jobs")
     * @Security("is_granted('VIEW', 'Modules\\TimetrackBundle\\Entity\\Timetrack')")
     * @Method("GET")
     */
    public function jobsAction()
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->get('security.token_storage')->getToken()->getUser();

        $jobtickets = false;
        if ($user->getMember()) $jobtickets = $em->getRepository('ModulesOrderManagementBundle:JobTicket')->findByMemberJobs($user->getMember());

        return $this->render('ModulesTimetrackBundle:Timetrack:jobs.html.twig', ['jobtickets' => $jobtickets]);
    }

    /**
     * Creates a new Timetrack entity.
     * @Route("/new/{user}", name="timetrack_new")
     * @Security("is_granted('CREATE', 'Modules\\TimetrackBundle\\Entity\\Timetrack')")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request, $user = false)
    {
        $timetrack = new Timetrack();
        if ($user) $timetrack->setUser($this->get('security.token_storage')->getToken()->getUser());
        $form = $this->createForm('Modules\TimetrackBundle\Form\TimetrackType', $timetrack);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $all = $request->request->all();

            if ($all['timetrack']['intern'] == '1') {
                $timetrack->setBreak(1);
                $timetrack->setStatus(Timetrack::STATUS_BREAK);
                $timetrack->setStatus(Timetrack::STATUS_DONE);
            } elseif ($all['timetrack']['intern'] == '2') {
                $timetrack->setTyp(Timetrack::TYPE_FREEJOB);
                $timetrack->setStatus(Timetrack::STATUS_DONE);
            } elseif ($all['timetrack']['intern'] == '3') {
                $timetrack->setTyp(Timetrack::TYPE_DRIVE);
                $timetrack->setStatus(Timetrack::STATUS_DONE);
            } elseif ($all['timetrack']['intern'] == '5') {
                $timetrack->setTyp(Timetrack::TYPE_CONSTRUCTION);
                $timetrack->setStatus(Timetrack::STATUS_DONE);
            }

            if ($timetrack->getJobticket()) {
                $timetrack->setTyp(Timetrack::TYPE_JOB);
                $timetrack->setStatus(Timetrack::STATUS_DONE);
            }

            if (!$timetrack->getTyp() && !$timetrack->getBreak()) {
                $timetrack->setBreak(1);
                $timetrack->setStatus(Timetrack::STATUS_DONE);
            }

            if (!$timetrack->getStart() && !$timetrack->getStop()) {
                $timetrack->setStatus(Timetrack::STATUS_DONE);
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($timetrack);
            $em->flush();

            $this->addFlash('success', '<i class="uk-icon-check"></i> Der Eintrag wurde erfolgreich angelegt!');

            if ($user) return $this->redirectToRoute('timetrack_user'); else
                return $this->redirectToRoute('timetrack_index', ['id' => $timetrack->getId()]);
        }

        return $this->render('ModulesTimetrackBundle:Timetrack:new.html.twig', ['timetrack' => $timetrack,
            'user' => $user,
            'form' => $form->createView(),]);
    }

    /**
     * Creates a new Timetrack entity.
     * @Route("/newajax", name="timetrack_new_ajax")
     * @Security("is_granted('CREATE', 'Modules\\TimetrackBundle\\Entity\\Timetrack')")
     * @Method({"GET", "POST"})
     */
    public function newAjaxAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->get('security.token_storage')->getToken()->getUser();

        $typ = '';
        if ($request->getMethod() == 'POST') {

            //CHECK IF OLDJOB MUST BE FINISHED WITH SPEREATE TIME
            if ($request->request->get('current')) {
                /**
                 * @var Timetrack $oldtTimetrack
                 */
                $oldtTimetrack = $em->getRepository('ModulesTimetrackBundle:Timetrack')->find($request->request->get('current'));
                if ($request->request->get('close_old_job') == 1) {
                    $stop = $request->request->get('time_ende_date') . ' ' . $request->request->get('form_ende_hour') . ':' . $request->request->get('form_ende_minuten') . ':00';
                    $oldtTimetrack->setStop(new \DateTime(date('Y-m-d H:i:s', strtotime($stop))));

                    if ($oldtTimetrack->getStop()->format('Y-m-d H:i') != date('Y-m-d H:i')) {
                        $timetrackBreak = new Timetrack();
                        $timetrackBreak->setUser($user);
                        $timetrackBreak->setStart(new \DateTime(date('Y-m-d H:i:s', strtotime($stop))));
                        $timetrackBreak->setStop(new \DateTime(date('Y-m-d H:i:s')));
                        $timetrackBreak->setStatus(Timetrack::STATUS_DONE);
                        $timetrackBreak->setBreak(1);
                        $em->persist($timetrackBreak);
                    }

                } else {
                    $oldtTimetrack->setStop(new \DateTime(date('Y-m-d H:i:s')));
                }
                $oldtTimetrack->setStatus(Timetrack::STATUS_DONE);
            }


            $timetrack = new Timetrack();
            $timetrack->setUser($user);
            $timetrack->setStart(new \DateTime(date('Y-m-d H:i:s')));
            $timetrack->setComment($request->request->get('comment'));

            if ($request->request->get('internal')) {
                if ($request->request->get('internal') == '0') {
                    $typ = 'Pause';
                    $timetrack->setBreak(1);
                    $timetrack->setStatus($request->request->get('internal'));
                } elseif ($request->request->get('internal') == '4') {
                    $timetrack->setTyp(Timetrack::TYPE_DRIVE);
                    $typ = 'Fahrt';
                } elseif ($request->request->get('internal') == '5') {
                    $timetrack->setTyp(Timetrack::TYPE_CONSTRUCTION);
                    $typ = 'Baustelle';
                } elseif ($request->request->get('internal') == '6') {
                    $timetrack->setTyp(Timetrack::TYPE_DONE);
                    $typ = 'Ausstempeln';
                } else {
                    $timetrack->setTyp(Timetrack::TYPE_FREEJOB);
                    $typ = 'Büro';
                }


            } elseif ($request->request->get('jobticket')) {
                $jobTickt = $em->getRepository('ModulesOrderManagementBundle:JobTicket')->find($request->request->get('jobticket'));
                $timetrack->setJobticket($jobTickt);
                $timetrack->setTyp(Timetrack::TYPE_JOB);
                $typ = $jobTickt->getTitle();
            } else {
                $timetrack->setBreak(1);
                $timetrack->setStatus(Timetrack::STATUS_BREAK);
                $typ = 'Pause';
            }


            if ($timetrack->getTyp() != Timetrack::TYPE_DONE) {
                $em->persist($timetrack);
            }

            $em->flush();
        }

        return new Response($typ, 200);
    }

    /**
     * Creates a new Timetrack break entity.
     * @Route("/newbreak", name="timetrack_new_break")
     * @Security("is_granted('CREATE', 'Modules\\TimetrackBundle\\Entity\\Timetrack')")
     * @Method({"GET", "POST"})
     */
    public function newBreakAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->get('security.token_storage')->getToken()->getUser();

        if ($this->getParameter('modules_timetrack_modus') != 1) {
            $timetrack = new Timetrack();
            $timetrack->setUser($user);
            $timetrack->setStart(new \DateTime(date('Y-m-d H:i:s')));
            $timetrack->setStatus(Timetrack::STATUS_BREAK);
            $timetrack->setBreak(1);
            $em->persist($timetrack);
        }


        if ($request->request->get('job')) {
            $oldtTimetrack = $em->getRepository('ModulesTimetrackBundle:Timetrack')->find($request->request->get('job'));
            $oldtTimetrack->setStop(new \DateTime(date('Y-m-d H:i:s')));
            $oldtTimetrack->setStatus(Timetrack::STATUS_DONE);
        }


        $em->flush();

        return new Response('', 200);

    }

    /**
     * continue Timetrack entity.
     * @Route("/continue", name="timetrack_continue")
     * @Security("is_granted('CREATE', 'Modules\\TimetrackBundle\\Entity\\Timetrack')")
     * @Method({"GET", "POST"})
     */
    public function continueAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->get('security.token_storage')->getToken()->getUser();


        if ($request->request->get('job')) {
            $oldtTimetrackData = $em->getRepository('ModulesTimetrackBundle:Timetrack')->find($request->request->get('job'));
            $timetrack = new Timetrack();
            $timetrack->setUser($user);
            $timetrack->setStart(new \DateTime(date('Y-m-d H:i:s')));
            if ($oldtTimetrackData->getJobticket()) $timetrack->setTyp(Timetrack::TYPE_JOB); elseif ($oldtTimetrackData->getProject()) $timetrack->setTyp(Timetrack::TYPE_PROJECT);
            else
                $timetrack->setTyp(Timetrack::TYPE_FREEJOB);

            $timetrack->setComment($oldtTimetrackData->getComment());
            $timetrack->setJobticket($oldtTimetrackData->getJobticket());
            $timetrack->setProject($oldtTimetrackData->getProject());
            $em->persist($timetrack);
        }


        if ($request->request->get('currentjob')) {
            $oldtTimetrack = $em->getRepository('ModulesTimetrackBundle:Timetrack')->find($request->request->get('currentjob'));
            $oldtTimetrack->setStop(new \DateTime(date('Y-m-d H:i:s')));
            $oldtTimetrack->setStatus(Timetrack::STATUS_DONE);
        }


        $em->flush();

        return new Response('', 200);

    }

    /**
     * continue Timetrack entity.
     * @Route("/continue/oldjob/{timetrack}", name="timetrack_continue_old")
     * @Security("is_granted('CREATE', 'Modules\\TimetrackBundle\\Entity\\Timetrack')")
     * @Method({"GET"})
     */
    public function continueOldAction(Timetrack $timetrack)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->get('security.token_storage')->getToken()->getUser();


        if ($timetrack) {
            $oldtTimetrackData = $em->getRepository('ModulesTimetrackBundle:Timetrack')->find($timetrack);
            $timetrack = new Timetrack();
            $timetrack->setUser($user);
            $timetrack->setStart(new \DateTime(date('Y-m-d H:i:s')));

            if ($oldtTimetrackData->getJobticket()) $timetrack->setTyp(Timetrack::TYPE_JOB); elseif ($oldtTimetrackData->getProject()) $timetrack->setTyp(Timetrack::TYPE_PROJECT);
            else
                $timetrack->setTyp(Timetrack::TYPE_FREEJOB);

            $timetrack->setComment($oldtTimetrackData->getComment());
            $timetrack->setJobticket($oldtTimetrackData->getJobticket());
            $timetrack->setProject($oldtTimetrackData->getProject());
            $em->persist($timetrack);
        }


        if ($currentJob = $em->getRepository('ModulesTimetrackBundle:Timetrack')->findLast($user)) {
            $currentJob->setStop(new \DateTime(date('Y-m-d H:i:s')));
            $currentJob->setStatus(Timetrack::STATUS_DONE);
        }


        $em->flush();

        return new Response('', 200);

    }

    /**
     * reload nav.
     * @Route("/reload", name="timetrack_reload")
     * @Method({"GET", "POST"})
     */
    public function reloadAction(Request $request)
    {
        $this->get('session')->migrate();

        return $this->render('ModulesTimetrackBundle:Timetrack:nav.html.twig');
    }

    /**
     * Displays a form to edit an existing Timetrack entity.
     * @Route("/{id}/edit/{user}", name="timetrack_edit")
     * @Security("is_granted('EDIT', 'Modules\\TimetrackBundle\\Entity\\Timetrack')")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Timetrack $timetrack, $user = false)
    {
        $deleteForm = $this->createDeleteForm($timetrack);
        $editForm = $this->createForm('Modules\TimetrackBundle\Form\TimetrackType', $timetrack);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {

            $all = $request->request->all();

            if ($all['timetrack']['intern'] == '1') {
                $timetrack->setBreak(1);
                $timetrack->setStatus(Timetrack::STATUS_BREAK);
            } elseif ($all['timetrack']['intern'] == '2') {
                $timetrack->setTyp(Timetrack::TYPE_FREEJOB);
                $timetrack->setBreak(NULL);
                $timetrack->setStatus(Timetrack::STATUS_DONE);
            } elseif ($all['timetrack']['intern'] == '3') {
                $timetrack->setTyp(Timetrack::TYPE_DRIVE);
                $timetrack->setBreak(NULL);
                $timetrack->setStatus(Timetrack::STATUS_DONE);
            } elseif ($all['timetrack']['intern'] == '5') {
                $timetrack->setTyp(Timetrack::TYPE_CONSTRUCTION);
                $timetrack->setBreak(NULL);
                $timetrack->setStatus(Timetrack::STATUS_DONE);
            }

            if ($timetrack->getJobticket()) {
                $timetrack->setTyp(Timetrack::TYPE_JOB);
                $timetrack->setBreak(NULL);
                $timetrack->setStatus(Timetrack::STATUS_DONE);
            }

            if (!$timetrack->getTyp() && !$timetrack->getBreak()) {
                $timetrack->setBreak(1);
                $timetrack->setStatus(Timetrack::STATUS_BREAK);
            }

            if (!$timetrack->getStart() && !$timetrack->getStop()) {
                $timetrack->setStatus(Timetrack::STATUS_DONE);
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($timetrack);
            $em->flush();

            $this->addFlash('success', '<i class="uk-icon-check"></i> Der Eintrag wurde erfolgreich bearbeitet!');

            if ($user) return $this->redirectToRoute('timetrack_user'); else
                return $this->redirectToRoute('timetrack_index');
        }

        return $this->render('ModulesTimetrackBundle:Timetrack:edit.html.twig', ['timetrack' => $timetrack,
            'edit_form' => $editForm->createView(),
            'user' => $user,
            'delete_form' => $deleteForm->createView(),]);
    }

    /**
     * Deletes a Timetrack entity.
     * @Route("/{id}", name="timetrack_delete")
     * @Security("is_granted('DELETE', 'Modules\\TimetrackBundle\\Entity\\Timetrack')")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Timetrack $timetrack)
    {
        $form = $this->createDeleteForm($timetrack);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($timetrack);
            $em->flush();

            $this->addFlash('success', '<i class="uk-icon-check"></i> Der Eintrag wurde erfolgreich gelöscht!');
        }

        return $this->redirectToRoute('timetrack_index');
    }

    /**
     * Deletes a Timetrack entity.
     * @Route("/deletemarked", name="timetrack_delete_marked")
     * @Security("is_granted('DELETE', 'Modules\\TimetrackBundle\\Entity\\Timetrack')")
     * @Method("POST")
     */
    public function deleteMarkedAction(Request $request)
    {

        if ($request->getMethod() == 'POST') {
            $em = $this->getDoctrine()->getManager();
            $allEntities = $request->request->get('deleteall');

            foreach ($allEntities as $entity) {
                $delete = $em->getRepository('ModulesTimetrackBundle:Timetrack')->find($entity);

                if ($delete) {
                    $em->remove($delete);
                    $em->flush();
                }
            }


            $this->addFlash('success', '<i class="uk-icon-check"></i> Die Einträge wurden erfolgreich gelöscht!');
        }

        return $this->redirectToRoute('timetrack_index');
    }
}
