<?php

namespace Modules\TimetrackBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TimetrackType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $data = 1;
        if ($options['data']->getTyp() == 1) {
            $data = 4;
        }
        if ($options['data']->getTyp() == 3) {
            $data = 2;
        }
        if ($options['data']->getTyp() == 4) {
            $data = 3;
        }
        if ($options['data']->getTyp() == 5) {
            $data = 5;
        }
        $builder->add('comment', null, ['label' => 'Info', 'required' => false]);
        $builder->add('user', null, ['label' => 'User', 'placeholder' => 'User wählen', 'required' => true]);
        $builder->add('intern', ChoiceType::class, array('choices' => array('Jobticket' => '4',
            'Pause' => '1',
            'Büro' => '2',
            'Fahrt' => '3',
            'Baustelle' => '5'),
            'label' => 'Tätigkeit',
            'data' => $data,
            'choices_as_values' => true,
            'multiple' => false,
            'expanded' => true,
            'required' => true,
            'mapped' => false));
        $builder->add('jobticket', null, ['label' => 'Jobticket',
            'placeholder' => 'Jobticket wählen',
            'required' => false]);
        $builder->add('start', DateTimeType::class, ['date_widget' => 'single_text',
            'time_widget' => 'choice',
            'hours' => [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23],
            'minutes' => [0,
                1,
                2,
                3,
                4,
                5,
                6,
                7,
                8,
                9,
                10,
                11,
                12,
                13,
                14,
                15,
                16,
                17,
                18,
                19,
                20,
                21,
                22,
                23,
                24,
                25,
                26,
                27,
                28,
                29,
                30,
                31,
                32,
                33,
                34,
                35,
                36,
                37,
                38,
                39,
                40,
                41,
                42,
                43,
                44,
                45,
                46,
                47,
                48,
                49,
                50,
                51,
                52,
                53,
                54,
                55,
                56,
                57,
                58,
                59],
            'required' => true,
            'date_format' => 'd.M.y']);
        $builder->add('stop', DateTimeType::class, ['date_widget' => 'single_text',
            'time_widget' => 'choice',
            'hours' => [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23],
            'minutes' => [0,
                1,
                2,
                3,
                4,
                5,
                6,
                7,
                8,
                9,
                10,
                11,
                12,
                13,
                14,
                15,
                16,
                17,
                18,
                19,
                20,
                21,
                22,
                23,
                24,
                25,
                26,
                27,
                28,
                29,
                30,
                31,
                32,
                33,
                34,
                35,
                36,
                37,
                38,
                39,
                40,
                41,
                42,
                43,
                44,
                45,
                46,
                47,
                48,
                49,
                50,
                51,
                52,
                53,
                54,
                55,
                56,
                57,
                58,
                59],
            'required' => true,
            'date_format' => 'd.M.y']);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array('data_class' => 'Modules\TimetrackBundle\Entity\Timetrack'));
    }
}
