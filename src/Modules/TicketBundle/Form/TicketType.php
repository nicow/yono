<?php

namespace Modules\TicketBundle\Form;

use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Modules\TicketBundle\Entity\Ticket;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TicketType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', null, ['label' => 'Titel'])->add('status', ChoiceType::class, ['choices' => ['offen' => Ticket::STATUS_OPEN, 'dringend' => Ticket::STATUS_URGENT, 'in Arbeit' => Ticket::STATUS_IN_PROGRESS, 'erledigt' => Ticket::STATUS_DONE]])->add('dueDate', DateTimeType::class, ['date_widget' => 'single_text', 'time_widget' => 'single_text', 'required' => false, 'date_format' => 'd.M.y'])->add('project', null, ['label' => 'Projekt', 'required' => false])->add('user', null, ['label' => 'Benutzer', 'required' => false])->add('text', CKEditorType::class, ['label' => 'Beschreibung', 'required' => false])->add('notes', CollectionType::class, ['label' => false, 'entry_type' => NoteType::class, 'allow_add' => true, 'allow_delete' => true, 'by_reference' => false, 'entry_options' => ['label' => false]]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array('data_class' => Ticket::class,));
    }
}
