<?php

namespace Modules\TicketBundle\Entity;

use Core\AppBundle\Annotation\Acl;
use Core\AppBundle\Entity\BlameableTrait;
use Core\AppBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\Exception\InvalidArgumentException;
use Doctrine\ORM\Mapping as ORM;
use Modules\ProjectBundle\Entity\Project;

/**
 * Ticket
 * @Acl("Tickets")
 * @ORM\Table(name="mod_ticket_ticket")
 * @ORM\Entity(repositoryClass="Modules\TicketBundle\Repository\TicketRepository")
 */
class Ticket
{
    use BlameableTrait;

    const STATUS_OPEN = 'open';
    const STATUS_URGENT = 'urgent';
    const STATUS_IN_PROGRESS = 'in_progress';
    const STATUS_DONE = 'done';

    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(name="status", type="string")
     */
    private $status;

    /**
     * @var \DateTime
     * @ORM\Column(name="due_date", type="datetime", nullable=true)
     */
    private $dueDate;

    /**
     * @var Project
     * @ORM\ManyToOne(targetEntity="Modules\ProjectBundle\Entity\Project", inversedBy="tickets")
     */
    private $project;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\User")
     */
    private $user;

    /**
     * @var string
     * @ORM\Column(name="text", type="text", nullable=true)
     */
    private $text;

    /**
     * @var Note[]
     * @ORM\OneToMany(targetEntity="Modules\TicketBundle\Entity\Note", mappedBy="ticket", cascade={"persist"})
     * @ORM\OrderBy({"createdAt" = "DESC"})
     */
    private $notes;

    /**
     * @var Document[]
     * @ORM\OneToMany(targetEntity="Modules\TicketBundle\Entity\Document", mappedBy="ticket", cascade={"persist"})
     * @ORM\OrderBy({"createdAt" = "DESC"})
     */
    private $documents;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->notes = new ArrayCollection();
        $this->documents = new ArrayCollection();
    }

    /**
     * Get id
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     * @param string $name
     * @return Ticket
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get status
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set status
     * @param integer $status
     * @return Ticket
     * @throws InvalidArgumentException
     */
    public function setStatus($status)
    {
        if (in_array($status, [self::STATUS_OPEN, self::STATUS_URGENT, self::STATUS_IN_PROGRESS, self::STATUS_DONE])) {
            $this->status = $status;

            return $this;
        }

        throw new InvalidArgumentException();
    }

    /**
     * Get dueDate
     * @return \DateTime
     */
    public function getDueDate()
    {
        return $this->dueDate;
    }

    /**
     * Set dueDate
     * @param \DateTime $dueDate
     * @return Ticket
     */
    public function setDueDate($dueDate)
    {
        $this->dueDate = $dueDate;

        return $this;
    }

    /**
     * Get project
     * @return Project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Set project
     * @param Project $project
     * @return Ticket
     */
    public function setProject(Project $project = null)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get user
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set user
     * @param User $user
     * @return Ticket
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get text
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set text
     * @param string $text
     * @return Ticket
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Add note
     * @param Note $note
     * @return Ticket
     */
    public function addNote(Note $note)
    {
        $note->setTicket($this);
        $this->notes[] = $note;

        return $this;
    }

    /**
     * Remove note
     * @param Note $note
     */
    public function removeNote(Note $note)
    {
        $this->notes->removeElement($note);
    }

    /**
     * Get notes
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Add document
     * @param Document $document
     * @return Ticket
     */
    public function addDocument(Document $document)
    {
        $this->documents[] = $document;

        return $this;
    }

    /**
     * Remove document
     * @param Document $document
     */
    public function removeDocument(Document $document)
    {
        $this->documents->removeElement($document);
    }

    /**
     * Get documents
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDocuments()
    {
        return $this->documents;
    }
}
