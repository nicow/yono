<?php

namespace Modules\TicketBundle\Entity;

use Core\AppBundle\Entity\BlameableTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * Note
 * @ORM\Table(name="mod_ticket_note")
 * @ORM\Entity(repositoryClass="Modules\TicketBundle\Repository\NoteRepository")
 */
class Note
{
    use BlameableTrait;

    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="text", type="text")
     */
    private $text;

    /**
     * @var Ticket
     * @ORM\ManyToOne(targetEntity="Modules\TicketBundle\Entity\Ticket", inversedBy="notes")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $ticket;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * Get id
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get text
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set text
     * @param string $text
     * @return Note
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get ticket
     * @return Ticket
     */
    public function getTicket()
    {
        return $this->ticket;
    }

    /**
     * Set ticket
     * @param Ticket $ticket
     * @return Note
     */
    public function setTicket(Ticket $ticket = null)
    {
        $this->ticket = $ticket;

        return $this;
    }
}
