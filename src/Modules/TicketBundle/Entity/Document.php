<?php

namespace Modules\TicketBundle\Entity;

use Core\AppBundle\Entity\BlameableTrait;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Document
 * @ORM\Table(name="mod_ticket_document")
 * @ORM\Entity()
 * @Gedmo\Uploadable(path="uploads/tickets/documents", appendNumber=true)
 */
class Document
{
    use BlameableTrait;

    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="path", type="string", length=255)
     * @Gedmo\UploadableFilePath
     */
    private $path;

    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=255)
     * @Gedmo\UploadableFileName
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(name="mime", type="string", length=255)
     * @Gedmo\UploadableFileMimeType
     */
    private $mime;

    /**
     * @var float
     * @ORM\Column(name="size", type="decimal")
     * @Gedmo\UploadableFileSize
     */
    private $size;

    /**
     * @var Ticket
     * @ORM\ManyToOne(targetEntity="Modules\TicketBundle\Entity\Ticket", inversedBy="documents")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $ticket;


    /**
     * Get id
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get path
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set path
     * @param string $path
     * @return Document
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get name
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     * @param string $name
     * @return Document
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get mime
     * @return string
     */
    public function getMime()
    {
        return $this->mime;
    }

    /**
     * Set mime
     * @param string $mime
     * @return Document
     */
    public function setMime($mime)
    {
        $this->mime = $mime;

        return $this;
    }

    /**
     * Get size
     * @return string
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Set size
     * @param string $size
     * @return Document
     */
    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    /**
     * Get ticket
     * @return Ticket
     */
    public function getTicket()
    {
        return $this->ticket;
    }

    /**
     * Set ticket
     * @param Ticket $ticket
     * @return Document
     */
    public function setTicket(Ticket $ticket = null)
    {
        $this->ticket = $ticket;

        return $this;
    }
}
