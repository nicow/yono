<?php

namespace Modules\TicketBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Modules\TicketBundle\Entity\Document;
use Modules\TicketBundle\Entity\Ticket;
use Modules\TicketBundle\Form\TicketType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TicketController extends Controller
{
    /**
     * Lists all Ticket entities.
     * @Route("/")
     * @Method("GET")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $tickets = $em->getRepository('ModulesTicketBundle:Ticket')->findBy(['user' => $this->getUser()]);
        if ($this->isGranted('ROLE_ADMIN')) {
            $tickets = $em->getRepository('ModulesTicketBundle:Ticket')->findAll();
        }
        $columns = $this->get('app.helper')->setTableColumns('mod_contact_table_columns', ['addresses' => ['active' => 1, 'label' => 'Adressen'], 'persons' => ['active' => 1, 'label' => 'Ansprechpartner'], 'bank_accounts' => ['active' => 1, 'label' => 'Kontoverbindungen'], 'projects' => ['active' => 0, 'label' => 'Projekte'], 'tickets' => ['active' => 0, 'label' => 'Tickets'],]);
        $perPage = $this->get('app.helper')->setTablePerPage('mod_contact_table_rows');
        $paginator = $this->get('knp_paginator');
        $page = $request->query->getInt('page', 1);
        $pagination = $paginator->paginate($tickets, $page, $perPage);

        return $this->render('ModulesTicketBundle:Ticket:index.html.twig', ['pagination' => $pagination, 'columns' => $columns, 'perPage' => $perPage]);
    }

    /**
     * Creates a new Ticket entity.
     * @Route("/new")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $ticket = new Ticket();
        $form = $this->createForm(TicketType::class, $ticket);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($ticket);
            $em->flush();

            $this->addFlash('success', '<i class="uk-icon-check"></i> Das Ticket wurde erfolgreich angelegt!');

            return $this->redirectToRoute('modules_ticket_ticket_show', ['id' => $ticket->getId()]);
        }

        return $this->render('ModulesTicketBundle:Ticket:new.html.twig', ['ticket' => $ticket, 'form' => $form->createView(),]);
    }

    /**
     * Finds and displays a Ticket entity.
     * @Route("/{id}")
     * @Method("GET")
     */
    public function showAction(Ticket $ticket)
    {
        $deleteForm = $this->createDeleteForm($ticket);

        return $this->render('ModulesTicketBundle:Ticket:show.html.twig', ['ticket' => $ticket, 'delete_form' => $deleteForm->createView(),]);
    }

    /**
     * Creates a form to delete a Ticket entity.
     * @param Ticket $ticket The Ticket entity
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Ticket $ticket)
    {
        return $this->createFormBuilder()->setAction($this->generateUrl('modules_ticket_ticket_delete', ['id' => $ticket->getId()]))->setMethod('DELETE')->getForm();
    }

    /**
     * Displays a form to edit an existing Ticket entity.
     * @Route("/{id}/edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Ticket $ticket)
    {
        // get original notes
        $originalNotes = new ArrayCollection();
        foreach ($ticket->getNotes() as $note) {
            $originalNotes->add($note);
        }

        $deleteForm = $this->createDeleteForm($ticket);
        $editForm = $this->createForm('Modules\TicketBundle\Form\TicketType', $ticket);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();

            // remove addresses not submitted in the form
            foreach ($originalNotes as $note) {
                if (false === $ticket->getNotes()->contains($note)) {
                    $em->remove($note);
                }
            }

            $em->persist($ticket);
            $em->flush();

            $this->addFlash('success', '<i class="uk-icon-check"></i> Das Ticket wurde erfolgreich aktualisiert!');

            return $this->redirectToRoute('modules_ticket_ticket_show', ['id' => $ticket->getId()]);
        }

        return $this->render('ModulesTicketBundle:Ticket:edit.html.twig', ['ticket' => $ticket, 'edit_form' => $editForm->createView(), 'delete_form' => $deleteForm->createView(),]);
    }

    /**
     * Deletes a Ticket entity.
     * @Route("/{id}")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Ticket $ticket)
    {
        $form = $this->createDeleteForm($ticket);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($ticket);
            $em->flush();
        }

        return $this->redirectToRoute('modules_ticket_ticket_index');
    }

    /**
     * @Route("/document/upload/{ticket}")
     * @Method("POST")
     * @param Request $request
     * @param Ticket $ticket
     * @return JsonResponse
     */
    public function documentUploadAction(Request $request, Ticket $ticket)
    {
        $em = $this->getDoctrine()->getManager();
        $uploadManager = $this->get('stof_doctrine_extensions.uploadable.manager');

        $uploadedDocuments = [];
        foreach ($request->files->get('files', []) as $file) {
            $document = new Document();
            $document->setTicket($ticket);
            $em->persist($document);
            $uploadManager->markEntityToUpload($document, $file);
            $em->flush();

            $uploadedDocuments[] = ['id' => $document->getID(), 'name' => $document->getName(), 'size' => $document->getSize(), 'type' => $document->getMime(), 'delete_url' => $this->generateUrl('modules_ticket_ticket_documentdelete', ['document' => $document->getId()])];
        }

        return new JsonResponse($uploadedDocuments);
    }

    /**
     * @Route("/document/delete/{document}")
     * @Method("DELETE")
     * @param Document $document
     * @return Response
     */
    public function documentDeleteAction(Document $document)
    {
        $em = $this->getDoctrine()->getManager();

        $em->remove($document);
        $em->flush();

        return new Response('', 204);
    }

    /**
     * @Route("/document/row/{document}", options={"expose"=true})
     * @param Document $document
     * @return Response
     */
    public function documentRowAction(Document $document)
    {
        return $this->render('ModulesTicketBundle:Document:row.html.twig', ['document' => $document,]);
    }
}
