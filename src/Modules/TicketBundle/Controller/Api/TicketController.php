<?php

namespace Modules\TicketBundle\Controller\Api;

use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use Modules\TicketBundle\Entity\Ticket;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TicketController extends FOSRestController
{
    /**
     * @View
     * @ApiDoc(
     *   section="Tickets",
     *   resource=true,
     *   description="Updates the status of a ticket resource.",
     *   statusCodes={
     *     204 = "Returned if successful",
     *     400 = "Returned if validation fails"
     *   }
     * )
     * @param Request $request
     * @param Ticket $ticket
     * @return \Symfony\Component\Form\Form|Response
     */
    public function statusTicketAction(Request $request, Ticket $ticket)
    {
        $status = $request->request->get('status', $ticket->getStatus());

        $ticket->setStatus($status);

        $this->getDoctrine()->getManager()->flush();

        return;
    }
}