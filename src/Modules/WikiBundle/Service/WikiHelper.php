<?php
/**
 * Created by PhpStorm.
 * User: mkt
 * Date: 31.12.16
 * Time: 04:09
 */

namespace Modules\WikiBundle\Service;


use Core\AppBundle\Entity\User;
use Core\AppBundle\Entity\UserGroup;
use Doctrine\ORM\EntityManager;
use Modules\WikiBundle\Entity\Article;
use Modules\WikiBundle\Entity\Category;
use Symfony\Component\HttpFoundation\RequestStack;

class WikiHelper
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var RequestStack
     */
    private $requestStack;

    public function __construct(EntityManager $em, RequestStack $requestStack)
    {
        $this->em = $em;
        $this->requestStack = $requestStack;
        $this->request = $this->requestStack->getCurrentRequest();
    }

    /**
     * @return Category|null
     */
    public function getCurrentCategory()
    {
        if ($this->request && $this->request->attributes->has('_route') && strpos($this->request->attributes->get('_route'), 'modules_wiki') !== false && $this->request->attributes->has('category') && $this->request->attributes->get('category') instanceof Category) {
            return $this->request->attributes->get('category');
        }

        return null;
    }

    public function getArticleCountForCategory(Category $category)
    {
        $articles = $this->em->getRepository('ModulesWikiBundle:Article')->findByCategory($category);

        return count($articles);
    }

    /**
     * @param Article $article
     * @param User $user
     * @return bool
     */
    public function checkArticleGroup(Article $article, User $user)
    {
        $result = false;
        $groupsUser = [];
        foreach ($user->getGroups() as $group) {
            $groupsUser[] = $group->getId();
        }

        if ($article->getGroups() && !$user->hasRole('ROLE_ADMIN') && !$user->hasRole('ROLE_SUPER_ADMIN')) {

            $categoryGroupAcl = false;
            $categoryGroupIsSet = false;
            if (!$article->getCategories()->isEmpty()) {
                /**
                 * @var Category $category
                 */
                foreach ($article->getCategories() as $category) {

                    if (!$category->getGroups()->isEmpty()) {
                        $categoryGroupIsSet = true;
                        /**
                         * @var UserGroup $group
                         */
                        foreach ($category->getGroups() as $group) {
                            if (in_array($group->getId(), $groupsUser)) {
                                $categoryGroupAcl = true;
                            }
                        }
                    }
                }
            }

            if (!$article->getGroups()->isEmpty()) {
                /**
                 * @var UserGroup $group
                 */
                foreach ($article->getGroups() as $group) {

                    if (in_array($group->getId(), $groupsUser)) {
                        $result = true;
                        break;
                    }
                }

                if ($categoryGroupAcl)
                    $result = true;

            } else {

                if (!$categoryGroupIsSet || $categoryGroupAcl)
                    $result = true;
            }

        } else {
            $result = true;
        }

        return $result;
    }

    /**
     * @param      $articles
     * @param User $user
     * @return array
     */
    public function getArticlesByGroup($articles, User $user)
    {
        $result = [];
        $groupsUser = [];
        foreach ($user->getGroups() as $group) {
            $groupsUser[] = $group->getId();
        }

        /**
         * @var Article $article
         */
        foreach ($articles as $article) {

            if ($article->getGroups() && !$user->hasRole('ROLE_ADMIN') && !$user->hasRole('ROLE_SUPER_ADMIN')) {

                $categoryGroupAcl = false;
                $categoryGroupIsSet = false;
                if (!$article->getCategories()->isEmpty()) {
                    /**
                     * @var Category $category
                     */
                    foreach ($article->getCategories() as $category) {

                        if (!$category->getGroups()->isEmpty()) {
                            $categoryGroupIsSet = true;
                            /**
                             * @var UserGroup $group
                             */
                            foreach ($category->getGroups() as $group) {
                                if (in_array($group->getId(), $groupsUser)) {
                                    $categoryGroupAcl = true;
                                }
                            }
                        }
                    }
                }

                if (!$article->getGroups()->isEmpty()) {
                    /**
                     * @var UserGroup $group
                     */
                    foreach ($article->getGroups() as $group) {

                        if (in_array($group->getId(), $groupsUser)) {
                            $result[] = $article;
                            break;
                        }
                    }

                    if ($categoryGroupAcl)
                        $result[] = $article;

                } else {

                    if (!$categoryGroupIsSet || $categoryGroupAcl)
                        $result[] = $article;
                }

            } else {
                $result[] = $article;
            }
        }

        return $result;
    }
}