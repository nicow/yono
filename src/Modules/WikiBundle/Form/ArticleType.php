<?php

namespace Modules\WikiBundle\Form;

use Doctrine\ORM\EntityManager;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Modules\WikiBundle\Entity\Tag;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Validator\Constraints as Assert;

class ArticleType extends AbstractType
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var AuthorizationChecker
     */
    private $authorizationChecker;

    public function __construct(EntityManager $em, AuthorizationChecker $authorizationChecker)
    {
        $this->em = $em;
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', null, ['label' => 'Titel'])->add('text', CKEditorType::class, ['label' => 'Inhalt', 'constraints' => [new Assert\NotBlank(['message' => 'Geben Sie einen Inhalt ein.'])]])->add('categories', null, ['label' => 'Kategorien'])->add('tags', null, ['label' => 'Schlagworte'])->add('groups', null, ['label' => 'Gruppen']);

        if ($this->authorizationChecker->isGranted('VIEW', 'Modules\\ContactBundle\\Entity\\Contact')) {
            $builder->add('contact', null, ['label' => 'Kontakt']);
        }

        if ($this->authorizationChecker->isGranted('VIEW', 'Modules\\ProjectBundle\\Entity\\Project')) {
            $builder->add('project', null, ['label' => 'Projekt']);
        }

        // add new tags dynamically
        $em = $this->em;
        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) use ($em) {
            $article = $event->getData();
            if (array_key_exists('tags', $article) && is_array($article['tags'])) {
                foreach ($article['tags'] as $key => $tag) {
                    $tagEntity = $em->getRepository('ModulesWikiBundle:Tag')->find($tag);
                    if (!$tagEntity) {
                        $tagEntity = new Tag();
                        $tagEntity->setName($tag);
                        $em->persist($tagEntity);
                        $em->flush();
                        $article['tags'][$key] = $tagEntity->getId();
                    }
                }
                $event->setData($article);
            }
        });
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array('data_class' => 'Modules\WikiBundle\Entity\Article'));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'modules_wikibundle_article';
    }


}
