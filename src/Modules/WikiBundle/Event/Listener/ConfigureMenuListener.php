<?php
/**
 * Created by PhpStorm.
 * User: mkt
 * Date: 26.08.16
 * Time: 17:22
 */

namespace Modules\WikiBundle\Event\Listener;


use Core\AppBundle\Entity\User;
use Core\AppBundle\Event\ConfigureMenuEvent;
use Doctrine\ORM\EntityManager;
use Knp\Menu\FactoryInterface;
use Modules\WikiBundle\Entity\Category;
use Modules\WikiBundle\Service\WikiHelper;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class ConfigureMenuListener
{
    /**
     * @var User
     */
    protected $user;
    /**
     * @var EntityManager
     */
    private $em;
    /**
     * @var Router
     */
    private $router;
    /**
     * @var WikiHelper
     */
    private $wikiHelper;
    /**
     * @var RequestStack
     */
    private $requestStack;
    /**
     * @var array
     */
    private $order;

    /**
     * @var Category
     */
    private $currentCategory;

    public function __construct(EntityManager $em, Router $router, WikiHelper $wikiHelper, RequestStack $requestStack, TokenStorage $tokenStorage)
    {
        $this->em = $em;
        $token = $tokenStorage->getToken();
        if ($token) {
            $this->user = $token->getUser();
        } else {
            $this->user = null;
        }
        $this->router = $router;
        $this->wikiHelper = $wikiHelper;
        $this->requestStack = $requestStack;
        $this->request = $requestStack->getCurrentRequest();
    }

    public function onMenuConfigure(ConfigureMenuEvent $event)
    {
        $factory = $event->getFactory();
        $menu = $event->getMenu();

        $categories = $this->em->getRepository('ModulesWikiBundle:Category')->findByGroupMenu($this->user, true);

        if ($menu->getName() == 'main') {
            $wikiKey = 'Wiki';
            if ($menu[$wikiKey]) {
                $this->order = [];
                if ($menu[$wikiKey]->getChild('Übersicht')) {
                    $this->order[] = 'Übersicht';
                }

                $this->currentCategory = $this->wikiHelper->getCurrentCategory();

                // Add categories
                foreach ($categories as $category) {
                    $this->addChildren($factory, $category, $menu[$wikiKey]);
                    $this->order[] = $category->getName();
                }

                if ($menu[$wikiKey]->getChild('divider')) {
                    $this->order[] = 'divider';
                }

                if ($menu[$wikiKey]->getChild('Kategorien')) {
                    $this->order[] = 'Kategorien';
                }

                $menu[$wikiKey]->reorderChildren($this->order);

                // Handle "current" states
                if ($menu[$wikiKey]->getChild('Kategorien')) {
                    if (strpos($this->request->attributes->get('_route'), 'modules_wiki_category') !== false) {
                        $menu[$wikiKey]['Kategorien']->setCurrent(true);
                    }
                }
            }
        }
    }

    public function addChildren(FactoryInterface $factory, Category $category, $menu)
    {
        $options = [
            'route' => 'modules_wiki_article_categoryindex',
            'routeParameters' => ['category' => $category->getId()],
            'extras' => [
                'icon' => $category->getIcon()
            ]
        ];
        $item = $factory->createItem($category->getName(), $options);


        if ($category->getChildren()) {
            foreach ($category->getChildren() as $child) {
                $this->addChildren($factory, $child, $item);
            }
        }
        $menu->addChild($item);
    }
}