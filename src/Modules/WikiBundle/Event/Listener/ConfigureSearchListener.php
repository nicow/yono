<?php
/**
 * Created by PhpStorm.
 * User: mkt
 * Date: 07.01.17
 * Time: 16:27
 */

namespace Modules\WikiBundle\Event\Listener;


use Core\AppBundle\Service\Search;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;

class ConfigureSearchListener
{
    /**
     * @var Search
     */
    private $search;

    public function __construct(Search $search)
    {
        $this->search = $search;
    }

    public function onKernelController(FilterControllerEvent $event)
    {
        $route = $event->getRequest()->attributes->get('_route');

        if (strpos($route, 'modules_wiki_') === 0) {
            $this->search->overrideFilter(['modules_wiki_search_article',]);
        }
    }
}