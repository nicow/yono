<?php

namespace Modules\WikiBundle\Controller;

use Modules\WikiBundle\Entity\Article;
use Modules\WikiBundle\Entity\Category;
use Modules\WikiBundle\Entity\Document;
use Modules\WikiBundle\Form\ArticleType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Article controller.
 * @Route("/article")
 */
class ArticleController extends Controller
{
    /**
     * Lists all article entities.
     * @Route("/")
     * @Method({"GET", "POST"})
     * @Security("is_granted('VIEW', 'Modules\\WikiBundle\\Entity\\Article')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {

        $articles = $this->getDoctrine()->getManager()->getRepository('ModulesWikiBundle:Article')->findByGroup($this->get('mod.wiki.helper'), $this->getUser(), $request->query->get('sort', 'name'), $request->query->get('direction', 'ASC'));
        $columns = $this->get('app.helper')->setTableColumns('mod_wiki_article_table_columns', ['contact' => ['active' => 1, 'label' => 'Kontakt'], 'project' => ['active' => 0, 'label' => 'Projekt'], 'createdBy' => ['active' => 1, 'label' => 'Autor'], 'createdAt' => ['active' => 1, 'label' => 'erstellt am'], 'updatedAt' => ['active' => 0, 'label' => 'letzte Änderung'],]);
        $perPage = $this->get('app.helper')->setTablePerPage('mod_wiki_article_table_rows');
        $paginator = $this->get('knp_paginator');
        $page = $request->query->getInt('page', 1);
        $pagination = $paginator->paginate($articles, $page, $perPage);

        $deleteForms = [];
        foreach ($articles as $article) {
            $deleteForms[$article->getId()] = $this->createDeleteForm($article)->createView();
        }

        return $this->render('ModulesWikiBundle:Article:index.html.twig', ['pagination' => $pagination, 'columns' => $columns, 'perPage' => $perPage, 'deleteForms' => $deleteForms]);
    }

    /**
     * Creates a form to delete a article entity.
     * @param Article $article The article entity
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Article $article)
    {
        return $this->createFormBuilder()->setAction($this->generateUrl('modules_wiki_article_delete', ['id' => $article->getId()]))->setMethod('DELETE')->getForm();
    }

    /**
     * Lists all article entities by category.
     * @Route("/category/{category}")
     * @Method({"GET", "POST"})
     * @Security("is_granted('VIEW', 'Modules\\WikiBundle\\Entity\\Article')")
     * @param Request $request
     * @param Category $category
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function categoryIndexAction(Request $request, Category $category)
    {

        $articles = $this->getDoctrine()->getManager()->getRepository('ModulesWikiBundle:Article')->findByCategoryGroup($this->get('mod.wiki.helper'), $this->getUser(), $category, $request->query->get('sort', 'name'), $request->query->get('direction', 'ASC'));
        $columns = $this->get('app.helper')->setTableColumns('mod_wiki_article_table_columns', ['contact' => ['active' => 1, 'label' => 'Kontakt'], 'project' => ['active' => 0, 'label' => 'Projekt'], 'createdBy' => ['active' => 1, 'label' => 'Autor'], 'createdAt' => ['active' => 1, 'label' => 'erstellt am'], 'updatedAt' => ['active' => 0, 'label' => 'letzte Änderung'],]);
        $perPage = $this->get('app.helper')->setTablePerPage('mod_wiki_article_table_rows');
        $paginator = $this->get('knp_paginator');
        $page = $request->query->getInt('page', 1);
        $pagination = $paginator->paginate($articles, $page, $perPage);

        $deleteForms = [];
        foreach ($articles as $article) {
            $deleteForms[$article->getId()] = $this->createDeleteForm($article)->createView();
        }

        return $this->render('ModulesWikiBundle:Article:index_category.html.twig', ['category' => $category, 'pagination' => $pagination, 'columns' => $columns, 'perPage' => $perPage, 'deleteForms' => $deleteForms]);
    }

    /**
     * Creates a new article entity.
     * @Route("/new")
     * @Route("/category/{category}/new")
     * @Method({"GET", "POST"})
     * @Security("is_granted('CREATE', 'Modules\\WikiBundle\\Entity\\Article')")
     * @param Request $request
     * @param Category $category
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request, Category $category = null)
    {
        $article = new Article();
        if ($category) {
            $article->addCategory($category);
        }
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($article);
            $em->flush();

            $this->addFlash('success', '<i class="uk-icon-check"></i> Der Artikel wurde erfolgreich angelegt!');

            if ($category) {
                return $this->redirectToRoute('modules_wiki_article_show_1', ['id' => $article->getId(), 'category' => $category->getId()]);
            } else {
                return $this->redirectToRoute('modules_wiki_article_show', ['id' => $article->getId()]);
            }
        }

        return $this->render('ModulesWikiBundle:Article:new.html.twig', ['category' => $category, 'article' => $article, 'form' => $form->createView(),]);
    }

    /**
     * Finds and displays a article entity.
     * @Route("/{id}")
     * @Route("/category/{category}/{id}")
     * @Method("GET")
     * @Security("is_granted('VIEW', article)")
     * @param Article $article
     * @param Category $category
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Article $article, Category $category = null)
    {
        if (!$this->get('mod.wiki.helper')->checkArticleGroup($article, $this->getUser())) {
            $article = null;
            $deleteForm = null;
        } else {
            $deleteForm = $this->createDeleteForm($article)->createView();
        }


        return $this->render('ModulesWikiBundle:Article:show.html.twig', ['category' => $category, 'article' => $article, 'deleteForm' => $deleteForm]);
    }

    /**
     * Displays a form to edit an existing article entity.
     * @Route("/{id}/edit")
     * @Route("/category/{category}/{id}/edit")
     * @Method({"GET", "POST"})
     * @Security("is_granted('EDIT', article)")
     * @param Request $request
     * @param Article $article
     * @param Category $category
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @ParamConverter("id", class="Modules\WikiBundle\Entity\Article")
     */
    public function editAction(Request $request, Article $article, Category $category = null)
    {


        if (!$this->get('mod.wiki.helper')->checkArticleGroup($article, $this->getUser())) {
            $article = null;
            $deleteForm = null;
            $editForm = null;
        } else {
            $deleteForm = $this->createDeleteForm($article)->createView();
            $editForm = $this->createForm(ArticleType::class, $article);
            $editForm->handleRequest($request);

            if ($editForm->isSubmitted() && $editForm->isValid()) {
                $this->getDoctrine()->getManager()->flush();

                $this->addFlash('success', '<i class="uk-icon-check"></i> Der Artikel wurde erfolgreich aktualisiert!');

                if ($category) {
                    return $this->redirectToRoute('modules_wiki_article_edit_1', ['id' => $article->getId(), 'category' => $category->getId()]);
                }

                return $this->redirectToRoute('modules_wiki_article_show', ['id' => $article->getId()]);
            }

            $editForm = $editForm->createView();

        }

        return $this->render('ModulesWikiBundle:Article:edit.html.twig', ['category' => $category, 'article' => $article, 'edit_form' => $editForm, 'delete_form' => $deleteForm,]);
    }

    /**
     * Deletes a article entity.
     * @Route("/{id}")
     * @Method("DELETE")
     * @Security("is_granted('DELETE', article)")
     * @param Request $request
     * @param Article $article
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, Article $article)
    {
        $form = $this->createDeleteForm($article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($article);
            $em->flush();

            $this->addFlash('success', '<i class="uk-icon-check"></i> Der Artikel wurde gelöscht!');
        }

        return $this->redirectToRoute('modules_wiki_article_index');
    }

    /**
     * @Route("/document/upload/{article}")
     * @Method("POST")
     * @Security("is_granted('EDIT', article)")
     * @param Request $request
     * @param Article $article
     * @return JsonResponse
     */
    public function documentUploadAction(Request $request, Article $article)
    {
        $em = $this->getDoctrine()->getManager();
        $uploadManager = $this->get('stof_doctrine_extensions.uploadable.manager');

        $uploadedDocuments = [];
        foreach ($request->files->get('files', []) as $file) {
            $document = new Document();
            $document->setArticle($article);
            $em->persist($document);
            $uploadManager->markEntityToUpload($document, $file);
            $em->flush();

            $uploadedDocuments[] = ['id' => $document->getID(), 'name' => $document->getName(), 'size' => $document->getSize(), 'type' => $document->getMime(), 'delete_url' => $this->generateUrl('modules_wiki_article_documentdelete', ['document' => $document->getId()])];
        }

        return new JsonResponse($uploadedDocuments);
    }

    /**
     * @Route("/document/delete/{document}")
     * @Method("DELETE")
     * @Security("is_granted('EDIT', 'Modules\\WikiBundle\\Entity\\Article')")
     * @param Document $document
     * @return Response
     */
    public function documentDeleteAction(Document $document)
    {
        $em = $this->getDoctrine()->getManager();

        $em->remove($document);
        $em->flush();

        return new Response('', 204);
    }

    /**
     * @Route("/document/row/{document}", options={"expose"=true})
     * @Security("is_granted('VIEW', 'Modules\\WikiBundle\\Entity\\Article')")
     * @param Document $document
     * @return Response
     */
    public function documentRowAction(Document $document)
    {
        return $this->render('ModulesWikiBundle:Document:row.html.twig', ['document' => $document]);
    }
}
