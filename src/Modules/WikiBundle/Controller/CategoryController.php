<?php

namespace Modules\WikiBundle\Controller;

use Modules\WikiBundle\Entity\Article;
use Modules\WikiBundle\Entity\Category;
use Modules\WikiBundle\Form\CategoryType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\HttpFoundation\Request;

/**
 * Category controller.
 * @Route("/category")
 */
class CategoryController extends Controller
{
    /**
     * Lists all category entities.
     * @Route("/")
     * @Method("GET")
     * @Security("is_granted('VIEW', 'Modules\\WikiBundle\\Entity\\Category')")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $categories = $em->getRepository('ModulesWikiBundle:Category')->findByGroup($this->getUser());

        // get delete forms
        $deleteForms = [];
        foreach ($categories as $category) {
            $deleteForms[$category->getId()] = $this->createDeleteForm($category)->createView();
        }

        return $this->render('ModulesWikiBundle:Category:index.html.twig', ['categories' => $categories, 'deleteForms' => $deleteForms]);
    }

    /**
     * Creates a form to delete a category entity.
     * @param Category $category The category entity
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Category $category)
    {
        $otherCategories = $this->getDoctrine()->getManager()->getRepository('ModulesWikiBundle:Category')->getOtherCategories($category);
        $choices = ['nichts (nur die Kategorie entfernen)' => 'remove_category_only', 'Artikel auch löschen' => 'delete'];
        /** @var Category $otherCategory */
        foreach ($otherCategories as $otherCategory) {
            $choices['Kategorie "' . $otherCategory->getName() . '" zuweisen'] = $otherCategory->getId();
        }

        return $this->createFormBuilder()->add('articles_action', ChoiceType::class, ['choices' => $choices])->setAction($this->generateUrl('modules_wiki_category_delete', ['id' => $category->getId()]))->setMethod('DELETE')->getForm();
    }

    /**
     * Creates a new category entity.
     * @Route("/new")
     * @Method({"GET", "POST"})
     * @Security("is_granted('CREATE', 'Modules\\WikiBundle\\Entity\\Category')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $category = new Category();
        $category->setShowInMenu(true);
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();

            $this->addFlash('success', '<i class="uk-icon-check"></i> Die Kategorie wurde erfolgreich angelegt!');

            return $this->redirectToRoute('modules_wiki_category_index');
        }

        return $this->render('ModulesWikiBundle:Category:new.html.twig', ['category' => $category, 'form' => $form->createView(),]);
    }

    /**
     * Displays a form to edit an existing category entity.
     * @Route("/{id}/edit")
     * @Method({"GET", "POST"})
     * @Security("is_granted('EDIT', category)")
     * @param Request $request
     * @param Category $category
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @ParamConverter("id", class="Modules\WikiBundle\Entity\Category")
     */
    public function editAction(Request $request, Category $category)
    {
        $editForm = $this->createForm(CategoryType::class, $category);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', '<i class="uk-icon-check"></i> Die Kategorie wurde aktualisiert!');

            return $this->redirectToRoute('modules_wiki_category_index');
        }

        return $this->render('ModulesWikiBundle:Category:edit.html.twig', ['category' => $category, 'edit_form' => $editForm->createView(),]);
    }

    /**
     * Deletes a category entity.
     * @Route("/{id}")
     * @Method("DELETE")
     * @Security("is_granted('EDIT', category)")
     * @param Request $request
     * @param Category $category
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, Category $category)
    {
        $form = $this->createDeleteForm($category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $articlesAction = $request->request->get('form')['articles_action'];
            if ($articlesAction != 'remove_category_only') {
                // get all articles from category and category children
                $articles = $em->getRepository('ModulesWikiBundle:Article')->findByCategory($category);
                if ($articlesAction == 'delete') {
                    // delete them
                    foreach ($articles as $article) {
                        $em->remove($article);
                    }
                } elseif ((int)$articlesAction) {
                    // assign new category
                    $newCategory = $em->getRepository('ModulesWikiBundle:Category')->find((int)$articlesAction);
                    if ($newCategory) {
                        /** @var Article $article */
                        foreach ($articles as $article) {
                            if (!$article->getCategories()->contains($newCategory)) {
                                $article->addCategory($newCategory);
                            }
                        }
                    }
                }
            }

            $em->remove($category);
            $em->flush();

            $this->addFlash('success', '<i class="uk-icon-check"></i> Die Kategorie wurde gelöscht!');
        }

        return $this->redirectToRoute('modules_wiki_category_index');
    }
}
