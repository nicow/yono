<?php

namespace Modules\WikiBundle\Repository;

use Core\AppBundle\Entity\User;
use Core\AppBundle\Entity\UserGroup;
use Doctrine\ORM\EntityRepository;
use Modules\WikiBundle\Entity\Category;

/**
 * CategoryRepository
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class CategoryRepository extends EntityRepository
{
    public function getOtherCategories(Category $category)
    {
        return $this->createQueryBuilder('c')->where('c.id != :id')->setParameter('id', $category->getId())->getQuery()->getResult();
    }

    /**
     * @param User $user
     * @return array|null
     */
    public function findByGroup($user)
    {
        $categories = $this->createQueryBuilder('c')->getQuery()->getResult();

        $result = [];
        $groupsUser = [];
        foreach ($user->getGroups() as $group) {
            $groupsUser[] = $group->getId();
        }

        /**
         * @var Category $category
         */
        foreach ($categories as $category) {

            if ($category->getGroups() && !$user->hasRole('ROLE_ADMIN') && !$user->hasRole('ROLE_SUPER_ADMIN')) {
                $empty = true;
                /**
                 * @var UserGroup $group
                 */
                foreach ($category->getGroups() as $group) {
                    $empty = false;
                    if (in_array($group->getId(), $groupsUser)) {
                        $result[] = $category;
                        break;
                    }
                }
                if ($empty) {
                    $result[] = $category;
                }

            } else {
                $result[] = $category;
            }
        }

        return $result;
    }

    /**
     * @param User $user
     * @param      $showInMenu
     * @return array|null
     */
    public function findByGroupMenu($user, $showInMenu)
    {
        $categories = $this->createQueryBuilder('c')->where('c.showInMenu = :showInMenu')->andWhere('c.parent is NULL')->setParameter('showInMenu', $showInMenu)->getQuery()->getResult();

        $result = [];
        $groupsUser = [];
        foreach ($user->getGroups() as $group) {
            $groupsUser[] = $group->getId();
        }

        /**
         * @var Category $category
         */
        foreach ($categories as $category) {

            if ($category->getGroups() && !$user->hasRole('ROLE_ADMIN') && !$user->hasRole('ROLE_SUPER_ADMIN')) {
                $empty = true;
                /**
                 * @var UserGroup $group
                 */
                foreach ($category->getGroups() as $group) {
                    $empty = false;
                    if (in_array($group->getId(), $groupsUser)) {
                        $result[] = $category;
                        break;
                    }
                }
                if ($empty) {
                    $result[] = $category;
                }

            } else {
                $result[] = $category;
            }
        }

        return $result;
    }
}
