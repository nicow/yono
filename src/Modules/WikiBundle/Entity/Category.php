<?php

namespace Modules\WikiBundle\Entity;

use Core\AppBundle\Annotation\Acl;
use Core\AppBundle\Entity\BlameableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Category
 * @Acl("Kategorien")
 * @ORM\Table(name="mod_wiki_category")
 * @ORM\Entity(repositoryClass="Modules\WikiBundle\Repository\CategoryRepository")
 */
class Category
{
    use BlameableTrait;

    /**
     * @var Category[]
     * @ORM\OneToMany(targetEntity="Category", mappedBy="parent")
     */
    protected $children;
    /**
     * @ORM\ManyToMany(targetEntity="Core\AppBundle\Entity\UserGroup")
     * @ORM\JoinTable(name="mod_wiki_category_users_groups",
     *      joinColumns={@ORM\JoinColumn(name="category_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="group_id", referencedColumnName="id", onDelete="CASCADE")}
     *)
     */
    protected $groups;
    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=50, unique=true)
     */
    private $name;
    /**
     * @var string
     * @ORM\Column(name="icon", type="string", length=50, nullable=true)
     */
    private $icon;
    /**
     * @var boolean
     * @ORM\Column(name="show_in_menu", type="boolean", nullable=true)
     */
    private $showInMenu;
    /**
     * @var Category
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $parent;
    /**
     * @var Article[]
     * @ORM\ManyToMany(targetEntity="Modules\WikiBundle\Entity\Article", mappedBy="categories")
     */
    private $articles;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->children = new ArrayCollection();
        $this->articles = new ArrayCollection();
    }

    /**
     * Get id
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     * @param string $name
     * @return Category
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get icon
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * Set icon
     * @param string $icon
     * @return Category
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;

        return $this;
    }

    public function __toString()
    {
        return $this->name;
    }

    /**
     * Add child
     * @param Category $child
     * @return Category
     */
    public function addChild(Category $child)
    {
        $this->children[] = $child;

        return $this;
    }

    /**
     * Remove child
     * @param Category $child
     */
    public function removeChild(Category $child)
    {
        $this->children->removeElement($child);
    }

    /**
     * Get children
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Get parent
     * @return Category
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set parent
     * @param Category $parent
     * @return Category
     */
    public function setParent(Category $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get showInMenu
     * @return boolean
     */
    public function getShowInMenu()
    {
        return $this->showInMenu;
    }

    /**
     * Set showInMenu
     * @param boolean $showInMenu
     * @return Category
     */
    public function setShowInMenu($showInMenu)
    {
        $this->showInMenu = $showInMenu;

        return $this;
    }

    /**
     * Add article
     * @param Article $article
     * @return Category
     */
    public function addArticle(Article $article)
    {
        $this->articles[] = $article;

        return $this;
    }

    /**
     * Remove article
     * @param Article $article
     */
    public function removeArticle(Article $article)
    {
        $this->articles->removeElement($article);
    }

    /**
     * Get articles
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getArticles()
    {
        return $this->articles;
    }

    /**
     * @return mixed
     */
    public function getGroups()
    {
        return $this->groups;
    }

    /**
     * @param mixed $groups
     * @return Category
     */
    public function setGroups($groups)
    {
        $this->groups = $groups;

        return $this;
    }
}
