<?php

namespace Modules\WikiBundle\Entity;

use Core\AppBundle\Annotation\Acl;
use Core\AppBundle\Entity\BlameableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Modules\ContactBundle\Entity\Contact;
use Modules\ProjectBundle\Entity\Project;

/**
 * Article
 * @Acl("Artikel")
 * @ORM\Table(name="mod_wiki_article")
 * @ORM\Entity(repositoryClass="Modules\WikiBundle\Repository\ArticleRepository")
 */
class Article
{
    use BlameableTrait;

    /**
     * @ORM\ManyToMany(targetEntity="Core\AppBundle\Entity\UserGroup")
     * @ORM\JoinTable(name="mod_wiki_article_users_groups",
     *      joinColumns={@ORM\JoinColumn(name="article_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="group_id", referencedColumnName="id", onDelete="CASCADE")}
     *)
     */
    protected $groups;
    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;
    /**
     * @var string
     * @ORM\Column(name="text", type="text")
     */
    private $text;
    /**
     * @var Category[]
     * @ORM\ManyToMany(targetEntity="Modules\WikiBundle\Entity\Category", inversedBy="articles")
     * @ORM\JoinTable(name="mod_wiki_articles_categories")
     */
    private $categories;
    /**
     * @var Project
     * @ORM\ManyToOne(targetEntity="Modules\ProjectBundle\Entity\Project")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $project;
    /**
     * @var Contact
     * @ORM\ManyToOne(targetEntity="Modules\ContactBundle\Entity\Contact")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $contact;
    /**
     * @var Tag[]
     * @ORM\ManyToMany(targetEntity="Modules\WikiBundle\Entity\Tag", inversedBy="articles")
     * @ORM\JoinTable(name="mod_wiki_articles_tags")
     */
    private $tags;
    /**
     * @var Document[]
     * @ORM\OneToMany(targetEntity="Modules\WikiBundle\Entity\Document", mappedBy="article", cascade={"persist"})
     * @ORM\OrderBy({"createdAt" = "DESC"})
     */
    private $documents;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->categories = new ArrayCollection();
        $this->tags = new ArrayCollection();
        $this->documents = new ArrayCollection();
    }

    /**
     * Get id
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     * @param string $name
     * @return Article
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get text
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set text
     * @param string $text
     * @return Article
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Add category
     * @param Category $category
     * @return Article
     */
    public function addCategory(Category $category)
    {
        $this->categories[] = $category;

        return $this;
    }

    /**
     * Remove category
     * @param Category $category
     */
    public function removeCategory(Category $category)
    {
        $this->categories->removeElement($category);
    }

    /**
     * Get categories
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * Get project
     * @return Project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Set project
     * @param Project $project
     * @return Article
     */
    public function setProject(Project $project = null)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get contact
     * @return Contact
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set contact
     * @param Contact $contact
     * @return Article
     */
    public function setContact(Contact $contact = null)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Add tag
     * @param Tag $tag
     * @return Article
     */
    public function addTag(Tag $tag)
    {
        $this->tags[] = $tag;

        return $this;
    }

    /**
     * Remove tag
     * @param Tag $tag
     */
    public function removeTag(Tag $tag)
    {
        $this->tags->removeElement($tag);
    }

    /**
     * Get tags
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Add document
     * @param Document $document
     * @return Article
     */
    public function addDocument(Document $document)
    {
        $this->documents[] = $document;

        return $this;
    }

    /**
     * Remove document
     * @param Document $document
     */
    public function removeDocument(Document $document)
    {
        $this->documents->removeElement($document);
    }

    /**
     * Get documents
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDocuments()
    {
        return $this->documents;
    }

    /**
     * @return mixed
     */
    public function getGroups()
    {
        return $this->groups;
    }

    /**
     * @param mixed $groups
     * @return Article
     */
    public function setGroups($groups)
    {
        $this->groups = $groups;

        return $this;
    }
}
