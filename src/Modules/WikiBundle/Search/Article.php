<?php
/**
 * Created by PhpStorm.
 * User: mkt
 * Date: 04.01.17
 * Time: 20:54
 */

namespace Modules\WikiBundle\Search;

use Core\AppBundle\Search\SearchItem;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Twig\TwigEngine;
use Symfony\Bundle\FrameworkBundle\Routing\Router;

class Article extends SearchItem
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var TwigEngine
     */
    private $twig;

    /**
     * @var Router
     */
    private $router;

    public function __construct(EntityManager $em, TwigEngine $twig, Router $router)
    {
        $this->em = $em;
        $this->twig = $twig;
        $this->router = $router;
    }

    /**
     * @param $query
     * @return array
     */
    public function getResults($query)
    {
        $articles = $this->em->getRepository('ModulesWikiBundle:Article')->createQueryBuilder('a')->leftJoin('a.tags', 't')->where('a.name LIKE :query')->orWhere('a.text LIKE :query')->orWhere('t.name LIKE :query')->setParameter('query', '%' . $query . '%')->getQuery()->getResult();

        $results = [];
        if ($articles) {
            foreach ($articles as $article) {
                $results[] = $this->twig->render('@ModulesWiki/Search/article.html.twig', ['article' => $article]);
            }
        }

        return $results;
    }

    /**
     * @param $query
     * @return array
     */
    public function getAutocompleteResults($query)
    {
        $articles = $this->em->getRepository('ModulesWikiBundle:Article')->createQueryBuilder('a')->leftJoin('a.tags', 't')->where('a.name LIKE :query')->orWhere('a.text LIKE :query')->orWhere('t.name LIKE :query')->setParameter('query', '%' . $query . '%')->getQuery()->getResult();

        $results = [];
        if ($articles) {
            /** @var \Modules\WikiBundle\Entity\Article $article */
            foreach ($articles as $article) {
                $results[] = ['icon' => 'wikipedia-w', 'title' => $article->getName(), 'url' => $this->router->generate('modules_wiki_article_show', ['id' => $article->getId()]), 'text' => strip_tags($article->getText())];
            }
        }

        return $results;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'Artikel';
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'wikipedia-w';
    }

    public function getRequiredPermissions()
    {
        return ['VIEW' => 'Modules\\WikiBundle\\Entity\\Article'];
    }
}