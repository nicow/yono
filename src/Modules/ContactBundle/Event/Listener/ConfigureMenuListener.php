<?php
/**
 * Created by PhpStorm.
 * User: mkt
 * Date: 26.08.16
 * Time: 17:22
 */

namespace Modules\ContactBundle\Event\Listener;


use Core\AppBundle\Event\ConfigureMenuEvent;
use Doctrine\ORM\EntityManager;
use Knp\Menu\FactoryInterface;
use Modules\ContactBundle\Entity\Category;
use Modules\ContactBundle\Service\ContactHelper;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\HttpFoundation\RequestStack;

class ConfigureMenuListener
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var Router
     */
    private $router;
    /**
     * @var ContactHelper
     */
    private $contactHelper;
    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var array
     */
    private $order;

    /**
     * @var Category
     */
    private $currentCategory;

    public function __construct(EntityManager $em, Router $router, ContactHelper $contactHelper, RequestStack $requestStack)
    {
        $this->em = $em;
        $this->router = $router;
        $this->contactHelper = $contactHelper;
        $this->requestStack = $requestStack;
        $this->request = $requestStack->getCurrentRequest();
    }

    public function onMenuConfigure(ConfigureMenuEvent $event)
    {
        $factory = $event->getFactory();
        $menu = $event->getMenu();

        $categories = $this->em->getRepository('ModulesContactBundle:Category')->findBy(['showInMenu' => true, 'parent' => null]);

        if ($menu->getName() == 'main') {
            $contactsKey = 'Kontakte';
            if ($menu[$contactsKey]) {
                $this->order = [];
                if ($menu[$contactsKey]->getChild('Übersicht')) {
                    $this->order[] = 'Übersicht';
                }

                $this->currentCategory = $this->contactHelper->getCurrentCategory();

                // Add categories
                foreach ($categories as $category) {
                    $this->addChildren($factory, $category, $menu[$contactsKey]);
                    $this->order[] = $category->getName();
                }

                if ($menu[$contactsKey]->getChild('divider')) {
                    $this->order[] = 'divider';
                }
                if ($menu[$contactsKey]->getChild('Kategorien')) {
                    $this->order[] = 'Kategorien';
                }
                if ($menu[$contactsKey]->getChild('Formularfelder')) {
                    $this->order[] = 'Formularfelder';
                }

                $menu[$contactsKey]->reorderChildren($this->order);

                // Handle "current" states
                if ($menu[$contactsKey]->getChild('Kategorien')) {
                    if (strpos($this->request->attributes->get('_route'), 'modules_contact_category') !== false) {
                        $menu[$contactsKey]['Kategorien']->setCurrent(true);
                    }
                }
                if ($menu[$contactsKey]->getChild('Formularfelder')) {
                    if (strpos($this->request->attributes->get('_route'), 'modules_contact_field') !== false) {
                        $menu[$contactsKey]['Formularfelder']->setCurrent(true);
                    }
                }
            }
        }
    }

    public function addChildren(FactoryInterface $factory, Category $category, $menu)
    {
        $options = ['route' => 'modules_contact_default_categoryindex', 'routeParameters' => ['category' => $category->getId()], 'extras' => ['icon' => $category->getIcon()]];
        $item = $factory->createItem($category->getName(), $options);

        try {
            if ($this->currentCategory === $category) {
                $item->setCurrent(true);
            }
        } catch (\Exception $ex) {
            //@TODO check this error out: KM-HAUSTECHNIK-K
        }

        if ($category->getChildren()) {
            foreach ($category->getChildren() as $child) {
                $this->addChildren($factory, $child, $item);
            }
        }
        $menu->addChild($item);
    }
}