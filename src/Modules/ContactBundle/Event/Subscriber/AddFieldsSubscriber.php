<?php
/**
 * Created by PhpStorm.
 * User: mkt
 * Date: 30.12.16
 * Time: 18:23
 */

namespace Modules\ContactBundle\Event\Subscriber;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Modules\ContactBundle\Entity\Category;
use Modules\ContactBundle\Entity\ContactRelatedEntityInterface;
use Modules\ContactBundle\Entity\Field;
use Modules\ContactBundle\Entity\FieldOption;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class AddFieldsSubscriber implements EventSubscriberInterface
{
    /**
     * @var EntityManager
     */
    private $em;
    /**
     * @var
     */
    private $form;
    /**
     * @var Category
     */
    private $category;

    /**
     * @param               $form
     * @param EntityManager $em
     * @param Category $category
     */
    public function __construct($form, EntityManager $em, Category $category = null)
    {
        $this->em = $em;
        $this->form = $form;
        $this->category = $category;
    }

    public static function getSubscribedEvents()
    {
        return [FormEvents::PRE_SET_DATA => 'preSetData'];
    }

    /**
     * @param FormEvent $event
     */
    public function preSetData(FormEvent $event)
    {
        /** @var ContactRelatedEntityInterface $entity */
        $entity = $event->getData();
        $form = $event->getForm();

        if ($entity && $contact = $entity->getContact()) {
            $fields = $this->em->getRepository('ModulesContactBundle:Field')->findByFormAndContact($this->form, $contact);
        } elseif ($this->category) {
            $fields = $this->em->getRepository('ModulesContactBundle:Field')->findByFormAndCategory($this->form, $this->category);
        } else {
            $fields = $this->em->getRepository('ModulesContactBundle:Field')->findByFormWithNoCategories($this->form);
        }

        if ($fields->count()) {
            /** @var Field $field */
            foreach ($fields as $field) {
                // field settings applying to all fields
                $options = ['label' => $field->getName(), 'required' => $field->getRequired(), 'mapped' => false,];

                // get values from database
                $values = [];
                if ($entity instanceof ContactRelatedEntityInterface) {
                    $values = $this->em->getRepository('ModulesContactBundle:FieldValue')->findByFieldAndEntity($field, $entity);
                }

                // type specific field settings
                switch ($field->getType()) {
                    default:
                    case 'text':
                        $type = TextType::class;
                        if ($values) {
                            $options['data'] = $values[0]->getValueText();
                        }
                        break;
                    case 'textarea':
                        $type = TextareaType::class;
                        if ($values) {
                            $options['data'] = $values[0]->getValueText();
                        }
                        break;
                    case 'select_single':
                        $type = EntityType::class;
                        $options['class'] = FieldOption::class;
                        $options['choices'] = $field->getFieldOptions();
                        if ($values) {
                            $options['data'] = $values[0]->getValueOption();
                        }
                        break;
                    case 'select_multiple':
                        $type = EntityType::class;
                        $options['class'] = FieldOption::class;
                        $options['choices'] = $field->getFieldOptions();
                        $options['multiple'] = true;

                        if ($values) {
                            $options['data'] = new ArrayCollection();
                            foreach ($values as $value) {
                                $options['data']->add($value->getValueOption());
                            }
                        }
                        break;
                    case 'checkbox':
                        $type = CheckboxType::class;
                        if ($values) {
                            $options['data'] = $values[0]->getValueBool();
                        }
                        break;
                }

                // add the field
                $form->add('_field_entity_' . $field->getId(), $type, $options);
            }
        }
    }
}