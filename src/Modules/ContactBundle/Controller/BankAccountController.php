<?php

namespace Modules\ContactBundle\Controller;

use Modules\ContactBundle\Entity\BankAccount;
use Modules\ContactBundle\Entity\Category;
use Modules\ContactBundle\Entity\Contact;
use Modules\ContactBundle\Form\BankAccountType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Person controller.
 * @Route("/bankaccount")
 */
class BankAccountController extends Controller
{
    /**
     * Creates a new bankaccount entity.
     * @Route("/new/{contact}")
     * @Route("/category/{category}/new/{contact}")
     * @Method({"GET", "POST"})
     * @Security("is_granted('EDIT', 'Modules\\ContactBundle\\Entity\\Contact') or (contact and is_granted('EDIT', contact))")
     * @param Request $request
     * @param Contact $contact
     * @param Category $category
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request, Contact $contact = null, Category $category = null)
    {
        $bankAccount = new BankAccount();
        if ($contact) {
            $bankAccount->setContact($contact);
        }
        $form = $this->createForm(BankAccountType::class, $bankAccount, ['contact' => $contact ? 0 : 1, 'action' => $category ? $this->generateUrl('modules_contact_bankaccount_new_1', ['contact' => $bankAccount->getContact()->getId(), 'category' => $category->getId()]) : $this->generateUrl('modules_contact_bankaccount_new', ['contact' => $bankAccount->getContact() ? $bankAccount->getContact()->getId() : null])]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($bankAccount);
            $em->flush();

            $this->get('mod.contact.helper')->saveFields($form);

            $this->addFlash('success', '<i class="uk-icon-check"></i> Die Kontoverbindung wurde erfolgreich angelegt!');

            if ($category) {
                return new JsonResponse(['redirect' => $this->generateUrl('modules_contact_default_detailsbankaccounts_1', ['contact' => $bankAccount->getContact()->getId(), 'category' => $category->getId()])]);
            } else {
                return new JsonResponse(['redirect' => $this->generateUrl('modules_contact_default_detailsbankaccounts', ['contact' => $bankAccount->getContact()->getId()])]);
            }
        }

        return $this->render('ModulesContactBundle:BankAccount:new.html.twig', ['currentCategory' => $category, 'bankAccount' => $bankAccount, 'form' => $form->createView(),]);
    }
}