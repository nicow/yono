<?php

namespace Modules\ContactBundle\Controller;

use Modules\ContactBundle\Entity\Category;
use Modules\ContactBundle\Entity\Contact;
use Modules\ContactBundle\Form\CategoryType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\HttpFoundation\Request;

/**
 * Category controller.
 * @Route("/category")
 */
class CategoryController extends Controller
{
    /**
     * Lists all category entities.
     * @Route("/")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $categories = $em->getRepository('ModulesContactBundle:Category')->findAll();

        // get delete forms
        $deleteForms = [];
        foreach ($categories as $category) {
            $deleteForms[$category->getId()] = $this->createDeleteForm($category)->createView();
        }

        return $this->render('ModulesContactBundle:Category:index.html.twig', ['categories' => $categories, 'deleteForms' => $deleteForms]);
    }

    /**
     * Creates a form to delete a category entity.
     * @param Category $category The category entity
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Category $category)
    {
        $otherCategories = $this->getDoctrine()->getManager()->getRepository('ModulesContactBundle:Category')->getOtherCategories($category);
        $choices = ['nichts (nur die Kategorie entfernen)' => 'remove_category_only', 'Kontakte auch löschen' => 'delete'];
        /** @var Category $otherCategory */
        foreach ($otherCategories as $otherCategory) {
            $choices['Kategorie "' . $otherCategory->getName() . '" zuweisen'] = $otherCategory->getId();
        }

        return $this->createFormBuilder()->add('contacts_action', ChoiceType::class, ['choices' => $choices])->setAction($this->generateUrl('modules_contact_category_delete', ['id' => $category->getId()]))->setMethod('DELETE')->getForm();
    }

    /**
     * Creates a new category entity.
     * @Route("/new")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $category = new Category();
        $category->setShowInMenu(true);
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();

            $this->addFlash('success', '<i class="uk-icon-check"></i> Kategorie angelegt!');

            return $this->redirectToRoute('modules_contact_category_index');
        }

        return $this->render('ModulesContactBundle:Category:new.html.twig', ['category' => $category, 'form' => $form->createView(),]);
    }

    /**
     * Displays a form to edit an existing category entity.
     * @Route("/{id}/edit")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param Category $category
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @ParamConverter("id", class="Modules\ContactBundle\Entity\Category")
     */
    public function editAction(Request $request, Category $category)
    {
        $editForm = $this->createForm(CategoryType::class, $category);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('modules_contact_category_index');
        }

        return $this->render('ModulesContactBundle:Category:edit.html.twig', ['category' => $category, 'edit_form' => $editForm->createView(),]);
    }

    /**
     * Deletes a category entity.
     * @Route("/{id}")
     * @Method("DELETE")
     * @param Request $request
     * @param Category $category
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, Category $category)
    {
        $form = $this->createDeleteForm($category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $contactsAction = $request->request->get('form')['contacts_action'];
            if ($contactsAction != 'remove_category_only') {
                // get all contacts from category and category children
                $contacts = $em->getRepository('ModulesContactBundle:Contact')->findByCategory($category);
                if ($contactsAction == 'delete') {
                    // delete them
                    foreach ($contacts as $contact) {
                        $em->remove($contact);
                    }
                } elseif ((int)$contactsAction) {
                    // assign new category
                    $newCategory = $em->getRepository('ModulesContactBundle:Category')->find((int)$contactsAction);
                    if ($newCategory) {
                        /** @var Contact $contact */
                        foreach ($contacts as $contact) {
                            if (!$contact->getCategories()->contains($newCategory)) {
                                $contact->addCategory($newCategory);
                            }
                        }
                    }
                }
            }

            $em->remove($category);
            $em->flush();
        }

        return $this->redirectToRoute('modules_contact_category_index');
    }
}
