<?php

namespace Modules\ContactBundle\Controller;

use Modules\ContactBundle\Entity\Address;
use Modules\ContactBundle\Entity\Category;
use Modules\ContactBundle\Entity\Contact;
use Modules\ContactBundle\Form\AddressType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Address controller.
 * @Route("/address")
 */
class AddressController extends Controller
{
    /**
     * Creates a new address entity.
     * @Route("/new/{contact}")
     * @Route("/category/{category}/new/{contact}")
     * @Method({"GET", "POST"})
     * @Security("is_granted('EDIT', 'Modules\\ContactBundle\\Entity\\Contact') or (contact and is_granted('EDIT', contact))")
     * @param Request $request
     * @param Contact $contact
     * @param Category $category
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request, Contact $contact = null, Category $category = null)
    {
        $address = new Address();
        if ($contact) {
            $address->setContact($contact);
        }
        $form = $this->createForm(AddressType::class, $address, ['contact' => $contact ? 0 : 1, 'action' => $category ? $this->generateUrl('modules_contact_address_new_1', ['contact' => $address->getContact()->getId(), 'category' => $category->getId()]) : $this->generateUrl('modules_contact_address_new', ['contact' => $address->getContact() ? $address->getContact()->getId() : null])]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($address);
            $em->flush();

            $this->get('mod.contact.helper')->saveFields($form);

            $this->addFlash('success', '<i class="uk-icon-check"></i> Die Adresse wurde erfolgreich angelegt!');

            if ($category) {
                return new JsonResponse(['redirect' => $this->generateUrl('modules_contact_default_detailsaddresses_1', ['contact' => $address->getContact()->getId(), 'category' => $category->getId()])]);
            } else {
                return new JsonResponse(['redirect' => $this->generateUrl('modules_contact_default_detailsaddresses', ['contact' => $address->getContact()->getId()])]);
            }
        }

        return $this->render('ModulesContactBundle:Address:new.html.twig', ['currentCategory' => $category, 'address' => $address, 'form' => $form->createView(),]);
    }
}