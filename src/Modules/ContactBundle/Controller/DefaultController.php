<?php

namespace Modules\ContactBundle\Controller;

use Core\AppBundle\Annotation\MenuItem;
use Doctrine\Common\Collections\ArrayCollection;
use Modules\ContactBundle\Entity\Category;
use Modules\ContactBundle\Entity\Contact;
use Modules\ContactBundle\Entity\Field;
use Modules\ContactBundle\Form\ContactType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/")
     * @MenuItem("Kontakte")
     * @Security("is_granted('VIEW', 'Modules\\ContactBundle\\Entity\\Contact')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $fields = $this->getDoctrine()->getManager()->getRepository('ModulesContactBundle:Field')->findByFormWithNoCategories('contact');
        $contacts = $this->getDoctrine()->getManager()->getRepository('ModulesContactBundle:Contact')->findAllOrdered($request->query->get('sort', 'name'), $request->query->get('direction', 'ASC'));
        $columnDefaults = ['addresses' => ['active' => 1, 'label' => 'Adressen'], 'persons' => ['active' => 1, 'label' => 'Ansprechpartner'], 'email' => ['active' => 1, 'label' => 'E-Mail'], 'phone' => ['active' => 1, 'label' => 'Telefon'], 'mobile' => ['active' => 1, 'label' => 'Handy'], 'bank_accounts' => ['active' => 0, 'label' => 'Kontoverbindungen'], 'projects' => ['active' => 0, 'label' => 'Projekte'], 'tickets' => ['active' => 0, 'label' => 'Tickets'],];
        /** @var Field $field */
        foreach ($fields as $field) {
            $columnDefaults['_field_' . $field->getName()] = ['active' => 1, 'label' => $field->getName()];
        }
        $columns = $this->get('app.helper')->setTableColumns('mod_contact_table_columns', $columnDefaults);

        $perPage = $this->get('app.helper')->setTablePerPage('mod_contact_table_rows');
        $paginator = $this->get('knp_paginator');
        $page = $request->query->getInt('page', 1);
        $pagination = $paginator->paginate($contacts, $page, $perPage);


        $personFields = $this->getDoctrine()->getManager()->getRepository('ModulesContactBundle:Field')->findByFormWithNoCategories('person');
        $bankAccountFields = $this->getDoctrine()->getManager()->getRepository('ModulesContactBundle:Field')->findByFormWithNoCategories('bank_account');

        return $this->render('ModulesContactBundle:Default:index.html.twig', ['pagination' => $pagination, 'fields' => $fields, 'columns' => $columns, 'perPage' => $perPage, 'personFields' => $personFields, 'bankAccountFields' => $bankAccountFields]);
    }

    /**
     * @Route("/category/{category}")
     * @MenuItem("Kontakte")
     * @Security("is_granted('VIEW', 'Modules\\ContactBundle\\Entity\\Contact')")
     * @param Request $request
     * @param Category $category
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function categoryIndexAction(Request $request, Category $category)
    {
        $fields = $this->getDoctrine()->getManager()->getRepository('ModulesContactBundle:Field')->findByFormAndCategory('contact', $category);
        $contacts = $this->getDoctrine()->getManager()->getRepository('ModulesContactBundle:Contact')->findByCategory($category, $request->query->get('sort', 'name'), $request->query->get('direction', 'ASC'));

        $columnDefaults = ['addresses' => ['active' => 1, 'label' => 'Adressen'], 'persons' => ['active' => 1, 'label' => 'Ansprechpartner'], 'email' => ['active' => 1, 'label' => 'E-Mail'], 'phone' => ['active' => 1, 'label' => 'Telefon'], 'mobile' => ['active' => 1, 'label' => 'Handy'], 'bank_accounts' => ['active' => 1, 'label' => 'Kontoverbindungen'], 'projects' => ['active' => 0, 'label' => 'Projekte'], 'tickets' => ['active' => 0, 'label' => 'Tickets'],];
        /** @var Field $field */
        foreach ($fields as $field) {
            $columnDefaults['_field_' . $field->getName()] = ['active' => 1, 'label' => $field->getName()];
        }
        $columns = $this->get('app.helper')->setTableColumns('mod_contact_table_columns', $columnDefaults);

        $perPage = $this->get('app.helper')->setTablePerPage('mod_contact_table_rows');
        $paginator = $this->get('knp_paginator');
        $page = $request->query->getInt('page', 1);
        $pagination = $paginator->paginate($contacts, $page, $perPage);

        $deleteForms = [];
        foreach ($contacts as $contact) {
            // $deleteForms[$contact->getId()] = $this->createDeleteForm($contact)->createView();
        }

        $personFields = $this->getDoctrine()->getManager()->getRepository('ModulesContactBundle:Field')->findByFormAndCategory('person', $category);
        $bankAccountFields = $this->getDoctrine()->getManager()->getRepository('ModulesContactBundle:Field')->findByFormAndCategory('bank_account', $category);

        return $this->render('ModulesContactBundle:Default:index_category.html.twig', ['category' => $category, 'fields' => $fields, 'pagination' => $pagination, 'columns' => $columns, 'perPage' => $perPage, 'deleteForms' => $deleteForms, 'personFields' => $personFields, 'bankAccountFields' => $bankAccountFields]);
    }

    /**
     * @Route("/{contact}/details")
     * @Route("/category/{category}/{contact}/details")
     * @Security("is_granted('VIEW', 'Modules\\ContactBundle\\Entity\\Contact')")
     * @param Contact $contact
     * @param Category $category
     * @return RedirectResponse
     */
    public function detailsAction(Contact $contact, Category $category = null)
    {
        if ($category) {
            return new RedirectResponse($this->generateUrl('modules_contact_default_detailsaddresses_1', ['contact' => $contact->getId(), 'category' => $category->getId()]));
        }

        return new RedirectResponse($this->generateUrl('modules_contact_default_detailsaddresses', ['contact' => $contact->getId()]));
    }


    /**
     * @Route("/{contact}/details/invoices")
     * @Route("/category/{category}/{contact}/details/invoices")
     * @Security("is_granted('VIEW', 'Modules\\OrderManagementBundle\\Entity\\Invoice')")
     * @param Request $request
     * @param Contact $contact
     * @param Category $category
     * @return \Symfony\Component\HttpFoundation\Response
     */

    public function detailsInvoiceAction(Request $request, Contact $contact, Category $category = null)
    {
        $helper = $this->get('mod.contact.helper');

        $contactFields = $this->getDoctrine()->getManager()->getRepository('ModulesContactBundle:Field')->findByFormWithNoCategories('contact');

        $invoices = $helper->getEntitiesForContact($contact, 'Modules\\OrderManagementBundle\\Entity\\Invoice');

        $perPage = 10;

        $paginator = $this->get('knp_paginator');

        $page = $request->query->getInt('page', 1);

        $pagination = $paginator->paginate($invoices, $page, $perPage);

        return $this->render('ModulesContactBundle:Default/Invoice:index.html.twig', [
            'pagination' => $pagination,
            'contact' => $contact,
            'currentCategory' => $category,
            'contactFields' => $contactFields
        ]);
    }

    /**
     * @Route("/{contact}/details/order")
     * @Route("/category/{category}/{contact}/details/order")
     * @Security("is_granted('VIEW', 'Modules\\OrderManagementBundle\\Entity\\Orders')")
     * @param Request $request
     * @param Contact $contact
     * @param Category $category
     * @return \Symfony\Component\HttpFoundation\Response
     */

    public function detailsOrderAction(Request $request, Contact $contact, Category $category = null)
    {
        $helper = $this->get('mod.contact.helper');

        $contactFields = $this->getDoctrine()->getManager()->getRepository('ModulesContactBundle:Field')->findByFormWithNoCategories('contact');

        $orders = $helper->getEntitiesForContact($contact, 'Modules\\OrderManagementBundle\\Entity\\Orders');

        $perPage = 10;

        $paginator = $this->get('knp_paginator');

        $page = $request->query->getInt('page', 1);

        $pagination = $paginator->paginate($orders, $page, $perPage);

        return $this->render('ModulesContactBundle:Default/Order:index.html.twig', [
            'pagination' => $pagination,
            'contact' => $contact,
            'currentCategory' => $category,
            'contactFields' => $contactFields
        ]);
    }

    /**
     * @Route("/{contact}/details/offer")
     * @Route("/category/{category}/{contact}/details/offer")
     * @Security("is_granted('VIEW', 'Modules\\OrderManagementBundle\\Entity\\Offer')")
     * @param Request $request
     * @param Contact $contact
     * @param Category $category
     * @return \Symfony\Component\HttpFoundation\Response
     */

    public function detailsOfferAction(Request $request, Contact $contact, Category $category = null)
    {
        $helper = $this->get('mod.contact.helper');

        $contactFields = $this->getDoctrine()->getManager()->getRepository('ModulesContactBundle:Field')->findByFormWithNoCategories('contact');

        $offers = $helper->getEntitiesForContact($contact, 'Modules\\OrderManagementBundle\\Entity\\Offer');

        $perPage = 10;

        $paginator = $this->get('knp_paginator');

        $page = $request->query->getInt('page', 1);

        $pagination = $paginator->paginate($offers, $page, $perPage);

        return $this->render('ModulesContactBundle:Default/Offer:index.html.twig', [
            'pagination' => $pagination,
            'contact' => $contact,
            'currentCategory' => $category,
            'contactFields' => $contactFields
        ]);
    }


    /**
     * @Route("/{contact}/details/addresses")
     * @Route("/category/{category}/{contact}/details/addresses")
     * @Security("is_granted('VIEW', 'Modules\\ContactBundle\\Entity\\Contact')")
     * @param Request $request
     * @param Contact $contact
     * @param Category $category
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function detailsAddressesAction(Request $request, Contact $contact, Category $category = null)
    {
        $fields = $this->getDoctrine()->getManager()->getRepository('ModulesContactBundle:Field')->findByFormAndContact('address', $contact);
        $contactFields = $this->getDoctrine()->getManager()->getRepository('ModulesContactBundle:Field')->findByFormWithNoCategories('contact');

        $addresses = $this->getDoctrine()->getManager()->getRepository('ModulesContactBundle:Address')->findOrderedForContact($contact, $request->query->get('sort', 'order'), $request->query->get('direction', 'ASC'));
        $columnDefaults = ['street' => ['active' => 1, 'label' => 'Straße'], 'zip' => ['active' => 1, 'label' => 'PLZ'], 'city' => ['active' => 1, 'label' => 'Ort'], 'state' => ['active' => 1, 'label' => 'Bundesland'], 'country' => ['active' => 1, 'label' => 'Land'],];
        /** @var Field $field */
        foreach ($fields as $field) {
            $columnDefaults['_field_' . $field->getName()] = ['active' => 1, 'label' => $field->getName()];
        }
        $columns = $this->get('app.helper')->setTableColumns('mod_contact_addresses_table_columns', $columnDefaults);
        $perPage = $this->get('app.helper')->setTablePerPage('mod_contact_addresses_table_rows');
        $paginator = $this->get('knp_paginator');
        $page = $request->query->getInt('page', 1);
        $pagination = $paginator->paginate($addresses, $page, $perPage);

        return $this->render('ModulesContactBundle:Default/Address:index.html.twig', ['currentCategory' => $category, 'contact' => $contact, 'pagination' => $pagination, 'fields' => $fields, 'contactFields' => $contactFields, 'columns' => $columns, 'perPage' => $perPage]);
    }

    /**
     * @Route("/{contact}/details/persons")
     * @Route("/category/{category}/{contact}/details/persons")
     * @Security("is_granted('VIEW', 'Modules\\ContactBundle\\Entity\\Contact')")
     * @param Request $request
     * @param Contact $contact
     * @param Category $category
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function detailsPersonsAction(Request $request, Contact $contact, Category $category = null)
    {
        $fields = $this->getDoctrine()->getManager()->getRepository('ModulesContactBundle:Field')->findByFormAndContact('person', $contact);
        $contactFields = $this->getDoctrine()->getManager()->getRepository('ModulesContactBundle:Field')->findByFormWithNoCategories('contact');

        $persons = $this->getDoctrine()->getManager()->getRepository('ModulesContactBundle:Person')->findOrderedForContact($contact, $request->query->get('sort', 'order'), $request->query->get('direction', 'ASC'));
        $columnDefaults = ['position' => ['active' => 1, 'label' => 'Position'], 'contact' => ['active' => 1, 'label' => 'Kontakt'],];
        /** @var Field $field */
        foreach ($fields as $field) {
            $columnDefaults['_field_' . $field->getName()] = ['active' => 1, 'label' => $field->getName()];
        }
        $columns = $this->get('app.helper')->setTableColumns('mod_contact_persons_table_columns', $columnDefaults);
        $perPage = $this->get('app.helper')->setTablePerPage('mod_contact_persons_table_rows');
        $paginator = $this->get('knp_paginator');
        $page = $request->query->getInt('page', 1);
        $pagination = $paginator->paginate($persons, $page, $perPage);

        return $this->render('ModulesContactBundle:Default/Person:index.html.twig', ['currentCategory' => $category, 'contact' => $contact, 'pagination' => $pagination, 'fields' => $fields, 'contactFields' => $contactFields, 'columns' => $columns, 'perPage' => $perPage]);
    }

    /**
     * @Route("/{contact}/details/bankAccounts")
     * @Route("/category/{category}/{contact}/details/bankAccounts")
     * @Security("is_granted('VIEW', 'Modules\\ContactBundle\\Entity\\Contact')")
     * @param Request $request
     * @param Contact $contact
     * @param Category $category
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function detailsBankAccountsAction(Request $request, Contact $contact, Category $category = null)
    {
        $fields = $this->getDoctrine()->getManager()->getRepository('ModulesContactBundle:Field')->findByFormAndContact('bank_account', $contact);
        $contactFields = $this->getDoctrine()->getManager()->getRepository('ModulesContactBundle:Field')->findByFormWithNoCategories('contact');

        $bankAccounts = $this->getDoctrine()->getManager()->getRepository('ModulesContactBundle:BankAccount')->findOrderedForContact($contact, $request->query->get('sort', 'order'), $request->query->get('direction', 'ASC'));
        $columnDefaults = ['owner' => ['active' => 1, 'label' => 'Inhaber'], 'bank' => ['active' => 1, 'label' => 'Kreditinstitut'], 'iban' => ['active' => 1, 'label' => 'IBAN'], 'bic' => ['active' => 1, 'label' => 'BIC'],];
        /** @var Field $field */
        foreach ($fields as $field) {
            $columnDefaults['_field_' . $field->getName()] = ['active' => 1, 'label' => $field->getName()];
        }
        $columns = $this->get('app.helper')->setTableColumns('mod_contact_bank_accounts_table_columns', $columnDefaults);
        $perPage = $this->get('app.helper')->setTablePerPage('mod_contact_bank_accounts_table_rows');
        $paginator = $this->get('knp_paginator');
        $page = $request->query->getInt('page', 1);
        $pagination = $paginator->paginate($bankAccounts, $page, $perPage);

        return $this->render('ModulesContactBundle:Default/Account:index.html.twig', ['currentCategory' => $category, 'contact' => $contact, 'pagination' => $pagination, 'fields' => $fields, 'contactFields' => $contactFields, 'columns' => $columns, 'perPage' => $perPage]);
    }

    /**
     * @Route("/{contact}/details/users")
     * @Route("/category/{category}/{contact}/details/users")
     * @Security("is_granted('VIEW', 'Modules\\ContactBundle\\Entity\\Contact')")
     * @param Request $request
     * @param Contact $contact
     * @param Category $category
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function detailsUsersAction(Request $request, Contact $contact, Category $category = null)
    {
        $contactFields = $this->getDoctrine()->getManager()->getRepository('ModulesContactBundle:Field')->findByFormWithNoCategories('contact');

        $users = $this->getDoctrine()->getManager()->getRepository('CoreAppBundle:User')->findOrderedForContact($contact, $request->query->get('sort', 'username'), $request->query->get('direction', 'ASC'));
        $columnDefaults = ['email' => ['active' => 1, 'label' => 'E-Mail'], 'groups' => ['active' => 1, 'label' => 'Gruppen'], 'lastLogin' => ['active' => 1, 'label' => 'Letzte Anmeldung'], 'active' => ['active' => 0, 'label' => 'Aktiviert'],];

        $columns = $this->get('app.helper')->setTableColumns('mod_contact_users_table_columns', $columnDefaults);
        $perPage = $this->get('app.helper')->setTablePerPage('mod_contact_users_table_rows');
        $paginator = $this->get('knp_paginator');
        $page = $request->query->getInt('page', 1);
        $pagination = $paginator->paginate($users, $page, $perPage);

        return $this->render('ModulesContactBundle:Default/User:index.html.twig', ['currentCategory' => $category, 'contact' => $contact, 'pagination' => $pagination, 'contactFields' => $contactFields, 'columns' => $columns, 'perPage' => $perPage]);
    }

    /**
     * Lists all article entities.
     * @Route("/{contact}/details/articles")
     * @Route("/category/{category}/{contact}/details/articles")
     * @Security("is_granted('VIEW', 'Modules\\ContactBundle\\Entity\\Contact') and is_granted('VIEW', 'Modules\\WikiBundle\\Entity\\Article')")
     * @param Request $request
     * @param Contact $contact
     * @param Category $category
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function detailsArticlesAction(Request $request, Contact $contact, Category $category = null)
    {
        $contactFields = $this->getDoctrine()->getManager()->getRepository('ModulesContactBundle:Field')->findByFormWithNoCategories('contact');

        $articles = $this->getDoctrine()->getManager()->getRepository('ModulesWikiBundle:Article')->findAllOrderedByContact($contact, $request->query->get('sort', 'name'), $request->query->get('direction', 'ASC'));
        $columns = $this->get('app.helper')->setTableColumns('mod_contact_wiki_article_table_columns', ['project' => ['active' => 0, 'label' => 'Projekt'], 'createdBy' => ['active' => 1, 'label' => 'Autor'], 'createdAt' => ['active' => 1, 'label' => 'erstellt am'], 'updatedAt' => ['active' => 0, 'label' => 'letzte Änderung'],]);
        $perPage = $this->get('app.helper')->setTablePerPage('mod_contact_wiki_article_table_rows');
        $paginator = $this->get('knp_paginator');
        $page = $request->query->getInt('page', 1);
        $pagination = $paginator->paginate($articles, $page, $perPage);

        return $this->render('ModulesContactBundle:Default/WikiArticle:index.html.twig', ['currentCategory' => $category, 'contact' => $contact, 'pagination' => $pagination, 'contactFields' => $contactFields, 'columns' => $columns, 'perPage' => $perPage]);
    }

    /**
     * Creates a new contact entity.
     * @Route("/new")
     * @Route("/category/{category}/new")
     * @Method({"GET", "POST"})
     * @Security("is_granted('CREATE', 'Modules\\ContactBundle\\Entity\\Contact')")
     * @param Request $request
     * @param Category $category
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request, Category $category = null)
    {
        $contact = new Contact();
        if ($category) {
            $contact->addCategory($category);
        }
        $form = $this->createForm(ContactType::class, $contact);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($contact);
            $em->flush();

            $this->saveFields($form);

            $this->addFlash('success', '<i class="uk-icon-check"></i> Der Kontakt wurde erfolgreich angelegt!');

            if ($category) {
                return $this->redirectToRoute('modules_contact_default_details_1', ['contact' => $contact->getId(), 'category' => $category->getId()]);
            }

            return $this->redirectToRoute('modules_contact_default_details', ['contact' => $contact->getId()]);
        }

        return $this->render('ModulesContactBundle:Default:new.html.twig', ['currentCategory' => $category, 'contact' => $contact, 'form' => $form->createView(),]);
    }

    /**
     * @param Form $form
     */
    private function saveFields(Form $form)
    {
        // save custom fields in main form
        $this->get('mod.contact.helper')->saveFields($form);

        // save custom fields in sub forms (addresses, persons, etc.)
        /** @var Form $contactField */
        foreach ($form as $contactField) {
            if (in_array($contactField->getName(), ['addresses', 'persons', 'bankAccounts'])) {
                foreach ($contactField as $entityForm) {
                    $this->get('mod.contact.helper')->saveFields($entityForm);
                }
            }
        }
    }

    /**
     * Displays a form to edit an existing contact entity.
     * @Route("/{contact}/edit")
     * @Route("/category/{category}/{contact}/edit")
     * @Method({"GET", "POST"})
     * @Security("is_granted('EDIT', 'Modules\\ContactBundle\\Entity\\Contact') or is_granted('EDIT', contact)")
     * @param Request $request
     * @param Contact $contact
     * @param Category $category
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @ParamConverter("contact", class="Modules\ContactBundle\Entity\Contact")
     */
    public function editAction(Request $request, Contact $contact, Category $category = null)
    {
        $em = $this->getDoctrine()->getManager();
        $deleteForm = $this->createDeleteForm($contact);

        // get original addresses
        $originalAddresses = new ArrayCollection();
        foreach ($contact->getAddresses() as $address) {
            $originalAddresses->add($address);
        }
        // get original persons
        $originalPersons = new ArrayCollection();
        foreach ($contact->getPersons() as $person) {
            $originalPersons->add($person);
        }
        // get original bankAccounts
        $originalBankAccounts = new ArrayCollection();
        foreach ($contact->getBankAccounts() as $bankAccount) {
            $originalBankAccounts->add($bankAccount);
        }
        // get original users
        $originalUsers = new ArrayCollection();
        foreach ($contact->getUsers() as $user) {
            $originalUsers->add($user);
        }

        $editForm = $this->createForm(ContactType::class, $contact, ['edit' => true]);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            // remove addresses not submitted in the form
            foreach ($originalAddresses as $address) {
                if (false === $contact->getAddresses()->contains($address)) {
                    $em->remove($address);
                }
            }
            // remove persons not submitted in the form
            foreach ($originalPersons as $person) {
                if (false === $contact->getPersons()->contains($person)) {
                    $em->remove($person);
                }
            }
            // remove bankAccounts not submitted in the form
            foreach ($originalBankAccounts as $bankAccount) {
                if (false === $contact->getBankAccounts()->contains($bankAccount)) {
                    $em->remove($bankAccount);
                }
            }
            // remove users not submitted in the form
            foreach ($originalUsers as $user) {
                if (false === $contact->getUsers()->contains($user)) {
                    $em->remove($user);
                }
            }

            $em->flush();

            $this->saveFields($editForm);

            $this->addFlash('success', '<i class="uk-icon-check"></i> Der Kontakt wurde erfolgreich aktualisiert!');

            if ($category) {
                return $this->redirectToRoute('modules_contact_default_details_1', ['contact' => $contact->getId(), 'category' => $category->getId()]);
            }

            return $this->redirectToRoute('modules_contact_default_details', ['contact' => $contact->getId()]);
        }

        return $this->render('ModulesContactBundle:Default:edit.html.twig', ['currentCategory' => $category, 'contact' => $contact, 'edit_form' => $editForm->createView(), 'delete_form' => $deleteForm->createView(),]);
    }

    /**
     * Creates a form to delete a contact entity.
     * @param Contact $contact The contact entity
     * @param Category $category
     * @return Form The form
     */
    private function createDeleteForm(Contact $contact, Category $category = null)
    {
        $others = $this->getDoctrine()->getManager()->getRepository('ModulesContactBundle:Contact')->getOthers($contact);
        $choices = ['auch löschen' => 'delete'];
        /** @var Contact $other */
        foreach ($others as $other) {
            $label = 'Kontakt "' . $other->getName() . '" zuweisen';
            if ($other->getCategories()) {
                $label .= ' (';
                $labelCategories = [];
                foreach ($other->getCategories() as $cat) {
                    $labelCategories[] = $cat->getName();
                }
                $label .= implode(', ', $labelCategories);
                $label .= ')';
            }
            $choices[$label] = $other->getId();
        }

        return $this->createFormBuilder()->add('addresses_action', ChoiceType::class, ['choices' => $choices, 'label' => 'Adressen'])->add('persons_action', ChoiceType::class, ['choices' => $choices, 'label' => 'Ansprechpartner'])->add('bank_accounts_action', ChoiceType::class, ['choices' => $choices, 'label' => 'Kontoverbindungen'])->add('wiki_articles_action', ChoiceType::class, ['choices' => array_merge(['behalten' => 'keep'], $choices), 'label' => 'Wiki-Artikel'])->setAction($category ? $this->generateUrl('modules_contact_default_delete_1', ['contact' => $contact->getId(), 'category' => $category->getId()]) : $this->generateUrl('modules_contact_default_delete', ['contact' => $contact->getId()]))->setMethod('DELETE')->getForm();
    }

    /**
     * Deletes a contact entity.
     * @Route("/{contact}/delete")
     * @Route("/category/{category}/{contact}/delete")
     * @Method({"GET", "DELETE"})
     * @Security("is_granted('DELETE', 'Modules\\ContactBundle\\Entity\\Contact') or is_granted('DELETE', contact)")
     * @param Request $request
     * @param Contact $contact
     * @param Category $category
     * @return RedirectResponse
     */
    public function deleteAction(Request $request, Contact $contact, Category $category = null)
    {
        $em = $this->getDoctrine()->getManager();

        // handle related entities
        $addressesAction = $request->request->get('form')['addresses_action'];
        if ((int)$addressesAction) {
            $newContact = $em->getRepository('ModulesContactBundle:Contact')->find((int)$addressesAction);
            if ($newContact) {
                /** @var Contact $contact */
                foreach ($contact->getAddresses() as $address) {
                    if (!$newContact->getAddresses()->contains($address)) {
                        $newContact->addAddress($address);
                    }
                }
            }
        }
        $personsAction = $request->request->get('form')['persons_action'];
        if ((int)$personsAction) {
            $newContact = $em->getRepository('ModulesContactBundle:Contact')->find((int)$personsAction);
            if ($newContact) {
                /** @var Contact $contact */
                foreach ($contact->getPersons() as $person) {
                    if (!$newContact->getPersons()->contains($person)) {
                        $newContact->addPerson($person);
                    }
                }
            }
        }
        $bankAccountsAction = $request->request->get('form')['bank_accounts_action'];
        if ((int)$bankAccountsAction) {
            $newContact = $em->getRepository('ModulesContactBundle:Contact')->find((int)$bankAccountsAction);
            if ($newContact) {
                /** @var Contact $contact */
                foreach ($contact->getBankAccounts() as $bankAccount) {
                    if (!$newContact->getBankAccounts()->contains($bankAccount)) {
                        $newContact->addBankAccount($bankAccount);
                    }
                }
            }
        }
        $wikiArticlesAction = $request->request->get('form')['wiki_articles_action'];
        $wikiArticles = $em->getRepository('ModulesWikiBundle:Article')->findBy(['contact' => $contact]);
        if ($wikiArticlesAction == 'delete') {
            foreach ($wikiArticles as $wikiArticle) {
                $em->remove($wikiArticle);
            }
        } elseif ((int)$wikiArticlesAction) {
            $newContact = $em->getRepository('ModulesContactBundle:Contact')->find((int)$wikiArticlesAction);
            if ($newContact) {
                /** @var Contact $contact */
                foreach ($wikiArticles as $wikiArticle) {
                    $wikiArticle->setContact($newContact);
                }
                $em->flush();
            }
        }

        $em->remove($contact);
        $em->flush();


        if ($category) {
            return $this->redirectToRoute('modules_contact_default_categoryindex', ['category' => $category->getId()]);
        }

        return $this->redirectToRoute('modules_contact_default_index');
    }

    /**
     * @param Request $request
     * @param Contact $contact
     * @Route("/{contact}/deleteModal",options={"expose"=true})
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function renderDeleteModal(Request $request, Contact $contact)
    {
        return $this->render("ModulesContactBundle:Default:delete-modal.html.twig", ["contact" => $contact]);
    }
}