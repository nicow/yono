<?php

namespace Modules\ContactBundle\Controller;

use Modules\ContactBundle\Entity\Category;
use Modules\ContactBundle\Entity\Contact;
use Modules\ContactBundle\Entity\Person;
use Modules\ContactBundle\Form\PersonType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Person controller.
 * @Route("/person")
 */
class PersonController extends Controller
{
    /**
     * Creates a new person entity.
     * @Route("/new/{contact}")
     * @Route("/category/{category}/new/{contact}")
     * @Method({"GET", "POST"})
     * @Security("is_granted('EDIT', 'Modules\\ContactBundle\\Entity\\Contact') or (contact and is_granted('EDIT', contact))")
     * @param Request $request
     * @param Contact $contact
     * @param Category $category
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request, Contact $contact = null, Category $category = null)
    {
        $person = new Person();
        if ($contact) {
            $person->setContact($contact);
        }
        $form = $this->createForm(PersonType::class, $person, ['contact' => $contact ? 0 : 1, 'action' => $category ? $this->generateUrl('modules_contact_person_new_1', ['contact' => $person->getContact()->getId(), 'category' => $category->getId()]) : $this->generateUrl('modules_contact_person_new', ['contact' => $person->getContact() ? $person->getContact()->getId() : null])]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($person);
            $em->flush();

            $this->get('mod.contact.helper')->saveFields($form);

            $this->addFlash('success', '<i class="uk-icon-check"></i> Der Ansprechpartner wurde erfolgreich angelegt!');

            if ($category) {
                return new JsonResponse(['redirect' => $this->generateUrl('modules_contact_default_detailspersons_1', ['contact' => $person->getContact()->getId(), 'category' => $category->getId()])]);
            } else {
                return new JsonResponse(['redirect' => $this->generateUrl('modules_contact_default_detailspersons', ['contact' => $person->getContact()->getId()])]);
            }
        }

        return $this->render('ModulesContactBundle:Person:new.html.twig', ['currentCategory' => $category, 'person' => $person, 'form' => $form->createView(),]);
    }
}