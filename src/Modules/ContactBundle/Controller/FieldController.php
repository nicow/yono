<?php

namespace Modules\ContactBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Modules\ContactBundle\Entity\Field;
use Modules\ContactBundle\Form\FieldType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Field controller.
 * @Route("field")
 */
class FieldController extends Controller
{
    /**
     * Lists all field entities.
     * @Route("/")
     * @Method("GET")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $fields = $this->getDoctrine()->getManager()->getRepository('ModulesContactBundle:Field')->findBy([], [$request->query->get('sort', 'form') => $request->query->get('direction', 'ASC')]);
        $paginator = $this->get('knp_paginator');
        $page = $request->query->getInt('page', 1);
        $limit = 10;
        $pagination = $paginator->paginate($fields, $page, $limit);

        $deleteForms = [];
        foreach ($fields as $field) {
            $deleteForms[$field->getId()] = $this->createDeleteForm($field)->createView();
        }

        return $this->render('ModulesContactBundle:Field:index.html.twig', ['pagination' => $pagination, 'deleteForms' => $deleteForms]);
    }

    /**
     * Creates a form to delete a field entity.
     * @param Field $field The field entity
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Field $field)
    {
        return $this->createFormBuilder()->setAction($this->generateUrl('modules_contact_field_delete', ['id' => $field->getId()]))->setMethod('DELETE')->getForm();
    }

    /**
     * Creates a new field entity.
     * @Route("/new")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $field = new Field();
        $form = $this->createForm(FieldType::class, $field);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($field);
            $em->flush();

            return $this->redirectToRoute('modules_contact_field_index');
        }

        return $this->render('ModulesContactBundle:Field:new.html.twig', ['field' => $field, 'form' => $form->createView(),]);
    }

    /**
     * Displays a form to edit an existing field entity.
     * @Route("/{id}/edit")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param Field $field
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, Field $field)
    {
        $deleteForm = $this->createDeleteForm($field);

        // get original field options
        $originalFieldOptions = new ArrayCollection();
        foreach ($field->getFieldOptions() as $fieldOption) {
            $originalFieldOptions->add($fieldOption);
        }

        $editForm = $this->createForm('Modules\ContactBundle\Form\FieldType', $field);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();

            // remove fieldOptions not submitted in the form
            foreach ($originalFieldOptions as $fieldOption) {
                if (false === $field->getFieldOptions()->contains($fieldOption)) {
                    $em->remove($fieldOption);
                }
            }

            $em->flush();

            $this->addFlash('success', '<i class="uk-icon-check"></i> Formularfeld wurde aktualisiert!');

            return $this->redirectToRoute('modules_contact_field_edit', ['id' => $field->getId()]);
        }

        return $this->render('ModulesContactBundle:Field:edit.html.twig', ['field' => $field, 'edit_form' => $editForm->createView(), 'delete_form' => $deleteForm->createView(),]);
    }

    /**
     * Deletes a field entity.
     * @Route("/{id}")
     * @Method("DELETE")
     * @param Request $request
     * @param Field $field
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, Field $field)
    {
        $form = $this->createDeleteForm($field);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($field);
            $em->flush();
        }

        return $this->redirectToRoute('modules_contact_field_index');
    }
}
