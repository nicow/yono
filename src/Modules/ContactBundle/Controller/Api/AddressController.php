<?php

namespace Modules\ContactBundle\Controller\Api;

use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use Modules\ContactBundle\Entity\Address;
use Modules\ContactBundle\Entity\Contact;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Response;

class AddressController extends FOSRestController
{
    /**
     * @View
     * @ApiDoc(
     *   section="Contacts",
     *   resource=true,
     *   description="Creates a new address resource.",
     *   statusCodes={
     *     201 = "Returned if successful",
     *     400 = "Returned if validation fails"
     *   }
     * )
     * @return \Symfony\Component\Form\Form|Response
     */
    public function postAddressAction()
    {
        return $this->processForm(new Address());
    }

    /**
     * @param Address $address
     * @return \Symfony\Component\Form\Form|Response
     */
    private function processForm(Address $address)
    {
        // create form and let it handle the request
        $form = $this->createForm('Modules\ContactBundle\Form\AddressType', $address, ['method' => $this->get('request_stack')->getCurrentRequest()->getMethod()]);
        $form->handleRequest($this->get('request_stack')->getCurrentRequest());

        // create/update the resource
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $statusCode = Response::HTTP_NO_CONTENT;

            // if address is new persist it and set status code
            if (!$address->getId()) {
                $em->persist($address);
                $statusCode = Response::HTTP_CREATED;
            }

            $em->flush();

            $response = new Response();
            $response->setStatusCode($statusCode);

            // set the Location header only when creating new resources
            if (Response::HTTP_CREATED === $statusCode) {
                $response->headers->set('Location', $this->generateUrl('api_get_address', ['address' => $address->getId()], 0));
            }

            return $response;
        }

        // return form object with error messages
        return $form;
    }

    /**
     * @View
     * @ApiDoc(
     *   section="Contacts",
     *   resource=true,
     *   description="Updates an existing address resource.",
     *   statusCodes={
     *     204 = "Returned if successful",
     *     400 = "Returned if validation fails"
     *   }
     * )
     * @param Address $address
     * @return \Symfony\Component\Form\Form|Response
     */
    public function putAddressAction(Address $address)
    {
        return $this->processForm($address);
    }

    /**
     * @View
     * @ApiDoc(
     *   section="Contacts",
     *   resource=true,
     *   description="Gets an address resource.",
     *   statusCodes={
     *     200 = "Returned if successful",
     *     404 = "Returned if not found"
     *   }
     * )
     * @param Address $address
     * @return Address
     */
    public function getAddressAction(Address $address)
    {
        return $address;
    }

    /**
     * @View
     * @ApiDoc(
     *   section="Contacts",
     *   resource=true,
     *   description="Gets a collection of address resources.",
     *   statusCodes={
     *     200 = "Returned if successful"
     *   }
     * )
     * @return mixed
     */
    public function getAddressesAction()
    {
        $addresses = $this->getDoctrine()->getManager()->getRepository('ModulesContactBundle:Address')->findAll();

        return $addresses;
    }

    /**
     * @View
     * @ApiDoc(
     *   section="Contacts",
     *   resource=true,
     *   description="Gets a collection of address resources for a specific contact.",
     *   statusCodes={
     *     200 = "Returned if successful"
     *   }
     * )
     * @param Contact $contact
     * @return mixed
     */
    public function getContactAddressesAction(Contact $contact)
    {
        $addresses = $this->getDoctrine()->getManager()->getRepository('ModulesContactBundle:Address')->findBy(['contact' => $contact]);

        return $addresses;
    }

    /**
     * @View()
     * @ApiDoc(
     *   section="Contacts",
     *   resource=true,
     *   description="Deletes an address resources.",
     *   statusCodes={
     *     204 = "Returned if successful",
     *     404 = "Returned if not found"
     *   }
     * )
     * @param Address $address
     */
    public function deleteAddressAction(Address $address)
    {
        $this->getDoctrine()->getManager()->remove($address);
        $this->getDoctrine()->getManager()->flush();
    }
}