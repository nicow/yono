<?php

namespace Modules\ContactBundle\Controller\Api;

use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use Modules\ContactBundle\Entity\Contact;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Response;

class ContactController extends FOSRestController
{
    /**
     * @View
     * @ApiDoc(
     *   section="Contacts",
     *   resource=true,
     *   description="Creates a new contact resource.",
     *   statusCodes={
     *     201 = "Returned if successful",
     *     400 = "Returned if validation fails"
     *   }
     * )
     * @return \Symfony\Component\Form\Form|Response
     */
    public function postContactAction()
    {
        return $this->processForm(new Contact());
    }

    /**
     * @param Contact $contact
     * @return \Symfony\Component\Form\Form|Response
     */
    private function processForm(Contact $contact)
    {
        // create form and let it handle the request
        $form = $this->createForm('Modules\ContactBundle\Form\ContactType', $contact, ['method' => $this->get('request_stack')->getCurrentRequest()->getMethod()]);
        $form->handleRequest($this->get('request_stack')->getCurrentRequest());

        // create/update the resource
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $statusCode = Response::HTTP_NO_CONTENT;

            // if address is new persist it and set status code
            if (!$contact->getId()) {
                $em->persist($contact);
                $statusCode = Response::HTTP_CREATED;
            }

            $em->flush();

            $response = new Response();
            $response->setStatusCode($statusCode);

            // set the Location header only when creating new resources
            if (Response::HTTP_CREATED === $statusCode) {
                $response->headers->set('Location', $this->generateUrl('get_contact', ['contact' => $contact->getId()], 0));
            }

            return $response;
        }

        // return form object with error messages
        return $form;
    }

    /**
     * @View
     * @ApiDoc(
     *   section="Contacts",
     *   resource=true,
     *   description="Updates an existing contact resource.",
     *   statusCodes={
     *     204 = "Returned if successful",
     *     400 = "Returned if validation fails"
     *   }
     * )
     * @param Contact $contact
     * @return \Symfony\Component\Form\Form|Response
     */
    public function putContactAction(Contact $contact)
    {
        return $this->processForm($contact);
    }

    /**
     * @View
     * @ApiDoc(
     *   section="Contacts",
     *   resource=true,
     *   description="Gets a contact resource.",
     *   statusCodes={
     *     200 = "Returned if successful",
     *     404 = "Returned if not found"
     *   }
     * )
     * @param Contact $contact
     * @return Contact
     */
    public function getContactAction(Contact $contact)
    {
        return $contact;
    }

    /**
     * @View
     * @ApiDoc(
     *   section="Contacts",
     *   resource=true,
     *   description="Gets a collection of contact resources.",
     *   statusCodes={
     *     200 = "Returned if successful"
     *   }
     * )
     * @return mixed
     */
    public function getContactsAction()
    {
        $contacts = $this->getDoctrine()->getManager()->getRepository('ModulesContactBundle:Contact')->findAll();

        return $contacts;
    }

    /**
     * @ApiDoc(
     *   section="Contacts",
     *   resource=true,
     *   description="Deletes a contact resources.",
     *   statusCodes={
     *     204 = "Returned if successful",
     *     404 = "Returned if not found"
     *   }
     * )
     * @param Contact $contact
     */
    public function deleteContactAction(Contact $contact)
    {
        $this->getDoctrine()->getManager()->remove($contact);
        $this->getDoctrine()->getManager()->flush();
    }
}