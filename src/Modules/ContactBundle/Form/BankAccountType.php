<?php

namespace Modules\ContactBundle\Form;

use Doctrine\ORM\EntityManager;
use Modules\ContactBundle\Entity\BankAccount;
use Modules\ContactBundle\Event\Subscriber\AddFieldsSubscriber;
use Modules\ContactBundle\Service\ContactHelper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class BankAccountType extends AbstractType
{
    /**
     * @var EntityManager
     */
    private $em;
    /**
     * @var ContactHelper
     */
    private $contactHelper;

    public function __construct(EntityManager $em, ContactHelper $contactHelper)
    {
        $this->em = $em;
        $this->contactHelper = $contactHelper;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if ($options['contact']) {
            $builder->add('contact', null, ['label' => 'Kontakt', 'constraints' => [new Assert\NotNull(['message' => 'Bitte wählen Sie einen Kontakt.'])]]);
        }

        $builder->add('owner', null, ['label' => 'Inhaber', 'required' => false])->add('bank', null, ['label' => 'Kreditinstitut', 'required' => false])->add('iban', null, ['label' => 'IBAN', 'constraints' => [new Assert\Iban()]])->add('bic', null, ['label' => 'BIC', 'required' => false, 'constraints' => [new Assert\Bic()]])->add('order', HiddenType::class, ['attr' => ['class' => 'order']]);

        // add individual fields from database
        $builder->addEventSubscriber(new AddFieldsSubscriber('bank_account', $this->em, $this->contactHelper->getCurrentCategory()));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array('data_class' => BankAccount::class, 'csrf_protection' => false, 'contact' => false));
    }
}
