<?php

namespace Modules\ContactBundle\Form;

use Doctrine\ORM\EntityManager;
use Modules\ContactBundle\Entity\Address;
use Modules\ContactBundle\Event\Subscriber\AddFieldsSubscriber;
use Modules\ContactBundle\Service\ContactHelper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class AddressType extends AbstractType
{
    /**
     * @var EntityManager
     */
    private $em;
    /**
     * @var ContactHelper
     */
    private $contactHelper;

    public function __construct(EntityManager $em, ContactHelper $contactHelper)
    {
        $this->em = $em;
        $this->contactHelper = $contactHelper;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if ($options['contact']) {
            $builder->add('contact', null, ['label' => 'Kontakt', 'constraints' => [new Assert\NotNull(['message' => 'Bitte wählen Sie einen Kontakt.'])]]);
        }

        $builder->add('name', null, ['label' => 'Bezeichnung', 'constraints' => [new Assert\NotBlank()]])->add('street', null, ['label' => 'Straße Nr.', 'constraints' => [new Assert\NotBlank()]])->add('zip', null, ['label' => 'PLZ'])->add('city', null, ['label' => 'Stadt'])->add('state', ChoiceType::class, ['label' => 'Bundesland', 'choices' => ['Deutschland' => ['Baden-Württemberg' => 'Baden-Württemberg', 'Bayern' => 'Bayern', 'Berlin' => 'Berlin', 'Brandenburg' => 'Brandenburg', 'Bremen' => 'Bremen', 'Hamburg' => 'Hamburg', 'Hessen' => 'Hessen', 'Niedersachsen' => 'Niedersachsen', 'Nordrhein-Westfalen' => 'Nordrhein-Westfalen', 'Saarland' => 'Saarland', 'Rheinland-Pfalz' => 'Rheinland-Pfalz', 'Mecklenburg-Vorpommern' => 'Mecklenburg-Vorpommern', 'Sachsen' => 'Sachsen', 'Sachsen-Anhalt' => 'Sachsen-Anhalt', 'Schleswig-Holstein' => 'Schleswig-Holstein', 'Thüringen' => 'Thüringen',], 'Österreich' => ['Burgenland' => 'Burgenland', 'Kärnten' => 'Kärnten', 'Níederösterreich' => 'Níederösterreich', 'Oberösterreich' => 'Oberösterreich', 'Salzburg' => 'Salzburg', 'Steiermark' => 'Steiermark', 'Tirol' => 'Tirol', 'Vorarlberg' => 'Vorarlberg', 'Wien' => 'Wien',], 'Schweiz' => ['Aargau' => 'Aargau', 'Appenzell-Ausserrhoden' => 'Appenzell-Ausserrhoden', 'Appenzell-Innerrhoden' => 'Appenzell-Innerrhoden', 'Basel-Land' => 'Basel-Land', 'Basel-Stadt' => 'Basel-Stadt', 'Bern' => 'Bern', 'Fribourg/Freiburg' => 'Fribourg/Freiburg', 'Geneve/Genf' => 'Geneve/Genf', 'Glarus' => 'Glarus', 'Graubünden/Grischun' => 'Graubünden/Grischun', 'Jura' => 'Jura', 'Luzern' => 'Luzern', 'Neuchatel/Neuenburg' => 'Neuchatel/Neuenburg', 'Nidwalden' => 'Nidwalden', 'Obwalden' => 'Obwalden', 'Schaffhausen' => 'Schaffhausen', 'Schwyz' => 'Schwyz', 'Solothurn' => 'Solothurn', 'St. Gallen' => 'St. Gallen', 'Thurgau' => 'Thurgau', 'Ticino/Tessin' => 'Ticino/Tessin', 'Uri' => 'Uri', 'Valais/Wallis' => 'Valais/Wallis', 'Vaud/Waadt' => 'Vaud/Waadt', 'Zug' => 'Zug', 'Zürich' => 'Zürich',]]])->add('country', ChoiceType::class, ['label' => 'Land', 'choices' => ['Deutschland' => 'Deutschland', 'Österreich' => 'Österreich', 'Schweiz' => 'Schweiz']])->add('order', HiddenType::class, ['attr' => ['class' => 'order']]);

        // add individual fields from database
        $builder->addEventSubscriber(new AddFieldsSubscriber('address', $this->em, $this->contactHelper->getCurrentCategory()));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => Address::class, 'csrf_protection' => false, 'contact' => false]);
    }
}
