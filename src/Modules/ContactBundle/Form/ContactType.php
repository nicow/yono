<?php

namespace Modules\ContactBundle\Form;

use Doctrine\ORM\EntityManager;
use Modules\ContactBundle\Event\Subscriber\AddFieldsSubscriber;
use Modules\ContactBundle\Service\ContactHelper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Validator\Constraints as Assert;

class ContactType extends AbstractType
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var ContactHelper
     */
    private $contactHelper;

    /**
     * @var AuthorizationChecker
     */
    private $authorizationChecker;

    public function __construct(EntityManager $em, ContactHelper $contactHelper, AuthorizationChecker $authorizationChecker)
    {
        $this->em = $em;
        $this->contactHelper = $contactHelper;
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', null, ['label' => 'Geschäftspartner'])->add('categories', null, ['label' => 'Kategorien'])->add('discount', null, ['label' => 'Rabattsatz (%)', 'constraints' => [new Assert\Range(['min' => 0, 'max' => 100])]])
            ->add('addresses', CollectionType::class, ['label' => false, 'entry_type' => AddressType::class, 'allow_add' => true, 'allow_delete' => true, 'by_reference' => false, 'entry_options' => ['label' => false]])
            ->add('persons', CollectionType::class, ['label' => false, 'entry_type' => PersonType::class, 'allow_add' => true, 'allow_delete' => true, 'by_reference' => false, 'entry_options' => ['label' => false]])->add('bankAccounts', CollectionType::class, ['label' => false, 'entry_type' => BankAccountType::class, 'allow_add' => true, 'allow_delete' => true, 'by_reference' => false, 'entry_options' => ['label' => false]]);

        $builder->add('email', EmailType::class, ['label' => 'E-Mail', 'required' => false, 'constraints' => [new Assert\Email()]]);
        // add individual fields from database
        $builder->addEventSubscriber(new AddFieldsSubscriber('contact', $this->em, $this->contactHelper->getCurrentCategory()));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => 'Modules\ContactBundle\Entity\Contact', 'csrf_protection' => false, 'edit' => false]);
    }
}
