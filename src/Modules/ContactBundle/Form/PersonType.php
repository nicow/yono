<?php

namespace Modules\ContactBundle\Form;

use Doctrine\ORM\EntityManager;
use Modules\ContactBundle\Entity\Person;
use Modules\ContactBundle\Event\Subscriber\AddFieldsSubscriber;
use Modules\ContactBundle\Service\ContactHelper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class PersonType extends AbstractType
{
    /**
     * @var EntityManager
     */
    private $em;
    /**
     * @var ContactHelper
     */
    private $contactHelper;

    public function __construct(EntityManager $em, ContactHelper $contactHelper)
    {
        $this->em = $em;
        $this->contactHelper = $contactHelper;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if ($options['contact']) {
            $builder->add('contact', null, ['label' => 'Kontakt', 'constraints' => [new Assert\NotNull(['message' => 'Bitte wählen Sie einen Kontakt.'])]]);
        }

        $builder->add('salutation', ChoiceType::class, ['choices' => ['Herr' => 'Herr', 'Frau' => 'Frau','Firma' => 'Firma'], 'label' => 'Anrede'])->add('firstname', null, ['label' => 'Vorname', 'required' => true])->add('lastname', null, ['label' => 'Nachname', 'constraints' => [new Assert\NotBlank()]])->add('position', null, ['required' => false])->add('phone', null, ['label' => 'Telefonnummer', 'required' => false])->add('mobile', null, ['label' => 'Handynummer', 'required' => false])->add('email', EmailType::class, ['label' => 'E-Mail', 'required' => false, 'constraints' => [new Assert\Email()]])->add('order', HiddenType::class, ['attr' => ['class' => 'order']]);

        // add individual fields from database
        $builder->addEventSubscriber(new AddFieldsSubscriber('person', $this->em, $this->contactHelper->getCurrentCategory()));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array('data_class' => Person::class, 'csrf_protection' => false, 'contact' => false));
    }
}
