<?php

namespace Modules\ContactBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FieldType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name')->add('required', null, ['label' => 'Pflichtfeld?'])->add('form', ChoiceType::class, ['label' => 'Formular', 'choices' => ['Kontakt' => 'contact', 'Adresse' => 'address', 'Ansprechpartner' => 'person', 'Kontoverbindung' => 'bank_account']])->add('type', ChoiceType::class, ['label' => 'Typ', 'choices' => ['Text (einzeilig)' => 'text', 'Text (mehrzeilig)' => 'textarea', 'Auswahl (einfach)' => 'select_single', 'Auswahl (mehrfach)' => 'select_multiple', 'Kontrollkästchen' => 'checkbox']])//            ->add('defaultValue')
        ->add('categories', null, ['label' => 'Kategorien'])->add('fieldOptions', CollectionType::class, ['label' => false, 'entry_type' => FieldOptionType::class, 'allow_add' => true, 'allow_delete' => true, 'by_reference' => false, 'entry_options' => ['label' => false]]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array('data_class' => 'Modules\ContactBundle\Entity\Field'));
    }
}
