<?php

namespace Modules\ContactBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * FieldOptionRepository
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class FieldOptionRepository extends EntityRepository
{
}
