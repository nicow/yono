<?php

namespace Modules\ContactBundle;

use Core\YonoBundleInterface;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class ModulesContactBundle extends Bundle implements YonoBundleInterface
{
    /**
     * @return string
     */
    public function getDisplayName()
    {
        return 'Kontakte';
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return 'Verwalten Sie Ihre Kontakte.';
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'briefcase';
    }
}
