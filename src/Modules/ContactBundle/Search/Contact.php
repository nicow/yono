<?php
/**
 * Created by PhpStorm.
 * User: mkt
 * Date: 04.01.17
 * Time: 20:54
 */

namespace Modules\ContactBundle\Search;

use Core\AppBundle\Search\SearchItem;
use Doctrine\ORM\EntityManager;
use Modules\ContactBundle\Service\ContactHelper;
use Symfony\Bridge\Twig\TwigEngine;
use Symfony\Bundle\FrameworkBundle\Routing\Router;

class Contact extends SearchItem
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var TwigEngine
     */
    private $twig;

    /**
     * @var ContactHelper
     */
    private $contactHelper;

    /**
     * @var Router
     */
    private $router;

    public function __construct(EntityManager $em, TwigEngine $twig, ContactHelper $contactHelper, Router $router)
    {
        $this->em = $em;
        $this->twig = $twig;
        $this->contactHelper = $contactHelper;
        $this->router = $router;
    }

    /**
     * @param $query
     * @return array
     */
    public function getResults($query)
    {
        $contacts = $this->em->getRepository('ModulesContactBundle:Contact')->createQueryBuilder('c')->where('c.name LIKE :query')->setParameter('query', '%' . $query . '%')->getQuery()->getResult();

        $results = [];
        if ($contacts) {
            foreach ($contacts as $contact) {
                $results[] = $this->twig->render('@ModulesContact/Search/contact.html.twig', ['contact' => $contact]);
            }
        }

        return $results;
    }

    /**
     * @param $query
     * @return array
     */
    public function getAutocompleteResults($query)
    {
        $contacts = $this->em->getRepository('ModulesContactBundle:Contact')->createQueryBuilder('c')->where('c.name LIKE :query')->setParameter('query', '%' . $query . '%')->getQuery()->getResult();

        $results = [];
        if ($contacts) {
            /** @var \Modules\ContactBundle\Entity\Contact $contact */
            foreach ($contacts as $contact) {
                $results[] = ['icon' => 'briefcase', 'title' => $contact->getName(), 'url' => $this->router->generate('modules_contact_default_details', ['contact' => $contact->getId()])];
            }
        }

        return $results;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'Kontakte';
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'briefcase';
    }

    public function getRequiredPermissions()
    {
        return ['VIEW' => 'Modules\\ContactBundle\\Entity\\Contact'];
    }
}