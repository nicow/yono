<?php
/**
 * Created by PhpStorm.
 * User: mkt
 * Date: 04.01.17
 * Time: 20:54
 */

namespace Modules\ContactBundle\Search;

use Core\AppBundle\Search\SearchItem;
use Doctrine\ORM\EntityManager;
use Modules\ContactBundle\Service\ContactHelper;
use Symfony\Bridge\Twig\TwigEngine;
use Symfony\Bundle\FrameworkBundle\Routing\Router;

class Person extends SearchItem
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var TwigEngine
     */
    private $twig;

    /**
     * @var ContactHelper
     */
    private $contactHelper;

    /**
     * @var Router
     */
    private $router;

    public function __construct(EntityManager $em, TwigEngine $twig, ContactHelper $contactHelper, Router $router)
    {
        $this->em = $em;
        $this->twig = $twig;
        $this->contactHelper = $contactHelper;
        $this->router = $router;
    }

    /**
     * @param $query
     * @return array
     */
    public function getResults($query)
    {
        $persons = $this->em->getRepository('ModulesContactBundle:Person')->createQueryBuilder('p')->where('p.firstname LIKE :query')->orWhere('p.lastname LIKE :query')->setParameter('query', '%' . $query . '%')->getQuery()->getResult();

        $results = [];
        if ($persons) {
            foreach ($persons as $person) {
                $results[] = $this->twig->render('@ModulesContact/Search/person.html.twig', ['person' => $person]);
            }
        }

        return $results;
    }

    /**
     * @param $query
     * @return array
     */
    public function getAutocompleteResults($query)
    {
        $persons = $this->em->getRepository('ModulesContactBundle:Person')->createQueryBuilder('p')->where('p.firstname LIKE :query')->orWhere('p.lastname LIKE :query')->setParameter('query', '%' . $query . '%')->getQuery()->getResult();

        $results = [];
        if ($persons) {
            /** @var \Modules\ContactBundle\Entity\Person $person */
            foreach ($persons as $person) {
                $results[] = ['icon' => 'user', 'title' => implode(' ', [$person->getSalutation(), $person->getFirstname(), $person->getLastname()]), 'url' => $this->router->generate('modules_contact_default_detailspersons', ['contact' => $person->getContact()->getId()]),];
            }
        }

        return $results;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'Ansprechpartner';
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'user';
    }

    public function getRequiredPermissions()
    {
        return ['VIEW' => 'Modules\\ContactBundle\\Entity\\Contact'];
    }
}