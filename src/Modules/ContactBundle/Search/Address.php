<?php
/**
 * Created by PhpStorm.
 * User: mkt
 * Date: 04.01.17
 * Time: 20:54
 */

namespace Modules\ContactBundle\Search;

use Core\AppBundle\Search\SearchItem;
use Doctrine\ORM\EntityManager;
use Modules\ContactBundle\Service\ContactHelper;
use Symfony\Bridge\Twig\TwigEngine;
use Symfony\Bundle\FrameworkBundle\Routing\Router;

class Address extends SearchItem
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var TwigEngine
     */
    private $twig;

    /**
     * @var ContactHelper
     */
    private $contactHelper;

    /**
     * @var Router
     */
    private $router;

    public function __construct(EntityManager $em, TwigEngine $twig, ContactHelper $contactHelper, Router $router)
    {
        $this->em = $em;
        $this->twig = $twig;
        $this->contactHelper = $contactHelper;
        $this->router = $router;
    }

    /**
     * @param $query
     * @return array
     */
    public function getResults($query)
    {
        $addresses = $this->em->getRepository('ModulesContactBundle:Address')->createQueryBuilder('a')->where('a.street LIKE :query')->orWhere('a.city LIKE :query')->setParameter('query', '%' . $query . '%')->getQuery()->getResult();

        $results = [];
        if ($addresses) {
            foreach ($addresses as $address) {
                $results[] = $this->twig->render('@ModulesContact/Search/address.html.twig', ['address' => $address]);
            }
        }

        return $results;
    }

    /**
     * @param $query
     * @return array
     */
    public function getAutocompleteResults($query)
    {
        $addresses = $this->em->getRepository('ModulesContactBundle:Address')->createQueryBuilder('a')->where('a.street LIKE :query')->orWhere('a.city LIKE :query')->setParameter('query', '%' . $query . '%')->getQuery()->getResult();

        $results = [];
        if ($addresses) {
            /** @var \Modules\ContactBundle\Entity\Address $address */
            foreach ($addresses as $address) {
                $results[] = ['icon' => 'map-marker', 'title' => $address->getStreet() . ' ' . $address->getCity(), 'url' => $this->router->generate('modules_contact_default_detailsaddresses', ['contact' => $address->getContact()->getId()]), 'text' => $address->getName()];
            }
        }

        return $results;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'Adressen';
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'map-marker';
    }

    public function getRequiredPermissions()
    {
        return ['VIEW' => 'Modules\\ContactBundle\\Entity\\Contact'];
    }
}