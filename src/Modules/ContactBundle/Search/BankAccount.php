<?php
/**
 * Created by PhpStorm.
 * User: mkt
 * Date: 04.01.17
 * Time: 20:54
 */

namespace Modules\ContactBundle\Search;

use Core\AppBundle\Search\SearchItem;
use Doctrine\ORM\EntityManager;
use Modules\ContactBundle\Service\ContactHelper;
use Symfony\Bridge\Twig\TwigEngine;
use Symfony\Bundle\FrameworkBundle\Routing\Router;

class BankAccount extends SearchItem
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var TwigEngine
     */
    private $twig;

    /**
     * @var ContactHelper
     */
    private $contactHelper;

    /**
     * @var Router
     */
    private $router;

    public function __construct(EntityManager $em, TwigEngine $twig, ContactHelper $contactHelper, Router $router)
    {
        $this->em = $em;
        $this->twig = $twig;
        $this->contactHelper = $contactHelper;
        $this->router = $router;
    }

    /**
     * @param $query
     * @return array
     */
    public function getResults($query)
    {
        $bankAccounts = $this->em->getRepository('ModulesContactBundle:BankAccount')->createQueryBuilder('b')->where('b.iban LIKE :query')->orWhere('b.bic LIKE :query')->setParameter('query', '%' . $query . '%')->getQuery()->getResult();

        $results = [];
        if ($bankAccounts) {
            foreach ($bankAccounts as $bankAccount) {
                $results[] = $this->twig->render('@ModulesContact/Search/bankAccount.html.twig', ['bankAccount' => $bankAccount]);
            }
        }

        return $results;
    }

    /**
     * @param $query
     * @return array
     */
    public function getAutocompleteResults($query)
    {
        $bankAccounts = $this->em->getRepository('ModulesContactBundle:BankAccount')->createQueryBuilder('b')->where('b.iban LIKE :query')->orWhere('b.bic LIKE :query')->setParameter('query', '%' . $query . '%')->getQuery()->getResult();

        $results = [];
        if ($bankAccounts) {
            /** @var \Modules\ContactBundle\Entity\BankAccount $bankAccount */
            foreach ($bankAccounts as $bankAccount) {
                $title = $bankAccount->getIban();
                if ($bankAccount->getBic()) {
                    $title .= ' (' . $bankAccount->getBic() . ')';
                }
                $text = [];
                if ($bankAccount->getOwner()) {
                    $text[] = 'Inh: ' . $bankAccount->getOwner();
                }
                if ($bankAccount->getBank()) {
                    $text[] = 'Bank: ' . $bankAccount->getBank() . $bankAccount->getBank();
                }
                $results[] = ['icon' => 'credit-card', 'title' => $title, 'url' => $this->router->generate('modules_contact_default_detailsbankaccounts', ['contact' => $bankAccount->getContact()->getId()]), 'text' => implode(', ', $text)];
            }
        }

        return $results;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'Kontoverbindungen';
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'credit-card';
    }

    public function getRequiredPermissions()
    {
        return ['VIEW' => 'Modules\\ContactBundle\\Entity\\Contact'];
    }
}