<?php

namespace Modules\ContactBundle\Entity;

use Core\AppBundle\Entity\BlameableTrait;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * Address
 * @ORM\Table(name="mod_contact_address")
 * @ORM\Entity(repositoryClass="Modules\ContactBundle\Repository\AddressRepository")
 * @Serializer\ExclusionPolicy("All")
 */
class Address implements ContactRelatedEntityInterface
{
    use BlameableTrait;

    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Expose()
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=255)
     * @Serializer\Expose()
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(name="country", type="string", length=100, nullable=true)
     * @Serializer\Expose()
     */
    private $country;

    /**
     * @var string
     * @ORM\Column(name="city", type="string", length=50, nullable=true)
     * @Serializer\Expose()
     */
    private $city;

    /**
     * @var string
     * @ORM\Column(name="zip", type="string", length=10, nullable=true)
     * @Serializer\Expose()
     */
    private $zip;

    /**
     * @var string
     * @ORM\Column(name="street", type="string", length=100)
     * @Serializer\Expose()
     */
    private $street;

    /**
     * @var string
     * @ORM\Column(name="state", type="string", length=50, nullable=true)
     * @Serializer\Expose()
     */
    private $state;

    /**
     * @var Contact
     * @ORM\ManyToOne(targetEntity="Modules\ContactBundle\Entity\Contact", inversedBy="addresses")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $contact;

    /**
     * @var integer
     * @ORM\Column(name="`order`", type="integer", nullable=true)
     */
    private $order;


    /**
     * Get id
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     * @param string $name
     * @return Address
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get contact
     * @return Contact
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set contact
     * @param Contact $contact
     * @return Address
     */
    public function setContact(Contact $contact = null)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get order
     * @return integer
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set order
     * @param integer $order
     * @return Address
     */
    public function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }

    public function __toString()
    {
        $string = $this->getStreet();
        if ($this->getZip() || $this->getCity() || $this->getCountry() || $this->getState()) {
            $string .= ', ';

            $string .= implode(' ', array_filter([$this->getZip(), $this->getCity()]));

            if ($this->getZip() || $this->getCity()) {
                $string .= ', ';
            }

            $string .= implode(', ', array_filter([$this->getState(), $this->getCountry()]));
        }

        return $string;
    }

    /**
     * Get street
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set street
     * @param string $street
     * @return Address
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get zip
     * @return string
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * Set zip
     * @param string $zip
     * @return Address
     */
    public function setZip($zip)
    {
        $this->zip = $zip;

        return $this;
    }

    /**
     * Get city
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set city
     * @param string $city
     * @return Address
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get country
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set country
     * @param string $country
     * @return Address
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get state
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set state
     * @param string $state
     * @return Address
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }
}
