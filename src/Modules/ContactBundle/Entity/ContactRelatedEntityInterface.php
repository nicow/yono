<?php
/**
 * Created by PhpStorm.
 * User: mkt
 * Date: 30.12.16
 * Time: 18:53
 */

namespace Modules\ContactBundle\Entity;


interface ContactRelatedEntityInterface
{
    public function getId();

    public function getContact();
}