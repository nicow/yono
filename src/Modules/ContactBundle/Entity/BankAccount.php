<?php

namespace Modules\ContactBundle\Entity;

use Core\AppBundle\Entity\BlameableTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * BankAccount
 * @ORM\Table(name="mod_contact_bank_account")
 * @ORM\Entity(repositoryClass="Modules\ContactBundle\Repository\BankAccountRepository")
 */
class BankAccount implements ContactRelatedEntityInterface
{
    use BlameableTrait;

    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="bank", type="string", length=100, nullable=true)
     */
    private $bank;

    /**
     * @var string
     * @ORM\Column(name="owner", type="string", length=50, nullable=true)
     */
    private $owner;

    /**
     * @var string
     * @ORM\Column(name="iban", type="string", length=50)
     */
    private $iban;

    /**
     * @var string
     * @ORM\Column(name="bic", type="string", length=25, nullable=true)
     */
    private $bic;

    /**
     * @var Contact
     * @ORM\ManyToOne(targetEntity="Modules\ContactBundle\Entity\Contact", inversedBy="bankAccounts")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $contact;

    /**
     * @var integer
     * @ORM\Column(name="`order`", type="integer", nullable=true)
     */
    private $order;


    /**
     * Get id
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get contact
     * @return \Modules\ContactBundle\Entity\Contact
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set contact
     * @param Contact $contact
     * @return BankAccount
     */
    public function setContact(Contact $contact = null)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get order
     * @return integer
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set order
     * @param integer $order
     * @return BankAccount
     */
    public function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }

    public function __toString()
    {
        $parts = [];

        if ($this->getIban()) {
            $parts[] = 'IBAN: ' . $this->getIban();
        }

        if ($this->getBic()) {
            $parts[] = 'BIC: ' . $this->getBic();
        }

        $string = implode(' ', $parts);

        if ($this->getBank() || $this->getOwner()) {
            $string .= ' (' . implode(', ', array_filter([$this->getOwner(), $this->getBank()])) . ')';
        }

        return $string;
    }

    /**
     * Get iban
     * @return string
     */
    public function getIban()
    {
        return $this->iban;
    }

    /**
     * Set iban
     * @param string $iban
     * @return BankAccount
     */
    public function setIban($iban)
    {
        $this->iban = $iban;

        return $this;
    }

    /**
     * Get bic
     * @return string
     */
    public function getBic()
    {
        return $this->bic;
    }

    /**
     * Set bic
     * @param string $bic
     * @return BankAccount
     */
    public function setBic($bic)
    {
        $this->bic = $bic;

        return $this;
    }

    /**
     * Get bank
     * @return string
     */
    public function getBank()
    {
        return $this->bank;
    }

    /**
     * Set bank
     * @param string $bank
     * @return BankAccount
     */
    public function setBank($bank)
    {
        $this->bank = $bank;

        return $this;
    }

    /**
     * Get owner
     * @return string
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Set owner
     * @param string $owner
     * @return BankAccount
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;

        return $this;
    }
}
