<?php

namespace Modules\ContactBundle\Entity;

use Core\AppBundle\Entity\BlameableTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * FieldValue
 * @ORM\Table(name="mod_contact_field_value")
 * @ORM\Entity(repositoryClass="Modules\ContactBundle\Repository\FieldValueRepository")
 */
class FieldValue
{
    use BlameableTrait;

    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="value_text", type="text", nullable=true)
     */
    private $valueText;

    /**
     * @var float
     * @ORM\Column(name="value_number", type="float", nullable=true)
     */
    private $valueNumber;

    /**
     * @var bool
     * @ORM\Column(name="value_bool", type="boolean", nullable=true)
     */
    private $valueBool;

    /**
     * @var FieldOption
     * @ORM\ManyToOne(targetEntity="Modules\ContactBundle\Entity\FieldOption")
     */
    private $valueOption;

    /**
     * @var Field
     * @ORM\ManyToOne(targetEntity="Modules\ContactBundle\Entity\Field")
     * @ORM\JoinColumn(name="field_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $field;

    /**
     * @var string
     * @ORM\Column(name="entity_class", type="string")
     */
    private $entityClass;

    /**
     * @var int
     * @ORM\Column(name="entity_id", type="integer")
     */
    private $entityId;

    /**
     * Get id
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get valueText
     * @return string
     */
    public function getValueText()
    {
        return $this->valueText;
    }

    /**
     * Set valueText
     * @param string $valueText
     * @return FieldValue
     */
    public function setValueText($valueText)
    {
        $this->valueText = $valueText;

        return $this;
    }

    /**
     * Get valueNumber
     * @return float
     */
    public function getValueNumber()
    {
        return $this->valueNumber;
    }

    /**
     * Set valueNumber
     * @param float $valueNumber
     * @return FieldValue
     */
    public function setValueNumber($valueNumber)
    {
        $this->valueNumber = $valueNumber;

        return $this;
    }

    /**
     * Get valueBool
     * @return boolean
     */
    public function getValueBool()
    {
        return $this->valueBool;
    }

    /**
     * Set valueBool
     * @param boolean $valueBool
     * @return FieldValue
     */
    public function setValueBool($valueBool)
    {
        $this->valueBool = $valueBool;

        return $this;
    }

    /**
     * Get entityClass
     * @return string
     */
    public function getEntityClass()
    {
        return $this->entityClass;
    }

    /**
     * Set entityClass
     * @param string $entityClass
     * @return FieldValue
     */
    public function setEntityClass($entityClass)
    {
        $this->entityClass = $entityClass;

        return $this;
    }

    /**
     * Get entityId
     * @return integer
     */
    public function getEntityId()
    {
        return $this->entityId;
    }

    /**
     * Set entityId
     * @param integer $entityId
     * @return FieldValue
     */
    public function setEntityId($entityId)
    {
        $this->entityId = $entityId;

        return $this;
    }

    /**
     * Get valueOption
     * @return FieldOption
     */
    public function getValueOption()
    {
        return $this->valueOption;
    }

    /**
     * Set valueOption
     * @param FieldOption $valueOption
     * @return FieldValue
     */
    public function setValueOption(FieldOption $valueOption = null)
    {
        $this->valueOption = $valueOption;

        return $this;
    }

    /**
     * Get field
     * @return Field
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * Set field
     * @param Field $field
     * @return FieldValue
     */
    public function setField(Field $field = null)
    {
        $this->field = $field;

        return $this;
    }
}
