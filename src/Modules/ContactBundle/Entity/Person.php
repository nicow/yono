<?php

namespace Modules\ContactBundle\Entity;

use Core\AppBundle\Entity\BlameableTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * Person
 * @ORM\Table(name="mod_contact_person")
 * @ORM\Entity(repositoryClass="Modules\ContactBundle\Repository\PersonRepository")
 */
class Person implements ContactRelatedEntityInterface
{
    use BlameableTrait;

    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="salutation", type="string", length=25)
     */
    private $salutation;

    /**
     * @var string
     * @ORM\Column(name="firstname", type="string", length=50, nullable=true)
     */
    private $firstname;

    /**
     * @var string
     * @ORM\Column(name="lastname", type="string", length=50)
     */
    private $lastname;

    /**
     * @var string
     * @ORM\Column(name="position", type="string", length=100, nullable=true)
     */
    private $position;

    /**
     * @var string
     * @ORM\Column(name="phone", type="string", length=50, nullable=true)
     */
    private $phone;

    /**
     * @var string
     * @ORM\Column(name="mobile", type="string", length=50, nullable=true)
     */
    private $mobile;

    /**
     * @var string
     * @ORM\Column(name="email", type="string", length=100, nullable=true)
     */
    private $email;

    /**
     * @var Contact
     * @ORM\ManyToOne(targetEntity="Modules\ContactBundle\Entity\Contact", inversedBy="persons")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $contact;

    /**
     * @var integer
     * @ORM\Column(name="`order`", type="integer", nullable=true)
     */
    private $order;

    /**
     * Get id
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get phone
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set phone
     * @param string $phone
     * @return Person
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get mobile
     * @return string
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * Set mobile
     * @param string $mobile
     * @return Person
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;

        return $this;
    }

    /**
     * Get email
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set email
     * @param string $email
     * @return Person
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get contact
     * @return Contact
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set contact
     * @param Contact $contact
     * @return Person
     */
    public function setContact(Contact $contact = null)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get order
     * @return integer
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set order
     * @param integer $order
     * @return Person
     */
    public function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }

    public function __toString()
    {
        $string = implode(' ', array_filter([$this->getSalutation(), $this->getFirstname(), $this->getLastname()]));

        if ($this->getPosition()) {
            $string .= ' (' . $this->getPosition() . ')';
        }

        return $string;
    }

    /**
     * Get salutation
     * @return string
     */
    public function getSalutation()
    {
        return $this->salutation;
    }

    /**
     * Set salutation
     * @param string $salutation
     * @return Person
     */
    public function setSalutation($salutation)
    {
        $this->salutation = $salutation;

        return $this;
    }

    /**
     * Get firstname
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set firstname
     * @param string $firstname
     * @return Person
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get lastname
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set lastname
     * @param string $lastname
     * @return Person
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get position
     * @return string
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set position
     * @param string $position
     * @return Person
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }
}
