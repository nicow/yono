<?php

namespace Modules\ContactBundle\Entity;

use Core\AppBundle\Annotation\Acl;
use Core\AppBundle\Entity\BlameableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Field
 * @Acl("Formularfelder")
 * @ORM\Table(name="mod_contact_field")
 * @ORM\Entity(repositoryClass="Modules\ContactBundle\Repository\FieldRepository")
 */
class Field
{
    use BlameableTrait;

    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=50)
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(name="form", type="string", length=50)
     */
    private $form;

    /**
     * @var string
     * @ORM\Column(name="type", type="string", length=25)
     */
    private $type;

    /**
     * @var string
     * @ORM\Column(name="defaultValue", type="string", length=255, nullable=true)
     */
    private $defaultValue;

    /**
     * @var bool
     * @ORM\Column(name="required", type="boolean", nullable=true)
     */
    private $required;

    /**
     * @var FieldOption[]
     * @ORM\OneToMany(targetEntity="Modules\ContactBundle\Entity\FieldOption", mappedBy="field", cascade={"persist"})
     * @ORM\OrderBy({"order" = "ASC"})
     */
    private $fieldOptions;

    /**
     * @var Category[]
     * @ORM\ManyToMany(targetEntity="Modules\ContactBundle\Entity\Category", inversedBy="fields")
     * @ORM\JoinTable(name="mod_contact_fields_categories")
     */
    private $categories;


    public function __construct()
    {
        $this->fieldOptions = new ArrayCollection();
        $this->categories = new ArrayCollection();
    }

    /**
     * Get id
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     * @param string $name
     * @return Field
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get form
     * @return string
     */
    public function getForm()
    {
        return $this->form;
    }

    /**
     * Set form
     * @param string $form
     * @return Field
     */
    public function setForm($form)
    {
        $this->form = $form;

        return $this;
    }

    /**
     * Get type
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set type
     * @param string $type
     * @return Field
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get defaultValue
     * @return string
     */
    public function getDefaultValue()
    {
        return $this->defaultValue;
    }

    /**
     * Set defaultValue
     * @param string $defaultValue
     * @return Field
     */
    public function setDefaultValue($defaultValue)
    {
        $this->defaultValue = $defaultValue;

        return $this;
    }

    /**
     * Add fieldOption
     * @param FieldOption $fieldOption
     * @return Field
     */
    public function addFieldOption(FieldOption $fieldOption)
    {
        $fieldOption->setField($this);
        $this->fieldOptions->add($fieldOption);

        return $this;
    }

    /**
     * Remove fieldOption
     * @param FieldOption $fieldOption
     */
    public function removeFieldOption(FieldOption $fieldOption)
    {
        $this->fieldOptions->removeElement($fieldOption);
    }

    /**
     * Get fieldOptions
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFieldOptions()
    {
        return $this->fieldOptions;
    }

    /**
     * Add category
     * @param Category $category
     * @return Field
     */
    public function addCategory(Category $category)
    {
        $this->categories[] = $category;

        return $this;
    }

    /**
     * Remove category
     * @param Category $category
     */
    public function removeCategory(Category $category)
    {
        $this->categories->removeElement($category);
    }

    /**
     * Get categories
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * Get required
     * @return boolean
     */
    public function getRequired()
    {
        return $this->required;
    }

    /**
     * Set required
     * @param boolean $required
     * @return Field
     */
    public function setRequired($required)
    {
        $this->required = $required;

        return $this;
    }
}
