<?php

namespace Modules\ContactBundle\Entity;

use Core\AppBundle\Entity\BlameableTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * FieldOption
 * @ORM\Table(name="mod_contact_field_option")
 * @ORM\Entity(repositoryClass="Modules\ContactBundle\Repository\FieldOptionRepository")
 */
class FieldOption
{
    use BlameableTrait;

    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(name="value", type="text", nullable=true)
     */
    private $value;

    /**
     * @var Field
     * @ORM\ManyToOne(targetEntity="Modules\ContactBundle\Entity\Field", inversedBy="fieldOptions")
     * @ORM\JoinColumn(name="field_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $field;

    /**
     * @var integer
     * @ORM\Column(name="`order`", type="integer", nullable=true)
     */
    private $order;


    /**
     * Get id
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get value
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set value
     * @param string $value
     * @return FieldOption
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get order
     * @return integer
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set order
     * @param integer $order
     * @return FieldOption
     */
    public function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get field
     * @return Field
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * Set field
     * @param Field $field
     * @return FieldOption
     */
    public function setField(Field $field = null)
    {
        $this->field = $field;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }

    /**
     * Get name
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     * @param string $name
     * @return FieldOption
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }
}
