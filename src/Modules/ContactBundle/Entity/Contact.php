<?php

namespace Modules\ContactBundle\Entity;

use Core\AppBundle\Annotation\Acl;
use Core\AppBundle\Entity\BlameableTrait;
use Core\AppBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Modules\OrderManagementBundle\Entity\PriceList;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Contact
 * @Acl("Kontakte")
 * @ORM\Table(name="mod_contact_contact")
 * @ORM\Entity(repositoryClass="Modules\ContactBundle\Repository\ContactRepository")
 * @UniqueEntity("name")
 */
class Contact implements ContactRelatedEntityInterface
{
    use BlameableTrait;

    /**
     * @var Address[]
     * @ORM\OneToMany(targetEntity="Modules\ContactBundle\Entity\Address", mappedBy="contact", cascade={"persist"})
     * @ORM\OrderBy({"order" = "ASC"})
     */
    public $addresses;
    /**
     * @var Person[]
     * @ORM\OneToMany(targetEntity="Modules\ContactBundle\Entity\Person", mappedBy="contact", cascade={"persist"})
     * @ORM\OrderBy({"order" = "ASC"})
     */
    public $persons;
    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     * @Assert\NotBlank()
     */
    private $name;
    /**
     * @var Category[]
     * @ORM\ManyToMany(targetEntity="Modules\ContactBundle\Entity\Category", inversedBy="contacts")
     * @ORM\JoinTable(name="mod_contact_contacts_categories")
     */
    private $categories;
    /**
     * @var integer
     * @ORM\Column(name="discount", type="float", nullable=true)
     */
    private $discount;
    /**
     * @var BankAccount[]
     * @ORM\OneToMany(targetEntity="Modules\ContactBundle\Entity\BankAccount", mappedBy="contact", cascade={"persist"})
     * @ORM\OrderBy({"order" = "ASC"})
     */
    private $bankAccounts;

    /**
     * @var User[]
     * @ORM\OneToMany(targetEntity="Core\AppBundle\Entity\User", mappedBy="contact", cascade={"persist"})
     * @ORM\OrderBy({"username" = "ASC"})
     */
    private $users;

    /**
     * @ORM\OneToOne(targetEntity="Modules\OrderManagementBundle\Entity\PriceList", inversedBy="contact")
     * @ORM\JoinColumn(name="pricelist_id", referencedColumnName="id")
     */
    private $pricelist;

    /**
     * @var string
     * @ORM\Column(name="email", type="string", length=100, nullable=true)
     */
    private $email;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->categories = new ArrayCollection();
        $this->addresses = new ArrayCollection();
        $this->persons = new ArrayCollection();
        $this->bankAccounts = new ArrayCollection();
        $this->users = new ArrayCollection();
    }

    /**
     * Get id
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get discount
     * @return float
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Set discount
     * @param float $discount
     * @return Contact
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Add category
     * @param Category $category
     * @return Contact
     */
    public function addCategory(Category $category)
    {
        $this->categories[] = $category;

        return $this;
    }

    /**
     * Remove category
     * @param Category $category
     */
    public function removeCategory(Category $category)
    {
        $this->categories->removeElement($category);
    }

    /**
     * Get categories
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * Add address
     * @param Address $address
     * @return Contact
     */
    public function addAddress(Address $address)
    {
        $address->setContact($this);
        $this->addresses->add($address);

        return $this;
    }

    /**
     * Remove address
     * @param Address $address
     */
    public function removeAddress(Address $address)
    {
        $this->addresses->removeElement($address);
    }

    /**
     * Get addresses
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAddresses()
    {
        return $this->addresses;
    }

    /**
     * Add person
     * @param Person $person
     * @return Contact
     */
    public function addPerson(Person $person)
    {
        $person->setContact($this);
        $this->persons->add($person);

        return $this;
    }

    /**
     * Remove person
     * @param Person $person
     */
    public function removePerson(Person $person)
    {
        $this->persons->removeElement($person);
    }

    /**
     * Get persons
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPersons()
    {
        return $this->persons;
    }

    /**
     * Add bankAccount
     * @param BankAccount $bankAccount
     * @return Contact
     */
    public function addBankAccount(BankAccount $bankAccount)
    {
        $bankAccount->setContact($this);
        $this->bankAccounts->add($bankAccount);

        return $this;
    }

    /**
     * Remove bankAccount
     * @param BankAccount $bankAccount
     */
    public function removeBankAccount(BankAccount $bankAccount)
    {
        $this->bankAccounts->removeElement($bankAccount);
    }

    /**
     * Get bankAccounts
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBankAccounts()
    {
        return $this->bankAccounts;
    }

    public function __toString()
    {
        return $this->getName();
    }

    /**
     * Get name
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     * @param string $name
     * @return Contact
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function getContact()
    {
        return $this;
    }

    /**
     * Add user
     * @param User $user
     * @return Contact
     */
    public function addUser(User $user)
    {
        $user->setContact($this);
        $this->users[] = $user;

        return $this;
    }

    /**
     * Remove user
     * @param User $user
     */
    public function removeUser(User $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * Get users
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @return mixed
     */
    public function getPricelist()
    {
        return $this->pricelist;
    }

    /**
     * @param PriceList|NULL $pricelist
     * @return Contact
     */
    public function setPricelist($pricelist)
    {
        $this->pricelist = $pricelist;

        return $this;
    }

    /**
     * @param string $email
     * @return Contact
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }
}
