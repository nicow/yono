<?php
/**
 * Created by PhpStorm.
 * User: mkt
 * Date: 31.12.16
 * Time: 04:09
 */

namespace Modules\ContactBundle\Service;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Modules\ContactBundle\Entity\Category;
use Modules\ContactBundle\Entity\Contact;
use Modules\ContactBundle\Entity\ContactRelatedEntityInterface;
use Modules\ContactBundle\Entity\Field;
use Modules\ContactBundle\Entity\FieldOption;
use Modules\ContactBundle\Entity\FieldValue;
use Symfony\Component\Debug\Exception\ClassNotFoundException;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\Session;

class ContactHelper
{
    /**
     * @var EntityManager
     */
    private $em;
    /**
     * @var Session
     */
    private $session;
    /**
     * @var RequestStack
     */
    private $requestStack;

    public function __construct(EntityManager $em, Session $session, RequestStack $requestStack)
    {
        $this->em = $em;
        $this->session = $session;
        $this->requestStack = $requestStack;
        $this->request = $this->requestStack->getCurrentRequest();
    }

    /**
     * Return a value for a custom form field for display.
     * @param Field $field
     * @param ContactRelatedEntityInterface $entity
     * @return array|bool|string
     */
    public function getFieldValue(Field $field, ContactRelatedEntityInterface $entity)
    {
        return $this->em->getRepository('ModulesContactBundle:Contact')->getFieldValue($field, $entity);
    }

    /**
     * @return Category|null
     */
    public function getCurrentCategory()
    {
        if ($this->request && $this->request->attributes->has('_route') && strpos($this->request->attributes->get('_route'), 'modules_contact') !== false && $this->request->attributes->has('category') && $this->request->attributes->get('category') instanceof Category) {
            return $this->request->attributes->get('category');
        }

        return null;
    }

    public function getContactCountForCategory(Category $category)
    {
        $contacts = $this->em->getRepository('ModulesContactBundle:Contact')->findByCategory($category);

        return count($contacts);
    }

    public function getEntitiesForContact(Contact $contact, $className)
    {
        if (!class_exists($className)) {
            throw new ClassNotFoundException(sprintf("The class %s was not found!", $className), new \ErrorException());
        }

        $repo = $this->em->getRepository($className);

        return $repo->findBy(['contact' => $contact]);
    }

    /**
     * @param FormInterface $form
     */
    public function saveFields(FormInterface $form)
    {
        $entity = $form->getData();
        /** @var Form $field */
        foreach ($form as $field) {
            // find custom fields
            if (strpos($field->getName(), '_field_entity_') !== false) {
                $fieldId = (int)str_replace('_field_entity_', '', $field->getName());
                // if field found add/update value
                if ($fieldId && $fieldEntity = $this->em->getRepository('ModulesContactBundle:Field')->find($fieldId)) {
                    // get existing values and remove them
                    $values = $this->em->getRepository('ModulesContactBundle:FieldValue')->findBy(['field' => $fieldEntity->getId(), 'entityClass' => get_class($entity), 'entityId' => $entity->getId()]);
                    if ($values) {
                        foreach ($values as $value) {
                            $this->em->remove($value);
                        }
                    }

                    // save new values
                    switch ($fieldEntity->getType()) {
                        case 'text':
                        case 'textarea':
                            if (strlen($field->getData())) {
                                $value = new FieldValue();
                                $value->setField($fieldEntity);
                                $value->setEntityClass(get_class($entity));
                                $value->setEntityId($entity->getId());
                                $value->setValueText($field->getData());
                                $this->em->persist($value);
                            }
                            break;
                        case 'select_single':
                            if ($field->getData() instanceof FieldOption) {
                                $value = new FieldValue();
                                $value->setField($fieldEntity);
                                $value->setEntityClass(get_class($entity));
                                $value->setEntityId($entity->getId());
                                $value->setValueOption($field->getData());
                                $this->em->persist($value);
                            }
                            break;
                        case 'select_multiple':
                            if ($field->getData() instanceof ArrayCollection) {
                                foreach ($field->getData() as $option) {
                                    if ($option instanceof FieldOption) {
                                        $value = new FieldValue();
                                        $value->setField($fieldEntity);
                                        $value->setEntityClass(get_class($entity));
                                        $value->setEntityId($entity->getId());
                                        $value->setValueOption($option);
                                        $this->em->persist($value);
                                    }
                                }
                            }
                            break;
                        case 'checkbox':
                            $value = new FieldValue();
                            $value->setField($fieldEntity);
                            $value->setEntityClass(get_class($entity));
                            $value->setEntityId($entity->getId());
                            $value->setValueBool($field->getData());
                            $this->em->persist($value);
                            break;
                    }

                    $this->em->flush();
                }
            }
        }
    }
}