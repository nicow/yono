<?php

namespace Modules\CalendarBundle;

use Core\YonoBundleInterface;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class ModulesCalendarBundle extends Bundle implements YonoBundleInterface
{
    /**
     * @return string
     */
    public function getDisplayName()
    {
        return 'Kalender';
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return 'Kalender/Termine';
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'calendar';
    }
}
