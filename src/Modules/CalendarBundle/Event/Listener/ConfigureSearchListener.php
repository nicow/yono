<?php
/**
 * Created by PhpStorm.
 * User: mkt
 * Date: 07.01.17
 * Time: 16:27
 */

namespace Modules\CalendarBundle\Event\Listener;


use Core\AppBundle\Service\Search;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;

class ConfigureSearchListener
{
    /**
     * @var Search
     */
    private $search;

    public function __construct(Search $search)
    {
        $this->search = $search;
    }

    public function onKernelController(FilterControllerEvent $event)
    {
        $route = $event->getRequest()->attributes->get('_route');

        if (strpos($route, 'modules_calendar_') === 0) {
            $this->search->overrideFilter(['modules_calendar_search_eventdefault', 'modules_calendar_search_eventillness', 'modules_calendar_search_eventholidays', 'modules_calendar_search_eventemergency', 'modules_calendar_search_eventexternal', 'modules_calendar_search_eventspecial',]);
        }
    }
}