<?php

namespace Modules\CalendarBundle\Entity;

use Core\AppBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;

/**
 * Event
 * @ORM\Table(name="mod_calendar_google_event")
 * @ORM\Entity(repositoryClass="Modules\CalendarBundle\Repository\GoogleEventRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class GoogleEvent
{
    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Event
     * @ORM\ManyToOne(targetEntity="Modules\CalendarBundle\Entity\Event", inversedBy="googleEvent")
     * @ORM\JoinColumn(name="event", referencedColumnName="id", onDelete="CASCADE")
     */
    private $event;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\User", inversedBy="googleEvents")
     */
    private $user;

    /**
     * @ORM\Column(name="google_event_id", type="string", nullable=true)
     */
    private $googleEventId;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return GoogleEvent
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return Event
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * @param Event $event
     * @return GoogleEvent
     */
    public function setEvent($event)
    {
        $this->event = $event;

        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return GoogleEvent
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getGoogleEventId()
    {
        return $this->googleEventId;
    }

    /**
     * @param mixed $googleEventId
     * @return GoogleEvent
     */
    public function setGoogleEventId($googleEventId)
    {
        $this->googleEventId = $googleEventId;

        return $this;
    }

}