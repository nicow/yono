<?php

namespace Modules\CalendarBundle\Entity;

use Core\AppBundle\Annotation\Acl;
use Core\AppBundle\Entity\BlameableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\Exception\InvalidArgumentException;
use Doctrine\ORM\Mapping as ORM;
use Modules\ContactBundle\Entity\Contact;
use Modules\OrderManagementBundle\Entity\JobTicket;
use Modules\ProjectBundle\Entity\Project;
use Modules\TeamBundle\Entity\Member;
use Modules\ToDoBundle\Entity\ToDo;

/**
 * Event
 * @Acl("Termine")
 * @ORM\Table(name="mod_calendar_event")
 * @ORM\Entity(repositoryClass="Modules\CalendarBundle\Repository\EventRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Event
{
    const TYPE_DEFAULT = 'default';
    const TYPE_EXTERNAL = 'external';
    const TYPE_EMERGENCY = 'emergency';
    const TYPE_HOLIDAYS = 'holidays';
    const TYPE_ILLNESS = 'illness';
    const TYPE_SPECIAL = 'special';

    const VISIBLE_PUBLIC = true;
    const VISIBLE_PRIVATE = false;

    use BlameableTrait;

    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(name="type", type="string", length=25)
     */
    private $type;

    /**
     * @var bool
     * @ORM\Column(name="allDay", type="boolean")
     */
    private $allDay;

    /**
     * @var \DateTime
     * @ORM\Column(name="start", type="datetime")
     */
    private $start;

    /**
     * @var \DateTime
     * @ORM\Column(name="end", type="datetime", nullable=true)
     */
    private $end;

    /**
     * @var string
     * @ORM\Column(name="text", type="text", nullable=true)
     */
    private $text;

    /**
     * @var Member[]
     * @ORM\ManyToMany(targetEntity="Modules\TeamBundle\Entity\Member")
     * @ORM\JoinTable(name="mod_calendar_events_members")
     */
    private $members;

    /**
     * @var Contact
     * @ORM\ManyToOne(targetEntity="Modules\ContactBundle\Entity\Contact")
     */
    private $contact;

    /**
     * @var Project
     * @ORM\ManyToOne(targetEntity="Modules\ProjectBundle\Entity\Project")
     */
    private $project;

    /**
     * @var string
     * @ORM\Column(name="address", type="string", nullable=true)
     */
    private $address;

    /**
     * @var string
     * @ORM\Column(name="color", type="string", nullable=true)
     */
    private $color;

    /**
     * @var string
     * @ORM\Column(name="google_calendar_id", type="string", nullable=true)
     */
    private $googleCalendarId;

    /**
     * @var boolean
     * @ORM\Column(name="visibility", type="boolean")
     */
    private $visibility;

    /**
     * @var string
     * @ORM\Column(name="series", type="string", nullable=true)
     */
    private $series;

    /**
     * @var JobTicket
     * @ORM\ManyToOne(targetEntity="Modules\OrderManagementBundle\Entity\JobTicket")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    public $jobticket;

    /**
     * @var ToDo
     * @ORM\ManyToOne(targetEntity="Modules\ToDoBundle\Entity\ToDo")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    public $todo;

    /**
     * @ORM\OneToMany(targetEntity="Modules\CalendarBundle\Entity\GoogleEvent", mappedBy="event")
     */
    private $googleEvent;


    public function __construct()
    {
        $this->start = new \DateTime();
        $this->roundTime($this->start);
        $this->end = new \DateTime();
        $this->roundTime($this->end);
        $this->members = new ArrayCollection();
        $this->visibility = self::VISIBLE_PRIVATE;
    }

    function roundTime(\DateTime $datetime, $precision = 30)
    {
        // 1) Set number of seconds to 0 (by rounding up to the nearest minute if necessary)
        $second = (int)$datetime->format("s");
        if ($second > 30) {
            // Jumps to the next minute
            $datetime->add(new \DateInterval("PT" . (60 - $second) . "S"));
        } elseif ($second > 0) {
            // Back to 0 seconds on current minute
            $datetime->sub(new \DateInterval("PT" . $second . "S"));
        }
        // 2) Get minute
        $minute = (int)$datetime->format("i");
        // 3) Convert modulo $precision
        $minute = $minute % $precision;
        if ($minute > 0) {
            // 4) Count minutes to next $precision-multiple minutes
            $diff = $precision - $minute;
            // 5) Add the difference to the original date time
            $datetime->add(new \DateInterval("PT" . $diff . "M"));
        }
    }

    /**
     * Get id
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     * @param string $name
     * @return Event
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get type
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set type
     * @param string $type
     * @return Event
     * @throws InvalidArgumentException
     */
    public function setType($type)
    {
        if (in_array($type, [self::TYPE_DEFAULT, self::TYPE_EXTERNAL, self::TYPE_EMERGENCY, self::TYPE_HOLIDAYS, self::TYPE_ILLNESS,])) {
            $this->type = $type;

            return $this;
        }

        throw new InvalidArgumentException('Invalid event type.');
    }

    /**
     * Get allDay
     * @return bool
     */
    public function getAllDay()
    {
        return $this->allDay;
    }

    /**
     * Set allDay
     * @param boolean $allDay
     * @return Event
     */
    public function setAllDay($allDay)
    {
        $this->allDay = $allDay;

        return $this;
    }

    /**
     * Get start
     * @return \DateTime
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Set start
     * @param \DateTime $start
     * @return Event
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * Get end
     * @return \DateTime
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * Set end
     * @param \DateTime $end
     * @return Event
     */
    public function setEnd($end)
    {
        $this->end = $end;

        return $this;
    }

    /**
     * Get text
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set text
     * @param string $text
     * @return Event
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get contact
     * @return Contact
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set contact
     * @param Contact $contact
     * @return Event
     */
    public function setContact(Contact $contact = null)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get project
     * @return Project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Set project
     * @param Project $project
     * @return Event
     */
    public function setProject(Project $project = null)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get address
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set address
     * @param string $address
     * @return Event
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get color
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set color
     * @param string $color
     * @return Event
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get googleCalendarId
     * @return string
     */
    public function getGoogleCalendarId()
    {
        return $this->googleCalendarId;
    }

    /**
     * Set googleCalendarId
     * @param string $googleCalendarId
     * @return Event
     */
    public function setGoogleCalendarId($googleCalendarId)
    {
        $this->googleCalendarId = $googleCalendarId;

        return $this;
    }

    /**
     * Add member
     * @param Member $member
     * @return Event
     */
    public function addMember(Member $member)
    {
        $this->members[] = $member;

        return $this;
    }

    /**
     * Remove member
     * @param Member $member
     */
    public function removeMember(Member $member)
    {
        $this->members->removeElement($member);
    }

    /**
     * Get members
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMembers()
    {
        return $this->members;
    }

    /**
     * @return boolean
     */
    public function getVisibility()
    {
        return $this->visibility;
    }

    /**
     * @param integer $visibility
     * @return Event
     */
    public function setVisibility($visibility)
    {
        $this->visibility = $visibility;

        return $this;
    }

    /**
     * @return string
     */
    public function getSeries()
    {
        return $this->series;
    }

    /**
     * @param string $series
     */
    public function setSeries($series)
    {
        $this->series = $series;
    }

    /**
     * @return mixed
     */
    public function getGoogleEvent()
    {
        return $this->googleEvent;
    }

    /**
     * @param mixed $googleEvent
     * @return Event
     */
    public function setGoogleEvent($googleEvent)
    {
        $this->googleEvent = $googleEvent;

        return $this;
    }

    /**
     * @param mixed $jobticket
     * @return Event
     */
    public function setJobticket($jobticket)
    {
        $this->jobticket = $jobticket;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getJobticket()
    {
        return $this->jobticket;
    }

    /**
     * @param mixed $todo
     * @return Event
     */
    public function setTodo($todo)
    {
        $this->todo = $todo;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTodo()
    {
        return $this->todo;
    }
}
