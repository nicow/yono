<?php
/**
 * Created by PhpStorm.
 * User: mkt
 * Date: 30.01.17
 * Time: 17:04
 */

namespace Modules\CalendarBundle\Service;


use Core\AppBundle\Service\Settings;
use Doctrine\ORM\EntityManager;
use Modules\CalendarBundle\Entity\Event;
use Modules\InventoryBundle\Entity\ItemUse;
use Modules\TeamBundle\Entity\Plan;
use Symfony\Component\HttpFoundation\RequestStack;

class CalendarHelper
{
    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var Settings
     */
    private $settings;

    /**
     * @var string
     */
    private $defaultColor;

    public function __construct(RequestStack $requestStack, EntityManager $em, Settings $settings, $defaultColor)
    {
        $this->requestStack = $requestStack;
        $this->em = $em;
        $this->settings = $settings;
        $this->defaultColor = $defaultColor;
    }

    public function setMemberFilter()
    {
        $allMembers = $this->em->getRepository('ModulesTeamBundle:Member')->findAllByPosition();

        // handle form
        $request = $this->requestStack->getCurrentRequest();
        $settingsKey = 'mod_calendar_team_member_filter';
        if ($request && $request->request->has($settingsKey)) {
            $this->settings->set($settingsKey, $request->request->get($settingsKey));
        }

        $memberIds = $this->settings->get($settingsKey, []);
        $memberFilter = [];
        foreach ($allMembers as $member) {
            $memberFilter[$member->getId()] = ['member' => $member, 'active' => !count($memberIds) || in_array($member->getId(), $memberIds) // set member active if no filter or member in filter
            ];
        }

        return $memberFilter;
    }

    public function setInventoryFilter()
    {
        // handle form
        $request = $this->requestStack->getCurrentRequest();
        $settingsKey = 'mod_calendar_inventory_item_use_filter';
        if ($request && $request->request->has($settingsKey)) {
            $this->settings->set($settingsKey, $request->request->get($settingsKey));
        }

        $inventoryFilter = $this->settings->get($settingsKey, [ItemUse::STATUS_IN_USE => 1, ItemUse::STATUS_IN_REPAIR => 1]);

        return $inventoryFilter;
    }

    public function setPlanFilter()
    {
        // handle form
        $request = $this->requestStack->getCurrentRequest();
        $settingsKey = 'mod_calendar_team_plan_filter';
        if ($request && $request->request->has($settingsKey)) {
            $this->settings->set($settingsKey, $request->request->get($settingsKey));
        }

        $planFilter = $this->settings->get($settingsKey, [Plan::TYPE_HOLIDAY => 1, Plan::TYPE_ILLNESS => 1, Plan::TYPE_SCHOOL => 1]);

        return $planFilter;
    }

    public function displayDateTimeRange(\DateTime $dateTime1, \DateTime $dateTime2 = null, \DateTime $refDate = null)
    {
        if ($refDate->format('Ymd') == $dateTime1->format('Ymd')) {
            $string = $dateTime1->format('H:i');
        } else {
            $string = $dateTime1->format('d.m. H:i');
        }


        if ($dateTime2) {
            if ($dateTime1->format('Ymd') == $dateTime2->format('Ymd')) {
                if ($dateTime1->format('YmdHi') != $dateTime2->format('YmdHi')) {
                    $string .= ' - ' . $dateTime2->format('H:i');
                }
            } else {
                $string .= ' - ' . $dateTime2->format('d.m. H:i');
            }
        }

        return $string;
    }

    public function getEventColor(Event $event)
    {
        if ($event->getType() == Event::TYPE_EMERGENCY) {
            return '#cc0000';
        } elseif ($event->getType() == Event::TYPE_HOLIDAYS) {
            return '#00cc00';
        } elseif ($event->getType() == Event::TYPE_ILLNESS) {
            return '#cccc00';
        } elseif ($event->getMembers()->count() == 1 && $event->getMembers()->first()->getColor()) {
            return $event->getMembers()->first()->getColor();
        }

        return $this->defaultColor;
    }

    public function getEventIcon(Event $event)
    {
        if ($event->getType() == Event::TYPE_EMERGENCY) {
            return 'warning';
        } elseif ($event->getType() == Event::TYPE_HOLIDAYS) {
            return 'plane';
        } elseif ($event->getType() == Event::TYPE_ILLNESS) {
            return 'ambulance';
        } elseif ($event->getType() == Event::TYPE_EXTERNAL) {
            return 'truck';
        } else {
            return 'calendar';
        }
    }
}