<?php
/**
 * Created by PhpStorm.
 * User: mkt
 * Date: 04.01.17
 * Time: 20:54
 */

namespace Modules\CalendarBundle\Search;

use Core\AppBundle\Search\SearchItem;
use Doctrine\ORM\EntityManager;
use Modules\CalendarBundle\Entity\Event;
use Symfony\Bridge\Twig\TwigEngine;
use Symfony\Bundle\FrameworkBundle\Routing\Router;

class EventEmergency extends SearchItem
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var TwigEngine
     */
    private $twig;

    /**
     * @var Router
     */
    private $router;

    public function __construct(EntityManager $em, TwigEngine $twig, Router $router)
    {
        $this->em = $em;
        $this->twig = $twig;
        $this->router = $router;
    }

    /**
     * @param $query
     * @return array
     */
    public function getResults($query)
    {
        $events = $this->em->getRepository('ModulesCalendarBundle:Event')->createQueryBuilder('e')->where('e.name LIKE :query')->andWhere('e.type = :type')->setParameter('query', '%' . $query . '%')->setParameter('type', Event::TYPE_EMERGENCY)->getQuery()->getResult();

        $results = [];
        if ($events) {
            foreach ($events as $event) {
                $results[] = $this->twig->render('@ModulesCalendar/Search/event.html.twig', ['event' => $event]);
            }
        }

        return $results;
    }

    /**
     * @param $query
     * @return array
     */
    public function getAutocompleteResults($query)
    {
        $events = $this->em->getRepository('ModulesCalendarBundle:Event')->createQueryBuilder('e')->where('e.name LIKE :query')->andWhere('e.type = :type')->setParameter('query', '%' . $query . '%')->setParameter('type', Event::TYPE_EMERGENCY)->getQuery()->getResult();

        $results = [];
        if ($events) {
            /** @var \Modules\CalendarBundle\Entity\Event $event */
            foreach ($events as $event) {
                $results[] = ['icon' => $this->getIcon(), 'title' => $event->getName(), 'url' => $this->router->generate('modules_calendar_default_index_redirect', ['redirectDate' => $event->getStart()->format('Y-m-d')]), 'text' => strip_tags($event->getText())];
            }
        }

        return $results;
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'warning';
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'Notdienst';
    }

    public function getRequiredPermissions()
    {
        return ['VIEW' => 'Modules\\CalendarBundle\\Entity\\Event'];
    }
}