<?php
/**
 * Created by PhpStorm.
 * User: mkt
 * Date: 25.01.17
 * Time: 17:00
 */

namespace Modules\CalendarBundle\Command;


use Core\AppBundle\Service\Google;
use Core\AppBundle\Service\Settings;
use Doctrine\ORM\EntityManager;
use Google_Service_Calendar_Event;
use Modules\CalendarBundle\Entity\Event;
use Modules\CalendarBundle\Entity\GoogleEvent;
use Modules\ContactBundle\Entity\Person;
use Modules\TeamBundle\Entity\GooglePlan;
use Modules\TeamBundle\Entity\Member;
use Modules\TeamBundle\Entity\Plan;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use ColourStream\Bundle\CronBundle\Annotation\CronJob;

/**
 * Class GoogleCalendarUpdatePrivateCommand
 * @package Modules\CalendarBundle\Command
 * @CronJob(interval="PT5M")
 */
class GoogleCalendarUpdatePrivateCommand extends ContainerAwareCommand
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var Settings
     */
    private $settings;

    /**
     * @var Google
     */
    private $google;

    /**
     * @var \Google_Service_Calendar
     */
    private $googleCalendar;

    protected function configure()
    {
        $this->setName('yono:calendar:google:update:private')->setDescription('Updates each the google team calendar with private events.');
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $this->settings = $this->getContainer()->get('app.settings');

        $this->google = $this->getContainer()->get('app.google');
        $this->googleCalendar = new \Google_Service_Calendar($this->google->getClient());

        $output->writeln('Run on ' . date('Y-m-d H:i:s'));
        $lastRun = $this->settings->getGlobal('mod_calendar_google_update_last_run');
        $output->writeln('Last Run on ' . $lastRun);
        $lastRunDateTime = null;
        if ($lastRun) {
            $lastRunDateTime = new \DateTime($lastRun);
        } else {
            $lastRunDateTime = new \DateTime('1970-01-01');
        }

        $googleEventRepo = $this->em->getRepository('ModulesCalendarBundle:GoogleEvent');
        $events = $googleEventRepo->findNewEventsForUsers($lastRunDateTime);

        $googlePlanRepo = $this->em->getRepository('ModulesTeamBundle:GooglePlan');
        $plans = $googlePlanRepo->findNewEventsForUsers($lastRunDateTime);

        $output->writeln('Events found: ' . count($events));
        $output->writeln('Plans found: ' . count($plans));
        $updated = $inserted = 0;


        /**
         * @var GooglePlan $entry
         */
        foreach ($plans as $entry) {

            $this->google = $this->getContainer()->get('app.google');
            $this->googleCalendar = new \Google_Service_Calendar($this->google->getClient());

            $plan = $entry->getPlan();
            if ($entry->getUser()) {
                try {
                    $this->google->getClient()->fetchAccessTokenWithRefreshToken($entry->getUser()->getGoogleRefreshToken());


                    if ($plan->getType() == Plan::TYPE_HOLIDAY) {
                        $description = "Urlaub";
                    } elseif ($plan->getType() == Plan::TYPE_SPECIAL) {
                        $description = "Sonderurlaub";
                    } elseif ($plan->getType() == Plan::TYPE_ILLNESS) {
                        $description = "Krankheit";
                    } elseif ($plan->getType() == Plan::TYPE_SCHOOL) {
                        $description = "Berufsschule";
                    } else {
                        $description = "Unbekannte Einsatzplanung";
                    }


                    $who = $plan->getMember()->getFullName();


                    $options = ['summary' => $description, 'description' => $description . " - " . $who . "\n\n" . $plan->getNote(), 'start' => ['date' => $plan->getDay()->format('Y-m-d')], 'end' => ['date' => $plan->getDay()->format('Y-m-d')], 'guestCanModify' => 'false', 'guestsCanInviteOthers' => 'false', 'guestsCanSeeOtherGuests' => 'true', 'visibility' => 'private'];

                    $googleCalendarEvent = new Google_Service_Calendar_Event($options);

                    $oldGoogleCalendarEvent = null;


                    if ($entry->getGoogleEventId()) {
                        try {
                            $oldGoogleCalendarEvent = $this->googleCalendar->events->get($entry->getUser()->getGoogleCalendarId(), $entry->getGoogleEventId());
                        } catch (\Exception $exception) {

                        }
                    }

                    if ($oldGoogleCalendarEvent && $oldGoogleCalendarEvent instanceof Google_Service_Calendar_Event) {
                        // update
                        try {
                            $this->googleCalendar->events->patch($entry->getUser()->getGoogleCalendarId(), $entry->getGoogleEventId(), $googleCalendarEvent);
                            $updated++;
                        } catch (\Exception $exception) {

                        }
                    } else {
                        // add
                        try {
                            $googleCalendarEvent = $this->googleCalendar->events->insert($entry->getUser()->getGoogleCalendarId(), $googleCalendarEvent);
                            $entry->setGoogleEventId($googleCalendarEvent->getId());
                            $inserted++;
                        } catch (\Exception $exception) {

                        }
                    }

                    $this->em->flush();
                } catch (\Exception $exception) {

                }
            }
        }

        $output->writeln('');
        $output->writeln("Plans overview:");
        $output->writeln('Updated: ' . $updated);
        $output->writeln('Inserted: ' . $inserted);
        $output->writeln('');

        $updated = $inserted = 0;


        foreach ($events as $entry) {

            $this->google = $this->getContainer()->get('app.google');
            $this->googleCalendar = new \Google_Service_Calendar($this->google->getClient());

            if ($entry->getUser()) {
                try {
                    $accessToken = $this->google->getClient()->fetchAccessTokenWithRefreshToken($entry->getUser()->getGoogleRefreshToken());

                    if (!array_key_exists('error', $accessToken)) {

                        /**
                         * @var GoogleEvent $entry ;
                         */
                        $event = $entry->getEvent();

                        $description = strip_tags($event->getText());
                        if ($event->getProject()) {
                            $description = 'Projekt: ' . $event->getProject()->getName() . "\n\n" . $description;
                        }
                        if ($event->getContact()) {
                            $description = 'Kontakt: ' . $event->getContact()->getName() . "\n\n" . $description;
                            if ($event->getContact()->getPersons()->count() > 0) {
                                /** @var Person $person */
                                $person = $event->getContact()->getPersons()->first();
                                $personText = $person . ' ' . implode(' ', [$person->getPhone(), $person->getMobile(), $person->getEmail()]);
                                $description = 'Ansprechpartner: ' . $personText . "\n" . $description;
                            }
                        }
                        if ($event->getType() == Event::TYPE_EMERGENCY) {
                            $name = "Notdienst";

                            if ($description != '') {
                                $description = "\n\n";
                            }

                            $description .= "Mitarbeiter:\n\n";

                            for ($i = 0; $i < $event->getMembers()->count(); ++$i) {
                                $description .= $event->getMembers()->get($i)->getFullName() . "\n";
                            }

                            $description .= "\n\n" . strip_tags($event->getText());
                        } else {
                            $name = $event->getName();
                        }


                        if ($event->getAllDay()) {
                            $start = ['date' => $event->getStart()->format('Y-m-d')];
                            if ($event->getEnd() == null) {
                                $end = ['date' => $event->getStart()->format('Y-m-d')];
                            } else {
                                $end = ['date' => $event->getEnd()->format('Y-m-d')];
                            }

                        } else {
                            $start = ['dateTime' => $event->getStart()->format(\DateTime::RFC3339)];
                            if ($event->getEnd() == null) {
                                $end = ['dateTime' => $event->getStart()->format(\DateTime::RFC3339)];
                            } else {
                                $end = ['dateTime' => $event->getEnd()->format(\DateTime::RFC3339)];
                            }

                        }

                        if ($event->getJobticket()) {
                            $description.="\n\n Jobticket: " . $event->getJobticket()->getTitle() . "\n\n" . strip_tags($event->getJobticket()->getDescription());
                        }

                        if ($event->getTodo()) {
                            $description.="\n\n ToDo: " . $event->getTodo()->getTitle() . "\n\n" . strip_tags($event->getTodo()->getText());
                        }

                        $options = ['summary' => $name, 'location' => $event->getAddress(), 'description' => $description, 'start' => $start, 'end' => $end, 'guestCanModify' => 'false', 'guestsCanInviteOthers' => 'false', 'guestsCanSeeOtherGuests' => 'true', 'visibility' => 'private'];
                        $googleCalendarEvent = new Google_Service_Calendar_Event($options);

                        if ($event->getType() == Event::TYPE_EMERGENCY) {
                            $googleCalendarEvent->setColorId(11);
                        }

                        /**
                         * @var Member $member
                         */

                        $oldGoogleCalendarEvent = null;
                        if ($entry->getGoogleEventId()) {
                            try {

                                $oldGoogleCalendarEvent = $this->googleCalendar->events->get($entry->getUser()->getGoogleCalendarId(), $entry->getGoogleEventId());
                            } catch (\Exception $exception) {

                            }
                        }

                        if ($oldGoogleCalendarEvent && $oldGoogleCalendarEvent instanceof Google_Service_Calendar_Event) {
                            // update
                            try {
                                $this->googleCalendar->events->patch($entry->getUser()->getGoogleCalendarId(), $entry->getGoogleEventId(), $googleCalendarEvent);
                                $updated++;
                            } catch (\Exception $exception) {

                            }
                        } else {
                            // add
                            try {
                                $googleCalendarEvent = $this->googleCalendar->events->insert($entry->getUser()->getGoogleCalendarId(), $googleCalendarEvent);
                                $entry->setGoogleEventId($googleCalendarEvent->getId());
                                $inserted++;
                            } catch (\Exception $exception) {

                            }
                        }
                    }
                } catch (\Exception $exception) {

                }
            }


        }

        $output->writeln('');
        $output->writeln("Events overview:");
        $output->writeln('Updated: ' . $updated);
        $output->writeln('Inserted: ' . $inserted);
        $output->writeln('');

        $newRun = new \DateTime();
        $this->settings->setGlobal('mod_calendar_google_update_last_run', $newRun->format('d-m-Y H:i:s'));

        return 0;

    }
}