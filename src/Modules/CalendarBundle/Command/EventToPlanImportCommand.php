<?php
/**
 * Created by PhpStorm.
 * User: nicow
 * Date: 21.03.17.
 * Time: 17:00
 */

namespace Modules\CalendarBundle\Command;


use Doctrine\ORM\EntityManager;
use Modules\CalendarBundle\Entity\Event;
use Modules\TeamBundle\Entity\Plan;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class EventToPlanImportCommand extends ContainerAwareCommand
{
    /**
     * @var EntityManager
     */
    private $em;

    protected function configure()
    {
        $this->setName('yono:calendar:import:plan:from:events')->setDescription('Create Plan Entities from Calendar Events');
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->em = $this->getContainer()->get('doctrine.orm.entity_manager');

    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $eventRepo = $this->em->getRepository("ModulesCalendarBundle:Event");


        $qb = $eventRepo->createQueryBuilder('e');

        $result = $qb->where("e.type = :illness")->orWhere("e.type = :holiday")->orWhere("e.type = :special")->setParameters(['illness' => Event::TYPE_ILLNESS, 'holiday' => Event::TYPE_HOLIDAYS, 'special' => Event::TYPE_SPECIAL])->getQuery()->getResult();

        /**
         * @var Event $event
         */
        foreach ($result as $event) {

            $members = $event->getMembers();

            if ($members->count() > 0) {
                $member = $members->first();
                $name = $member->getFullName();
            } else {
                $member = null;
                $name = "all";
            }

            $days = $this->getContainer()->get('mod.team.helper')->getDaysByDateRange($event->getStart(), $event->getEnd());

            foreach ($days as $day) {
                $plan = new Plan();
                $plan->setType($event->getType())->setDay($day)->setMember($member)->setDaySection(Plan::DAY_SECTION_FULLTIME);

                $this->em->persist($plan);

                $type = $event->getType();
                $output->writeln("Create Plan \"" . $type . "\" for " . $name);
            }

            $this->em->remove($event);
            $this->em->flush();

        }

    }
}