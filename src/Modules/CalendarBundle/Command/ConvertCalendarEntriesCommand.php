<?php
/**
 * Created by PhpStorm.
 * User: nicow
 * Date: 21.03.17.
 * Time: 17:00
 */

namespace Modules\CalendarBundle\Command;


use Doctrine\ORM\EntityManager;
use Modules\CalendarBundle\Entity\GoogleEvent;
use Modules\TeamBundle\Entity\GooglePlan;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ConvertCalendarEntriesCommand extends ContainerAwareCommand
{
    /**
     * @var EntityManager
     */
    private $em;

    protected function configure()
    {
        $this->setName('yono:calendar:convert')->addOption('sure')->setDescription('Clear the primary calendar');
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->em = $this->getContainer()->get('doctrine.orm.entity_manager');

    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $events = $this->em->getRepository('ModulesCalendarBundle:Event')->findAll();
        $plans = $this->em->getRepository('ModulesTeamBundle:Plan')->findAll();

        foreach ($events as $event) {
            foreach ($event->getMembers() as $member) {
                $ge = new GoogleEvent();
                $ge->setEvent($event);
                $ge->setUser($member->getUser() ?: null);
                $this->em->persist($ge);
            }
            $this->em->flush();
        }

        foreach ($plans as $plan) {
            $gp = new GooglePlan();
            $gp->setPlan($plan);
            if ($plan->getMember()) {
                $gp->setUser($plan->getMember()->getUser() ?: null);
            } else {
                $gp->setUser(null);
            }
            $this->em->persist($gp);
            $this->em->flush();
        }


    }
}