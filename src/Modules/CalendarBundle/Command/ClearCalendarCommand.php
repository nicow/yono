<?php
/**
 * Created by PhpStorm.
 * User: nicow
 * Date: 21.03.17.
 * Time: 17:00
 */

namespace Modules\CalendarBundle\Command;


use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ClearCalendarCommand extends ContainerAwareCommand
{
    /**
     * @var EntityManager
     */
    private $em;

    protected function configure()
    {
        $this->setName('yono:calendar:clear')->addOption('sure')->setDescription('Clear the primary calendar');
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->em = $this->getContainer()->get('doctrine.orm.entity_manager');

    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if ($input->getOption('sure') != 1) {
            $output->writeln("<question>Please use the --sure option to ensure you really want to clear all events from the primary calendar!</question> <error>This can't be undone!</error>");
        } else {
            $output->writeln("<comment>Start removing all Events from the calendar!</comment>");
            $settings = $this->getContainer()->get('app.settings');
            $google = $this->getContainer()->get('app.google');
            $googleCalendar = new \Google_Service_Calendar($google->getClient());
            $googleRefreshToken = $settings->getGlobal('mod_calendar_google_refresh_token');
            $google->getClient()->fetchAccessTokenWithRefreshToken($googleRefreshToken);
            $googleCalendarId = $settings->getGlobal('mod_calendar_google_calendar_id');

            $calendars = $googleCalendar->calendarList->listCalendarList();

            $id = null;

            foreach ($calendars as $calendar) {
                if ($calendar['summary'] == 'KM - Allgemein') {
                    $id = $calendar['id'];
                }
            }

            if ($id) {
                $calendar = new \Google_Service_Calendar_Calendar();
                $calendar->setSummary("KM - Allgemein");
                $calendar->setTimeZone("Europe/Berlin");

                $googleCalendar->calendars->delete($googleCalendarId);
                $newCalendar = $googleCalendar->calendars->insert($calendar);
                $settings->setGlobal('mod_calendar_google_calendar_id', $newCalendar->getId());

                if ($id = $newCalendar->getId()) {
                    $output->writeln("Kalender angelegt! ID: " . $id);
                }
            }

        }

    }
}