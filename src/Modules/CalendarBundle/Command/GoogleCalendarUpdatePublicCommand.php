<?php
/**
 * Created by PhpStorm.
 * User: mkt
 * Date: 25.01.17
 * Time: 17:00
 */

namespace Modules\CalendarBundle\Command;


use Core\AppBundle\Service\Google;
use Core\AppBundle\Service\Settings;
use Doctrine\ORM\EntityManager;
use Google_Service_Calendar_Event;
use Modules\CalendarBundle\Entity\Event;
use Modules\CalendarBundle\Entity\GoogleEvent;
use Modules\ContactBundle\Entity\Person;
use Modules\TeamBundle\Entity\GooglePlan;
use Modules\TeamBundle\Entity\Member;
use Modules\TeamBundle\Entity\Plan;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use ColourStream\Bundle\CronBundle\Annotation\CronJob;

/**
 * Class GoogleCalendarUpdatePublicCommand
 * @package Modules\CalendarBundle\Command
 * @CronJob(interval="PT5M")
 */
class GoogleCalendarUpdatePublicCommand extends ContainerAwareCommand
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var Settings
     */
    private $settings;

    /**
     * @var Google
     */
    private $google;

    /**
     * @var \Google_Service_Calendar
     */
    private $googleCalendar;

    protected function configure()
    {
        $this->setName('yono:calendar:google:update:public')->setDescription('Updates the team calendar with public events.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $this->em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $this->settings = $this->getContainer()->get('app.settings');

        $this->google = $this->getContainer()->get('app.google');
        $this->googleCalendar = new \Google_Service_Calendar($this->google->getClient());

        $output->writeln('Run on ' . date('Y-m-d H:i:s'));
        $lastRun = $this->settings->getGlobal('mod_calendar_google_update_last_run_public');
        $output->writeln('Last Run on ' . $lastRun);
        $lastRunDateTime = null;
        if ($lastRun) {
            $lastRunDateTime = new \DateTime($lastRun);
        } else {
            $lastRunDateTime = new \DateTime('1970-01-01');
        }

        $googleEventRepo = $this->em->getRepository('ModulesCalendarBundle:GoogleEvent');
        $events = $googleEventRepo->findNewAndUpdatedPublicEvents($lastRunDateTime);

        $googlePlanRepo = $this->em->getRepository('ModulesTeamBundle:GooglePlan');
        $plans = $googlePlanRepo->findNewAndUpdatedPlans($lastRunDateTime);

        $output->writeln('Events found: ' . count($events));
        $output->writeln('Plans found: ' . count($plans));
        $updated = $inserted = 0;

        $this->google = $this->getContainer()->get('app.google');
        $settings = $this->getContainer()->get('app.settings');
        $this->googleCalendar = new \Google_Service_Calendar($this->google->getClient());

        $googleEmail = $settings->getGlobal('mod_calendar_google_email');
        $googleCalendarId = $settings->getGlobal('mod_calendar_google_calendar_id');
        $googleRefreshToken = $settings->getGlobal('mod_calendar_google_refresh_token');

        $accessToken = $this->google->getClient()->fetchAccessTokenWithRefreshToken($googleRefreshToken);


        /**
         * @var GooglePlan $entry
         */
        foreach ($plans as $entry) {

            $plan = $entry->getPlan();
            $this->google->getClient()->fetchAccessTokenWithRefreshToken($googleRefreshToken);

            if ($plan->getType() == Plan::TYPE_HOLIDAY) {
                $description = "Urlaub";
            } elseif ($plan->getType() == Plan::TYPE_SPECIAL) {
                $description = "Sonderurlaub";
            } elseif ($plan->getType() == Plan::TYPE_ILLNESS) {
                $description = "Krankheit";
            } elseif ($plan->getType() == Plan::TYPE_SCHOOL) {
                $description = "Berufsschule";
            } else {
                $description = "Unbekannte Einsatzplanung";
            }


            $who = 'Alle';


            $options = ['summary' => $description, 'description' => $description . " - " . $who . "\n\n" . $plan->getNote(), 'start' => ['date' => $plan->getDay()->format('Y-m-d')], 'end' => ['date' => $plan->getDay()->format('Y-m-d')], 'guestCanModify' => 'false', 'guestsCanInviteOthers' => 'false', 'guestsCanSeeOtherGuests' => 'true', 'visibility' => 'public'];

            $googleCalendarEvent = new Google_Service_Calendar_Event($options);

            $oldGoogleCalendarEvent = null;


            if ($entry->getGoogleEventId()) {
                $oldGoogleCalendarEvent = $this->googleCalendar->events->get($googleCalendarId, $entry->getGoogleEventId());
            }

            if ($oldGoogleCalendarEvent && $oldGoogleCalendarEvent instanceof Google_Service_Calendar_Event) {
                // update
                $this->googleCalendar->events->patch($googleCalendarId, $entry->getGoogleEventId(), $googleCalendarEvent);
                $updated++;
            } else {
                // add
                $googleCalendarEvent = $this->googleCalendar->events->insert($googleCalendarId, $googleCalendarEvent);
                $entry->setGoogleEventId($googleCalendarEvent->getId());
                $inserted++;
            }

            $this->em->flush();
        }

        $output->writeln('');
        $output->writeln("Plans overview:");
        $output->writeln('Updated: ' . $updated);
        $output->writeln('Inserted: ' . $inserted);
        $output->writeln('');

        $updated = $inserted = 0;


        foreach ($events as $entry) {

            if (!array_key_exists('error', $accessToken)) {
                /**
                 * @var GoogleEvent $entry ;
                 */
                $event = $entry->getEvent();

                $description = strip_tags($event->getText());
                if ($event->getProject()) {
                    $description = 'Projekt: ' . $event->getProject()->getName() . "\n\n" . $description;
                }
                if ($event->getContact()) {
                    $description = 'Kontakt: ' . $event->getContact()->getName() . "\n\n" . $description;
                    if ($event->getContact()->getPersons()->count() > 0) {
                        /** @var Person $person */
                        $person = $event->getContact()->getPersons()->first();
                        $personText = $person . ' ' . implode(' ', [$person->getPhone(), $person->getMobile(), $person->getEmail()]);
                        $description = 'Ansprechpartner: ' . $personText . "\n" . $description;
                    }
                }
                if ($event->getType() == Event::TYPE_EMERGENCY) {
                    $name = "Notdienst - ";

                    if ($description != '') {
                        $description = "\n\n";
                    }

                    $description .= "Mitarbeiter:\n\n";

                    for ($i = 0; $i < $event->getMembers()->count(); ++$i) {
                        $description .= $event->getMembers()->get($i)->getFullName() . "\n";
                        $name .= $event->getMembers()->get($i)->getFullName() . ' ';
                    }

                    $description .= "\n\n" . strip_tags($event->getText());
                } else {
                    $name = $event->getName();
                }


                if ($event->getAllDay()) {
                    $start = ['date' => $event->getStart()->format('Y-m-d')];
                    if ($event->getEnd() == null) {
                        $end = ['date' => $event->getStart()->format('Y-m-d')];
                    } else {
                        $end = ['date' => $event->getEnd()->format('Y-m-d')];
                    }

                } else {
                    $start = ['dateTime' => $event->getStart()->format(\DateTime::RFC3339)];
                    if ($event->getEnd() == null) {
                        $end = ['dateTime' => $event->getStart()->format(\DateTime::RFC3339)];
                    } else {
                        $end = ['dateTime' => $event->getEnd()->format(\DateTime::RFC3339)];
                    }

                }

                if ($event->getJobticket()) {
                    $description.="\n\n Jobticket: " . $event->getJobticket()->getTitle() . "\n\n" . strip_tags($event->getJobticket()->getDescription());
                }

                if ($event->getTodo()) {
                    $description.="\n\n ToDo: " . $event->getTodo()->getTitle() . "\n\n" . strip_tags($event->getTodo()->getText());
                }

                $options = ['summary' => $name, 'location' => $event->getAddress(), 'description' => $description, 'start' => $start, 'end' => $end, 'guestCanModify' => 'false', 'guestsCanInviteOthers' => 'false', 'guestsCanSeeOtherGuests' => 'true', 'visibility' => 'public'];
                $googleCalendarEvent = new Google_Service_Calendar_Event($options);

                if ($event->getType() == Event::TYPE_EMERGENCY) {
                    $googleCalendarEvent->setColorId(11);
                }

                /**
                 * @var Member $member
                 */

                $oldGoogleCalendarEvent = null;
                if ($entry->getGoogleEventId()) {
                    try {
                        $oldGoogleCalendarEvent = $this->googleCalendar->events->get($googleCalendarId, $entry->getGoogleEventId());
                    } catch (\Exception $exception) {

                    }
                }

                if ($oldGoogleCalendarEvent && $oldGoogleCalendarEvent instanceof Google_Service_Calendar_Event) {
                    // update
                    try {
                        $this->googleCalendar->events->patch($googleCalendarId, $entry->getGoogleEventId(), $googleCalendarEvent);
                        $updated++;
                    } catch (\Exception $exception) {

                    }
                } else {
                    // add
                    try {
                        $googleCalendarEvent = $this->googleCalendar->events->insert($googleCalendarId, $googleCalendarEvent);
                        $entry->setGoogleEventId($googleCalendarEvent->getId());
                        $inserted++;
                    } catch (\Exception $exception) {

                    }
                }
            }

        }

        $output->writeln('');
        $output->writeln("Events overview:");
        $output->writeln('Updated: ' . $updated);
        $output->writeln('Inserted: ' . $inserted);
        $output->writeln('');

        $newRun = new \DateTime();
        $this->settings->setGlobal('mod_calendar_google_update_last_run_public', $newRun->format('d-m-Y H:i:s'));
        return 0;
    }
}