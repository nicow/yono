<?php

namespace Modules\CalendarBundle\Controller;

use Modules\CalendarBundle\Entity\Event;
use Modules\CalendarBundle\Entity\GoogleEvent;
use Modules\CalendarBundle\Form\EventType;
use Modules\TeamBundle\Entity\Member;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Event controller.
 * @Route("event")
 */
class EventController extends Controller
{
    /**
     * Creates a new event entity (or copies an existing one).
     * @Route("/new/{type}")
     * @Route("/new/copy/{baseEvent}", name="modules_calendar_event_new_copy")
     * @Route("/new/date/{date}/{member}" , name="modules_calendar_event_new_date", options={"expose"=true})
     * @Method({"GET", "POST"})
     * @Security("is_granted('CREATE', 'Modules\\CalendarBundle\\Entity\\Event')")
     * @param Request $request
     * @param string $type
     * @param Event $baseEvent
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request, $type = Event::TYPE_DEFAULT, Event $baseEvent = null, $date = null, $member = null)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if ($baseEvent && $request->attributes->get('_route') == 'modules_calendar_event_new_copy') {

            $event = new Event();
            $event->setAddress($baseEvent->getAddress());
            $event->setAllDay($baseEvent->getAllDay());
            $event->setColor($baseEvent->getColor());
            $event->setContact($baseEvent->getContact());
            $event->setEnd($baseEvent->getEnd());
            $event->setName($baseEvent->getName());
            $event->setProject($baseEvent->getProject());
            $event->setSeries($baseEvent->getSeries());
            $event->setStart($baseEvent->getStart());
            $event->setText($baseEvent->getText());
            $event->setType($baseEvent->getType());
            $event->setVisibility($baseEvent->getVisibility());

        } else {
            $event = new Event();
            $event->setType($type);
            if ($event->getType() == Event::TYPE_EMERGENCY) {
                $event->setVisibility(Event::VISIBLE_PUBLIC);
            }
            if ($member) {
                $setMember = $em->getRepository('ModulesTeamBundle:Member')->find($member);
                $event->addMember($setMember);
            } elseif ($this->getUser()->getMember()) {
                $event->addMember($this->getUser()->getMember());
            }
        }

        $form = $this->createForm(EventType::class, $event, ['user' => $user]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {


            if ($form->get('series')->getData()) {
                $days = $this->get('mod.team.helper')->getDaysBySeriesRange($form->get('startSeries')->getData(), $form->get('endSeries')->getData(), $form->get('start')->getData(), $form->get('end')->getData(), $form->get('days')->getData());

                $countDays = 0;
                $hashForSeries = time() . $this->getUser()->getId();
                foreach ($days['start'] as $day) {

                    $event = new Event();
                    $event->setType($form->get('type')->getData());
                    $event->setAddress($form->get('address')->getData());
                    $event->setAllDay($form->get('allDay')->getData());

                    if ($form->has('contact')) {
                        $event->setContact($form->get('contact')->getData());
                    }

                    $event->setEnd($days['ende'][$countDays]);
                    $event->setName($form->get('name')->getData());
                    if ($form->has('project')) {
                        $event->setProject($form->get('project')->getData());
                    }
                    $event->setSeries($hashForSeries);
                    $event->setStart($day);
                    $event->setText($form->get('text')->getData());

                    $event->setVisibility($form->get('visibility')->getData());

                    if ($event->getType() == Event::TYPE_EMERGENCY) {
                        $event->setVisibility(Event::VISIBLE_PUBLIC);
                    }

                    if ($form->get('members')->getData()) {
                        foreach ($form->get('members')->getData() as $members) {
                            $event->addMember($members);

                        }
                    }

                    $em->persist($event);
                    $em->flush();

                    /**
                     * @var Member $members
                     */
                    foreach ($event->getMembers() as $members) {
                        $googlePlan = new GoogleEvent();
                        $googlePlan->setEvent($event);
                        $googlePlan->setUser($members->getUser());
                        $em->persist($googlePlan);
                    }
                    $countDays++;
                }
            } else {
                if ($event->getType() == Event::TYPE_EMERGENCY) {
                    $event->setVisibility(Event::VISIBLE_PUBLIC);
                }
                $event->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
                $event->setUpdatedAt(new \DateTime(date('Y-m-d H:i:s')));
                $em->persist($event);
                $em->flush();

                /**
                 * @var Member $members
                 */
                foreach ($event->getMembers() as $members) {
                    $googlePlan = new GoogleEvent();
                    $googlePlan->setEvent($event);
                    $googlePlan->setUser($members->getUser());
                    $em->persist($googlePlan);
                }

            }

            if ($form->get('jobticket')->getData()) {
                $jobticket = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:JobTicket')->find($form->get('jobticket')->getData());

                $jobticketEvent = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:JobTicket')->findByEvent($event, $jobticket);

                if (!$jobticketEvent) $jobticket->addEvent($event);
            }

            $em->flush();

            $this->addFlash('success', '<i class="uk-icon-check"></i> Der Termin wurde erfolgreich angelegt.');

            $this->get('session')->set('modules_calendar_current_date', $event->getStart()->format('Y-m-d'));


            if ($form->get('jobticket_new')->getData()) {
                return $this->redirectToRoute('modules_ordermanagement_jobticket_new', ['todo' => 0,
                    'order' => 0,
                    'event' => $event->getId()]);
            } else { // redirect to date of added event
                return $this->redirectToRoute('modules_calendar_default_index');
            }

        }

        if ($date && $request->attributes->get('_route') == 'modules_calendar_event_new_date') {
            $dt = new \DateTime();
            $dt->setTimestamp($date);

            $dt->modify("+9 hours");
            $form->get('start')->setData($dt);

            $dt->modify("+30 minutes");
            $form->get('end')->setData($dt);
        }


        return $this->render('ModulesCalendarBundle:Event:new.html.twig', ['event' => $event,
            'form' => $form->createView(),]);
    }

    /**
     * Finds and displays a event entity.
     * @Route("/{id}")
     * @Method("GET")
     * @Security("is_granted('VIEW', 'Modules\\CalendarBundle\\Entity\\Event') or is_granted('VIEW', event)")
     * @param Event $event
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Event $event)
    {
        $deleteForm = $this->createDeleteForm($event);

        return $this->render('ModulesCalendarBundle:Event:show.html.twig', ['event' => $event,
            'delete_form' => $deleteForm->createView(),]);
    }

    /**
     * Creates a form to delete a event entity.
     * @param Event $event The event entity
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Event $event)
    {
        return $this->createFormBuilder()->setAction($this->generateUrl('modules_calendar_event_delete', ['id' => $event->getId()]))->setMethod('POST')->getForm();
    }

    /**
     * Finds and displays a event entity.
     * @Route("/modal/{id}")
     * @Method("GET")
     * @Security("is_granted('VIEW', 'Modules\\CalendarBundle\\Entity\\Event') or is_granted('VIEW', event)")
     * @param Event $event
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function modalAction(Event $event)
    {
        $deleteForm = $this->createDeleteForm($event);

        return $this->render('ModulesCalendarBundle:Event:modal.html.twig', ['event' => $event,
            'deleteForm' => $deleteForm->createView(),]);
    }

    /**
     * Displays a form to edit an existing event entity.
     * @Route("/{id}/edit")
     * @Method({"GET", "POST"})
     * @Security("is_granted('EDIT', 'Modules\\CalendarBundle\\Entity\\Event') or is_granted('EDIT', event)")
     * @param Request $request
     * @param Event $event
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @ParamConverter("id", class="Modules\CalendarBundle\Entity\Event")
     */
    public function editAction(Request $request, Event $event)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $deleteForm = $this->createDeleteForm($event);
        $editForm = $this->createForm(EventType::class, $event, ['user' => $user]);
        $editForm->remove('series');
        $editForm->remove('startSeries');
        $editForm->remove('endSeries');
        $editForm->remove('days');

        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {

            $googleEventRepo = $this->getDoctrine()->getRepository('ModulesCalendarBundle:GoogleEvent');

            /**
             * @var Member $members
             */
            foreach ($editForm->get('members')->getData() as $members) {

                $eventUser = $googleEventRepo->findOneBy(['event' => $event, 'user' => $members->getUser()]);
                if (!$eventUser) {
                    $googlePlan = new GoogleEvent();
                    $googlePlan->setEvent($event);
                    $googlePlan->setUser($members->getUser());
                    $this->getDoctrine()->getManager()->persist($googlePlan);
                }

            }
            if ($editForm->get('jobticket')->getData()) {
                $jobticket = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:JobTicket')->find($editForm->get('jobticket')->getData());

                $jobticketEvent = $this->getDoctrine()->getManager()->getRepository('ModulesOrderManagementBundle:JobTicket')->findByEvent($event, $jobticket);

                if (!$jobticketEvent) $jobticket->addEvent($event);
            }
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', '<i class="uk-icon-check"></i> Der Termin wurde erfolgreich aktualisiert.');

            // redirect to date of edited event
            $this->get('session')->set('modules_calendar_current_date', $event->getStart()->format('Y-m-d'));

            if ($editForm->get('jobticket_new')->getData()) {
                return $this->redirectToRoute('modules_ordermanagement_jobticket_new', ['todo' => 0,
                    'order' => 0,
                    'event' => $event->getId()]);
            } else {
                return $this->redirectToRoute('modules_calendar_default_index');
            }

        }

        return $this->render('ModulesCalendarBundle:Event:edit.html.twig', ['event' => $event,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),]);
    }

    /**
     * Deletes a event entity.
     * @Route("/delete/{id}")
     * @Method({"DELETE", "POST"})
     * @Security("is_granted('DELETE', 'Modules\\CalendarBundle\\Entity\\Event') or is_granted('DELETE', event)")
     * @param Request $request
     * @param Event $event
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, Event $event)
    {
        $form = $this->createDeleteForm($event);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // delete from google calendar
            $googleRefreshToken = $this->get('app.settings')->getGlobal('mod_calendar_google_refresh_token');
            $googleCalendarId = $this->get('app.settings')->getGlobal('mod_calendar_google_calendar_id');

            $googleEventRepo = $this->getDoctrine()->getRepository('ModulesCalendarBundle:GoogleEvent');
            $events = $googleEventRepo->findBy(['event' => $event]);

            if ($events) {
                /**
                 * @var GoogleEvent $eventGoogle ;
                 */
                foreach ($events as $eventGoogle) {

                    $googleId = $eventGoogle->getGoogleEventId();
                    if ($googleId) {
                        $user = $eventGoogle->getUser();
                        if ($user) {
                            $googleRefreshToken = $eventGoogle->getUser()->getGoogleRefreshToken();

                            if ($googleId && $googleRefreshToken && $googleCalendarId) {
                                $googleClient = $this->get('app.google')->getClient();
                                $accessToken = $googleClient->fetchAccessTokenWithRefreshToken($googleRefreshToken);

                                if (!array_key_exists('error', $accessToken)) {
                                    $googleCalendar = new \Google_Service_Calendar($googleClient);
                                    try {
                                        $googleCalendar->events->delete($user->getGoogleCalendarId(), $googleId);
                                    } catch (\Exception $exception) {
                                    }
                                }
                            } else {
                                $googleRefreshToken = $this->get('app.settings')->getGlobal('mod_calendar_google_refresh_token');

                                if ($eventGoogle->getGoogleEventId() && $googleRefreshToken && $googleCalendarId) {
                                    $accessToken = $this->get('app.google')->getClient()->fetchAccessTokenWithRefreshToken($googleRefreshToken);
                                    if (!array_key_exists('error', $accessToken)) {
                                        $googleCalendar = new \Google_Service_Calendar($this->get('app.google')->getClient());
                                        $googleCalendar->events->delete($googleCalendarId, $eventGoogle->getGoogleEventId());
                                    }
                                }
                            }
                        }
                    }

                }
            }


            $em = $this->getDoctrine()->getManager();
            $em->remove($event);
            $em->flush();

            $this->addFlash('success', '<i class="uk-icon-check"></i> Der Termin wurde erfolgreich gelöscht.');
        } else {
            $this->addFlash('danger', '<i class="uk-icon-check"></i> Der Termin konnte leider nicht gelöscht werden.');
        }

        return $this->redirectToRoute('modules_calendar_default_index');
    }

    /**
     * @Route("/autocomplete/addresses", options={"expose"=true})
     * @Security("is_granted('VIEW', 'Modules\\CalendarBundle\\Entity\\Address') and is_granted('VIEW', 'Modules\\ProjectBundle\\Entity\\Project')")
     * @return JsonResponse
     */
    public function autocompleteAddressesAction(Request $request)
    {
        $addressesArray = [];

        $contactId = $request->query->get('contact');
        $projectId = $request->query->get('project');

        if ($contactId) {
            $addresses = $this->getDoctrine()->getManager()->getRepository('ModulesContactBundle:Address')->findBy(['contact' => $contactId]);
            foreach ($addresses as $address) {
                $addressesArray[] = ['value' => $address->__toString(), 'title' => $address->__toString()];
            }
        }

        if ($projectId) {
            $project = $this->getDoctrine()->getManager()->getRepository('ModulesProjectBundle:Project')->find($projectId);
            if ($project && $project->getAddress()) {
                $addressesArray[] = ['value' => $project->getAddress(), 'title' => $project->getAddress()];
            }
        }

        return new JsonResponse($addressesArray);
    }

    /**
     * @param Request $request
     * @param Event $event
     * @Route("/print/{event}", options={"expose"=true})
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function printAction(Request $request, Event $event)
    {
        return $this->render('ModulesCalendarBundle:Event:print.html.twig', ['event' => $event]);
    }
}
