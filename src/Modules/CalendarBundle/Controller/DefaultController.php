<?php

namespace Modules\CalendarBundle\Controller;

use Modules\CalendarBundle\Entity\Event;
use Modules\InventoryBundle\Entity\ItemUse;
use Modules\TeamBundle\Entity\Plan;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;

class DefaultController extends Controller
{
    /**
     * @Route("/")
     * @Route("/redirect/{redirectDate}/{view}", requirements={"redirectDate": "^(19|20)\d\d-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$"}, options={"expose"=true}, name="modules_calendar_default_index_redirect")
     * @Security("is_granted('VIEW', 'Modules\\CalendarBundle\\Entity\\Event')")
     * @param null $redirectDate
     * @param null $view
     * @return RedirectResponse
     */
    public function indexAction($redirectDate = null, $view = null)
    {
        // get current view from session or default value from config
        if (!$view) {
            $view = $this->get('session')->get('modules_calendar_current_view', $this->getParameter('modules_calendar_default_view'));
        }

        // get current date from session or default to today
        if (!$redirectDate) {
            $redirectDate = $this->get('session')->get('modules_calendar_current_date', date('Y-m-d'));
        }

        // redirect based on current view and date
        $redirectDate = new \DateTime($redirectDate);
        $year = $redirectDate->format('Y');
        switch ($view) {
            default:
            case 'week':
                $week = $redirectDate->format('W');

                return new RedirectResponse($this->generateUrl('modules_calendar_default_week', ['week' => $week, 'year' => $year]));
            case 'month':
                $month = $redirectDate->format('m');

                return new RedirectResponse($this->generateUrl('modules_calendar_default_month', ['month' => $month, 'year' => $year]));
            case 'day':
                $day = $redirectDate->format('Y-m-d');

                return new RedirectResponse($this->generateUrl('modules_calendar_default_day', ['day' => $day]));
        }
    }

    /**
     * @Route("/day/{day}", requirements={"day": "^(19|20)\d\d-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$"})
     * @Security("is_granted('VIEW', 'Modules\\CalendarBundle\\Entity\\Event')")
     * @param null $day
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function dayAction($day = null)
    {
        // set start
        $start = new \DateTime($day);

        // save current view
        $this->get('session')->set('modules_calendar_current_view', 'day');

        // save current date
        $this->get('session')->set('modules_calendar_current_date', $start->format('Y-m-d'));

        // set member filter
        if ($this->isGranted('ROLE_ADMIN')) {
            $memberFilter = $this->get('mod.calendar_helper')->setMemberFilter();
        } elseif ($this->getUser()->getMember()) {
            $memberFilter = [['member' => $this->getUser()->getMember(), 'active' => true]];
        } else {
            $memberFilter = [];
        }
        $memberCategories = $this->getDoctrine()->getRepository('ModulesTeamBundle:Category')->findAll();

        // set inventory filter
        $inventoryFilter = $this->get('mod.calendar_helper')->setInventoryFilter();

        // set inventory filter
        $planFilter = $this->get('mod.calendar_helper')->setPlanFilter();

        // get events
        $members = [];
        foreach ($memberFilter as $member) {
            if ($member['active']) {
                $members[] = $member['member'];
            }
        }
        $eventRepo = $this->getDoctrine()->getManager()->getRepository('ModulesCalendarBundle:Event');
        $events = $eventRepo->findForDayAndMembers($start, $members);

        $eventsJson = [];
        /** @var Event $event */
        foreach ($events as $event) {
            $eventJson = ['title' => $event->getName(), 'icon' => $this->get('mod.calendar_helper')->getEventIcon($event), 'color' => $this->get('mod.calendar_helper')->getEventColor($event), 'start' => $event->getStart()->format('Y-m-d H:i'), 'allDay' => $event->getAllDay(), 'modalUrl' => $this->generateUrl('modules_calendar_event_modal', ['id' => $event->getId()])];
            if ($event->getEnd()) {
                $eventJson['end'] = $event->getEnd()->format('Y-m-d H:i');
            }
            $eventsJson[] = $eventJson;
        }

        // get inventory item usages
        $itemUsages = $this->getDoctrine()->getRepository('ModulesInventoryBundle:ItemUse')->findForDay($start);
        if (isset($inventoryFilter[ItemUse::STATUS_IN_USE])) {
            foreach ($itemUsages as $usage) {
                if ($usage->getStatus() == ItemUse::STATUS_IN_USE) {
                    $eventsJson[] = ['title' => $usage->getItem()->getName(), 'icon' => 'dropbox', 'color' => '#faa732', 'start' => $usage->getStart()->format('Y-m-d H:i'), 'end' => $usage->getEnd()->format('Y-m-d H:i'), 'modalUrl' => $this->generateUrl('modules_inventory_itemuse_modal', ['item' => $usage->getItem()->getId(), 'itemUse' => $usage->getId()])];
                }
            }
        }
        if (isset($inventoryFilter[ItemUse::STATUS_IN_REPAIR])) {
            foreach ($itemUsages as $usage) {
                if ($usage->getStatus() == ItemUse::STATUS_IN_REPAIR) {
                    $eventsJson[] = ['title' => $usage->getItem()->getName(), 'icon' => 'dropbox', 'color' => '#da314b', 'start' => $usage->getStart()->format('Y-m-d H:i'), 'end' => $usage->getEnd()->format('Y-m-d H:i'), 'modalUrl' => $this->generateUrl('modules_inventory_itemuse_modal', ['item' => $usage->getItem()->getId(), 'itemUse' => $usage->getId()])];
                }
            }
        }

        // get team member plans
        $plans = $this->getDoctrine()->getRepository('ModulesTeamBundle:Plan')->findForDay($start);
        if (isset($planFilter[Plan::TYPE_HOLIDAY])) {
            /** @var Plan $plan */
            foreach ($plans as $plan) {
                if ($plan->getType() == Plan::TYPE_HOLIDAY) {
                    $eventsJson[] = ['title' => 'Urlaub ' . ($plan->getMember() ?: 'Alle'), 'icon' => 'plan', 'color' => '#00cc00', 'start' => $plan->getDay()->format('Y-m-d H:i'), 'end' => $plan->getDay()->format('Y-m-d H:i'), 'modalUrl' => $this->generateUrl('modules_team_plan_modal', ['plan' => $plan->getId()]), 'allDay' => true];
                }
            }
        }
        if (isset($planFilter[Plan::TYPE_ILLNESS])) {
            /** @var Plan $plan */
            foreach ($plans as $plan) {
                if ($plan->getType() == Plan::TYPE_ILLNESS) {
                    $eventsJson[] = ['title' => 'Krankheit ' . ($plan->getMember() ?: 'Alle'), 'icon' => 'plan', 'color' => '#cccc00', 'start' => $plan->getDay()->format('Y-m-d H:i'), 'end' => $plan->getDay()->format('Y-m-d H:i'), 'modalUrl' => $this->generateUrl('modules_team_plan_modal', ['plan' => $plan->getId()]), 'allDay' => true];
                }
            }
        }
        if (isset($planFilter[Plan::TYPE_SCHOOL])) {
            /** @var Plan $plan */
            foreach ($plans as $plan) {
                if ($plan->getType() == Plan::TYPE_SCHOOL) {
                    $eventsJson[] = ['title' => 'Berufsschule ' . ($plan->getMember() ?: 'Alle'), 'icon' => 'plan', 'color' => '#0000cc', 'start' => $plan->getDay()->format('Y-m-d H:i'), 'end' => $plan->getDay()->format('Y-m-d H:i'), 'modalUrl' => $this->generateUrl('modules_team_plan_modal', ['plan' => $plan->getId()]), 'allDay' => true];
                }
            }
        }

        return $this->render('ModulesCalendarBundle:Default:day.html.twig', ['events' => $events, 'memberFilter' => $memberFilter, 'inventoryFilter' => $inventoryFilter, 'eventsJson' => $eventsJson, 'start' => $start, 'memberCategories' => $memberCategories]);
    }

    /**
     * @Route("/week/{week}/{year}", requirements={"week": "^0[1-9]|[1-4][0-9]|5[0123]$", "year": "^(19|20)\d\d$"})
     * @Security("is_granted('VIEW', 'Modules\\CalendarBundle\\Entity\\Event')")
     * @param null $week
     * @param null $year
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function weekAction($week = null, $year = null)
    {
        // set dates
        $week = $week ?: date('W');
        $year = $year ?: date('Y');
        $weekYear = $year . 'W' . $week;

        $monday = $start = (new \DateTime($weekYear));
        $tuesday = (new \DateTime($weekYear))->modify('+1 day');
        $wednesday = (new \DateTime($weekYear))->modify('+2 day');
        $thursday = (new \DateTime($weekYear))->modify('+3 day');
        $friday = (new \DateTime($weekYear))->modify('+4 day');
        $saturday = (new \DateTime($weekYear))->modify('+5 day');
        $sunday = (new \DateTime($weekYear))->modify('+6 day');

        // save current view
        $this->get('session')->set('modules_calendar_current_view', 'week');

        // save current date
        $this->get('session')->set('modules_calendar_current_date', $start->format('Y-m-d'));

        $memberFilter = $this->get('mod.calendar_helper')->setMemberFilter();
        $memberCategories = $this->getDoctrine()->getRepository('ModulesTeamBundle:Category')->findAll();

        // set inventory filter
        $inventoryFilter = $this->get('mod.calendar_helper')->setInventoryFilter();

        // set plan filter
        $planFilter = $this->get('mod.calendar_helper')->setPlanFilter();

        // get events
        $eventRepo = $this->getDoctrine()->getManager()->getRepository('ModulesCalendarBundle:Event');
        $events = [];

        foreach ($memberFilter as $member) {

            if ($member['member']->getUser() && !$this->getUser()->isSuperAdmin() && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
                if ($this->getUser()->getId() == $member['member']->getUser()->getId()) {
                    $ignoreVisibility = true;
                } else {
                    $ignoreVisibility = false;
                }
            } else {
                if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
                    $ignoreVisibility = true;
                } else {
                    $ignoreVisibility = $this->getUser()->isSuperAdmin();
                }
            }

            if ($member['active']) {
                $events[] = ['member' => $member['member'], 0 => $eventRepo->findForMemberAndDay($member['member'], $monday, $ignoreVisibility), 1 => $eventRepo->findForMemberAndDay($member['member'], $tuesday, $ignoreVisibility), 2 => $eventRepo->findForMemberAndDay($member['member'], $wednesday, $ignoreVisibility), 3 => $eventRepo->findForMemberAndDay($member['member'], $thursday, $ignoreVisibility), 4 => $eventRepo->findForMemberAndDay($member['member'], $friday, $ignoreVisibility), 5 => $eventRepo->findForMemberAndDay($member['member'], $saturday, $ignoreVisibility), 6 => $eventRepo->findForMemberAndDay($member['member'], $sunday, $ignoreVisibility),];
            }
        }

        // get inventory item usages
        $itemUseRepo = $this->getDoctrine()->getRepository('ModulesInventoryBundle:ItemUse');
        for ($i = 0; $i < count($events); $i++) {
            $events[$i][0] = array_merge($events[$i][0], $itemUseRepo->findForDayAndMemberByFilter($monday, $events[$i]['member'], $inventoryFilter));
            $events[$i][1] = array_merge($events[$i][1], $itemUseRepo->findForDayAndMemberByFilter($tuesday, $events[$i]['member'], $inventoryFilter));
            $events[$i][2] = array_merge($events[$i][2], $itemUseRepo->findForDayAndMemberByFilter($wednesday, $events[$i]['member'], $inventoryFilter));
            $events[$i][3] = array_merge($events[$i][3], $itemUseRepo->findForDayAndMemberByFilter($thursday, $events[$i]['member'], $inventoryFilter));
            $events[$i][4] = array_merge($events[$i][4], $itemUseRepo->findForDayAndMemberByFilter($friday, $events[$i]['member'], $inventoryFilter));
            $events[$i][5] = array_merge($events[$i][5], $itemUseRepo->findForDayAndMemberByFilter($saturday, $events[$i]['member'], $inventoryFilter));
            $events[$i][6] = array_merge($events[$i][6], $itemUseRepo->findForDayAndMemberByFilter($sunday, $events[$i]['member'], $inventoryFilter));
        }

        // get team plannings
        $planRepo = $this->getDoctrine()->getRepository('ModulesTeamBundle:Plan');
        for ($i = 0; $i < count($events); $i++) {
            $events[$i][0] = array_merge($events[$i][0], $planRepo->findForDayAndMemberByFilter($monday, $events[$i]['member'], $planFilter));
            $events[$i][1] = array_merge($events[$i][1], $planRepo->findForDayAndMemberByFilter($tuesday, $events[$i]['member'], $planFilter));
            $events[$i][2] = array_merge($events[$i][2], $planRepo->findForDayAndMemberByFilter($wednesday, $events[$i]['member'], $planFilter));
            $events[$i][3] = array_merge($events[$i][3], $planRepo->findForDayAndMemberByFilter($thursday, $events[$i]['member'], $planFilter));
            $events[$i][4] = array_merge($events[$i][4], $planRepo->findForDayAndMemberByFilter($friday, $events[$i]['member'], $planFilter));
            $events[$i][5] = array_merge($events[$i][5], $planRepo->findForDayAndMemberByFilter($saturday, $events[$i]['member'], $planFilter));
            $events[$i][6] = array_merge($events[$i][6], $planRepo->findForDayAndMemberByFilter($sunday, $events[$i]['member'], $planFilter));
        }
        $events['all'] = [];
        $events['all'][0] = $planRepo->findForDayAndAllMembers($monday, $planFilter);
        $events['all'][1] = $planRepo->findForDayAndAllMembers($tuesday, $planFilter);
        $events['all'][2] = $planRepo->findForDayAndAllMembers($wednesday, $planFilter);
        $events['all'][3] = $planRepo->findForDayAndAllMembers($thursday, $planFilter);
        $events['all'][4] = $planRepo->findForDayAndAllMembers($friday, $planFilter);
        $events['all'][5] = $planRepo->findForDayAndAllMembers($saturday, $planFilter);
        $events['all'][6] = $planRepo->findForDayAndAllMembers($sunday, $planFilter);

        return $this->render('ModulesCalendarBundle:Default:week.html.twig', ['events' => $events, 'memberFilter' => $memberFilter, 'inventoryFilter' => $inventoryFilter, 'planFilter' => $planFilter, 'start' => $start, 'monday' => $monday, 'tuesday' => $tuesday, 'wednesday' => $wednesday, 'thursday' => $thursday, 'friday' => $friday, 'saturday' => $saturday, 'sunday' => $sunday, 'memberCategories' => $memberCategories]);
    }

    /**
     * @Route("/month/{month}/{year}", requirements={"month": "^0[1-9]|1[0-2]$", "year": "^(19|20)\d\d$"})
     * @Security("is_granted('VIEW', 'Modules\\CalendarBundle\\Entity\\Event')")
     * @param null $month
     * @param null $year
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function monthAction($month = null, $year = null)
    {
        // set start
        $month = $month ?: date('m');
        $year = $year ?: date('Y');
        $start = new \DateTime($year . '-' . $month);

        // save current view
        $this->get('session')->set('modules_calendar_current_view', 'month');

        // save current date
        $this->get('session')->set('modules_calendar_current_date', $start->format('Y-m-d'));

        // set member filter
        if ($this->isGranted('ROLE_ADMIN')) {
            $memberFilter = $this->get('mod.calendar_helper')->setMemberFilter();
        } elseif ($this->getUser()->getMember()) {
            $memberFilter = [['member' => $this->getUser()->getMember(), 'active' => true]];
        } else {
            $memberFilter = [];
        }
        $memberCategories = $this->getDoctrine()->getRepository('ModulesTeamBundle:Category')->findAll();

        // set inventory filter
        $inventoryFilter = $this->get('mod.calendar_helper')->setInventoryFilter();

        // set inventory filter
        $planFilter = $this->get('mod.calendar_helper')->setPlanFilter();

        // get events
        $members = [];
        foreach ($memberFilter as $member) {
            if ($member['active']) {
                $members[] = $member['member'];
            }
        }
        $eventRepo = $this->getDoctrine()->getManager()->getRepository('ModulesCalendarBundle:Event');
        $events = $eventRepo->findForMonthAndMembers($start, $members);

        $eventsJson = [];
        /** @var Event $event */
        foreach ($events as $event) {
            $eventJson = ['title' => $event->getName(), 'icon' => $this->get('mod.calendar_helper')->getEventIcon($event), 'color' => $this->get('mod.calendar_helper')->getEventColor($event), 'start' => $event->getStart()->format('Y-m-d H:i'), 'allDay' => $event->getAllDay(), 'modalUrl' => $this->generateUrl('modules_calendar_event_modal', ['id' => $event->getId()])];
            if ($event->getEnd()) {
                $eventJson['end'] = $event->getEnd()->format('Y-m-d H:i');
            }
            $eventsJson[] = $eventJson;
        }

        // get inventory item usages
        $itemUsages = $this->getDoctrine()->getRepository('ModulesInventoryBundle:ItemUse')->findForMonth($start);
        if (isset($inventoryFilter[ItemUse::STATUS_IN_USE])) {
            foreach ($itemUsages as $usage) {
                if ($usage->getStatus() == ItemUse::STATUS_IN_USE) {
                    $eventsJson[] = ['title' => $usage->getItem()->getName(), 'icon' => 'dropbox', 'color' => '#faa732', 'start' => $usage->getStart()->format('Y-m-d H:i'), 'end' => $usage->getEnd()->format('Y-m-d H:i'), 'modalUrl' => $this->generateUrl('modules_inventory_itemuse_modal', ['item' => $usage->getItem()->getId(), 'itemUse' => $usage->getId()])];
                }
            }
        }
        if (isset($inventoryFilter[ItemUse::STATUS_IN_REPAIR])) {
            foreach ($itemUsages as $usage) {
                if ($usage->getStatus() == ItemUse::STATUS_IN_REPAIR) {
                    $eventsJson[] = ['title' => $usage->getItem()->getName(), 'icon' => 'dropbox', 'color' => '#da314b', 'start' => $usage->getStart()->format('Y-m-d H:i'), 'end' => $usage->getEnd()->format('Y-m-d H:i'), 'modalUrl' => $this->generateUrl('modules_inventory_itemuse_modal', ['item' => $usage->getItem()->getId(), 'itemUse' => $usage->getId()])];
                }
            }
        }

        // get team member plans
        $plans = $this->getDoctrine()->getRepository('ModulesTeamBundle:Plan')->findForMonth($start);
        if (isset($planFilter[Plan::TYPE_HOLIDAY])) {
            /** @var Plan $plan */
            foreach ($plans as $plan) {
                if ($plan->getType() == Plan::TYPE_HOLIDAY) {
                    $eventsJson[] = ['title' => 'Urlaub ' . ($plan->getMember() ?: 'Alle'), 'icon' => 'plan', 'color' => '#00cc00', 'start' => $plan->getDay()->format('Y-m-d H:i'), 'end' => $plan->getDay()->format('Y-m-d H:i'), 'modalUrl' => $this->generateUrl('modules_team_plan_modal', ['plan' => $plan->getId()]), 'allDay' => true];
                }
            }
        }
        if (isset($planFilter[Plan::TYPE_ILLNESS])) {
            /** @var Plan $plan */
            foreach ($plans as $plan) {
                if ($plan->getType() == Plan::TYPE_ILLNESS) {
                    $eventsJson[] = ['title' => 'Krankheit ' . ($plan->getMember() ?: 'Alle'), 'icon' => 'plan', 'color' => '#cccc00', 'start' => $plan->getDay()->format('Y-m-d H:i'), 'end' => $plan->getDay()->format('Y-m-d H:i'), 'modalUrl' => $this->generateUrl('modules_team_plan_modal', ['plan' => $plan->getId()]), 'allDay' => true];
                }
            }
        }
        if (isset($planFilter[Plan::TYPE_SCHOOL])) {
            /** @var Plan $plan */
            foreach ($plans as $plan) {
                if ($plan->getType() == Plan::TYPE_SCHOOL) {
                    $eventsJson[] = ['title' => 'Berufsschule ' . ($plan->getMember() ?: 'Alle'), 'icon' => 'plan', 'color' => '#0000cc', 'start' => $plan->getDay()->format('Y-m-d H:i'), 'end' => $plan->getDay()->format('Y-m-d H:i'), 'modalUrl' => $this->generateUrl('modules_team_plan_modal', ['plan' => $plan->getId()]), 'allDay' => true];
                }
            }
        }

        return $this->render('ModulesCalendarBundle:Default:month.html.twig', ['events' => $events, 'memberFilter' => $memberFilter, 'inventoryFilter' => $inventoryFilter, 'eventsJson' => $eventsJson, 'start' => $start, 'memberCategories' => $memberCategories]);
    }
}
