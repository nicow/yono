<?php

namespace Modules\CalendarBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGenerator;

/**
 * @Route("/google")
 */
class GoogleController extends Controller
{
    /**
     * @Route("/")
     * @Security("is_granted('ROLE_ADMIN')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function indexAction(Request $request)
    {
        $googleClient = $this->get('app.google')->getClient();
        $googleClient->setRedirectUri($this->generateUrl('modules_calendar_google_connect', [], UrlGenerator::ABSOLUTE_URL));

        $googleEmail = $this->get('app.settings')->getGlobal('mod_calendar_google_email');
        $googleRefreshToken = $this->get('app.settings')->getGlobal('mod_calendar_google_refresh_token');
        $googleCalendarId = $this->get('app.settings')->getGlobal('mod_calendar_google_calendar_id');

        $currentCalendar = null;
        $availableCalendars = [];

        if ($googleEmail && $googleRefreshToken) {
            $accessToken = $googleClient->fetchAccessTokenWithRefreshToken($googleRefreshToken);
            if (!array_key_exists('error', $accessToken)) {
                $googleCalendar = new \Google_Service_Calendar($googleClient);

                if ($googleCalendarId) {
                    $currentCalendar = $googleCalendar->calendars->get($googleCalendarId);
                }

                /** @var \Google_Service_Calendar_CalendarListEntry $calendar */
                foreach ($googleCalendar->calendarList->listCalendarList()->getItems() as $calendar) {
                    $label = $calendar->getSummary();
                    if ($calendar->getDescription()) {
                        $label .= ' (' . $calendar->getDescription() . ')';
                    }
                    $availableCalendars[$calendar->getId()] = $label;
                }

                if ($request->request->has('mod_calendar_google_calendar_id')) {
                    $calendarId = $request->request->get('mod_calendar_google_calendar_id');
                    if (array_key_exists($calendarId, $availableCalendars)) {
                        $this->get('app.settings')->setGlobal('mod_calendar_google_calendar_id', $calendarId);

                        return new RedirectResponse($this->generateUrl('modules_calendar_google_index'));
                    }
                }
            } else {
                throw new \Exception("Fetching the access token failed: " . $accessToken['error_description'] . ': ' . $accessToken['error']);
            }
        }

        return $this->render('ModulesCalendarBundle:Google:index.html.twig', ['googleEmail' => $googleEmail, 'googleRefreshToken' => $googleRefreshToken, 'googleCalendarId' => $googleCalendarId, 'availableCalendars' => $availableCalendars, 'currentCalendar' => $currentCalendar]);
    }

    /**
     * @Route("/connect")
     * @param Request $request
     * @return RedirectResponse
     * @throws \Exception
     */
    public function connectAction(Request $request)
    {
        if ($request->query->has('code')) {
            $googleClient = $this->get('app.google')->getClient();
            $googleClient->setRedirectUri($this->generateUrl('modules_calendar_google_connect', [], UrlGenerator::ABSOLUTE_URL));
            $accessToken = $googleClient->authenticate($request->query->get('code'));
            if (!array_key_exists('error', $accessToken)) {
                if ($refreshToken = $googleClient->getRefreshToken()) {
                    $googlePlus = new \Google_Service_Plus($googleClient);
                    $me = $googlePlus->people->get('me');
                    /** @var \Google_Service_Plus_PersonEmails $email */
                    foreach ($me->getEmails() as $email) {
                        if ($email->getType() == 'account') {
                            // update settings
                            $this->get('app.settings')->setGlobal('mod_calendar_google_email', $email->getValue());
                            $this->get('app.settings')->setGlobal('mod_calendar_google_refresh_token', $refreshToken);

                            $this->addFlash('success', '<i class="uk-icon-check"></i> Sie haben sich erfolgreich mit dem Google Konto <i>' . $email->getValue() . '</i> verbunden!');

                            return new RedirectResponse($this->generateUrl('modules_calendar_google_index'));
                        }
                    }
                    throw new \Exception("Fetching the account email failed for some unknown reason.");
                }
                throw new \Exception("Fetching the access token failed for some unknown reason.");
            } else {
                throw new \Exception("Fetching the access token failed: " . $accessToken['error_description']);
            }
        } elseif ($request->query->has('error')) {
            $this->addFlash('error', '<i class="uk-icon-warning"></i> Die Verbindung zum Google Konto konnte nicht hergestellt werden!');

            return new RedirectResponse($this->generateUrl('modules_calendar_google_index'));
        }

        throw new \Exception("This route should only be used for a redirect from google's oauth dialog (containing a 'code' or 'error' query parameter).");
    }

    /**
     * @Route("/reset")
     * @Security("is_granted('ROLE_ADMIN')")
     * @return RedirectResponse
     * @throws \Exception
     */
    public function resetAction()
    {
        $this->get('app.settings')->setGlobal('mod_calendar_google_email', null);
        $this->get('app.settings')->setGlobal('mod_calendar_google_refresh_token', null);
        $this->get('app.settings')->setGlobal('mod_calendar_google_calendar_id', null);

        return new RedirectResponse($this->generateUrl('modules_calendar_google_index'));
    }
}
