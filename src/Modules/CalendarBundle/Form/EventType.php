<?php

namespace Modules\CalendarBundle\Form;

use Core\AppBundle\Form\Constraint\NoDateRangeConflict;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\QueryBuilder;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Modules\CalendarBundle\Entity\Event;
use Modules\TeamBundle\Entity\Member;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Validator\Constraints as Assert;

class EventType extends AbstractType
{
    /**
     * @var AuthorizationChecker
     */
    private $authorizationChecker;

    /** @var \Doctrine\ORM\EntityManager */
    private $em;

    /**
     * @var Container
     */
    private $container;

    public function __construct(AuthorizationChecker $authorizationChecker, EntityManager $em, Container $container)
    {
        $this->authorizationChecker = $authorizationChecker;
        $this->em = $em;
        $this->container = $container;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', null, ['label' => 'Bezeichnung'])->add('type', ChoiceType::class, ['label' => 'Art',
            'choices' => ['Termin' => Event::TYPE_DEFAULT,
                'Außendienst' => Event::TYPE_EXTERNAL,
                'Notdienst' => Event::TYPE_EMERGENCY]])->add('allDay', null, ['label' => 'ganztägig'])->add('visibility', null, ['label' => 'öffentlich'])->add('start', DateTimeType::class, ['date_widget' => 'single_text',
            'time_widget' => 'choice',
            'hours' => [6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21],
            'minutes' => [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55],
            'required' => false,
            'date_format' => 'd.M.y'])->add('end', DateTimeType::class, ['date_widget' => 'single_text',
            'time_widget' => 'choice',
            'hours' => [6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21],
            'minutes' => [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55],
            'required' => false,
            'date_format' => 'd.M.y'])->add('series', CheckboxType::class, ['label' => 'Serientermin',
            'required' => false,
            'mapped' => false])->add('startSeries', DateType::class, ['label' => 'Von',
            'widget' => 'single_text',
            'format' => 'd.M.y',
            'data' => new \DateTime(),
            'required' => false,
            'mapped' => false])->add('endSeries', DateType::class, ['label' => 'Bis',
            'widget' => 'single_text',
            'format' => 'd.M.y',
            'data' => new \DateTime(),
            'required' => false,
            'mapped' => false])->add('days', ChoiceType::class, ['label' => 'Tage',
            'choices' => ['Montag' => 1,
                'Dienstag' => 2,
                'Mittwoch' => 3,
                'Donnerstag' => 4,
                'Freitag' => 5,
                'Samstag' => 6,
                'Sonntag' => 7,],
            'required' => false,
            'mapped' => false,
            'multiple' => true,])->add('text', CKEditorType::class, ['label' => 'Beschreibung'])->add('address', null, ['label' => 'Adresse'])
            ->add('members', null, ['label' => 'Mitarbeiter','choices' => $this->getMembers(),
            'required' => true,
            'constraints' => [new Assert\Count(['min' => 1,
                'minMessage' => 'Es muss mindestens ein Mitarbeiter ausgewählt werden.'])]]);

        if ($this->authorizationChecker->isGranted('VIEW', 'Modules\\ContactBundle\\Entity\\Contact')) {
            $builder->add('contact', null, ['label' => 'Kontakt', 'placeholder' => 'kein Kontakt']);
        }

        if ($this->authorizationChecker->isGranted('VIEW', 'Modules\\ProjectBundle\\Entity\\Project')) {
            $builder->add('project', null, ['label' => 'Projekt', 'placeholder' => 'kein Projekt']);
        }

        $builder->add('jobticket', null, ['label' => 'Jobticket',
            'placeholder' => 'kein Jobticket',
            'choices' => $this->getJobtickets()]);
        
        $builder->add('jobticket_new', CheckboxType::class, ['label' => 'Neues Jobticket anlegen',
            'attr' => ['class' => 'newJobModal'],
            'required' => false,
            'mapped' => false]);

        $builder->add('todo', null, ['label' => 'Todo',
            'placeholder' => 'kein Todo',
            'choices' => $this->getTodos($options['user'])]);
        
        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $formEvent) {
            $event = $formEvent->getData();

            // set times if all day is selected
            if (isset($event['allDay'])) {
                $event['start']['time']['hour'] = '8';
                $event['start']['time']['minute'] = '0';
                $event['end']['time']['hour'] = '17';
                $event['end']['time']['minute'] = '0';
            }

            // set end if no end date is provided
            if (!$event['end']['date']) {
                $event['end']['date'] = $event['start']['date'];
            }
            if (!$event['end']['time']) {
                $event['end']['time'] = $event['start']['time'];
            }

            $formEvent->setData($event);
        });
    }

    protected function getJobtickets()
    {
        $jobticketRepo = $this->em->getRepository('ModulesOrderManagementBundle:JobTicket');

        $jobtickets = $jobticketRepo->findOpen();

        return $jobtickets;
    }

    protected function getTodos($user)
    {
        $todoRepo = $this->em->getRepository('ModulesToDoBundle:ToDo');

        $todos = $todoRepo->findAllOpen($user);

        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $archiv = $this->container->get('todo.helper')->getArchiv($user);
        $counter = 0;
        if (isset($filter['status'])) {
            if ($filter['status'] != 4) {
                foreach ($todos as $todo) {
                    if ($this->container->get('todo.helper')->isArchiv($todo, $archiv)) {
                        unset($todos[$counter]);
                    }
                    $counter++;
                }
            } else {
                foreach ($todos as $todo) {
                    if (!$this->container->get('todo.helper')->isArchiv($todo, $archiv)) {
                        unset($todos[$counter]);
                    }
                    $counter++;
                }
            }
        } else {
            foreach ($todos as $todo) {
                if ($this->container->get('todo.helper')->isArchiv($todo, $archiv)) {
                    unset($todos[$counter]);
                }
                $counter++;
            }
        }

        return $todos;
    }

    /**
     * @return \Modules\TeamBundle\Entity\Member[]
     */
    protected function getMembers()
    {
        $projectRepo = $this->em->getRepository('ModulesTeamBundle:Member');

        $projects = $projectRepo->findByNotHidden();

        return $projects;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array('data_class' => 'Modules\CalendarBundle\Entity\Event','user'=>null,
            'constraints' => [new NoDateRangeConflict(['message_callback' => function (Event $event, $conflictingEvents) {
                $conflictingMembers = [];
                /** @var Event $conflictingEvent */
                foreach ($conflictingEvents as $conflictingEvent) {
                    foreach ($conflictingEvent->getMembers() as $member) {
                        if ($event->getMembers()->contains($member)) {
                            $conflictingMembers[] = $member;
                        }
                    }
                }

                if (count($conflictingMembers) > 1) {
                    $membersString = '';
                    /** @var Member $conflictingMember */
                    foreach ($conflictingMembers as $conflictingMember) {
                        if ($conflictingMember == reset($conflictingMembers)) {
                            $membersString .= $conflictingMember->getFirstname() . ' ' . $conflictingMember->getLastname();
                        } elseif ($conflictingMember == end($conflictingMembers)) {
                            $membersString .= ' und ' . $conflictingMember->getFirstname() . ' ' . $conflictingMember->getLastname();
                        } else {
                            $membersString .= ', ' . $conflictingMember->getFirstname() . ' ' . $conflictingMember->getLastname();
                        }
                    }

                    return 'Für diesen Zeitraum und die Mitarbeiter ' . $membersString . ' ist bereits ein Termin eingetragen.';
                } else {
                    return 'Für diesen Zeitraum und den Mitarbeiter ' . $conflictingMembers[0]->getFirstname() . ' ' . $conflictingMembers[0]->getLastname() . ' ist bereits ein Termin eingetragen.';
                }
            },
                'query_builder' => function (QueryBuilder $qb, Event $entity) {
                    return $qb->join('entity.members', 'm')->andWhere('m IN (:members)')->setParameter('members', $entity->getMembers());
                }])]));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'modules_calendarbundle_event';
    }


}
