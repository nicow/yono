<?php
/**
 * Created by PhpStorm.
 * User: mkt
 * Date: 26.08.16
 * Time: 17:22
 */

namespace Modules\InventoryBundle\Event\Listener;


use Core\AppBundle\Event\ConfigureMenuEvent;
use Doctrine\ORM\EntityManager;
use Knp\Menu\FactoryInterface;
use Modules\InventoryBundle\Entity\Category;
use Modules\InventoryBundle\Service\InventoryHelper;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\HttpFoundation\RequestStack;

class ConfigureMenuListener
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var Router
     */
    private $router;
    /**
     * @var InventoryHelper
     */
    private $inventoryHelper;
    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var array
     */
    private $order;

    /**
     * @var Category
     */
    private $currentCategory;

    public function __construct(EntityManager $em, Router $router, InventoryHelper $inventoryHelper, RequestStack $requestStack)
    {
        $this->em = $em;
        $this->router = $router;
        $this->inventoryHelper = $inventoryHelper;
        $this->requestStack = $requestStack;
        $this->request = $requestStack->getCurrentRequest();
    }

    public function onMenuConfigure(ConfigureMenuEvent $event)
    {
        $factory = $event->getFactory();
        $menu = $event->getMenu();

        $categories = $this->em->getRepository('ModulesInventoryBundle:Category')->findBy(['showInMenu' => true, 'parent' => null]);

        if ($menu->getName() == 'main') {
            $inventoryKey = 'Inventar';
            if ($menu[$inventoryKey]) {
                $this->order = [];
                if ($menu[$inventoryKey]->getChild('Übersicht')) {
                    $this->order[] = 'Übersicht';
                }

                $this->currentCategory = $this->inventoryHelper->getCurrentCategory();

                // Add categories
                foreach ($categories as $category) {
                    $this->addChildren($factory, $category, $menu[$inventoryKey]);
                    $this->order[] = $category->getName();
                }

                if ($menu[$inventoryKey]->getChild('divider')) {
                    $this->order[] = 'divider';
                }
                if ($menu[$inventoryKey]->getChild('Suche')) {
                    $this->order[] = 'Suche';
                }
                if ($menu[$inventoryKey]->getChild('Kategorien')) {
                    $this->order[] = 'Kategorien';
                }

                $menu[$inventoryKey]->reorderChildren($this->order);

                // Handle "current" states
                if ($menu[$inventoryKey]->getChild('Kategorien')) {
                    if (strpos($this->request->attributes->get('_route'), 'modules_inventory_category') !== false) {
                        $menu[$inventoryKey]['Kategorien']->setCurrent(true);
                    }
                }
            }
        }
    }

    public function addChildren(FactoryInterface $factory, Category $category, $menu)
    {
        $options = ['route' => 'modules_inventory_item_categoryindex', 'routeParameters' => ['category' => $category->getId()], 'extras' => ['icon' => $category->getIcon()]];
        $item = $factory->createItem($category->getName(), $options);
        if ($this->currentCategory == $category) {
            $item->setCurrent(true);
        }
        if ($category->getChildren()) {
            foreach ($category->getChildren() as $child) {
                $this->addChildren($factory, $child, $item);
            }
        }
        $menu->addChild($item);
    }
}