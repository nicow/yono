<?php

namespace Modules\InventoryBundle;

use Core\YonoBundleInterface;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class ModulesInventoryBundle extends Bundle implements YonoBundleInterface
{
    /**
     * @return string
     */
    public function getDisplayName()
    {
        return 'Inventar';
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return 'Verwalten Sie Ihr Inventar.';
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'dropbox';
    }
}
