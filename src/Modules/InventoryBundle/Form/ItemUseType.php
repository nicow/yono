<?php

namespace Modules\InventoryBundle\Form;


use Modules\InventoryBundle\Entity\ItemUse;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

class ItemUseType extends AbstractType
{
    /**
     * @var AuthorizationChecker
     */
    private $authorizationChecker;

    public function __construct(AuthorizationChecker $authorizationChecker)
    {
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $itemUse = $builder->getData();
        $item = $itemUse->getItem();

        $statusChoices = [];
        if (!$item->getDefect() || $itemUse->getId()) {
            $statusChoices['in Verwendung'] = ItemUse::STATUS_IN_USE;
        }
        $statusChoices['in Reparatur'] = ItemUse::STATUS_IN_REPAIR;

        $builder->add('status', ChoiceType::class, ['choices' => $statusChoices])->add('end', DateTimeType::class, ['date_widget' => 'single_text', 'time_widget' => 'choice', 'hours' => [6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20], 'minutes' => [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55], 'date_format' => 'd.M.y'])->add('member', null, ['label' => 'Mitarbeiter', 'required' => true])->add('note', null, ['label' => 'Anmerkung']);

        if ($this->authorizationChecker->isGranted('VIEW', 'Modules\\ContactBundle\\Entity\\Contact')) {
            $builder->add('contact', null, ['label' => 'Kontakt']);
        }

        if ($this->authorizationChecker->isGranted('VIEW', 'Modules\\ProjectBundle\\Entity\\Project')) {
            $builder->add('project', null, ['label' => 'Projekt']);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array('data_class' => 'Modules\InventoryBundle\Entity\ItemUse'));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'modules_inventorybundle_itemuse';
    }


}
