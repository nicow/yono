<?php

namespace Modules\InventoryBundle\Form;


use Modules\InventoryBundle\Entity\ItemUse;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ReturnItemUseType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // set current date
        /** @var ItemUse $itemUse */
        $itemUse = $builder->getData();
        if (!$itemUse->getReturnedAt()) {
            $returnedAt = new \DateTime();
            $returnedAt->sub(new \DateInterval("PT5M")); // go 5 min into the past
            $this->roundTime($returnedAt, 5);
            $itemUse->setReturnedAt($returnedAt);
        }

        $builder->add('returnedAt', DateTimeType::class, ['date_widget' => 'single_text', 'time_widget' => 'choice', 'hours' => [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23], 'minutes' => [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55], 'date_format' => 'd.M.y'])->add('defect', CheckboxType::class, ['label' => 'Defekt', 'data' => $itemUse->getItem()->getDefect(), 'required' => false, 'mapped' => false])->add('note', null, ['label' => 'Anmerkung']);
    }

    function roundTime(\DateTime $datetime, $precision = 30)
    {
        // 1) Set number of seconds to 0 (by rounding up to the nearest minute if necessary)
        $second = (int)$datetime->format("s");
        if ($second > 30) {
            // Jumps to the next minute
            $datetime->add(new \DateInterval("PT" . (60 - $second) . "S"));
        } elseif ($second > 0) {
            // Back to 0 seconds on current minute
            $datetime->sub(new \DateInterval("PT" . $second . "S"));
        }
        // 2) Get minute
        $minute = (int)$datetime->format("i");
        // 3) Convert modulo $precision
        $minute = $minute % $precision;
        if ($minute > 0) {
            // 4) Count minutes to next $precision-multiple minutes
            $diff = $precision - $minute;
            // 5) Add the difference to the original date time
            $datetime->add(new \DateInterval("PT" . $diff . "M"));
        }
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array('data_class' => ItemUse::class));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'modules_inventorybundle_returnitemuse';
    }
}
