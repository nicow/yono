<?php

namespace Modules\InventoryBundle\Form;

use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class ItemType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', null, ['label' => 'Titel'])->add('itemNumber', null, ['label' => 'Artikelnummer'])->add('text', CKEditorType::class, ['label' => 'Beschreibung', 'required' => false])->add('categories', null, ['label' => 'Kategorien'])->add('image', FileType::class, ['label' => 'Bild wählen', 'required' => false, 'constraints' => [new Assert\Image(['maxSize' => '5M'])]])->add('defect', null, ['label' => 'Defekt']);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array('data_class' => 'Modules\InventoryBundle\Entity\Item'));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'modules_inventorybundle_item';
    }


}
