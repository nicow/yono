<?php
/**
 * Created by PhpStorm.
 * User: mkt
 * Date: 04.01.17
 * Time: 20:54
 */

namespace Modules\InventoryBundle\Search;

use Core\AppBundle\Search\SearchItem;
use Doctrine\ORM\EntityManager;
use Modules\InventoryBundle\Entity\ItemUse;
use Modules\InventoryBundle\Service\InventoryHelper;
use Symfony\Bridge\Twig\TwigEngine;
use Symfony\Bundle\FrameworkBundle\Routing\Router;

class Item extends SearchItem
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var TwigEngine
     */
    private $twig;

    /**
     * @var Router
     */
    private $router;

    /**
     * @var InventoryHelper
     */
    private $inventoryHelper;

    public function __construct(EntityManager $em, TwigEngine $twig, Router $router, InventoryHelper $inventoryHelper)
    {
        $this->em = $em;
        $this->twig = $twig;
        $this->router = $router;
        $this->inventoryHelper = $inventoryHelper;
    }

    /**
     * @param $query
     * @return array
     */
    public function getResults($query)
    {
        $items = $this->em->getRepository('ModulesInventoryBundle:Item')->createQueryBuilder('i')->where('i.name LIKE :query')->orWhere('i.text LIKE :query')->orWhere('i.itemNumber LIKE :query')->setParameter('query', '%' . $query . '%')->getQuery()->getResult();

        $results = [];
        if ($items) {
            foreach ($items as $item) {
                $results[] = $this->twig->render('@ModulesInventory/Search/item.html.twig', ['item' => $item]);
            }
        }

        return $results;
    }

    /**
     * @param $query
     * @return array
     */
    public function getAutocompleteResults($query)
    {
        $items = $this->em->getRepository('ModulesInventoryBundle:Item')->createQueryBuilder('i')->where('i.name LIKE :query')->orWhere('i.text LIKE :query')->orWhere('i.itemNumber LIKE :query')->setParameter('query', '%' . $query . '%')->getQuery()->getResult();

        $results = [];
        if ($items) {
            /** @var \Modules\InventoryBundle\Entity\Item $item */
            foreach ($items as $item) {
                $usage = '<span class="uk-badge uk-badge-success">Verfügbar</span>';
                $currentUse = $this->inventoryHelper->getCurrentUse($item);
                if ($currentUse) {
                    if ($currentUse->getStatus() == ItemUse::STATUS_IN_USE) {
                        $usage = '<span class="uk-badge uk-badge-warning">in Verwendung</span>';
                    } elseif ($currentUse->getStatus() == ItemUse::STATUS_IN_REPAIR) {
                        $usage = '<span class="uk-badge uk-badge-warning">in Reparatur</span>';
                    }
                }

                $title = $item->getName();
                if ($item->getItemNumber()) {
                    $title .= ' <small class="uk-text-muted">(' . $item->getItemNumber() . ')</small>';
                }
                $results[] = ['icon' => 'dropbox', 'title' => $title, 'url' => $this->router->generate('modules_inventory_item_show', ['item' => $item->getId()]), 'text' => $usage . ' ' . strip_tags($item->getText())];
            }
        }

        return $results;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'Arbeitsmittel';
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'dropbox';
    }

    public function getRequiredPermissions()
    {
        return ['VIEW' => 'Modules\\InventoryBundle\\Entity\\Item'];
    }
}