<?php

namespace Modules\InventoryBundle\Controller;

use Modules\InventoryBundle\Entity\Category;
use Modules\InventoryBundle\Entity\Item;
use Modules\InventoryBundle\Form\CategoryType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\HttpFoundation\Request;

/**
 * Category controller.
 * @Route("/category")
 */
class CategoryController extends Controller
{
    /**
     * Lists all category entities.
     * @Route("/")
     * @Security("is_granted('VIEW', 'Modules\\InventoryBundle\\Entity\\Category')")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $categories = $em->getRepository('ModulesInventoryBundle:Category')->findAll();

        // get delete forms
        $deleteForms = [];
        foreach ($categories as $category) {
            $deleteForms[$category->getId()] = $this->createDeleteForm($category)->createView();
        }

        return $this->render('ModulesInventoryBundle:Category:index.html.twig', ['categories' => $categories, 'deleteForms' => $deleteForms]);
    }

    /**
     * Creates a form to delete a category entity.
     * @param Category $category The category entity
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Category $category)
    {
        $otherCategories = $this->getDoctrine()->getManager()->getRepository('ModulesInventoryBundle:Category')->getOtherCategories($category);
        $choices = ['nichts (nur die Kategorie entfernen)' => 'remove_category_only', 'Arbeitsmittel auch löschen' => 'delete'];
        /** @var Category $otherCategory */
        foreach ($otherCategories as $otherCategory) {
            $choices['Kategorie "' . $otherCategory->getName() . '" zuweisen'] = $otherCategory->getId();
        }

        return $this->createFormBuilder()->add('items_action', ChoiceType::class, ['choices' => $choices])->setAction($this->generateUrl('modules_inventory_category_delete', ['id' => $category->getId()]))->setMethod('DELETE')->getForm();
    }

    /**
     * Creates a new category entity.
     * @Route("/new")
     * @Security("is_granted('CREATE', 'Modules\\InventoryBundle\\Entity\\Category')")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $category = new Category();
        $category->setShowInMenu(true);
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();

            $this->addFlash('success', '<i class="uk-icon-check"></i> Die Kategorie wurde erfolgreich angelegt!');

            return $this->redirectToRoute('modules_inventory_category_index');
        }

        return $this->render('ModulesInventoryBundle:Category:new.html.twig', ['category' => $category, 'form' => $form->createView(),]);
    }

    /**
     * Displays a form to edit an existing category entity.
     * @Route("/{id}/edit")
     * @Security("is_granted('EDIT', category)")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param Category $category
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @ParamConverter("id", class="Modules\InventoryBundle\Entity\Category")
     */
    public function editAction(Request $request, Category $category)
    {
        $editForm = $this->createForm(CategoryType::class, $category);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('modules_inventory_category_index');
        }

        return $this->render('ModulesInventoryBundle:Category:edit.html.twig', ['category' => $category, 'edit_form' => $editForm->createView(),]);
    }

    /**
     * Deletes a category entity.
     * @Route("/{id}")
     * @Security("is_granted('DELETE', category)")
     * @Method("DELETE")
     * @param Request $request
     * @param Category $category
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, Category $category)
    {
        $form = $this->createDeleteForm($category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $itemsAction = $request->request->get('form')['items_action'];
            if ($itemsAction != 'remove_category_only') {
                // get all items from category and category children
                $items = $em->getRepository('ModulesInventoryBundle:Item')->findByCategory($category);
                if ($itemsAction == 'delete') {
                    // delete them
                    foreach ($items as $item) {
                        $em->remove($item);
                    }
                } elseif ((int)$itemsAction) {
                    // assign new category
                    $newCategory = $em->getRepository('ModulesInventoryBundle:Category')->find((int)$itemsAction);
                    if ($newCategory) {
                        /** @var Item $item */
                        foreach ($items as $item) {
                            if (!$item->getCategories()->contains($newCategory)) {
                                $item->addCategory($newCategory);
                            }
                        }
                    }
                }
            }

            $em->remove($category);
            $em->flush();
        }

        return $this->redirectToRoute('modules_inventory_category_index');
    }
}
