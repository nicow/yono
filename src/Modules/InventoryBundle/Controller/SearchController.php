<?php

namespace Modules\InventoryBundle\Controller;

use Modules\InventoryBundle\Entity\Item;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Item controller.
 * @Route("search")
 */
class SearchController extends Controller
{
    /**
     * Lists all item entities.
     * @Route("/")
     */
    public function indexAction()
    {
        return $this->render('ModulesInventoryBundle:Search:index.html.twig');
    }

    /**
     * @Route("/items.json")
     * @param Request $request
     * @return JsonResponse
     */
    public function jsonItemsAction(Request $request)
    {
        $query = $request->query->get('q', '');
        $page = $request->query->getInt('page', 1);

        $em = $this->getDoctrine()->getManager();

        $items = $em->getRepository('ModulesInventoryBundle:Item')->createQueryBuilder('i')->where('i.name LIKE :query OR i.itemNumber LIKE :query')->setParameter('query', '%' . $query . '%')->getQuery()->getResult();


        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate($items, $page, 10);

        $resultItems = [];
        $itemUseRepo = $em->getRepository('ModulesInventoryBundle:ItemUse');
        /** @var Item $resultItem */
        foreach ($pagination->getItems() as $resultItem) {
            // truncate text
            $text = strip_tags($resultItem->getText());
            if (strlen($text) > 200) {
                $text = substr($text, 0, 200) . '...';
            }

            // get usage
            $currentItemUse = $itemUseRepo->findCurrentForItem($resultItem);
            $usage = 'available';
            if ($currentItemUse) {
                $usage = $currentItemUse->getStatus();
            }

            $resultItems[] = ['id' => $resultItem->getId(), 'name' => $resultItem->getName(), 'itemNumber' => $resultItem->getItemNumber(), 'text' => $text, 'imagePath' => $resultItem->getThumbPath(), 'usage' => $usage, 'disabled' => false];
        }

        $result = ['total_count' => $pagination->getTotalItemCount(), 'items' => $resultItems];

        return new JsonResponse($result);
    }
}
