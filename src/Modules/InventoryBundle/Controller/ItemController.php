<?php

namespace Modules\InventoryBundle\Controller;

use Modules\InventoryBundle\Entity\Category;
use Modules\InventoryBundle\Entity\Item;
use Modules\InventoryBundle\Entity\ItemUse;
use Modules\InventoryBundle\Form\ItemType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Item controller.
 * @Route("item")
 */
class ItemController extends Controller
{
    /**
     * Lists all item entities.
     * @Route("/")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $items = $this->getDoctrine()->getManager()->getRepository('ModulesInventoryBundle:Item')->findAllOrdered($request->query->get('sort', 'name'), $request->query->get('direction', 'ASC'));

        $columns = $this->get('app.helper')->setTableColumns('mod_inventory_table_columns', ['status' => ['active' => 1, 'label' => 'Status'], 'text' => ['active' => 1, 'label' => 'Beschreibung'], 'member' => ['active' => 1, 'label' => 'Mitarbeiter'], 'contact' => ['active' => 1, 'label' => 'Kontakt'], 'project' => ['active' => 1, 'label' => 'Projekt'], 'defect' => ['active' => 1, 'label' => 'Defekt'],]);
        $perPage = $this->get('app.helper')->setTablePerPage('mod_inventory_table_rows');
        $paginator = $this->get('knp_paginator');
        $page = $request->query->getInt('page', 1);
        $pagination = $paginator->paginate($items, $page, $perPage);

        $deleteForms = [];
        foreach ($items as $item) {
            $deleteForms[$item->getId()] = $this->createDeleteForm($item)->createView();
        }

        return $this->render('ModulesInventoryBundle:Item:index.html.twig', ['pagination' => $pagination, 'columns' => $columns, 'perPage' => $perPage, 'deleteForms' => $deleteForms]);
    }

    /**
     * Creates a form to delete a item entity.
     * @param Item $item The item entity
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Item $item)
    {
        return $this->createFormBuilder()->setAction($this->generateUrl('modules_inventory_item_delete', ['item' => $item->getId()]))->setMethod('DELETE')->getForm();
    }

    /**
     * Lists all item entities from a given category.
     * @Route("/category/{category}")
     * @Method("GET")
     * @param Request $request
     * @param Category $category
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function categoryIndexAction(Request $request, Category $category)
    {
        $items = $this->getDoctrine()->getManager()->getRepository('ModulesInventoryBundle:Item')->findByCategory($category, $request->query->get('sort', 'name'), $request->query->get('direction', 'ASC'));

        $columns = $this->get('app.helper')->setTableColumns('mod_inventory_table_columns', ['status' => ['active' => 1, 'label' => 'Status'], 'text' => ['active' => 1, 'label' => 'Beschreibung'], 'member' => ['active' => 1, 'label' => 'Mitarbeiter'], 'contact' => ['active' => 1, 'label' => 'Kontakt'], 'project' => ['active' => 1, 'label' => 'Projekt'], 'defect' => ['active' => 1, 'label' => 'Defekt'],]);
        $perPage = $this->get('app.helper')->setTablePerPage('mod_inventory_table_rows');
        $paginator = $this->get('knp_paginator');
        $page = $request->query->getInt('page', 1);
        $pagination = $paginator->paginate($items, $page, $perPage);

        $deleteForms = [];
        foreach ($items as $item) {
            $deleteForms[$item->getId()] = $this->createDeleteForm($item)->createView();
        }

        return $this->render('ModulesInventoryBundle:Item:index_category.html.twig', ['category' => $category, 'pagination' => $pagination, 'columns' => $columns, 'perPage' => $perPage, 'deleteForms' => $deleteForms]);
    }

    /**
     * Creates a new item entity.
     * @Route("/new")
     * @Route("/category/{category}/new")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param Category $category
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request, Category $category = null)
    {
        $item = new Item();
        if ($category) {
            $item->addCategory($category);
        }
        $form = $this->createForm(ItemType::class, $item);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($form->get('image')->getData()) {
                $uploadManager = $this->get('stof_doctrine_extensions.uploadable.manager');
                $uploadManager->markEntityToUpload($item, $form->get('image')->getData());
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($item);
            $em->flush();

            $this->addFlash('success', '<i class="uk-icon-check"></i> Das Arbeitsmittel wurde erfolgreich angelegt!');

            if ($category) {
                return $this->redirectToRoute('modules_inventory_item_show_1', ['item' => $item->getId(), 'category' => $category->getId()]);
            }

            return $this->redirectToRoute('modules_inventory_item_show', ['item' => $item->getId()]);
        }

        return $this->render('ModulesInventoryBundle:Item:new.html.twig', ['category' => $category, 'item' => $item, 'form' => $form->createView(),]);
    }

    /**
     * Finds and displays a item entity.
     * @Route("/{item}")
     * @Route("/category/{category}/{item}")
     * @Method("GET")
     * @param Request $request
     * @param Item $item
     * @param Category $category
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Request $request, Item $item, Category $category = null)
    {
        $itemUses = $this->getDoctrine()->getManager()->getRepository('ModulesInventoryBundle:ItemUse')->findForItem($item, $request->query->get('sort', 'start'), $request->query->get('direction', 'DESC'));
        $columns = $this->get('app.helper')->setTableColumns('mod_inventory_itemuse_table_columns', ['start' => ['active' => 1, 'label' => 'Von'], 'end' => ['active' => 1, 'label' => 'Bis'], 'member' => ['active' => 1, 'label' => 'Mitarbeiter'], 'contact' => ['active' => 1, 'label' => 'Kontakt'], 'project' => ['active' => 1, 'label' => 'Projekt'], 'note' => ['active' => 1, 'label' => 'Anmerkung'], 'returnedAt' => ['active' => 1, 'label' => 'zurück seit'],]);
        $perPage = $this->get('app.helper')->setTablePerPage('mod_inventory_itemuse_table_rows');
        $paginator = $this->get('knp_paginator');
        $page = $request->query->getInt('page', 1);
        $pagination = $paginator->paginate($itemUses, $page, $perPage);

        $deleteForms = [];
        foreach ($itemUses as $itemUse) {
            $deleteForms[$itemUse->getId()] = $this->createItemUseDeleteForm($item, $itemUse)->createView();
        }

        $deleteForm = $this->createDeleteForm($item)->createView();

        return $this->render('ModulesInventoryBundle:Item:show.html.twig', ['category' => $category, 'item' => $item, 'pagination' => $pagination, 'columns' => $columns, 'perPage' => $perPage, 'deleteForms' => $deleteForms, 'deleteForm' => $deleteForm]);
    }

    /**
     * Creates a form to delete a itemUse entity.
     * @param Item $item
     * @param ItemUse $itemUse
     * @return \Symfony\Component\Form\Form|\Symfony\Component\Form\FormInterface
     */
    private function createItemUseDeleteForm(Item $item, ItemUse $itemUse)
    {
        return $this->createFormBuilder()->setAction($this->generateUrl('modules_inventory_itemuse_delete', ['item' => $item->getId(), 'id' => $itemUse->getId()]))->setMethod('DELETE')->getForm();
    }

    /**
     * Displays a form to edit an existing item entity.
     * @Route("/{item}/edit")
     * @Route("/category/{category}/{item}/edit")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param Item $item
     * @param Category $category
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @ParamConverter("item", class="Modules\InventoryBundle\Entity\Item")
     */
    public function editAction(Request $request, Item $item, Category $category = null)
    {
        $deleteForm = $this->createDeleteForm($item);
        $editForm = $this->createForm('Modules\InventoryBundle\Form\ItemType', $item);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            if ($editForm->get('image')->getData()) {
                $uploadManager = $this->get('stof_doctrine_extensions.uploadable.manager');
                $uploadManager->markEntityToUpload($item, $editForm->get('image')->getData());
            }
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', '<i class="uk-icon-check"></i> Das Arbeitsmittel wurde erfolgreich aktualisiert!');

            if ($category) {
                return $this->redirectToRoute('modules_inventory_item_edit_1', ['item' => $item->getId(), 'category' => $category->getId()]);
            }

            return $this->redirectToRoute('modules_inventory_item_show', ['item' => $item->getId()]);
        }

        return $this->render('ModulesInventoryBundle:Item:edit.html.twig', ['category' => $category, 'item' => $item, 'edit_form' => $editForm->createView(), 'delete_form' => $deleteForm->createView(),]);
    }

    /**
     * Deletes a item entity.
     * @Route("/{item}")
     * @Method("DELETE")
     * @param Request $request
     * @param Item $item
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, Item $item)
    {
        $form = $this->createDeleteForm($item);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->remove($item);
            $em->flush();

            // remove thumb (main image gets delete by gedmo uploadable extension)
            if ($item->getThumbPath()) {
                $thumbPath = $this->getParameter('kernel.root_dir') . '/../web/' . $item->getThumbPath();
                if ($this->get('filesystem')->exists($thumbPath) && !is_dir($thumbPath)) {
                    $this->get('filesystem')->remove($thumbPath);
                }
            }

            $this->addFlash('success', '<i class="uk-icon-check"></i> Das Arbeitsmittel wurde erfolgreich gelöscht!');
        }

        return $this->redirectToRoute('modules_inventory_item_index');
    }
}
