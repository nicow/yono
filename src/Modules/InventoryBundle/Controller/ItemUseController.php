<?php

namespace Modules\InventoryBundle\Controller;

use Modules\InventoryBundle\Entity\Category;
use Modules\InventoryBundle\Entity\Item;
use Modules\InventoryBundle\Entity\ItemUse;
use Modules\InventoryBundle\Form\ItemUseType;
use Modules\InventoryBundle\Form\ReturnItemUseType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Itemuse controller.
 * @Route("/{item}/itemuse")
 */
class ItemUseController extends Controller
{
    /**
     * Creates a new itemUse entity.
     * @Route("/new", options={"expose"=true})
     * @Route("/category/{category}/new", options={"expose"=true})
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param Item $item
     * @param Category $category
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request, Item $item, Category $category = null)
    {
        // redirect to return form if item is currently in use
        if ($currentUse = $this->get('mod.inventory.helper')->getCurrentUse($item)) {
            if ($category) {
                return $this->redirectToRoute('modules_inventory_itemuse_return_1', ['item' => $item->getId(), 'itemUse' => $currentUse->getId(), 'category' => $category->getId()]);
            }

            return $this->redirectToRoute('modules_inventory_itemuse_return', ['item' => $item->getId(), 'itemUse' => $currentUse->getId()]);
        }

        // else handle new form
        $itemUse = new Itemuse();
        $itemUse->setItem($item);
        if ($this->getUser()->getMember()) {
            $itemUse->setMember($this->getUser()->getMember());
        }
        $form = $this->createForm(ItemUseType::class, $itemUse);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($itemUse);
            $em->flush();

            $this->addFlash('success', '<i class="uk-icon-check"></i> Die Verwendung wurde erfolgreich eingetragen.');

            if ($category) {
                return $this->redirectToRoute('modules_inventory_item_show_1', ['item' => $item->getId(), 'category' => $category->getId()]);
            }

            return $this->redirectToRoute('modules_inventory_item_show', ['item' => $item->getId()]);
        }

        return $this->render('ModulesInventoryBundle:ItemUse:new.html.twig', ['category' => $category, 'item' => $item, 'itemUse' => $itemUse, 'form' => $form->createView(),]);
    }

    /**
     * Displays a form to edit an existing itemUse entity.
     * @Route("/{itemUse}/edit")
     * @Route("/category/{category}/{itemUse}/edit")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param Item $item
     * @param ItemUse $itemUse
     * @param Category $category
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @ParamConverter("itemUse", class="Modules\InventoryBundle\Entity\ItemUse")
     */
    public function editAction(Request $request, Item $item, ItemUse $itemUse, Category $category = null)
    {
        $deleteForm = $this->createDeleteForm($item, $itemUse);
        $editForm = $this->createForm(ItemUseType::class, $itemUse);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', '<i class="uk-icon-check"></i> Die Verwendung wurde erfolgreich aktualisiert.');

            if ($category) {
                return $this->redirectToRoute('modules_inventory_item_show_1', ['item' => $item->getId(), 'category' => $category->getId()]);
            }

            return $this->redirectToRoute('modules_inventory_item_show', ['item' => $item->getId()]);
        }

        return $this->render('ModulesInventoryBundle:ItemUse:edit.html.twig', ['category' => $category, 'item' => $item, 'itemUse' => $itemUse, 'edit_form' => $editForm->createView(), 'delete_form' => $deleteForm->createView(),]);
    }

    /**
     * Creates a form to delete a itemUse entity.
     * @param Item $item
     * @param ItemUse $itemUse The itemUse entity
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Item $item, ItemUse $itemUse)
    {
        return $this->createFormBuilder()->setAction($this->generateUrl('modules_inventory_itemuse_delete', ['item' => $item->getId(), 'id' => $itemUse->getId()]))->setMethod('DELETE')->getForm();
    }

    /**
     * Displays a form to edit the returnedAt property of an existing itemUse entity.
     * @Route("/{itemUse}/return")
     * @Route("/category/{category}/{itemUse}/return")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param Item $item
     * @param ItemUse $itemUse
     * @param Category $category
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function returnAction(Request $request, Item $item, ItemUse $itemUse, Category $category = null)
    {
        $editForm = $this->createForm(ReturnItemUseType::class, $itemUse);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            // set defect status on item
            $item->setDefect($editForm->get('defect')->getData());

            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', '<i class="uk-icon-check"></i> Die Verwendung wurde erfolgreich abgeschlossen. Sie können nun eine neue Verwendung eintragen.');

            if ($category) {
                return $this->redirectToRoute('modules_inventory_itemuse_new_1', ['item' => $item->getId(), 'category' => $category->getId()]);
            }

            return $this->redirectToRoute('modules_inventory_itemuse_new', ['item' => $item->getId()]);
        }

        return $this->render('ModulesInventoryBundle:ItemUse:return.html.twig', ['category' => $category, 'item' => $item, 'itemUse' => $itemUse, 'edit_form' => $editForm->createView(),]);
    }

    /**
     * Deletes a itemUse entity.
     * @Route("/{id}")
     * @Method("DELETE")
     * @param Request $request
     * @param Item $item
     * @param ItemUse $itemUse
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, Item $item, ItemUse $itemUse)
    {
        $form = $this->createDeleteForm($item, $itemUse);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($itemUse);
            $em->flush();
        }

        return $this->redirectToRoute('modules_inventory_item_show', ['item' => $item->getId()]);
    }

    /**
     * @Route("/modal/{itemUse}")
     * @Method("GET")
     * @param ItemUse $itemUse
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function modalAction(ItemUse $itemUse)
    {
        return $this->render('ModulesInventoryBundle:ItemUse:modal.html.twig', ['itemUse' => $itemUse]);
    }
}
