<?php

namespace Modules\InventoryBundle\Entity;

use Core\AppBundle\Annotation\Acl;
use Core\AppBundle\Entity\BlameableTrait;
use Doctrine\ORM\Mapping as ORM;
use Modules\ContactBundle\Entity\Contact;
use Modules\ProjectBundle\Entity\Project;
use Modules\TeamBundle\Entity\Member;

/**
 * ItemUse
 * @Acl("Arbeitsmittel Verwendung")
 * @ORM\Table(name="mod_inventory_item_use")
 * @ORM\Entity(repositoryClass="Modules\InventoryBundle\Repository\ItemUseRepository")
 */
class ItemUse
{
    use BlameableTrait;

    const STATUS_IN_USE = 'in_use';
    const STATUS_IN_REPAIR = 'in_repair';

    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="status", type="string", length=50)
     */
    private $status;

    /**
     * @var \DateTime
     * @ORM\Column(name="returned_at", type="datetime", nullable=true)
     */
    private $returnedAt;

    /**
     * @var \DateTime
     * @ORM\Column(name="start", type="datetime")
     */
    private $start;

    /**
     * @var \DateTime
     * @ORM\Column(name="end", type="datetime")
     */
    private $end;

    /**
     * @var Item
     * @ORM\ManyToOne(targetEntity="Modules\InventoryBundle\Entity\Item", inversedBy="itemUses")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $item;

    /**
     * @var Member
     * @ORM\ManyToOne(targetEntity="Modules\TeamBundle\Entity\Member")
     */
    private $member;

    /**
     * @var Contact
     * @ORM\ManyToOne(targetEntity="Modules\ContactBundle\Entity\Contact")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $contact;

    /**
     * @var Project
     * @ORM\ManyToOne(targetEntity="Modules\ProjectBundle\Entity\Project")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $project;

    /**
     * @var string
     * @ORM\Column(name="note", type="text", nullable=true)
     */
    private $note;

    public function __construct()
    {
        $this->start = new \DateTime();
        $this->roundTime($this->start, 5);
        $this->end = new \DateTime();
        $this->roundTime($this->end);
    }

    function roundTime(\DateTime $datetime, $precision = 30)
    {
        // 1) Set number of seconds to 0 (by rounding up to the nearest minute if necessary)
        $second = (int)$datetime->format("s");
        if ($second > 30) {
            // Jumps to the next minute
            $datetime->add(new \DateInterval("PT" . (60 - $second) . "S"));
        } elseif ($second > 0) {
            // Back to 0 seconds on current minute
            $datetime->sub(new \DateInterval("PT" . $second . "S"));
        }
        // 2) Get minute
        $minute = (int)$datetime->format("i");
        // 3) Convert modulo $precision
        $minute = $minute % $precision;
        if ($minute > 0) {
            // 4) Count minutes to next $precision-multiple minutes
            $diff = $precision - $minute;
            // 5) Add the difference to the original date time
            $datetime->add(new \DateInterval("PT" . $diff . "M"));
        }
    }

    /**
     * Get id
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get status
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set status
     * @param string $status
     * @return ItemUse
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get start
     * @return \DateTime
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Set start
     * @param \DateTime $start
     * @return ItemUse
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * Get end
     * @return \DateTime
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * Set end
     * @param \DateTime $end
     * @return ItemUse
     */
    public function setEnd($end)
    {
        $this->end = $end;

        return $this;
    }

    /**
     * Get item
     * @return Item
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * Set item
     * @param Item $item
     * @return ItemUse
     */
    public function setItem(Item $item = null)
    {
        $this->item = $item;

        return $this;
    }

    /**
     * Get contact
     * @return Contact
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set contact
     * @param Contact $contact
     * @return ItemUse
     */
    public function setContact(Contact $contact = null)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get project
     * @return Project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Set project
     * @param Project $project
     * @return ItemUse
     */
    public function setProject(Project $project = null)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get note
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set note
     * @param string $note
     * @return ItemUse
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get member
     * @return Member
     */
    public function getMember()
    {
        return $this->member;
    }

    /**
     * Set member
     * @param Member $member
     * @return ItemUse
     */
    public function setMember(Member $member = null)
    {
        $this->member = $member;

        return $this;
    }

    /**
     * Get returnedAt
     * @return \DateTime
     */
    public function getReturnedAt()
    {
        return $this->returnedAt;
    }

    /**
     * Set returnedAt
     * @param \DateTime $returnedAt
     * @return ItemUse
     */
    public function setReturnedAt($returnedAt)
    {
        $this->returnedAt = $returnedAt;

        return $this;
    }
}
