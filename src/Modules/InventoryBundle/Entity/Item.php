<?php

namespace Modules\InventoryBundle\Entity;

use Core\AppBundle\Annotation\Acl;
use Core\AppBundle\Entity\BlameableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Imagine\Gd\Imagine;
use Imagine\Image\Box;
use Imagine\Image\ImageInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Item
 * @Acl("Arbeitsmittel")
 * @ORM\Table(name="mod_inventory_item")
 * @ORM\Entity(repositoryClass="Modules\InventoryBundle\Repository\ItemRepository")
 * @Gedmo\Uploadable(path="uploads/inventory/items", appendNumber=true, callback="resizeImage")
 */
class Item
{
    use BlameableTrait;

    protected $image;
    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var string
     * @ORM\Column(name="item_number", type="string", nullable=true)
     */
    private $itemNumber;
    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;
    /**
     * @var string
     * @ORM\Column(name="text", type="text", nullable=true)
     */
    private $text;
    /**
     * @var Category[]
     * @ORM\ManyToMany(targetEntity="Modules\InventoryBundle\Entity\Category", inversedBy="items")
     * @ORM\JoinTable(name="mod_inventory_items_categories")
     */
    private $categories;
    /**
     * @var ItemUse[]
     * @ORM\OneToMany(targetEntity="Modules\InventoryBundle\Entity\ItemUse", mappedBy="item")
     */
    private $itemUses;
    /**
     * @var string
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     * @Gedmo\UploadableFilePath
     */
    private $imagePath;
    /**
     * @var string
     * @ORM\Column(name="thumb", type="string", length=255, nullable=true)
     */
    private $thumbPath;
    /**
     * @var bool
     * @ORM\Column(name="defect", type="boolean", nullable=true)
     */
    private $defect;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->categories = new ArrayCollection();
        $this->itemUses = new ArrayCollection();
    }

    /**
     * Get id
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     * @param string $name
     * @return Item
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get text
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set text
     * @param string $text
     * @return Item
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Add category
     * @param Category $category
     * @return Item
     */
    public function addCategory(Category $category)
    {
        $this->categories[] = $category;

        return $this;
    }

    /**
     * Remove category
     * @param Category $category
     */
    public function removeCategory(Category $category)
    {
        $this->categories->removeElement($category);
    }

    /**
     * Get categories
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * Get image
     * @return UploadedFile
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set image
     * @param UploadedFile $image
     * @return Item
     */
    public function setImage(UploadedFile $image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return string
     */
    public function getImagePath()
    {
        return $this->imagePath;
    }

    /**
     * @param string $imagePath
     * @return $this
     */
    public function setImagePath($imagePath)
    {
        $this->imagePath = $imagePath;

        return $this;
    }

    /**
     * @return string
     */
    public function getThumbPath()
    {
        return $this->thumbPath;
    }

    /**
     * @param string $thumbPath
     * @return $this
     */
    public function setThumbPath($thumbPath)
    {
        $this->thumbPath = $thumbPath;

        return $this;
    }

    /**
     * Add itemUse
     * @param ItemUse $itemUse
     * @return Item
     */
    public function addItemUse(ItemUse $itemUse)
    {
        $this->itemUses[] = $itemUse;

        return $this;
    }

    /**
     * Remove itemUse
     * @param ItemUse $itemUse
     */
    public function removeItemUse(ItemUse $itemUse)
    {
        $this->itemUses->removeElement($itemUse);
    }

    /**
     * Get itemUses
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getItemUses()
    {
        return $this->itemUses;
    }

    public function resizeImage($fileInfo)
    {
        $imagine = new Imagine();

        // image
        $imagine->open($fileInfo['filePath'])->thumbnail(new Box(800, 800), ImageInterface::THUMBNAIL_INSET)->save($fileInfo['filePath'], ['quality' => 50]);

        // thumb
        $thumbFileName = $fileInfo['fileWithoutExt'] . '-thumb' . $fileInfo['fileExtension'];
        $imagine->open($fileInfo['filePath'])->thumbnail(new Box(100, 100), ImageInterface::THUMBNAIL_OUTBOUND)->save($thumbFileName);
        $this->setThumbPath($thumbFileName);
    }

    /**
     * Get defect
     * @return boolean
     */
    public function getDefect()
    {
        return $this->defect;
    }

    /**
     * Set defect
     * @param boolean $defect
     * @return Item
     */
    public function setDefect($defect)
    {
        $this->defect = $defect;

        return $this;
    }

    /**
     * Get itemNumber
     * @return string
     */
    public function getItemNumber()
    {
        return $this->itemNumber;
    }

    /**
     * Set itemNumber
     * @param string $itemNumber
     * @return Item
     */
    public function setItemNumber($itemNumber)
    {
        $this->itemNumber = $itemNumber;

        return $this;
    }
}
