<?php

namespace Modules\InventoryBundle\Repository;


use Doctrine\ORM\EntityRepository;
use Modules\InventoryBundle\Entity\Item;
use Modules\InventoryBundle\Entity\ItemUse;
use Modules\TeamBundle\Entity\Member;

/**
 * ItemUseRepository
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ItemUseRepository extends EntityRepository
{
    /**
     * @param Item $item
     * @param string $sort
     * @param string $direction
     * @return ItemUse[]
     */
    public function findForItem(Item $item, $sort = 'date', $direction = 'ASC')
    {
        $direction = strtoupper($direction);

        $qb = $this->createQueryBuilder('iu')->leftJoin('iu.member', 'm')->leftJoin('iu.contact', 'c')->leftJoin('iu.project', 'p')->where('iu.item = :item')->setParameter('item', $item);

        switch ($sort) {
            case 'status':
                $qb->addOrderBy('iu.status', $direction);
                break;
            case 'start':
                $qb->addOrderBy('iu.start', $direction);
                $qb->addOrderBy('iu.createdAt', $direction);
                break;
            case 'end':
                $qb->addOrderBy('iu.end', $direction);
                $qb->addOrderBy('iu.createdAt', $direction);
                break;
            case 'returnedAt':
                $qb->addOrderBy('iu.returnedAt', $direction);
                break;
            case 'member':
                $qb->addOrderBy('m.lastname', $direction);
                break;
            case 'contact':
                $qb->addOrderBy('c.name', $direction);
                break;
            case 'project':
                $qb->addOrderBy('p.name', $direction);
                break;
            case 'note':
                $qb->addOrderBy('iu.note', $direction);
                break;
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @param Item $item
     * @return ItemUse|null
     */
    public function findCurrentForItem(Item $item)
    {
        return $this->createQueryBuilder('iu')->where('iu.item = :item')->andWhere('iu.returnedAt IS NULL')->setParameter('item', $item)->orderBy('iu.start', 'DESC')->setMaxResults(1)->getQuery()->getOneOrNullResult();
    }

    /**
     * @param \DateTime $month
     * @return ItemUse[]
     */
    public function findForMonth(\DateTime $month)
    {
        return $this->createQueryBuilder('iu')->where('(YEAR(iu.start) = YEAR(:month) AND MONTH(iu.start) = MONTH(:month)) OR (YEAR(iu.end) = YEAR(:month) AND MONTH(iu.end) = MONTH(:month))')->setParameter('month', $month)->getQuery()->getResult();
    }

    /**
     * @param \DateTime $day
     * @return ItemUse[]
     */
    public function findForDay(\DateTime $day)
    {
        return $this->createQueryBuilder('iu')->where('DATE(iu.start) = DATE(:day) OR DATE(iu.end) = DATE(:day) OR (DATE(:day) BETWEEN DATE(iu.start) AND DATE(iu.end))')->setParameter('day', $day)->getQuery()->getResult();
    }

    /**
     * @param \DateTime $day
     * @param Member $member
     * @param           $filter
     * @return ItemUse[]
     */
    public function findForDayAndMemberByFilter(\DateTime $day, Member $member, $filter)
    {
        $qb = $this->createQueryBuilder('iu')->where('iu.member = :member')
            ->andWhere('DATE(iu.start) = DATE(:day) OR DATE(iu.end) = DATE(:day) OR (DATE(:day) BETWEEN DATE(iu.start) AND DATE(iu.end))')
            ->setParameter('day', $day)->setParameter('member', $member);

        if (!isset($filter[ItemUse::STATUS_IN_USE])) {
            $qb->andWhere('iu.status != :statusInUse')->setParameter('statusInUse', ItemUse::STATUS_IN_USE);
        }
        if (!isset($filter[ItemUse::STATUS_IN_REPAIR])) {
            $qb->andWhere('iu.status != :statusInRepair')->setParameter('statusInRepair', ItemUse::STATUS_IN_REPAIR);
        }

        return $qb->getQuery()->getResult();
    }
}
