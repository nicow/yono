<?php

namespace Modules\InventoryBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Modules\InventoryBundle\Entity\Category;

/**
 * CategoryRepository
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class CategoryRepository extends EntityRepository
{
    public function getOtherCategories(Category $category)
    {
        return $this->createQueryBuilder('c')->where('c.id != :id')->setParameter('id', $category->getId())->getQuery()->getResult();
    }
}
