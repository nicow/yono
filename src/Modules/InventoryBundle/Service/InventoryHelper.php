<?php
/**
 * Created by PhpStorm.
 * User: mkt
 * Date: 31.12.16
 * Time: 04:09
 */

namespace Modules\InventoryBundle\Service;


use Doctrine\ORM\EntityManager;
use Modules\InventoryBundle\Entity\Category;
use Modules\InventoryBundle\Entity\Item;
use Symfony\Component\HttpFoundation\RequestStack;

class InventoryHelper
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var RequestStack
     */
    private $requestStack;

    public function __construct(EntityManager $em, RequestStack $requestStack)
    {
        $this->em = $em;
        $this->requestStack = $requestStack;
        $this->request = $this->requestStack->getCurrentRequest();
    }

    /**
     * @return Category|null
     */
    public function getCurrentCategory()
    {
        if ($this->request && $this->request->attributes->has('_route') && strpos($this->request->attributes->get('_route'), 'modules_wiki') !== false && $this->request->attributes->has('category') && $this->request->attributes->get('category') instanceof Category) {
            return $this->request->attributes->get('category');
        }

        return null;
    }

    public function getItemCountForCategory(Category $category)
    {
        $articles = $this->em->getRepository('ModulesInventoryBundle:Item')->findByCategory($category);

        return count($articles);
    }

    public function getCurrentUse(Item $item)
    {
        return $this->em->getRepository('ModulesInventoryBundle:ItemUse')->findCurrentForItem($item);
    }
}