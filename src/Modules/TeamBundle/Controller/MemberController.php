<?php

namespace Modules\TeamBundle\Controller;

use Modules\TeamBundle\Entity\Category;
use Modules\TeamBundle\Entity\Member;
use Modules\TeamBundle\Entity\Plan;
use Modules\TeamBundle\Form\MemberType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MemberController extends Controller
{
    /**
     * @Route("/")
     * @Security("is_granted('VIEW', 'Modules\\TeamBundle\\Entity\\Member')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $members = $this->getDoctrine()->getManager()->getRepository('ModulesTeamBundle:Member')->findAllOrdered($request->query->get('sort', 'lastname'), $request->query->get('direction', 'ASC'));

        if ($this->getUser()->hasRole('ROLE_ADMIN') or $this->getUser()->hasRole('ROLE_SUPER_ADMIN')) {

            $columns = $this->get('app.helper')->setTableColumns('mod_team_member_table_columns', ['phone_email' => ['active' => 1,
                'label' => 'Kontaktdaten'],
                'address' => ['active' => 1, 'label' => 'Adresse'],
                'birthday' => ['active' => 1, 'label' => 'Geburtstag'],
                'joinedTeam' => ['active' => 1, 'label' => 'Eintritt'],
                'leftTeam' => ['active' => 1, 'label' => 'Austritt'],
                'holidayClaim' => ['active' => 1, 'label' => 'Urlaubsanspruch'],
                'bankAccount' => ['active' => 1, 'label' => 'Kontoverbindung'],
                'wageTaxClass' => ['active' => 1, 'label' => 'LStkl.'],
                'children' => ['active' => 1, 'label' => 'Kinder'],]);

        } else {
            $columns = $this->get('app.helper')->setTableColumns('mod_team_member_table_columns', ['phone_email' => ['active' => 1,
                'label' => 'Kontaktdaten'],
                'address' => ['active' => 1, 'label' => 'Adresse'],
                'birthday' => ['active' => 1, 'label' => 'Geburtstag'],
                'joinedTeam' => ['active' => 0, 'label' => 'Eintritt', 'hidden' => 1],
                'leftTeam' => ['active' => 0, 'label' => 'Austritt', 'hidden' => 1],
                'holidayClaim' => ['active' => 0, 'label' => 'Urlaubsanspruch', 'hidden' => 1],
                'bankAccount' => ['active' => 0, 'label' => 'Kontoverbindung', 'hidden' => 1],
                'wageTaxClass' => ['active' => 0, 'label' => 'LStkl.', 'hidden' => 1],
                'children' => ['active' => 0, 'label' => 'Kinder', 'hidden' => 1],]);

        }


        $perPage = $this->get('app.helper')->setTablePerPage('mod_team_member_table_rows');
        $paginator = $this->get('knp_paginator');
        $page = $request->query->getInt('page', 1);
        $pagination = $paginator->paginate($members, $page, $perPage);

        $deleteForms = [];
        foreach ($members as $member) {
            $deleteForms[$member->getId()] = $this->createDeleteForm($member)->createView();
        }

        return $this->render('ModulesTeamBundle:Member:index.html.twig', ['columns' => $columns,
            'pagination' => $pagination,
            'perPage' => $perPage,
            'deleteForms' => $deleteForms,]);
    }

    /**
     * Creates a form to delete a member entity.
     * @param Member $member The member entity
     * @param Category $category
     * @return Form The form
     */
    private function createDeleteForm(Member $member, Category $category = null)
    {
        return $this->createFormBuilder()->setAction($category ? $this->generateUrl('modules_team_member_delete_1', ['member' => $member->getId(),
            'category' => $category->getId()]) : $this->generateUrl('modules_team_member_delete', ['member' => $member->getId()]))->setMethod('DELETE')->getForm();
    }

    /**
     * @Route("/category/{category}")
     * @Security("is_granted('VIEW', 'Modules\\TeamBundle\\Entity\\Member')")
     * @param Request $request
     * @param Category $category
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function categoryIndexAction(Request $request, Category $category)
    {
        $members = $this->getDoctrine()->getManager()->getRepository('ModulesTeamBundle:Member')->findByCategory($category, $request->query->get('sort', 'lastname'), $request->query->get('direction', 'ASC'));

        if ($this->getUser()->hasRole('ROLE_ADMIN') or $this->getUser()->hasRole('ROLE_SUPER_ADMIN')) {

            $columns = $this->get('app.helper')->setTableColumns('mod_team_member_table_columns', ['phone_email' => ['active' => 1,
                'label' => 'Kontaktdaten'],
                'address' => ['active' => 1, 'label' => 'Adresse'],
                'birthday' => ['active' => 1, 'label' => 'Geburtstag'],
                'joinedTeam' => ['active' => 1, 'label' => 'Eintritt'],
                'leftTeam' => ['active' => 1, 'label' => 'Austritt'],
                'holidayClaim' => ['active' => 1, 'label' => 'Urlaubsanspruch'],
                'bankAccount' => ['active' => 1, 'label' => 'Kontoverbindung'],
                'wageTaxClass' => ['active' => 1, 'label' => 'LStkl.'],
                'children' => ['active' => 1, 'label' => 'Kinder'],]);

        } else {

            $columns = $this->get('app.helper')->setTableColumns('mod_team_member_table_columns', ['phone_email' => ['active' => 1,
                'label' => 'Kontaktdaten'],
                'address' => ['active' => 1, 'label' => 'Adresse'],
                'birthday' => ['active' => 1, 'label' => 'Geburtstag']]);

        }

        $perPage = $this->get('app.helper')->setTablePerPage('mod_team_member_table_rows');
        $paginator = $this->get('knp_paginator');
        $page = $request->query->getInt('page', 1);
        $pagination = $paginator->paginate($members, $page, $perPage);

        $deleteForms = [];
        foreach ($members as $member) {
            $deleteForms[$member->getId()] = $this->createDeleteForm($member)->createView();
        }

        return $this->render('ModulesTeamBundle:Member:index_category.html.twig', ['category' => $category,
            'columns' => $columns,
            'pagination' => $pagination,
            'perPage' => $perPage,
            'deleteForms' => $deleteForms]);
    }

    /**
     * @Route("/{member}/details")
     * @Route("/category/{category}/{member}/details")
     * @Security("is_granted('VIEW', 'Modules\\TeamBundle\\Entity\\Member')")
     * @param Member $member
     * @param Category $category
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function detailsAction(Request $request, Member $member, Category $category = null)
    {
        $em = $this->getDoctrine()->getManager();

        $planRepository = $em->getRepository('ModulesTeamBundle:Plan');

        $plans = $em->getRepository('ModulesTeamBundle:Plan')->findAllByMember($request->query->get('sort', 'day'), $request->query->get('direction', 'DESC'), $member);

        $columns = $this->get('app.helper')->setTableColumns('mod_team_member_plan_table_columns', ['type' => ['active' => 1,
            'label' => 'Art'],
            'day' => ['active' => 1, 'label' => 'Tag'],
            'note' => ['active' => 1, 'label' => 'Anmerkung'],]);

        $perPage = $this->get('app.helper')->setTablePerPage('mod_team_plan_table_rows');
        $paginator = $this->get('knp_paginator');
        $page = $request->query->getInt('page', 1);
        $pagination = $paginator->paginate($plans, $page, $perPage);

        $saturdays = $planRepository->findForTypeAndDayOfWeek($member, Plan::TYPE_HOLIDAY, Plan::DAY_SATURDAY);
        $sundays = $planRepository->findForTypeAndDayOfWeek($member, Plan::TYPE_HOLIDAY, Plan::DAY_SUNDAY);

        return $this->render('ModulesTeamBundle:Member:details.html.twig', ['category' => $category,
            'member' => $member,
            'columns' => $columns,
            'pagination' => $pagination,
            'perPage' => $perPage,
            'saturdays' => $saturdays,
            'sundays' => $sundays]);
    }

    /**
     * Creates a new member entity.
     * @Route("/new")
     * @Route("/category/{category}/new")
     * @Method({"GET", "POST"})
     * @Security("is_granted('CREATE', 'Modules\\TeamBundle\\Entity\\Member')")
     * @param Request $request
     * @param Category $category
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request, Category $category = null)
    {
        $member = new Member();
        if ($category) {
            $member->addCategory($category);
        }
        $form = $this->createForm(MemberType::class, $member);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($member);
            $em->flush();

            $this->addFlash('success', '<i class="uk-icon-check"></i> Der Mitarbeiter wurde erfolgreich angelegt!');

            if ($category) {
                return $this->redirectToRoute('modules_team_member_details_1', ['member' => $member->getId(),
                    'category' => $category->getId()]);
            }

            return $this->redirectToRoute('modules_team_member_details', ['member' => $member->getId()]);
        }

        return $this->render('ModulesTeamBundle:Member:new.html.twig', ['category' => $category,
            'member' => $member,
            'form' => $form->createView(),]);
    }

    /**
     * Displays a form to edit an existing member entity.
     * @Route("/{member}/edit")
     * @Route("/category/{category}/{member}/edit")
     * @Method({"GET", "POST"})
     * @Security("is_granted('EDIT', member)")
     * @param Request $request
     * @param Member $member
     * @param Category $category
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @ParamConverter("member", class="Modules\TeamBundle\Entity\Member")
     */
    public function editAction(Request $request, Member $member, Category $category = null)
    {
        $em = $this->getDoctrine()->getManager();
        $deleteForm = $this->createDeleteForm($member);

        $editForm = $this->createForm(MemberType::class, $member);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em->flush();

            $this->addFlash('success', '<i class="uk-icon-check"></i> Der Mitarbeiter wurde erfolgreich aktualisiert!');

            if ($category) {
                return $this->redirectToRoute('modules_team_member_details_1', ['member' => $member->getId(),
                    'category' => $category->getId()]);
            }

            return $this->redirectToRoute('modules_team_member_details', ['member' => $member->getId()]);
        }

        return $this->render('ModulesTeamBundle:Member:edit.html.twig', ['category' => $category,
            'member' => $member,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),]);
    }

    /**
     * Deletes a member entity.
     * @Route("/{member}/delete")
     * @Route("/category/{category}/{member}/delete")
     * @Method({"GET", "DELETE"})
     * @Security("is_granted('DELETE', 'Modules\\TeamBundle\\Entity\\Member') or is_granted('DELETE', member)")
     * @param Request $request
     * @param Member $member
     * @param Category $category
     * @return RedirectResponse
     */
    public function deleteAction(Request $request, Member $member, Category $category = null)
    {
        $form = $this->createDeleteForm($member, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($member);
            $em->flush();

            $this->addFlash('success', '<i class="uk-icon-check"></i> Der Mitarbeiter wurde erfolgreich gelöscht!');
        }

        if ($category) {
            return $this->redirectToRoute('modules_team_member_categoryindex', ['category' => $category->getId()]);
        }

        return $this->redirectToRoute('modules_team_member_index');
    }

    /**
     * Edits a member status.
     * @Route("/{member}/status/{status}/{category}")
     * @Method({"GET", "POST"})
     * @Security("is_granted('EDIT',  'Modules\\TeamBundle\\Entity\\Member')")
     * @param $status
     * @param Member $member
     * @return RedirectResponse|Response
     */
    public function changeStatusAction($status, Member $member, Category $category = null)
    {
        $em = $this->getDoctrine()->getManager();

        $member->setStatus($status);


        $em->flush();

        $this->addFlash('success', '<i class="uk-icon-check"></i> Der Status des Mitarbeiter wurde erfolgreich geändert!');
        if ($category) {
            return $this->redirectToRoute('modules_team_member_categoryindex', ['category' => $category->getId()]);
        }

        return $this->redirectToRoute('modules_team_member_index');

    }

    /**
     * Edits a position in calendar.
     * @Route("/position/{member}/{position}/{member2}/{position2}")
     * @Method({"GET", "POST"})
     * @Security("is_granted('EDIT',  'Modules\\TeamBundle\\Entity\\Member')")
     * @param $position
     * @param $position2
     * @param Member $member
     * @param Member $member2
     * @return RedirectResponse|Response
     */
    public function changePositionAction($position = null, Member $member = null, $position2 = null, Member $member2 = null)
    {
        $em = $this->getDoctrine()->getManager();

        $member->setPositionCalendar($position);
        $member2->setPositionCalendar($position2);


        $em->flush();

        return new Response('', '200');

    }
}