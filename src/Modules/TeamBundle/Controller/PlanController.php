<?php

namespace Modules\TeamBundle\Controller;

use Modules\TeamBundle\Entity\GooglePlan;
use Modules\TeamBundle\Entity\Member;
use Modules\TeamBundle\Entity\Plan;
use Modules\TeamBundle\Form\PlanEditType;
use Modules\TeamBundle\Form\PlanType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/plan")
 */
class PlanController extends Controller
{
    /**
     * @Route("/{member}")
     * @Security("is_granted('VIEW', 'Modules\\TeamBundle\\Entity\\Plan')")
     * @param Request $request
     * @param Member|null $member
     * @return Response
     */
    public function indexAction(Request $request, Member $member = null)
    {
        $em = $this->getDoctrine()->getManager();

        /*
         * form
         */

        $planForm = $this->createForm(PlanType::class, null, ['show_member' => $this->isGranted('ROLE_ADMIN'), 'member' => $member]);

        if ($this->isGranted('ROLE_ADMIN') || $this->getUser()->getMember()) {
            $planForm->handleRequest($request);

            if ($planForm->isSubmitted() && $planForm->isValid()) {
                $days = $this->get('mod.team.helper')->getDaysByDateRange($planForm->get('start')->getData(), $planForm->get('end')->getData());

                foreach ($days as $day) {
                    $plan = new Plan();

                    $memberForPlan = $this->getUser()->getMember();
                    if ($this->isGranted('ROLE_ADMIN')) {
                        $memberForPlan = $planForm->get('member')->getData();
                    }

                    $plan->setMember($memberForPlan);
                    $plan->setNote($planForm->get('note')->getData());
                    $plan->setType($planForm->get('type')->getData());
                    $plan->setDay($day);
                    $plan->setDaySection(Plan::DAY_SECTION_FULLTIME);
                    $plan->setApproved(true);

                    $em->persist($plan);

                    if (!$plan->getMember()) {
                        $googlePlan = new GooglePlan();
                        $googlePlan->setPlan($plan);
                        $googlePlan->setUser(null);
                        $em->persist($googlePlan);
                        $em->flush();
                    } else {
                        $googlePlan = new GooglePlan();
                        $googlePlan->setPlan($plan);
                        $googlePlan->setUser($plan->getMember()->getUser());
                        $em->persist($googlePlan);
                        $em->flush();
                    }


                }
                $em->flush();

                if ($planForm->get('type')->getData() == Plan::TYPE_HOLIDAY) {
                    $this->addFlash('success', '<i class="uk-icon-check"></i> Urlaubstage erfolgreich eingetragen!');
                } elseif ($planForm->get('type')->getData() == Plan::TYPE_ILLNESS) {
                    $this->addFlash('success', '<i class="uk-icon-check"></i> Krankheittage erfolgreich eingetragen!');
                } elseif ($planForm->get('type')->getData() == Plan::TYPE_SCHOOL) {
                    $this->addFlash('success', '<i class="uk-icon-check"></i> Berufsschuletage erfolgreich eingetragen!');
                } elseif ($planForm->get('type')->getData() == Plan::TYPE_SPECIAL) {
                    $this->addFlash('success', '<i class="uk-icon-check"></i> Sonderurlaub erfolgreich eingetragen!');

                } else {
                    $this->addFlash('success', '<i class="uk-icon-check"></i> Planung erfolgreich eingetragen!');
                }

                return new RedirectResponse($this->generateUrl('modules_team_plan_index'));
            }
        }

        /*
         * table
         */

        // member filter
        $memberFilter = [];
        if ($this->isGranted('ROLE_ADMIN')) {
            $memberFilter = $this->get('mod.team.helper')->setMemberFilter();
        } elseif ($this->getUser()->getMember()) {
            $memberFilter = [['member' => $this->getUser()->getMember(), 'active' => true]];
        }
        $memberCategories = $this->getDoctrine()->getRepository('ModulesTeamBundle:Category')->findAll();

        $plans = $em->getRepository('ModulesTeamBundle:Plan')->findAllOrdered($request->query->get('sort', 'day'), $request->query->get('direction', 'DESC'), $memberFilter);

        $deleteForms = [];
        foreach ($plans as $plan) {
            $deleteForms[$plan->getId()] = $this->createDeleteForm($plan)->createView();
        }

        $columns = $this->get('app.helper')->setTableColumns('mod_team_plan_table_columns', ['member' => ['active' => 1, 'label' => 'Mitarbeiter'], 'type' => ['active' => 1, 'label' => 'Art'], 'day' => ['active' => 1, 'label' => 'Tag'], 'note' => ['active' => 1, 'label' => 'Anmerkung'],]);

        $perPage = $this->get('app.helper')->setTablePerPage('mod_team_plan_table_rows');
        $paginator = $this->get('knp_paginator');
        $page = $request->query->getInt('page', 1);
        $pagination = $paginator->paginate($plans, $page, $perPage);

        return $this->render('ModulesTeamBundle:Plan:index.html.twig', ['planForm' => $planForm->createView(), 'deleteForms' => $deleteForms, 'memberFilter' => $memberFilter, 'memberCategories' => $memberCategories, 'columns' => $columns, 'pagination' => $pagination, 'perPage' => $perPage]);
    }

    /**
     * Creates a form to delete a plan entity.
     * @param Plan $plan
     * @return \Symfony\Component\Form\Form|\Symfony\Component\Form\FormInterface
     */
    private function createDeleteForm(Plan $plan)
    {
        return $this->createFormBuilder()->setAction($this->generateUrl('modules_team_plan_delete', ['plan' => $plan->getId()]))->setMethod('DELETE')->getForm();
    }

    /**
     * @Route("/modal/{plan}")
     * @Method("GET")
     * @param Plan $plan
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function modalAction(Plan $plan)
    {
        return $this->render('ModulesTeamBundle:Plan:modal.html.twig', ['plan' => $plan]);
    }

    /**
     * Displays a form to edit an existing plan entity.
     * @Route("/{id}/edit")
     * @Method({"GET", "POST"})
     * @Security("is_granted('EDIT', plan)")
     * @param Request $request
     * @param Plan $plan
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @ParamConverter("id", class="Modules\TeamBundle\Entity\Plan")
     */
    public function editAction(Request $request, Plan $plan)
    {
        $deleteForm = $this->createDeleteForm($plan);
        $editForm = $this->createForm(PlanEditType::class, $plan, ['show_member' => $this->isGranted('ROLE_ADMIN')]);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', '<i class="uk-icon-check"></i> Die Planung wurde erfolgreich aktualisiert!');

            return $this->redirectToRoute('modules_team_plan_index');
        }


        return $this->render('ModulesTeamBundle:Plan:edit.html.twig', ['plan' => $plan, 'edit_form' => $editForm->createView(), 'delete_form' => $deleteForm->createView(),]);
    }

    /**
     * Deletes a plan entity.
     * @Route("/{plan}/delete")
     * @Method({"GET", "DELETE"})
     * @Security("is_granted('DELETE', plan)")
     * @param Request $request
     * @param Plan $plan
     * @return RedirectResponse
     */
    public function deleteAction(Request $request, Plan $plan)
    {
        $form = $this->createDeleteForm($plan);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $googleRefreshToken = $this->get('app.settings')->getGlobal('mod_calendar_google_refresh_token');
            $googleCalendarId = $this->get('app.settings')->getGlobal('mod_calendar_google_calendar_id');

            $googleEventRepo = $this->getDoctrine()->getRepository('ModulesTeamBundle:GooglePlan');
            $events = $googleEventRepo->findBy(['plan' => $plan]);

            /**
             * @var GooglePlan $plan ;
             */
            foreach ($events as $plan) {

                $googleId = $plan->getGoogleEventId();
                $googleRefreshToken = $plan->getUser()->getGoogleRefreshToken();
                $user = $plan->getUser();
                if ($googleId && $googleRefreshToken && $googleCalendarId) {
                    $googleClient = $this->get('app.google')->getClient();
                    $accessToken = $googleClient->fetchAccessTokenWithRefreshToken($googleRefreshToken);

                    if (!array_key_exists('error', $accessToken)) {
                        $googleCalendar = new \Google_Service_Calendar($googleClient);
                        try {
                            $googleCalendar->events->delete($user->getGoogleCalendarId(), $googleId);
                        } catch (\Exception $exception) {
                        }
                    }
                } else {
                    $googleRefreshToken = $this->get('app.settings')->getGlobal('mod_calendar_google_refresh_token');

                    if ($plan->getGoogleEventId() && $googleRefreshToken && $googleCalendarId) {
                        $accessToken = $this->get('app.google')->getClient()->fetchAccessTokenWithRefreshToken($googleRefreshToken);
                        if (!array_key_exists('error', $accessToken)) {
                            $googleCalendar = new \Google_Service_Calendar($this->get('app.google')->getClient());
                            $googleCalendar->events->delete($googleCalendarId, $plan->getGoogleEventId());
                        }
                    }
                }
            }

            $em = $this->getDoctrine()->getManager();
            $em->remove($plan);
            $em->flush();

            $this->addFlash('success', '<i class="uk-icon-check"></i> Die Planung wurde erfolgreich gelöscht!');
        }

        return $this->redirectToRoute('modules_team_plan_index');
    }

    /**
     * @Route("/ajax/multiDeleteRender", options={"expose"=true})
     * @return JsonResponse
     */
    public function multiDeleteRenderAction(Request $request)
    {

        $em = $this->getDoctrine()->getRepository('ModulesTeamBundle:Plan');

        $content = "";

        if ($request->request->has('ids')) {
            $ids = $request->request->get('ids');
            $ids = json_decode($ids, true);

            foreach ($ids as $id) {
                $plan = $em->findOneBy(['id' => $id]);
                if ($plan) {
                    $content .= $this->renderView("ModulesTeamBundle:Plan:multi_modal_entry.html.twig", ['plan' => $plan]);
                }
            }
        }


        return new JsonResponse(['content' => $content,]);

    }

    /**
     * @Route("/ajax/multiDelete", options={"expose"=true})
     * @return RedirectResponse
     */
    public function multiDeleteAction(Request $request)
    {

        $em = $this->getDoctrine();

        $repo = $em->getRepository('ModulesTeamBundle:Plan');

        if ($request->request->has('ids')) {
            $ids = $request->request->get('ids');
            $ids = json_decode($ids, true);

            foreach ($ids as $id) {
                $plan = $repo->findOneBy(['id' => $id]);
                if ($plan) {
                    $em->getManager()->remove($plan);
                    $em->getManager()->flush();
                }
            }
        }


        return $this->redirectToRoute('modules_team_plan_index');


    }
}