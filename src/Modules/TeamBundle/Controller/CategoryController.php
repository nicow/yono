<?php

namespace Modules\TeamBundle\Controller;

use Modules\TeamBundle\Entity\Category;
use Modules\TeamBundle\Entity\Member;
use Modules\TeamBundle\Form\CategoryType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\HttpFoundation\Request;

/**
 * Category controller.
 * @Route("/category")
 */
class CategoryController extends Controller
{
    /**
     * Lists all category entities.
     * @Route("/")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $categories = $em->getRepository('ModulesTeamBundle:Category')->findAll();

        // get delete forms
        $deleteForms = [];
        foreach ($categories as $category) {
            $deleteForms[$category->getId()] = $this->createDeleteForm($category)->createView();
        }

        return $this->render('ModulesTeamBundle:Category:index.html.twig', ['categories' => $categories, 'deleteForms' => $deleteForms]);
    }

    /**
     * Creates a form to delete a category entity.
     * @param Category $category The category entity
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Category $category)
    {
        $otherCategories = $this->getDoctrine()->getManager()->getRepository('ModulesTeamBundle:Category')->getOtherCategories($category);
        $choices = ['nichts (nur die Abteilung entfernen)' => 'remove_category_only', 'Mitarbeiter auch löschen' => 'delete'];
        /** @var Category $otherCategory */
        foreach ($otherCategories as $otherCategory) {
            $choices['Abteilung "' . $otherCategory->getName() . '" zuweisen'] = $otherCategory->getId();
        }

        return $this->createFormBuilder()->add('members_action', ChoiceType::class, ['choices' => $choices])->setAction($this->generateUrl('modules_team_category_delete', ['id' => $category->getId()]))->setMethod('DELETE')->getForm();
    }

    /**
     * Creates a new category entity.
     * @Route("/new")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $category = new Category();
        $category->setShowInMenu(true);
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();

            $this->addFlash('success', '<i class="uk-icon-check"></i> Die Abteilung wurde erfolgreich angelegt!');

            return $this->redirectToRoute('modules_team_category_index');
        }

        return $this->render('ModulesTeamBundle:Category:new.html.twig', ['category' => $category, 'form' => $form->createView(),]);
    }

    /**
     * Displays a form to edit an existing category entity.
     * @Route("/{id}/edit")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param Category $category
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @ParamConverter("id", class="Modules\TeamBundle\Entity\Category")
     */
    public function editAction(Request $request, Category $category)
    {
        $editForm = $this->createForm(CategoryType::class, $category);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', '<i class="uk-icon-check"></i> Die Abteilung wurde erfolgreich aktualisiert!');

            return $this->redirectToRoute('modules_team_category_index');
        }

        return $this->render('ModulesTeamBundle:Category:edit.html.twig', ['category' => $category, 'edit_form' => $editForm->createView(),]);
    }

    /**
     * Deletes a category entity.
     * @Route("/{id}")
     * @Method("DELETE")
     * @param Request $request
     * @param Category $category
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, Category $category)
    {
        $form = $this->createDeleteForm($category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $membersAction = $request->request->get('form')['members_action'];
            if ($membersAction != 'remove_category_only') {
                // get all members from category and category children
                $members = $em->getRepository('ModulesTeamBundle:Member')->findByCategory($category);
                if ($membersAction == 'delete') {
                    // delete them
                    foreach ($members as $member) {
                        $em->remove($member);
                    }
                } elseif ((int)$membersAction) {
                    // assign new category
                    $newCategory = $em->getRepository('ModulesTeamBundle:Category')->find((int)$membersAction);
                    if ($newCategory) {
                        /** @var Member $member */
                        foreach ($members as $member) {
                            if (!$member->getCategories()->contains($newCategory)) {
                                $member->addCategory($newCategory);
                            }
                        }
                    }
                }
            }

            $this->addFlash('success', '<i class="uk-icon-check"></i> Die Abteilung wurde erfolgreich gelöscht!');

            $em->remove($category);
            $em->flush();
        }

        return $this->redirectToRoute('modules_team_category_index');
    }
}
