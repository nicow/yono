<?php
/**
 * Created by PhpStorm.
 * User: mkt
 * Date: 04.01.17
 * Time: 20:54
 */

namespace Modules\TeamBundle\Search;

use Core\AppBundle\Search\SearchItem;
use Doctrine\ORM\EntityManager;
use Modules\TeamBundle\Service\TeamHelper;
use Symfony\Bridge\Twig\TwigEngine;
use Symfony\Bundle\FrameworkBundle\Routing\Router;

class Member extends SearchItem
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var TwigEngine
     */
    private $twig;

    /**
     * @var TeamHelper
     */
    private $teamHelper;

    /**
     * @var Router
     */
    private $router;

    public function __construct(EntityManager $em, TwigEngine $twig, TeamHelper $teamHelper, Router $router)
    {
        $this->em = $em;
        $this->twig = $twig;
        $this->teamHelper = $teamHelper;
        $this->router = $router;
    }

    /**
     * @param $query
     * @return array
     */
    public function getResults($query)
    {
        $members = $this->em->getRepository('ModulesTeamBundle:Member')->createQueryBuilder('m')->where('m.firstname LIKE :query')->orWhere('m.lastname LIKE :query')->setParameter('query', '%' . $query . '%')->getQuery()->getResult();

        $results = [];
        if ($members) {
            foreach ($members as $member) {
                $results[] = $this->twig->render('@ModulesTeam/Search/member.html.twig', ['member' => $member]);
            }
        }

        return $results;
    }

    /**
     * @param $query
     * @return array
     */
    public function getAutocompleteResults($query)
    {
        $members = $this->em->getRepository('ModulesTeamBundle:Member')->createQueryBuilder('m')->where('m.firstname LIKE :query')->orWhere('m.lastname LIKE :query')->setParameter('query', '%' . $query . '%')->getQuery()->getResult();

        $results = [];
        if ($members) {
            /** @var \Modules\TeamBundle\Entity\Member $member */
            foreach ($members as $member) {
                $results[] = ['icon' => 'users', 'title' => $member->getFullName(), 'url' => $this->router->generate('modules_team_member_details', ['member' => $member->getId()])];
            }
        }

        return $results;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'Team Mitglied';
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'user';
    }

    public function getRequiredPermissions()
    {
        return ['VIEW' => 'Modules\\TeamBundle\\Entity\\Member'];
    }
}