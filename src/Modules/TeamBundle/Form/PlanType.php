<?php

namespace Modules\TeamBundle\Form;

use Modules\TeamBundle\Entity\Member;
use Modules\TeamBundle\Entity\Plan;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PlanType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if ($options['show_member']) {
            $builder->add('member', EntityType::class, ['label' => 'Mitarbeiter', 'class' => Member::class, 'required' => false, 'placeholder' => 'Alle', 'data' => $options['member']]);
        }

        $builder->add('type', ChoiceType::class, ['label' => 'Art', 'choices' => ['Urlaub' => Plan::TYPE_HOLIDAY, 'Krankheit' => Plan::TYPE_ILLNESS, 'Berufsschule' => Plan::TYPE_SCHOOL, 'Sonderurlaub' => Plan::TYPE_SPECIAL]])->add('start', DateType::class, ['label' => 'Von', 'widget' => 'single_text', 'format' => 'd.M.y', 'data' => new \DateTime()])->add('end', DateType::class, ['label' => 'Bis', 'widget' => 'single_text', 'format' => 'd.M.y', 'data' => new \DateTime()])->add('note', TextType::class, ['label' => 'Anmerkung', 'required' => false]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['show_member' => false, 'member' => null]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'modules_teambundle_plan';
    }
}
