<?php

namespace Modules\TeamBundle\Form;

use Core\AppBundle\Form\ColorType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class MemberType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('salutation', ChoiceType::class, ['choices' => ['Herr' => 'Herr', 'Frau' => 'Frau'], 'label' => 'Anrede'])->add('firstname', null, ['label' => 'Vorname'])->add('lastname', null, ['label' => 'Nachname'])->add('shortName', null, ['label' => 'Kürzel'])->add('birthday', BirthdayType::class, ['label' => 'Geburtstag', 'widget' => 'single_text', 'format' => 'dd.MM.yyyy', 'required' => false])->add('joinedTeam', DateType::class, ['label' => 'Eintritt', 'widget' => 'single_text', 'format' => 'dd.MM.yyyy'])->add('leftTeam', DateType::class, ['label' => 'Austritt', 'widget' => 'single_text', 'format' => 'dd.MM.yyyy', 'required' => false])->add('holidayClaim', IntegerType::class, ['label' => 'Urlaubsanspruch'])->add('street', null, ['label' => 'Straße'])->add('zip', null, ['label' => 'PLZ'])->add('city', null, ['label' => 'Stadt'])->add('phone', null, ['label' => 'Telefon'])->add('mobile', null, ['label' => 'Mobiltelefon'])->add('email', EmailType::class, ['label' => 'E-Mail', 'required' => false])->add('bankAccountIban', null, ['label' => 'IBAN', 'constraints' => [new Assert\Iban()]])->add('bankAccountBic', null, ['label' => 'BIC', 'constraints' => [new Assert\Bic()]])->add('color', ColorType::class, ['label' => 'Farbe'])->add('categories', null, ['label' => 'Abteilungen'])->add('wageTaxClass', IntegerType::class, ['label' => 'LStkl.', 'required' => false, 'attr' => ['min' => 1, 'max' => 6]])->add('children', null, ['label' => 'Kinder'])->add('note', CKEditorType::class, ['label' => 'Anmerkung']);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => 'Modules\TeamBundle\Entity\Member', 'csrf_protection' => false]);
    }
}
