<?php

namespace Modules\TeamBundle\Form;

use Doctrine\ORM\EntityRepository;
use Modules\TeamBundle\Entity\Category;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CategoryType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $category = $builder->getData();

        $builder->add('name')->add('parent', EntityType::class, ['label' => 'Elternabteilung', 'query_builder' => function (EntityRepository $er) use ($category) {
            $qb = $er->createQueryBuilder('c');

            // if we are editing an existing category, exclude it
            if ($category->getId()) {
                $qb->where('c != :category')->setParameter('category', $category);
            }

            return $qb;
        }, 'class' => Category::class, 'required' => false])->add('showInMenu', null, ['label' => 'Im Menü anzeigen?']);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array('data_class' => Category::class));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'modules_teambundle_category';
    }
}
