<?php

namespace Modules\TeamBundle\Entity;

use Core\AppBundle\Annotation\Acl;
use Core\AppBundle\Entity\BlameableTrait;
use Core\AppBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Member
 * @Acl("Mitarbeiter")
 * @ORM\Table(name="mod_team_member")
 * @ORM\Entity(repositoryClass="Modules\TeamBundle\Repository\MemberRepository")
 * @UniqueEntity(fields={"email"}, message="Ein Mitarbeiter mit dieser E-Mail existiert bereits.")
 * @UniqueEntity(fields={"shortName"}, message="Dieses Kürzel ist bereits vergeben.")
 */
class Member
{

    const STATUS_HIDDEN = 1;

    use BlameableTrait;

    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="salutation", type="string", length=50)
     * @Assert\NotBlank()
     */
    private $salutation;

    /**
     * @var string
     * @ORM\Column(name="firstname", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $firstname;

    /**
     * @var string
     * @ORM\Column(name="lastname", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $lastname;

    /**
     * @var Category[]
     * @ORM\ManyToMany(targetEntity="Modules\TeamBundle\Entity\Category", inversedBy="members")
     * @ORM\JoinTable(name="mod_team_members_categories")
     */
    private $categories;

    /**
     * @var User
     * @ORM\OneToOne(targetEntity="Core\AppBundle\Entity\User", mappedBy="member")
     */
    private $user;

    /**
     * @var string
     * @ORM\Column(name="short_name", type="string", length=10, nullable=true)
     */
    private $shortName;

    /**
     * @var \DateTime
     * @ORM\Column(name="birthday", type="date", nullable=true)
     */
    private $birthday;

    /**
     * @var \DateTime
     * @ORM\Column(name="joined_team", type="date", nullable=true)
     */
    private $joinedTeam;

    /**
     * @var \DateTime
     * @ORM\Column(name="left_team", type="date", nullable=true)
     */
    private $leftTeam;

    /**
     * @var string
     * @ORM\Column(name="street", type="string", length=100, nullable=true)
     */
    private $street;

    /**
     * @var string
     * @ORM\Column(name="city", type="string", length=100, nullable=true)
     */
    private $city;

    /**
     * @var string
     * @ORM\Column(name="zip", type="string", length=10, nullable=true)
     */
    private $zip;

    /**
     * @var string
     * @ORM\Column(name="phone", type="string", length=50, nullable=true)
     */
    private $phone;

    /**
     * @var string
     * @ORM\Column(name="mobile", type="string", length=50, nullable=true)
     */
    private $mobile;

    /**
     * @var string
     * @ORM\Column(name="email", type="string", length=100, nullable=true)
     */
    private $email;

    /**
     * @var string
     * @ORM\Column(name="bank_account_iban", type="string", nullable=true)
     */
    private $bankAccountIban;

    /**
     * @var string
     * @ORM\Column(name="bank_account_bic", type="string", nullable=true)
     */
    private $bankAccountBic;

    /**
     * @var string
     * @ORM\Column(name="color", type="string", length=30, nullable=true)
     */
    private $color;

    /**
     * @var Plan[]
     * @ORM\OneToMany(targetEntity="Modules\TeamBundle\Entity\Plan", mappedBy="member")
     */
    private $plans;

    /**
     * @var integer
     * @ORM\Column(name="wage_tax_class", type="integer", nullable=true)
     */
    private $wageTaxClass;

    /**
     * @var integer
     * @ORM\Column(name="children", type="string", nullable=true)
     */
    private $children;

    /**
     * @var string
     * @ORM\Column(name="note", type="text", nullable=true)
     */
    private $note;

    /**
     * @var string
     * @ORM\Column(name="holiday_claim", type="integer")
     */
    private $holidayClaim;

    /**
     * @var integer
     * @ORM\Column(name="positionCalendar", type="integer", nullable=true)
     */
    private $positionCalendar;


    /**
     * @var integer
     * @ORM\Column(name="status", type="string", nullable=true)
     */
    private $status;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->categories = new ArrayCollection();
        $this->plans = new ArrayCollection();
        $this->color = '#777777';
    }

    /**
     * Get id
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add category
     * @param Category $category
     * @return Member
     */
    public function addCategory(Category $category)
    {
        $this->categories[] = $category;

        return $this;
    }

    /**
     * Remove category
     * @param Category $category
     */
    public function removeCategory(Category $category)
    {
        $this->categories->removeElement($category);
    }

    /**
     * Get categories
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCategories()
    {
        return $this->categories;
    }

    public function getFullName()
    {
        return $this->getSalutation() . ' ' . $this->getFirstname() . ' ' . $this->getLastname();
    }

    /**
     * Get salutation
     * @return string
     */
    public function getSalutation()
    {
        return $this->salutation;
    }

    /**
     * Set salutation
     * @param string $salutation
     * @return Member
     */
    public function setSalutation($salutation)
    {
        $this->salutation = $salutation;

        return $this;
    }

    /**
     * Get firstname
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set firstname
     * @param string $firstname
     * @return Member
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get lastname
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set lastname
     * @param string $lastname
     * @return Member
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function __toString()
    {
        return substr($this->getFirstname(), 0, 1) . '. ' . $this->getLastname();
    }

    /**
     * Get user
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set user
     * @param User $user
     * @return Member
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get shortName
     * @return string
     */
    public function getShortName()
    {
        return $this->shortName;
    }

    /**
     * Set shortName
     * @param string $shortName
     * @return Member
     */
    public function setShortName($shortName)
    {
        $this->shortName = $shortName;

        return $this;
    }

    /**
     * Get birthday
     * @return \DateTime
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * Set birthday
     * @param \DateTime $birthday
     * @return Member
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * Get joinedTeam
     * @return \DateTime
     */
    public function getJoinedTeam()
    {
        return $this->joinedTeam;
    }

    /**
     * Set joinedTeam
     * @param \DateTime $joinedTeam
     * @return Member
     */
    public function setJoinedTeam($joinedTeam)
    {
        $this->joinedTeam = $joinedTeam;

        return $this;
    }

    /**
     * Get leftTeam
     * @return \DateTime
     */
    public function getLeftTeam()
    {
        return $this->leftTeam;
    }

    /**
     * Set leftTeam
     * @param \DateTime $leftTeam
     * @return Member
     */
    public function setLeftTeam($leftTeam)
    {
        $this->leftTeam = $leftTeam;

        return $this;
    }

    /**
     * Get street
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set street
     * @param string $street
     * @return Member
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get city
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set city
     * @param string $city
     * @return Member
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get zip
     * @return string
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * Set zip
     * @param string $zip
     * @return Member
     */
    public function setZip($zip)
    {
        $this->zip = $zip;

        return $this;
    }

    /**
     * Get phone
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set phone
     * @param string $phone
     * @return Member
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get mobile
     * @return string
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * Set mobile
     * @param string $mobile
     * @return Member
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;

        return $this;
    }

    /**
     * Get email
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set email
     * @param string $email
     * @return Member
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get bankAccountIban
     * @return string
     */
    public function getBankAccountIban()
    {
        return $this->bankAccountIban;
    }

    /**
     * Set bankAccountIban
     * @param string $bankAccountIban
     * @return Member
     */
    public function setBankAccountIban($bankAccountIban)
    {
        $this->bankAccountIban = $bankAccountIban;

        return $this;
    }

    /**
     * Get bankAccountBic
     * @return string
     */
    public function getBankAccountBic()
    {
        return $this->bankAccountBic;
    }

    /**
     * Set bankAccountBic
     * @param string $bankAccountBic
     * @return Member
     */
    public function setBankAccountBic($bankAccountBic)
    {
        $this->bankAccountBic = $bankAccountBic;

        return $this;
    }

    /**
     * Get color
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set color
     * @param string $color
     * @return Member
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Add plan
     * @param Plan $plan
     * @return Member
     */
    public function addPlan(Plan $plan)
    {
        $this->plans[] = $plan;

        return $this;
    }

    /**
     * Remove plan
     * @param Plan $plan
     */
    public function removePlan(Plan $plan)
    {
        $this->plans->removeElement($plan);
    }

    /**
     * Get plans
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPlans()
    {
        return $this->plans;
    }

    /**
     * Get wageTaxClass
     * @return integer
     */
    public function getWageTaxClass()
    {
        return $this->wageTaxClass;
    }

    /**
     * Set wageTaxClass
     * @param integer $wageTaxClass
     * @return Member
     */
    public function setWageTaxClass($wageTaxClass)
    {
        $this->wageTaxClass = $wageTaxClass;

        return $this;
    }

    /**
     * Get children
     * @return string
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Set children
     * @param string $children
     * @return Member
     */
    public function setChildren($children)
    {
        $this->children = $children;

        return $this;
    }

    /**
     * Get note
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set note
     * @param string $note
     * @return Member
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * @return string
     */
    public function getHolidayClaim()
    {
        return $this->holidayClaim;
    }

    /**
     * @param string $holidayClaim
     * @return Member
     */
    public function setHolidayClaim($holidayClaim)
    {
        $this->holidayClaim = $holidayClaim;

        return $this;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return Member
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return int
     */
    public function getPositionCalendar()
    {
        return $this->positionCalendar;
    }

    /**
     * @param int $positionCalendar
     * @return Member
     */
    public function setPositionCalendar($positionCalendar)
    {
        $this->positionCalendar = $positionCalendar;

        return $this;
    }
}
