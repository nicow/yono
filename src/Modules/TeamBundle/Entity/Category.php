<?php

namespace Modules\TeamBundle\Entity;

use Core\AppBundle\Annotation\Acl;
use Core\AppBundle\Entity\BlameableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Category
 * @Acl("Abteilungen")
 * @ORM\Table(name="mod_team_category")
 * @ORM\Entity(repositoryClass="Modules\TeamBundle\Repository\CategoryRepository")
 */
class Category
{
    use BlameableTrait;

    /**
     * @var Category[]
     * @ORM\OneToMany(targetEntity="Category", mappedBy="parent")
     */
    protected $children;
    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=50, unique=true)
     */
    private $name;
    /**
     * @var string
     * @ORM\Column(name="icon", type="string", length=50, nullable=true)
     */
    private $icon;
    /**
     * @var boolean
     * @ORM\Column(name="show_in_menu", type="boolean", nullable=true)
     */
    private $showInMenu;
    /**
     * @var Category
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $parent;

    /**
     * @var Member[]
     * @ORM\ManyToMany(targetEntity="Modules\TeamBundle\Entity\Member", mappedBy="categories")
     */
    private $members;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->members = new ArrayCollection();
        $this->children = new ArrayCollection();
    }

    /**
     * Get id
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     * @param string $name
     * @return Category
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get icon
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * Set icon
     * @param string $icon
     * @return Category
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * Add member
     * @param Member $member
     * @return Category
     */
    public function addMember(Member $member)
    {
        $this->members[] = $member;

        return $this;
    }

    /**
     * Remove member
     * @param Member $member
     */
    public function removeMember(Member $member)
    {
        $this->members->removeElement($member);
    }

    /**
     * Get members
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMembers()
    {
        return $this->members;
    }

    public function __toString()
    {
        return $this->name;
    }

    /**
     * Add child
     * @param Category $child
     * @return Category
     */
    public function addChild(Category $child)
    {
        $this->children[] = $child;

        return $this;
    }

    /**
     * Remove child
     * @param Category $child
     */
    public function removeChild(Category $child)
    {
        $this->children->removeElement($child);
    }

    /**
     * Get children
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Get parent
     * @return Category
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set parent
     * @param Category $parent
     * @return Category
     */
    public function setParent(Category $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get showInMenu
     * @return boolean
     */
    public function getShowInMenu()
    {
        return $this->showInMenu;
    }

    /**
     * Set showInMenu
     * @param boolean $showInMenu
     * @return Category
     */
    public function setShowInMenu($showInMenu)
    {
        $this->showInMenu = $showInMenu;

        return $this;
    }
}
