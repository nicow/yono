<?php

namespace Modules\TeamBundle\Entity;

use Core\AppBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;

/**
 * Event
 * @ORM\Table(name="mod_team_google_plan")
 * @ORM\Entity(repositoryClass="Modules\TeamBundle\Repository\GooglePlanRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class GooglePlan
{
    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Plan
     * @ORM\ManyToOne(targetEntity="Modules\TeamBundle\Entity\Plan", inversedBy="googlePlan")
     * @ORM\JoinColumn(name="plan", referencedColumnName="id", onDelete="CASCADE")
     */
    private $plan;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\User", inversedBy="googlePlans")
     */
    private $user;

    /**
     * @ORM\Column(name="google_event_id", type="string", nullable=true)
     */
    private $googleEventId;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return GooglePlan
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return GooglePlan
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getGoogleEventId()
    {
        return $this->googleEventId;
    }

    /**
     * @param mixed $googleEventId
     * @return GooglePlan
     */
    public function setGoogleEventId($googleEventId)
    {
        $this->googleEventId = $googleEventId;

        return $this;
    }

    /**
     * @return Plan
     */
    public function getPlan()
    {
        return $this->plan;
    }

    /**
     * @param Plan $plan
     * @return GooglePlan
     */
    public function setPlan($plan)
    {
        $this->plan = $plan;

        return $this;
    }

}