<?php

namespace Modules\TeamBundle\Entity;

use Core\AppBundle\Annotation\Acl;
use Core\AppBundle\Entity\BlameableTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * Plan
 * @Acl("Planung")
 * @ORM\Table(name="mod_team_plan")
 * @ORM\Entity(repositoryClass="Modules\TeamBundle\Repository\PlanRepository")
 */
class Plan
{
    use BlameableTrait;

    const DAY_SECTION_MORNING = 'morning';
    const DAY_SECTION_AFTERNOON = 'afternoon';
    const DAY_SECTION_FULLTIME = 'fulltime';

    const TYPE_HOLIDAY = 'holiday';
    const TYPE_ILLNESS = 'illness';
    const TYPE_SCHOOL = 'school';
    const TYPE_SPECIAL = 'special';

    const DAY_MONDAY = 1;
    const DAY_TUESDAY = 2;
    const DAY_WEDNESDAY = 3;
    const DAY_THURSDAY = 4;
    const DAY_FRIDAY = 5;
    const DAY_SATURDAY = 6;
    const DAY_SUNDAY = 0;

    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Member
     * @ORM\ManyToOne(targetEntity="Modules\TeamBundle\Entity\Member", inversedBy="plans")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $member;

    /**
     * @var string
     * @ORM\Column(name="type", type="string")
     */
    private $type;

    /**
     * @var \DateTime
     * @ORM\Column(name="day", type="date")
     */
    private $day;

    /**
     * @var string
     * @ORM\Column(name="day_section", type="string")
     */
    private $daySection;

    /**
     * @var string
     * @ORM\Column(name="note", type="string", nullable=true)
     */
    private $note;

    /**
     * @var boolean
     * @ORM\Column(name="approved", type="boolean", nullable=true)
     */
    private $approved;

    /**
     * @var string
     * @ORM\Column(name="google_calendar_id", type="string", nullable=true)
     */
    private $googleCalendarId;

    /**
     * @ORM\OneToMany(targetEntity="Modules\TeamBundle\Entity\GooglePlan", mappedBy="plan")
     */
    private $googlePlan;

    /**
     * Get id
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get member
     * @return Member
     */
    public function getMember()
    {
        return $this->member;
    }

    /**
     * Set member
     * @param Member $member
     * @return Plan
     */
    public function setMember(Member $member = null)
    {
        $this->member = $member;

        return $this;
    }

    /**
     * Get type
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set type
     * @param string $type
     * @return Plan
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get day
     * @return \DateTime
     */
    public function getDay()
    {
        return $this->day;
    }

    /**
     * Set day
     * @param \DateTime $day
     * @return Plan
     */
    public function setDay($day)
    {
        $this->day = $day;

        return $this;
    }

    /**
     * Get daySection
     * @return string
     */
    public function getDaySection()
    {
        return $this->daySection;
    }

    /**
     * Set daySection
     * @param string $daySection
     * @return Plan
     */
    public function setDaySection($daySection)
    {
        $this->daySection = $daySection;

        return $this;
    }

    /**
     * Get note
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set note
     * @param string $note
     * @return Plan
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get approved
     * @return boolean
     */
    public function getApproved()
    {
        return $this->approved;
    }

    /**
     * Set approved
     * @param boolean $approved
     * @return Plan
     */
    public function setApproved($approved)
    {
        $this->approved = $approved;

        return $this;
    }

    /**
     * @return string
     */
    public function getGoogleCalendarId()
    {
        return $this->googleCalendarId;
    }

    /**
     * @param string $googleCalendarId
     * @return Plan
     */
    public function setGoogleCalendarId($googleCalendarId)
    {
        $this->googleCalendarId = $googleCalendarId;

        return $this;
    }
}
