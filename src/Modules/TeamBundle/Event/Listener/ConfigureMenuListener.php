<?php
/**
 * Created by PhpStorm.
 * User: mkt
 * Date: 26.08.16
 * Time: 17:22
 */

namespace Modules\TeamBundle\Event\Listener;


use Core\AppBundle\Event\ConfigureMenuEvent;
use Doctrine\ORM\EntityManager;
use Knp\Menu\FactoryInterface;
use Modules\TeamBundle\Entity\Category;
use Modules\TeamBundle\Service\TeamHelper;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\HttpFoundation\RequestStack;

class ConfigureMenuListener
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var Router
     */
    private $router;
    /**
     * @var TeamHelper
     */
    private $teamHelper;
    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var array
     */
    private $order;

    /**
     * @var Category
     */
    private $currentCategory;

    public function __construct(EntityManager $em, Router $router, TeamHelper $teamHelper, RequestStack $requestStack)
    {
        $this->em = $em;
        $this->router = $router;
        $this->teamHelper = $teamHelper;
        $this->requestStack = $requestStack;
        $this->request = $requestStack->getCurrentRequest();
    }

    public function onMenuConfigure(ConfigureMenuEvent $event)
    {
        $factory = $event->getFactory();
        $menu = $event->getMenu();

        $categories = $this->em->getRepository('ModulesTeamBundle:Category')->findBy(['showInMenu' => true, 'parent' => null]);

        if ($menu->getName() == 'main') {
            $teamKey = 'Team';
            if ($menu[$teamKey]) {
                $this->order = [];
                if ($menu[$teamKey]->getChild('Übersicht')) {
                    $this->order[] = 'Übersicht';
                }

                $this->currentCategory = $this->teamHelper->getCurrentCategory();

                // Add categories
                foreach ($categories as $category) {
                    $this->addChildren($factory, $category, $menu[$teamKey]);
                    $this->order[] = $category->getName();
                }

                if ($menu[$teamKey]->getChild('divider')) {
                    $this->order[] = 'divider';
                }
                if ($menu[$teamKey]->getChild('Abteilungen')) {
                    $this->order[] = 'Abteilungen';
                }
                if ($menu[$teamKey]->getChild('Planung')) {
                    $this->order[] = 'Planung';
                }

                $menu[$teamKey]->reorderChildren($this->order);

                // Handle "current" states
                if ($menu[$teamKey]->getChild('Abteilungen')) {
                    if (strpos($this->request->attributes->get('_route'), 'modules_team_category') !== false) {
                        $menu[$teamKey]['Abteilungen']->setCurrent(true);
                    }
                }
                if ($menu[$teamKey]->getChild('Planung')) {
                    if (strpos($this->request->attributes->get('_route'), 'modules_team_plan') !== false) {
                        $menu[$teamKey]['Planung']->setCurrent(true);
                    }
                }
            }
        }
    }

    public function addChildren(FactoryInterface $factory, Category $category, $menu)
    {
        $options = ['route' => 'modules_team_member_categoryindex', 'routeParameters' => ['category' => $category->getId()], 'extras' => ['icon' => $category->getIcon()]];
        $item = $factory->createItem($category->getName(), $options);
//        if ($this->currentCategory == $category) {
//            $item->setCurrent(true);
//        }
        if ($category->getChildren()) {
            foreach ($category->getChildren() as $child) {
                $this->addChildren($factory, $child, $item);
            }
        }
        $menu->addChild($item);
    }
}