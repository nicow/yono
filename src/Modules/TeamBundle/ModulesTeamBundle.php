<?php

namespace Modules\TeamBundle;

use Core\YonoBundleInterface;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class ModulesTeamBundle extends Bundle implements YonoBundleInterface
{
    /**
     * @return string
     */
    public function getDisplayName()
    {
        return 'Team';
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return 'Koordinieren Sie Ihr Team.';
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'users';
    }
}
