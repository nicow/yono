<?php
/**
 * Created by PhpStorm.
 * User: mkt
 * Date: 31.12.16
 * Time: 04:09
 */

namespace Modules\TeamBundle\Service;


use Core\AppBundle\Service\Settings;
use Doctrine\ORM\EntityManager;
use Modules\TeamBundle\Entity\Category;
use Modules\TeamBundle\Entity\Member;
use Modules\TeamBundle\Entity\Plan;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\Session;

class TeamHelper
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var Session
     */
    private $session;

    /**
     * @var Settings
     */
    private $settings;

    /**
     * @var RequestStack
     */
    private $requestStack;

    public function __construct(EntityManager $em, Session $session, Settings $settings, RequestStack $requestStack)
    {
        $this->em = $em;
        $this->session = $session;
        $this->settings = $settings;
        $this->requestStack = $requestStack;
        $this->request = $this->requestStack->getCurrentRequest();
    }

    /**
     * @return Category|null
     */
    public function getCurrentCategory()
    {
        if ($this->request && $this->request->attributes->has('_route') && strpos($this->request->attributes->get('_route'), 'modules_team') !== false && $this->request->attributes->has('category') && $this->request->attributes->get('category') instanceof Category) {
            return $this->request->attributes->get('category');
        }

        return null;
    }

    public function getMemberCountForCategory(Category $category)
    {
        $members = $this->em->getRepository('ModulesTeamBundle:Member')->findByCategory($category);

        return count($members);
    }

    public function getAge(\DateTime $birthday)
    {
        $today = new \DateTime();
        $diff = $today->diff($birthday);

        return $diff->y;
    }

    public function getDaysByDateRange(\DateTime $start, \DateTime $end)
    {
        $days = [$start];

        if ($start < $end) {
            $dayWalker = clone $start;
            while ($dayWalker->diff($end)->days) {
                $dayWalker->modify('+1 day');
                $day = clone $dayWalker;
                $days[] = $day;
            }
        }

        return $days;
    }

    public function getDaysBySeriesRange(\DateTime $startSeries, \DateTime $endSeries, \DateTime $start, \DateTime $end, $choosenDays)
    {
        $days['start'][0] = $start;
        $days['ende'][0] = $end;

        $count = 1;
        if ($startSeries < $endSeries) {
            $dayWalker = clone $start;
            while ($dayWalker->diff($endSeries)->days) {
                $dayWalker->modify('+1 day');
                $day = clone $dayWalker;

                if (in_array(date('w', $day->getTimestamp()), $choosenDays) || empty($choosenDays)) {
                    $days['start'][$count] = $day;
                    $count++;
                }


            }
            $count = 1;
            $dayWalker = clone $end;
            while ($dayWalker->diff($endSeries)->days) {
                $dayWalker->modify('+1 day');
                $day = clone $dayWalker;

                if (in_array(date('w', $day->getTimestamp()), $choosenDays) || empty($choosenDays)) {
                    $days['ende'][$count] = $day;
                    $count++;
                }
            }
        }

        return $days;
    }

    public function isHoliday(\DateTime $day)
    {
        if ($day->format('N') == 7) {
            return true;
        }

        return false;
    }

    public function countPlansForMemberAndYear(Member $member, $year = null, $type = null)
    {
        if (!$year) {
            $year = date('Y');
        }

        $holidays = $this->em->getRepository('ModulesTeamBundle:Plan')->findForMemberAndYear($member, $year, $type);

        return count($holidays);
    }

    public function setMemberFilter()
    {
        $allMembers = $this->em->getRepository('ModulesTeamBundle:Member')->findAll();

        // handle form
        $request = $this->requestStack->getCurrentRequest();
        $settingsKey = 'mod_team_plan_member_filter';
        if ($request && $request->request->has($settingsKey)) {
            $this->settings->set($settingsKey, $request->request->get($settingsKey));
        }

        $memberIds = $this->settings->get($settingsKey, []);
        $memberFilter = [];
        $memberFilter['all'] = ['member' => null, 'active' => !count($memberIds) || in_array('all', $memberIds) // set 'all' active if no filter or 'all' in filter
        ];
        foreach ($allMembers as $member) {
            $memberFilter[$member->getId()] = ['member' => $member, 'active' => !count($memberIds) || in_array($member->getId(), $memberIds) // set member active if no filter or member in filter
            ];
        }

        return $memberFilter;
    }

    public function isFirstOfRow(Plan $plan)
    {
        return !$this->em->getRepository('ModulesTeamBundle:Plan')->findAdjacent($plan, 'left');
    }

    public function isLastOfRow(Plan $plan)
    {
        return !$this->em->getRepository('ModulesTeamBundle:Plan')->findAdjacent($plan, 'right');
    }
}