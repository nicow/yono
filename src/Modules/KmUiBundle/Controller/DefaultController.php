<?php

namespace Modules\KmUiBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    /**
     * @Route("/profile", name="kmui_profile")
     */
    public function profileAction()
    {
        return $this->render('ModulesKmUiBundle:Default:profile.html.twig');
    }
}