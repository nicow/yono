<?php

namespace Modules\KmUiBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/timetrack")
 */
class TimetrackController extends Controller
{
    /**
     * @Route("/", name="kmui_timetrack_index")
     */
    public function indexAction()
    {
        return $this->render('ModulesKmUiBundle:Timetrack:index.html.twig');
    }

    /**
     * @Route("/new", name="kmui_timetrack_new")
     */
    public function newAction()
    {
        return $this->render('ModulesKmUiBundle:Timetrack:new.html.twig');
    }
}