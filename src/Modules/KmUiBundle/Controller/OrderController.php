<?php

namespace Modules\KmUiBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/order")
 */
class OrderController extends Controller
{
    /**
     * @Route("/", name="kmui_order_index")
     */
    public function indexAction()
    {
        return $this->render('ModulesKmUiBundle:Order:index.html.twig');
    }

    /**
     * @Route("/new", name="kmui_order_new")
     */
    public function newAction()
    {
        return $this->render('ModulesKmUiBundle:Order:new.html.twig');
    }

    /**
     * @Route("/edit", name="kmui_order_edit")
     */
    public function editAction()
    {
        return $this->render('ModulesKmUiBundle:Order:edit.html.twig');
    }

    /**
     * @Route("/detail", name="kmui_order_detail")
     */
    public function detailAction()
    {
        return $this->render('ModulesKmUiBundle:Order:detail.html.twig');
    }
}