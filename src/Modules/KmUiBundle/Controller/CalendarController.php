<?php

namespace Modules\KmUiBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/calendar")
 */
class CalendarController extends Controller
{
    /**
     * @Route("/", name="kmui_calandar_index")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function calendarAction(Request $request)
    {
        $start = new \DateTime('monday this week');

        if ($request->query->get('deleted')) {
            $this->addFlash('success', '<i class="uk-icon-check"></i> Termin wurde gelöscht!');
        }

        return $this->render('ModulesKmUiBundle:Calendar:week.html.twig', ['start' => $start]);
    }

    /**
     * @Route("/month", name="kmui_calandar_month")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function monthAction(Request $request)
    {
        if ($request->query->get('deleted')) {
            $this->addFlash('success', '<i class="uk-icon-check"></i> Termin wurde gelöscht!');
        }

        return $this->render('ModulesKmUiBundle:Calendar:month.html.twig');
    }

    /**
     * @Route("/day", name="kmui_calandar_day")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function dayAction(Request $request)
    {
        if ($request->query->get('deleted')) {
            $this->addFlash('success', '<i class="uk-icon-check"></i> Termin wurde gelöscht!');
        }

        return $this->render('ModulesKmUiBundle:Calendar:day.html.twig');
    }

    /**
     * @Route("/new", name="kmui_calandar_new")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        if ($request->query->get('saved')) {
            $this->addFlash('success', '<i class="uk-icon-check"></i> Termin wurde gespeichert!');

            return $this->redirect($this->generateUrl('kmui_calandar_edit'));
        }

        return $this->render('ModulesKmUiBundle:Calendar:new.html.twig');
    }

    /**
     * @Route("/edit", name="kmui_calandar_edit")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request)
    {
        if ($request->query->get('saved')) {
            $this->addFlash('success', '<i class="uk-icon-check"></i> Termin wurde aktualisiert!');
        }

        if ($request->query->get('deleted')) {
            $this->addFlash('success', '<i class="uk-icon-check"></i> Termin wurde gelöscht!');

            return $this->redirect($this->generateUrl('kmui_calandar_index'));
        }

        return $this->render('ModulesKmUiBundle:Calendar:edit.html.twig');
    }
}