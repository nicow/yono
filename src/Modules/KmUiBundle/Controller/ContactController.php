<?php

namespace Modules\KmUiBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/contact")
 */
class ContactController extends Controller
{
    /**
     * @Route("/", name="kmui_contact_index")
     */
    public function indexAction(Request $request)
    {
        if ($request->query->get('deleted')) {
            $this->addFlash('success', '<i class="uk-icon-check"></i> Kontakt wurde gelöscht!');
        }

        return $this->render('ModulesKmUiBundle:Contact:index.html.twig');
    }

    /**
     * @Route("/lieferanten", name="kmui_contact_index_category")
     */
    public function indexCategoryAction(Request $request)
    {
        return $this->render('ModulesKmUiBundle:Contact:index_category.html.twig');
    }

    /**
     * @Route("/new", name="kmui_contact_new")
     */
    public function newAction(Request $request)
    {
        if ($request->query->get('saved')) {
            $this->addFlash('success', '<i class="uk-icon-check"></i> Kontakt wurde gespeichert!');
        }

        return $this->render('ModulesKmUiBundle:Contact:new.html.twig');
    }

    /**
     * @Route("/edit", name="kmui_contact_edit")
     */
    public function editAction(Request $request)
    {
        if ($request->query->get('saved')) {
            $this->addFlash('success', '<i class="uk-icon-check"></i> Kontakt wurde gespeichert!');
        }

        return $this->render('ModulesKmUiBundle:Contact:edit.html.twig');
    }

    /**
     * @Route("/detail", name="kmui_contact_detail")
     */
    public function detailAction()
    {
        return new RedirectResponse($this->generateUrl('kmui_contact_detail_address'));
    }

    /**
     * @Route("/detail/address", name="kmui_contact_detail_address")
     */
    public function addressAction(Request $request)
    {
        if ($request->query->get('deleted')) {
            $this->addFlash('success', '<i class="uk-icon-check"></i> Adresse wurde gelöscht!');
        }

        return $this->render('ModulesKmUiBundle:Contact:address.html.twig');
    }

    /**
     * @Route("/detail/address/edit", name="kmui_contact_detail_address_edit")
     */
    public function addressEditAction(Request $request)
    {
        if ($request->query->get('saved')) {
            $this->addFlash('success', '<i class="uk-icon-check"></i> Änderungen wurden gespeichert!');
        }

        return $this->render('ModulesKmUiBundle:Contact:address_edit.html.twig');
    }

    /**
     * @Route("/detail/address/new", name="kmui_contact_detail_address_new")
     */
    public function addressNewAction(Request $request)
    {
        if ($request->query->get('saved')) {
            $this->addFlash('success', '<i class="uk-icon-check"></i> Adresse wurde gespeichert!');
        }

        return $this->render('ModulesKmUiBundle:Contact:address_new.html.twig');
    }

    /**
     * @Route("/detail/contactpersons", name="kmui_contact_detail_contactpersons")
     */
    public function contactpersonsAction()
    {
        return $this->render('ModulesKmUiBundle:Contact:contactpersons.html.twig');
    }

    /**
     * @Route("/detail/contactpersons/new", name="kmui_contact_detail_contactpersons_new")
     */
    public function contactpersonsNewAction(Request $request)
    {
        if ($request->query->get('saved')) {
            $this->addFlash('success', '<i class="uk-icon-check"></i> Ansprechpartner wurde gespeichert!');
        }

        return $this->render('ModulesKmUiBundle:Contact:contactpersons_new.html.twig');
    }

    /**
     * @Route("/detail/accounts", name="kmui_contact_detail_accounts")
     */
    public function accountsAction()
    {
        return $this->render('ModulesKmUiBundle:Contact:acounts.html.twig');
    }

    /**
     * @Route("/detail/accounts/new", name="kmui_contact_detail_accounts_new")
     */
    public function accountsNewAction(Request $request)
    {
        if ($request->query->get('saved')) {
            $this->addFlash('success', '<i class="uk-icon-check"></i> Kontoverbindung wurde gespeichert!');
        }

        return $this->render('ModulesKmUiBundle:Contact:accounts_new.html.twig');
    }

    /**
     * @Route("/detail/projects", name="kmui_contact_detail_projects")
     */
    public function projectsAction()
    {
        return $this->render('ModulesKmUiBundle:Contact:projects.html.twig');
    }

    /**
     * @Route("/detail/tickets", name="kmui_contact_detail_tickets")
     */
    public function ticketsAction()
    {
        return $this->render('ModulesKmUiBundle:Contact:tickets.html.twig');
    }

    /**
     * @Route("/detail/users", name="kmui_contact_detail_users")
     */
    public function usersAction()
    {
        return $this->render('ModulesKmUiBundle:Contact:users.html.twig');
    }

    /**
     * @Route("/detail/users/new", name="kmui_contact_detail_users_new")
     */
    public function usersNewAction(Request $request)
    {
        if ($request->query->get('saved')) {
            $this->addFlash('success', '<i class="uk-icon-check"></i> Zugang wurde gespeichert!');
        }

        return $this->render('ModulesKmUiBundle:Contact:users_new.html.twig');
    }

    /**
     * @Route("/category", name="kmui_contact_category")
     */
    public function categoryAction()
    {
        return $this->render('ModulesKmUiBundle:Contact:category.html.twig');
    }

    /**
     * @Route("/category/new", name="kmui_contact_category_new")
     */
    public function categoryNewAction(Request $request)
    {
        if ($request->query->get('saved')) {
            $this->addFlash('success', '<i class="uk-icon-check"></i> Kategorie wurde gespeichert!');
        }

        return $this->render('ModulesKmUiBundle:Contact:category_new.html.twig');
    }

    /**
     * @Route("/category/edit", name="kmui_contact_category_edit")
     */
    public function categoryEditAction(Request $request)
    {
        if ($request->query->get('saved')) {
            $this->addFlash('success', '<i class="uk-icon-check"></i> Kategorie wurde gespeichert!');
        }

        return $this->render('ModulesKmUiBundle:Contact:category_edit.html.twig');
    }
}