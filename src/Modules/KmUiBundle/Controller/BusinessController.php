<?php

namespace Modules\KmUiBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/business")
 */
class BusinessController extends Controller
{
    /**
     * @Route("/", name="kmui_business_index")
     */
    public function indexAction()
    {
        return $this->render('ModulesKmUiBundle:Business:index.html.twig');
    }

    /**
     * @Route("/new", name="kmui_business_new")
     */
    public function newAction()
    {
        return $this->render('ModulesKmUiBundle:Business:new.html.twig');
    }

    /**
     * @Route("/edit", name="kmui_business_edit")
     */
    public function editAction()
    {
        return $this->render('ModulesKmUiBundle:Business:edit.html.twig');
    }
}