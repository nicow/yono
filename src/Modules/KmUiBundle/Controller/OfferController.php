<?php

namespace Modules\KmUiBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/offer")
 */
class OfferController extends Controller
{
    /**
     * @Route("/", name="kmui_offer_index")
     */
    public function indexAction()
    {
        return $this->render('ModulesKmUiBundle:Offer:index.html.twig');
    }

    /**
     * @Route("/new", name="kmui_offer_new")
     */
    public function newAction()
    {
        return $this->render('ModulesKmUiBundle:Offer:new.html.twig');
    }

    /**
     * @Route("/edit", name="kmui_offer_edit")
     */
    public function editAction()
    {
        return $this->render('ModulesKmUiBundle:Offer:edit.html.twig');
    }

    /**
     * @Route("/detail", name="kmui_offer_detail")
     */
    public function detailAction()
    {
        return $this->render('ModulesKmUiBundle:Offer:detail.html.twig');
    }
}