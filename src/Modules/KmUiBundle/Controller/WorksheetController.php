<?php

namespace Modules\KmUiBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/worksheet")
 */
class WorksheetController extends Controller
{
    /**
     * @Route("/", name="kmui_worksheet_index")
     */
    public function indexAction()
    {
        return $this->render('ModulesKmUiBundle:Worksheet:index.html.twig');
    }

    /**
     * @Route("/detail", name="kmui_worksheet_detail")
     */
    public function detailAction()
    {
        return $this->render('ModulesKmUiBundle:Worksheet:detail.html.twig');
    }

    /**
     * @Route("/edit", name="kmui_worksheet_edit")
     */
    public function editAction()
    {
        return $this->render('ModulesKmUiBundle:Worksheet:edit.html.twig');
    }

    /**
     * @Route("/new", name="kmui_worksheet_new")
     */
    public function newAction()
    {
        return $this->render('ModulesKmUiBundle:Worksheet:new.html.twig');
    }
}