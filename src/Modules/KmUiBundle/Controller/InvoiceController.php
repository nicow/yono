<?php

namespace Modules\KmUiBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/invoice")
 */
class InvoiceController extends Controller
{
    /**
     * @Route("/", name="kmui_invoice_index")
     */
    public function indexAction()
    {
        return $this->render('ModulesKmUiBundle:Invoice:index.html.twig');
    }

    /**
     * @Route("/new", name="kmui_invoice_new")
     */
    public function newAction()
    {
        return $this->render('ModulesKmUiBundle:Invoice:new.html.twig');
    }

    /**
     * @Route("/edit", name="kmui_invoice_edit")
     */
    public function editAction()
    {
        return $this->render('ModulesKmUiBundle:Invoice:edit.html.twig');
    }

    /**
     * @Route("/detail", name="kmui_invoice_detail")
     */
    public function detailAction()
    {
        return $this->render('ModulesKmUiBundle:Invoice:detail.html.twig');
    }
}