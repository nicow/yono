<?php

namespace Modules\KmUiBundle\Controller;


use Modules\OrderManagementBundle\Entity\Document;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class TBSController extends Controller
{
    /**
     * @Route("/tbs", name="kmui_tbs_index")
     */
    public function indexAction()
    {
        return $this->render('ModulesKmUiBundle:TBS:index.html.twig');
    }

    /**
     * @Route("/tbs/download", name="kmui_tbs_download")
     */
    public function downloadAction()
    {
//        $tbs = $this->get('app.opentbs');
//        $tbs->loadTemplate("/srv/www/vhosts/yono/src/Modules/OrderManagementBundle/Resources/documents/rechnung_email.odt");
//
//        dump($tbs->TplStore);

        $doc = new Document(Document::getDocument('report_jobticket.odt'));

        $em = $this->getDoctrine()->getManager();
        $em->persist($doc);
        $em->flush();

        dump($doc);


//        $tbs->Show(OPENTBS_FILE, '/home/nico/Schreibtisch/test_result.docx');
//
//        $converter = $this->get('app.documentconverter');
//        $converter->convertTo('/home/nico/Schreibtisch/test_result.docx', '/home/nico/Schreibtisch/test_result.pdf', 'pdf');


        return $this->render('ModulesKmUiBundle:TBS:index.html.twig', ['vars' => $doc->getVars(),

        ]);
    }
}