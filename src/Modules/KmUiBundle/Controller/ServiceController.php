<?php

namespace Modules\KmUiBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/service")
 */
class ServiceController extends Controller
{
    /**
     * @Route("/", name="kmui_service_index")
     */
    public function indexAction()
    {
        return $this->render('ModulesKmUiBundle:Service:index.html.twig');
    }

    /**
     * @Route("/new", name="kmui_service_new")
     */
    public function newAction()
    {
        return $this->render('ModulesKmUiBundle:Service:new.html.twig');
    }

    /**
     * @Route("/edit", name="kmui_service_edit")
     */
    public function editAction()
    {
        return $this->render('ModulesKmUiBundle:Service:edit.html.twig');
    }

    /**
     * @Route("/detail", name="kmui_service_detail")
     */
    public function detailAction()
    {
        return $this->render('ModulesKmUiBundle:Service:detail.html.twig');
    }
}