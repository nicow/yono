<?php

namespace Modules\KmUiBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/wiki")
 */
class WikiController extends Controller
{
    /**
     * @Route("/", name="kmui_wiki_index")
     */
    public function indexAction()
    {
        return $this->render('ModulesKmUiBundle:Wiki:index.html.twig');
    }

    /**
     * @Route("/new", name="kmui_wiki_new")
     */
    public function newAction()
    {
        return $this->render('ModulesKmUiBundle:Wiki:new.html.twig');
    }

    /**
     * @Route("/edit", name="kmui_wiki_edit")
     */
    public function editAction()
    {
        return $this->render('ModulesKmUiBundle:Wiki:edit.html.twig');
    }

    /**
     * @Route("/detail", name="kmui_wiki_detail")
     */
    public function detailAction()
    {
        return $this->render('ModulesKmUiBundle:Wiki:detail.html.twig');
    }
}