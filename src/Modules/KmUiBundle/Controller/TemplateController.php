<?php

namespace Modules\KmUiBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/template")
 */
class TemplateController extends Controller
{
    /**
     * @Route("/", name="kmui_template_index")
     */
    public function indexAction()
    {
        return $this->render('ModulesKmUiBundle:Template:index.html.twig');
    }

    /**
     * @Route("/new", name="kmui_template_new")
     */
    public function newAction()
    {
        return $this->render('ModulesKmUiBundle:Template:new.html.twig');
    }

    /**
     * @Route("/edit", name="kmui_template_edit")
     */
    public function editAction()
    {
        return $this->render('ModulesKmUiBundle:Template:edit.html.twig');
    }

    /**
     * @Route("/detail", name="kmui_template_detail")
     */
    public function detailAction()
    {
        return $this->render('ModulesKmUiBundle:Template:detail.html.twig');
    }
}