<?php

namespace Modules\KmUiBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/contact/form")
 */
class FormController extends Controller
{
    /**
     * @Route("/", name="kmui_form_index")
     */
    public function indexAction()
    {
        return $this->render('ModulesKmUiBundle:Contact/Form:index.html.twig');
    }

    /**
     * @Route("/new", name="kmui_form_new")
     */
    public function newAction()
    {
        return $this->render('ModulesKmUiBundle:Contact/Form:new.html.twig');
    }

    /**
     * @Route("/edit", name="kmui_form_edit")
     */
    public function editAction()
    {
        return $this->render('ModulesKmUiBundle:Contact/Form:edit.html.twig');
    }
}