<?php

namespace Modules\KmUiBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/project")
 */
class ProjectController extends Controller
{
    /**
     * @Route("/", name="kmui_project_index")
     */
    public function indexAction()
    {
        return $this->render('ModulesKmUiBundle:Project:index.html.twig');
    }

    /**
     * @Route("/detail", name="kmui_project_detail")
     */
    public function detailAction()
    {
        return $this->render('ModulesKmUiBundle:Project:detail.html.twig');
    }

    /**
     * @Route("/new", name="kmui_project_new")
     */
    public function newAction()
    {
        return $this->render('ModulesKmUiBundle:Project:new.html.twig');
    }

    /**
     * @Route("/edit", name="kmui_project_edit")
     */
    public function editAction()
    {
        return $this->render('ModulesKmUiBundle:Project:edit.html.twig');
    }
}