<?php

namespace Modules\KmUiBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/ticket")
 */
class TicketController extends Controller
{
    /**
     * @Route("/", name="kmui_ticket_index")
     */
    public function indexAction()
    {
        return $this->render('ModulesKmUiBundle:Ticket:index.html.twig');
    }

    /**
     * @Route("/detail", name="kmui_ticket_detail")
     */
    public function detailAction()
    {
        return $this->render('ModulesKmUiBundle:Ticket:detail.html.twig');
    }

    /**
     * @Route("/edit", name="kmui_ticket_edit")
     */
    public function editAction()
    {
        return $this->render('ModulesKmUiBundle:Ticket:edit.html.twig');
    }

    /**
     * @Route("/new", name="kmui_ticket_new")
     */
    public function newAction()
    {
        return $this->render('ModulesKmUiBundle:Ticket:new.html.twig');
    }
}