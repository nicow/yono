<?php

namespace Modules\KmUiBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/inventory")
 */
class InventoryController extends Controller
{
    /**
     * @Route("/", name="kmui_inventory_index")
     */
    public function indexAction()
    {
        return $this->render('ModulesKmUiBundle:Inventory:index.html.twig');
    }

    /**
     * @Route("/detail", name="kmui_inventory_detail")
     */
    public function detailAction()
    {
        return $this->render('ModulesKmUiBundle:Inventory:detail.html.twig');
    }

    /**
     * @Route("/new", name="kmui_inventory_new")
     */
    public function newAction(Request $request)
    {
        if ($request->query->get('saved')) {
            $this->addFlash('success', '<i class="uk-icon-check"></i> Arbeitsmittel wurde gespeichert!');
        }

        return $this->render('ModulesKmUiBundle:Inventory:new.html.twig');
    }

    /**
     * @Route("/new_plan", name="kmui_inventory_new_plan")
     */
    public function newPlanAction(Request $request)
    {
        if ($request->query->get('saved')) {
            $this->addFlash('success', '<i class="uk-icon-check"></i> Einsatzplanung wurde gespeichert!');
        }

        return $this->render('ModulesKmUiBundle:Inventory:new_plan.html.twig');
    }

    /**
     * @Route("/edit", name="kmui_inventory_edit")
     */
    public function editAction(Request $request)
    {
        if ($request->query->get('saved')) {
            $this->addFlash('success', '<i class="uk-icon-check"></i> Arbeitsmittel wurde gespeichert!');
        }

        return $this->render('ModulesKmUiBundle:Inventory:edit.html.twig');
    }

    /**
     * @Route("/search", name="kmui_inventory_search")
     */
    public function searchction()
    {
        return $this->render('ModulesKmUiBundle:Inventory:search.html.twig');
    }
}