<?php

namespace Modules\KmUiBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class MenuController
 * @package Modules\KmUiBundle\Controller
 * @Route("/menu")
 */
class MenuController extends Controller
{
    /**
     * @Route("/main", options={"expose"=true}, name="kmui_menu_main")
     */
    public function mainAction()
    {
        return $this->render('@CoreApp/menu-main.html.twig');
    }

    /**
     * @Route("/bookmarks", options={"expose"=true}, name="kmui_menu_bookmarks")
     */
    public function bookmarksAction()
    {
        return $this->render('@CoreApp/menu-bookmarks.html.twig');
    }

    /**
     * @Route("/favorites", options={"expose"=true}, name="kmui_menu_favorites")
     */
    public function favoritesAction()
    {
        return $this->render('@CoreApp/menu-favorites.html.twig');
    }
}