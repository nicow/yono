<?php

namespace Modules\KmUiBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/pricelist")
 */
class PricelistController extends Controller
{
    /**
     * @Route("/", name="kmui_pricelist_index")
     */
    public function indexAction()
    {
        return $this->render('ModulesKmUiBundle:Pricelist:index.html.twig');
    }

    /**
     * @Route("/new", name="kmui_pricelist_new")
     */
    public function newAction()
    {
        return $this->render('ModulesKmUiBundle:Pricelist:new.html.twig');
    }

    /**
     * @Route("/edit", name="kmui_pricelist_edit")
     */
    public function editAction()
    {
        return $this->render('ModulesKmUiBundle:Pricelist:edit.html.twig');
    }

    /**
     * @Route("/detail", name="kmui_pricelist_detail")
     */
    public function detailAction()
    {
        return $this->render('ModulesKmUiBundle:Pricelist:detail.html.twig');
    }
}