<?php

namespace Modules\KmUiBundle;

use Core\YonoBundleInterface;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class ModulesKmUiBundle extends Bundle implements YonoBundleInterface
{
    /**
     * @return string
     */
    public function getDisplayName()
    {
        return 'KM Haustechnik UI';
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return 'UI Planung für KM Haustechnik';
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return '';
    }
}
