<?php
/**
 * Created by PhpStorm.
 * User: mkt
 * Date: 14.02.17
 * Time: 18:43
 */

namespace Core\AppBundle\Twig;


class Filters extends \Twig_Extension
{
    public function getFilters()
    {
        return array(new \Twig_SimpleFilter('instanceof', [$this, 'instanceofFilter']),);
    }

    public function instanceofFilter($object, $class)
    {
        return $object instanceof $class;
    }

    public function getName()
    {
        return 'core_app_extension';
    }
}