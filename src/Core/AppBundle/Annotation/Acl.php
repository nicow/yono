<?php

namespace Core\AppBundle\Annotation;


/**
 * Acl
 *
 * Definition of the acl annotation
 *
 * @Annotation
 */
class Acl
{
    public $class;
    private $label;

    public function __construct($options)
    {
        if ( isset($options[ 'value' ]) ) {
            $this->label = $options[ 'value' ];
        }
        if ( !$this->label ) {
            throw new \Exception("@Acl annotation needs a string parameter or object with property 'label'.");
        }
    }

    public function getLabel()
    {
        return $this->label;
    }
}