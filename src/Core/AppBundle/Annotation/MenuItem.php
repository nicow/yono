<?php
/**
 * Created by PhpStorm.
 * User: mkt
 * Date: 26.08.16
 * Time: 17:39
 */

namespace Core\AppBundle\Annotation;


/**
 * Menu item
 *
 * Definition of the menu item annotation
 *
 * @Annotation
 */
class MenuItem
{
    /**
     * Label of the menu item shown to the user
     * @var string
     */
    private $label;

    /**
     * Route the menu item leads to
     * @var string
     */
    private $route;

    /**
     * Route parameter for dynamic routes
     * @var array
     */
    private $routeParameters = [];

    /**
     * Icon of the menu item shown to the user
     * @var string
     */
    private $icon;

    /**
     * FIXME ?
     * @var array
     */
    private $attributes = [];

    /**
     * Initializes the class properties by the given options
     * @param array $options Annotation parameter
     */
    public function __construct($options)
    {
        // Rename key "value" to "label"
        if ( isset($options[ 'value' ]) ) {
            $options[ 'label' ] = $options[ 'value' ];
            unset($options[ 'value' ]);
        }

        // Map the given properties in the option array to the class properties
        foreach ( $options as $key => $value ) {
            if ( !property_exists($this, $key) ) {
                throw new \InvalidArgumentException(sprintf('Property "%s" does not exist', $key));
            }

            $this->$key = $value;
        }
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @return string
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * @return array
     */
    public function getRouteParameters()
    {
        return $this->routeParameters;
    }

    /**
     * @return array
     */
    public function getAttributes()
    {
        return $this->attributes;
    }
}