<?php
/**
 * Created by PhpStorm.
 * User: mkt
 * Date: 26.08.16
 * Time: 17:04
 */

namespace Core\AppBundle\Menu;


use Core\AppBundle\Entity\MenuItem;
use Core\AppBundle\Entity\User;
use Core\AppBundle\Entity\UserGroup;
use Core\AppBundle\Event\ConfigureMenuEvent;
use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Main menu builder
 *
 * Declares a builder for the main menu as KnpMenuBundle needs it.
 *
 * @package Core\AppBundle\Menu
 */
class MainBuilder implements ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var ItemInterface
     */
    private $menu;

    /**
     * Sets the container.
     * @param ContainerInterface|null $container A ContainerInterface instance or null
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * Generates the main menu.
     * @param FactoryInterface $factory
     * @param array $options
     * @return ItemInterface
     */
    public function mainMenu(FactoryInterface $factory, array $options)
    {
        $this->createMenu($factory, $options);

        return $this->menu;
    }

    /**
     * Builds a menu by a database query related to the given topic and the logged in user.
     * @param FactoryInterface $factory
     * @param array $options
     * @param string $menu
     * @return ItemInterface
     * TODO: Try to reduce database queries.
     */
    private function createMenu(FactoryInterface $factory, array $options, $menu = 'main')
    {
        // create menu
        $this->menu = $factory->createItem($menu, $options);
        $this->menu->setChildrenAttributes(array_merge($this->menu->getChildrenAttributes(), $options));

        // get menu items from database by topic and user
        $em = $this->container->get('doctrine.orm.entity_manager');
        $menuItems = $em->getRepository('CoreAppBundle:MenuItem')->findByMenuAndUser($menu, $this->container->get('security.token_storage')->getToken()->getUser());
        $this->addChildren($factory, $this->menu, $menuItems, $this->container->get('security.token_storage')->getToken()->getUser());

        // dispatch event
        $this->container->get('event_dispatcher')->dispatch(ConfigureMenuEvent::CONFIGURE, new ConfigureMenuEvent($factory, $this->menu));

        return $this->menu;
    }

    /**
     * Adds the actual menu items recursive.
     * @param FactoryInterface $factory
     * @param ItemInterface $menu
     * @param array $children Database result array
     */
    private function addChildren(FactoryInterface $factory, ItemInterface $menu, $children)
    {
        $authChecker = $this->container->get('security.authorization_checker');

        /** @var MenuItem $child */
        foreach ( $children as $child ) {
            if ( $child->getPermissionsNeeded() ) {
                // Test if all permissions for the menu item are granted
                foreach ( $child->getPermissionsNeeded() as $permission ) {
                    if ( !$authChecker->isGranted($permission) && !$authChecker->isGranted('ROLE_ADMIN') ) {
                        // Jump to the end of the most outer foreach loop --> next child
                        continue 2;
                    }
                }
            }

            $options = ['uri' => $child->getUri(), 'extras' => ['id' => $child->getId(), 'icon' => $child->getIcon(), 'header' => $child->getHeader(), 'divider' => $child->getDivider()]];

            if ( $child->getRoute() ) {
                $options[ 'route' ] = $child->getRoute();
                $options[ 'routeParameters' ] = $child->getRouteParameters();
            }

            $item = $factory->createItem($child->getLabel(), $options);
            if ( $child->getAttributes() ) {
                $item->setAttributes($child->getAttributes());
            }
            if ( $child->getChildren() ) {
                $this->addChildren($factory, $item, $child->getChildren());
            }
            $menu->addChild($item);
        }
    }

    /**
     * Generates bookmarks menu.
     * @param FactoryInterface $factory
     * @param array $options
     * @return ItemInterface
     */
    public function bookmarksMenu(FactoryInterface $factory, array $options)
    {
        $this->createMenu($factory, $options, 'bookmarks');

        return $this->menu;
    }

    /**
     * Generates favorites menu.
     * @param FactoryInterface $factory
     * @param array $options
     * @return ItemInterface
     */
    public function favoritesMenu(FactoryInterface $factory, array $options)
    {
        $this->createMenu($factory, $options, 'favorites');

        return $this->menu;
    }
}