<?php

namespace Core\AppBundle\Search;

/**
 * Search item interface
 *
 * Classes which implement this interface are able to return search results and autocomplete suggestions for the global
 * search page.
 *
 * Author: joerg.sommer@crea.de
 *
 * Interface SearchItemInterface
 * @package Core\AppBundle\Search
 */
interface SearchItemInterface
{
    /**
     * Returns found entities in the database filtered by the given query.
     * @param $query Search query
     * @return array List of search hits as HTML partial strings
     */
    public function getResults($query);

    /**
     * Returns autocomplete suggestions by the given query.
     * @param $query Search query
     * @return array List of autocomplete suggestions as associative array
     */
    public function getAutocompleteResults($query);

    /**
     * Returns the label for the filter settings dialog
     * @return string Label
     */
    public function getName();

    /**
     * Returns the icon for the filter settings dialog
     * @return string Icon
     */
    public function getIcon();

    /**
     * Returns the permissions required to be allowed to search this entities.
     * @return array Permissions
     */
    public function getRequiredPermissions();
}