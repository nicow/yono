<?php
/**
 * Created by PhpStorm.
 * User: mkt
 * Date: 04.01.17
 * Time: 22:13
 */

namespace Core\AppBundle\Search;

/**
 * Search item
 *
 * Classes which extends this class are able to return search results and autocomplete suggestions for the global
 * search page. Label for the filter settings dialog defaults to the entity name. No default icon is set. The permissions
 * list is empty by default.
 *
 * Author: joerg.sommer@crea.de
 *
 * Interface SearchItemInterface
 * @package Core\AppBundle\Search
 */
abstract class SearchItem implements SearchItemInterface
{
    // FIXME public property? --> private und getter
    public $active = true;

    /**
     * Returns the label for the filter settings dialog by simply taking this class name
     * @return string Label
     */
    public function getName()
    {
        $name = array_pop(explode('\\', get_class($this)));

        return $name;
    }

    /**
     * Returns the icon for the filter settings dialog
     * @return string Icon
     */
    public function getIcon()
    {
        return '';
    }

    /**
     * Returns the permissions required to be allowed to search this entities. Returns an empty list as default list.
     * If you want to restrict this, overwrite this method.
     * @return array Permissions
     */
    public function getRequiredPermissions()
    {
        return [];
    }
}