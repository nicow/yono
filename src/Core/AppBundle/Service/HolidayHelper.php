<?php

namespace Core\AppBundle\Service;

class HolidayHelper

{
    const DAY_HOLIDAY_FULL = 0;
    const DAY_HOLIDAY_HALF = 1;
    const DAY_WORKDAY = 2;
    const DAY_SATURDAY = 3;
    const DAY_SUNDAY = 4;

    public function isDayHoliday(\Datetime $date)
    {

        // HolidayFull[]
        // fixed holidays
        $holidayFull = array(
            '0101', //Neujahr
            '0601', //Heilige Drei Könige
            '0105', //Tag der Arbeit
            '1508', //Mariaä Himmelfahrt
            '0310', //Tag der Deutschen Einheit
            '3110', //Reformationstag
            '0111', //Allerheiligen
            '2512', //Erster Weihnachtstag
            '2612', //Zweiter Weihnachtstag
        );

        // easter_date() as base for flexible holidays
        $easterDate = easter_date($date->format('Y'));
        $easter = new \DateTime();
        $easter->setTimestamp($easterDate);

        //flexible holidays into $holidayFull[]
        $holidayFull[] = $easter->format('dm'); // Ostersonntag
        $holidayFull[] = (clone $easter)->sub((new \DateInterval('P2D')))->format('dm');// Karfreitag
        $holidayFull[] = (clone $easter)->add((new \DateInterval('P1D')))->format('dm');// Ostermontag
        $holidayFull[] = (clone $easter)->add((new \DateInterval('P39D')))->format('dm');// Himmelfahrt
        $holidayFull[] = (clone $easter)->add((new \DateInterval('P49D')))->format('dm'); // Pfingstsonntag
        $holidayFull[] = (clone $easter)->add((new \DateInterval('P50D')))->format('dm'); // Pfingstmontag
        $holidayFull[] = (clone $easter)->add((new \DateInterval('P60D')))->format('dm'); // Fronleichnam


        //Buß- und Bettag fehlt noch (Mittwoch vor dem 23. November)

        // test if in_array holidaysFull[]
        if (in_array($date->format('dm'), $holidayFull)) {
            return self::DAY_HOLIDAY_FULL;
        }

        //HolidayHalf[]
        $holidayHalf = array(
            '2412', //Heilig Abend
            '3112', //Silvester
        );

        if (in_array($date->format('dm'), $holidayHalf)) {
            return self::DAY_HOLIDAY_HALF;
        }


        //test for weekend
        $weekend = $date->format('w');

        //SATURDAY
        if ($weekend == 6) {
            return self::DAY_SATURDAY;
        }

        //SUNDAY
        if ($weekend == 0) {
            return self::DAY_SUNDAY;
        }


        return self::DAY_WORKDAY;

    }


    public function getAllHolidaysForMonthAndYear($month, $year)
    {
        $days_in_month = cal_days_in_month(CAL_GREGORIAN, $month, $year);

        $dayArray = [];

        for ($i = 1; $i <= $days_in_month; $i++) {

            $dayArray[$i] = $this::isDayHoliday(new \DateTime(".$i.$month.$year."));
        }

        return $dayArray;
    }

    public function getAllHolidaysForYear($year)
    {
        $monthArray = [];

        for ($i = 1; $i <= 12; $i++) {

            $monthArray[$i] = $this->getAllHolidaysForMonthAndYear($i, $year);
        }

        return $monthArray;

    }


}