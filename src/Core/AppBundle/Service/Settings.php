<?php
/**
 * Created by PhpStorm.
 * User: mkt
 * Date: 02.01.17
 * Time: 01:32
 */

namespace Core\AppBundle\Service;


use Core\AppBundle\Entity\Setting;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

/**
 * User setting service
 *
 * Service to store and load single settings of a user.
 * Container service id: app.settings
 *
 * Author: joerg.sommer@crea.de
 */
class Settings
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var TokenStorage
     */
    private $tokenStorage;

    public function __construct(EntityManager $em, TokenStorage $tokenStorage)
    {
        $this->em = $em;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * Gets a global setting value.
     * @param string $name Name of the single setting
     * @param null $default Setting value which will be returned if no setting of this name is loadable
     * @return mixed Single value as scalar type, arrays as an associative array
     */
    public function getGlobal($name, $default = null)
    {
        return $this->get($name, $default, true);
    }

    /**
     * Returns a setting value of a given setting name.
     * @param string $name Name of the single setting
     * @param string|null $default Setting value which will be returned if there is no setting for this name
     * @param bool $global Setting is not linked to a user
     * @return mixed Single value as scalar type, arrays as an associative array
     */
    public function get($name, $default = null, $global = false)
    {
        if ( $global ) {
            // Look for a setting of this name without user dependencies...
            $setting = $this->em->getRepository('CoreAppBundle:Setting')->findOneBy(['name' => $name, 'user' => null]);
        } else {
            // Try to find a user setting if a user is logged in. If not, return the global setting
            $user = $this->tokenStorage->getToken() ? $this->tokenStorage->getToken()->getUser() : null;
            $setting = $this->em->getRepository('CoreAppBundle:Setting')->findOneBy(['name' => $name, 'user' => $user]);
        }
        if ( $setting ) {
            // If the setting exists, return it
            return $setting->getValue();
        }

        // If no setting is found the default value is returned
        return $default;
    }

    /**
     * Sets a global settings value.
     * @param string $name Name of the single setting
     * @param mixed $value Value as scalar type or associative array
     */
    public function setGlobal($name, $value)
    {
        $this->set($name, $value, true);
    }

    /**
     * Sets a settings value.
     * @param string $name Name of the single setting
     * @param mixed $value Value as scalar type or associative array
     * @param bool $global If true the setting is stored without user relation
     */
    public function set($name, $value, $global = false)
    {
        // FIXME Effizienter:
        // $user = null;
        // if ($global == false) {
        // $user = $this->tokenStorage->getToken() ? $this->tokenStorage->getToken()->getUser() : null;
        // }
        // weitere Zeilen dann anpassen

        $user = $this->tokenStorage->getToken() ? $this->tokenStorage->getToken()->getUser() : null;

        // Query allready stored setting with this name
        if ( $global ) {
            $setting = $this->em->getRepository('CoreAppBundle:Setting')->findOneBy(['name' => $name, 'user' => null]);
        } else {
            $setting = $this->em->getRepository('CoreAppBundle:Setting')->findOneBy(['name' => $name, 'user' => $user]);
        }

        if ( !$setting ) {
            // Setting does not exist allready --> create new one
            $setting = new Setting();
            $setting->setName($name);
            if ( !$global ) {
                $setting->setUser($user);
            }
            $this->em->persist($setting);
        }
        $setting->setValue($value);
        $this->em->flush();
    }
}