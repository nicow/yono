<?php
/**
 * Created by PhpStorm.
 * User: mkt
 * Date: 06.10.16
 * Time: 14:37
 */

namespace Core\AppBundle\Service;


use Doctrine\ORM\EntityManager;
use GuzzleHttp\Client as HttpClient;
use Symfony\Bundle\FrameworkBundle\Routing\Router;

class OAuth
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var Router
     */
    private $router;

    private $accessToken;

    private $expiresAt = 0;

    private $schema;

    private $host;

    public function __construct(EntityManager $em, Router $router, $schema, $host)
    {
        $this->em = $em;
        $this->router = $router;
        $this->schema = $schema;
        $this->host = $host;
    }

    /**
     * @return string
     */
    public function getAccessToken()
    {
        if ( !$this->accessToken || time() > $this->expiresAt ) {
            $this->refreshToken();
        }

        return $this->accessToken ? $this->accessToken->access_token : null;
    }

    /**
     * @param string $accessToken
     */
    public function setAccessToken($accessToken)
    {
        $this->accessToken = $accessToken;
    }

    private function refreshToken()
    {
        $client = $this->em->getRepository('CoreOAuthBundle:Client')->findOneBy(['name' => '_self']);

        if ( !$client ) {
            throw new \Exception("The interal oauth client (_self) was not found.");
        }

        $http = new HttpClient();
        $response = $http->post($this->schema . '://' . $this->host . $this->router->generate('fos_oauth_server_token'), ['form_params' => ['client_id' => $client->getId() . '_' . $client->getRandomId(), 'client_secret' => $client->getSecret(), 'grant_type' => 'client_credentials']]);

        $this->accessToken = json_decode($response->getBody());
        if ( $this->accessToken ) {
            $this->expiresAt = time() + $this->accessToken->expires_in;
        }
    }

}