<?php

namespace Core\AppBundle\Service;

use Symfony\Component\Process\Process;

/**
 * Document converter
 *
 * Converts a document from one format in another. The possible import and export filter are the same as in the
 * OpenOffice suite.
 * Container service id: FIXME
 *
 * Author: joerg.sommer@crea.de
 */
class DocumentConverter
{
    /**
     * Converts documents from one format to another
     * @param string $originFilePath Origin File Path
     * @param string $outputFilePath Output directory path
     * @param string $toFormat Format to export to
     * @return string Standard output of the convert without the error output
     */
    public function convertTo($originFilePath, $outputFilePath, $toFormat)
    {
        $command = 'unoconv --format %s --output %s %s';
        $command = sprintf($command, $toFormat, $outputFilePath, $originFilePath);

        $process = new Process($command);
        $process->run();

        return $process->getOutput();
    }
}