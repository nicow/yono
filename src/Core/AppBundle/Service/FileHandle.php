<?php

namespace Core\AppBundle\Service;

use Core\AppBundle\Entity\Documents;
use Doctrine\ORM\EntityManager;
use Modules\OrderManagementBundle\Entity\Document;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Routing\Router;

/**
 * File handle
 *
 * Methods to handle uploaded files
 *
 * Author: joerg.sommer@crea.de
 *
 * @package Core\AppBundle\Service
 */
class FileHandle
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var Router
     */
    private $router;

    /**
     * @var string
     */
    private $kernelRootDir;

    public function __construct(EntityManager $entityManager, Router $router, $kernelRootDir)
    {
        $this->em = $entityManager;
        $this->router = $router;

        $this->kernelRootDir = $this->normalizePath($kernelRootDir);
    }

    /**
     * Moves a uploaded file to a new location depending on the given entity and a primary key id. Stores needed
     * information in the database and returns the document entity object.
     * @param UploadedFile $file File object of the uploaded file
     * @param $id Primary key of the entity the document is related to
     * @param $entity Namespace of the entity the document is related to
     * @return Documents Stored document object
     */
    public function internalDocumentUploadAction(UploadedFile $file, $id, $entity)
    {
        $em = $this->em;

        // Create path from entity namespace
        $path = str_replace('\\', '/', $entity);
        $path = str_replace('Modules/', '', $path);
        $path = str_replace('/Entity/', '/', $path);

        // Create document object and persist it in the database
        $document = new Documents();
        $document->setName(time() . '_' . $file->getClientOriginalName());
        $document->setMime($file->getClientMimeType());
        $document->setSize($file->getClientSize());
        $document->setEntity($entity);
        $document->setEntry($id);
        $document->setPath('../data/' . $path . '/' . $id . '/');
        $file->move('../data/' . $path . '/' . $id . '/', time() . '_' . $file->getClientOriginalName());
        $em->persist($document);
        $em->flush($document);

        return $document;
    }

    /**
     * @param $id
     * @param $table
     * @return string
     */
    public function getCompiledPath($id, $table)
    {
        $em = $this->em;

        $fs = new Filesystem();

        //CREATE PATH FROM ENTITY PATH
        $path = str_replace('\\', '/', $table);
        $path = str_replace('Modules/', '', $path);
        $path = str_replace('/Entity/', '/', $path);

        $name = Document::generateRandomString(10);

        $path = 'data/' . $path . '/' . $id . '/compiled/';

        $path = str_replace('app/','',$this->kernelRootDir . '/' . $path);

//        if (DIRECTORY_SEPARATOR != '/') {
//            $path = str_replace('/', '\\', $path);
//        }
        if (!$fs->exists($path)) {
            $fs->mkdir($path);
        }

        return $path . $name . '.odt';
    }

    /**
     * @param $id
     * @param $number
     * @param $table
     * @return string
     */
    public function getConvertedPath($id,$number, $table, $newExtention = 'pdf')
    {
        $em = $this->em;

        $fs = new Filesystem();

        //CREATE PATH FROM ENTITY PATH
        $path = str_replace('\\', '/', $table);
        $path = str_replace('Modules/', '', $path);
        $path = str_replace('/Entity/', '/', $path);

        $name = str_replace('&','',$number);

        $path = 'data/' . $path . '/' . $id . '/converted/';

        $path = str_replace('app/','',$this->kernelRootDir . '/' . $path);


        if (!$fs->exists($path)) {
            $fs->mkdir($path);
        }

        return $path . $name . '.' . $newExtention;
    }

    private function normalizePath($path)
    {
        $parts = array();// Array to build a new path from the good parts
        $path = str_replace('\\', '/', $path);// Replace backslashes with forwardslashes
        $path = preg_replace('/\/+/', '/', $path);// Combine multiple slashes into a single slash
        $segments = explode('/', $path);// Collect path segments
        $test = '';// Initialize testing variable
        foreach($segments as $segment)
        {
            if($segment != '.')
            {
                $test = array_pop($parts);
                if(is_null($test))
                    $parts[] = $segment;
                else if($segment == '..')
                {
                    if($test == '..')
                        $parts[] = $test;

                    if($test == '..' || $test == '')
                        $parts[] = $segment;
                }
                else
                {
                    $parts[] = $test;
                    $parts[] = $segment;
                }
            }
        }
        return implode('/', $parts);
    }
}