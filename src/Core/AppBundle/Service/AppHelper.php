<?php

namespace Core\AppBundle\Service;

use Core\AppBundle\Entity\Documents;
use Core\AppBundle\Entity\User;
use Core\AppBundle\Entity\UserGroup;
use Core\YonoBundleInterface;
use Doctrine\ORM\Mapping\ClassMetadata;
use FOS\UserBundle\Model\Group;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Application helper service
 *
 * Collection of utility/helper methods
 * Container service id: app.helper
 *
 * @package Core\AppBundle\Service
 */
class AppHelper
{
    const BUNDLES_ALL = 'all';
    const BUNDLES_YONO = 'yono';
    const BUNDLES_YONO_MODULES = 'yono_modules';
    const BUNDLES_YONO_CORE = 'yono_core';
    const BUNDLES_NOT_YONO = 'not_yono';

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * @var \Symfony\Component\HttpFoundation\RequestStack
     */
    private $request;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->em = $this->container->get('doctrine.orm.entity_manager');
        $this->request = $this->container->get('request_stack')->getCurrentRequest();
    }

    /**
     * Get entity manager
     * @return \Doctrine\ORM\EntityManager|object
     */
    public function getEntityManager()
    {
        return $this->em;
    }

    /**
     * Check if a module is active. Therefor modules parameter in the parameter.yml must be 'all' or contain the module
     * name in an array.
     * @param $module Module to check
     * @return bool True, if module is active
     */
    public function isActiveModule($module)
    {
        $modules = $this->getParameter('modules');

        if ( $modules == 'all' || (is_array($modules) && in_array($module, $modules)) )
            return true;

        return false;
    }

    /**
     * Returns a configuration parameter from parameters.yml.
     * @param $key Name of the parameter
     * @return mixed Value of the parameter FIXME Type?
     */
    public function getParameter($key)
    {
        return $this->container->getParameter($key);
    }

    /**
     * Returns a fresh access token for the internal oauth client (_self).
     * @return string
     */
    public function getAccessToken()
    {
        return $this->container->get('app.oauth')->getAccessToken();
    }

    /**
     * Returns the name of the table of a given entity
     * @param $entity Entity, the table name is ask for
     * @return string Name of the table
     */
    public function getTableName($entity)
    {
        $className = $this->em->getClassMetadata(get_class($entity))->getName();

        return $className;
    }

    /**
     * Returns all documents related to the given row and entity
     * @param $entity Entity, the documents are needed for
     * @param $id Primary key of the row of the given entity
     * @return Documents[] Related documents
     */
    public function getDocumentsForEntity($entity, $id)
    {
        $repo = $this->em->getRepository('CoreAppBundle:Documents');

        $className = $this->getTableName($entity);

        $documents = $repo->findBy(['entity' => $className, 'entry' => $id]);

        return $documents;
    }

    /**
     * Returns the configured starting page of the logged in user
     * @return string URL of the starting page
     */
    public function getUserHomeUrl()
    {
        // Get the user object of the user that is logged in the moment
        $user = $this->container->get('security.token_storage')->getToken()->getUser();

        $homeRoute = $user->getHomeRoute();
        if ( $homeRoute && $this->container->get('router')->getRouteCollection()->get($homeRoute) ) {
            // If the user has a starting page configured and if this route exists
            // generate a absolute URL or path for the starting page based on the given parameters
            return $this->container->get('router')->generate($homeRoute, $user->getHomeParameters());
        }

        // Default is leading the user to the dashboard
        return $this->container->get('router')->generate('dashboard');
    }

    /**
     * Returns if there is a menu item that leads to the current route in the given menu
     * @param string $menu Menu
     * @return bool True, if there is any
     */
    public function isCurrentInMenu($menu = 'main')
    {
        // Get the current route
        // FIXME Does not work after a redirect --> StackOverFlow
        $route = $this->request->attributes->get('_route');
        // Get the parameters of the current route
        $routeParameters = $this->request->attributes->get('_route_params');

        $menuItem = $this->em->getRepository('CoreAppBundle:MenuItem')->findByRouteAndMenu($route, $routeParameters, $menu);
        return $menuItem ? true : false;
    }

    /**
     * FIXME Setter für eine get-Methode? besser getTableColumns?
     * @param $setting
     * @param $defaults
     * @return array
     */
    public function setTableColumns($setting, $defaults)
    {
        // set options with defaults
        $resolver = new OptionsResolver();
        $resolver->setDefaults($defaults);

        // handle form
        if ( $this->request && $this->request->request->has('table_columns') ) {
            $form = $this->request->request->get('table_columns');
            $settings = [];

            foreach ( $defaults as $key => $default ) {
                $settings[ $key ][ 'label' ] = $defaults[ $key ][ 'label' ];
                if ( array_key_exists($key, $form) ) {
                    $settings[ $key ][ 'active' ] = 1;
                } else {
                    $settings[ $key ][ 'active' ] = 0;
                }
            }

            $this->container->get('app.settings')->set($setting, $settings);
        }

        // get columns
        return $resolver->resolve((array)$this->container->get('app.settings')->get($setting, []));
    }

    /**
     * Returns the quantity of rows a table should show. For this the URL parameter "table_rows" is used first,
     * then stored settings in the database, then the given default value. If the URL parameter exists it is stored
     * in the database.
     * FIXME Setter für eine get-Methode? besser getTableRowsPerPage?
     * @param $setting
     * @param int $default Default value if not quantity can be found
     * @return int Quantity of table rows
     */
    function setTablePerPage($setting, $default = 10)
    {
        // Store URL parameter in the database if exists
        if ( $this->request && $this->request->request->has('table_rows') ) {
            $this->container->get('app.settings')->set($setting, (int)$this->request->request->get('table_rows'));
        }

        $perPage = $this->container->get('app.settings')->get($setting, $default);

        return $perPage;
    }

    /**
     * Returns the quantity of users belonging to the given group
     * @param UserGroup $group Group
     * @return int Quantity of users
     */
    function userCountByGroup(UserGroup $group)
    {
        $users = $this->em->getRepository('CoreAppBundle:User')->findByGroup($group);
        if ( $users ) {
            return count($users);
        }

        return 0;
    }

    /**
     * FIXME ?
     * @return array
     */
    function getSearchItems()
    {
        $search = $this->container->get('app.search');
        $searchItems = $search->getSearchItems();
        $filter = $this->container->get('app.settings')->get('core_app_search_filter', []);
        $bundles = $this->getBundles(self::BUNDLES_YONO);
        $bundledSearchItems = [];

        foreach ( $bundles as $bundle ) {
            $items = [];
            foreach ( $searchItems as $searchItem ) {
                if ( strpos(get_class($searchItem), $bundle->getNamespace()) !== false ) {
                    if ( array_key_exists($search->getSearchItemName($searchItem), $filter) ) {
                        $searchItem->active = (bool)$filter[ $search->getSearchItemName($searchItem) ];
                    }
                    $items[ $search->getSearchItemName($searchItem) ] = $searchItem;
                }
            }
            if ( $items ) {
                $bundledSearchItems[ $bundle->getName() ] = ['label' => $bundle->getDisplayName(), 'icon' => $bundle->getIcon(), 'items' => $items];
            }
        }

        return $bundledSearchItems;
    }

    /**
     * Returns an array of bundle objects.
     * @param string $filter Which bundles do you want? [BUNDLES_ALL, BUNDLES_YONO, BUNDLES_YONO_MODULES, BUNDLES_YONO_CORE, BUNDLES_NOT_YONO]
     * @return array List of bundles
     */
    public function getBundles($filter = self::BUNDLES_ALL)
    {
        $bundles = [];
        // Get all bundles currently available
        $allBundles = $this->container->get('kernel')->getBundles();

        // Filter all bundles currently available...
        switch ( $filter ) {
            // no filter
            case self::BUNDLES_ALL:
                $bundles = $allBundles;
                break;
            // yono bundles only
            case self::BUNDLES_YONO:
                foreach ( $allBundles as $bundle ) {
                    if ( $bundle instanceof YonoBundleInterface ) {
                        $bundles[] = $bundle;
                    }
                }
                break;
            // yono bundles which are modules
            case self::BUNDLES_YONO_MODULES:
                foreach ( $allBundles as $bundle ) {
                    if ( $bundle instanceof YonoBundleInterface ) {
                        if ( strpos(get_class($bundle), 'Modules\\') === 0 ) {
                            $bundles[] = $bundle;
                        }
                    }
                }
                break;
            // yono bundle which are part of the core
            case self::BUNDLES_YONO_CORE:
                foreach ( $allBundles as $bundle ) {
                    if ( $bundle instanceof YonoBundleInterface ) {
                        if ( strpos(get_class($bundle), 'Core\\') === 0 ) {
                            $bundles[] = $bundle;
                        }
                    }
                }
                break;
            // bandles which are not part of yono
            case self::BUNDLES_NOT_YONO:
                foreach ( $allBundles as $bundle ) {
                    if ( !$bundle instanceof YonoBundleInterface ) {
                        $bundles[] = $bundle;
                    }
                }
                break;
        }

        return $bundles;
    }

    /**
     * Returns the user setting if a user is logged in, otherwise the global one
     * @param string $name Name of the single setting
     * @param string|null $default Setting value which will be returned if there is no setting for this name
     * @return string|null Value of the setting
     */
    function getSetting($name, $default = null)
    {
        return $this->container->get('app.settings')->get($name, $default);
    }

    /**
     * Collects entities with acl support by Acl annotations.
     * FIXME ?
     * @return array
     */
    public function getAclEntities()
    {
        $entities = [];

        // get doctrine mapping information
        $mappings = $this->em->getMetadataFactory()->getAllMetadata();

        // loop through all mapped entities and find @Acl annotations
        /** @var ClassMetadata $mapping */
        foreach ( $mappings as $mapping ) {
            $entity = new \ReflectionClass($mapping->getName());

            /** @var \Core\AppBundle\Annotation\Acl $aclAnnotation */
            $aclAnnotation = $this->container->get('annotation_reader')->getClassAnnotation($entity, 'Core\\AppBundle\\Annotation\\Acl');

            if ( $aclAnnotation ) {
                $bundle = $this->getBundleFromNamespace($mapping->getName());
                $aclAnnotation->class = $mapping->getName();
                $entities[ $bundle->getDisplayName() ][ $mapping->getName() ] = $aclAnnotation;
            }
        }

        return $entities;
    }

    /**
     * Returns the yono bundle the namespace is in
     * @param object|string $namespace Namespace
     * @return YonoBundleInterface Bundle
     */
    public function getBundleFromNamespace($namespace)
    {
        // Converts an object to a string
        if ( is_object($namespace) ) {
            $namespace = get_class($namespace);
        }

        // Get all yono bundles
        $bundles = $this->getBundles(self::BUNDLES_YONO);

        foreach ( $bundles as $type => $bundle ) {
            $bundleRefClass = new \ReflectionClass($bundle);
            // Looks if namespace string starts with bundle name
            if ( strpos($namespace, $bundleRefClass->getNamespaceName()) !== false ) {
                return $bundle;
            }
        }
    }

    /**
     * Returns if an action on an entity is allowed for a specific user/group
     * @param $identity User or group object
     * @param $attribute "VIEW", "CREATE", "EDIT", "DELETE" entity
     * @param $subject Entity object or class name with namespace which should be protected
     * @return mixed True, if access ist granted
     */
    public function can($identity, $attribute, $subject)
    {
        // calls Core\AppBundle\Security\Voters\PermissionVoter
        return $this->container->get('app.permission.voter')->can($identity, $attribute, $subject);
    }

    /**
     * @param Group $group
     * @param $permissions
     */
    public function saveGroupPermissions(Group $group, $permissions)
    {
        // remove current permission roles
        foreach ( $group->getRoles() as $role ) {
            if ( strpos($role, 'ROLE_PERM_') === 0 ) {
                $group->removeRole($role);
            }
        }

        // add new permission roles
        foreach ( $permissions as $class => $attributes ) {
            $classRole = strtoupper(str_replace('\\', '_', $class));
            foreach ( $attributes as $attribute => $active ) {
                if ( $active ) {
                    $role = 'ROLE_PERM_' . $attribute . '_' . $classRole;
                    $group->addRole($role);
                }
            }

            // add dependet roles
            if ( $group->hasRole('ROLE_PERM_DELETE_' . $classRole) ) {
                if ( !$group->hasRole('ROLE_PERM_EDIT_' . $classRole) ) {
                    $group->addRole('ROLE_PERM_EDIT_' . $classRole);
                }
            }
            if ( $group->hasRole('ROLE_PERM_EDIT_' . $classRole) ) {
                if ( !$group->hasRole('ROLE_PERM_CREATE_' . $classRole) ) {
                    $group->addRole('ROLE_PERM_CREATE_' . $classRole);
                }
            }
            if ( $group->hasRole('ROLE_PERM_CREATE_' . $classRole) ) {
                if ( !$group->hasRole('ROLE_PERM_VIEW_' . $classRole) ) {
                    $group->addRole('ROLE_PERM_VIEW_' . $classRole);
                }
            }
        }

        $this->em->flush();
    }

    public function saveUserPermissions(User $user, $permissions)
    {
        // remove current permission roles
        foreach ( $user->getRoles() as $role ) {
            if ( strpos($role, 'ROLE_PERM_') === 0 ) {
                $user->removeRole($role);
            }
        }

        // add new permission roles
        foreach ( $permissions as $class => $attributes ) {
            $classRole = strtoupper(str_replace('\\', '_', $class));
            foreach ( $attributes as $attribute => $active ) {
                if ( $active ) {
                    $role = 'ROLE_PERM_' . $attribute . '_' . $classRole;
                    $user->addRole($role);
                }
            }

            // add dependet roles
            if ( $user->hasRole('ROLE_PERM_DELETE_' . $classRole) ) {
                if ( !$user->hasRole('ROLE_PERM_EDIT_' . $classRole) ) {
                    $user->addRole('ROLE_PERM_EDIT_' . $classRole);
                }
            }
            if ( $user->hasRole('ROLE_PERM_EDIT_' . $classRole) ) {
                if ( !$user->hasRole('ROLE_PERM_CREATE_' . $classRole) ) {
                    $user->addRole('ROLE_PERM_CREATE_' . $classRole);
                }
            }
            if ( $user->hasRole('ROLE_PERM_CREATE_' . $classRole) ) {
                if ( !$user->hasRole('ROLE_PERM_VIEW_' . $classRole) ) {
                    $user->addRole('ROLE_PERM_VIEW_' . $classRole);
                }
            }
        }

        // empty users permissions if they are equal to those inherited from groups
        $groupRoles = [];
        $userRoles = [];

        foreach ( $user->getGroups() as $group ) {
            $groupRoles = array_merge($groupRoles, $group->getRoles());
        }

        foreach ( $user->getRoles() as $role ) {
            if ( strpos($role, 'ROLE_PERM_') === 0 ) {
                $userRoles[] = $role;
            }
        }

        $groupRoles = array_unique($groupRoles);
        $userRoles = array_unique($userRoles);
        sort($groupRoles);
        sort($userRoles);

        if ( $groupRoles == $userRoles ) {
            foreach ( $userRoles as $role ) {
                $user->removeRole($role);
            }
        }

        $this->em->flush();
    }

    /**
     * Returns all users of this instance
     * @return array List of users
     */
    public function getUsers()
    {
        return $this->em->getRepository('CoreAppBundle:User')->findAll();
    }

    /**
     * Returns all groups of this instance
     * @return array
     */
    public function getGroups()
    {
        return $this->em->getRepository('CoreAppBundle:UserGroup')->findAll();
    }
}