<?php
/**
 * Created by PhpStorm.
 * User: mkt
 * Date: 30.08.16
 * Time: 17:49
 */

namespace Core\AppBundle\Service;


use Core\AppBundle\Entity\Notification;
use Core\AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;

/**
 * User notification service
 *
 * Service to store and load user notifications (FIXME till now only store - perhaps a good location to get them, too)
 * Container service id: FIXME
 *
 * Author: joerg.sommer@crea.de
 */
class Notifications
{
    /**
     * @var EntityManager
     */
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * Stores an user notification
     *
     * @param string $title Title of the notification
     * @param null $description Description of the notification
     * @param User|null $user User the notification is for
     * @param null $icon Icon of the notification
     * @param null $link Link of the notification
     * @param null $context
     */
    public function add($title, $description = null, User $user = null, $icon = null, $link = null, $context = null)
    {
        $notification = new Notification();
        $notification->setTitle($title);
        $notification->setDescription($description);
        $notification->setUser($user);
        $notification->setIcon($icon);
        $notification->setLink($link);
        $notification->setContext($context);
        $notification->setDate(new \DateTime());

        $this->em->persist($notification);
        $this->em->flush();
    }
}