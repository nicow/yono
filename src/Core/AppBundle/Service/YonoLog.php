<?php
/**
 * Created by PhpStorm.
 * User: nico
 * Date: 31.03.17
 * Time: 11:49
 */

namespace Core\AppBundle\Service;


use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * FIXME Warum nicht MonoLog? Markus fragen
 * NOT USED
 *
 *
 * Class YonoLog
 * @package Core\AppBundle\Service
 */
class YonoLog
{
    const TYPE_INFO = 'INFO';
    const TYPE_WARNING = 'WARNING';
    const TYPE_ERROR = 'ERROR';
    const TYPE_DEBUG = 'DEBUG';
    protected $stream;
    protected $options;
    protected $twig;
    protected $mailer;
    /**
     * @var ContainerInterface
     */
    private $container;
    private $logDir;
    private $logFile = 'yono.log';
    /**
     * @var string
     * [TYPE][DD.MM.YYY][H:i][CLASS][METHOD]: MESSAGE
     */
    private $logFormat = '[%s][%s][%s][%s][%s]: %s';

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->twig = $container->get('twig');
        $this->mailer = $container->get('mailer');
        $this->logDir = $container->getParameter('kernel.logs_dir');
        $this->stream = fopen($this->logDir . DIRECTORY_SEPARATOR . $this->logFile, 'a+');
    }

    public function setOptions(array $options = [])
    {
        if ( !isset($options[ 'class' ]) ) {
            throw new \Exception("Missing Option \"class\"!");
        }

        if ( !isset($options[ 'method' ]) ) {
            throw new \Exception("Missing Option \"method\"!");
        }

        if ( !isset($options[ 'type' ]) ) {
            throw new \Exception("Missing Option \"type\"!");
        }

        if ( !isset($options[ 'message' ]) ) {
            throw new \Exception("Missing Option \"message\"!");
        }

        $this->options = $options;
    }

    public function __destruct()
    {
        fwrite($this->stream, $this->getMessage());

        // FIXME Admin EMail-Adresse sollten irgendwo zentral in den Einstellungen gespeichert werden (parameter.yml)
        if ( $this->options[ 'type' ] == self::TYPE_ERROR ) {
            $message = \Swift_Message::newInstance()->setSubject('Fatal Error in Yono Detected')->setFrom('james@yono.eu')->setTo('tim.strakerjahn@crea.de')->setTo('nico.wehmoeller@crea.de')->setBody($this->twig->renderView(// app/Resources/views/Emails/registration.html.twig
                    'CoreAppBundle:Default:errorEmail.html.twig', array('error' => $this->getMessage())), 'text/html');
            $this->mailer->send($message);
        }

    }

    public function getMessage()
    {
        return sprintf($this->logFormat, $this->options[ 'type' ], date('d.m.Y'), date('H:i'), $this->options[ 'class' ], str_replace($this->options[ 'class' ] . '::', '', $this->options[ 'method' ]), $this->options[ 'message' ]) . PHP_EOL;
    }


}