<?php
/**
 * Created by PhpStorm.
 * User: mkt
 * Date: 25.01.17
 * Time: 12:02
 */

namespace Core\AppBundle\Service;


use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Routing\Generator\UrlGenerator;

class Google
{
    /**
     * @var Router
     */
    private $router;

    /**
     * @var \Google_Client
     */
    private $client;

    private $googleClientId;
    private $googleClientSecret;

    public function __construct(Router $router, $googleClientId, $googleClientSecret)
    {
        $this->router = $router;
        $this->googleClientId = $googleClientId;
        $this->googleClientSecret = $googleClientSecret;
    }

    /**
     * @return string
     * @param mixed $route
     */
    public function getAuthUrl($route = null)
    {
        return $this->getClient($route)->createAuthUrl();
    }

    /**
     * @return \Google_Client
     */
    public function getClient($route = null)
    {
        if ( !$this->client ) {
            $this->client = new \Google_Client();
            $this->client->setClientId($this->googleClientId);
            $this->client->setClientSecret($this->googleClientSecret);
            $this->client->setAccessType('offline');
            $this->client->setApprovalPrompt('force');
            $this->client->addScope(['email', 'profile', \Google_Service_Calendar::CALENDAR] // TODO: collect scopes from bundles
            );
            $this->client->setIncludeGrantedScopes(true);
            $this->client->setRedirectUri($this->router->generate($route ?: 'core_app_google_auth', [], UrlGenerator::ABSOLUTE_URL));
        }

        return $this->client;
    }
}