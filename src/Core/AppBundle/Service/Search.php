<?php
/**
 * Created by PhpStorm.
 * User: mkt
 * Date: 02.01.17
 * Time: 01:32
 * TODO: Configure SearchItems with annotations.
 */

namespace Core\AppBundle\Service;


use Core\AppBundle\Search\SearchItem;
use Core\AppBundle\Search\SearchItemInterface;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

/**
 * Search service
 *
 * Service to get search hits and autocomplete suggestions from the database.
 * Container service id: app.search
 *
 * Author: joerg.sommer@crea.de
 *
 */
class Search
{
    /**
     * Doctrines entity manager
     * @var EntityManager
     */
    private $em;

    /**
     * Application helper service "app.helper"
     * @var AppHelper
     */
    private $appHelper;

    /**
     * List of all search item services tagged by "app.search_item". Filled by SearchItemPass.
     * See Core\AppBundle\DependencyInjection\SearchItemPass
     * @var SearchItemInterface[]
     */
    private $searchItems = [];

    /**
     * Settings service "app.setting".
     * @var Settings
     */
    private $settings;

    /**
     * Authorization checker service "security.authorization_checker".
     * @var AuthorizationChecker
     */
    private $authorizationChecker;

    /**
     * Search constructor.
     * @param EntityManager $em Doctrines entity manager
     * @param AppHelper $appHelper Service AppHelper
     * @param Settings $settings Service Settings
     * @param AuthorizationChecker $authorizationChecker Service AuthorizationChecker
     */
    public function __construct(EntityManager $em, AppHelper $appHelper, Settings $settings, AuthorizationChecker $authorizationChecker)
    {
        $this->em = $em;
        $this->appHelper = $appHelper;
        $this->settings = $settings;
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * Returns found search hits as HTML partials filtered by the filter settings and the permissions of the logged in user
     * @param string $query Key words in the search input field
     * @return array List of HTML partial strings
     */
    public function getResults($query)
    {
        $results = [];

        // get filter settings as an array
        $filter = $this->getFilter();

        if ( $query ) {
            foreach ( $this->getSearchItems() as $item ) {
                $searchItemName = $this->getSearchItemName($item);
                if ( !$filter || (array_key_exists($searchItemName, $filter) && $filter[ $searchItemName ]) ) {
                    $results = array_merge($results, $item->getResults($query));
                }
            }
        }

        return $results;
    }

    /**
     * Returns the stored filter settings
     * If an user is logged in the user filter settings are returned, else default settings
     * @return array Associative array with filter settings
     */
    public function getFilter()
    {
        return $this->settings->get('core_app_search_filter', []);
    }

    /**
     * Returns all search item services which are allowed for the current logged in user
     * @return SearchItemInterface[] List of search item services
     */
    public function getSearchItems()
    {
        $searchItems = [];

        // check permissions
        foreach ( $this->searchItems as $searchItem ) {
            if ( $perms = $searchItem->getRequiredPermissions() ) {
                foreach ( $perms as $attr => $class ) {
                    if ( !$this->authorizationChecker->isGranted($attr, $class) ) {
                        continue 2;
                    }
                }
            }

            $searchItems[ $this->getSearchItemName($searchItem) ] = $searchItem;
        }

        return $searchItems;
    }

    /**
     * Generates a name from the namespace of a search item
     * @param SearchItemInterface $item Search item a name has to be generated for
     * @return string Generated name
     */
    public function getSearchItemName(SearchItemInterface $item)
    {
        $reflection = new \ReflectionObject($item);
        $name = strtolower(str_replace('\\', '_', $reflection->name));
        $name = str_replace('bundle', '', $name);

        return $name;
    }

    /**
     * Returns found autocomplete suggestions as array filtered by the filter settings and the permissions of the
     * logged in user
     * @param string $query Key words in the search input field
     * @return array List of autocomplete suggestions as associative array
     */
    public function getAutocompleteResults($query)
    {
        $results = [];

        // get filter settings as an array
        $filter = $this->getFilter();

        if ( $query ) {
            foreach ( $this->getSearchItems() as $item ) {
                $searchItemName = $this->getSearchItemName($item);
                if ( !$filter || (array_key_exists($searchItemName, $filter) && $filter[ $searchItemName ]) ) {
                    $results = array_merge($results, $item->getAutocompleteResults($query));
                }
            }
        }

        return $results;
    }

    /**
     * Adds a search item with the service tag "app.search_item".
     * See Core\AppBundle\DependencyInjection\SearchItemPass
     * @param SearchItemInterface $item Search item service tagged with "app.search_item"
     */
    public function addSearchItem(SearchItemInterface $item)
    {
        $this->searchItems[ $this->getSearchItemName($item) ] = $item;
    }

    /**
     * Returns the current filter settings
     * FIXME Ein Setter, der returniert? getFilterSettings ist besser! Nur in SearchController verwendet!
     * @param Request $request Symfonies request object
     * @return array List of searchable items and their state
     */
    public function setFilter(Request $request)
    {
        // set options with defaults
        $resolver = new OptionsResolver();
        $resolver->setDefaults($this->getFilterDefaults());

        // get settings from the form
        if ( $request->getMethod() == 'POST' ) {
            $form = $request->request->get('core_app_search_filter', []);
            $filter = [];

            // convert the form filter settings
            foreach ( $this->getFilterDefaults() as $key => $default ) {
                if ( array_key_exists($key, $form) ) {
                    $filter[ $key ] = 1;
                } else {
                    $filter[ $key ] = 0;
                }
            }

            // store the form filter settings in the database
            $this->settings->set('core_app_search_filter', $filter);
        }

        // merge the stored settings with the default settings and return them
        return $resolver->resolve((array)$this->settings->get('core_app_search_filter', []));
    }

    /**
     * Returns default filter settings with all searchable items in active state
     * @return array List of all searchable items
     */
    private function getFilterDefaults()
    {
        $defaults = [];
        /** @var SearchItem $searchItem */
        foreach ( $this->getSearchItems() as $searchItem ) {
            $defaults[ $this->getSearchItemName($searchItem) ] = $searchItem->active;
        }

        return $defaults;
    }

    /**
     * Stores filter settings with the given search items in active state in the database if the filter settings are not
     * locked by the user
     * @param array $overrides List of search item names which have to set active in the filter settings
     */
    public function overrideFilter($overrides)
    {
        if ( !$this->settings->get('core_app_search_lock', false) ) {
            $filter = [];
            foreach ( $this->getFilterDefaults() as $key => $default ) {
                $filter[ $key ] = in_array($key, $overrides);
            }

            $this->settings->set('core_app_search_filter', $filter);
        }
    }
}