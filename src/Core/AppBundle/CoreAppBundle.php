<?php

namespace Core\AppBundle;


use Core\AppBundle\DependencyInjection\SearchItemPass;
use Core\YonoBundleInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class CoreAppBundle extends Bundle implements YonoBundleInterface
{
    /**
     * @param ContainerBuilder $container
     */
    public function build(ContainerBuilder $container)
    {
        $container->addCompilerPass(new SearchItemPass());
    }

    /**
     * @return string
     */
    public function getDisplayName()
    {
        return 'Yono Core';
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return 'Yono Kernkomponenten';
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return '';
    }
}