<?php
/**
 * Created by PhpStorm.
 * User: Nico
 * Date: 30.05.2017
 * Time: 10:30
 */

namespace Core\AppBundle\Command;


use Core\AppBundle\Entity\MenuItem;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\Yaml\Yaml;

PHP_OS == "Windows" || PHP_OS == "WINNT" ? define("SEPARATOR", "\\") : define("SEPARATOR", "/");

class DataFixtureCommand extends ContainerAwareCommand
{
    /**
     * @var QuestionHelper $helper
     */
    private $helper;

    public function configure()
    {
        $this->setName('yono:datafixtures')->setDescription('This command is useful! USE IT!');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $this->helper = $this->getHelper('question');

        $output->writeln([".-.  .-..----. .-. .-. .----. ",
                          " \ \/ //  {}  \|  `| |/  {}  \ ",
                          "  }  { \      /| |\  |\      /",
                          "  `--'  `----' `-' `-' `----' ",
                          "",
                          ""]);

        $choice = $this->menu($input, $output);

        switch ($choice) {
            case 1:
                $menuAction = $this->pageMenu($input, $output);
                switch ($menuAction) {
                    case 1:
                        $this->deleteAllMenuItems($input, $output);
                        break;
                    case 2:
                        $this->createAllMenuItems($input, $output);
                        break;
                    case 3:
                        $this->updateAllMenuItems($input, $output);
                        break;
                    case 4:
                        $this->createCustomMenuItem($input, $output);
                        break;
                    default:
                        $output->writeln("ByeBye");

                        return;
                }
                break;
            case 2:
                $output->writeln("Das geht aktuell noch nicht!");
                break;
            case 3:
                $randomPassword = $this->randomPassword(8);

                $output->writeln(["Es wurde ein zufälliges Passwort generiert:", $randomPassword]);

                $useItQuestion = new Question("Möchten Sie dieses verwenden? (J/N): ", "J", '/^(j|n)/i');

                $useIt = $this->helper->ask($input, $output, $useItQuestion);

                if (strtolower($useIt) == "n") {
                    $passwordQuestion = new Question("Geben Sie ein neues Passwort für den SuperAdmin ein: ", $randomPassword);
                    do {
                        $password = $this->helper->ask($input, $output, $passwordQuestion);
                        if (strlen($password) < 8) {
                            $output->writeln("Das Passwort muss aus mindestens 8 Zeichen bestehen!");
                        }
                    } while (strlen($passwordQuestion) < 8);
                } else {
                    $password = $randomPassword;
                }

                $userManager = $this->getContainer()->get('fos_user.user_manager');

                $userAdmin = $this->getContainer()->get('doctrine')->getManager()->getRepository('CoreAppBundle:User')->findOneBy(['username' => 'superadmin']);

                $userAdmin->setPlainPassword($password);

                $userManager->updateUser($userAdmin, true);

                $output->writeln("Das Passwort für den SuperAdmin wurde erfolgreich geändert!");

                break;
            case 4:
                $output->writeln("ByeBye");
                break;

        }

    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|mixed|Question
     */
    private function menu(InputInterface $input, OutputInterface $output)
    {
        $output->writeln(['+++ Menü +++', '1. Seitenmenü', '2. Demo Daten', '3. Super Admin Verwaltung', '4. Beenden']);

        $decision = new Question('Bitte wählen Sie die Option mit der Sie fortfahren möchten: ', '4');

        $decision = $this->helper->ask($input, $output, $decision);

        $decision = intval($decision);

        if ($decision < 1 || $decision > 4) {
            $decision = 4;
        }

        return $decision;


    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|mixed|Question
     */
    private function pageMenu(InputInterface $input, OutputInterface $output)
    {
        $output->writeln(["",
            "",
            "+++ Seiten Menü +++",
            "1. Alle Menüpunkte löschen",
            "2. Alle Menüpunkte anlegen",
            "3. Alle Menüpunkte updaten",
            "4. Menüpunkt manuell hinzufügen",
            "5. Beenden"]);

        $decision = new Question("Was möchten Sie tun?: ", "5");

        $decision = $this->helper->ask($input, $output, $decision);

        $decision = intval($decision);

        if ($decision < 1 || $decision > 5) {
            $decision = 5;
        }

        return $decision;
    }

    /**
     * Deletes all menu items by dropping the menu item table and recreating it
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    private function deleteAllMenuItems(InputInterface $input, OutputInterface $output)
    {
        $decision = new Question("Sind Sie sicher das sie alle Menüpunkte löschen möchten? (J/N): ", "N", '/^(j|n)/i');

        $decision = $this->helper->ask($input, $output, $decision);

        if (strtolower($decision) == 'j') {

            $em = $this->getContainer()->get('doctrine')->getManager();
            $metadata = $em->getClassMetadata(MenuItem::class);

            $schemaTool = new \Doctrine\ORM\Tools\SchemaTool($em);
            $schemaTool->dropSchema(array($metadata));
            $schemaTool->createSchema(array($metadata));
            $output->writeln(["", "", "", "Es wurden alle Menüpunkte gelöscht!", ""]);
        } else {
            $output->writeln("Es wurden keine Änderungen an der Datenbank vorgenommen.");
        }
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    private function createAllMenuItems(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        if (count($em->getRepository('Core\AppBundle\Entity\MenuItem')->findAll()) > 0) {
            $output->writeln("Es sind bereits Menüpunkte angelegt. Bitte alle Menüpunkte löschen oder die Update Methode verwenden.");

            return;
        }

        $question = new Question("Welche Umgebung soll geladen werden? (dev/prod): ", "dev");

        $env = $this->helper->ask($input, $output, $question);

        $fs = new Filesystem();
        $fi = new Finder();

        $corePath = realpath(__DIR__ . SEPARATOR . '..' . SEPARATOR . '..' . SEPARATOR . '..' . SEPARATOR . 'Core' . SEPARATOR);
        $dataFixturesPath = SEPARATOR . "AppBundle" . SEPARATOR . 'DataFixtures' . SEPARATOR . 'ORM' . SEPARATOR . $env . SEPARATOR . 'MenuItem.yml';

        $menuItemsYml = realpath($corePath . $dataFixturesPath);

        $coreContent = Yaml::parse(file_get_contents($menuItemsYml));

        foreach ($coreContent['Core\AppBundle\Entity\MenuItem'] as $name => $item) {

            $menuItem = new MenuItem();

            $menuItem->setIdentifier($name);

            if (isset($item['icon'])) {
                $menuItem->setIcon($item['icon']);
            }
            $menuItem->setLabel($item['label']);

            if (strpos($item['label'], 'divider')) {
                $menuItem->setDivider(true);
            }

            if ($item['label'] == 'header') {
                $menuItem->setHeader(true);
            }

            $menuItem->setOrder($item['order']);

            if (isset($item['uri'])) {
                $menuItem->setUri($item['uri']);
            }
            if (isset($item['route'])) {
                $menuItem->setRoute($item['route']);
            }

            if (isset($item['permissionsNeeded'])) {
                $menuItem->setPermissionsNeeded($item['permissionsNeeded']);
            }


            $menuItem->setMenu($item['menu']);
            $menuItems[$name] = $menuItem;

            if (isset($item['parent'])) {
                $menuItem->setParent($menuItems[str_replace('@', '', $item['parent'])]);
            }

            $em->persist($menuItem);
            $em->flush();

            $output->writeln("Menüpunkt " . $menuItem->getLabel() . ' erstellt.');

        }

        $modulesPath = realpath(__DIR__ . SEPARATOR . '..' . SEPARATOR . '..' . SEPARATOR . '..' . SEPARATOR . 'Modules' . SEPARATOR);

        $modules = $fi->directories()->depth(0)->in($modulesPath);

        /**
         * @var SplFileInfo $module
         */
        foreach ($modules as $module) {
            $name = $module->getBasename();
            $dataFixturesPath = SEPARATOR . $name . SEPARATOR . 'DataFixtures' . SEPARATOR . 'ORM' . SEPARATOR . $env . SEPARATOR . 'MenuItem.yml';

            $menuItemsYml = realpath($modulesPath . $dataFixturesPath);

            $menuItems = [];

            if ($fs->exists($menuItemsYml)) {
                $content = Yaml::parse(file_get_contents($menuItemsYml));
                foreach ($content['Core\AppBundle\Entity\MenuItem'] as $name => $item) {

                    $menuItem = new MenuItem();

                    $menuItem->setIdentifier($name);

                    if (isset($item['icon'])) {
                        $menuItem->setIcon($item['icon']);
                    }
                    $menuItem->setLabel($item['label']);

                    if ($item['label'] == 'divider') {
                        $menuItem->setDivider(true);
                    }

                    if ($item['label'] == 'header') {
                        $menuItem->setHeader(true);
                    }

                    $menuItem->setOrder($item['order']);

                    if (isset($item['uri'])) {
                        $menuItem->setUri($item['uri']);
                    }
                    if (isset($item['route'])) {
                        $menuItem->setRoute($item['route']);
                    }

                    if (isset($item['permissionsNeeded'])) {
                        $menuItem->setPermissionsNeeded($item['permissionsNeeded']);
                    }


                    $menuItem->setMenu($item['menu']);
                    $menuItems[$name] = $menuItem;

                    if (isset($item['parent'])) {
                        if (isset($menuItems[str_replace('@', '', $item['parent'])])) {
                            $menuItem->setParent($menuItems[str_replace('@', '', $item['parent'])]);
                        } else {
                            $parent = $em->getRepository('CoreAppBundle:MenuItem')->findOneBy(['identifier' => str_replace('@', '', $item['parent'])]);
                            $menuItem->setParent($parent);
                        }
                    }

                    $em->persist($menuItem);
                    $em->flush();

                    $output->writeln("Menüpunkt " . $menuItem->getLabel() . ' erstellt.');

                }
            } else {
                $output->writeln("Keine Menüpunkte für Bundle " . $name);
            }

        }


    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    private function updateAllMenuItems(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $repo = $em->getRepository('Core\AppBundle\Entity\MenuItem');

        $question = new Question("Welche Umgebung soll geladen werden? (dev/prod): ", "dev");

        $env = $this->helper->ask($input, $output, $question);

        $fs = new Filesystem();
        $fi = new Finder();

        $modulesPath = realpath(__DIR__ . SEPARATOR . '..' . SEPARATOR . '..' . SEPARATOR . '..' . SEPARATOR . 'Modules' . SEPARATOR);

        $modules = $fi->directories()->depth(0)->in($modulesPath);

        /**
         * @var SplFileInfo $module
         */
        foreach ($modules as $module) {
            $name = $module->getBasename();
            $dataFixturesPath = SEPARATOR . $name . SEPARATOR . 'DataFixtures' . SEPARATOR . 'ORM' . SEPARATOR . $env . SEPARATOR . 'MenuItem.yml';

            $menuItemsYml = realpath($modulesPath . $dataFixturesPath);

            $menuItems = [];

            if ($fs->exists($menuItemsYml)) {
                $content = Yaml::parse(file_get_contents($menuItemsYml));
                foreach ($content['Core\AppBundle\Entity\MenuItem'] as $name => $item) {
                    $existingMenuItems = $repo->findBy(['identifier' => $name]);
                    if (count($existingMenuItems) > 1) {

                        $output->writeln(["Es wurden mehrere Möglichkeiten gefunden einen Menüpunk zu aktualisieren.",
                            "",
                            "Name: " . $item['label']]);

                        if (isset($item['uri'])) {
                            $output->writeln("URI: " . $item['uri']);
                        }
                        if (isset($item['route'])) {
                            $output->writeln("Route: " . $item['route']);
                        }

                        $output->writeln(["",
                            "Bitte wählen Sie den Menüpunkt aus den Sie mit den oben genannten Daten aktualisieren möchten."]);
                        $counter = 1;
                        foreach ($existingMenuItems as $menuItem) {
                            $output->writeln(["",
                                $counter . ". " . $menuItem->getLabel(),
                                "URI: " . $menuItem->getUri(),
                                "Route: " . $menuItem->getRoute()]);
                            $counter++;
                        }

                        $output->writeln(["", $counter . ". Überspringen", intval($counter + 1) . ". Abbrechen"]);

                        $question = new Question("Welchen Menüpunkt möchten Sie aktualisieren: ", "-1");

                        $response = $this->helper->ask($input, $output, $question);

                        //check ob abbrechen
                        $total = count($existingMenuItems);
                        if ($response >= $total + 2 || $response < 1) {
                            $output->writeln("Das Updaten der Menüpunkte wurde abgebrochen!");

                            return;
                        }
                        if ($response == $total + 1) {
                            continue;
                        }

                        /**
                         * @var MenuItem $menuItem
                         */
                        $menuItem = $existingMenuItems[$response - 1];

                        if (isset($item['icon'])) {
                            $menuItem->setIcon($item['icon']);
                        }
                        $menuItem->setLabel($item['label']);

                        if ($item['label'] == 'divider') {
                            $menuItem->setDivider(true);
                        }

                        if ($item['label'] == 'header') {
                            $menuItem->setHeader(true);
                        }

                        $menuItem->setOrder($item['order']);

                        if (isset($item['uri'])) {
                            $menuItem->setUri($item['uri']);
                        }
                        if (isset($item['route'])) {
                            $menuItem->setRoute($item['route']);
                        }

                        if (isset($item['permissionsNeeded'])) {
                            $menuItem->setPermissionsNeeded($item['permissionsNeeded']);
                        }


                        $menuItem->setMenu($item['menu']);

                        if (isset($item['parent'])) {

                            $parent = $repo->findOneBy(['label' => $content['Core\AppBundle\Entity\MenuItem'][str_replace('@', '', $item['parent'])]['label'],
                                'order' => $content['Core\AppBundle\Entity\MenuItem'][str_replace('@', '', $item['parent'])]['order']]);

                            if (!$parent) {
                                $output->writeln("Elternmenüpunkt wurde nicht gefunden!");
                            } else {
                                $menuItem->setParent($parent);
                            }

                        }
                        $em->flush();

                        $output->writeln("Menüpunkt " . $menuItem->getIdentifier() . " wurde aktualisiert!");


                    } elseif (count($existingMenuItems) < 1) {

                        $menuItem = new MenuItem();

                        $menuItem->setIdentifier($name);

                        if (isset($item['icon'])) {
                            $menuItem->setIcon($item['icon']);
                        }
                        $menuItem->setLabel($item['label']);

                        if ($item['label'] == 'divider') {
                            $menuItem->setDivider(true);
                        }

                        if ($item['label'] == 'header') {
                            $menuItem->setHeader(true);
                        }

                        $menuItem->setOrder($item['order']);

                        if (isset($item['uri'])) {
                            $menuItem->setUri($item['uri']);
                        }
                        if (isset($item['route'])) {
                            $menuItem->setRoute($item['route']);
                        }

                        if (isset($item['permissionsNeeded'])) {
                            $menuItem->setPermissionsNeeded($item['permissionsNeeded']);
                        }


                        $menuItem->setMenu($item['menu']);

                        if (isset($item['parent'])) {

                            $parent = $repo->findOneBy(['label' => $content['Core\AppBundle\Entity\MenuItem'][str_replace('@', '', $item['parent'])]['label'],
                                'order' => $content['Core\AppBundle\Entity\MenuItem'][str_replace('@', '', $item['parent'])]['order']]);

                            if (!$parent) {
                                $output->writeln("Elternmenüpunkt wurde nicht gefunden!");
                            } else {
                                $menuItem->setParent($parent);
                            }

                        }

                        $em->persist($menuItem);
                        $em->flush();

                        $output->writeln("Menüpunkt " . $menuItem->getIdentifier() . " wurde aktualisiert!");


                    } else {

                        /**
                         * @var MenuItem $menuItem
                         */
                        $menuItem = $existingMenuItems[0];

                        if (isset($item['icon'])) {
                            $menuItem->setIcon($item['icon']);
                        }
                        $menuItem->setLabel($item['label']);

                        if ($item['label'] == 'divider') {
                            $menuItem->setDivider(true);
                        }

                        if ($item['label'] == 'header') {
                            $menuItem->setHeader(true);
                        }

                        $menuItem->setOrder($item['order']);

                        if (isset($item['uri'])) {
                            $menuItem->setUri($item['uri']);
                        }
                        if (isset($item['route'])) {
                            $menuItem->setRoute($item['route']);
                        }

                        if (isset($item['permissionsNeeded'])) {
                            $menuItem->setPermissionsNeeded($item['permissionsNeeded']);
                        }


                        $menuItem->setMenu($item['menu']);

                        if (isset($item['parent'])) {

                            $parent = $repo->findOneBy(['label' => $content['Core\AppBundle\Entity\MenuItem'][str_replace('@', '', $item['parent'])]['label'],
                                'order' => $content['Core\AppBundle\Entity\MenuItem'][str_replace('@', '', $item['parent'])]['order']]);

                            if (!$parent) {
                                $output->writeln("Elternmenüpunkt wurde nicht gefunden!");
                            } else {
                                $menuItem->setParent($parent);
                            }

                        }

                        $em->flush();

                        $output->writeln("Menüpunkt " . $menuItem->getIdentifier() . " wurde aktualisiert!");
                    }
                }
            }
        }
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    private function createCustomMenuItem(InputInterface $input, OutputInterface $output)
    {
        $labelQuestion = new Question("Geben Sie eine Bezeichnung für den Menüpunkt ein: ", "");
        do {
            $label = $this->helper->ask($input, $output, $labelQuestion);
        } while ($label == "");

        $iconQuestion = new Question("Geben Sie den Namen für ein Icon an. (Leer lassen wenn kein Icon verwendet werden soll): ", "");
        $icon = $this->helper->ask($input, $output, $labelQuestion);

        $targetQuestion = new Question("Geben Sie den Link an auf den der Menüpunkt zeigen soll: ", "");
        do {
            $target = $this->helper->ask($input, $output, $targetQuestion);
        } while ($target == "");

        $menuItem = new MenuItem();

        if ($icon != "") {
            $menuItem->setIcon($icon);
        }

        $menuItem->setMenu("main");
        $menuItem->setOrder(95000);
        $menuItem->setPermissionsNeeded([]);
        $menuItem->setIdentifier('custom_' . time());
        $menuItem->setLabel($label);
        $menuItem->setUri($target);

        $em = $this->getContainer()->get("doctrine")->getManager();

        $em->persist($menuItem);
        $em->flush();

        $output->writeln("Der Menüpunkt " . $label . " wurde erstellt.");
    }

    private function randomPassword($length)
    {

        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

        return substr(str_shuffle($chars), 0, $length);

    }

}

