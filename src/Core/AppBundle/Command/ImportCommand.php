<?php

namespace Core\AppBundle\Command;

use Modules\ContactBundle\Entity\Address;
use Modules\ContactBundle\Entity\Category;
use Modules\ContactBundle\Entity\Contact;
use Modules\ContactBundle\Entity\Field;
use Modules\ContactBundle\Entity\FieldValue;
use Modules\ContactBundle\Entity\Person;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\File\File;

/**
 * Created by PhpStorm.
 * User: nicow
 * Date: 10.05.2017
 * Time: 17:11
 */
class ImportCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('yono:km:contactimport')
             ->setDescription('KM Haustechnkik Contact import')
             ->setHelp("Import contacts")
             ->addArgument('file',
                           InputArgument::REQUIRED,
                           'The CSV file with Contacts');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // welcome
        $output->writeln(['<info>',
            '',
            '__  ____   ____    ____',
            '_  ||_  _| |_   \  /  _|',
            '| |_/ /     |   \/   |',
            '|  __\'.     | |\  /| |',
            '| |__\ \_  _| |_\/_| |_',
            '|____||___||____||____| ',

            '',
            '############# Importer #############',
            '</info>']);


        $em = $this->getContainer()->get("doctrine")->getManager();
        $fieldRepository = $em->getRepository('Modules\ContactBundle\Entity\Field');
        $contactCategoryRepository = $em->getRepository('Modules\ContactBundle\Entity\Category');
        $contactRepository = $em->getRepository('Modules\ContactBundle\Entity\Contact');

        $customFieldName = "Alte Kundennummer";
        $categoryName = "Importierte Kontakte RRDT";

        $file = new File($input->getArgument('file'));

        $fileContent = file_get_contents($file->getRealPath());

        $csvLines = explode(PHP_EOL, $fileContent);

        $fields = $csvLines[0];

        unset($csvLines[0]);

        $csvLines = array_values($csvLines);

        $category = $contactCategoryRepository->findOneBy(["name" => $categoryName]);

        if (!$category) {
            $category = new Category();
            $category->setName($categoryName);
            $category->setIcon(null);
            $category->setShowInMenu(true);

            $em->persist($category);
            $em->flush();
        }

        $field = $fieldRepository->findOneBy(["name" => $customFieldName]);

        if (!$field) {
            $field = new Field();
            $field->setDefaultValue("/");
            $field->setForm("contact");
            $field->setName($customFieldName);
            $field->setType("text");
            $field->setRequired(false);

            $em->persist($field);
            $em->flush();
        }


        foreach ($csvLines as $csvLine) {
            $line = $this->getArrayFromCSV($fields, $csvLine);

            $rawContact = new ArrayGet($line);

            $name = $rawContact->getGFirma();
            if ($name == "") {
                $name = $rawContact->getVorname() . ' ' . $rawContact->getNachname();
                if (str_replace(" ", "", $name) == "") {
                    continue;
                }
            }

            $contact = $contactRepository->findOneBy(['name' => $name]);

            if ($contact) {
                continue;
            }

            $contact = new Contact();
            $contact->setName($name);
            $contact->addCategory($category);

            $em->persist($contact);
            $em->flush();

            $customField = new FieldValue();
            $customField->setField($field);
            $customField->setValueText($rawContact->getId($field->getDefaultValue()));
            $customField->setEntityClass('Modules\ContactBundle\Entity\Contact');
            $customField->setEntityId($contact->getId());

            $em->persist($customField);


            $address = new Address();

            $address->setName("Geschäftlich");
            $address->setStreet($rawContact->getGStrasse());
            $address->setCity($rawContact->getGOrt());
            if (is_numeric($rawContact->getGPlz())) $address->setZip($rawContact->getGPlz());
            $address->setCountry($rawContact->getGLand());

            $em->persist($address);
            $em->flush();
            $contact->addAddress($address);

            if ($rawContact->getPStrasse() != "") {
                $address2 = new Address();

                $address2->setName("Privat");
                $address2->setStreet($rawContact->getPStrasse());
                $address2->setCity($rawContact->getPOrt());
                $address2->setZip($rawContact->getPPlz());
                $address2->setCountry($rawContact->getPLand());

                $em->persist($address2);
                $em->flush();
                $contact->addAddress($address2);
            }

            $em->flush();

            $person = new Person();

            $person->setEmail($rawContact->getGEmail());
            $person->setFirstname($rawContact->getVorname());
            $person->setLastname(substr($rawContact->getNachname(), 0, 50));
            $person->setMobile($rawContact->getGHandy());
            $person->setPhone($rawContact->getGTelefon());
            $person->setSalutation(substr($rawContact->getTitel(), 0, 25));

            $em->persist($person);
            $em->flush();

            $contact->addPerson($person);
            $em->flush();

            $output->writeln("Created Contact: " . $contact->getName());

        }

        // finish
        $output->writeln(['', 'Finished! Import successful!', '']);
    }

    private function getArrayFromCSV($schema, $data)
    {
        $keys = explode(";", $schema);
        $values = explode(";", $data);

        if (count($keys) == count($values)) {
            return array_combine($keys, $values);
        } else {
            if (count($keys) > count($values)) {
                while (count($keys) > count($values)) {
                    $values[$this->getRandomArrayFillKeyValues()] = $this->getRandomArrayFillKeyValues();
                }
            } else {
                while (count($keys) < count($values)) {
                    $keys[$this->getRandomArrayFillKeyValues()] = $this->getRandomArrayFillKeyValues();
                }
            }

            return array_combine($keys, $values);
        }

    }

    private function getRandomArrayFillKeyValues($length = 5)
    {
        return substr(str_shuffle(str_repeat($x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length / strlen($x)))), 1, $length);
    }
}


class ArrayGet
{
    /**
     * @var array
     */
    private $datenArray;

    public function __construct(array $datenArray)
    {
        $this->datenArray = $datenArray;
    }

    public function __call($name, $arguments)
    {
        $name = strtolower($name);
        $name = str_replace('get', '', $name);

        if (isset($arguments[0])) {
            $default = $arguments[0];
        } else {
            $default = "";
        }

        foreach ($this->datenArray as $k => $v) {
            if (strtolower(preg_replace("/[^A-Za-z0-9 ]/", '', $k)) == $name) {
                return $v;
            }
        }

        return $default;
    }
}
