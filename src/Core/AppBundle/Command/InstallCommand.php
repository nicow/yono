<?php

namespace Core\AppBundle\Command;

use Core\AppBundle\Service\AppHelper;
use Core\OAuthBundle\Entity\Client;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Process\Process;

/**
 * Created by PhpStorm.
 * User: mkt
 * Date: 18.08.16
 * Time: 17:11
 */
class InstallCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('yono:install')
             ->setDescription('Yono Installer')
             ->setHelp("Install Yono")
             ->addOption('renew-assets',
                         null,
                         InputOption::VALUE_NONE,
                         'Delete bower assets and reinstall them?');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // welcome
        $output->writeln(['<info>',
                          '',
                          '  \\\\    / /',
                          '   \\\\  / /  ___     ___     ___',
                          '    \\\\/ / //   )) //   )) //   ))',
                            '     / / //   // //   // //   //',
                            '    / / ((___// //   // ((___//',
                          '',
                          '############# Installer #############',
                          '</info>']);

        // prepare
        $yonoBundles = $this->getContainer()->get('app.helper')->getBundles(AppHelper::BUNDLES_YONO);
        $rootDir = $this->getContainer()->getParameter('kernel.root_dir') . '/..';
        $fs = new Filesystem();

        // step 1: install themes bower assets
        $output->write('Install theme assets for theme (' . $this->getContainer()->getParameter('theme') . '): ');
        $themeAssetsDir = $rootDir . '/web/assets/' . $this->getContainer()->getParameter('theme');
        if ( $fs->exists($themeAssetsDir) ) {
            if ( $input->getOption('renew-assets') ) {
                $fs->remove($themeAssetsDir . '/vendor');
            }
            if ( $fs->exists($themeAssetsDir . '/vendor') ) {
                $output->writeln('<comment>(skipped, already exists)</comment> <error>[FAIL]</error>');
            } else {
                if ( $fs->exists($themeAssetsDir . '/bower.json') ) {
                    chdir($themeAssetsDir);
                    $process = new Process('bower install --allow-root');
                    $process->run();
                    if ( !$process->isSuccessful() ) {
                        throw new \RuntimeException('An error occurred when executing the following command: ' . $process->getCommandLine() . ' in InstallCommand (line: ' . __LINE__ . ')' . "\n" . $process->getErrorOutput());
                    }
                    chdir($rootDir);
                    $output->writeln('<info>[OK]</info>');
                } else {
                    $output->writeln('<comment>(skipped, no bower.json)</comment> <error>[FAIL]</error>');
                }
            }
        } else {
            throw new \RuntimeException('Theme assets directory (' . $themeAssetsDir . ') does not exist.');
        }

        // step 2: handle all core/module bundles
        $output->write('Installing bundles assets: ');
        foreach ( $yonoBundles as $bundle ) {
            $bundleClass = new \ReflectionClass($bundle);
            $bundleDir = dirname($bundleClass->getFileName());
            $assetsDir = $bundleDir . '/Resources/public';
            if ( $fs->exists($bundleDir) && $fs->exists($assetsDir . '/bower.json') ) {
                if ( $input->getOption('renew-assets') ) {
                    $fs->remove($assetsDir . '/vendor');
                }
                chdir($assetsDir);
                $process = new Process('bower install --allow-root');
                $process->run();
                if ( !$process->isSuccessful() ) {
                    throw new \RuntimeException('An error occurred when executing the following command: ' . $process->getCommandLine() . ' in InstallCommand (line: ' . __LINE__ . ')' . "\n" . $process->getErrorOutput());
                }
                chdir($rootDir);
            }
        }
        $output->writeln('<info>[OK]</info>');

        // step 3: assets:install
        $output->write('Creating symlinks to bundle assets: ');
        $process = new Process('php bin/console assets:install --symlink');
        $process->run();
        if ( !$process->isSuccessful() ) {
            throw new \RuntimeException($process->getErrorOutput());
        }
        $output->writeln('<info>[OK]</info>');

        // step 4: doctrine:schema:create
        $output->write('Creating database schema: ');
        if ( !$this->getContainer()->get('doctrine')->getConnection()->getSchemaManager()->listTables() ) {
            $process = new Process('php bin/console doctrine:schema:create');
            $process->run();
            if ( !$process->isSuccessful() ) {
                throw new \RuntimeException($process->getErrorOutput());
            }
            $output->writeln('<info>[OK]</info>');
        } else {
            $output->writeln('<comment>(skipped, already exists)</comment> <error>[FAIL]</error>');
        }

        // step 5: cache:clear
        $output->write('Clearing caches: ');
        $clearCacheDev = new Process('php bin/console cache:clear --no-warmup');
        $clearCacheDev->run();
        if ( !$clearCacheDev->isSuccessful() ) {
            throw new \RuntimeException($clearCacheDev->getErrorOutput());
        }

        $clearCacheProd = new Process('php bin/console cache:clear --no-warmup --env=prod');
        $clearCacheProd->run();
        if ( !$clearCacheProd->isSuccessful() ) {
            throw new \RuntimeException($clearCacheProd->getErrorOutput());
        }
        $output->writeln('<info>[OK]</info>');

        //setp 6: create default oatuh client
        $output->write('Create default OAuth Client: ');
        $em = $this->getContainer()->get('doctrine')->getManager();

        $existingClient = $em->getRepository('Core\OAuthBundle\Entity\Client')->findOneBy(['name' => '_self']);

        if ( !$existingClient ) {

            $oauthClient = new Client();
            $oauthClient->setName('_self');
            $oauthClient->setRandomId($this->generateRandomString(51));
            $oauthClient->setSecret($this->generateRandomString(51));
            $oauthClient->setRedirectUris([$this->getContainer()->getParameter('schema') . '://' . $this->getContainer()->getParameter('host')]);
            $oauthClient->setAllowedGrantTypes(['client_credentials']);


            $em->persist($oauthClient);
            $em->flush();

            $output->writeln('<info>[OK]</info>');

        } else {
            $output->writeln('<info>[FAIL/EXISTS]</info>');
        }

        //step 7 create superadmin
        $output->write("Create superadmin: ");

        $existingUser = $em->getRepository('Core\AppBundle\Entity\User')->findOneBy(['username' => 'superadmin']);

        if ( !$existingUser ) {

            $helper = $this->getContainer()->get('fos_user.user_manager');

            $admin = $helper->createUser();

            $admin->setUsername('superadmin');
            $admin->setEmail('superadmin@yono.local');
            $admin->setEnabled(true);
            $admin->addRole("ROLE_SUPER_ADMIN");
            $admin->setPlainPassword('superadmin');

            $helper->updateUser($admin);

            $output->writeln('<info>[OK]</info>');
        } else {
            $output->writeln('<info>[FAIL/EXISTS]</info>');
        }

        //step 8 Dump Assets
        $output->write("Dump assets: ");
        $assets = new Process('php bin/console assetic:dump');
        $assets->run();
        if ( !$assets->isSuccessful() ) {
            throw new \RuntimeException($assets->getErrorOutput());
        }
        $assets = new Process('php bin/console assets:install');
        $assets->run();
        if ( !$assets->isSuccessful() ) {
            throw new \RuntimeException($assets->getErrorOutput());
        }
        $output->writeln('<info>[OK]</info>');

        //step 9 update cronjobs
        $output->write("Update Cronjobs: ");
        $updateCronjobs = new Process("php bin/console cron:scan");

        $updateCronjobs->run();

        if ( !$updateCronjobs->isSuccessful() ) {
            throw new \RuntimeException($updateCronjobs->getErrorOutput());
        }
        $output->writeln('<info>[OK]</info>');



        // finish
        $output->writeln(['', 'Finished! Installation successful!', '']);
    }

    /**
     * Returns a random string of given length.
     *
     * FIXME Ich weiß nicht, was dieser Wert absichert, aber er ist nicht besonders random relative zur Länge!
     *
     * @param int $length Length of the random string
     * @return string Random string of given length
     */
    private function generateRandomString($length = 10)
    {
        return substr(
                   str_shuffle(
                       str_repeat($x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
                                  ceil($length / strlen($x))
                       )
                   ), 1, $length);
    }

}