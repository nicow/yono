<?php

namespace Core\AppBundle\Command;

use Modules\OrderManagementBundle\Entity\PriceList;
use Modules\OrderManagementBundle\Entity\PriceListProduct;
use Modules\OrderManagementBundle\Entity\PriceListService;
use Modules\OrderManagementBundle\Entity\Product;
use Modules\OrderManagementBundle\Entity\ProductCategory;
use Modules\OrderManagementBundle\Entity\Service;
use Modules\OrderManagementBundle\Entity\ServiceCategory;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\File\File;

/**
 * Created by PhpStorm.
 * User: nicow
 * Date: 10.05.2017
 * Time: 17:11
 */
class ImportPSCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('yono:km:psimport')->setDescription('KM Haustechnkik Product and Servie import')->setHelp("Import contacts")->addArgument('user', InputArgument::REQUIRED, 'User ID')->addArgument('list', InputArgument::REQUIRED, 'Name Pricelist')->addArgument('file', InputArgument::REQUIRED, 'The CSV file with Data');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // welcome
        $output->writeln(['<info>', '', '__  ____   ____    ____', '_  ||_  _| |_   \  /  _|', '| |_/ /     |   \/   |', '|  __\'.     | |\  /| |', '| |__\ \_  _| |_\/_| |_', '|____||___||____||____| ',

            '', '############# Importer #############', '</info>']);


        $em = $this->getContainer()->get("doctrine")->getManager();

        $productRepository = $em->getRepository('Modules\OrderManagementBundle\Entity\Product');
        $serviceRepository = $em->getRepository('Modules\OrderManagementBundle\Entity\Service');
        $priceListRepository = $em->getRepository('Modules\OrderManagementBundle\Entity\PriceList');
        $SCRepository = $em->getRepository('Modules\OrderManagementBundle\Entity\ServiceCategory');
        $PCRepository = $em->getRepository('Modules\OrderManagementBundle\Entity\ProductCategory');
        $userR = $em->getRepository('CoreAppBundle:User');

        $user = $userR->find($input->getArgument('user'));
        $priceList = $input->getArgument('list');

        $file = new File($input->getArgument('file'));

        $fileContent = file_get_contents($file->getRealPath());

        $csvLines = explode(PHP_EOL, $fileContent);

        $fields = $csvLines[ 0 ];

        unset($csvLines[ 0 ]);

        $csvLines = array_values($csvLines);

        $priceListNew = $priceListRepository->findOneBy(["title" => $priceList]);

        if ( !$priceListNew ) {
            $priceListNew = new PriceList();
            $priceListNew->setTitle($priceList);
            $priceListNew->setCreatedBy($user);
            $priceListNew->setUpdatedBy($user);
            $em->persist($priceListNew);
            $em->flush();
        }
        foreach ( $csvLines as $csvLine ) {
            $line = $this->getArrayFromCSV($fields, $csvLine);

            $rawContact = new ArrayGetData($line);

            $typ = $rawContact->getTyp();
            $name = $rawContact->getName();
            $price = str_replace('.', '', $rawContact->getPreis());
            $price = str_replace(',', '.', $price);
            $price = str_replace(' ', '', $price);

            if ( $typ == "Leistung" ) {
                $service = $serviceRepository->findOneBy(["title" => $name]);

                $category = $SCRepository->findOneBy(["name" => $rawContact->getKategorie()]);

                if ( !$category ) {
                    $category = new ServiceCategory();
                    $category->setName($rawContact->getKategorie());
                    $em->persist($category);
                    $em->flush();
                }
                if ( !$service ) {
                    $service = new Service();
                    $service->setTitle($name);
                    $service->setPrice($price);
                    $service->setTax(floatval($rawContact->getMwst()));
                    $service->setPriceType($this->getPriceSType($rawContact->getPreisart()));
                    $service->setCategories([$category]);
                    $service->setCreatedBy($user);
                    $service->setUpdatedBy($user);
                    $em->persist($service);
                    $em->flush();
                }

                $pSerice = new PriceListService();
                $pSerice->setPrice($price);
                $pSerice->setPricelist($priceListNew);
                $pSerice->setService($service);
                $pSerice->setCreatedBy($user);
                $pSerice->setUpdatedBy($user);
                $em->persist($pSerice);
                $em->flush();

            }
            if ( $typ == "Produkt" ) {
                $product = $productRepository->findOneBy(["title" => $name]);

                $category = $PCRepository->findOneBy(["name" => $rawContact->getKategorie()]);

                if ( !$category ) {
                    $category = new ProductCategory();
                    $category->setName($rawContact->getKategorie());
                    $category->setCreatedBy($user);
                    $category->setUpdatedBy($user);
                    $em->persist($category);

                    $em->flush();
                }
                if ( !$product ) {
                    $product = new Product();
                    $product->setTitle($name);
                    $product->setPrice($price);
                    $product->setTax(floatval($rawContact->getMwst()));
                    $product->setPriceType($this->getPricePType($rawContact->getPreisart()));
                    $product->setCategories([$category]);
                    $product->setCreatedBy($user);
                    $product->setUpdatedBy($user);
                    $em->persist($product);
                    $em->flush();
                }

                $pProduct = new PriceListProduct();
                $pProduct->setPrice($price);
                $pProduct->setPricelist($priceListNew);
                $pProduct->setProduct($product);
                $pProduct->setCreatedBy($user);
                $pProduct->setUpdatedBy($user);
                $em->persist($pProduct);
                $em->flush();

            }


            $output->writeln("Created Contact: " . $name);

        }

        // finish
        $output->writeln(['', 'Finished! Import successful!', '']);
    }

    private function getArrayFromCSV($schema, $data)
    {
        $keys = explode(";", $schema);
        $values = explode(";", $data);

        if ( count($keys) == count($values) ) {
            return array_combine($keys, $values);
        } else {
            if ( count($keys) > count($values) ) {
                while ( count($keys) > count($values) ) {
                    $values[ $this->getRandomArrayFillKeyValues() ] = $this->getRandomArrayFillKeyValues();
                }
            } else {
                while ( count($keys) < count($values) ) {
                    $keys[ $this->getRandomArrayFillKeyValues() ] = $this->getRandomArrayFillKeyValues();
                }
            }

            return array_combine($keys, $values);
        }

    }

    private function getRandomArrayFillKeyValues($length = 5)
    {
        return substr(str_shuffle(str_repeat($x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length / strlen($x)))), 1, $length);
    }

    private function getPriceSType($typ)
    {
        switch ( $typ ) {
            case 'm2':
                return 'square meters';
            case 'lfdm':
                return 'running meters';
            case 'psch':
            case 'pauschal':
                return 'flatrate';
            case 'Stck':
            case 'Stck.':
            case 'Stk.':
                return 'amount';
            case 'Std.':
                return 'hour';
            case 'kg':
                return 'kg';
            case 'ltr':
                return 'liter';
            case 'TO':
                return 'ton';
            case 'm':
                return 'meter';
        }

        return 1;
    }

    private function getPricePType($typ)
    {
        switch ( $typ ) {
            case 'm²':
                return 1;
            case 'lfdm':
                return 4;
            case 'psch':
            case 'pauschal':
                return 2;
            case 'Stck':
            case 'Stck.':
            case 'Stk.':
                return 0;
            case 'Std.':
                return 5;
            case 'kg':
                return 6;
            case 'ltr':
                return 7;
            case 'TO':
                return 8;
            case 'm':
                return 9;
        }

        return 0;
    }
}


class ArrayGetData
{
    /**
     * @var array
     */
    private $datenArray;

    public function __construct(array $datenArray)
    {
        $this->datenArray = $datenArray;
    }

    public function __call($name, $arguments)
    {
        $name = strtolower($name);
        $name = str_replace('get', '', $name);

        if ( isset($arguments[ 0 ]) ) {
            $default = $arguments[ 0 ];
        } else {
            $default = "";
        }

        foreach ( $this->datenArray as $k => $v ) {
            if ( strtolower(preg_replace("/[^A-Za-z0-9 ]/", '', $k)) == $name ) {
                return $v;
            }
        }

        return $default;
    }
}
