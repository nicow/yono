<?php

namespace Core\AppBundle\Command;

use Core\AppBundle\Service\HolidayHelper;
use Faker\Provider\DateTime;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Created by PhpStorm.
 * User: tim
 * Date: 22.06.17
 * Time: 16:04
 */
class DemoCommand extends ContainerAwareCommand
{
    public function configure()
    {
        $this->setName("app:test");
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $helper = $this->getContainer()->get('app.holidayhelper');

        $array = $helper->getAllHolidaysForMonthAndYear(2, 2016);

        dump($array);
    }
}