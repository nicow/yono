<?php

namespace Core\AppBundle\Command;

use Modules\OrderManagementBundle\Entity\Division;
use Modules\OrderManagementBundle\Entity\Document;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TestCommand extends ContainerAwareCommand
{
    public function configure()
    {
        $this->setName('app:test')
            ->setDescription('Test');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("You gonna make me wet! :D");
    }
}