<?php
/**
 * Created by PhpStorm.
 * User: mkt
 * Date: 23.08.16
 * Time: 15:24
 */

namespace Core\AppBundle\Event\Listener;


use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

interface LoginListenerInterface
{
    /**
     * Performs actions on login related to the user entity.
     * @param ContainerInterface $container
     * @param Request            $request
     * @param TokenInterface     $token
     * @return void
     */
    public function login(ContainerInterface $container, Request $request, TokenInterface $token);

    /**
     * Returns an integer value for the order in which the listeners are executed.
     * A higher priority results in earlier execution.
     * @return integer
     */
    public function getPriority();
}