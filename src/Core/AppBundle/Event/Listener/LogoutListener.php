<?php
/**
 * Created by PhpStorm.
 * User: mkt
 * Date: 23.08.16
 * Time: 14:33
 */

namespace Core\AppBundle\Event\Listener;


use Core\AppBundle\Service\AppHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Logout\LogoutHandlerInterface;

class LogoutListener implements LogoutHandlerInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    private $listeners = [];

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * This method is called by the LogoutListener when a user has requested
     * to be logged out. Usually, you would unset session variables, or remove
     * cookies, etc.
     * @param Request        $request
     * @param Response       $response
     * @param TokenInterface $token
     */
    public function logout(Request $request, Response $response, TokenInterface $token)
    {
        // collect listeners from all bundles
        $this->collectListeners();

        // sort listeners so that highest priority gets executed first
        $this->sortListeners();

        // execute listeners
        foreach ( $this->listeners as $priority => $listeners ) {
            /** @var LogoutListenerInterface $listener */
            foreach ( $listeners as $listener ) {
                $listener->logout($this->container, $request, $response, $token);
                $request->getSession()->invalidate();
            }
        }
    }

    /**
     * Collects all LoginListeners from all core/module Bundles.
     */
    private function collectListeners()
    {
        $bundles = $this->container->get('app.helper')->getBundles(AppHelper::BUNDLES_YONO);
        foreach ( $bundles as $bundle ) {
            $listener = get_class($bundle) . '\\Event\\Listener\\LogoutListener';
            if ( class_exists($listener) ) {
                $listenerClass = new \ReflectionClass($listener);
                if ( $listenerClass->implementsInterface('Core\\AppBundle\\Event\\Listener\\LogoutListenerInterface') ) {
                    /** @var LoginListenerInterface $listener */
                    $listenerInstance = $listenerClass->newInstance();
                    $this->listeners[ $listenerInstance->getPriority() ][] = $listenerInstance;
                }
            }
        }
    }

    /**
     * Sorts the listeners by there priority keys.
     */
    private function sortListeners()
    {
        ksort($this->listeners);
        $this->listeners = array_reverse($this->listeners);
    }
}