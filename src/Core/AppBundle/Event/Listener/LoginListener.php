<?php
/**
 * Created by PhpStorm.
 * User: mkt
 * Date: 23.08.16
 * Time: 14:33
 */

namespace Core\AppBundle\Event\Listener;


use Core\AppBundle\Entity\User;
use Core\AppBundle\Service\AppHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;

class LoginListener implements AuthenticationSuccessHandlerInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    private $listeners = [];

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * This is called when an interactive authentication attempt succeeds. This
     * is called by authentication listeners inheriting from
     * AbstractAuthenticationListener.
     * @param Request        $request
     * @param TokenInterface $token
     * @return Response never null
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        // collect listeners from all bundles
        $this->collectListeners();

        // sort listeners so that highest priority gets executed first
        $this->sortListeners();

        // execute listeners
        foreach ( $this->listeners as $priority => $listeners ) {
            /** @var LoginListenerInterface $listener */
            foreach ( $listeners as $listener ) {
                $listener->login($this->container, $request, $token);
            }
        }

        /**
         * @var User $user
         */
        $user=$this->container->get('security.token_storage')->getToken()->getUser();
        if ($this->container->getParameter('modules_timetrack_enabled')) {
            $timetrack = $this->container->get('mod.timetrack.helper');
            $timetrack->startTracking($user);
        }

        return new RedirectResponse($this->container->get('router')->generate('home'));
    }

    /**
     * Collects all LoginListeners from all core/module Bundles.
     */
    private function collectListeners()
    {
        $bundles = $this->container->get('app.helper')->getBundles(AppHelper::BUNDLES_YONO);
        foreach ( $bundles as $bundle ) {
            $listener = get_class($bundle) . '\\Event\\Listener\\LoginListener';
            if ( class_exists($listener) ) {
                $listenerClass = new \ReflectionClass($listener);
                if ( $listenerClass->implementsInterface('Core\\AppBundle\\Event\\Listener\\LoginListenerInterface') ) {
                    /** @var LoginListenerInterface $listener */
                    $listenerInstance = $listenerClass->newInstance();
                    $this->listeners[ $listenerInstance->getPriority() ][] = $listenerInstance;
                }
            }
        }
    }

    /**
     * Sorts the listeners by there priority keys.
     */
    private function sortListeners()
    {
        ksort($this->listeners);
        $this->listeners = array_reverse($this->listeners);
    }
}