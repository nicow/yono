<?php
/**
 * Created by PhpStorm.
 * User: mkt
 * Date: 23.08.16
 * Time: 15:24
 */

namespace Core\AppBundle\Event\Listener;


use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

interface LogoutListenerInterface
{
    /**
     * Performs actions right before logout related to the user entity.
     * @param ContainerInterface $container
     * @param Request            $request
     * @param Response           $response
     * @param TokenInterface     $token
     */
    public function logout(ContainerInterface $container, Request $request, Response $response, TokenInterface $token);

    /**
     * Returns an integer value for the order in which the listeners are executed.
     * A higher priority results in earlier execution.
     * @return integer
     */
    public function getPriority();
}