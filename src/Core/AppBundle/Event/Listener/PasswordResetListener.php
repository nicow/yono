<?php
/**
 * Created by PhpStorm.
 * User: mkt
 * Date: 08.02.17
 * Time: 15:29
 */

namespace Core\AppBundle\Event\Listener;


use FOS\UserBundle\Event\FormEvent;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\HttpFoundation\RedirectResponse;

class PasswordResetListener
{
    /**
     * @var Router
     */
    private $router;

    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    public function onPasswordResetSuccess(FormEvent $event)
    {
        $event->setResponse(new RedirectResponse($this->router->generate('home')));
    }
}