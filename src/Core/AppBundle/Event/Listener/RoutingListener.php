<?php
/**
 * Created by PhpStorm.
 * User: tim
 * Date: 06.04.17
 * Time: 10:34
 */

namespace Core\AppBundle\Event\Listener;

use Core\AppBundle\Entity\LockEntity;
use Core\AppBundle\Entity\User;
use Core\AppBundle\Service\AppHelper;
use Doctrine\Common\Annotations\Reader;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

class RoutingListener
{

    /**
     * @var ContainerInterface
     */
    protected $container;
    /**
     * @var Reader
     */
    protected $reader;
    /**
     * @var TokenStorage
     */
    private $tokenStorage;
    /**
     * @var AuthorizationChecker
     */
    private $authorizationChecker;
    /**
     * @var AppHelper
     */
    private $appHelper;
    /**
     * @var EntityManager
     */
    private $em;

    public function __construct(ContainerInterface $container, TokenStorage $tokenStorage, AuthorizationChecker $authorizationChecker, AppHelper $appHelper, EntityManager $em, Reader $reader)
    {
        $this->container = $container;
        $this->tokenStorage = $tokenStorage;
        $this->authorizationChecker = $authorizationChecker;
        $this->appHelper = $appHelper;
        $this->em = $em;
        $this->reader = $reader;
    }


    public function onKernelController(FilterControllerEvent $event)
    {
        $request = $event->getRequest();


        if ( $this->tokenStorage->getToken() && $this->tokenStorage->getToken()->getUser() && 'fos_js_routing.controller:indexAction' != $request->attributes->get('_controller') && !$event->getRequest()->isXmlHttpRequest() ) {

            /**
             * @var User $user
             */
            $user = $this->tokenStorage->getToken()->getUser();

            if ( !is_array($controller = $event->getController()) || HttpKernelInterface::MASTER_REQUEST != $event->getRequestType() || HttpKernel::MASTER_REQUEST != $event->getRequestType() )
                return;

            $object = new \ReflectionObject($controller[ 0 ]);
            $method = $object->getMethod($controller[ 1 ]);
            $parameters = new ArrayCollection();

            if ( $method ) {

                foreach ( $this->reader->getMethodAnnotations($method) as $configuration ) {
                    if ( $configuration instanceof \Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter )
                        $parameters->add($configuration);
                }

                $entry = $this->em->getRepository('CoreAppBundle:LockEntity')->findOneBy(['editBy' => $user]);
                if ( $entry ) {
                    $entry->setEntityTable(null);
                    $entry->setEditSince(null);
                    $entry->setEntityId(null);
                    $this->em->flush();
                }

                if ( $parameters ) {
                    foreach ( $parameters as $parameter ) {
                        $parametersRequest = $request->attributes->all();
                        foreach ( $parametersRequest as $parameterRequest ) {
                            if ( is_numeric($parameterRequest) ) {
                                $entity = $this->em->getRepository('CoreAppBundle:LockEntity')->findOneBy(['editBy' => $user]);
                                if ( $entity ) {
                                    $entity->setEntityTable($parameter->getClass());
                                    $entity->setEditSince(new \DateTime(date('Y-m-d H:i:s')));
                                    $entity->setEntityId($parameterRequest);
                                    $this->em->flush();
                                } else {
                                    $entity = new LockEntity();
                                    $entity->setEditBy($user);
                                    $entity->setEntityTable($parameter->getClass());
                                    $entity->setEditSince(new \DateTime(date('Y-m-d H:i:s')));
                                    $entity->setEntityId($parameterRequest);
                                    $this->em->persist($entity);
                                    $this->em->flush();
                                }
                            }
                        }
                    }
                }
            }
        }
    }

}