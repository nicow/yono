<?php
/**
 * Created by PhpStorm.
 * User: mkt
 * Date: 09.01.17
 * Time: 09:13
 */

namespace Core\AppBundle\Event\Subscriber;


use Core\AppBundle\Service\AppHelper;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\FOSUserEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

class GroupSubscriber implements EventSubscriberInterface
{
    /**
     * @var Router
     */
    private $router;

    /**
     * @var AuthorizationChecker
     */
    private $authorizationChecker;

    /**
     * @var AppHelper
     */
    private $appHelper;

    public function __construct(Router $router, AuthorizationChecker $authorizationChecker, AppHelper $appHelper)
    {
        $this->router = $router;
        $this->authorizationChecker = $authorizationChecker;
        $this->appHelper = $appHelper;
    }

    public static function getSubscribedEvents()
    {
        return [FOSUserEvents::GROUP_EDIT_SUCCESS => 'onGroupComplete', FOSUserEvents::GROUP_CREATE_SUCCESS => 'onGroupComplete',];
    }

    public function onGroupComplete(FormEvent $event)
    {
        // save permissions
        if ( $this->authorizationChecker->isGranted('ROLE_ADMIN') && $event->getRequest()->request->has('core_app_group_permissions') ) {
            $this->appHelper->saveGroupPermissions($event->getForm()->getData(), $event->getRequest()->request->get('core_app_group_permissions', []));
        }

        // redirect to edit form
        $url = $this->router->generate('fos_user_group_edit', ['groupName' => $event->getForm()->getData()->getName()]);
        $event->setResponse(new RedirectResponse($url));
    }
}