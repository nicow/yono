<?php
/**
 * Created by PhpStorm.
 * User: mkt
 * Date: 23.08.16
 * Time: 14:33
 */

namespace Core\AppBundle\Event\Subscriber;


use Core\AppBundle\Service\Settings;
use Symfony\Bundle\FrameworkBundle\Controller\Controller as BaseController;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

class MaintenanceSubscriber implements EventSubscriberInterface
{
    /**
     * @var Settings
     */
    private $settings;

    /**
     * @var Session
     */
    private $session;

    /**
     * @var TokenStorage
     */
    private $tokenStorage;

    /**
     * @var Router
     */
    private $router;

    /**
     * @var AuthorizationChecker
     */
    private $authorizationChecker;

    public function __construct(Settings $settings, Session $session, TokenStorage $tokenStorage, Router $router, AuthorizationChecker $authorizationChecker)
    {
        $this->settings = $settings;
        $this->session = $session;
        $this->tokenStorage = $tokenStorage;
        $this->authorizationChecker = $authorizationChecker;

        $this->active = $this->settings->getGlobal('maintenance');
        $this->start = new \DateTime($this->settings->getGlobal('maintenance_start'));
        $this->end = new \DateTime($this->settings->getGlobal('maintenance_end'));
        $this->note = $this->settings->getGlobal('maintenance_note');
        $this->router = $router;
    }

    /**
     * @inheritdoc
     */
    public static function getSubscribedEvents()
    {
        return [KernelEvents::CONTROLLER => 'onKernelController', KernelEvents::REQUEST => 'onKernelRequest'];
    }

    public function onKernelController(FilterControllerEvent $event)
    {
        $controller = $event->getController();

        if ( $controller[ 0 ] instanceof BaseController ) {
            if ( $event->isMasterRequest() ) {
                $this->handleForm($event->getRequest());
                $this->showMessage();
            }
        }
    }

    private function handleForm(Request $request)
    {
        if ( $systemSettings = $request->request->get('system_settings') ) {
            if ( isset($systemSettings[ 'maintenance' ]) ) {
                $maintenance = $systemSettings[ 'maintenance' ];
                if ( isset($maintenance[ 'active' ]) && $maintenance[ 'active' ] ) {
                    $start = explode('.', $maintenance[ 'start' ][ 'date' ]);
                    $start = $start[ 2 ] . '-' . $start[ 1 ] . '-' . $start[ 0 ] . ' ' . $maintenance[ 'start' ][ 'time' ];
                    $end = explode('.', $maintenance[ 'end' ][ 'date' ]);
                    $end = $end[ 2 ] . '-' . $end[ 1 ] . '-' . $end[ 0 ] . ' ' . $maintenance[ 'end' ][ 'time' ];

                    $this->settings->setGlobal('maintenance', true);
                    $this->settings->setGlobal('maintenance_start', $start);
                    $this->settings->setGlobal('maintenance_end', $end);
                    if ( isset($maintenance[ 'note' ]) && $maintenance[ 'note' ] ) {
                        $this->settings->setGlobal('maintenance_note', $maintenance[ 'note' ]);
                    }
                } else {
                    $this->settings->setGlobal('maintenance', false);
                    $this->settings->setGlobal('maintenance_start', false);
                    $this->settings->setGlobal('maintenance_end', false);
                }
            }
        }
    }

    private function showMessage()
    {
        if ( $this->active && time() < $this->end->getTimestamp() && $this->tokenStorage->getToken() && $this->tokenStorage->getToken()->getUser() && $this->authorizationChecker->isGranted('IS_AUTHENTICATED_REMEMBERED') ) {

            $timeString = 'vom ' . $this->start->format('d.m. H:i') . ' bis ' . $this->end->format('d.m. H:i');
            if ( $this->start->format('dmY') == $this->end->format('dmY') ) {
                $timeString = $this->start->format('d.m. H:i') . ' - ' . $this->end->format('H:i');
            }
            if ( $this->start->format('dmY') == date('dmY') && $this->end->format('dmY') == date('dmY') ) {
                $timeString = 'heute um ' . $this->start->format('H:i') . ' bis ' . $this->end->format('H:i');
            }

            $message = '<i class="uk-icon-warning"></i> <b>ACHTUNG:</b> Das System wird ' . $timeString . ' in den Wartungsmodus versetzt. Alle Benutzer werden dann automatisch abgemeldet.';
            if ( $this->note ) {
                $message .= '<br><i>' . $this->note . '</i>';
            }
            if ( !$this->session->getFlashBag()->has('maintenance') ) {
                $this->session->getFlashBag()->add('maintenance', $message);
            }
        }
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        if ( $event->isMasterRequest() && $this->tokenStorage->getToken() && $this->tokenStorage->getToken()->getUser() && $this->authorizationChecker->isGranted('IS_AUTHENTICATED_REMEMBERED') && $this->active && time() > $this->start->getTimestamp() && time() < $this->end->getTimestamp() && $event->getRequest()->get('_route') != 'fos_user_security_login' ) {
            $this->tokenStorage->setToken(null);
            $this->session->invalidate();
            $event->setResponse(new RedirectResponse($this->router->generate('fos_user_security_login')));
        }
    }
}