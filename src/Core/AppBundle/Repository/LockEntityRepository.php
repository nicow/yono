<?php

namespace Core\AppBundle\Repository;

/**
 * Lock entity repository
 *
 * Special database queries for locked entities
 *
 * Author: joerg.sommer@crea.de
 */
class LockEntityRepository extends \Doctrine\ORM\EntityRepository
{
}
