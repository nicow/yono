<?php

namespace Core\AppBundle\Repository;

/**
 * Documents repository
 *
 * Special database queries for documents
 *
 * Author: joerg.sommer@crea.de
 */
class DocumentsRepository extends \Doctrine\ORM\EntityRepository
{
}
