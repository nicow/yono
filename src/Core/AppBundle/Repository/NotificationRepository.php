<?php

namespace Core\AppBundle\Repository;

/**
 * Notification repository
 *
 * Special database queries for notifications
 *
 * Author: joerg.sommer@crea.de
 */
class NotificationRepository extends \Doctrine\ORM\EntityRepository
{
}
