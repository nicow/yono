<?php

namespace Core\AppBundle\Repository;

/**
 * SettingRepository
 *
 * Special database queries for settings
 *
 * Author: joerg.sommer@crea.de
 */
class SettingRepository extends \Doctrine\ORM\EntityRepository
{
}
