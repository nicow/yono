<?php

namespace Core\AppBundle\Repository;

use Core\AppBundle\Entity\UserGroup;
use Doctrine\ORM\EntityRepository;
use Modules\ContactBundle\Entity\Contact;

/**
 * User repository
 *
 * Special database queries for user accounts
 *
 * Author: joerg.sommer@crea.de
 */
class UserRepository extends EntityRepository
{
    /**
     * Returns a list of users belonging to the given group
     * @param UserGroup $group Group users are searched for
     * @return array List of users
     */
    public function findByGroup(UserGroup $group)
    {
        return $this->createQueryBuilder('u')
                    ->join('u.groups', 'ug')
                    ->where('ug.id = :group')
                    ->setParameter('group', $group->getId())
                    ->getQuery()
                    ->getResult();
    }

    /**
     * Returns alist of users belonging to a extern contact
     * @param Contact $contact Extern contact
     * @param string $sort Column the list is sorted by
     * @param string $direction Direction the sort column is sorted by
     * @return array List of users
     */
    public function findOrderedForContact(Contact $contact, $sort = 'name', $direction = 'ASC')
    {
        $direction = strtoupper($direction);

        $qb = $this->createQueryBuilder('u')
                   ->where('u.contact = :contact')
                   ->setParameter('contact', $contact)
                   ->addOrderBy('u.' . $sort, $direction);

        $users = $qb->getQuery()->getResult();

        return $users;
    }

    /**
     * Returns a list of all users who have a google account and calendar
     * @return array List of users
     */
    public function findWithGoogleCalendar()
    {
        return $this->createQueryBuilder('u')
                    ->where('u.googleEmail is not null')
                    ->andWhere('u.googleCalendarId is not null')
                    ->getQuery()
                    ->getResult();
    }
}
