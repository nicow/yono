<?php

namespace Core\AppBundle\Repository;

use Core\AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;

/**
 * Menu item repository
 *
 * Special database queries for menu items
 *
 * Author: joerg.sommer@crea.de
 */
class MenuItemRepository extends EntityRepository
{
    /**
     * Returns menu items for the given menu and user, including menu items for all users.
     * @param      $menu
     * @param User $user
     * @return array
     */
    public function findByMenuAndUser($menu, User $user)
    {
        $qb = $this->createQueryBuilder('m')
                   ->where('m.user = :user OR m.user IS NULL')
                   ->andWhere('m.menu = :menu')
                   ->andWhere('m.parent IS NULL')
                   ->setParameters(['user' => $user, 'menu' => $menu])
                   ->orderBy('m.order', 'ASC');

        return $qb->getQuery()->getResult();
    }

    /**
     * Returns menu items for the given menu and route/parameters only
     * @param $route
     * @param $routeParameters
     * @param string $menu
     * @return array
     */
    public function findByRouteAndMenu($route, $routeParameters, $menu = 'main')
    {
        $qb = $this->createQueryBuilder('m')
                   ->where('m.route = :route')
                   ->andWhere('m.menu = :menu')
                   ->andWhere('m.routeParameters = :routeParameters')
                   ->setParameters(['route' => $route, 'routeParameters' => serialize($routeParameters), 'menu' => $menu]);

        $menuItem = $qb->getQuery()->getResult();

        return $menuItem;
    }
}
