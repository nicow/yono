<?php
/**
 * Created by PhpStorm.
 * User: mkt
 * Date: 04.01.17
 * Time: 22:02
 */

namespace Core\AppBundle\DependencyInjection;


use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Search item pass
 *
 * Searches for all search item services in all modules tagged by "app.search_item" and adds them to the search
 * service "app.search".
 *
 * Author: joerg.sommer@crea.de
 *
 * Class SearchItemPass
 * @package Core\AppBundle\DependencyInjection
 */
class SearchItemPass implements CompilerPassInterface
{
    /**
     * You can modify the container here before it is dumped to PHP code.
     * @param ContainerBuilder $container
     */
    public function process(ContainerBuilder $container)
    {
        // check if the primary service is defined
        if ( !$container->has('app.search') ) {
            return;
        }

        $search = $container->findDefinition('app.search');

        // find all service IDs with the "app.search_item" tag
        $taggedServices = $container->findTaggedServiceIds('app.search_item');

        foreach ( $taggedServices as $id => $tags ) {
            $reference = new Reference($id);

            // add the search item to the search service "app.search"
            $search->addMethodCall('addSearchItem', [$reference]);
        }
    }
}