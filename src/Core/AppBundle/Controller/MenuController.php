<?php

namespace Core\AppBundle\Controller;

use Core\AppBundle\Annotation\MenuItem;
use Core\AppBundle\Service\AppHelper;
use Core\YonoBundleInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/menu")
 */
class MenuController extends Controller
{
    /**
     * @Route("/", name="menu")
     */
    public function indexAction()
    {
        return $this->render('@CoreApp/Menu/index.html.twig', ['items' => $this->getAvailableMenuItems()]);
    }

    /**
     * Collects available menu items by @MenuItem annotations.
     * @return array
     */
    private function getAvailableMenuItems()
    {
        $items = [];

        // loop through all core/module bundles
        $bundles = $this->get('app.helper')->getBundles(AppHelper::BUNDLES_YONO);
        /** @var YonoBundleInterface $bundle */
        foreach ( $bundles as $bundle ) {
            // collect by annotation
            $controllerDirectory = $bundle->getPath() . '/Controller';
            if ( file_exists($controllerDirectory) ) {
                // loop through all controllers of that bundle
                $d = dir($controllerDirectory);
                while ( false !== ($entry = $d->read()) ) {
                    $controllerClass = $bundle->getNamespace() . '\\Controller\\' . substr($entry, 0, -4);
                    if ( class_exists($controllerClass) ) {
                        // loop through all methods of that controller
                        $controller = new \ReflectionClass($controllerClass);
                        foreach ( $controller->getMethods() as $method ) {
                            // look for @MenuItem annotation
                            /** @var MenuItem $menuItemAnnotation */
                            $menuItemAnnotation = $this->get('annotation_reader')->getMethodAnnotation($method, 'Core\\AppBundle\\Annotation\\MenuItem');

                            // if annotation exists try to get a route for this menu item
                            if ( $menuItemAnnotation ) {
                                $routeName = null;

                                if ( $menuItemAnnotation->getRoute() ) {
                                    // get route by name defined in the @MenuItem annotation
                                    $routeName = $menuItemAnnotation->getRoute();
                                } else {
                                    // look for @Route annotation if @MenuItem does not define a route name
                                    $routeAnnotation = $this->get('annotation_reader')->getMethodAnnotation($method, 'Sensio\\Bundle\\FrameworkExtraBundle\\Configuration\\Route');
                                    if ( $routeAnnotation ) {
                                        $routeName = $routeAnnotation->getName();
                                    }
                                }

                                // add menu item if a route could be found
                                if ( $routeName ) {
                                    $items[ $bundle->getDisplayName() ][] = ['label' => $menuItemAnnotation->getLabel(), 'icon' => $menuItemAnnotation->getIcon(), 'attributes' => $menuItemAnnotation->getAttributes(), 'route' => $routeName, 'routeParameters' => $menuItemAnnotation->getRouteParameters()];
                                }
                            }
                        }
                    }
                }
                $d->close();
            }
        }

        return $items;
    }
}
