<?php

namespace Core\AppBundle\Controller\Api;

use Core\AppBundle\Entity\MenuItem;
use Core\AppBundle\Form\MenuItemType;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Response;

class MenuItemController extends FOSRestController
{
    /**
     * @View
     * @ApiDoc(
     *   section="Menu Items",
     *   resource=true,
     *   description="Creates a new menu item resource.",
     *   statusCodes={
     *     201 = "Returned if successful",
     *     400 = "Returned if validation fails"
     *   }
     * )
     * @return \Symfony\Component\Form\Form|Response
     */
    public function postMenuitemAction()
    {
        $menuItem = new MenuItem();

        return $this->processForm($menuItem);
    }

    /**
     * @param MenuItem $menuItem
     * @return \Symfony\Component\Form\Form|Response
     */
    private function processForm(MenuItem $menuItem)
    {
        // create form and let it handle the request
        $form = $this->createForm(MenuItemType::class, $menuItem, ['method' => $this->get('request_stack')->getCurrentRequest()->getMethod()]);
        $form->handleRequest($this->get('request_stack')->getCurrentRequest());

        // create/update the resource
        if ( $form->isValid() ) {
            $em = $this->getDoctrine()->getManager();

            $statusCode = Response::HTTP_NO_CONTENT;

            // if user is new persist it and set status code
            if ( !$menuItem->getId() ) {
                $em->persist($menuItem);
                $statusCode = Response::HTTP_CREATED;
            }

            $em->flush();

            $response = new Response();
            $response->setStatusCode($statusCode);

            // set the Location header only when creating new resources
            if ( Response::HTTP_CREATED === $statusCode ) {
                $response->headers->set('Location', $this->generateUrl('api_get_menu_item', ['menuItem' => $menuItem->getId()], 0));
            }

            return $response;
        }

        // return form object with error messages
        return $form;
    }

    /**
     * @View
     * @ApiDoc(
     *   section="Menu Items",
     *   resource=true,
     *   description="Updates an existing menu item resource.",
     *   statusCodes={
     *     204 = "Returned if successful",
     *     400 = "Returned if validation fails"
     *   }
     * )
     * @param MenuItem $menuItem
     * @return \Symfony\Component\Form\Form|Response
     */
    public function putMenuitemAction(MenuItem $menuItem)
    {
        return $this->processForm($menuItem);
    }

    /**
     * @View
     * @ApiDoc(
     *   section="Menu Items",
     *   resource=true,
     *   description="Gets a menu item resource.",
     *   statusCodes={
     *     200 = "Returned if successful",
     *     404 = "Returned if not found"
     *   }
     * )
     * @param MenuItem $menuItem
     * @return MenuItem
     */
    public function getMenuitemAction(MenuItem $menuItem)
    {
        return $menuItem;
    }

    /**
     * @View
     * @ApiDoc(
     *   section="Menu Items",
     *   resource=true,
     *   description="Gets a collection of menu item resources.",
     *   statusCodes={
     *     200 = "Returned if successful"
     *   }
     * )
     * @return mixed
     */
    public function getMenuitemsAction()
    {
        $menuItems = $this->getDoctrine()->getManager()->getRepository('CoreAppBundle:MenuItem')->findAll();

        return $menuItems;
    }

    /**
     * @View()
     * @ApiDoc(
     *   section="Menu Items",
     *   resource=true,
     *   description="Deletes a menu item resources.",
     *   statusCodes={
     *     204 = "Returned if successful",
     *     404 = "Returned if not found"
     *   }
     * )
     * @param MenuItem $menuItem
     */
    public function deleteMenuitemAction(MenuItem $menuItem)
    {
        $this->getDoctrine()->getManager()->remove($menuItem);
        $this->getDoctrine()->getManager()->flush();
    }
}