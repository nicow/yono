<?php

namespace Core\AppBundle\Controller\Api;

use Core\AppBundle\Entity\User;
use Core\AppBundle\Form\UserType;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UserController extends FOSRestController
{
    /**
     * @View
     * @ApiDoc(
     *   section="Users",
     *   resource=true,
     *   description="Creates a new user resource.",
     *   statusCodes={
     *     201 = "Returned if successful",
     *     400 = "Returned if validation fails"
     *   }
     * )
     * @return \Symfony\Component\Form\Form|Response
     */
    public function postUserAction()
    {
        $user = $this->get('fos_user.user_manager')->createUser();

        return $this->processForm($user);
    }

    /**
     * @param User $user
     * @return \Symfony\Component\Form\Form|Response
     */
    private function processForm(User $user)
    {
        // create form and let it handle the request
        $form = $this->createForm(UserType::class, $user, ['method' => $this->get('request_stack')->getCurrentRequest()->getMethod()]);
        $form->handleRequest($this->get('request_stack')->getCurrentRequest());

        // create/update the resource
        if ( $form->isValid() ) {
            $statusCode = Response::HTTP_NO_CONTENT;

            // if user is new persist it and set status code
            if ( !$user->getId() ) {
                $statusCode = Response::HTTP_CREATED;
            }
            $this->get('fos_user.user_manager')->updateUser($user);

            $response = new Response();
            $response->setStatusCode($statusCode);

            // set the Location header only when creating new resources
            if ( Response::HTTP_CREATED === $statusCode ) {
                $response->headers->set('Location', $this->generateUrl('api_get_user', ['user' => $user->getId()], 0));
            }

            return $response;
        }

        // return form object with error messages
        return $form;
    }

    /**
     * @View
     * @ApiDoc(
     *   section="Users",
     *   resource=true,
     *   description="Updates an existing user resource.",
     *   statusCodes={
     *     204 = "Returned if successful",
     *     400 = "Returned if validation fails"
     *   }
     * )
     * @param User $user
     * @return \Symfony\Component\Form\Form|Response
     */
    public function putUserAction(User $user)
    {
        return $this->processForm($user);
    }

    /**
     * @View
     * @ApiDoc(
     *   section="Users",
     *   resource=true,
     *   description="Updates the home route for a user resource.",
     *   statusCodes={
     *     204 = "Returned if successful",
     *     400 = "Returned if validation fails"
     *   }
     * )
     * @param User $user
     * @return \Symfony\Component\Form\Form|Response
     */
    public function homeUserAction(Request $request, User $user)
    {
        $homeRoute = null;
        $homeParameters = null;

        if ( $request->request->has('home_route') ) {
            $homeRoute = $request->request->get('home_route');
            if ( $this->get('router')->getRouteCollection()->get($homeRoute) ) {

                if ( $request->request->has('home_parameters') ) {
                    $homeParameters = $request->request->get('home_parameters');
                }
            }
        }

        $user->setHomeRoute($homeRoute);
        $user->setHomeParameters($homeParameters);

        $this->get('fos_user.user_manager')->updateUser($user);

        return;
    }

    /**
     * @View
     * @ApiDoc(
     *   section="Users",
     *   resource=true,
     *   description="Gets a user resource.",
     *   statusCodes={
     *     200 = "Returned if successful",
     *     404 = "Returned if not found"
     *   }
     * )
     * @param User $user
     * @return User
     */
    public function getUserAction(User $user)
    {
        return $user;
    }

    /**
     * @View
     * @ApiDoc(
     *   section="Users",
     *   resource=true,
     *   description="Gets a collection of user resources.",
     *   statusCodes={
     *     200 = "Returned if successful"
     *   }
     * )
     * @return mixed
     */
    public function getUsersAction()
    {
        $users = $this->getDoctrine()->getManager()->getRepository('CoreAppBundle:User')->findAll();

        return $users;
    }

    /**
     * @View()
     * @ApiDoc(
     *   section="Users",
     *   resource=true,
     *   description="Deletes a user resources.",
     *   statusCodes={
     *     204 = "Returned if successful",
     *     404 = "Returned if not found"
     *   }
     * )
     * @param User $user
     */
    public function deleteUserAction(User $user)
    {
        $this->getDoctrine()->getManager()->remove($user);
        $this->getDoctrine()->getManager()->flush();
    }
}