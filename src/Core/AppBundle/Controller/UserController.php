<?php

namespace Core\AppBundle\Controller;

use Core\AppBundle\Entity\User;
use Core\AppBundle\Form\UserType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * User controller.
 * @Route("/user")
 */
class UserController extends Controller
{
    /**
     * Lists all User entities.
     * @Route("/")
     * @Method({"GET", "POST"})
     * @Security("is_granted('ROLE_ADMIN')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $users = $this->getDoctrine()->getManager()->getRepository('CoreAppBundle:User')->findBy([], [$request->query->get('sort', 'username') => $request->query->get('direction', 'ASC')]);
        $columns = $this->get('app.helper')->setTableColumns('core_user_table_columns', ['email' => ['active' => 1, 'label' => 'E-Mail'], 'groups' => ['active' => 1, 'label' => 'Gruppen'], 'lastLogin' => ['active' => 1, 'label' => 'Letzte Anmeldung'], 'active' => ['active' => 1, 'label' => 'Aktiviert'], 'contact' => ['active' => 1, 'label' => 'Kontakt'], 'member' => ['active' => 1, 'label' => 'Mitarbeiter'], 'role' => ['active' => 1, 'label' => 'Rolle'],]);
        $perPage = $this->get('app.helper')->setTablePerPage('core_user_table_rows');
        $paginator = $this->get('knp_paginator');
        $page = $request->query->getInt('page', 1);
        $pagination = $paginator->paginate($users, $page, $perPage);

        $deleteForms = [];
        foreach ( $users as $user ) {
            $deleteForms[ $user->getId() ] = $this->createDeleteForm($user)->createView();
        }

        return $this->render('@CoreApp/User/index.html.twig', ['pagination' => $pagination, 'columns' => $columns, 'perPage' => $perPage, 'deleteForms' => $deleteForms]);
    }

    /**
     * Creates a form to delete a User entity.
     * @param User $user The User entity
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(User $user)
    {
        return $this->createFormBuilder()->setAction($this->generateUrl('core_app_user_delete', ['id' => $user->getId()]))->setMethod('DELETE')->getForm();
    }

    /**
     * Creates a new User entity.
     * @Route("/new")
     * @Method({"GET", "POST"})
     * @Security("is_granted('ROLE_ADMIN')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $userManager = $this->get('fos_user.user_manager');
        $user = $userManager->createUser();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ( $form->isSubmitted() && $form->isValid() ) {
            // set admin role
            if ( $form->get('admin')->getData() ) {
                $user->addRole('ROLE_ADMIN');
            } else {
                $user->removeRole('ROLE_ADMIN');
            }

            $userManager->updateUser($user);

            if ( $this->isGranted('ROLE_ADMIN') ) {
                $this->get('app.helper')->saveUserPermissions($user, $request->request->get('core_app_user_permissions', []));
            }

            $this->addFlash('success', '<i class="uk-icon-check"></i> Benutzer wurde angelegt!');

            return $this->redirectToRoute('core_app_user_index');
        }

        return $this->render('@CoreApp/User/new.html.twig', ['user' => $user, 'form' => $form->createView(),]);
    }

    /**
     * Finds and displays a User entity.
     * @Route("/{id}")
     * @Route("/me")
     * @Method("GET")
     * @param User $user
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(User $user = null)
    {
        if ( !$user ) {
            $user = $this->getUser();
        }

        $deleteForm = $this->createDeleteForm($user);

        return $this->render('@CoreApp/User/show.html.twig', ['user' => $user, 'deleteForm' => $deleteForm->createView(),]);
    }

    /**
     * Displays a form to edit an existing User entity.
     * @Route("/{id}/edit")
     * @Method({"GET", "POST"})
     * @Security("is_granted('ROLE_ADMIN')")
     * @param Request $request
     * @param User    $user
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @ParamConverter("id", class="Core\AppBundle\Entity\User")
     */
    public function editAction(Request $request, User $user)
    {
        if ( $user->hasRole('ROLE_SUPER_ADMIN') ) {
            $this->addFlash('error', '<i class="uk-icon-warning"></i> Der Super Admin kann nicht bearbeitet werden!');

            return $this->redirectToRoute('core_app_user_index');
        }

        $editForm = $this->createForm(UserType::class, $user, ['edit' => true]);
        $editForm->handleRequest($request);

        if ( $editForm->isSubmitted() && $editForm->isValid() ) {
            // set admin role
            if ( $editForm->get('admin')->getData() ) {
                $user->addRole('ROLE_ADMIN');
            } else {
                $user->removeRole('ROLE_ADMIN');
            }

            $this->get('fos_user.user_manager')->updateUser($user);

            if ( $this->isGranted('ROLE_ADMIN') ) {
                $this->get('app.helper')->saveUserPermissions($user, $request->request->get('core_app_user_permissions', []));
            }

            $this->addFlash('success', '<i class="uk-icon-check"></i> Benutzer wurde aktualisiert!');

            return $this->redirectToRoute('core_app_user_edit', ['id' => $user->getId()]);
        }

        return $this->render('@CoreApp/User/edit.html.twig', ['user' => $user, 'edit_form' => $editForm->createView()]);
    }

    /**
     * Deletes a User entity.
     * @Route("/{id}")
     * @Method("DELETE")
     * @Security("is_granted('ROLE_ADMIN')")
     * @param Request $request
     * @param User    $user
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, User $user)
    {
        if ( $user == $this->getUser() ) {
            $this->addFlash('error', '<i class="uk-icon-warning"></i> Der eigene Zugang kann nicht gelöscht werden!');

            return $this->redirectToRoute('core_app_user_index');
        }

        if ( $user->hasRole('ROLE_SUPER_ADMIN') ) {
            $this->addFlash('error', '<i class="uk-icon-warning"></i> Der Super-Administrator kann nicht gelöscht werden!');

            return $this->redirectToRoute('core_app_user_index');
        }

        $form = $this->createDeleteForm($user);
        $form->handleRequest($request);

        if ( $form->isSubmitted() && $form->isValid() ) {
            $em = $this->getDoctrine()->getManager();

            $em->remove($user);
            $em->flush();

            $this->addFlash('success', '<i class="uk-icon-check"></i> Benutzer <b>' . $user->getUsername() . '</b> wurde gelöscht!');
        }

        return $this->redirectToRoute('core_app_user_index');
    }
}
