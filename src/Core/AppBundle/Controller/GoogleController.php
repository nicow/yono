<?php

namespace Core\AppBundle\Controller;

use Core\AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class GoogleController extends Controller
{
    /**
     * @Route("/google/auth")
     * @param Request $request
     * @return RedirectResponse
     * @throws \Exception
     */
    public function authAction(Request $request)
    {
        if ( $request->query->has('code') ) {
            $google = $this->get('app.google');
            $googleClient = $google->getClient();
            $accessToken = $googleClient->authenticate($request->query->get('code'));
            if ( !array_key_exists('error', $accessToken) ) {
                if ( $refreshToken = $googleClient->getRefreshToken() ) {
                    $googlePlus = new \Google_Service_Plus($googleClient);
                    $me = $googlePlus->people->get('me');
                    /** @var \Google_Service_Plus_PersonEmails $email */
                    foreach ( $me->getEmails() as $email ) {
                        if ( $email->getType() == 'account' ) {
                            $em = $this->getDoctrine()->getManager();

                            // check if account is already in use
                            $inUse = $em->getRepository('CoreAppBundle:User')->findBy(['googleEmail' => $email->getValue()]);
                            $googleCalendar = new \Google_Service_Calendar($googleClient);
                            $calendars = $googleCalendar->calendarList->listCalendarList();
                            $settings = $this->get('app.settings');

                            if ( count($inUse) <= 2 ) {
                                // update the user
                                $this->getUser()->setGoogleEmail($email->getValue());
                                $this->getUser()->setGoogleRefreshToken($refreshToken);
                                $em->flush();

                                $settings = $this->get('app.settings');
                                $googleCalendar = new \Google_Service_Calendar($googleClient);
                                $googleCalendarId = $settings->getGlobal('mod_calendar_google_calendar_id');

                                $calendars = $googleCalendar->calendarList->listCalendarList();

                                /**
                                 * @var User $user
                                 */
                                $user = $this->getUser();

                                if ( $user->getMember() ) {
                                    $name = $user->getMember()->getShortName();
                                } else {
                                    $name = $user->getUsername();
                                }
                                $id = null;

                                foreach ( $calendars as $calendar ) {
                                    if ( $calendar[ 'summary' ] == "KM - " . $name ) {
                                        $id = $calendar[ 'id' ];
                                    }
                                }

                                if ( !$id ) {

                                    $calendar = new \Google_Service_Calendar_Calendar();

                                    $calendar->setSummary("KM - " . $name);
                                    $calendar->setTimeZone("Europe/Berlin");

                                    $newCalendar = $googleCalendar->calendars->insert($calendar);

                                    $id = $newCalendar->getId();

                                    $user = $this->getUser();

                                    if ( $user->getGoogleEmail() != $settings->getGlobal('mod_calendar_google_email') ) {

                                        $body = ['role' => 'owner', 'scope' => ['type' => 'user', 'value' => $settings->getGlobal('mod_calendar_google_email')]];

                                        $acl = new \Google_Service_Calendar_AclRule($body);

                                        $acl = $googleCalendar->acl->insert($id, $acl);

                                    }

                                }
                                $this->getUser()->setGoogleCalendarId($id);

                                $em->flush();
                                /**
                                 * @var User $user
                                 */
                                $user = $this->getUser();

                                $google = $googleCalendar = $googleRefreshToken = $accessToken = $googleEmail = $googleCalendarId = null;

                                $settings = $this->get('app.settings');
                                $google = $this->get('app.google');
                                $googleCalendar = new \Google_Service_Calendar($google->getClient());
                                $googleRefreshToken = $settings->getGlobal('mod_calendar_google_refresh_token');

                                if ( $googleRefreshToken ) {
                                    $accessToken = $google->getClient()->fetchAccessTokenWithRefreshToken($googleRefreshToken);
                                    $googleEmail = $settings->getGlobal('mod_calendar_google_email');
                                    $googleCalendarId = $settings->getGlobal('mod_calendar_google_calendar_id');


                                    $role = 'writer';

                                    if ( $user->hasRole('ROLE_SUPER_ADMIN') ) {
                                        $role = 'owner';
                                    }

                                    if ( $user->getGoogleEmail() != $settings->getGlobal('mod_calendar_google_email') ) {

                                        $body = ['role' => $role, 'scope' => ['type' => 'user', 'value' => $user->getGoogleEmail(),]];

                                        $acl = new \Google_Service_Calendar_AclRule($body);

                                        $acl = $googleCalendar->acl->insert($googleCalendarId, $acl);

                                    }
                                }

                                $em->flush();

                                $this->addFlash('success', '<i class="uk-icon-check"></i> Sie haben sich erfolgreich mit Ihrem Google Konto verbunden!');
                            } else {
                                $this->addFlash('error', '<i class="uk-icon-warning"></i> Das Google Konto <i>' . $email->getValue() . '</i> ist bereits mit dem Benutzer <i>' . $inUse->getUsername() . '</i> verbunden.');
                            }

                            return new RedirectResponse($this->generateUrl('core_app_user_show', ['id' => $this->getUser()->getId()]));
                        }
                    }
                    throw new \Exception("Fetching the account email failed for some unknown reason.");
                }
                throw new \Exception("Fetching the access token failed for some unknown reason.");
            } else {
                throw new \Exception("Fetching the access token failed: " . $accessToken[ 'error_description' ] . ': ' . $accessToken[ 'error' ]);
            }
        } elseif ( $request->query->has('error') ) {
            $this->addFlash('error', '<i class="uk-icon-warning"></i> Die Verbindung zu Ihrem Google Konto konnte nicht hergestellt werden!');

            return new RedirectResponse($this->generateUrl('core_app_user_show', ['id' => $this->getUser()->getId()]));
        }

        throw new \Exception("This route should only be used for a redirect from google's oauth dialog (containing a 'code' or 'error' query parameter).");
    }

    /**
     * @Route("/google/disconnect")
     * @return RedirectResponse
     * @throws \Exception
     */
    public function disconnectAction()
    {
        // update the user
        $this->getUser()->setGoogleEmail(null);
        $this->getUser()->setGoogleRefreshToken(null);
        $this->getDoctrine()->getManager()->flush();

        $this->addFlash('success', '<i class="uk-icon-check"></i> Die Verbindung zu Ihrem Google Konto wurde erfolgreich getrennt!');

        return new RedirectResponse($this->generateUrl('core_app_user_show', ['id' => $this->getUser()->getId()]));
    }
}
