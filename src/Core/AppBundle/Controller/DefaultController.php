<?php

namespace Core\AppBundle\Controller;

use Core\AppBundle\Annotation\MenuItem;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Home/Dashboard controller
 *
 * Controller for the home/dashboard page and the connection test
 *
 * Author: joerg.sommer@crea.de
 *
 * @package Core\AppBundle\Controller
 */
class DefaultController extends Controller
{
    /**
     * Redirect of / to the stored user starting page
     * @Route("/", name="home")
     * @MenuItem("Home")
     */
    public function homeAction()
    {
        return new RedirectResponse($this->get('app.helper')->getUserHomeUrl());
    }

    /**
     * Renders the dashboard
     * @Route("/dashboard", name="dashboard")
     * @MenuItem("Dashboard")
     */
    public function indexAction()
    {
        return $this->render('@CoreApp/Default/index.html.twig');
    }

    /**
     * Returns a status for testing
     * @Route("/connection-test", options={"expose"=true})
     * @Method("HEAD")
     * @Cache(expires="now")
     * @return Response
     */
    public function connectionTestAction()
    {
        return new Response('', 204); // 204 No Content status code
    }
}
