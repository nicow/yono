<?php

namespace Core\AppBundle\Controller;

use Core\AppBundle\Entity\Documents;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UploadController extends Controller
{
    /**
     * @Route("/document/upload/{id}/{table}")
     * @Method("POST")
     * @param Request $request
     * @return JsonResponse
     */
    public function documentUploadAction(Request $request, $id, $table)
    {
        $em = $this->getDoctrine()->getManager();

        $uploadedDocuments = [];

        //CREATE PATH FROM ENTITY PATH
        $path = str_replace('\\', '/', $table);
        $path = str_replace('Modules/', '', $path);
        $path = str_replace('/Entity/', '/', $path);

        /**
         * @var UploadedFile $file
         */
        foreach ($request->files->get('files', []) as $file) {

            $document = new Documents();
            $document->setName(time() . '_' . $file->getClientOriginalName());
            $document->setMime($file->getClientMimeType());
            $document->setSize($file->getClientSize());
            $document->setEntity($table);
            $document->setEntry($id);
            $document->setPath('../data/' . $path . '/' . $id . '/');
            $file->move('../data/' . $path . '/' . $id . '/', time() . '_' . $file->getClientOriginalName());
            $em->persist($document);
            $em->flush();

            $uploadedDocuments[] = ['id' => $document->getId(), 'pathDelete' => $this->get('router')->generate('core_app_upload_deletedocument', ['document' => $document->getId()]), 'download' => $this->get('router')->generate('core_app_download_download', ['id' => $document->getId()]), 'name' => $document->getName(), 'size' => $document->getSize(), 'type' => $document->getMime(),];
        }

        return new JsonResponse($uploadedDocuments);
    }

    /**
     * @Route("/document/delete/{document}")
     * @Method("GET")
     * @param Documents $document
     * @return Response
     */
    public function deleteDocumentAction(Documents $document)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($document);
        $em->flush();

        return new Response('', 200);
    }
}
