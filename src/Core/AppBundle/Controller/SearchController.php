<?php

namespace Core\AppBundle\Controller;

use Core\AppBundle\Annotation\MenuItem;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Search controller
 *
 * Controller for all search related pages
 *
 * @package Core\AppBundle\Controller
 */
class SearchController extends Controller
{
    /**
     * Page, search results are shown and new searches can be started
     * @Route("/search")
     * @MenuItem("Suche")
     * @param Request $request Symfony's request object
     * @return Response Response Symfony's response object
     */
    public function indexAction(Request $request)
    {
        // get search key words
        $query = $request->query->get('q', '');
        // get search result set for the given search key words
        $results = $this->get('app.search')->getResults($query);
        // get table rows per page
        $perPage = $this->get('app.helper')->setTablePerPage('core_app_search_table_rows');
        // configure KNPPaginator
        $paginator = $this->get('knp_paginator');
        $page = $request->query->getInt('page', 1);
        $pagination = $paginator->paginate($results, $page, $perPage);

        return $this->render('@CoreApp/Search/index.html.twig', ['query' => $query, 'results' => $results, 'pagination' => $pagination, 'perPage' => $perPage]);
    }

    /**
     * Creates autocomplete list in JSON format for AJAX
     * @Route("/search/autocomplete.json")
     * @param Request $request Symfony's request object
     * @return JsonResponse Autocomplete list
     */
    public function autocompleteAction(Request $request)
    {
        $query = $request->request->get('search', $request->query->get('search', ''));
        $results = ['results' => $this->get('app.search')->getAutocompleteResults($query)];

        return new JsonResponse($results);
    }

    /**
     * Creates filter settings in JSON format for AJAX
     * @Route("/search/filter")
     * @param Request $request Symfony's request object
     * @return JsonResponse Filter settings
     */
    public function filterAction(Request $request)
    {
        return new JsonResponse($this->get('app.search')->setFilter($request));
    }

    /**
     * Toggles the lock state of the filter settings by AJAX.
     * FIXME ? Hier weiter
     * @Route("/search/lock")
     * @return JsonResponse
     */
    public function lockAction()
    {
        $this->get('app.settings')->set('core_app_search_lock', !$this->get('app.settings')->get('core_app_search_lock', false));

        return new JsonResponse($this->get('app.settings')->get('core_app_search_lock'));
    }
}
