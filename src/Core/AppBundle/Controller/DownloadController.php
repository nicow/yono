<?php

namespace Core\AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Download controller
 *
 * Controlls page to download documents stored in the database and the file system
 *
 * Author: joerg.sommer@crea.de
 *
 * @package Core\AppBundle\Controller
 */
class DownloadController extends Controller
{
    /**
     * Renders a download link to a given document
     * FIXME Wo ist die Absicherung gegen unbefugten Zugriff? Warum JSON return? Warum nicht mime type aus DB?
     * @Route("/document/download/{id}")
     * @Method("GET")
     * @param integer $id Primary key of the document entity
     * @return JsonResponse
     */
    public function downloadAction($id)
    {

        $em = $this->getDoctrine()->getManager();

        $file = $em->getRepository('CoreAppBundle:Documents')->find($id);

        // FIXME ACL check here

        if ($file) {

            $filename = $file->getPath() . $file->getName();
            $response = new Response();

            // Set headers
            $response->headers->set('Cache-Control', 'private');
            $response->headers->set('Content-type', mime_content_type($filename));
            $response->headers->set('Content-Disposition', 'attachment; filename="' . basename($filename) . '";');
            $response->headers->set('Content-length', filesize($filename));

            // Send headers before outputting anything
            $response->sendHeaders();

            $response->setContent(file_get_contents($filename));

            return $response;
        } else {
            // Document not found
            return new Response('', '404');
        }
    }
}
