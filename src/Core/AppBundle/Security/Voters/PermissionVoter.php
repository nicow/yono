<?php
/**
 * Created by PhpStorm.
 * User: mkt
 * Date: 08.01.17
 * Time: 16:32
 */

namespace Core\AppBundle\Security\Voters;


use Core\AppBundle\Entity\User;
use Doctrine\Common\Util\ClassUtils;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * Permission voter
 *
 * Methods to test role permissions on entities
 *
 * Author: joerg.sommer@crea.de
 *
 * Class PermissionVoter
 * @package Core\AppBundle\Security\Voters
 */
class PermissionVoter extends Voter
{
    const VIEW = 'VIEW';
    const EDIT = 'EDIT';
    const CREATE = 'CREATE';
    const DELETE = 'DELETE';

    /**
     * Determines if the attribute and subject are supported by this voter.
     * @param string $attribute An attribute
     * @param mixed  $subject The subject to secure, e.g. an object the user wants to access or any other PHP type
     * @return bool True if the attribute and subject are supported, false otherwise
     */
    protected function supports($attribute, $subject)
    {
        // if the attribute isn't one we support, return false
        if ( !in_array($attribute, array(self::VIEW, self::EDIT, self::CREATE, self::DELETE)) ) {
            return false;
        }

        return true;
    }

    /**
     * Perform a single access check operation on a given attribute, subject and token.
     * It is safe to assume that $attribute and $subject already passed the "supports()" method check.
     * @param string         $attribute
     * @param mixed          $subject
     * @param TokenInterface $token
     * @return bool
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if ( !$user instanceof User ) {
            // the user must be logged in; if not, deny access
            return false;
        }

        return $this->can($user, $attribute, $subject);
    }

    /**
     * Returns if an action on an entity is allowed for a specific user/group
     * @param $identity User or group object
     * @param $attribute "VIEW", "CREATE", "EDIT", "DELETE" entity
     * @param $subject Entity object or class name with namespace which should be protected
     * @return mixed True, if access ist granted
     */
    public function can($identity, $attribute, $subject)
    {
        if ( is_object($identity) && method_exists($identity, 'hasRole') ) {

            // Super administrators and administrators are allways allowed to do this
            if ( $identity->hasRole('ROLE_ADMIN') || $identity->hasRole('ROLE_SUPER_ADMIN') ) {
                return true;
            }

            if ( is_object($subject) ) {
                // Owner of an object are allways allowed to do this
                if ( method_exists($subject, 'getCreatedBy') && $subject->getCreatedBy() == $identity ) {
                    return true;
                }

                // Convert object to an class string representation
                $subject = ClassUtils::getClass($subject);
            }

            if ( class_exists($subject) ) {

                // Generate role string
                $classRole = strtoupper(str_replace('\\', '_', $subject));
                $role = 'ROLE_PERM_' . $attribute . '_' . $classRole;

                // Test if user/group has role permission
                if ( $identity->hasRole($role) ) {
                    return true;
                }
            }
        }

        return false;
    }
}