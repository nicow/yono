<?php

namespace Core\AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Menu item
 *
 * Menu itmes for the side menu
 *
 * Author: joerg.sommer@crea.de
 *
 * @ORM\Table(name="core_menu_item")
 * @ORM\Entity(repositoryClass="Core\AppBundle\Repository\MenuItemRepository")
 */
class MenuItem
{
    /**
     * Primary key as an auto incrementing id
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Route this menu item leads to
     * @var string
     * @ORM\Column(name="route", type="string", length=100, nullable=true)
     */
    private $route;

    /**
     * Parameters for the route the menu item leads to
     * @var array
     * @ORM\Column(name="route_parameters", type="array", nullable=true)
     */
    private $routeParameters = [];

    /**
     * Relative path the <a>-Tag leads to
     * @var string
     * @ORM\Column(name="uri", type="string", nullable=true)
     */
    private $uri;

    /**
     * Icon of the menu item
     * @var string
     * @ORM\Column(name="icon", type="string", length=50, nullable=true)
     */
    private $icon;

    /**
     * Menu item is a header
     * @var boolean
     * @ORM\Column(name="header", type="boolean", nullable=true)
     */
    private $header;

    /**
     * Menu item is a devider
     * @var string
     * @ORM\Column(name="divider", type="boolean", nullable=true)
     */
    private $divider;

    /**
     * NOT USED
     * @var array
     * @ORM\Column(name="attributes", type="array", nullable=true)
     */
    private $attributes = [];

    /**
     * Label of the menu item
     * @var string
     * @ORM\Column(name="label", type="string", length=100)
     */
    private $label;

    /**
     * FIXME Nico fragen --> Command
     * @var string
     * @ORM\Column(name="identifier", type="string", length=100, nullable=true)
     */
    private $identifier;

    /**
     * Parent menu item of this menu item
     * @var int
     * @ORM\ManyToOne(targetEntity="MenuItem", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $parent;

    /**
     * Children menu items of this menu item
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="MenuItem", mappedBy="parent")
     * @ORM\OrderBy({"order" = "ASC"})
     */
    private $children;

    /**
     * Number to order the menu items
     * @var integer
     * @ORM\Column(name="`order`", type="integer")
     */
    private $order;

    /**
     * Menu the menu item is part of
     * @var string
     * @ORM\Column(name="menu", type="string", length=50)
     */
    private $menu;

    /**
     * NOT USED
     * @var User
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\User")
     */
    private $user;

    /**
     * Permissions the user need to see this menu item
     * @var array
     * @ORM\Column(name="permissions_needed", type="array", nullable=true)
     */
    private $permissionsNeeded;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->children = new ArrayCollection();
    }

    /**
     * Get id
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get route
     * @return string
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * Set route
     * @param string $route
     * @return MenuItem
     */
    public function setRoute($route)
    {
        $this->route = $route;

        return $this;
    }

    /**
     * Get routeParameters
     * @return array
     */
    public function getRouteParameters()
    {
        return $this->routeParameters;
    }

    /**
     * Set routeParameters
     * @param array $routeParameters
     * @return MenuItem
     */
    public function setRouteParameters($routeParameters)
    {
        $this->routeParameters = $routeParameters;

        return $this;
    }

    /**
     * Get icon
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * Set icon
     * @param string $icon
     * @return MenuItem
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * Get attributes
     * @return array
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * Set attributes
     * @param array $attributes
     * @return MenuItem
     */
    public function setAttributes($attributes)
    {
        $this->attributes = $attributes;

        return $this;
    }

    /**
     * Get label
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set label
     * @param string $label
     * @return MenuItem
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get parent
     * @return MenuItem
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set parent
     * @param MenuItem $parent
     * @return MenuItem
     */
    public function setParent(MenuItem $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Add child
     * @param MenuItem $child
     * @return MenuItem
     */
    public function addChild(MenuItem $child)
    {
        $this->children[] = $child;

        return $this;
    }

    /**
     * Remove child
     * @param MenuItem $child
     */
    public function removeChild(MenuItem $child)
    {
        $this->children->removeElement($child);
    }

    /**
     * Get children
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Get uri
     * @return string
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * Set uri
     * @param string $uri
     * @return MenuItem
     */
    public function setUri($uri)
    {
        $this->uri = $uri;

        return $this;
    }

    /**
     * Get header
     * @return boolean
     */
    public function getHeader()
    {
        return $this->header;
    }

    /**
     * Set header
     * @param boolean $header
     * @return MenuItem
     */
    public function setHeader($header)
    {
        $this->header = $header;

        return $this;
    }

    /**
     * Get divider
     * @return boolean
     */
    public function getDivider()
    {
        return $this->divider;
    }

    /**
     * Set divider
     * @param boolean $divider
     * @return MenuItem
     */
    public function setDivider($divider)
    {
        $this->divider = $divider;

        return $this;
    }

    /**
     * Get order
     * @return integer
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set order
     * @param integer $order
     * @return MenuItem
     */
    public function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get menu
     * @return string
     */
    public function getMenu()
    {
        return $this->menu;
    }

    /**
     * Set menu
     * @param string $menu
     * @return MenuItem
     */
    public function setMenu($menu)
    {
        $this->menu = $menu;

        return $this;
    }

    /**
     * Get user
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set user
     * @param User $user
     * @return MenuItem
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get permissionsNeeded
     * @return array
     */
    public function getPermissionsNeeded()
    {
        return $this->permissionsNeeded;
    }

    /**
     * Set permissionsNeeded
     * @param array $permissionsNeeded
     * @return MenuItem
     */
    public function setPermissionsNeeded($permissionsNeeded)
    {
        $this->permissionsNeeded = $permissionsNeeded;

        return $this;
    }

    /**
     * @return string
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * @param string $identifier
     * @return $this
     */
    public function setIdentifier(string $identifier)
    {
        $this->identifier = $identifier;

        return $this;
    }
}
