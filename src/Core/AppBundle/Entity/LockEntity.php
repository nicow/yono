<?php

namespace Core\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LockEntity
 *
 * Keeps information by who and since when a row of an entity is in use by a user
 *
 * Author: joerg.sommer@crea.de
 *
 * @ORM\Table(name="core_lock_entity")
 * @ORM\Entity(repositoryClass="Core\AppBundle\Repository\LockEntityRepository")
 * @package Core\AppBundle\Entity
 */
class LockEntity
{
    /**
     * Primary key as an auto incrementing id
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Timestamp, since when the row is in use
     * @var \DateTime
     * @ORM\Column(name="editSince", type="datetime", nullable=true)
     */
    private $editSince;

    /**
     * Entity the row in use is in
     * @var string
     * @ORM\Column(name="entityTable", type="string", length=255, nullable=true)
     */
    private $entityTable;

    /**
     * Primary key of the row that is in use
     * @var integer
     * @ORM\Column(name="entityId", type="integer", length=7, nullable=true)
     */
    private $entityId;

    /**
     * user who uses the row
     * @var User
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\User")
     * @ORM\JoinColumn(name="edit_by_id", referencedColumnName="id",  onDelete="SET NULL")
     */
    private $editBy;


    /**
     * Get id
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get editSince
     * @return \DateTime
     */
    public function getEditSince()
    {
        return $this->editSince;
    }

    /**
     * Set editSince
     * @param \DateTime $editSince
     * @return LockEntity
     */
    public function setEditSince($editSince)
    {
        $this->editSince = $editSince;

        return $this;
    }

    /**
     * Get entityTable
     * @return string
     */
    public function getEntityTable()
    {
        return $this->entityTable;
    }

    /**
     * Set entityTable
     * @param string $entityTable
     * @return LockEntity
     */
    public function setEntityTable($entityTable)
    {
        $this->entityTable = $entityTable;

        return $this;
    }

    /**
     * @return User
     */
    public function getEditBy()
    {
        return $this->editBy;
    }

    /**
     * @param User $editBy
     * @return LockEntity
     */
    public function setEditBy($editBy)
    {
        $this->editBy = $editBy;

        return $this;
    }

    /**
     * @return int
     */
    public function getEntityId()
    {
        return $this->entityId;
    }

    /**
     * @param int $entityId
     * @return LockEntity
     */
    public function setEntityId($entityId)
    {
        $this->entityId = $entityId;

        return $this;
    }
}

