<?php

namespace Core\AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Modules\CalendarBundle\Entity\GoogleEvent;
use Modules\ContactBundle\Entity\Contact;
use Modules\TeamBundle\Entity\GooglePlan;
use Modules\TeamBundle\Entity\Member;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * User
 *
 * The account an user can log in.
 *
 * Author: joerg.sommer@crea.de
 *
 * @ORM\Entity(repositoryClass="Core\AppBundle\Repository\UserRepository")
 * @ORM\Table(name="core_user")
 * @UniqueEntity(fields={"username"}, message="Dieser Benutzername wird bereits verwendet.")
 * @UniqueEntity(fields={"email"}, message="Diese E-Mail-Adresse wird bereits verwendet.")
 * @Gedmo\SoftDeleteable()
 */
class User extends BaseUser
{
    // Includes module specific user properties
    use UserTraits;

    // Includes create/update information
    use BlameableTrait;

    // Includes deleted information
    // marks this row as deleted without deleting it physical
    // see Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity
    use SoftDeleteableEntity;

    /**
     * Primary key as an auto incrementing id
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Groups, the user is member in
     * @ORM\ManyToMany(targetEntity="Core\AppBundle\Entity\UserGroup")
     * @ORM\JoinTable(name="core_user_users_groups",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="group_id", referencedColumnName="id", onDelete="CASCADE")}
     * )
     */
    protected $groups;

    /**
     * User settings of the user
     * @var Setting[]
     * @ORM\OneToMany(targetEntity="Core\AppBundle\Entity\Setting", mappedBy="user")
     */
    private $settings;

    /**
     * Notifications of the user
     * @var Notification[]
     * @ORM\OneToMany(targetEntity="Core\AppBundle\Entity\Notification", mappedBy="user")
     */
    private $notifications;

    /**
     * Page the user wants to start with
     * @var string
     * @ORM\Column(type="string", name="home_route", nullable=true)
     */
    private $homeRoute;

    /**
     * Page parameters for the route the user wants to start with
     * @var array
     * @ORM\Column(type="json_array", name="home_parameters", nullable=true)
     */
    private $homeParameters;

    /**
     * Google refresh token of the user
     * @var string
     * @ORM\Column(name="google_refresh_token", type="string", nullable=true)
     */
    private $googleRefreshToken;

    /**
     * Google email of the user
     * @var string
     * @ORM\Column(name="google_email", type="string", nullable=true)
     */
    private $googleEmail;

    /**
     * Google calendar id of the user
     * @var string
     * @ORM\Column(name="google_calendar_id", type="string", nullable=true)
     */
    private $googleCalendarId;

    /**
     * FIXME ?
     * @var GoogleEvent[]
     * @ORM\OneToMany(targetEntity="Modules\CalendarBundle\Entity\GoogleEvent", mappedBy="user")
     */
    private $googleEvents;

    /**
     * FIXME ?
     * @var GooglePlan[]
     * @ORM\OneToMany(targetEntity="Modules\TeamBundle\Entity\GooglePlan", mappedBy="user")
     */
    private $googlePlans;


    /**
     * FIXME ?
     * @var Contact
     * @ORM\ManyToOne(targetEntity="Modules\ContactBundle\Entity\Contact", inversedBy="users")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $contact;

    /**
     * One-to-one link between user account and team member (employee) to hold domain
     * properties of the team member seperated from the abstract FOS user account management
     * @var Member
     * @ORM\OneToOne(targetEntity="Modules\TeamBundle\Entity\Member", inversedBy="user")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $member;

    public function __construct()
    {
        parent::__construct();

        $this->groups = new ArrayCollection();
        $this->settings = new ArrayCollection();
        $this->notifications = new ArrayCollection();
    }

    /**
     * Add notification
     * @param Notification $notification
     * @return User
     */
    public function addNotification(Notification $notification)
    {
        $this->notifications[] = $notification;

        return $this;
    }

    /**
     * Remove notification
     * @param Notification $notification
     */
    public function removeNotification(Notification $notification)
    {
        $this->notifications->removeElement($notification);
    }

    /**
     * Get notifications
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNotifications()
    {
        return $this->notifications;
    }

    /**
     * Get homeParameters
     * @return array
     */
    public function getHomeParameters()
    {
        if ( $this->getHomeRoute() == "modules_calendar_default_day" ) {
            return ["day" => date("Y-m-d")];
        }

        if ( $this->getHomeRoute() == "modules_calendar_default_month" ) {
            return ["month" => date("m"), "year" => date("Y")];
        }

        if ( $this->getHomeRoute() == "modules_calendar_default_week" ) {
            return ["week" => date("W"), "year" => date("Y")];
        }


        return $this->homeParameters;
    }

    /**
     * Set homeParameters
     * @param array $homeParameters
     * @return User
     */
    public function setHomeParameters($homeParameters)
    {
        $this->homeParameters = $homeParameters;

        return $this;
    }

    /**
     * Get homeRoute
     * @return string
     */
    public function getHomeRoute()
    {
        return $this->homeRoute;
    }

    /**
     * Set homeRoute
     * @param string $homeRoute
     * @return User
     */
    public function setHomeRoute($homeRoute)
    {
        $this->homeRoute = $homeRoute;

        return $this;
    }

    /**
     * Add setting
     * @param Setting $setting
     * @return User
     */
    public function addSetting(Setting $setting)
    {
        $this->settings[] = $setting;

        return $this;
    }

    /**
     * Remove setting
     * @param Setting $setting
     */
    public function removeSetting(Setting $setting)
    {
        $this->settings->removeElement($setting);
    }

    /**
     * Get settings
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSettings()
    {
        return $this->settings;
    }

    /**
     * {@inheritdoc}
     */
    public function getRoles()
    {
        $roles = $this->roles;

        // check for user specific permission roles
        $hasOwnPerms = false;
        foreach ( $roles as $role ) {
            if ( strpos($role, 'ROLE_PERM_') === 0 ) {
                $hasOwnPerms = true;
                break;
            }
        }

        // inherit groups roles if the user has no specific permissions on its own
        if ( !$hasOwnPerms ) {
            foreach ( $this->getGroups() as $group ) {
                $roles = array_merge($roles, $group->getRoles());
            }
        }

        // we need to make sure to have at least one role
        $roles[] = static::ROLE_DEFAULT;

        return array_unique($roles);
    }

    public function getCustomPermissions()
    {
        $customPermissions = [];
        foreach ( $this->roles as $role ) {
            if ( strpos($role, 'ROLE_PERM_') === 0 ) {
                $customPermissions[] = $role;
            }
        }

        return $customPermissions;
    }

    /**
     * @return string
     */
    public function getGoogleRefreshToken()
    {
        return $this->googleRefreshToken;
    }

    /**
     * @param string $googleRefreshToken
     * @return User
     */
    public function setGoogleRefreshToken($googleRefreshToken)
    {
        $this->googleRefreshToken = $googleRefreshToken;

        return $this;
    }

    /**
     * Get googleEmail
     * @return string
     */
    public function getGoogleEmail()
    {
        return $this->googleEmail;
    }

    /**
     * Set googleEmail
     * @param string $googleEmail
     * @return User
     */
    public function setGoogleEmail($googleEmail)
    {
        $this->googleEmail = $googleEmail;

        return $this;
    }

    /**
     * Get contact
     * @return Contact
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set contact
     * @param Contact $contact
     * @return User
     */
    public function setContact(Contact $contact = null)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get member
     * @return Member
     */
    public function getMember()
    {
        return $this->member;
    }

    /**
     * Set member
     * @param Member $member
     * @return User
     */
    public function setMember(Member $member = null)
    {
        $this->member = $member;

        return $this;
    }

    /**
     * @return string
     */
    public function getGoogleCalendarId()
    {
        return $this->googleCalendarId;
    }

    /**
     * @param string $googleCalendarId
     * @return User
     */
    public function setGoogleCalendarId($googleCalendarId)
    {
        $this->googleCalendarId = $googleCalendarId;

        return $this;
    }

    /**
     * @return GoogleEvent[]
     */
    public function getGoogleEvents()
    {
        return $this->googleEvents;
    }

    /**
     * @param GoogleEvent[] $googleEvents
     * @return User
     */
    public function setGoogleEvents($googleEvents)
    {
        $this->googleEvents = $googleEvents;

        return $this;
    }

    /**
     * @return GooglePlan[]
     */
    public function getGooglePlans()
    {
        return $this->googlePlans;
    }

    /**
     * @param GooglePlan[] $googlePlans
     * @return User
     */
    public function setGooglePlans($googlePlans)
    {
        $this->googlePlans = $googlePlans;

        return $this;
    }
}
