<?php

namespace Core\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Relation (NOT USED)
 * @ORM\Table(name="core_relation")
 * @ORM\Entity(repositoryClass="Core\AppBundle\Repository\RelationRepository")
 * @package Core\AppBundle\Entity
 */
class Relation
{
    /**
     * Primary key as an auto incrementing id
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(name="context", type="string", length=255, nullable=true)
     */
    private $context;

    /**
     * @var \DateTime
     * @ORM\Column(name="since", type="datetime")
     */
    private $since;

    /**
     * @var \DateTime
     * @ORM\Column(name="until", type="datetime", nullable=true)
     */
    private $until;

    /**
     * @var string
     * @ORM\Column(name="firstEntityClass", type="string", length=255)
     */
    private $firstEntityClass;

    /**
     * @var string
     * @ORM\Column(name="secondEntityClass", type="string", length=255)
     */
    private $secondEntityClass;

    /**
     * @var int
     * @ORM\Column(name="firstEntityId", type="integer")
     */
    private $firstEntityId;

    /**
     * @var int
     * @ORM\Column(name="secondEntityId", type="integer")
     */
    private $secondEntityId;

    /**
     * @var string
     * @ORM\Column(name="firstEntityRole", type="string", length=255, nullable=true)
     */
    private $firstEntityRole;

    /**
     * @var string
     * @ORM\Column(name="secondEntityRole", type="string", length=255, nullable=true)
     */
    private $secondEntityRole;

    public function __construct()
    {
        $this->since = new \DateTime();
    }

    /**
     * Get id
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     * @param string $name
     * @return Relation
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get context
     * @return string
     */
    public function getContext()
    {
        return $this->context;
    }

    /**
     * Set context
     * @param string $context
     * @return Relation
     */
    public function setContext($context)
    {
        $this->context = $context;

        return $this;
    }

    /**
     * Get since
     * @return \DateTime
     */
    public function getSince()
    {
        return $this->since;
    }

    /**
     * Set since
     * @param \DateTime $since
     * @return Relation
     */
    public function setSince($since)
    {
        $this->since = $since;

        return $this;
    }

    /**
     * Get until
     * @return \DateTime
     */
    public function getUntil()
    {
        return $this->until;
    }

    /**
     * Set until
     * @param \DateTime $until
     * @return Relation
     */
    public function setUntil($until)
    {
        $this->until = $until;

        return $this;
    }

    /**
     * Get firstEntityClass
     * @return string
     */
    public function getFirstEntityClass()
    {
        return $this->firstEntityClass;
    }

    /**
     * Set firstEntityClass
     * @param string $firstEntityClass
     * @return Relation
     */
    public function setFirstEntityClass($firstEntityClass)
    {
        $this->firstEntityClass = $firstEntityClass;

        return $this;
    }

    /**
     * Get secondEntityClass
     * @return string
     */
    public function getSecondEntityClass()
    {
        return $this->secondEntityClass;
    }

    /**
     * Set secondEntityClass
     * @param string $secondEntityClass
     * @return Relation
     */
    public function setSecondEntityClass($secondEntityClass)
    {
        $this->secondEntityClass = $secondEntityClass;

        return $this;
    }

    /**
     * Get firstEntityId
     * @return int
     */
    public function getFirstEntityId()
    {
        return $this->firstEntityId;
    }

    /**
     * Set firstEntityId
     * @param integer $firstEntityId
     * @return Relation
     */
    public function setFirstEntityId($firstEntityId)
    {
        $this->firstEntityId = $firstEntityId;

        return $this;
    }

    /**
     * Get secondEntityId
     * @return int
     */
    public function getSecondEntityId()
    {
        return $this->secondEntityId;
    }

    /**
     * Set secondEntityId
     * @param integer $secondEntityId
     * @return Relation
     */
    public function setSecondEntityId($secondEntityId)
    {
        $this->secondEntityId = $secondEntityId;

        return $this;
    }

    /**
     * Get firstEntityRole
     * @return string
     */
    public function getFirstEntityRole()
    {
        return $this->firstEntityRole;
    }

    /**
     * Set firstEntityRole
     * @param string $firstEntityRole
     * @return Relation
     */
    public function setFirstEntityRole($firstEntityRole)
    {
        $this->firstEntityRole = $firstEntityRole;

        return $this;
    }

    /**
     * Get secondEntityRole
     * @return string
     */
    public function getSecondEntityRole()
    {
        return $this->secondEntityRole;
    }

    /**
     * Set secondEntityRole
     * @param string $secondEntityRole
     * @return Relation
     */
    public function setSecondEntityRole($secondEntityRole)
    {
        $this->secondEntityRole = $secondEntityRole;

        return $this;
    }
}

