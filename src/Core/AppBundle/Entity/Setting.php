<?php

namespace Core\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * User settings
 *
 * Stores a single setting of an user as a JSON representation under a given name.
 *
 * Author: joerg.sommer@crea.de
 *
 * @ORM\Table(name="core_setting")
 * @ORM\Entity(repositoryClass="Core\AppBundle\Repository\SettingRepository")
 * @package Core\AppBundle\Entity
 */
class Setting
{
    /**
     * Primary key as an auto incrementing id
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Name of the single setting
     * @var string
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * Single setting as a JSON representation.
     * @var string
     * @ORM\Column(name="value", type="text")
     */
    private $value;

    /**
     * User, this single setting belongs to
     * @var User
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\User", inversedBy="settings")
     */
    private $user;


    /**
     * Get id
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     * @param string $name
     * @return Setting
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get value of this setting
     * Returns a representation of the stored string. In case of an array, an associative array is returned.
     * See http://php.net/manual/de/function.json-decode.php
     * @return mixed Value of the setting
     */
    public function getValue()
    {
        return json_decode($this->value, true);
    }

    /**
     * Set value of this setting
     * Stores a representation of the given valueas a string.
     * @param mixed $value Value of the setting
     * @return Setting This object
     */
    public function setValue($value)
    {
        $this->value = json_encode($value);

        return $this;
    }

    /**
     * Get user
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set user
     * @param User $user
     * @return Setting This object
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }
}
