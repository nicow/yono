<?php

namespace Core\AppBundle\Entity;

/**
 * User traits
 *
 * This class exists only to add all module specific user traits to the entity "User". By this you can add properties
 * needed by your module to the user entity.
 *
 * Author: joerg.sommer@crea.de
 *
 * @package Core\AppBundle\Entity
 */
trait UserTraits
{
    // List of all module specific user traits
    // use \Modules\MyBundle\Entity\UserTrait;

}