<?php

namespace Core\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Documents
 *
 * Stores all required information of a file/document
 * FIXME Warum Documents? --> Document
 *
 * Author: joerg.sommer@crea.de
 *
 * @ORM\Table(name="core_documents")
 * @ORM\Entity(repositoryClass="Core\AppBundle\Repository\DocumentsRepository")
 * @package Core\AppBundle\Entity
 */
class Documents
{
    // Includes create/update information
    use BlameableTrait;

    /**
     * Primary key as an auto incrementing id
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Name of the document
     * @var string
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * Entity this document belongs to
     * @var string
     * @ORM\Column(name="entity", type="string", length=255, nullable=true)
     */
    private $entity;

    /**
     * Primary key of the entity the document belongs to
     * @var integer
     * @ORM\Column(name="entry", type="integer", nullable=true)
     */
    private $entry;

    /**
     * Mime type of the document
     * @var string
     * @ORM\Column(name="mime", type="string", length=40, nullable=true)
     */
    private $mime;

    /**
     * Size of the document
     * @var string
     * @ORM\Column(name="size", type="string", length=20, nullable=true)
     */
    private $size;

    /**
     * Path to the document in the file system
     * @var string
     * @ORM\Column(name="path", type="string", length=255, nullable=true)
     */
    private $path;


    /**
     * Get id
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     * @param string $name
     * @return Documents
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get mime
     * @return string
     */
    public function getMime()
    {
        return $this->mime;
    }

    /**
     * Set type
     * @param string $mime
     * @return Documents
     */
    public function setMime($mime)
    {
        $this->mime = $mime;

        return $this;
    }

    /**
     * Get size
     * @return string
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Set size
     * @param string $size
     * @return Documents
     */
    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    /**
     * Get path
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set path
     * @param string $path
     * @return Documents
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * @return int
     */
    public function getEntry()
    {
        return $this->entry;
    }

    /**
     * @param int $entry
     * @return Documents
     */
    public function setEntry($entry)
    {
        $this->entry = $entry;

        return $this;
    }

    /**
     * @return string
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @param string $entity
     * @return Documents
     */
    public function setEntity($entity)
    {
        $this->entity = $entity;

        return $this;
    }
}

