<?php

namespace Core\AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class UserType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $user = $builder->getData();

        $builder->add('username', null, ['label' => 'Benutzername'])->add('email', EmailType::class, ['constraints' => [new Assert\Email()], 'label' => 'E-Mail'])->add('plain_password', RepeatedType::class, ['label' => 'Passwort', 'first_options' => ['label' => 'Passwort'], 'second_options' => ['label' => 'Passwort wiederholen'], 'invalid_message' => 'Die Passwörter müssen übereinstimmen.', 'type' => PasswordType::class, 'required' => !$options[ 'edit' ]])->add('groups', null, ['label' => 'Gruppen'])->add('enabled', null, ['label' => 'Aktiviert'])->add('contact', null, ['label' => 'Kontakt', 'placeholder' => 'kein Kontakt'])->add('member', null, ['label' => 'Mitarbeiter', 'placeholder' => 'kein Mitarbeiter'])->add('admin', CheckboxType::class, ['label' => 'Admin', 'required' => false, 'mapped' => false, 'data' => $user->hasRole('ROLE_ADMIN')]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => 'Core\AppBundle\Entity\User', 'csrf_protection' => false, 'edit' => true]);
    }
}
