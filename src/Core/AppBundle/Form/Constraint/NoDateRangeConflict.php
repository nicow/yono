<?php
/**
 * Created by PhpStorm.
 * User: mkt
 * Date: 30.01.17
 * Time: 14:20
 */

namespace Core\AppBundle\Form\Constraint;


use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class NoDateRangeConflict extends Constraint
{
    public $message = 'This date range conflicts with another one.';

    public $message_callback = null;

    public $start = 'start';

    public $end = 'end';

    public $query_builder = null;

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}