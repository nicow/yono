<?php
/**
 * Created by PhpStorm.
 * User: mkt
 * Date: 30.01.17
 * Time: 14:20
 */

namespace Core\AppBundle\Form\Constraint;


use Doctrine\ORM\EntityManager;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class NoDateRangeConflictValidator extends ConstraintValidator
{
    /** @var ContainerInterface */
    protected $container;
    /**
     * @var EntityManager
     */
    private $em;

    public function __construct(EntityManager $em, ContainerInterface $container)
    {
        $this->em = $em;
        $this->container = $container;
    }

    /**
     * Checks if the passed value is valid.
     * @param mixed      $entity
     * @param Constraint $constraint The constraint for the validation
     * @throws \Exception
     * @internal param mixed $value The value that should be validated
     */
    public function validate($entity, Constraint $constraint)
    {
        $accessor = PropertyAccess::createPropertyAccessor();
        $start = $accessor->getValue($entity, $constraint->start);
        $end = $accessor->getValue($entity, $constraint->end);

        if ( !$end ) {
            $end = $start;
        }

        $conflictingEntities = null;


        if ( $start instanceof \DateTime && $end instanceof \DateTime ) {
            // check start < end
            if ( $start->getTimestamp() > $end->getTimestamp() ) {
                $this->context->buildViolation('Das Start-Datum muss vor dem End-Datum liegen.')->atPath($constraint->start)->addViolation();
            }
            //check if an user can have multiple events on same time
            if ( !$this->container->getParameter('modules_calendar_allow_overlap') ) {

                $qb = $this->em->getRepository(get_class($entity))->createQueryBuilder('entity')->andWhere('entity.' . $constraint->start . ' < :end AND entity.' . $constraint->end . ' > :start')->setParameter('start', $start)->setParameter('end', $end);

                // if entity gets updated, exclude itself from query
                if ( $entity->getId() ) {
                    $qb->andWhere('entity.id != :id')->setParameter('id', $entity->getId());
                }

                // call query builder callback
                if ( is_callable($constraint->query_builder) ) {
                    $qb = call_user_func($constraint->query_builder, $qb, $entity);

                    if ( null !== $qb && !$qb instanceof QueryBuilder ) {
                        throw new UnexpectedTypeException($qb, 'Doctrine\ORM\QueryBuilder');
                    }
                }

                // get conflicts
                $conflictingEntities = $qb->getQuery()->getResult();
            }


            if ( $conflictingEntities ) {
                // call message callback
                if ( is_callable($constraint->message_callback) ) {
                    $constraint->message = call_user_func($constraint->message_callback, $entity, $conflictingEntities);
                }

                $this->context->buildViolation($constraint->message)->atPath($constraint->start)->addViolation();
            }
        } else {
            throw new \Exception('To use the NoDateRangeConflict constraint the properties "start" and "end" of class ' . get_class($entity) . ' must be of type \\DateTime. You can configure the properties to use as start and end values with the "start" and "end" options of the NoDateRangeConflict constraint.');
        }
    }
}