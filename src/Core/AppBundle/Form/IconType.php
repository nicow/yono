<?php

namespace Core\AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class IconType extends AbstractType
{
    public function getParent()
    {
        return TextType::class;
    }
}