<?php
/**
 * Created by PhpStorm.
 * User: mkt
 * Date: 09.09.16
 * Time: 16:53
 */

namespace Core;


interface YonoBundleInterface
{
    /**
     * @return string
     */
    public function getDisplayName();

    /**
     * @return string
     */
    public function getDescription();

    /**
     * @return string
     */
    public function getIcon();
}