<?php

namespace Core\OAuthBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\OAuthServerBundle\Entity\RefreshToken as BaseRefreshToken;

/**
 * RefreshToken
 * @ORM\Entity
 * @ORM\Table(name="core_oauth_refresh_token")
 */
class RefreshToken extends BaseRefreshToken
{
    /**
     * Primary key as an auto incrementing id
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Client")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $client;

    /**
     * @ORM\ManyToOne(targetEntity="Core\AppBundle\Entity\User")
     */
    protected $user;
}

