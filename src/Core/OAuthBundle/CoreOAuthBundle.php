<?php

namespace Core\OAuthBundle;

use Core\YonoBundleInterface;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class CoreOAuthBundle extends Bundle implements YonoBundleInterface
{
    /**
     * @return string
     */
    public function getDisplayName()
    {
        return 'Yono OAuth';
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return 'OAuth Implementierung für externe Schnittstellen.';
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return '';
    }
}
