<?php

namespace Core\OAuthBundle\Controller;

use Core\OAuthBundle\Entity\Client;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Client controller.
 * @Route("/oauth/v2/client")
 */
class ClientController extends Controller
{
    /**
     * Lists all Client entities.
     * @Route("/", name="oauth_v2_client_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $clients = $em->getRepository('CoreOAuthBundle:Client')->findAll();

        return $this->render('@CoreOAuth/Client/index.html.twig', ['clients' => $clients,]);
    }

    /**
     * Creates a new Client entity.
     * @Route("/new", name="oauth_v2_client_new")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function newAction(Request $request)
    {
        $client = new Client();
        $form = $this->createForm('Core\OAuthBundle\Form\ClientType', $client);
        $form->handleRequest($request);

        if ( $form->isSubmitted() && $form->isValid() ) {
            $client->setRedirectUris([$form->get('redirect_uris')->getData()]);
            $em = $this->getDoctrine()->getManager();
            $em->persist($client);
            $em->flush();

            return $this->redirectToRoute('oauth_v2_client_show', array('id' => $client->getId()));
        }

        return $this->render('@CoreOAuth/Client/new.html.twig', ['client' => $client, 'form' => $form->createView(),]);
    }

    /**
     * Finds and displays a Client entity.
     * @Route("/{id}", name="oauth_v2_client_show")
     * @Method("GET")
     * @param Client $client
     * @return array
     */
    public function showAction(Client $client)
    {
        $deleteForm = $this->createDeleteForm($client);

        return $this->render('@CoreOAuth/Client/show.html.twig', ['client' => $client, 'delete_form' => $deleteForm->createView(),]);
    }

    /**
     * Creates a form to delete a Client entity.
     * @param Client $client The Client entity
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Client $client)
    {
        return $this->createFormBuilder()->setAction($this->generateUrl('oauth_v2_client_delete', array('id' => $client->getId())))->setMethod('DELETE')->getForm();
    }

    /**
     * Displays a form to edit an existing Client entity.
     * @Route("/{id}/edit", name="oauth_v2_client_edit")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param Client  $client
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function editAction(Request $request, Client $client)
    {
        $deleteForm = $this->createDeleteForm($client);
        $editForm = $this->createForm('Core\OAuthBundle\Form\ClientType', $client);
        $editForm->handleRequest($request);

        if ( $editForm->isSubmitted() && $editForm->isValid() ) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($client);
            $em->flush();

            return $this->redirectToRoute('oauth_v2_client_edit', array('id' => $client->getId()));
        }

        return $this->render('@CoreOAuth/Client/edit.html.twig', ['client' => $client, 'edit_form' => $editForm->createView(), 'delete_form' => $deleteForm->createView(),]);
    }

    /**
     * Deletes a Client entity.
     * @Route("/{id}", name="oauth_v2_client_delete")
     * @Method("DELETE")
     * @param Request $request
     * @param Client  $client
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, Client $client)
    {
        $form = $this->createDeleteForm($client);
        $form->handleRequest($request);

        if ( $form->isSubmitted() && $form->isValid() ) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($client);
            $em->flush();
        }

        return $this->redirectToRoute('oauth_v2_client_index');
    }
}
