# Objekte

## Kalender

### Termin

**Felder**

- Bezeichnung
- Art (Termin, Außendienst, Urlaub, Krankheit)
- Ganztägig
- Von (Datum/Uhrzeit)
- Bis (Datum/Uhrzeit)
- Beschreibung

**Beziehungen**

- Benutzer
- Kontakt
- Projekt
- Adresse

## Kontakte

### Kontakt

**Felder**

- Name
- Rabatsatz

**Beziehungen**

- Kategorien

### Adresse

**Felder**

- Bezeichnung
- Straße
- PLZ
- Ort
- Bundesland
- Land
- (indiv. Formularfelder)

**Beziehungen**

- Kontakt

### Ansprechpartner

**Felder**

- Name
- Firma
- Position
- Telefon
- Mobil
- E-Mail
- (indiv. Formularfelder)

**Beziehungen**

- Kontakt

#### Kontoverbindungen

**Felder**

- Inhaber
- Kreditinstitut
- IBAN
- BIC
- Mobil
- E-Mail
- (indiv. Formularfelder)

**Beziehungen**

- Kontakt

## Buchhaltung

### Geschäftsbereich

**Felder**

- Name

**Bezieungen**

- Angebote
- Aufträge
- Rechnungen
- Mahnungen
- Gutschriften
- Leistungen
- Preislisten
- Vorlagen

### Angebot

**Felder**

- Titel
- Datum
- Angebotsnummer

**Beziehungen**

- Geschäftsbereich
- Kunde
- Projekt
- Auftrag
- Rechnung
- Vorlagen
- Leistungen
- Dateien

### Auftrag

**Felder**

- Titel
- Datum
- Auftragsnummer

**Beziehungen**

- Geschäftsbereich
- Kunde
- Projekt
- Angebot
- Rechnung
- Vorlagen
- Leistungen
- Dateien

### Rechnung

**Felder**

- Titel
- Datum
- Rechnungsdatum
- Rechnungsnummer

**Beziehungen**

- Geschäftsbereich
- Kunde
- Projekt
- Angebot
- Auftrag
- Mahnung
- Vorlagen
- Leistungen
- Dateien

### Mahnung

**Felder**

- Titel
- Datum
- Mahnungsdatum
- Mahnungsnummer

**Beziehungen**

- Geschäftsbereich
- Kunde
- Projekt
- Rechnung
- Vorlagen
- Leistungen
- Dateien

### Gutschrift

**Felder**

- Titel
- Datum
- Gutschriftsnummer
- Betrag

**Beziehungen**

- Geschäftsbereich
- Kunde
- Projekt
- Vorlagen
- Leistungen
- Dateien

### Leistung

**Felder**

- Titel
- Beschreibung
- Preis (je Preisliste)
- Mwst.

**Beziehungen**

- Geschäftsbereich
- Angebote
- Aufträge
- Rechnungen