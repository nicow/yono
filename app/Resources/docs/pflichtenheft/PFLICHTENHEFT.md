# Pflichtenheft

## Objekte

### Kalender

#### Termin

**Felder**

- Bezeichnung
- Art (Termin, Außendienst, Urlaub, Krankheit)
- Ganztägig
- Von (Datum/Uhrzeit)
- Bis (Datum/Uhrzeit)
- Mitarbeiter
- Kunde
- Projekt
- Beschreibung

**Beziehungen**

- Benutzer
- Kontakt
- Projekt
- Adresse

### Kontakte

#### Kontakt

**Felder**

- Name
- Kategorien
- Rabatsatz

#### Adresse

**Felder**

- Bezeichnung
- Straße
- PLZ
- Ort
- Bundesland
- Land
- (indiv. Formularfelder)

**Beziehungen**

- Kontakt

#### Ansprechpartner

**Felder**

- Name
- Firma
- Position
- Telefon
- Mobil
- E-Mail
- (indiv. Formularfelder)

**Beziehungen**

- Kontakt

#### Kontoverbindungen

**Felder**

- Inhaber
- Kreditinstitut
- IBAN
- BIC
- Mobil
- E-Mail
- (indiv. Formularfelder)

**Beziehungen**

- Kontakt

#### Zugänge

**Felder**

- Benutzername
- E-Mail
- Gruppe
- Aktiv
- Passwort

**Beziehungen**

- Kontakt


### Buchhaltung

- Geschäftsbereiche
    - Angebote (Kontakte, Projekte, Mitarbeiter, Auftrag, Rechnung)
    - Aufträge (Kontakte, Projekte, Mitarbeiter, Angebot, Rechnung)
    - Rechnungen (Kontakte, Projekte, Mitarbeiter, Angebot, Auftrag)
        - Mahnungen (Kontakte, Projekte, Mitarbeiter)
    - Gutschriften (Kontakte, Projekte, Mitarbeiter)
    - Leistungen
    - Preislisten
    - Vorlagen

### Projekte

- Projekte (Kontakt, Mitarbeiter)
    - Tickets (Mitarbeiter)
    - Arbeitszettel (Mitarbeiter)
- Kategorien

### Inventar

- Arbeitsmittel
    - Verwendung (Mitarbeiter, Projekt)
- Kategorien

### Wiki

- Artikel
- Kategorien

### Zeiterfassung

- Eintrag

### Benutzer

- Benutzer
- Kategorien
- Berechtigungen (alle Objekte)

### Anhänge

- Dateien (Angebote, Aufträge, Rechnungen, Mahnungen, Gutschriften, Tickets, Artikel)
- Notizen (Tickets)

## Funktion

# Kalender

- Anlegen, Bearbeiten, Löschen von Terminen
- Tages, Wochen und Monatsansicht
- Darestellung von Objekten mit Datum (Projekte, Tickets)


## Oberfläche:

### Anmeldung

Anmeldung per Benutzername/E-Mail und Passwort
An mich erinnern (Cookie)
Passwort vergessen

### Navigation

#### Menu

- Übersicht
- Kalender
- Kontakte
  - Alle Kontakte
  - (Kategorie A, B, ...)
  - Kategorien
  - Formularfelder
- Buchhaltung
    - Angebote
    - Aufträge
    - Rechnungen
    - Mahnungen
    - Gutschriften
    - Leistungen
    - Preislisten
    - Vorlagen
    - Geschäftsbereiche
- Projekte
    - Alle Projekte
    - (Kategorie A, B, ...)
    - Tickets
    - Arbeitszettel
    - Kategorien
- Inventar
    - Alle Arbeitsmittel
    - (Kategorie A, B, ...)
    - Suche
    - Kategorien
- Wiki
    - Alle Artikel
    - (Kategorie A, B, ...)
    - Kategorien
- Zeiterfassung
- Benutzer
    - Alle Benutzer
    - Gruppen
    - Berechtigungen
- Mein Profil
- Abmelden

#### Schnellzugriffe

Startseite (individuell)
Neu (Anlegen beliebiger Inhalte)
Favoriten (individuell)
Suche mit Filter (passt sich aktueller Ansicht an)
Zeiterfassung Start/Pause, Ticketauswahl mit Suche
Lesezeichenleiste

### Ansichten

#### Alle Ansichten

Breadcrumb/Orientierung
Überschrift
Bereich für Buttons
Als Startseite festlegen
Zu Lesezeichen/Favoriten hinzufügen
Fehler-/Erfolgsmeldungen (erfolgreich gespeichert/aktualisiert/gelöscht etc.)

#### Liste

Buttons:
    - Neu
Seitennavigation
Einstellungen (Einträge pro Seite, Tabellenspalten ein-/ausblenden)
Sortieren nach Spalten

#### Neu

Button:
    - zur Liste
Berechtigungen setzen

#### Bearbeiten

Buttons:
    - zur Liste
    - Details
    - Löschen
Berechtigungen setzen

#### Details

Buttons:
    - zur Liste
    - Bearbeiten
    - Löschen

### Kalender

Buttons:
    - Mitarbeiterfilter, Neu (Termin, Außendienst, Urlaub, Krankheit)
    - Ansicht wechseln (Monat, Woche, Tag)
    - Vor/Zurück (Monat, Woche, Tag)
Mitarbeiterfilter:
    - Termine von bestimmten Mitarbeitern ein-/ausblenden
    - Tages-/Wochenansicht: Nur ein Mitarbeiter zur Zeit
Termindetails in Popup:
    - Titel
    - Datum/Uhrzeit
    - Mitarbeiter
    - Kunde
    - Projekt
    - Adresse
    - Buttons: auf Karte anzeigen, Bearbeiten, Löschen

#### Wochenansicht

Raster: Tage horizontal, Mitarbeiter vertikal
Termine mit Uhrzeit und Titel, einer oder mehrere pro Zelle

#### Monatsansicht

Klassische Kalender-/Monatsansicht

#### Tagesansicht [?]

Stunden (0-23) vertikal
Termine horizontal

#### Formular

Bezeichnung
Art (Termin, Außendienst, Urlaub, Krankheit)
Von/Bis (Datum Uhrzeit) oder ganztägig
Mitarbeiter
Kunde
Projekt
Beschreibung

### Kontakte

Buttons:
    - Neuer Kontakt
    - Neue Kategorie

#### Listenansicht (Alle und nach Kategorie)

##### Tabelle

Name/Kategorie
Adresse(n)
Ansprechpartner
Kontoverbindungen
Projekte
Tickets (offen/erledigt)
Buttons:
    - Details
    - Bearbeiten
    - Löschen

#### Detailansicht

Buttons:
    - zur Liste
    - Bearbeiten
    - Neue Adresse
    - Neuer Ansprechpartner
    - Neue Kontoverbindung
    - Neues Projekt
    - Neues Ticket
    - Neuer Zugang
    - Neues Angebot
    - Neuer Auftrag
    - Neue Rechnung
    - Neue Mahnung
    - Neue Gutschrift
Kategorie
Rabattsatz

##### Adressen

###### Liste

Bezeichnung
Straße
PLZ
Ort
Bundesland
Land
(indiv. Felder)
Buttons:
    - Bearbeiten
    - Google Maps
    - Löschen

##### Formular

Bezeichnung
Straße
PLZ
Ort
Bundesland
Land
(indiv. Felder)

##### Ansprechpartner

###### Liste

Anrede, Vor-/Nachname
Firma
Position
Telefon (Mobil/Festnetz)
E-Mail
(indiv. Felder)
Buttons:
    - Bearbeiten
    - Löschen

###### Formular

Anrede
Vorname
Nachname
Firma
Position
Mobil
Festnetz
E-Mail
(indiv. Felder)

##### Kontoverbindungen

###### Liste

Inhaber
Kreditinstitut
IBAN
BIC
(indiv. Felder)
Buttons:
    - Bearbeiten
    - Löschen
    
###### Formular

Inhaber
Kreditinstitut
IBAN
BIC
(indiv. Felder)

##### Zugänge

###### Liste

Benutzername
E-Mail
Gruppe
Aktiv
letzte Anmeldung
(indiv. Felder)
Buttons:
    - Bearbeiten
    - Löschen

###### Formular

Benutzername
E-Mail
Gruppe
Aktiv
(indiv. Felder)