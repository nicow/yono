<?php

use ColourStream\Bundle\CronBundle\ColourStreamCronBundle;
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = [
            // Symfony Core
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),

            // Third Party
            new FOS\UserBundle\FOSUserBundle(),
            new FOS\RestBundle\FOSRestBundle(),
            new FOS\OAuthServerBundle\FOSOAuthServerBundle(),
            new FOS\JsRoutingBundle\FOSJsRoutingBundle(),
            new JMS\SerializerBundle\JMSSerializerBundle(),
            new Nelmio\ApiDocBundle\NelmioApiDocBundle(),
            new Knp\Bundle\MenuBundle\KnpMenuBundle(),
            new Knp\Bundle\PaginatorBundle\KnpPaginatorBundle(),
            new Liip\ThemeBundle\LiipThemeBundle(),
            new Ravenberg\UiKitBundle\RavenbergUiKitBundle(),
            new Stof\DoctrineExtensionsBundle\StofDoctrineExtensionsBundle(),
            new Ivory\CKEditorBundle\IvoryCKEditorBundle(),
            new Craue\FormFlowBundle\CraueFormFlowBundle(),
            new Sentry\SentryBundle\SentryBundle(),
            new ColourStreamCronBundle(),

            // Yono Core
            new Core\AppBundle\CoreAppBundle(),
            new Core\OAuthBundle\CoreOAuthBundle(),

            // Modules
            new Modules\CalendarBundle\ModulesCalendarBundle(),
            new Modules\ContactBundle\ModulesContactBundle(),
            new Modules\TeamBundle\ModulesTeamBundle(),
            new Modules\TimetrackBundle\ModulesTimetrackBundle(),
            new Modules\KmUiBundle\ModulesKmUiBundle(),
            new Modules\ProjectBundle\ModulesProjectBundle(),
            new Modules\TicketBundle\ModulesTicketBundle(),
            new Modules\WikiBundle\ModulesWikiBundle(),
            new Modules\InventoryBundle\ModulesInventoryBundle(),
            new Modules\OrderManagementBundle\ModulesOrderManagementBundle(),
            new Modules\ToDoBundle\ModulesToDoBundle(),
        ];

        if (in_array($this->getEnvironment(), ['dev', 'test'], true)) {
            $bundles[] = new Symfony\Bundle\DebugBundle\DebugBundle();
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
        }

        return $bundles;
    }

    public function getRootDir()
    {
        return __DIR__;
    }

    public function getCacheDir()
    {
        return dirname(__DIR__).'/var/cache/'.$this->getEnvironment();
    }

    public function getLogDir()
    {
        return dirname(__DIR__).'/var/logs';
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load($this->getRootDir().'/config/config_'.$this->getEnvironment().'.yml');
    }
}
