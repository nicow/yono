$(function () {
    // set offline.js options
    Offline.options = {checks: {xhr: {url: Routing.generate('core_app_default_connectiontest')}}};

    // init clipboad buttons
    clipboard = new Clipboard('.clipboard-button');

    clipboard.on('success', function (e) {
        UIkit.notify({
            message: 'Text "' + e.text + '" in Zwischenablage kopiert.',
            status: 'info',
            timeout: 3000,
            pos: 'top-center'
        });

        e.clearSelection();
    });

    jQuery.extend(jQuery.validator.messages, {
        required: "Dies ist ein Pflichtfeld"
    });

    //VALIDATE FORMS
    $('#validate_form').validate(
        {
            rules: {
                field: {
                    required: true,
                    digits: true,
                    special: true
                }
            },
            messages: {
                required: "Dies ist ein Pflichtfeld"
            }
        }
    );

    //REMOVE TOOLTIPS ON MOBILE DEVICES
    if (('ontouchstart' in window)) {
        $("[data-uk-tooltip]").removeAttr("data-uk-tooltip");
    }


    // init select2
    $('.select2').select2({language: 'de'});
    $(document).on('show.uk.modal', '.uk-modal', function () {
        $('.select2').select2({language: 'de'});
    });
    $(document).on('show.uk.switcher', '.uk-switcher', function () {
        $('.select2').select2({language: 'de'});
    });

    // ajax modals
    $(document).on('click', '.ajax-modal', function (e) {
        var opener = $(this),
            url = opener.data('url'),
            icon = opener.find('[class^="uk-icon-"]'),
            iconClass = icon.attr('class'),
            ajaxModal = $('#ajax-modal');

        var modal = UIkit.modal("#ajax-modal");

        if (modal.isActive()) {
            modal.hide();
        } else {
            icon.toggleClass(iconClass + ' uk-icon-spinner uk-icon-spin');
            ajaxModal.load(url, function () {
                ajaxModal.find('.select2').select2();
                icon.toggleClass(iconClass + ' uk-icon-spinner uk-icon-spin');
                modal.show();
            });
        }
    });

    // set navbar shadow on scroll
    $(window).scroll(function () {
        var scroll = $(window).scrollTop();
        if (scroll >= 50) {
            $("#navbar").addClass("box-shadow");
        } else {
            $("#navbar").removeClass("box-shadow");
        }
    });


    $(document).on('click', '.resetFilter', function (e) {
        $('.filterOverview').find("input[type=text], textarea, select").val("").removeAttr('checked').removeAttr('selected');

        $('.filterOverview').submit();
    });

    // search filter
    $(document).on('click', '.search-filter-set-all', function (e) {
        e.preventDefault();
        $('.searchfilter').find('.searchfilter-item').each(function (i, item) {
            $(item).find('[type="checkbox"]').prop('checked', true);
            $(item).find('.uk-button').addClass('uk-button-success');
        });
        $(this).parents('.searchfilter').find('form').submit();
    });
    $(document).on('click', '.search-filter-set-none', function (e) {
        e.preventDefault();
        $('.searchfilter').find('.searchfilter-item').each(function (i, item) {
            $(item).find('[type="checkbox"]').prop('checked', false);
            $(item).find('.uk-button').removeClass('uk-button-success');
        });
        $(this).parents('.searchfilter').find('form').submit();
    });

    $(document).on('click', '.searchfilter-item a', function (e) {
        e.preventDefault();
        $(this).toggleClass('uk-button-success');
        $(this).parent().find('input').prop('checked', !$(this).parent().find('input').prop('checked'));
        $(this).parents('form').submit();
    });
    $(document).on('submit', '.searchfilter-form', function (e) {
        e.preventDefault();
        $.post($(this).attr('action'), $(this).serialize());
    });

    // search lock
    $(document).on('click', '.toggle-search-lock', function (e) {
        e.preventDefault();
        $('.toggle-search-lock').toggleClass('uk-button-success').find('i').toggleClass('uk-icon-lock uk-icon-unlock');
        $.post($(this).attr('href'));
    });

    // search collapse filter
    $(document).on('keyup', '#contents .uk-search input', function () {
        $('.searchfilter').addClass('uk-hidden');
    });

    // set home
    $(document).on('click', '#home-toggle', function (e) {
        e.preventDefault();
        $.ajax({
            url: $(this).data('yono-url'),
            method: 'PATCH',
            data: {
                home_route: $(this).data('yono-route'),
                home_parameters: $(this).data('yono-parameters')
            }
        }).done(function () {
            $('#home-link').find('i').animateCss('flash');
            $('#home-link').attr('href', $('#home-toggle').data('yono-path'));
            UIkit.notify({
                message: '<i class="uk-icon-home"></i> Startseite wurde aktualisiert!',
                status: 'info',
                timeout: 2000,
                pos: 'top-center'
            });
        });
    });

    // set bookmark
    $(document).on('click', '#bookmark-toggle', function (e) {
        e.preventDefault();
        if (!$('#bookmarks').find('[href="' + $(this).data('yono-path') + '"]').length) {
            $.ajax({
                url: $(this).data('yono-url'),
                method: 'POST',
                data: {
                    menu_item: {
                        route: $(this).data('yono-route'),
                        route_parameters: $(this).data('yono-parameters'),
                        label: $(this).data('yono-label'),
                        icon: 'uk-icon-' + $(this).data('yono-icon') + ' fa fa-' + $(this).data('yono-icon'),
                        menu: 'bookmarks',
                        order: '0'
                    }
                }
            }).done(function () {
                UIkit.notify({
                    message: '<i class="uk-icon-download"></i> Lesezeichen hinzugefügt!',
                    status: 'info',
                    timeout: 2000,
                    pos: 'top-center'
                });
                $('#bookmarks').fadeOut(function () {
                    $('#bookmarks').load(Routing.generate('kmui_menu_bookmarks'), function () {
                        $('#bookmarks').fadeIn();
                    });
                });
            });
        }
    });

    // remove bookmark
    $(document).on('click', '#bookmarks .delete-menuitem', function (e) {
        e.preventDefault();
        var link = $(this).parent().parent(),
            id = link.data('menuitem-id');
        if (id) {
            $.ajax({
                url: Routing.generate('api_delete_menuitem', {'menuItem': id, 'access_token': yonoAccessToken}),
                method: 'DELETE'
            }).done(function () {
                link.fadeOut(function () {
                    $('#bookmarks').load(Routing.generate('kmui_menu_bookmarks'));
                });
            });
        }
    });

    // set favorite
    $(document).on('click', '#favorite-toggle', function (e) {
        e.preventDefault();
        if (!$('#favorites').find('[href="' + $(this).data('yono-path') + '"]').length) {
            $.ajax({
                url: $(this).data('yono-url'),
                method: 'POST',
                data: {
                    menu_item: {
                        route: $(this).data('yono-route'),
                        route_parameters: $(this).data('yono-parameters'),
                        label: $(this).data('yono-label'),
                        icon: 'uk-icon-' + $(this).data('yono-icon') + ' fa fa-' + $(this).data('yono-icon'),
                        menu: 'favorites',
                        order: '0'
                    }
                }
            }).done(function () {
                $('#favorites').load(Routing.generate('kmui_menu_favorites'));
                $('#favorites-dropdown').find('i').animateCss('flash');
                UIkit.notify({
                    message: '<i class="uk-icon-star"></i> Favoriten wurden aktualisiert!',
                    status: 'info',
                    timeout: 2000,
                    pos: 'top-center'
                });
            });
        }
    });

    // remove favorite
    $(document).on('click', '#favorites .delete-menuitem', function (e) {
        e.preventDefault();
        var link = $(this).parent().parent(),
            id = link.data('menuitem-id');
        if (id) {
            $.ajax({
                url: Routing.generate('api_delete_menuitem', {'menuItem': id, 'access_token': yonoAccessToken}),
                method: 'DELETE'
            }).done(function () {
                link.fadeOut(function () {
                    $('#favorites').load(Routing.generate('kmui_menu_favorites'));
                });
            });
        }
    });


    //TIMETRACK FUNCTIONS
    $(document).on('change', '#form_jobticket', function (e) {
        $('.form_internal').prop('checked', false).checkboxradio("refresh");
    });

    $(document).on('submit', '#timetrackform', function (e) {

        e.preventDefault();
        if ($('#form_jobticket').val() != '0' || $('.form_internal').is(':checked')) {
            $.ajax({
                url: $(this).attr('action'),
                method: 'POST',
                data: $(this).serialize()
            }).done(function (data) {
                reloadTimenav('Du hast dich erfolgreich auf ' + data + ' gebucht.');
                var modal = UIkit.modal("#set-job-modal");

                if (modal.isActive()) {
                    modal.hide();
                }
            });
        }
        else {
            UIkit.notify({
                message: 'Bitte ein Jobticket oder eine interne Tätigkeit auswählen.',
                status: 'danger',
                timeout: 2000,
                pos: 'top-center'
            });
        }


    });

    $(document).on('click', '.continue_timetrack_old', function (e) {
        e.preventDefault();
        $.ajax({
            url: $(this).attr('data-url'),
            method: 'GET'
        }).done(function (data) {
            reloadTimenav('Du hast dich erfolgreich umgebucht');
        });

    });

    $(document).on('click', '.timetrack-toggle', function (e) {
        var modal = UIkit.modal("#set-job-modal");

        if (modal.isActive()) {
            modal.hide();
        } else {
            var _this = $(this);
            if (($(this).attr('data-jobstatus') == '0' && $(this).attr('data-currentjob') == '' ) || $(this).attr('data-jobstatus') == 'new') {

                //RESET VIEW
                $('#time_last').addClass('uk-hidden');
                $('#form_jobticket input').val('');

                $.ajax({
                    url: $(this).data('url'),
                    method: 'GET'
                }).done(function (data) {
                    $('#jobContent').html(data);

                    modal.show();
                });
            }
            else {
                $.ajax({
                    url: $(this).data('break'),
                    method: 'POST',
                    data: {
                        job: $(this).attr('data-currentjob')
                    }
                }).done(function (data) {
                    reloadTimenav('Du hast dich erfolgreich ausgestempelt');
                });
            }

        }
    });

    function reloadTimenav(message) {

        $.ajax({
            url: $('#timeTrackNav').data('url'),
            method: 'GET',
        }).done(function (data) {
            $('#timeTrackNav').html(data);
            if (message) {
                UIkit.notify({
                    message: message,
                    status: 'info',
                    timeout: 2000,
                    pos: 'top-center'
                });
            }

        });
    }

    $(document).ready(function () {
        if ($('#timeTrackNav').length) {
            setInterval(reloadTimenav, 20000);
        }
    });

    //DELETE MARKED TIMETRACK

    $(document).on('change', '.markTimetrack', function (e) {
        if ($('.markTimetrack:checked').val()) {
            $('#buttonDeleteMarkedTimetrack').show();
        }
        else {
            $('#buttonDeleteMarkedTimetrack').hide();
        }
    });

    $(document).on('click', '#buttonDeleteMarkedTimetrack', function (e) {

        $('#timetrackTable').find('.markTimetrack:checked').each(function (index, element) {
            $('#deleteMarkedTimetrack').append('<input type="hidden" value="' + $(this).val() + '" name="deleteall[]" />');
        });

    });


    //SORT CALENDAR ORDER

    $(document).on('click', '.calendarMemberUp', function (e) {
        var position = parseInt($(this).attr('data-position'));
        var positionPrev = position - 1;
        if (position != 1) {
            var copyTR = $('.calendarPosition' + position).html();
            var copyTROLD = $('.calendarPosition' + positionPrev).html();

            $.ajax({
                url: $('.weekCalendarTable').data('url') + '/' + $(this).attr('data-member') + '/' + positionPrev + '/' + $('.calendarPosition' + positionPrev + ' td .calendarMemberUp').attr('data-member') + '/' + position,
                method: 'GET'
            }).done(function (data) {
                UIkit.notify({
                    message: 'Die Positionen wurden getauscht',
                    status: 'info',
                    timeout: 2000,
                    pos: 'top-center'
                });

                $('.calendarPosition' + position).html(copyTROLD);
                $('.calendarPosition' + positionPrev).html(copyTR);

                $('.calendarPosition' + position + ' td .calendarMemberUp').attr('data-position', position);
                $('.calendarPosition' + position + ' td .calendarMemberDown').attr('data-position', position);

                $('.calendarPosition' + positionPrev + ' td .calendarMemberUp').attr('data-position', positionPrev);
                $('.calendarPosition' + positionPrev + ' td .calendarMemberDown').attr('data-position', positionPrev);
            });


        }
    });

    $(document).on('click', '.calendarMemberDown', function (e) {
        var position = parseInt($(this).attr('data-position'));
        var positionNext = position + 1;

        if ($('.calendarPosition' + positionNext).length != 0) {
            var copyTR = $('.calendarPosition' + position).html();
            var copyTROLD = $('.calendarPosition' + positionNext).html();

            $.ajax({
                url: $('.weekCalendarTable').data('url') + '/' + $(this).attr('data-member') + '/' + positionNext + '/' + $('.calendarPosition' + positionNext + ' td .calendarMemberDown').attr('data-member') + '/' + position,
                method: 'GET'
            }).done(function (data) {
                UIkit.notify({
                    message: 'Die Positionen wurden getauscht',
                    status: 'info',
                    timeout: 2000,
                    pos: 'top-center'
                });

                $('.calendarPosition' + position).html(copyTROLD);
                $('.calendarPosition' + positionNext).html(copyTR);

                $('.calendarPosition' + position + ' td .calendarMemberUp').attr('data-position', position);
                $('.calendarPosition' + position + ' td .calendarMemberDown').attr('data-position', position);

                $('.calendarPosition' + positionNext + ' td .calendarMemberUp').attr('data-position', positionNext);
                $('.calendarPosition' + positionNext + ' td .calendarMemberDown').attr('data-position', positionNext);
            });
        }
    });

});


// trigger animations on elements once
$.fn.extend({
    animateCss: function (animationName) {
        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
        this.addClass('animated ' + animationName).one(animationEnd, function () {
            $(this).removeClass('animated ' + animationName);
        });
    }
});