var CACHE_NAME = 'dependencies-cache';

var ticketRequest = new Request('/ticket/', {credentials: 'include'});

// Files required to make this app work offline
var REQUIRED_FILES = [
    ticketRequest,
    '/css/app.min.css',
    '/js/app.min.js',
    '/js/routing?callback=fos.Router.setData',
    '/assets/uikit/img/favicon.png',
    '/assets/uikit/img/texture.jpg',
    '/assets/uikit/vendor/uikit/fonts/fontawesome-webfont.ttf',
    '/assets/uikit/vendor/uikit/fonts/fontawesome-webfont.woff',
    '/assets/uikit/vendor/uikit/fonts/fontawesome-webfont.woff2'
];

self.addEventListener('install', function(evt) {
    console.log('The service worker is being installed.');

    evt.waitUntil(precache());
});


self.addEventListener('fetch', function(evt) {
    console.log('The service worker is serving the asset.');

    evt.respondWith(fromNetwork(evt.request, 1000).catch(function () {
        return fromCache(evt.request);
    }));
});

function precache() {
    return caches.open(CACHE_NAME).then(function (cache) {
        return cache.addAll(REQUIRED_FILES);
    });
}

function fromNetwork(request, timeout) {
    return new Promise(function (fulfill, reject) {

        var timeoutId = setTimeout(reject, timeout);

        fetch(request).then(function (response) {
            clearTimeout(timeoutId);
            fulfill(response);
        }, reject);
    });
}

function fromCache(request) {
    return caches.open(CACHE_NAME).then(function (cache) {
        return cache.match(request).then(function (matching) {
            return matching || Promise.reject('no-match');
        });
    });
}